using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL13
    {
        #region Interop
        static GL13()
        {
            Console.WriteLine("Initalising GL13 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadActiveTexture();
            loadSampleCoverage();
            loadCompressedTexImage3D();
            loadCompressedTexImage2D();
            loadCompressedTexImage1D();
            loadCompressedTexSubImage3D();
            loadCompressedTexSubImage2D();
            loadCompressedTexSubImage1D();
            loadGetCompressedTexImage();
            loadClientActiveTexture();
            loadMultiTexCoord1d();
            loadMultiTexCoord1dv();
            loadMultiTexCoord1f();
            loadMultiTexCoord1fv();
            loadMultiTexCoord1i();
            loadMultiTexCoord1iv();
            loadMultiTexCoord1s();
            loadMultiTexCoord1sv();
            loadMultiTexCoord2d();
            loadMultiTexCoord2dv();
            loadMultiTexCoord2f();
            loadMultiTexCoord2fv();
            loadMultiTexCoord2i();
            loadMultiTexCoord2iv();
            loadMultiTexCoord2s();
            loadMultiTexCoord2sv();
            loadMultiTexCoord3d();
            loadMultiTexCoord3dv();
            loadMultiTexCoord3f();
            loadMultiTexCoord3fv();
            loadMultiTexCoord3i();
            loadMultiTexCoord3iv();
            loadMultiTexCoord3s();
            loadMultiTexCoord3sv();
            loadMultiTexCoord4d();
            loadMultiTexCoord4dv();
            loadMultiTexCoord4f();
            loadMultiTexCoord4fv();
            loadMultiTexCoord4i();
            loadMultiTexCoord4iv();
            loadMultiTexCoord4s();
            loadMultiTexCoord4sv();
            loadLoadTransposeMatrixf();
            loadLoadTransposeMatrixd();
            loadMultTransposeMatrixf();
            loadMultTransposeMatrixd();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE0 = 0x84C0;
        public static UInt32 GL_TEXTURE1 = 0x84C1;
        public static UInt32 GL_TEXTURE2 = 0x84C2;
        public static UInt32 GL_TEXTURE3 = 0x84C3;
        public static UInt32 GL_TEXTURE4 = 0x84C4;
        public static UInt32 GL_TEXTURE5 = 0x84C5;
        public static UInt32 GL_TEXTURE6 = 0x84C6;
        public static UInt32 GL_TEXTURE7 = 0x84C7;
        public static UInt32 GL_TEXTURE8 = 0x84C8;
        public static UInt32 GL_TEXTURE9 = 0x84C9;
        public static UInt32 GL_TEXTURE10 = 0x84CA;
        public static UInt32 GL_TEXTURE11 = 0x84CB;
        public static UInt32 GL_TEXTURE12 = 0x84CC;
        public static UInt32 GL_TEXTURE13 = 0x84CD;
        public static UInt32 GL_TEXTURE14 = 0x84CE;
        public static UInt32 GL_TEXTURE15 = 0x84CF;
        public static UInt32 GL_TEXTURE16 = 0x84D0;
        public static UInt32 GL_TEXTURE17 = 0x84D1;
        public static UInt32 GL_TEXTURE18 = 0x84D2;
        public static UInt32 GL_TEXTURE19 = 0x84D3;
        public static UInt32 GL_TEXTURE20 = 0x84D4;
        public static UInt32 GL_TEXTURE21 = 0x84D5;
        public static UInt32 GL_TEXTURE22 = 0x84D6;
        public static UInt32 GL_TEXTURE23 = 0x84D7;
        public static UInt32 GL_TEXTURE24 = 0x84D8;
        public static UInt32 GL_TEXTURE25 = 0x84D9;
        public static UInt32 GL_TEXTURE26 = 0x84DA;
        public static UInt32 GL_TEXTURE27 = 0x84DB;
        public static UInt32 GL_TEXTURE28 = 0x84DC;
        public static UInt32 GL_TEXTURE29 = 0x84DD;
        public static UInt32 GL_TEXTURE30 = 0x84DE;
        public static UInt32 GL_TEXTURE31 = 0x84DF;
        public static UInt32 GL_ACTIVE_TEXTURE = 0x84E0;
        public static UInt32 GL_MULTISAMPLE = 0x809D;
        public static UInt32 GL_SAMPLE_ALPHA_TO_COVERAGE = 0x809E;
        public static UInt32 GL_SAMPLE_ALPHA_TO_ONE = 0x809F;
        public static UInt32 GL_SAMPLE_COVERAGE = 0x80A0;
        public static UInt32 GL_SAMPLE_BUFFERS = 0x80A8;
        public static UInt32 GL_SAMPLES = 0x80A9;
        public static UInt32 GL_SAMPLE_COVERAGE_VALUE = 0x80AA;
        public static UInt32 GL_SAMPLE_COVERAGE_INVERT = 0x80AB;
        public static UInt32 GL_TEXTURE_CUBE_MAP = 0x8513;
        public static UInt32 GL_TEXTURE_BINDING_CUBE_MAP = 0x8514;
        public static UInt32 GL_TEXTURE_CUBE_MAP_POSITIVE_X = 0x8515;
        public static UInt32 GL_TEXTURE_CUBE_MAP_NEGATIVE_X = 0x8516;
        public static UInt32 GL_TEXTURE_CUBE_MAP_POSITIVE_Y = 0x8517;
        public static UInt32 GL_TEXTURE_CUBE_MAP_NEGATIVE_Y = 0x8518;
        public static UInt32 GL_TEXTURE_CUBE_MAP_POSITIVE_Z = 0x8519;
        public static UInt32 GL_TEXTURE_CUBE_MAP_NEGATIVE_Z = 0x851A;
        public static UInt32 GL_PROXY_TEXTURE_CUBE_MAP = 0x851B;
        public static UInt32 GL_MAX_CUBE_MAP_TEXTURE_SIZE = 0x851C;
        public static UInt32 GL_COMPRESSED_RGB = 0x84ED;
        public static UInt32 GL_COMPRESSED_RGBA = 0x84EE;
        public static UInt32 GL_TEXTURE_COMPRESSION_HINT = 0x84EF;
        public static UInt32 GL_TEXTURE_COMPRESSED_IMAGE_SIZE = 0x86A0;
        public static UInt32 GL_TEXTURE_COMPRESSED = 0x86A1;
        public static UInt32 GL_NUM_COMPRESSED_TEXTURE_FORMATS = 0x86A2;
        public static UInt32 GL_COMPRESSED_TEXTURE_FORMATS = 0x86A3;
        public static UInt32 GL_CLAMP_TO_BORDER = 0x812D;
        public static UInt32 GL_CLIENT_ACTIVE_TEXTURE = 0x84E1;
        public static UInt32 GL_MAX_TEXTURE_UNITS = 0x84E2;
        public static UInt32 GL_TRANSPOSE_MODELVIEW_MATRIX = 0x84E3;
        public static UInt32 GL_TRANSPOSE_PROJECTION_MATRIX = 0x84E4;
        public static UInt32 GL_TRANSPOSE_TEXTURE_MATRIX = 0x84E5;
        public static UInt32 GL_TRANSPOSE_COLOR_MATRIX = 0x84E6;
        public static UInt32 GL_MULTISAMPLE_BIT = 0x20000000;
        public static UInt32 GL_NORMAL_MAP = 0x8511;
        public static UInt32 GL_REFLECTION_MAP = 0x8512;
        public static UInt32 GL_COMPRESSED_ALPHA = 0x84E9;
        public static UInt32 GL_COMPRESSED_LUMINANCE = 0x84EA;
        public static UInt32 GL_COMPRESSED_LUMINANCE_ALPHA = 0x84EB;
        public static UInt32 GL_COMPRESSED_INTENSITY = 0x84EC;
        public static UInt32 GL_COMBINE = 0x8570;
        public static UInt32 GL_COMBINE_RGB = 0x8571;
        public static UInt32 GL_COMBINE_ALPHA = 0x8572;
        public static UInt32 GL_SOURCE0_RGB = 0x8580;
        public static UInt32 GL_SOURCE1_RGB = 0x8581;
        public static UInt32 GL_SOURCE2_RGB = 0x8582;
        public static UInt32 GL_SOURCE0_ALPHA = 0x8588;
        public static UInt32 GL_SOURCE1_ALPHA = 0x8589;
        public static UInt32 GL_SOURCE2_ALPHA = 0x858A;
        public static UInt32 GL_OPERAND0_RGB = 0x8590;
        public static UInt32 GL_OPERAND1_RGB = 0x8591;
        public static UInt32 GL_OPERAND2_RGB = 0x8592;
        public static UInt32 GL_OPERAND0_ALPHA = 0x8598;
        public static UInt32 GL_OPERAND1_ALPHA = 0x8599;
        public static UInt32 GL_OPERAND2_ALPHA = 0x859A;
        public static UInt32 GL_RGB_SCALE = 0x8573;
        public static UInt32 GL_ADD_SIGNED = 0x8574;
        public static UInt32 GL_INTERPOLATE = 0x8575;
        public static UInt32 GL_SUBTRACT = 0x84E7;
        public static UInt32 GL_CONSTANT = 0x8576;
        public static UInt32 GL_PRIMARY_COLOR = 0x8577;
        public static UInt32 GL_PREVIOUS = 0x8578;
        public static UInt32 GL_DOT3_RGB = 0x86AE;
        public static UInt32 GL_DOT3_RGBA = 0x86AF;
        #endregion

        #region Commands
        internal delegate void glActiveTextureFunc(GLenum @texture);
        internal static glActiveTextureFunc glActiveTexturePtr;
        internal static void loadActiveTexture()
        {
            try
            {
                glActiveTexturePtr = (glActiveTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glActiveTexture"), typeof(glActiveTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glActiveTexture'.");
            }
        }
        public static void glActiveTexture(GLenum @texture) => glActiveTexturePtr(@texture);

        internal delegate void glSampleCoverageFunc(GLfloat @value, GLboolean @invert);
        internal static glSampleCoverageFunc glSampleCoveragePtr;
        internal static void loadSampleCoverage()
        {
            try
            {
                glSampleCoveragePtr = (glSampleCoverageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleCoverage"), typeof(glSampleCoverageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleCoverage'.");
            }
        }
        public static void glSampleCoverage(GLfloat @value, GLboolean @invert) => glSampleCoveragePtr(@value, @invert);

        internal delegate void glCompressedTexImage3DFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexImage3DFunc glCompressedTexImage3DPtr;
        internal static void loadCompressedTexImage3D()
        {
            try
            {
                glCompressedTexImage3DPtr = (glCompressedTexImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexImage3D"), typeof(glCompressedTexImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexImage3D'.");
            }
        }
        public static void glCompressedTexImage3D(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLsizei @imageSize, const void * @data) => glCompressedTexImage3DPtr(@target, @level, @internalformat, @width, @height, @depth, @border, @imageSize, @data);

        internal delegate void glCompressedTexImage2DFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexImage2DFunc glCompressedTexImage2DPtr;
        internal static void loadCompressedTexImage2D()
        {
            try
            {
                glCompressedTexImage2DPtr = (glCompressedTexImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexImage2D"), typeof(glCompressedTexImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexImage2D'.");
            }
        }
        public static void glCompressedTexImage2D(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLsizei @imageSize, const void * @data) => glCompressedTexImage2DPtr(@target, @level, @internalformat, @width, @height, @border, @imageSize, @data);

        internal delegate void glCompressedTexImage1DFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLint @border, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexImage1DFunc glCompressedTexImage1DPtr;
        internal static void loadCompressedTexImage1D()
        {
            try
            {
                glCompressedTexImage1DPtr = (glCompressedTexImage1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexImage1D"), typeof(glCompressedTexImage1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexImage1D'.");
            }
        }
        public static void glCompressedTexImage1D(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLint @border, GLsizei @imageSize, const void * @data) => glCompressedTexImage1DPtr(@target, @level, @internalformat, @width, @border, @imageSize, @data);

        internal delegate void glCompressedTexSubImage3DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexSubImage3DFunc glCompressedTexSubImage3DPtr;
        internal static void loadCompressedTexSubImage3D()
        {
            try
            {
                glCompressedTexSubImage3DPtr = (glCompressedTexSubImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexSubImage3D"), typeof(glCompressedTexSubImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexSubImage3D'.");
            }
        }
        public static void glCompressedTexSubImage3D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTexSubImage3DPtr(@target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @imageSize, @data);

        internal delegate void glCompressedTexSubImage2DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexSubImage2DFunc glCompressedTexSubImage2DPtr;
        internal static void loadCompressedTexSubImage2D()
        {
            try
            {
                glCompressedTexSubImage2DPtr = (glCompressedTexSubImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexSubImage2D"), typeof(glCompressedTexSubImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexSubImage2D'.");
            }
        }
        public static void glCompressedTexSubImage2D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTexSubImage2DPtr(@target, @level, @xoffset, @yoffset, @width, @height, @format, @imageSize, @data);

        internal delegate void glCompressedTexSubImage1DFunc(GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexSubImage1DFunc glCompressedTexSubImage1DPtr;
        internal static void loadCompressedTexSubImage1D()
        {
            try
            {
                glCompressedTexSubImage1DPtr = (glCompressedTexSubImage1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexSubImage1D"), typeof(glCompressedTexSubImage1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexSubImage1D'.");
            }
        }
        public static void glCompressedTexSubImage1D(GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTexSubImage1DPtr(@target, @level, @xoffset, @width, @format, @imageSize, @data);

        internal delegate void glGetCompressedTexImageFunc(GLenum @target, GLint @level, void * @img);
        internal static glGetCompressedTexImageFunc glGetCompressedTexImagePtr;
        internal static void loadGetCompressedTexImage()
        {
            try
            {
                glGetCompressedTexImagePtr = (glGetCompressedTexImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCompressedTexImage"), typeof(glGetCompressedTexImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCompressedTexImage'.");
            }
        }
        public static void glGetCompressedTexImage(GLenum @target, GLint @level, void * @img) => glGetCompressedTexImagePtr(@target, @level, @img);

        internal delegate void glClientActiveTextureFunc(GLenum @texture);
        internal static glClientActiveTextureFunc glClientActiveTexturePtr;
        internal static void loadClientActiveTexture()
        {
            try
            {
                glClientActiveTexturePtr = (glClientActiveTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClientActiveTexture"), typeof(glClientActiveTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClientActiveTexture'.");
            }
        }
        public static void glClientActiveTexture(GLenum @texture) => glClientActiveTexturePtr(@texture);

        internal delegate void glMultiTexCoord1dFunc(GLenum @target, GLdouble @s);
        internal static glMultiTexCoord1dFunc glMultiTexCoord1dPtr;
        internal static void loadMultiTexCoord1d()
        {
            try
            {
                glMultiTexCoord1dPtr = (glMultiTexCoord1dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1d"), typeof(glMultiTexCoord1dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1d'.");
            }
        }
        public static void glMultiTexCoord1d(GLenum @target, GLdouble @s) => glMultiTexCoord1dPtr(@target, @s);

        internal delegate void glMultiTexCoord1dvFunc(GLenum @target, const GLdouble * @v);
        internal static glMultiTexCoord1dvFunc glMultiTexCoord1dvPtr;
        internal static void loadMultiTexCoord1dv()
        {
            try
            {
                glMultiTexCoord1dvPtr = (glMultiTexCoord1dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1dv"), typeof(glMultiTexCoord1dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1dv'.");
            }
        }
        public static void glMultiTexCoord1dv(GLenum @target, const GLdouble * @v) => glMultiTexCoord1dvPtr(@target, @v);

        internal delegate void glMultiTexCoord1fFunc(GLenum @target, GLfloat @s);
        internal static glMultiTexCoord1fFunc glMultiTexCoord1fPtr;
        internal static void loadMultiTexCoord1f()
        {
            try
            {
                glMultiTexCoord1fPtr = (glMultiTexCoord1fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1f"), typeof(glMultiTexCoord1fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1f'.");
            }
        }
        public static void glMultiTexCoord1f(GLenum @target, GLfloat @s) => glMultiTexCoord1fPtr(@target, @s);

        internal delegate void glMultiTexCoord1fvFunc(GLenum @target, const GLfloat * @v);
        internal static glMultiTexCoord1fvFunc glMultiTexCoord1fvPtr;
        internal static void loadMultiTexCoord1fv()
        {
            try
            {
                glMultiTexCoord1fvPtr = (glMultiTexCoord1fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1fv"), typeof(glMultiTexCoord1fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1fv'.");
            }
        }
        public static void glMultiTexCoord1fv(GLenum @target, const GLfloat * @v) => glMultiTexCoord1fvPtr(@target, @v);

        internal delegate void glMultiTexCoord1iFunc(GLenum @target, GLint @s);
        internal static glMultiTexCoord1iFunc glMultiTexCoord1iPtr;
        internal static void loadMultiTexCoord1i()
        {
            try
            {
                glMultiTexCoord1iPtr = (glMultiTexCoord1iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1i"), typeof(glMultiTexCoord1iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1i'.");
            }
        }
        public static void glMultiTexCoord1i(GLenum @target, GLint @s) => glMultiTexCoord1iPtr(@target, @s);

        internal delegate void glMultiTexCoord1ivFunc(GLenum @target, const GLint * @v);
        internal static glMultiTexCoord1ivFunc glMultiTexCoord1ivPtr;
        internal static void loadMultiTexCoord1iv()
        {
            try
            {
                glMultiTexCoord1ivPtr = (glMultiTexCoord1ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1iv"), typeof(glMultiTexCoord1ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1iv'.");
            }
        }
        public static void glMultiTexCoord1iv(GLenum @target, const GLint * @v) => glMultiTexCoord1ivPtr(@target, @v);

        internal delegate void glMultiTexCoord1sFunc(GLenum @target, GLshort @s);
        internal static glMultiTexCoord1sFunc glMultiTexCoord1sPtr;
        internal static void loadMultiTexCoord1s()
        {
            try
            {
                glMultiTexCoord1sPtr = (glMultiTexCoord1sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1s"), typeof(glMultiTexCoord1sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1s'.");
            }
        }
        public static void glMultiTexCoord1s(GLenum @target, GLshort @s) => glMultiTexCoord1sPtr(@target, @s);

        internal delegate void glMultiTexCoord1svFunc(GLenum @target, const GLshort * @v);
        internal static glMultiTexCoord1svFunc glMultiTexCoord1svPtr;
        internal static void loadMultiTexCoord1sv()
        {
            try
            {
                glMultiTexCoord1svPtr = (glMultiTexCoord1svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1sv"), typeof(glMultiTexCoord1svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1sv'.");
            }
        }
        public static void glMultiTexCoord1sv(GLenum @target, const GLshort * @v) => glMultiTexCoord1svPtr(@target, @v);

        internal delegate void glMultiTexCoord2dFunc(GLenum @target, GLdouble @s, GLdouble @t);
        internal static glMultiTexCoord2dFunc glMultiTexCoord2dPtr;
        internal static void loadMultiTexCoord2d()
        {
            try
            {
                glMultiTexCoord2dPtr = (glMultiTexCoord2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2d"), typeof(glMultiTexCoord2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2d'.");
            }
        }
        public static void glMultiTexCoord2d(GLenum @target, GLdouble @s, GLdouble @t) => glMultiTexCoord2dPtr(@target, @s, @t);

        internal delegate void glMultiTexCoord2dvFunc(GLenum @target, const GLdouble * @v);
        internal static glMultiTexCoord2dvFunc glMultiTexCoord2dvPtr;
        internal static void loadMultiTexCoord2dv()
        {
            try
            {
                glMultiTexCoord2dvPtr = (glMultiTexCoord2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2dv"), typeof(glMultiTexCoord2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2dv'.");
            }
        }
        public static void glMultiTexCoord2dv(GLenum @target, const GLdouble * @v) => glMultiTexCoord2dvPtr(@target, @v);

        internal delegate void glMultiTexCoord2fFunc(GLenum @target, GLfloat @s, GLfloat @t);
        internal static glMultiTexCoord2fFunc glMultiTexCoord2fPtr;
        internal static void loadMultiTexCoord2f()
        {
            try
            {
                glMultiTexCoord2fPtr = (glMultiTexCoord2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2f"), typeof(glMultiTexCoord2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2f'.");
            }
        }
        public static void glMultiTexCoord2f(GLenum @target, GLfloat @s, GLfloat @t) => glMultiTexCoord2fPtr(@target, @s, @t);

        internal delegate void glMultiTexCoord2fvFunc(GLenum @target, const GLfloat * @v);
        internal static glMultiTexCoord2fvFunc glMultiTexCoord2fvPtr;
        internal static void loadMultiTexCoord2fv()
        {
            try
            {
                glMultiTexCoord2fvPtr = (glMultiTexCoord2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2fv"), typeof(glMultiTexCoord2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2fv'.");
            }
        }
        public static void glMultiTexCoord2fv(GLenum @target, const GLfloat * @v) => glMultiTexCoord2fvPtr(@target, @v);

        internal delegate void glMultiTexCoord2iFunc(GLenum @target, GLint @s, GLint @t);
        internal static glMultiTexCoord2iFunc glMultiTexCoord2iPtr;
        internal static void loadMultiTexCoord2i()
        {
            try
            {
                glMultiTexCoord2iPtr = (glMultiTexCoord2iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2i"), typeof(glMultiTexCoord2iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2i'.");
            }
        }
        public static void glMultiTexCoord2i(GLenum @target, GLint @s, GLint @t) => glMultiTexCoord2iPtr(@target, @s, @t);

        internal delegate void glMultiTexCoord2ivFunc(GLenum @target, const GLint * @v);
        internal static glMultiTexCoord2ivFunc glMultiTexCoord2ivPtr;
        internal static void loadMultiTexCoord2iv()
        {
            try
            {
                glMultiTexCoord2ivPtr = (glMultiTexCoord2ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2iv"), typeof(glMultiTexCoord2ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2iv'.");
            }
        }
        public static void glMultiTexCoord2iv(GLenum @target, const GLint * @v) => glMultiTexCoord2ivPtr(@target, @v);

        internal delegate void glMultiTexCoord2sFunc(GLenum @target, GLshort @s, GLshort @t);
        internal static glMultiTexCoord2sFunc glMultiTexCoord2sPtr;
        internal static void loadMultiTexCoord2s()
        {
            try
            {
                glMultiTexCoord2sPtr = (glMultiTexCoord2sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2s"), typeof(glMultiTexCoord2sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2s'.");
            }
        }
        public static void glMultiTexCoord2s(GLenum @target, GLshort @s, GLshort @t) => glMultiTexCoord2sPtr(@target, @s, @t);

        internal delegate void glMultiTexCoord2svFunc(GLenum @target, const GLshort * @v);
        internal static glMultiTexCoord2svFunc glMultiTexCoord2svPtr;
        internal static void loadMultiTexCoord2sv()
        {
            try
            {
                glMultiTexCoord2svPtr = (glMultiTexCoord2svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2sv"), typeof(glMultiTexCoord2svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2sv'.");
            }
        }
        public static void glMultiTexCoord2sv(GLenum @target, const GLshort * @v) => glMultiTexCoord2svPtr(@target, @v);

        internal delegate void glMultiTexCoord3dFunc(GLenum @target, GLdouble @s, GLdouble @t, GLdouble @r);
        internal static glMultiTexCoord3dFunc glMultiTexCoord3dPtr;
        internal static void loadMultiTexCoord3d()
        {
            try
            {
                glMultiTexCoord3dPtr = (glMultiTexCoord3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3d"), typeof(glMultiTexCoord3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3d'.");
            }
        }
        public static void glMultiTexCoord3d(GLenum @target, GLdouble @s, GLdouble @t, GLdouble @r) => glMultiTexCoord3dPtr(@target, @s, @t, @r);

        internal delegate void glMultiTexCoord3dvFunc(GLenum @target, const GLdouble * @v);
        internal static glMultiTexCoord3dvFunc glMultiTexCoord3dvPtr;
        internal static void loadMultiTexCoord3dv()
        {
            try
            {
                glMultiTexCoord3dvPtr = (glMultiTexCoord3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3dv"), typeof(glMultiTexCoord3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3dv'.");
            }
        }
        public static void glMultiTexCoord3dv(GLenum @target, const GLdouble * @v) => glMultiTexCoord3dvPtr(@target, @v);

        internal delegate void glMultiTexCoord3fFunc(GLenum @target, GLfloat @s, GLfloat @t, GLfloat @r);
        internal static glMultiTexCoord3fFunc glMultiTexCoord3fPtr;
        internal static void loadMultiTexCoord3f()
        {
            try
            {
                glMultiTexCoord3fPtr = (glMultiTexCoord3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3f"), typeof(glMultiTexCoord3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3f'.");
            }
        }
        public static void glMultiTexCoord3f(GLenum @target, GLfloat @s, GLfloat @t, GLfloat @r) => glMultiTexCoord3fPtr(@target, @s, @t, @r);

        internal delegate void glMultiTexCoord3fvFunc(GLenum @target, const GLfloat * @v);
        internal static glMultiTexCoord3fvFunc glMultiTexCoord3fvPtr;
        internal static void loadMultiTexCoord3fv()
        {
            try
            {
                glMultiTexCoord3fvPtr = (glMultiTexCoord3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3fv"), typeof(glMultiTexCoord3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3fv'.");
            }
        }
        public static void glMultiTexCoord3fv(GLenum @target, const GLfloat * @v) => glMultiTexCoord3fvPtr(@target, @v);

        internal delegate void glMultiTexCoord3iFunc(GLenum @target, GLint @s, GLint @t, GLint @r);
        internal static glMultiTexCoord3iFunc glMultiTexCoord3iPtr;
        internal static void loadMultiTexCoord3i()
        {
            try
            {
                glMultiTexCoord3iPtr = (glMultiTexCoord3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3i"), typeof(glMultiTexCoord3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3i'.");
            }
        }
        public static void glMultiTexCoord3i(GLenum @target, GLint @s, GLint @t, GLint @r) => glMultiTexCoord3iPtr(@target, @s, @t, @r);

        internal delegate void glMultiTexCoord3ivFunc(GLenum @target, const GLint * @v);
        internal static glMultiTexCoord3ivFunc glMultiTexCoord3ivPtr;
        internal static void loadMultiTexCoord3iv()
        {
            try
            {
                glMultiTexCoord3ivPtr = (glMultiTexCoord3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3iv"), typeof(glMultiTexCoord3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3iv'.");
            }
        }
        public static void glMultiTexCoord3iv(GLenum @target, const GLint * @v) => glMultiTexCoord3ivPtr(@target, @v);

        internal delegate void glMultiTexCoord3sFunc(GLenum @target, GLshort @s, GLshort @t, GLshort @r);
        internal static glMultiTexCoord3sFunc glMultiTexCoord3sPtr;
        internal static void loadMultiTexCoord3s()
        {
            try
            {
                glMultiTexCoord3sPtr = (glMultiTexCoord3sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3s"), typeof(glMultiTexCoord3sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3s'.");
            }
        }
        public static void glMultiTexCoord3s(GLenum @target, GLshort @s, GLshort @t, GLshort @r) => glMultiTexCoord3sPtr(@target, @s, @t, @r);

        internal delegate void glMultiTexCoord3svFunc(GLenum @target, const GLshort * @v);
        internal static glMultiTexCoord3svFunc glMultiTexCoord3svPtr;
        internal static void loadMultiTexCoord3sv()
        {
            try
            {
                glMultiTexCoord3svPtr = (glMultiTexCoord3svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3sv"), typeof(glMultiTexCoord3svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3sv'.");
            }
        }
        public static void glMultiTexCoord3sv(GLenum @target, const GLshort * @v) => glMultiTexCoord3svPtr(@target, @v);

        internal delegate void glMultiTexCoord4dFunc(GLenum @target, GLdouble @s, GLdouble @t, GLdouble @r, GLdouble @q);
        internal static glMultiTexCoord4dFunc glMultiTexCoord4dPtr;
        internal static void loadMultiTexCoord4d()
        {
            try
            {
                glMultiTexCoord4dPtr = (glMultiTexCoord4dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4d"), typeof(glMultiTexCoord4dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4d'.");
            }
        }
        public static void glMultiTexCoord4d(GLenum @target, GLdouble @s, GLdouble @t, GLdouble @r, GLdouble @q) => glMultiTexCoord4dPtr(@target, @s, @t, @r, @q);

        internal delegate void glMultiTexCoord4dvFunc(GLenum @target, const GLdouble * @v);
        internal static glMultiTexCoord4dvFunc glMultiTexCoord4dvPtr;
        internal static void loadMultiTexCoord4dv()
        {
            try
            {
                glMultiTexCoord4dvPtr = (glMultiTexCoord4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4dv"), typeof(glMultiTexCoord4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4dv'.");
            }
        }
        public static void glMultiTexCoord4dv(GLenum @target, const GLdouble * @v) => glMultiTexCoord4dvPtr(@target, @v);

        internal delegate void glMultiTexCoord4fFunc(GLenum @target, GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @q);
        internal static glMultiTexCoord4fFunc glMultiTexCoord4fPtr;
        internal static void loadMultiTexCoord4f()
        {
            try
            {
                glMultiTexCoord4fPtr = (glMultiTexCoord4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4f"), typeof(glMultiTexCoord4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4f'.");
            }
        }
        public static void glMultiTexCoord4f(GLenum @target, GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @q) => glMultiTexCoord4fPtr(@target, @s, @t, @r, @q);

        internal delegate void glMultiTexCoord4fvFunc(GLenum @target, const GLfloat * @v);
        internal static glMultiTexCoord4fvFunc glMultiTexCoord4fvPtr;
        internal static void loadMultiTexCoord4fv()
        {
            try
            {
                glMultiTexCoord4fvPtr = (glMultiTexCoord4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4fv"), typeof(glMultiTexCoord4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4fv'.");
            }
        }
        public static void glMultiTexCoord4fv(GLenum @target, const GLfloat * @v) => glMultiTexCoord4fvPtr(@target, @v);

        internal delegate void glMultiTexCoord4iFunc(GLenum @target, GLint @s, GLint @t, GLint @r, GLint @q);
        internal static glMultiTexCoord4iFunc glMultiTexCoord4iPtr;
        internal static void loadMultiTexCoord4i()
        {
            try
            {
                glMultiTexCoord4iPtr = (glMultiTexCoord4iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4i"), typeof(glMultiTexCoord4iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4i'.");
            }
        }
        public static void glMultiTexCoord4i(GLenum @target, GLint @s, GLint @t, GLint @r, GLint @q) => glMultiTexCoord4iPtr(@target, @s, @t, @r, @q);

        internal delegate void glMultiTexCoord4ivFunc(GLenum @target, const GLint * @v);
        internal static glMultiTexCoord4ivFunc glMultiTexCoord4ivPtr;
        internal static void loadMultiTexCoord4iv()
        {
            try
            {
                glMultiTexCoord4ivPtr = (glMultiTexCoord4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4iv"), typeof(glMultiTexCoord4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4iv'.");
            }
        }
        public static void glMultiTexCoord4iv(GLenum @target, const GLint * @v) => glMultiTexCoord4ivPtr(@target, @v);

        internal delegate void glMultiTexCoord4sFunc(GLenum @target, GLshort @s, GLshort @t, GLshort @r, GLshort @q);
        internal static glMultiTexCoord4sFunc glMultiTexCoord4sPtr;
        internal static void loadMultiTexCoord4s()
        {
            try
            {
                glMultiTexCoord4sPtr = (glMultiTexCoord4sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4s"), typeof(glMultiTexCoord4sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4s'.");
            }
        }
        public static void glMultiTexCoord4s(GLenum @target, GLshort @s, GLshort @t, GLshort @r, GLshort @q) => glMultiTexCoord4sPtr(@target, @s, @t, @r, @q);

        internal delegate void glMultiTexCoord4svFunc(GLenum @target, const GLshort * @v);
        internal static glMultiTexCoord4svFunc glMultiTexCoord4svPtr;
        internal static void loadMultiTexCoord4sv()
        {
            try
            {
                glMultiTexCoord4svPtr = (glMultiTexCoord4svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4sv"), typeof(glMultiTexCoord4svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4sv'.");
            }
        }
        public static void glMultiTexCoord4sv(GLenum @target, const GLshort * @v) => glMultiTexCoord4svPtr(@target, @v);

        internal delegate void glLoadTransposeMatrixfFunc(const GLfloat * @m);
        internal static glLoadTransposeMatrixfFunc glLoadTransposeMatrixfPtr;
        internal static void loadLoadTransposeMatrixf()
        {
            try
            {
                glLoadTransposeMatrixfPtr = (glLoadTransposeMatrixfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadTransposeMatrixf"), typeof(glLoadTransposeMatrixfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadTransposeMatrixf'.");
            }
        }
        public static void glLoadTransposeMatrixf(const GLfloat * @m) => glLoadTransposeMatrixfPtr(@m);

        internal delegate void glLoadTransposeMatrixdFunc(const GLdouble * @m);
        internal static glLoadTransposeMatrixdFunc glLoadTransposeMatrixdPtr;
        internal static void loadLoadTransposeMatrixd()
        {
            try
            {
                glLoadTransposeMatrixdPtr = (glLoadTransposeMatrixdFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadTransposeMatrixd"), typeof(glLoadTransposeMatrixdFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadTransposeMatrixd'.");
            }
        }
        public static void glLoadTransposeMatrixd(const GLdouble * @m) => glLoadTransposeMatrixdPtr(@m);

        internal delegate void glMultTransposeMatrixfFunc(const GLfloat * @m);
        internal static glMultTransposeMatrixfFunc glMultTransposeMatrixfPtr;
        internal static void loadMultTransposeMatrixf()
        {
            try
            {
                glMultTransposeMatrixfPtr = (glMultTransposeMatrixfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultTransposeMatrixf"), typeof(glMultTransposeMatrixfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultTransposeMatrixf'.");
            }
        }
        public static void glMultTransposeMatrixf(const GLfloat * @m) => glMultTransposeMatrixfPtr(@m);

        internal delegate void glMultTransposeMatrixdFunc(const GLdouble * @m);
        internal static glMultTransposeMatrixdFunc glMultTransposeMatrixdPtr;
        internal static void loadMultTransposeMatrixd()
        {
            try
            {
                glMultTransposeMatrixdPtr = (glMultTransposeMatrixdFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultTransposeMatrixd"), typeof(glMultTransposeMatrixdFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultTransposeMatrixd'.");
            }
        }
        public static void glMultTransposeMatrixd(const GLdouble * @m) => glMultTransposeMatrixdPtr(@m);
        #endregion
    }
}
