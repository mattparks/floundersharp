using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARM_shader_framebuffer_fetch
    {
        #region Interop
        static GL_ARM_shader_framebuffer_fetch()
        {
            Console.WriteLine("Initalising GL_ARM_shader_framebuffer_fetch interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FETCH_PER_SAMPLE_ARM = 0x8F65;
        public static UInt32 GL_FRAGMENT_SHADER_FRAMEBUFFER_FETCH_MRT_ARM = 0x8F66;
        #endregion

        #region Commands
        #endregion
    }
}
