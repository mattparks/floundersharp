using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SUN_slice_accum
    {
        #region Interop
        static GL_SUN_slice_accum()
        {
            Console.WriteLine("Initalising GL_SUN_slice_accum interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SLICE_ACCUM_SUN = 0x85CC;
        #endregion

        #region Commands
        #endregion
    }
}
