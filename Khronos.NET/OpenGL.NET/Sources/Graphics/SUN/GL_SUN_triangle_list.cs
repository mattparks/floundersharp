using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SUN_triangle_list
    {
        #region Interop
        static GL_SUN_triangle_list()
        {
            Console.WriteLine("Initalising GL_SUN_triangle_list interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadReplacementCodeuiSUN();
            loadReplacementCodeusSUN();
            loadReplacementCodeubSUN();
            loadReplacementCodeuivSUN();
            loadReplacementCodeusvSUN();
            loadReplacementCodeubvSUN();
            loadReplacementCodePointerSUN();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RESTART_SUN = 0x0001;
        public static UInt32 GL_REPLACE_MIDDLE_SUN = 0x0002;
        public static UInt32 GL_REPLACE_OLDEST_SUN = 0x0003;
        public static UInt32 GL_TRIANGLE_LIST_SUN = 0x81D7;
        public static UInt32 GL_REPLACEMENT_CODE_SUN = 0x81D8;
        public static UInt32 GL_REPLACEMENT_CODE_ARRAY_SUN = 0x85C0;
        public static UInt32 GL_REPLACEMENT_CODE_ARRAY_TYPE_SUN = 0x85C1;
        public static UInt32 GL_REPLACEMENT_CODE_ARRAY_STRIDE_SUN = 0x85C2;
        public static UInt32 GL_REPLACEMENT_CODE_ARRAY_POINTER_SUN = 0x85C3;
        public static UInt32 GL_R1UI_V3F_SUN = 0x85C4;
        public static UInt32 GL_R1UI_C4UB_V3F_SUN = 0x85C5;
        public static UInt32 GL_R1UI_C3F_V3F_SUN = 0x85C6;
        public static UInt32 GL_R1UI_N3F_V3F_SUN = 0x85C7;
        public static UInt32 GL_R1UI_C4F_N3F_V3F_SUN = 0x85C8;
        public static UInt32 GL_R1UI_T2F_V3F_SUN = 0x85C9;
        public static UInt32 GL_R1UI_T2F_N3F_V3F_SUN = 0x85CA;
        public static UInt32 GL_R1UI_T2F_C4F_N3F_V3F_SUN = 0x85CB;
        #endregion

        #region Commands
        internal delegate void glReplacementCodeuiSUNFunc(GLuint @code);
        internal static glReplacementCodeuiSUNFunc glReplacementCodeuiSUNPtr;
        internal static void loadReplacementCodeuiSUN()
        {
            try
            {
                glReplacementCodeuiSUNPtr = (glReplacementCodeuiSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiSUN"), typeof(glReplacementCodeuiSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiSUN'.");
            }
        }
        public static void glReplacementCodeuiSUN(GLuint @code) => glReplacementCodeuiSUNPtr(@code);

        internal delegate void glReplacementCodeusSUNFunc(GLushort @code);
        internal static glReplacementCodeusSUNFunc glReplacementCodeusSUNPtr;
        internal static void loadReplacementCodeusSUN()
        {
            try
            {
                glReplacementCodeusSUNPtr = (glReplacementCodeusSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeusSUN"), typeof(glReplacementCodeusSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeusSUN'.");
            }
        }
        public static void glReplacementCodeusSUN(GLushort @code) => glReplacementCodeusSUNPtr(@code);

        internal delegate void glReplacementCodeubSUNFunc(GLubyte @code);
        internal static glReplacementCodeubSUNFunc glReplacementCodeubSUNPtr;
        internal static void loadReplacementCodeubSUN()
        {
            try
            {
                glReplacementCodeubSUNPtr = (glReplacementCodeubSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeubSUN"), typeof(glReplacementCodeubSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeubSUN'.");
            }
        }
        public static void glReplacementCodeubSUN(GLubyte @code) => glReplacementCodeubSUNPtr(@code);

        internal delegate void glReplacementCodeuivSUNFunc(const GLuint * @code);
        internal static glReplacementCodeuivSUNFunc glReplacementCodeuivSUNPtr;
        internal static void loadReplacementCodeuivSUN()
        {
            try
            {
                glReplacementCodeuivSUNPtr = (glReplacementCodeuivSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuivSUN"), typeof(glReplacementCodeuivSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuivSUN'.");
            }
        }
        public static void glReplacementCodeuivSUN(const GLuint * @code) => glReplacementCodeuivSUNPtr(@code);

        internal delegate void glReplacementCodeusvSUNFunc(const GLushort * @code);
        internal static glReplacementCodeusvSUNFunc glReplacementCodeusvSUNPtr;
        internal static void loadReplacementCodeusvSUN()
        {
            try
            {
                glReplacementCodeusvSUNPtr = (glReplacementCodeusvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeusvSUN"), typeof(glReplacementCodeusvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeusvSUN'.");
            }
        }
        public static void glReplacementCodeusvSUN(const GLushort * @code) => glReplacementCodeusvSUNPtr(@code);

        internal delegate void glReplacementCodeubvSUNFunc(const GLubyte * @code);
        internal static glReplacementCodeubvSUNFunc glReplacementCodeubvSUNPtr;
        internal static void loadReplacementCodeubvSUN()
        {
            try
            {
                glReplacementCodeubvSUNPtr = (glReplacementCodeubvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeubvSUN"), typeof(glReplacementCodeubvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeubvSUN'.");
            }
        }
        public static void glReplacementCodeubvSUN(const GLubyte * @code) => glReplacementCodeubvSUNPtr(@code);

        internal delegate void glReplacementCodePointerSUNFunc(GLenum @type, GLsizei @stride, const void ** @pointer);
        internal static glReplacementCodePointerSUNFunc glReplacementCodePointerSUNPtr;
        internal static void loadReplacementCodePointerSUN()
        {
            try
            {
                glReplacementCodePointerSUNPtr = (glReplacementCodePointerSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodePointerSUN"), typeof(glReplacementCodePointerSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodePointerSUN'.");
            }
        }
        public static void glReplacementCodePointerSUN(GLenum @type, GLsizei @stride, const void ** @pointer) => glReplacementCodePointerSUNPtr(@type, @stride, @pointer);
        #endregion
    }
}
