using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SUN_global_alpha
    {
        #region Interop
        static GL_SUN_global_alpha()
        {
            Console.WriteLine("Initalising GL_SUN_global_alpha interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGlobalAlphaFactorbSUN();
            loadGlobalAlphaFactorsSUN();
            loadGlobalAlphaFactoriSUN();
            loadGlobalAlphaFactorfSUN();
            loadGlobalAlphaFactordSUN();
            loadGlobalAlphaFactorubSUN();
            loadGlobalAlphaFactorusSUN();
            loadGlobalAlphaFactoruiSUN();
        }
        #endregion

        #region Enums
        public static UInt32 GL_GLOBAL_ALPHA_SUN = 0x81D9;
        public static UInt32 GL_GLOBAL_ALPHA_FACTOR_SUN = 0x81DA;
        #endregion

        #region Commands
        internal delegate void glGlobalAlphaFactorbSUNFunc(GLbyte @factor);
        internal static glGlobalAlphaFactorbSUNFunc glGlobalAlphaFactorbSUNPtr;
        internal static void loadGlobalAlphaFactorbSUN()
        {
            try
            {
                glGlobalAlphaFactorbSUNPtr = (glGlobalAlphaFactorbSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGlobalAlphaFactorbSUN"), typeof(glGlobalAlphaFactorbSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGlobalAlphaFactorbSUN'.");
            }
        }
        public static void glGlobalAlphaFactorbSUN(GLbyte @factor) => glGlobalAlphaFactorbSUNPtr(@factor);

        internal delegate void glGlobalAlphaFactorsSUNFunc(GLshort @factor);
        internal static glGlobalAlphaFactorsSUNFunc glGlobalAlphaFactorsSUNPtr;
        internal static void loadGlobalAlphaFactorsSUN()
        {
            try
            {
                glGlobalAlphaFactorsSUNPtr = (glGlobalAlphaFactorsSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGlobalAlphaFactorsSUN"), typeof(glGlobalAlphaFactorsSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGlobalAlphaFactorsSUN'.");
            }
        }
        public static void glGlobalAlphaFactorsSUN(GLshort @factor) => glGlobalAlphaFactorsSUNPtr(@factor);

        internal delegate void glGlobalAlphaFactoriSUNFunc(GLint @factor);
        internal static glGlobalAlphaFactoriSUNFunc glGlobalAlphaFactoriSUNPtr;
        internal static void loadGlobalAlphaFactoriSUN()
        {
            try
            {
                glGlobalAlphaFactoriSUNPtr = (glGlobalAlphaFactoriSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGlobalAlphaFactoriSUN"), typeof(glGlobalAlphaFactoriSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGlobalAlphaFactoriSUN'.");
            }
        }
        public static void glGlobalAlphaFactoriSUN(GLint @factor) => glGlobalAlphaFactoriSUNPtr(@factor);

        internal delegate void glGlobalAlphaFactorfSUNFunc(GLfloat @factor);
        internal static glGlobalAlphaFactorfSUNFunc glGlobalAlphaFactorfSUNPtr;
        internal static void loadGlobalAlphaFactorfSUN()
        {
            try
            {
                glGlobalAlphaFactorfSUNPtr = (glGlobalAlphaFactorfSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGlobalAlphaFactorfSUN"), typeof(glGlobalAlphaFactorfSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGlobalAlphaFactorfSUN'.");
            }
        }
        public static void glGlobalAlphaFactorfSUN(GLfloat @factor) => glGlobalAlphaFactorfSUNPtr(@factor);

        internal delegate void glGlobalAlphaFactordSUNFunc(GLdouble @factor);
        internal static glGlobalAlphaFactordSUNFunc glGlobalAlphaFactordSUNPtr;
        internal static void loadGlobalAlphaFactordSUN()
        {
            try
            {
                glGlobalAlphaFactordSUNPtr = (glGlobalAlphaFactordSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGlobalAlphaFactordSUN"), typeof(glGlobalAlphaFactordSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGlobalAlphaFactordSUN'.");
            }
        }
        public static void glGlobalAlphaFactordSUN(GLdouble @factor) => glGlobalAlphaFactordSUNPtr(@factor);

        internal delegate void glGlobalAlphaFactorubSUNFunc(GLubyte @factor);
        internal static glGlobalAlphaFactorubSUNFunc glGlobalAlphaFactorubSUNPtr;
        internal static void loadGlobalAlphaFactorubSUN()
        {
            try
            {
                glGlobalAlphaFactorubSUNPtr = (glGlobalAlphaFactorubSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGlobalAlphaFactorubSUN"), typeof(glGlobalAlphaFactorubSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGlobalAlphaFactorubSUN'.");
            }
        }
        public static void glGlobalAlphaFactorubSUN(GLubyte @factor) => glGlobalAlphaFactorubSUNPtr(@factor);

        internal delegate void glGlobalAlphaFactorusSUNFunc(GLushort @factor);
        internal static glGlobalAlphaFactorusSUNFunc glGlobalAlphaFactorusSUNPtr;
        internal static void loadGlobalAlphaFactorusSUN()
        {
            try
            {
                glGlobalAlphaFactorusSUNPtr = (glGlobalAlphaFactorusSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGlobalAlphaFactorusSUN"), typeof(glGlobalAlphaFactorusSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGlobalAlphaFactorusSUN'.");
            }
        }
        public static void glGlobalAlphaFactorusSUN(GLushort @factor) => glGlobalAlphaFactorusSUNPtr(@factor);

        internal delegate void glGlobalAlphaFactoruiSUNFunc(GLuint @factor);
        internal static glGlobalAlphaFactoruiSUNFunc glGlobalAlphaFactoruiSUNPtr;
        internal static void loadGlobalAlphaFactoruiSUN()
        {
            try
            {
                glGlobalAlphaFactoruiSUNPtr = (glGlobalAlphaFactoruiSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGlobalAlphaFactoruiSUN"), typeof(glGlobalAlphaFactoruiSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGlobalAlphaFactoruiSUN'.");
            }
        }
        public static void glGlobalAlphaFactoruiSUN(GLuint @factor) => glGlobalAlphaFactoruiSUNPtr(@factor);
        #endregion
    }
}
