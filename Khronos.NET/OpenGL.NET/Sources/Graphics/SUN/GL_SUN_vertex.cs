using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SUN_vertex
    {
        #region Interop
        static GL_SUN_vertex()
        {
            Console.WriteLine("Initalising GL_SUN_vertex interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadColor4ubVertex2fSUN();
            loadColor4ubVertex2fvSUN();
            loadColor4ubVertex3fSUN();
            loadColor4ubVertex3fvSUN();
            loadColor3fVertex3fSUN();
            loadColor3fVertex3fvSUN();
            loadNormal3fVertex3fSUN();
            loadNormal3fVertex3fvSUN();
            loadColor4fNormal3fVertex3fSUN();
            loadColor4fNormal3fVertex3fvSUN();
            loadTexCoord2fVertex3fSUN();
            loadTexCoord2fVertex3fvSUN();
            loadTexCoord4fVertex4fSUN();
            loadTexCoord4fVertex4fvSUN();
            loadTexCoord2fColor4ubVertex3fSUN();
            loadTexCoord2fColor4ubVertex3fvSUN();
            loadTexCoord2fColor3fVertex3fSUN();
            loadTexCoord2fColor3fVertex3fvSUN();
            loadTexCoord2fNormal3fVertex3fSUN();
            loadTexCoord2fNormal3fVertex3fvSUN();
            loadTexCoord2fColor4fNormal3fVertex3fSUN();
            loadTexCoord2fColor4fNormal3fVertex3fvSUN();
            loadTexCoord4fColor4fNormal3fVertex4fSUN();
            loadTexCoord4fColor4fNormal3fVertex4fvSUN();
            loadReplacementCodeuiVertex3fSUN();
            loadReplacementCodeuiVertex3fvSUN();
            loadReplacementCodeuiColor4ubVertex3fSUN();
            loadReplacementCodeuiColor4ubVertex3fvSUN();
            loadReplacementCodeuiColor3fVertex3fSUN();
            loadReplacementCodeuiColor3fVertex3fvSUN();
            loadReplacementCodeuiNormal3fVertex3fSUN();
            loadReplacementCodeuiNormal3fVertex3fvSUN();
            loadReplacementCodeuiColor4fNormal3fVertex3fSUN();
            loadReplacementCodeuiColor4fNormal3fVertex3fvSUN();
            loadReplacementCodeuiTexCoord2fVertex3fSUN();
            loadReplacementCodeuiTexCoord2fVertex3fvSUN();
            loadReplacementCodeuiTexCoord2fNormal3fVertex3fSUN();
            loadReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN();
            loadReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUN();
            loadReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glColor4ubVertex2fSUNFunc(GLubyte @r, GLubyte @g, GLubyte @b, GLubyte @a, GLfloat @x, GLfloat @y);
        internal static glColor4ubVertex2fSUNFunc glColor4ubVertex2fSUNPtr;
        internal static void loadColor4ubVertex2fSUN()
        {
            try
            {
                glColor4ubVertex2fSUNPtr = (glColor4ubVertex2fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4ubVertex2fSUN"), typeof(glColor4ubVertex2fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4ubVertex2fSUN'.");
            }
        }
        public static void glColor4ubVertex2fSUN(GLubyte @r, GLubyte @g, GLubyte @b, GLubyte @a, GLfloat @x, GLfloat @y) => glColor4ubVertex2fSUNPtr(@r, @g, @b, @a, @x, @y);

        internal delegate void glColor4ubVertex2fvSUNFunc(const GLubyte * @c, const GLfloat * @v);
        internal static glColor4ubVertex2fvSUNFunc glColor4ubVertex2fvSUNPtr;
        internal static void loadColor4ubVertex2fvSUN()
        {
            try
            {
                glColor4ubVertex2fvSUNPtr = (glColor4ubVertex2fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4ubVertex2fvSUN"), typeof(glColor4ubVertex2fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4ubVertex2fvSUN'.");
            }
        }
        public static void glColor4ubVertex2fvSUN(const GLubyte * @c, const GLfloat * @v) => glColor4ubVertex2fvSUNPtr(@c, @v);

        internal delegate void glColor4ubVertex3fSUNFunc(GLubyte @r, GLubyte @g, GLubyte @b, GLubyte @a, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glColor4ubVertex3fSUNFunc glColor4ubVertex3fSUNPtr;
        internal static void loadColor4ubVertex3fSUN()
        {
            try
            {
                glColor4ubVertex3fSUNPtr = (glColor4ubVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4ubVertex3fSUN"), typeof(glColor4ubVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4ubVertex3fSUN'.");
            }
        }
        public static void glColor4ubVertex3fSUN(GLubyte @r, GLubyte @g, GLubyte @b, GLubyte @a, GLfloat @x, GLfloat @y, GLfloat @z) => glColor4ubVertex3fSUNPtr(@r, @g, @b, @a, @x, @y, @z);

        internal delegate void glColor4ubVertex3fvSUNFunc(const GLubyte * @c, const GLfloat * @v);
        internal static glColor4ubVertex3fvSUNFunc glColor4ubVertex3fvSUNPtr;
        internal static void loadColor4ubVertex3fvSUN()
        {
            try
            {
                glColor4ubVertex3fvSUNPtr = (glColor4ubVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4ubVertex3fvSUN"), typeof(glColor4ubVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4ubVertex3fvSUN'.");
            }
        }
        public static void glColor4ubVertex3fvSUN(const GLubyte * @c, const GLfloat * @v) => glColor4ubVertex3fvSUNPtr(@c, @v);

        internal delegate void glColor3fVertex3fSUNFunc(GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glColor3fVertex3fSUNFunc glColor3fVertex3fSUNPtr;
        internal static void loadColor3fVertex3fSUN()
        {
            try
            {
                glColor3fVertex3fSUNPtr = (glColor3fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3fVertex3fSUN"), typeof(glColor3fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3fVertex3fSUN'.");
            }
        }
        public static void glColor3fVertex3fSUN(GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @x, GLfloat @y, GLfloat @z) => glColor3fVertex3fSUNPtr(@r, @g, @b, @x, @y, @z);

        internal delegate void glColor3fVertex3fvSUNFunc(const GLfloat * @c, const GLfloat * @v);
        internal static glColor3fVertex3fvSUNFunc glColor3fVertex3fvSUNPtr;
        internal static void loadColor3fVertex3fvSUN()
        {
            try
            {
                glColor3fVertex3fvSUNPtr = (glColor3fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3fVertex3fvSUN"), typeof(glColor3fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3fVertex3fvSUN'.");
            }
        }
        public static void glColor3fVertex3fvSUN(const GLfloat * @c, const GLfloat * @v) => glColor3fVertex3fvSUNPtr(@c, @v);

        internal delegate void glNormal3fVertex3fSUNFunc(GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glNormal3fVertex3fSUNFunc glNormal3fVertex3fSUNPtr;
        internal static void loadNormal3fVertex3fSUN()
        {
            try
            {
                glNormal3fVertex3fSUNPtr = (glNormal3fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3fVertex3fSUN"), typeof(glNormal3fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3fVertex3fSUN'.");
            }
        }
        public static void glNormal3fVertex3fSUN(GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z) => glNormal3fVertex3fSUNPtr(@nx, @ny, @nz, @x, @y, @z);

        internal delegate void glNormal3fVertex3fvSUNFunc(const GLfloat * @n, const GLfloat * @v);
        internal static glNormal3fVertex3fvSUNFunc glNormal3fVertex3fvSUNPtr;
        internal static void loadNormal3fVertex3fvSUN()
        {
            try
            {
                glNormal3fVertex3fvSUNPtr = (glNormal3fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3fVertex3fvSUN"), typeof(glNormal3fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3fVertex3fvSUN'.");
            }
        }
        public static void glNormal3fVertex3fvSUN(const GLfloat * @n, const GLfloat * @v) => glNormal3fVertex3fvSUNPtr(@n, @v);

        internal delegate void glColor4fNormal3fVertex3fSUNFunc(GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @a, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glColor4fNormal3fVertex3fSUNFunc glColor4fNormal3fVertex3fSUNPtr;
        internal static void loadColor4fNormal3fVertex3fSUN()
        {
            try
            {
                glColor4fNormal3fVertex3fSUNPtr = (glColor4fNormal3fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4fNormal3fVertex3fSUN"), typeof(glColor4fNormal3fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4fNormal3fVertex3fSUN'.");
            }
        }
        public static void glColor4fNormal3fVertex3fSUN(GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @a, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z) => glColor4fNormal3fVertex3fSUNPtr(@r, @g, @b, @a, @nx, @ny, @nz, @x, @y, @z);

        internal delegate void glColor4fNormal3fVertex3fvSUNFunc(const GLfloat * @c, const GLfloat * @n, const GLfloat * @v);
        internal static glColor4fNormal3fVertex3fvSUNFunc glColor4fNormal3fVertex3fvSUNPtr;
        internal static void loadColor4fNormal3fVertex3fvSUN()
        {
            try
            {
                glColor4fNormal3fVertex3fvSUNPtr = (glColor4fNormal3fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4fNormal3fVertex3fvSUN"), typeof(glColor4fNormal3fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4fNormal3fVertex3fvSUN'.");
            }
        }
        public static void glColor4fNormal3fVertex3fvSUN(const GLfloat * @c, const GLfloat * @n, const GLfloat * @v) => glColor4fNormal3fVertex3fvSUNPtr(@c, @n, @v);

        internal delegate void glTexCoord2fVertex3fSUNFunc(GLfloat @s, GLfloat @t, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glTexCoord2fVertex3fSUNFunc glTexCoord2fVertex3fSUNPtr;
        internal static void loadTexCoord2fVertex3fSUN()
        {
            try
            {
                glTexCoord2fVertex3fSUNPtr = (glTexCoord2fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2fVertex3fSUN"), typeof(glTexCoord2fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2fVertex3fSUN'.");
            }
        }
        public static void glTexCoord2fVertex3fSUN(GLfloat @s, GLfloat @t, GLfloat @x, GLfloat @y, GLfloat @z) => glTexCoord2fVertex3fSUNPtr(@s, @t, @x, @y, @z);

        internal delegate void glTexCoord2fVertex3fvSUNFunc(const GLfloat * @tc, const GLfloat * @v);
        internal static glTexCoord2fVertex3fvSUNFunc glTexCoord2fVertex3fvSUNPtr;
        internal static void loadTexCoord2fVertex3fvSUN()
        {
            try
            {
                glTexCoord2fVertex3fvSUNPtr = (glTexCoord2fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2fVertex3fvSUN"), typeof(glTexCoord2fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2fVertex3fvSUN'.");
            }
        }
        public static void glTexCoord2fVertex3fvSUN(const GLfloat * @tc, const GLfloat * @v) => glTexCoord2fVertex3fvSUNPtr(@tc, @v);

        internal delegate void glTexCoord4fVertex4fSUNFunc(GLfloat @s, GLfloat @t, GLfloat @p, GLfloat @q, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glTexCoord4fVertex4fSUNFunc glTexCoord4fVertex4fSUNPtr;
        internal static void loadTexCoord4fVertex4fSUN()
        {
            try
            {
                glTexCoord4fVertex4fSUNPtr = (glTexCoord4fVertex4fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4fVertex4fSUN"), typeof(glTexCoord4fVertex4fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4fVertex4fSUN'.");
            }
        }
        public static void glTexCoord4fVertex4fSUN(GLfloat @s, GLfloat @t, GLfloat @p, GLfloat @q, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glTexCoord4fVertex4fSUNPtr(@s, @t, @p, @q, @x, @y, @z, @w);

        internal delegate void glTexCoord4fVertex4fvSUNFunc(const GLfloat * @tc, const GLfloat * @v);
        internal static glTexCoord4fVertex4fvSUNFunc glTexCoord4fVertex4fvSUNPtr;
        internal static void loadTexCoord4fVertex4fvSUN()
        {
            try
            {
                glTexCoord4fVertex4fvSUNPtr = (glTexCoord4fVertex4fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4fVertex4fvSUN"), typeof(glTexCoord4fVertex4fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4fVertex4fvSUN'.");
            }
        }
        public static void glTexCoord4fVertex4fvSUN(const GLfloat * @tc, const GLfloat * @v) => glTexCoord4fVertex4fvSUNPtr(@tc, @v);

        internal delegate void glTexCoord2fColor4ubVertex3fSUNFunc(GLfloat @s, GLfloat @t, GLubyte @r, GLubyte @g, GLubyte @b, GLubyte @a, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glTexCoord2fColor4ubVertex3fSUNFunc glTexCoord2fColor4ubVertex3fSUNPtr;
        internal static void loadTexCoord2fColor4ubVertex3fSUN()
        {
            try
            {
                glTexCoord2fColor4ubVertex3fSUNPtr = (glTexCoord2fColor4ubVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2fColor4ubVertex3fSUN"), typeof(glTexCoord2fColor4ubVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2fColor4ubVertex3fSUN'.");
            }
        }
        public static void glTexCoord2fColor4ubVertex3fSUN(GLfloat @s, GLfloat @t, GLubyte @r, GLubyte @g, GLubyte @b, GLubyte @a, GLfloat @x, GLfloat @y, GLfloat @z) => glTexCoord2fColor4ubVertex3fSUNPtr(@s, @t, @r, @g, @b, @a, @x, @y, @z);

        internal delegate void glTexCoord2fColor4ubVertex3fvSUNFunc(const GLfloat * @tc, const GLubyte * @c, const GLfloat * @v);
        internal static glTexCoord2fColor4ubVertex3fvSUNFunc glTexCoord2fColor4ubVertex3fvSUNPtr;
        internal static void loadTexCoord2fColor4ubVertex3fvSUN()
        {
            try
            {
                glTexCoord2fColor4ubVertex3fvSUNPtr = (glTexCoord2fColor4ubVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2fColor4ubVertex3fvSUN"), typeof(glTexCoord2fColor4ubVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2fColor4ubVertex3fvSUN'.");
            }
        }
        public static void glTexCoord2fColor4ubVertex3fvSUN(const GLfloat * @tc, const GLubyte * @c, const GLfloat * @v) => glTexCoord2fColor4ubVertex3fvSUNPtr(@tc, @c, @v);

        internal delegate void glTexCoord2fColor3fVertex3fSUNFunc(GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glTexCoord2fColor3fVertex3fSUNFunc glTexCoord2fColor3fVertex3fSUNPtr;
        internal static void loadTexCoord2fColor3fVertex3fSUN()
        {
            try
            {
                glTexCoord2fColor3fVertex3fSUNPtr = (glTexCoord2fColor3fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2fColor3fVertex3fSUN"), typeof(glTexCoord2fColor3fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2fColor3fVertex3fSUN'.");
            }
        }
        public static void glTexCoord2fColor3fVertex3fSUN(GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @x, GLfloat @y, GLfloat @z) => glTexCoord2fColor3fVertex3fSUNPtr(@s, @t, @r, @g, @b, @x, @y, @z);

        internal delegate void glTexCoord2fColor3fVertex3fvSUNFunc(const GLfloat * @tc, const GLfloat * @c, const GLfloat * @v);
        internal static glTexCoord2fColor3fVertex3fvSUNFunc glTexCoord2fColor3fVertex3fvSUNPtr;
        internal static void loadTexCoord2fColor3fVertex3fvSUN()
        {
            try
            {
                glTexCoord2fColor3fVertex3fvSUNPtr = (glTexCoord2fColor3fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2fColor3fVertex3fvSUN"), typeof(glTexCoord2fColor3fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2fColor3fVertex3fvSUN'.");
            }
        }
        public static void glTexCoord2fColor3fVertex3fvSUN(const GLfloat * @tc, const GLfloat * @c, const GLfloat * @v) => glTexCoord2fColor3fVertex3fvSUNPtr(@tc, @c, @v);

        internal delegate void glTexCoord2fNormal3fVertex3fSUNFunc(GLfloat @s, GLfloat @t, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glTexCoord2fNormal3fVertex3fSUNFunc glTexCoord2fNormal3fVertex3fSUNPtr;
        internal static void loadTexCoord2fNormal3fVertex3fSUN()
        {
            try
            {
                glTexCoord2fNormal3fVertex3fSUNPtr = (glTexCoord2fNormal3fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2fNormal3fVertex3fSUN"), typeof(glTexCoord2fNormal3fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2fNormal3fVertex3fSUN'.");
            }
        }
        public static void glTexCoord2fNormal3fVertex3fSUN(GLfloat @s, GLfloat @t, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z) => glTexCoord2fNormal3fVertex3fSUNPtr(@s, @t, @nx, @ny, @nz, @x, @y, @z);

        internal delegate void glTexCoord2fNormal3fVertex3fvSUNFunc(const GLfloat * @tc, const GLfloat * @n, const GLfloat * @v);
        internal static glTexCoord2fNormal3fVertex3fvSUNFunc glTexCoord2fNormal3fVertex3fvSUNPtr;
        internal static void loadTexCoord2fNormal3fVertex3fvSUN()
        {
            try
            {
                glTexCoord2fNormal3fVertex3fvSUNPtr = (glTexCoord2fNormal3fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2fNormal3fVertex3fvSUN"), typeof(glTexCoord2fNormal3fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2fNormal3fVertex3fvSUN'.");
            }
        }
        public static void glTexCoord2fNormal3fVertex3fvSUN(const GLfloat * @tc, const GLfloat * @n, const GLfloat * @v) => glTexCoord2fNormal3fVertex3fvSUNPtr(@tc, @n, @v);

        internal delegate void glTexCoord2fColor4fNormal3fVertex3fSUNFunc(GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @a, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glTexCoord2fColor4fNormal3fVertex3fSUNFunc glTexCoord2fColor4fNormal3fVertex3fSUNPtr;
        internal static void loadTexCoord2fColor4fNormal3fVertex3fSUN()
        {
            try
            {
                glTexCoord2fColor4fNormal3fVertex3fSUNPtr = (glTexCoord2fColor4fNormal3fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2fColor4fNormal3fVertex3fSUN"), typeof(glTexCoord2fColor4fNormal3fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2fColor4fNormal3fVertex3fSUN'.");
            }
        }
        public static void glTexCoord2fColor4fNormal3fVertex3fSUN(GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @a, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z) => glTexCoord2fColor4fNormal3fVertex3fSUNPtr(@s, @t, @r, @g, @b, @a, @nx, @ny, @nz, @x, @y, @z);

        internal delegate void glTexCoord2fColor4fNormal3fVertex3fvSUNFunc(const GLfloat * @tc, const GLfloat * @c, const GLfloat * @n, const GLfloat * @v);
        internal static glTexCoord2fColor4fNormal3fVertex3fvSUNFunc glTexCoord2fColor4fNormal3fVertex3fvSUNPtr;
        internal static void loadTexCoord2fColor4fNormal3fVertex3fvSUN()
        {
            try
            {
                glTexCoord2fColor4fNormal3fVertex3fvSUNPtr = (glTexCoord2fColor4fNormal3fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2fColor4fNormal3fVertex3fvSUN"), typeof(glTexCoord2fColor4fNormal3fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2fColor4fNormal3fVertex3fvSUN'.");
            }
        }
        public static void glTexCoord2fColor4fNormal3fVertex3fvSUN(const GLfloat * @tc, const GLfloat * @c, const GLfloat * @n, const GLfloat * @v) => glTexCoord2fColor4fNormal3fVertex3fvSUNPtr(@tc, @c, @n, @v);

        internal delegate void glTexCoord4fColor4fNormal3fVertex4fSUNFunc(GLfloat @s, GLfloat @t, GLfloat @p, GLfloat @q, GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @a, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glTexCoord4fColor4fNormal3fVertex4fSUNFunc glTexCoord4fColor4fNormal3fVertex4fSUNPtr;
        internal static void loadTexCoord4fColor4fNormal3fVertex4fSUN()
        {
            try
            {
                glTexCoord4fColor4fNormal3fVertex4fSUNPtr = (glTexCoord4fColor4fNormal3fVertex4fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4fColor4fNormal3fVertex4fSUN"), typeof(glTexCoord4fColor4fNormal3fVertex4fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4fColor4fNormal3fVertex4fSUN'.");
            }
        }
        public static void glTexCoord4fColor4fNormal3fVertex4fSUN(GLfloat @s, GLfloat @t, GLfloat @p, GLfloat @q, GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @a, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glTexCoord4fColor4fNormal3fVertex4fSUNPtr(@s, @t, @p, @q, @r, @g, @b, @a, @nx, @ny, @nz, @x, @y, @z, @w);

        internal delegate void glTexCoord4fColor4fNormal3fVertex4fvSUNFunc(const GLfloat * @tc, const GLfloat * @c, const GLfloat * @n, const GLfloat * @v);
        internal static glTexCoord4fColor4fNormal3fVertex4fvSUNFunc glTexCoord4fColor4fNormal3fVertex4fvSUNPtr;
        internal static void loadTexCoord4fColor4fNormal3fVertex4fvSUN()
        {
            try
            {
                glTexCoord4fColor4fNormal3fVertex4fvSUNPtr = (glTexCoord4fColor4fNormal3fVertex4fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4fColor4fNormal3fVertex4fvSUN"), typeof(glTexCoord4fColor4fNormal3fVertex4fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4fColor4fNormal3fVertex4fvSUN'.");
            }
        }
        public static void glTexCoord4fColor4fNormal3fVertex4fvSUN(const GLfloat * @tc, const GLfloat * @c, const GLfloat * @n, const GLfloat * @v) => glTexCoord4fColor4fNormal3fVertex4fvSUNPtr(@tc, @c, @n, @v);

        internal delegate void glReplacementCodeuiVertex3fSUNFunc(GLuint @rc, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glReplacementCodeuiVertex3fSUNFunc glReplacementCodeuiVertex3fSUNPtr;
        internal static void loadReplacementCodeuiVertex3fSUN()
        {
            try
            {
                glReplacementCodeuiVertex3fSUNPtr = (glReplacementCodeuiVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiVertex3fSUN"), typeof(glReplacementCodeuiVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiVertex3fSUN'.");
            }
        }
        public static void glReplacementCodeuiVertex3fSUN(GLuint @rc, GLfloat @x, GLfloat @y, GLfloat @z) => glReplacementCodeuiVertex3fSUNPtr(@rc, @x, @y, @z);

        internal delegate void glReplacementCodeuiVertex3fvSUNFunc(const GLuint * @rc, const GLfloat * @v);
        internal static glReplacementCodeuiVertex3fvSUNFunc glReplacementCodeuiVertex3fvSUNPtr;
        internal static void loadReplacementCodeuiVertex3fvSUN()
        {
            try
            {
                glReplacementCodeuiVertex3fvSUNPtr = (glReplacementCodeuiVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiVertex3fvSUN"), typeof(glReplacementCodeuiVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiVertex3fvSUN'.");
            }
        }
        public static void glReplacementCodeuiVertex3fvSUN(const GLuint * @rc, const GLfloat * @v) => glReplacementCodeuiVertex3fvSUNPtr(@rc, @v);

        internal delegate void glReplacementCodeuiColor4ubVertex3fSUNFunc(GLuint @rc, GLubyte @r, GLubyte @g, GLubyte @b, GLubyte @a, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glReplacementCodeuiColor4ubVertex3fSUNFunc glReplacementCodeuiColor4ubVertex3fSUNPtr;
        internal static void loadReplacementCodeuiColor4ubVertex3fSUN()
        {
            try
            {
                glReplacementCodeuiColor4ubVertex3fSUNPtr = (glReplacementCodeuiColor4ubVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiColor4ubVertex3fSUN"), typeof(glReplacementCodeuiColor4ubVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiColor4ubVertex3fSUN'.");
            }
        }
        public static void glReplacementCodeuiColor4ubVertex3fSUN(GLuint @rc, GLubyte @r, GLubyte @g, GLubyte @b, GLubyte @a, GLfloat @x, GLfloat @y, GLfloat @z) => glReplacementCodeuiColor4ubVertex3fSUNPtr(@rc, @r, @g, @b, @a, @x, @y, @z);

        internal delegate void glReplacementCodeuiColor4ubVertex3fvSUNFunc(const GLuint * @rc, const GLubyte * @c, const GLfloat * @v);
        internal static glReplacementCodeuiColor4ubVertex3fvSUNFunc glReplacementCodeuiColor4ubVertex3fvSUNPtr;
        internal static void loadReplacementCodeuiColor4ubVertex3fvSUN()
        {
            try
            {
                glReplacementCodeuiColor4ubVertex3fvSUNPtr = (glReplacementCodeuiColor4ubVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiColor4ubVertex3fvSUN"), typeof(glReplacementCodeuiColor4ubVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiColor4ubVertex3fvSUN'.");
            }
        }
        public static void glReplacementCodeuiColor4ubVertex3fvSUN(const GLuint * @rc, const GLubyte * @c, const GLfloat * @v) => glReplacementCodeuiColor4ubVertex3fvSUNPtr(@rc, @c, @v);

        internal delegate void glReplacementCodeuiColor3fVertex3fSUNFunc(GLuint @rc, GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glReplacementCodeuiColor3fVertex3fSUNFunc glReplacementCodeuiColor3fVertex3fSUNPtr;
        internal static void loadReplacementCodeuiColor3fVertex3fSUN()
        {
            try
            {
                glReplacementCodeuiColor3fVertex3fSUNPtr = (glReplacementCodeuiColor3fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiColor3fVertex3fSUN"), typeof(glReplacementCodeuiColor3fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiColor3fVertex3fSUN'.");
            }
        }
        public static void glReplacementCodeuiColor3fVertex3fSUN(GLuint @rc, GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @x, GLfloat @y, GLfloat @z) => glReplacementCodeuiColor3fVertex3fSUNPtr(@rc, @r, @g, @b, @x, @y, @z);

        internal delegate void glReplacementCodeuiColor3fVertex3fvSUNFunc(const GLuint * @rc, const GLfloat * @c, const GLfloat * @v);
        internal static glReplacementCodeuiColor3fVertex3fvSUNFunc glReplacementCodeuiColor3fVertex3fvSUNPtr;
        internal static void loadReplacementCodeuiColor3fVertex3fvSUN()
        {
            try
            {
                glReplacementCodeuiColor3fVertex3fvSUNPtr = (glReplacementCodeuiColor3fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiColor3fVertex3fvSUN"), typeof(glReplacementCodeuiColor3fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiColor3fVertex3fvSUN'.");
            }
        }
        public static void glReplacementCodeuiColor3fVertex3fvSUN(const GLuint * @rc, const GLfloat * @c, const GLfloat * @v) => glReplacementCodeuiColor3fVertex3fvSUNPtr(@rc, @c, @v);

        internal delegate void glReplacementCodeuiNormal3fVertex3fSUNFunc(GLuint @rc, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glReplacementCodeuiNormal3fVertex3fSUNFunc glReplacementCodeuiNormal3fVertex3fSUNPtr;
        internal static void loadReplacementCodeuiNormal3fVertex3fSUN()
        {
            try
            {
                glReplacementCodeuiNormal3fVertex3fSUNPtr = (glReplacementCodeuiNormal3fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiNormal3fVertex3fSUN"), typeof(glReplacementCodeuiNormal3fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiNormal3fVertex3fSUN'.");
            }
        }
        public static void glReplacementCodeuiNormal3fVertex3fSUN(GLuint @rc, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z) => glReplacementCodeuiNormal3fVertex3fSUNPtr(@rc, @nx, @ny, @nz, @x, @y, @z);

        internal delegate void glReplacementCodeuiNormal3fVertex3fvSUNFunc(const GLuint * @rc, const GLfloat * @n, const GLfloat * @v);
        internal static glReplacementCodeuiNormal3fVertex3fvSUNFunc glReplacementCodeuiNormal3fVertex3fvSUNPtr;
        internal static void loadReplacementCodeuiNormal3fVertex3fvSUN()
        {
            try
            {
                glReplacementCodeuiNormal3fVertex3fvSUNPtr = (glReplacementCodeuiNormal3fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiNormal3fVertex3fvSUN"), typeof(glReplacementCodeuiNormal3fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiNormal3fVertex3fvSUN'.");
            }
        }
        public static void glReplacementCodeuiNormal3fVertex3fvSUN(const GLuint * @rc, const GLfloat * @n, const GLfloat * @v) => glReplacementCodeuiNormal3fVertex3fvSUNPtr(@rc, @n, @v);

        internal delegate void glReplacementCodeuiColor4fNormal3fVertex3fSUNFunc(GLuint @rc, GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @a, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glReplacementCodeuiColor4fNormal3fVertex3fSUNFunc glReplacementCodeuiColor4fNormal3fVertex3fSUNPtr;
        internal static void loadReplacementCodeuiColor4fNormal3fVertex3fSUN()
        {
            try
            {
                glReplacementCodeuiColor4fNormal3fVertex3fSUNPtr = (glReplacementCodeuiColor4fNormal3fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiColor4fNormal3fVertex3fSUN"), typeof(glReplacementCodeuiColor4fNormal3fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiColor4fNormal3fVertex3fSUN'.");
            }
        }
        public static void glReplacementCodeuiColor4fNormal3fVertex3fSUN(GLuint @rc, GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @a, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z) => glReplacementCodeuiColor4fNormal3fVertex3fSUNPtr(@rc, @r, @g, @b, @a, @nx, @ny, @nz, @x, @y, @z);

        internal delegate void glReplacementCodeuiColor4fNormal3fVertex3fvSUNFunc(const GLuint * @rc, const GLfloat * @c, const GLfloat * @n, const GLfloat * @v);
        internal static glReplacementCodeuiColor4fNormal3fVertex3fvSUNFunc glReplacementCodeuiColor4fNormal3fVertex3fvSUNPtr;
        internal static void loadReplacementCodeuiColor4fNormal3fVertex3fvSUN()
        {
            try
            {
                glReplacementCodeuiColor4fNormal3fVertex3fvSUNPtr = (glReplacementCodeuiColor4fNormal3fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiColor4fNormal3fVertex3fvSUN"), typeof(glReplacementCodeuiColor4fNormal3fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiColor4fNormal3fVertex3fvSUN'.");
            }
        }
        public static void glReplacementCodeuiColor4fNormal3fVertex3fvSUN(const GLuint * @rc, const GLfloat * @c, const GLfloat * @n, const GLfloat * @v) => glReplacementCodeuiColor4fNormal3fVertex3fvSUNPtr(@rc, @c, @n, @v);

        internal delegate void glReplacementCodeuiTexCoord2fVertex3fSUNFunc(GLuint @rc, GLfloat @s, GLfloat @t, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glReplacementCodeuiTexCoord2fVertex3fSUNFunc glReplacementCodeuiTexCoord2fVertex3fSUNPtr;
        internal static void loadReplacementCodeuiTexCoord2fVertex3fSUN()
        {
            try
            {
                glReplacementCodeuiTexCoord2fVertex3fSUNPtr = (glReplacementCodeuiTexCoord2fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiTexCoord2fVertex3fSUN"), typeof(glReplacementCodeuiTexCoord2fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiTexCoord2fVertex3fSUN'.");
            }
        }
        public static void glReplacementCodeuiTexCoord2fVertex3fSUN(GLuint @rc, GLfloat @s, GLfloat @t, GLfloat @x, GLfloat @y, GLfloat @z) => glReplacementCodeuiTexCoord2fVertex3fSUNPtr(@rc, @s, @t, @x, @y, @z);

        internal delegate void glReplacementCodeuiTexCoord2fVertex3fvSUNFunc(const GLuint * @rc, const GLfloat * @tc, const GLfloat * @v);
        internal static glReplacementCodeuiTexCoord2fVertex3fvSUNFunc glReplacementCodeuiTexCoord2fVertex3fvSUNPtr;
        internal static void loadReplacementCodeuiTexCoord2fVertex3fvSUN()
        {
            try
            {
                glReplacementCodeuiTexCoord2fVertex3fvSUNPtr = (glReplacementCodeuiTexCoord2fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiTexCoord2fVertex3fvSUN"), typeof(glReplacementCodeuiTexCoord2fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiTexCoord2fVertex3fvSUN'.");
            }
        }
        public static void glReplacementCodeuiTexCoord2fVertex3fvSUN(const GLuint * @rc, const GLfloat * @tc, const GLfloat * @v) => glReplacementCodeuiTexCoord2fVertex3fvSUNPtr(@rc, @tc, @v);

        internal delegate void glReplacementCodeuiTexCoord2fNormal3fVertex3fSUNFunc(GLuint @rc, GLfloat @s, GLfloat @t, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glReplacementCodeuiTexCoord2fNormal3fVertex3fSUNFunc glReplacementCodeuiTexCoord2fNormal3fVertex3fSUNPtr;
        internal static void loadReplacementCodeuiTexCoord2fNormal3fVertex3fSUN()
        {
            try
            {
                glReplacementCodeuiTexCoord2fNormal3fVertex3fSUNPtr = (glReplacementCodeuiTexCoord2fNormal3fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiTexCoord2fNormal3fVertex3fSUN"), typeof(glReplacementCodeuiTexCoord2fNormal3fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiTexCoord2fNormal3fVertex3fSUN'.");
            }
        }
        public static void glReplacementCodeuiTexCoord2fNormal3fVertex3fSUN(GLuint @rc, GLfloat @s, GLfloat @t, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z) => glReplacementCodeuiTexCoord2fNormal3fVertex3fSUNPtr(@rc, @s, @t, @nx, @ny, @nz, @x, @y, @z);

        internal delegate void glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUNFunc(const GLuint * @rc, const GLfloat * @tc, const GLfloat * @n, const GLfloat * @v);
        internal static glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUNFunc glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUNPtr;
        internal static void loadReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN()
        {
            try
            {
                glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUNPtr = (glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN"), typeof(glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN'.");
            }
        }
        public static void glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN(const GLuint * @rc, const GLfloat * @tc, const GLfloat * @n, const GLfloat * @v) => glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUNPtr(@rc, @tc, @n, @v);

        internal delegate void glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUNFunc(GLuint @rc, GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @a, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUNFunc glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUNPtr;
        internal static void loadReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUN()
        {
            try
            {
                glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUNPtr = (glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUN"), typeof(glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUN'.");
            }
        }
        public static void glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUN(GLuint @rc, GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @g, GLfloat @b, GLfloat @a, GLfloat @nx, GLfloat @ny, GLfloat @nz, GLfloat @x, GLfloat @y, GLfloat @z) => glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUNPtr(@rc, @s, @t, @r, @g, @b, @a, @nx, @ny, @nz, @x, @y, @z);

        internal delegate void glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUNFunc(const GLuint * @rc, const GLfloat * @tc, const GLfloat * @c, const GLfloat * @n, const GLfloat * @v);
        internal static glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUNFunc glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUNPtr;
        internal static void loadReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN()
        {
            try
            {
                glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUNPtr = (glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN"), typeof(glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN'.");
            }
        }
        public static void glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN(const GLuint * @rc, const GLfloat * @tc, const GLfloat * @c, const GLfloat * @n, const GLfloat * @v) => glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUNPtr(@rc, @tc, @c, @n, @v);
        #endregion
    }
}
