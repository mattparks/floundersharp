using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SUN_convolution_border_modes
    {
        #region Interop
        static GL_SUN_convolution_border_modes()
        {
            Console.WriteLine("Initalising GL_SUN_convolution_border_modes interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_WRAP_BORDER_SUN = 0x81D4;
        #endregion

        #region Commands
        #endregion
    }
}
