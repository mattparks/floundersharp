using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SUN_mesh_array
    {
        #region Interop
        static GL_SUN_mesh_array()
        {
            Console.WriteLine("Initalising GL_SUN_mesh_array interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawMeshArraysSUN();
        }
        #endregion

        #region Enums
        public static UInt32 GL_QUAD_MESH_SUN = 0x8614;
        public static UInt32 GL_TRIANGLE_MESH_SUN = 0x8615;
        #endregion

        #region Commands
        internal delegate void glDrawMeshArraysSUNFunc(GLenum @mode, GLint @first, GLsizei @count, GLsizei @width);
        internal static glDrawMeshArraysSUNFunc glDrawMeshArraysSUNPtr;
        internal static void loadDrawMeshArraysSUN()
        {
            try
            {
                glDrawMeshArraysSUNPtr = (glDrawMeshArraysSUNFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawMeshArraysSUN"), typeof(glDrawMeshArraysSUNFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawMeshArraysSUN'.");
            }
        }
        public static void glDrawMeshArraysSUN(GLenum @mode, GLint @first, GLsizei @count, GLsizei @width) => glDrawMeshArraysSUNPtr(@mode, @first, @count, @width);
        #endregion
    }
}
