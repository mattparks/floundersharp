using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OML_resample
    {
        #region Interop
        static GL_OML_resample()
        {
            Console.WriteLine("Initalising GL_OML_resample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_PACK_RESAMPLE_OML = 0x8984;
        public static UInt32 GL_UNPACK_RESAMPLE_OML = 0x8985;
        public static UInt32 GL_RESAMPLE_REPLICATE_OML = 0x8986;
        public static UInt32 GL_RESAMPLE_ZERO_FILL_OML = 0x8987;
        public static UInt32 GL_RESAMPLE_AVERAGE_OML = 0x8988;
        public static UInt32 GL_RESAMPLE_DECIMATE_OML = 0x8989;
        #endregion

        #region Commands
        #endregion
    }
}
