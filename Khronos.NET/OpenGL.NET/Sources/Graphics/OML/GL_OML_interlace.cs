using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OML_interlace
    {
        #region Interop
        static GL_OML_interlace()
        {
            Console.WriteLine("Initalising GL_OML_interlace interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_INTERLACE_OML = 0x8980;
        public static UInt32 GL_INTERLACE_READ_OML = 0x8981;
        #endregion

        #region Commands
        #endregion
    }
}
