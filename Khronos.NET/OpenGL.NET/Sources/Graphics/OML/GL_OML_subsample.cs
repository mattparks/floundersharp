using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OML_subsample
    {
        #region Interop
        static GL_OML_subsample()
        {
            Console.WriteLine("Initalising GL_OML_subsample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FORMAT_SUBSAMPLE_24_24_OML = 0x8982;
        public static UInt32 GL_FORMAT_SUBSAMPLE_244_244_OML = 0x8983;
        #endregion

        #region Commands
        #endregion
    }
}
