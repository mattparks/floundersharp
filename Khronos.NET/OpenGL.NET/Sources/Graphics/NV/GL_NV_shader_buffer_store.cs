using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_shader_buffer_store
    {
        #region Interop
        static GL_NV_shader_buffer_store()
        {
            Console.WriteLine("Initalising GL_NV_shader_buffer_store interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SHADER_GLOBAL_ACCESS_BARRIER_BIT_NV = 0x00000010;
        public static UInt32 GL_READ_WRITE = 0x88BA;
        public static UInt32 GL_WRITE_ONLY = 0x88B9;
        #endregion

        #region Commands
        #endregion
    }
}
