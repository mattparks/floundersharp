using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_texture_multisample
    {
        #region Interop
        static GL_NV_texture_multisample()
        {
            Console.WriteLine("Initalising GL_NV_texture_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexImage2DMultisampleCoverageNV();
            loadTexImage3DMultisampleCoverageNV();
            loadTextureImage2DMultisampleNV();
            loadTextureImage3DMultisampleNV();
            loadTextureImage2DMultisampleCoverageNV();
            loadTextureImage3DMultisampleCoverageNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_COVERAGE_SAMPLES_NV = 0x9045;
        public static UInt32 GL_TEXTURE_COLOR_SAMPLES_NV = 0x9046;
        #endregion

        #region Commands
        internal delegate void glTexImage2DMultisampleCoverageNVFunc(GLenum @target, GLsizei @coverageSamples, GLsizei @colorSamples, GLint @internalFormat, GLsizei @width, GLsizei @height, GLboolean @fixedSampleLocations);
        internal static glTexImage2DMultisampleCoverageNVFunc glTexImage2DMultisampleCoverageNVPtr;
        internal static void loadTexImage2DMultisampleCoverageNV()
        {
            try
            {
                glTexImage2DMultisampleCoverageNVPtr = (glTexImage2DMultisampleCoverageNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage2DMultisampleCoverageNV"), typeof(glTexImage2DMultisampleCoverageNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage2DMultisampleCoverageNV'.");
            }
        }
        public static void glTexImage2DMultisampleCoverageNV(GLenum @target, GLsizei @coverageSamples, GLsizei @colorSamples, GLint @internalFormat, GLsizei @width, GLsizei @height, GLboolean @fixedSampleLocations) => glTexImage2DMultisampleCoverageNVPtr(@target, @coverageSamples, @colorSamples, @internalFormat, @width, @height, @fixedSampleLocations);

        internal delegate void glTexImage3DMultisampleCoverageNVFunc(GLenum @target, GLsizei @coverageSamples, GLsizei @colorSamples, GLint @internalFormat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedSampleLocations);
        internal static glTexImage3DMultisampleCoverageNVFunc glTexImage3DMultisampleCoverageNVPtr;
        internal static void loadTexImage3DMultisampleCoverageNV()
        {
            try
            {
                glTexImage3DMultisampleCoverageNVPtr = (glTexImage3DMultisampleCoverageNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage3DMultisampleCoverageNV"), typeof(glTexImage3DMultisampleCoverageNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage3DMultisampleCoverageNV'.");
            }
        }
        public static void glTexImage3DMultisampleCoverageNV(GLenum @target, GLsizei @coverageSamples, GLsizei @colorSamples, GLint @internalFormat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedSampleLocations) => glTexImage3DMultisampleCoverageNVPtr(@target, @coverageSamples, @colorSamples, @internalFormat, @width, @height, @depth, @fixedSampleLocations);

        internal delegate void glTextureImage2DMultisampleNVFunc(GLuint @texture, GLenum @target, GLsizei @samples, GLint @internalFormat, GLsizei @width, GLsizei @height, GLboolean @fixedSampleLocations);
        internal static glTextureImage2DMultisampleNVFunc glTextureImage2DMultisampleNVPtr;
        internal static void loadTextureImage2DMultisampleNV()
        {
            try
            {
                glTextureImage2DMultisampleNVPtr = (glTextureImage2DMultisampleNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureImage2DMultisampleNV"), typeof(glTextureImage2DMultisampleNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureImage2DMultisampleNV'.");
            }
        }
        public static void glTextureImage2DMultisampleNV(GLuint @texture, GLenum @target, GLsizei @samples, GLint @internalFormat, GLsizei @width, GLsizei @height, GLboolean @fixedSampleLocations) => glTextureImage2DMultisampleNVPtr(@texture, @target, @samples, @internalFormat, @width, @height, @fixedSampleLocations);

        internal delegate void glTextureImage3DMultisampleNVFunc(GLuint @texture, GLenum @target, GLsizei @samples, GLint @internalFormat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedSampleLocations);
        internal static glTextureImage3DMultisampleNVFunc glTextureImage3DMultisampleNVPtr;
        internal static void loadTextureImage3DMultisampleNV()
        {
            try
            {
                glTextureImage3DMultisampleNVPtr = (glTextureImage3DMultisampleNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureImage3DMultisampleNV"), typeof(glTextureImage3DMultisampleNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureImage3DMultisampleNV'.");
            }
        }
        public static void glTextureImage3DMultisampleNV(GLuint @texture, GLenum @target, GLsizei @samples, GLint @internalFormat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedSampleLocations) => glTextureImage3DMultisampleNVPtr(@texture, @target, @samples, @internalFormat, @width, @height, @depth, @fixedSampleLocations);

        internal delegate void glTextureImage2DMultisampleCoverageNVFunc(GLuint @texture, GLenum @target, GLsizei @coverageSamples, GLsizei @colorSamples, GLint @internalFormat, GLsizei @width, GLsizei @height, GLboolean @fixedSampleLocations);
        internal static glTextureImage2DMultisampleCoverageNVFunc glTextureImage2DMultisampleCoverageNVPtr;
        internal static void loadTextureImage2DMultisampleCoverageNV()
        {
            try
            {
                glTextureImage2DMultisampleCoverageNVPtr = (glTextureImage2DMultisampleCoverageNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureImage2DMultisampleCoverageNV"), typeof(glTextureImage2DMultisampleCoverageNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureImage2DMultisampleCoverageNV'.");
            }
        }
        public static void glTextureImage2DMultisampleCoverageNV(GLuint @texture, GLenum @target, GLsizei @coverageSamples, GLsizei @colorSamples, GLint @internalFormat, GLsizei @width, GLsizei @height, GLboolean @fixedSampleLocations) => glTextureImage2DMultisampleCoverageNVPtr(@texture, @target, @coverageSamples, @colorSamples, @internalFormat, @width, @height, @fixedSampleLocations);

        internal delegate void glTextureImage3DMultisampleCoverageNVFunc(GLuint @texture, GLenum @target, GLsizei @coverageSamples, GLsizei @colorSamples, GLint @internalFormat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedSampleLocations);
        internal static glTextureImage3DMultisampleCoverageNVFunc glTextureImage3DMultisampleCoverageNVPtr;
        internal static void loadTextureImage3DMultisampleCoverageNV()
        {
            try
            {
                glTextureImage3DMultisampleCoverageNVPtr = (glTextureImage3DMultisampleCoverageNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureImage3DMultisampleCoverageNV"), typeof(glTextureImage3DMultisampleCoverageNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureImage3DMultisampleCoverageNV'.");
            }
        }
        public static void glTextureImage3DMultisampleCoverageNV(GLuint @texture, GLenum @target, GLsizei @coverageSamples, GLsizei @colorSamples, GLint @internalFormat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedSampleLocations) => glTextureImage3DMultisampleCoverageNVPtr(@texture, @target, @coverageSamples, @colorSamples, @internalFormat, @width, @height, @depth, @fixedSampleLocations);
        #endregion
    }
}
