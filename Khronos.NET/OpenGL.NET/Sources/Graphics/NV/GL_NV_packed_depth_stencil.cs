using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_packed_depth_stencil
    {
        #region Interop
        static GL_NV_packed_depth_stencil()
        {
            Console.WriteLine("Initalising GL_NV_packed_depth_stencil interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_STENCIL_NV = 0x84F9;
        public static UInt32 GL_UNSIGNED_INT_24_8_NV = 0x84FA;
        #endregion

        #region Commands
        #endregion
    }
}
