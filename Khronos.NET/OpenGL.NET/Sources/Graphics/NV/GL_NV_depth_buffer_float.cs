using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_depth_buffer_float
    {
        #region Interop
        static GL_NV_depth_buffer_float()
        {
            Console.WriteLine("Initalising GL_NV_depth_buffer_float interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDepthRangedNV();
            loadClearDepthdNV();
            loadDepthBoundsdNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_COMPONENT32F_NV = 0x8DAB;
        public static UInt32 GL_DEPTH32F_STENCIL8_NV = 0x8DAC;
        public static UInt32 GL_FLOAT_32_UNSIGNED_INT_24_8_REV_NV = 0x8DAD;
        public static UInt32 GL_DEPTH_BUFFER_FLOAT_MODE_NV = 0x8DAF;
        #endregion

        #region Commands
        internal delegate void glDepthRangedNVFunc(GLdouble @zNear, GLdouble @zFar);
        internal static glDepthRangedNVFunc glDepthRangedNVPtr;
        internal static void loadDepthRangedNV()
        {
            try
            {
                glDepthRangedNVPtr = (glDepthRangedNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangedNV"), typeof(glDepthRangedNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangedNV'.");
            }
        }
        public static void glDepthRangedNV(GLdouble @zNear, GLdouble @zFar) => glDepthRangedNVPtr(@zNear, @zFar);

        internal delegate void glClearDepthdNVFunc(GLdouble @depth);
        internal static glClearDepthdNVFunc glClearDepthdNVPtr;
        internal static void loadClearDepthdNV()
        {
            try
            {
                glClearDepthdNVPtr = (glClearDepthdNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearDepthdNV"), typeof(glClearDepthdNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearDepthdNV'.");
            }
        }
        public static void glClearDepthdNV(GLdouble @depth) => glClearDepthdNVPtr(@depth);

        internal delegate void glDepthBoundsdNVFunc(GLdouble @zmin, GLdouble @zmax);
        internal static glDepthBoundsdNVFunc glDepthBoundsdNVPtr;
        internal static void loadDepthBoundsdNV()
        {
            try
            {
                glDepthBoundsdNVPtr = (glDepthBoundsdNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthBoundsdNV"), typeof(glDepthBoundsdNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthBoundsdNV'.");
            }
        }
        public static void glDepthBoundsdNV(GLdouble @zmin, GLdouble @zmax) => glDepthBoundsdNVPtr(@zmin, @zmax);
        #endregion
    }
}
