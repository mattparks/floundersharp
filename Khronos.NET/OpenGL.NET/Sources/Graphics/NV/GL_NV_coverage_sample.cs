using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_coverage_sample
    {
        #region Interop
        static GL_NV_coverage_sample()
        {
            Console.WriteLine("Initalising GL_NV_coverage_sample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCoverageMaskNV();
            loadCoverageOperationNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COVERAGE_COMPONENT_NV = 0x8ED0;
        public static UInt32 GL_COVERAGE_COMPONENT4_NV = 0x8ED1;
        public static UInt32 GL_COVERAGE_ATTACHMENT_NV = 0x8ED2;
        public static UInt32 GL_COVERAGE_BUFFERS_NV = 0x8ED3;
        public static UInt32 GL_COVERAGE_SAMPLES_NV = 0x8ED4;
        public static UInt32 GL_COVERAGE_ALL_FRAGMENTS_NV = 0x8ED5;
        public static UInt32 GL_COVERAGE_EDGE_FRAGMENTS_NV = 0x8ED6;
        public static UInt32 GL_COVERAGE_AUTOMATIC_NV = 0x8ED7;
        public static UInt32 GL_COVERAGE_BUFFER_BIT_NV = 0x00008000;
        #endregion

        #region Commands
        internal delegate void glCoverageMaskNVFunc(GLboolean @mask);
        internal static glCoverageMaskNVFunc glCoverageMaskNVPtr;
        internal static void loadCoverageMaskNV()
        {
            try
            {
                glCoverageMaskNVPtr = (glCoverageMaskNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCoverageMaskNV"), typeof(glCoverageMaskNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCoverageMaskNV'.");
            }
        }
        public static void glCoverageMaskNV(GLboolean @mask) => glCoverageMaskNVPtr(@mask);

        internal delegate void glCoverageOperationNVFunc(GLenum @operation);
        internal static glCoverageOperationNVFunc glCoverageOperationNVPtr;
        internal static void loadCoverageOperationNV()
        {
            try
            {
                glCoverageOperationNVPtr = (glCoverageOperationNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCoverageOperationNV"), typeof(glCoverageOperationNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCoverageOperationNV'.");
            }
        }
        public static void glCoverageOperationNV(GLenum @operation) => glCoverageOperationNVPtr(@operation);
        #endregion
    }
}
