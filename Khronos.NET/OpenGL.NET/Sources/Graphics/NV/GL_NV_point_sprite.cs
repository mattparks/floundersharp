using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_point_sprite
    {
        #region Interop
        static GL_NV_point_sprite()
        {
            Console.WriteLine("Initalising GL_NV_point_sprite interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPointParameteriNV();
            loadPointParameterivNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_POINT_SPRITE_NV = 0x8861;
        public static UInt32 GL_COORD_REPLACE_NV = 0x8862;
        public static UInt32 GL_POINT_SPRITE_R_MODE_NV = 0x8863;
        #endregion

        #region Commands
        internal delegate void glPointParameteriNVFunc(GLenum @pname, GLint @param);
        internal static glPointParameteriNVFunc glPointParameteriNVPtr;
        internal static void loadPointParameteriNV()
        {
            try
            {
                glPointParameteriNVPtr = (glPointParameteriNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameteriNV"), typeof(glPointParameteriNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameteriNV'.");
            }
        }
        public static void glPointParameteriNV(GLenum @pname, GLint @param) => glPointParameteriNVPtr(@pname, @param);

        internal delegate void glPointParameterivNVFunc(GLenum @pname, const GLint * @params);
        internal static glPointParameterivNVFunc glPointParameterivNVPtr;
        internal static void loadPointParameterivNV()
        {
            try
            {
                glPointParameterivNVPtr = (glPointParameterivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterivNV"), typeof(glPointParameterivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterivNV'.");
            }
        }
        public static void glPointParameterivNV(GLenum @pname, const GLint * @params) => glPointParameterivNVPtr(@pname, @params);
        #endregion
    }
}
