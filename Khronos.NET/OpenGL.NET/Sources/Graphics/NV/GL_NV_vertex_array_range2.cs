using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_vertex_array_range2
    {
        #region Interop
        static GL_NV_vertex_array_range2()
        {
            Console.WriteLine("Initalising GL_NV_vertex_array_range2 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ARRAY_RANGE_WITHOUT_FLUSH_NV = 0x8533;
        #endregion

        #region Commands
        #endregion
    }
}
