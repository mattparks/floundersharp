using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_half_float
    {
        #region Interop
        static GL_NV_half_float()
        {
            Console.WriteLine("Initalising GL_NV_half_float interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertex2hNV();
            loadVertex2hvNV();
            loadVertex3hNV();
            loadVertex3hvNV();
            loadVertex4hNV();
            loadVertex4hvNV();
            loadNormal3hNV();
            loadNormal3hvNV();
            loadColor3hNV();
            loadColor3hvNV();
            loadColor4hNV();
            loadColor4hvNV();
            loadTexCoord1hNV();
            loadTexCoord1hvNV();
            loadTexCoord2hNV();
            loadTexCoord2hvNV();
            loadTexCoord3hNV();
            loadTexCoord3hvNV();
            loadTexCoord4hNV();
            loadTexCoord4hvNV();
            loadMultiTexCoord1hNV();
            loadMultiTexCoord1hvNV();
            loadMultiTexCoord2hNV();
            loadMultiTexCoord2hvNV();
            loadMultiTexCoord3hNV();
            loadMultiTexCoord3hvNV();
            loadMultiTexCoord4hNV();
            loadMultiTexCoord4hvNV();
            loadFogCoordhNV();
            loadFogCoordhvNV();
            loadSecondaryColor3hNV();
            loadSecondaryColor3hvNV();
            loadVertexWeighthNV();
            loadVertexWeighthvNV();
            loadVertexAttrib1hNV();
            loadVertexAttrib1hvNV();
            loadVertexAttrib2hNV();
            loadVertexAttrib2hvNV();
            loadVertexAttrib3hNV();
            loadVertexAttrib3hvNV();
            loadVertexAttrib4hNV();
            loadVertexAttrib4hvNV();
            loadVertexAttribs1hvNV();
            loadVertexAttribs2hvNV();
            loadVertexAttribs3hvNV();
            loadVertexAttribs4hvNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_HALF_FLOAT_NV = 0x140B;
        #endregion

        #region Commands
        internal delegate void glVertex2hNVFunc(GLhalfNV @x, GLhalfNV @y);
        internal static glVertex2hNVFunc glVertex2hNVPtr;
        internal static void loadVertex2hNV()
        {
            try
            {
                glVertex2hNVPtr = (glVertex2hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2hNV"), typeof(glVertex2hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2hNV'.");
            }
        }
        public static void glVertex2hNV(GLhalfNV @x, GLhalfNV @y) => glVertex2hNVPtr(@x, @y);

        internal delegate void glVertex2hvNVFunc(const GLhalfNV * @v);
        internal static glVertex2hvNVFunc glVertex2hvNVPtr;
        internal static void loadVertex2hvNV()
        {
            try
            {
                glVertex2hvNVPtr = (glVertex2hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2hvNV"), typeof(glVertex2hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2hvNV'.");
            }
        }
        public static void glVertex2hvNV(const GLhalfNV * @v) => glVertex2hvNVPtr(@v);

        internal delegate void glVertex3hNVFunc(GLhalfNV @x, GLhalfNV @y, GLhalfNV @z);
        internal static glVertex3hNVFunc glVertex3hNVPtr;
        internal static void loadVertex3hNV()
        {
            try
            {
                glVertex3hNVPtr = (glVertex3hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3hNV"), typeof(glVertex3hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3hNV'.");
            }
        }
        public static void glVertex3hNV(GLhalfNV @x, GLhalfNV @y, GLhalfNV @z) => glVertex3hNVPtr(@x, @y, @z);

        internal delegate void glVertex3hvNVFunc(const GLhalfNV * @v);
        internal static glVertex3hvNVFunc glVertex3hvNVPtr;
        internal static void loadVertex3hvNV()
        {
            try
            {
                glVertex3hvNVPtr = (glVertex3hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3hvNV"), typeof(glVertex3hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3hvNV'.");
            }
        }
        public static void glVertex3hvNV(const GLhalfNV * @v) => glVertex3hvNVPtr(@v);

        internal delegate void glVertex4hNVFunc(GLhalfNV @x, GLhalfNV @y, GLhalfNV @z, GLhalfNV @w);
        internal static glVertex4hNVFunc glVertex4hNVPtr;
        internal static void loadVertex4hNV()
        {
            try
            {
                glVertex4hNVPtr = (glVertex4hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4hNV"), typeof(glVertex4hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4hNV'.");
            }
        }
        public static void glVertex4hNV(GLhalfNV @x, GLhalfNV @y, GLhalfNV @z, GLhalfNV @w) => glVertex4hNVPtr(@x, @y, @z, @w);

        internal delegate void glVertex4hvNVFunc(const GLhalfNV * @v);
        internal static glVertex4hvNVFunc glVertex4hvNVPtr;
        internal static void loadVertex4hvNV()
        {
            try
            {
                glVertex4hvNVPtr = (glVertex4hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4hvNV"), typeof(glVertex4hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4hvNV'.");
            }
        }
        public static void glVertex4hvNV(const GLhalfNV * @v) => glVertex4hvNVPtr(@v);

        internal delegate void glNormal3hNVFunc(GLhalfNV @nx, GLhalfNV @ny, GLhalfNV @nz);
        internal static glNormal3hNVFunc glNormal3hNVPtr;
        internal static void loadNormal3hNV()
        {
            try
            {
                glNormal3hNVPtr = (glNormal3hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3hNV"), typeof(glNormal3hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3hNV'.");
            }
        }
        public static void glNormal3hNV(GLhalfNV @nx, GLhalfNV @ny, GLhalfNV @nz) => glNormal3hNVPtr(@nx, @ny, @nz);

        internal delegate void glNormal3hvNVFunc(const GLhalfNV * @v);
        internal static glNormal3hvNVFunc glNormal3hvNVPtr;
        internal static void loadNormal3hvNV()
        {
            try
            {
                glNormal3hvNVPtr = (glNormal3hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3hvNV"), typeof(glNormal3hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3hvNV'.");
            }
        }
        public static void glNormal3hvNV(const GLhalfNV * @v) => glNormal3hvNVPtr(@v);

        internal delegate void glColor3hNVFunc(GLhalfNV @red, GLhalfNV @green, GLhalfNV @blue);
        internal static glColor3hNVFunc glColor3hNVPtr;
        internal static void loadColor3hNV()
        {
            try
            {
                glColor3hNVPtr = (glColor3hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3hNV"), typeof(glColor3hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3hNV'.");
            }
        }
        public static void glColor3hNV(GLhalfNV @red, GLhalfNV @green, GLhalfNV @blue) => glColor3hNVPtr(@red, @green, @blue);

        internal delegate void glColor3hvNVFunc(const GLhalfNV * @v);
        internal static glColor3hvNVFunc glColor3hvNVPtr;
        internal static void loadColor3hvNV()
        {
            try
            {
                glColor3hvNVPtr = (glColor3hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3hvNV"), typeof(glColor3hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3hvNV'.");
            }
        }
        public static void glColor3hvNV(const GLhalfNV * @v) => glColor3hvNVPtr(@v);

        internal delegate void glColor4hNVFunc(GLhalfNV @red, GLhalfNV @green, GLhalfNV @blue, GLhalfNV @alpha);
        internal static glColor4hNVFunc glColor4hNVPtr;
        internal static void loadColor4hNV()
        {
            try
            {
                glColor4hNVPtr = (glColor4hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4hNV"), typeof(glColor4hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4hNV'.");
            }
        }
        public static void glColor4hNV(GLhalfNV @red, GLhalfNV @green, GLhalfNV @blue, GLhalfNV @alpha) => glColor4hNVPtr(@red, @green, @blue, @alpha);

        internal delegate void glColor4hvNVFunc(const GLhalfNV * @v);
        internal static glColor4hvNVFunc glColor4hvNVPtr;
        internal static void loadColor4hvNV()
        {
            try
            {
                glColor4hvNVPtr = (glColor4hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4hvNV"), typeof(glColor4hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4hvNV'.");
            }
        }
        public static void glColor4hvNV(const GLhalfNV * @v) => glColor4hvNVPtr(@v);

        internal delegate void glTexCoord1hNVFunc(GLhalfNV @s);
        internal static glTexCoord1hNVFunc glTexCoord1hNVPtr;
        internal static void loadTexCoord1hNV()
        {
            try
            {
                glTexCoord1hNVPtr = (glTexCoord1hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1hNV"), typeof(glTexCoord1hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1hNV'.");
            }
        }
        public static void glTexCoord1hNV(GLhalfNV @s) => glTexCoord1hNVPtr(@s);

        internal delegate void glTexCoord1hvNVFunc(const GLhalfNV * @v);
        internal static glTexCoord1hvNVFunc glTexCoord1hvNVPtr;
        internal static void loadTexCoord1hvNV()
        {
            try
            {
                glTexCoord1hvNVPtr = (glTexCoord1hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1hvNV"), typeof(glTexCoord1hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1hvNV'.");
            }
        }
        public static void glTexCoord1hvNV(const GLhalfNV * @v) => glTexCoord1hvNVPtr(@v);

        internal delegate void glTexCoord2hNVFunc(GLhalfNV @s, GLhalfNV @t);
        internal static glTexCoord2hNVFunc glTexCoord2hNVPtr;
        internal static void loadTexCoord2hNV()
        {
            try
            {
                glTexCoord2hNVPtr = (glTexCoord2hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2hNV"), typeof(glTexCoord2hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2hNV'.");
            }
        }
        public static void glTexCoord2hNV(GLhalfNV @s, GLhalfNV @t) => glTexCoord2hNVPtr(@s, @t);

        internal delegate void glTexCoord2hvNVFunc(const GLhalfNV * @v);
        internal static glTexCoord2hvNVFunc glTexCoord2hvNVPtr;
        internal static void loadTexCoord2hvNV()
        {
            try
            {
                glTexCoord2hvNVPtr = (glTexCoord2hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2hvNV"), typeof(glTexCoord2hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2hvNV'.");
            }
        }
        public static void glTexCoord2hvNV(const GLhalfNV * @v) => glTexCoord2hvNVPtr(@v);

        internal delegate void glTexCoord3hNVFunc(GLhalfNV @s, GLhalfNV @t, GLhalfNV @r);
        internal static glTexCoord3hNVFunc glTexCoord3hNVPtr;
        internal static void loadTexCoord3hNV()
        {
            try
            {
                glTexCoord3hNVPtr = (glTexCoord3hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3hNV"), typeof(glTexCoord3hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3hNV'.");
            }
        }
        public static void glTexCoord3hNV(GLhalfNV @s, GLhalfNV @t, GLhalfNV @r) => glTexCoord3hNVPtr(@s, @t, @r);

        internal delegate void glTexCoord3hvNVFunc(const GLhalfNV * @v);
        internal static glTexCoord3hvNVFunc glTexCoord3hvNVPtr;
        internal static void loadTexCoord3hvNV()
        {
            try
            {
                glTexCoord3hvNVPtr = (glTexCoord3hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3hvNV"), typeof(glTexCoord3hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3hvNV'.");
            }
        }
        public static void glTexCoord3hvNV(const GLhalfNV * @v) => glTexCoord3hvNVPtr(@v);

        internal delegate void glTexCoord4hNVFunc(GLhalfNV @s, GLhalfNV @t, GLhalfNV @r, GLhalfNV @q);
        internal static glTexCoord4hNVFunc glTexCoord4hNVPtr;
        internal static void loadTexCoord4hNV()
        {
            try
            {
                glTexCoord4hNVPtr = (glTexCoord4hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4hNV"), typeof(glTexCoord4hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4hNV'.");
            }
        }
        public static void glTexCoord4hNV(GLhalfNV @s, GLhalfNV @t, GLhalfNV @r, GLhalfNV @q) => glTexCoord4hNVPtr(@s, @t, @r, @q);

        internal delegate void glTexCoord4hvNVFunc(const GLhalfNV * @v);
        internal static glTexCoord4hvNVFunc glTexCoord4hvNVPtr;
        internal static void loadTexCoord4hvNV()
        {
            try
            {
                glTexCoord4hvNVPtr = (glTexCoord4hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4hvNV"), typeof(glTexCoord4hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4hvNV'.");
            }
        }
        public static void glTexCoord4hvNV(const GLhalfNV * @v) => glTexCoord4hvNVPtr(@v);

        internal delegate void glMultiTexCoord1hNVFunc(GLenum @target, GLhalfNV @s);
        internal static glMultiTexCoord1hNVFunc glMultiTexCoord1hNVPtr;
        internal static void loadMultiTexCoord1hNV()
        {
            try
            {
                glMultiTexCoord1hNVPtr = (glMultiTexCoord1hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1hNV"), typeof(glMultiTexCoord1hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1hNV'.");
            }
        }
        public static void glMultiTexCoord1hNV(GLenum @target, GLhalfNV @s) => glMultiTexCoord1hNVPtr(@target, @s);

        internal delegate void glMultiTexCoord1hvNVFunc(GLenum @target, const GLhalfNV * @v);
        internal static glMultiTexCoord1hvNVFunc glMultiTexCoord1hvNVPtr;
        internal static void loadMultiTexCoord1hvNV()
        {
            try
            {
                glMultiTexCoord1hvNVPtr = (glMultiTexCoord1hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1hvNV"), typeof(glMultiTexCoord1hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1hvNV'.");
            }
        }
        public static void glMultiTexCoord1hvNV(GLenum @target, const GLhalfNV * @v) => glMultiTexCoord1hvNVPtr(@target, @v);

        internal delegate void glMultiTexCoord2hNVFunc(GLenum @target, GLhalfNV @s, GLhalfNV @t);
        internal static glMultiTexCoord2hNVFunc glMultiTexCoord2hNVPtr;
        internal static void loadMultiTexCoord2hNV()
        {
            try
            {
                glMultiTexCoord2hNVPtr = (glMultiTexCoord2hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2hNV"), typeof(glMultiTexCoord2hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2hNV'.");
            }
        }
        public static void glMultiTexCoord2hNV(GLenum @target, GLhalfNV @s, GLhalfNV @t) => glMultiTexCoord2hNVPtr(@target, @s, @t);

        internal delegate void glMultiTexCoord2hvNVFunc(GLenum @target, const GLhalfNV * @v);
        internal static glMultiTexCoord2hvNVFunc glMultiTexCoord2hvNVPtr;
        internal static void loadMultiTexCoord2hvNV()
        {
            try
            {
                glMultiTexCoord2hvNVPtr = (glMultiTexCoord2hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2hvNV"), typeof(glMultiTexCoord2hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2hvNV'.");
            }
        }
        public static void glMultiTexCoord2hvNV(GLenum @target, const GLhalfNV * @v) => glMultiTexCoord2hvNVPtr(@target, @v);

        internal delegate void glMultiTexCoord3hNVFunc(GLenum @target, GLhalfNV @s, GLhalfNV @t, GLhalfNV @r);
        internal static glMultiTexCoord3hNVFunc glMultiTexCoord3hNVPtr;
        internal static void loadMultiTexCoord3hNV()
        {
            try
            {
                glMultiTexCoord3hNVPtr = (glMultiTexCoord3hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3hNV"), typeof(glMultiTexCoord3hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3hNV'.");
            }
        }
        public static void glMultiTexCoord3hNV(GLenum @target, GLhalfNV @s, GLhalfNV @t, GLhalfNV @r) => glMultiTexCoord3hNVPtr(@target, @s, @t, @r);

        internal delegate void glMultiTexCoord3hvNVFunc(GLenum @target, const GLhalfNV * @v);
        internal static glMultiTexCoord3hvNVFunc glMultiTexCoord3hvNVPtr;
        internal static void loadMultiTexCoord3hvNV()
        {
            try
            {
                glMultiTexCoord3hvNVPtr = (glMultiTexCoord3hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3hvNV"), typeof(glMultiTexCoord3hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3hvNV'.");
            }
        }
        public static void glMultiTexCoord3hvNV(GLenum @target, const GLhalfNV * @v) => glMultiTexCoord3hvNVPtr(@target, @v);

        internal delegate void glMultiTexCoord4hNVFunc(GLenum @target, GLhalfNV @s, GLhalfNV @t, GLhalfNV @r, GLhalfNV @q);
        internal static glMultiTexCoord4hNVFunc glMultiTexCoord4hNVPtr;
        internal static void loadMultiTexCoord4hNV()
        {
            try
            {
                glMultiTexCoord4hNVPtr = (glMultiTexCoord4hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4hNV"), typeof(glMultiTexCoord4hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4hNV'.");
            }
        }
        public static void glMultiTexCoord4hNV(GLenum @target, GLhalfNV @s, GLhalfNV @t, GLhalfNV @r, GLhalfNV @q) => glMultiTexCoord4hNVPtr(@target, @s, @t, @r, @q);

        internal delegate void glMultiTexCoord4hvNVFunc(GLenum @target, const GLhalfNV * @v);
        internal static glMultiTexCoord4hvNVFunc glMultiTexCoord4hvNVPtr;
        internal static void loadMultiTexCoord4hvNV()
        {
            try
            {
                glMultiTexCoord4hvNVPtr = (glMultiTexCoord4hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4hvNV"), typeof(glMultiTexCoord4hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4hvNV'.");
            }
        }
        public static void glMultiTexCoord4hvNV(GLenum @target, const GLhalfNV * @v) => glMultiTexCoord4hvNVPtr(@target, @v);

        internal delegate void glFogCoordhNVFunc(GLhalfNV @fog);
        internal static glFogCoordhNVFunc glFogCoordhNVPtr;
        internal static void loadFogCoordhNV()
        {
            try
            {
                glFogCoordhNVPtr = (glFogCoordhNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoordhNV"), typeof(glFogCoordhNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoordhNV'.");
            }
        }
        public static void glFogCoordhNV(GLhalfNV @fog) => glFogCoordhNVPtr(@fog);

        internal delegate void glFogCoordhvNVFunc(const GLhalfNV * @fog);
        internal static glFogCoordhvNVFunc glFogCoordhvNVPtr;
        internal static void loadFogCoordhvNV()
        {
            try
            {
                glFogCoordhvNVPtr = (glFogCoordhvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoordhvNV"), typeof(glFogCoordhvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoordhvNV'.");
            }
        }
        public static void glFogCoordhvNV(const GLhalfNV * @fog) => glFogCoordhvNVPtr(@fog);

        internal delegate void glSecondaryColor3hNVFunc(GLhalfNV @red, GLhalfNV @green, GLhalfNV @blue);
        internal static glSecondaryColor3hNVFunc glSecondaryColor3hNVPtr;
        internal static void loadSecondaryColor3hNV()
        {
            try
            {
                glSecondaryColor3hNVPtr = (glSecondaryColor3hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3hNV"), typeof(glSecondaryColor3hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3hNV'.");
            }
        }
        public static void glSecondaryColor3hNV(GLhalfNV @red, GLhalfNV @green, GLhalfNV @blue) => glSecondaryColor3hNVPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3hvNVFunc(const GLhalfNV * @v);
        internal static glSecondaryColor3hvNVFunc glSecondaryColor3hvNVPtr;
        internal static void loadSecondaryColor3hvNV()
        {
            try
            {
                glSecondaryColor3hvNVPtr = (glSecondaryColor3hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3hvNV"), typeof(glSecondaryColor3hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3hvNV'.");
            }
        }
        public static void glSecondaryColor3hvNV(const GLhalfNV * @v) => glSecondaryColor3hvNVPtr(@v);

        internal delegate void glVertexWeighthNVFunc(GLhalfNV @weight);
        internal static glVertexWeighthNVFunc glVertexWeighthNVPtr;
        internal static void loadVertexWeighthNV()
        {
            try
            {
                glVertexWeighthNVPtr = (glVertexWeighthNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexWeighthNV"), typeof(glVertexWeighthNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexWeighthNV'.");
            }
        }
        public static void glVertexWeighthNV(GLhalfNV @weight) => glVertexWeighthNVPtr(@weight);

        internal delegate void glVertexWeighthvNVFunc(const GLhalfNV * @weight);
        internal static glVertexWeighthvNVFunc glVertexWeighthvNVPtr;
        internal static void loadVertexWeighthvNV()
        {
            try
            {
                glVertexWeighthvNVPtr = (glVertexWeighthvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexWeighthvNV"), typeof(glVertexWeighthvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexWeighthvNV'.");
            }
        }
        public static void glVertexWeighthvNV(const GLhalfNV * @weight) => glVertexWeighthvNVPtr(@weight);

        internal delegate void glVertexAttrib1hNVFunc(GLuint @index, GLhalfNV @x);
        internal static glVertexAttrib1hNVFunc glVertexAttrib1hNVPtr;
        internal static void loadVertexAttrib1hNV()
        {
            try
            {
                glVertexAttrib1hNVPtr = (glVertexAttrib1hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1hNV"), typeof(glVertexAttrib1hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1hNV'.");
            }
        }
        public static void glVertexAttrib1hNV(GLuint @index, GLhalfNV @x) => glVertexAttrib1hNVPtr(@index, @x);

        internal delegate void glVertexAttrib1hvNVFunc(GLuint @index, const GLhalfNV * @v);
        internal static glVertexAttrib1hvNVFunc glVertexAttrib1hvNVPtr;
        internal static void loadVertexAttrib1hvNV()
        {
            try
            {
                glVertexAttrib1hvNVPtr = (glVertexAttrib1hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1hvNV"), typeof(glVertexAttrib1hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1hvNV'.");
            }
        }
        public static void glVertexAttrib1hvNV(GLuint @index, const GLhalfNV * @v) => glVertexAttrib1hvNVPtr(@index, @v);

        internal delegate void glVertexAttrib2hNVFunc(GLuint @index, GLhalfNV @x, GLhalfNV @y);
        internal static glVertexAttrib2hNVFunc glVertexAttrib2hNVPtr;
        internal static void loadVertexAttrib2hNV()
        {
            try
            {
                glVertexAttrib2hNVPtr = (glVertexAttrib2hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2hNV"), typeof(glVertexAttrib2hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2hNV'.");
            }
        }
        public static void glVertexAttrib2hNV(GLuint @index, GLhalfNV @x, GLhalfNV @y) => glVertexAttrib2hNVPtr(@index, @x, @y);

        internal delegate void glVertexAttrib2hvNVFunc(GLuint @index, const GLhalfNV * @v);
        internal static glVertexAttrib2hvNVFunc glVertexAttrib2hvNVPtr;
        internal static void loadVertexAttrib2hvNV()
        {
            try
            {
                glVertexAttrib2hvNVPtr = (glVertexAttrib2hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2hvNV"), typeof(glVertexAttrib2hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2hvNV'.");
            }
        }
        public static void glVertexAttrib2hvNV(GLuint @index, const GLhalfNV * @v) => glVertexAttrib2hvNVPtr(@index, @v);

        internal delegate void glVertexAttrib3hNVFunc(GLuint @index, GLhalfNV @x, GLhalfNV @y, GLhalfNV @z);
        internal static glVertexAttrib3hNVFunc glVertexAttrib3hNVPtr;
        internal static void loadVertexAttrib3hNV()
        {
            try
            {
                glVertexAttrib3hNVPtr = (glVertexAttrib3hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3hNV"), typeof(glVertexAttrib3hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3hNV'.");
            }
        }
        public static void glVertexAttrib3hNV(GLuint @index, GLhalfNV @x, GLhalfNV @y, GLhalfNV @z) => glVertexAttrib3hNVPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttrib3hvNVFunc(GLuint @index, const GLhalfNV * @v);
        internal static glVertexAttrib3hvNVFunc glVertexAttrib3hvNVPtr;
        internal static void loadVertexAttrib3hvNV()
        {
            try
            {
                glVertexAttrib3hvNVPtr = (glVertexAttrib3hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3hvNV"), typeof(glVertexAttrib3hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3hvNV'.");
            }
        }
        public static void glVertexAttrib3hvNV(GLuint @index, const GLhalfNV * @v) => glVertexAttrib3hvNVPtr(@index, @v);

        internal delegate void glVertexAttrib4hNVFunc(GLuint @index, GLhalfNV @x, GLhalfNV @y, GLhalfNV @z, GLhalfNV @w);
        internal static glVertexAttrib4hNVFunc glVertexAttrib4hNVPtr;
        internal static void loadVertexAttrib4hNV()
        {
            try
            {
                glVertexAttrib4hNVPtr = (glVertexAttrib4hNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4hNV"), typeof(glVertexAttrib4hNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4hNV'.");
            }
        }
        public static void glVertexAttrib4hNV(GLuint @index, GLhalfNV @x, GLhalfNV @y, GLhalfNV @z, GLhalfNV @w) => glVertexAttrib4hNVPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4hvNVFunc(GLuint @index, const GLhalfNV * @v);
        internal static glVertexAttrib4hvNVFunc glVertexAttrib4hvNVPtr;
        internal static void loadVertexAttrib4hvNV()
        {
            try
            {
                glVertexAttrib4hvNVPtr = (glVertexAttrib4hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4hvNV"), typeof(glVertexAttrib4hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4hvNV'.");
            }
        }
        public static void glVertexAttrib4hvNV(GLuint @index, const GLhalfNV * @v) => glVertexAttrib4hvNVPtr(@index, @v);

        internal delegate void glVertexAttribs1hvNVFunc(GLuint @index, GLsizei @n, const GLhalfNV * @v);
        internal static glVertexAttribs1hvNVFunc glVertexAttribs1hvNVPtr;
        internal static void loadVertexAttribs1hvNV()
        {
            try
            {
                glVertexAttribs1hvNVPtr = (glVertexAttribs1hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs1hvNV"), typeof(glVertexAttribs1hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs1hvNV'.");
            }
        }
        public static void glVertexAttribs1hvNV(GLuint @index, GLsizei @n, const GLhalfNV * @v) => glVertexAttribs1hvNVPtr(@index, @n, @v);

        internal delegate void glVertexAttribs2hvNVFunc(GLuint @index, GLsizei @n, const GLhalfNV * @v);
        internal static glVertexAttribs2hvNVFunc glVertexAttribs2hvNVPtr;
        internal static void loadVertexAttribs2hvNV()
        {
            try
            {
                glVertexAttribs2hvNVPtr = (glVertexAttribs2hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs2hvNV"), typeof(glVertexAttribs2hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs2hvNV'.");
            }
        }
        public static void glVertexAttribs2hvNV(GLuint @index, GLsizei @n, const GLhalfNV * @v) => glVertexAttribs2hvNVPtr(@index, @n, @v);

        internal delegate void glVertexAttribs3hvNVFunc(GLuint @index, GLsizei @n, const GLhalfNV * @v);
        internal static glVertexAttribs3hvNVFunc glVertexAttribs3hvNVPtr;
        internal static void loadVertexAttribs3hvNV()
        {
            try
            {
                glVertexAttribs3hvNVPtr = (glVertexAttribs3hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs3hvNV"), typeof(glVertexAttribs3hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs3hvNV'.");
            }
        }
        public static void glVertexAttribs3hvNV(GLuint @index, GLsizei @n, const GLhalfNV * @v) => glVertexAttribs3hvNVPtr(@index, @n, @v);

        internal delegate void glVertexAttribs4hvNVFunc(GLuint @index, GLsizei @n, const GLhalfNV * @v);
        internal static glVertexAttribs4hvNVFunc glVertexAttribs4hvNVPtr;
        internal static void loadVertexAttribs4hvNV()
        {
            try
            {
                glVertexAttribs4hvNVPtr = (glVertexAttribs4hvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs4hvNV"), typeof(glVertexAttribs4hvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs4hvNV'.");
            }
        }
        public static void glVertexAttribs4hvNV(GLuint @index, GLsizei @n, const GLhalfNV * @v) => glVertexAttribs4hvNVPtr(@index, @n, @v);
        #endregion
    }
}
