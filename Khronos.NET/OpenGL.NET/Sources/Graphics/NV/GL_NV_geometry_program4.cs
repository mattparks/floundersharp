using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_geometry_program4
    {
        #region Interop
        static GL_NV_geometry_program4()
        {
            Console.WriteLine("Initalising GL_NV_geometry_program4 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadProgramVertexLimitNV();
            loadFramebufferTextureEXT();
            loadFramebufferTextureLayerEXT();
            loadFramebufferTextureFaceEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_LINES_ADJACENCY_EXT = 0x000A;
        public static UInt32 GL_LINE_STRIP_ADJACENCY_EXT = 0x000B;
        public static UInt32 GL_TRIANGLES_ADJACENCY_EXT = 0x000C;
        public static UInt32 GL_TRIANGLE_STRIP_ADJACENCY_EXT = 0x000D;
        public static UInt32 GL_GEOMETRY_PROGRAM_NV = 0x8C26;
        public static UInt32 GL_MAX_PROGRAM_OUTPUT_VERTICES_NV = 0x8C27;
        public static UInt32 GL_MAX_PROGRAM_TOTAL_OUTPUT_COMPONENTS_NV = 0x8C28;
        public static UInt32 GL_GEOMETRY_VERTICES_OUT_EXT = 0x8DDA;
        public static UInt32 GL_GEOMETRY_INPUT_TYPE_EXT = 0x8DDB;
        public static UInt32 GL_GEOMETRY_OUTPUT_TYPE_EXT = 0x8DDC;
        public static UInt32 GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS_EXT = 0x8C29;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_LAYERED_EXT = 0x8DA7;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS_EXT = 0x8DA8;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_LAYER_COUNT_EXT = 0x8DA9;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER_EXT = 0x8CD4;
        public static UInt32 GL_PROGRAM_POINT_SIZE_EXT = 0x8642;
        #endregion

        #region Commands
        internal delegate void glProgramVertexLimitNVFunc(GLenum @target, GLint @limit);
        internal static glProgramVertexLimitNVFunc glProgramVertexLimitNVPtr;
        internal static void loadProgramVertexLimitNV()
        {
            try
            {
                glProgramVertexLimitNVPtr = (glProgramVertexLimitNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramVertexLimitNV"), typeof(glProgramVertexLimitNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramVertexLimitNV'.");
            }
        }
        public static void glProgramVertexLimitNV(GLenum @target, GLint @limit) => glProgramVertexLimitNVPtr(@target, @limit);

        internal delegate void glFramebufferTextureEXTFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level);
        internal static glFramebufferTextureEXTFunc glFramebufferTextureEXTPtr;
        internal static void loadFramebufferTextureEXT()
        {
            try
            {
                glFramebufferTextureEXTPtr = (glFramebufferTextureEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureEXT"), typeof(glFramebufferTextureEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureEXT'.");
            }
        }
        public static void glFramebufferTextureEXT(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level) => glFramebufferTextureEXTPtr(@target, @attachment, @texture, @level);

        internal delegate void glFramebufferTextureLayerEXTFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer);
        internal static glFramebufferTextureLayerEXTFunc glFramebufferTextureLayerEXTPtr;
        internal static void loadFramebufferTextureLayerEXT()
        {
            try
            {
                glFramebufferTextureLayerEXTPtr = (glFramebufferTextureLayerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureLayerEXT"), typeof(glFramebufferTextureLayerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureLayerEXT'.");
            }
        }
        public static void glFramebufferTextureLayerEXT(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer) => glFramebufferTextureLayerEXTPtr(@target, @attachment, @texture, @level, @layer);

        internal delegate void glFramebufferTextureFaceEXTFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLenum @face);
        internal static glFramebufferTextureFaceEXTFunc glFramebufferTextureFaceEXTPtr;
        internal static void loadFramebufferTextureFaceEXT()
        {
            try
            {
                glFramebufferTextureFaceEXTPtr = (glFramebufferTextureFaceEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureFaceEXT"), typeof(glFramebufferTextureFaceEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureFaceEXT'.");
            }
        }
        public static void glFramebufferTextureFaceEXT(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLenum @face) => glFramebufferTextureFaceEXTPtr(@target, @attachment, @texture, @level, @face);
        #endregion
    }
}
