using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_path_rendering
    {
        #region Interop
        static GL_NV_path_rendering()
        {
            Console.WriteLine("Initalising GL_NV_path_rendering interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGenPathsNV();
            loadDeletePathsNV();
            loadIsPathNV();
            loadPathCommandsNV();
            loadPathCoordsNV();
            loadPathSubCommandsNV();
            loadPathSubCoordsNV();
            loadPathStringNV();
            loadPathGlyphsNV();
            loadPathGlyphRangeNV();
            loadWeightPathsNV();
            loadCopyPathNV();
            loadInterpolatePathsNV();
            loadTransformPathNV();
            loadPathParameterivNV();
            loadPathParameteriNV();
            loadPathParameterfvNV();
            loadPathParameterfNV();
            loadPathDashArrayNV();
            loadPathStencilFuncNV();
            loadPathStencilDepthOffsetNV();
            loadStencilFillPathNV();
            loadStencilStrokePathNV();
            loadStencilFillPathInstancedNV();
            loadStencilStrokePathInstancedNV();
            loadPathCoverDepthFuncNV();
            loadCoverFillPathNV();
            loadCoverStrokePathNV();
            loadCoverFillPathInstancedNV();
            loadCoverStrokePathInstancedNV();
            loadGetPathParameterivNV();
            loadGetPathParameterfvNV();
            loadGetPathCommandsNV();
            loadGetPathCoordsNV();
            loadGetPathDashArrayNV();
            loadGetPathMetricsNV();
            loadGetPathMetricRangeNV();
            loadGetPathSpacingNV();
            loadIsPointInFillPathNV();
            loadIsPointInStrokePathNV();
            loadGetPathLengthNV();
            loadPointAlongPathNV();
            loadMatrixLoad3x2fNV();
            loadMatrixLoad3x3fNV();
            loadMatrixLoadTranspose3x3fNV();
            loadMatrixMult3x2fNV();
            loadMatrixMult3x3fNV();
            loadMatrixMultTranspose3x3fNV();
            loadStencilThenCoverFillPathNV();
            loadStencilThenCoverStrokePathNV();
            loadStencilThenCoverFillPathInstancedNV();
            loadStencilThenCoverStrokePathInstancedNV();
            loadPathGlyphIndexRangeNV();
            loadPathGlyphIndexArrayNV();
            loadPathMemoryGlyphIndexArrayNV();
            loadProgramPathFragmentInputGenNV();
            loadGetProgramResourcefvNV();
            loadPathColorGenNV();
            loadPathTexGenNV();
            loadPathFogGenNV();
            loadGetPathColorGenivNV();
            loadGetPathColorGenfvNV();
            loadGetPathTexGenivNV();
            loadGetPathTexGenfvNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PATH_FORMAT_SVG_NV = 0x9070;
        public static UInt32 GL_PATH_FORMAT_PS_NV = 0x9071;
        public static UInt32 GL_STANDARD_FONT_NAME_NV = 0x9072;
        public static UInt32 GL_SYSTEM_FONT_NAME_NV = 0x9073;
        public static UInt32 GL_FILE_NAME_NV = 0x9074;
        public static UInt32 GL_PATH_STROKE_WIDTH_NV = 0x9075;
        public static UInt32 GL_PATH_END_CAPS_NV = 0x9076;
        public static UInt32 GL_PATH_INITIAL_END_CAP_NV = 0x9077;
        public static UInt32 GL_PATH_TERMINAL_END_CAP_NV = 0x9078;
        public static UInt32 GL_PATH_JOIN_STYLE_NV = 0x9079;
        public static UInt32 GL_PATH_MITER_LIMIT_NV = 0x907A;
        public static UInt32 GL_PATH_DASH_CAPS_NV = 0x907B;
        public static UInt32 GL_PATH_INITIAL_DASH_CAP_NV = 0x907C;
        public static UInt32 GL_PATH_TERMINAL_DASH_CAP_NV = 0x907D;
        public static UInt32 GL_PATH_DASH_OFFSET_NV = 0x907E;
        public static UInt32 GL_PATH_CLIENT_LENGTH_NV = 0x907F;
        public static UInt32 GL_PATH_FILL_MODE_NV = 0x9080;
        public static UInt32 GL_PATH_FILL_MASK_NV = 0x9081;
        public static UInt32 GL_PATH_FILL_COVER_MODE_NV = 0x9082;
        public static UInt32 GL_PATH_STROKE_COVER_MODE_NV = 0x9083;
        public static UInt32 GL_PATH_STROKE_MASK_NV = 0x9084;
        public static UInt32 GL_COUNT_UP_NV = 0x9088;
        public static UInt32 GL_COUNT_DOWN_NV = 0x9089;
        public static UInt32 GL_PATH_OBJECT_BOUNDING_BOX_NV = 0x908A;
        public static UInt32 GL_CONVEX_HULL_NV = 0x908B;
        public static UInt32 GL_BOUNDING_BOX_NV = 0x908D;
        public static UInt32 GL_TRANSLATE_X_NV = 0x908E;
        public static UInt32 GL_TRANSLATE_Y_NV = 0x908F;
        public static UInt32 GL_TRANSLATE_2D_NV = 0x9090;
        public static UInt32 GL_TRANSLATE_3D_NV = 0x9091;
        public static UInt32 GL_AFFINE_2D_NV = 0x9092;
        public static UInt32 GL_AFFINE_3D_NV = 0x9094;
        public static UInt32 GL_TRANSPOSE_AFFINE_2D_NV = 0x9096;
        public static UInt32 GL_TRANSPOSE_AFFINE_3D_NV = 0x9098;
        public static UInt32 GL_UTF8_NV = 0x909A;
        public static UInt32 GL_UTF16_NV = 0x909B;
        public static UInt32 GL_BOUNDING_BOX_OF_BOUNDING_BOXES_NV = 0x909C;
        public static UInt32 GL_PATH_COMMAND_COUNT_NV = 0x909D;
        public static UInt32 GL_PATH_COORD_COUNT_NV = 0x909E;
        public static UInt32 GL_PATH_DASH_ARRAY_COUNT_NV = 0x909F;
        public static UInt32 GL_PATH_COMPUTED_LENGTH_NV = 0x90A0;
        public static UInt32 GL_PATH_FILL_BOUNDING_BOX_NV = 0x90A1;
        public static UInt32 GL_PATH_STROKE_BOUNDING_BOX_NV = 0x90A2;
        public static UInt32 GL_SQUARE_NV = 0x90A3;
        public static UInt32 GL_ROUND_NV = 0x90A4;
        public static UInt32 GL_TRIANGULAR_NV = 0x90A5;
        public static UInt32 GL_BEVEL_NV = 0x90A6;
        public static UInt32 GL_MITER_REVERT_NV = 0x90A7;
        public static UInt32 GL_MITER_TRUNCATE_NV = 0x90A8;
        public static UInt32 GL_SKIP_MISSING_GLYPH_NV = 0x90A9;
        public static UInt32 GL_USE_MISSING_GLYPH_NV = 0x90AA;
        public static UInt32 GL_PATH_ERROR_POSITION_NV = 0x90AB;
        public static UInt32 GL_ACCUM_ADJACENT_PAIRS_NV = 0x90AD;
        public static UInt32 GL_ADJACENT_PAIRS_NV = 0x90AE;
        public static UInt32 GL_FIRST_TO_REST_NV = 0x90AF;
        public static UInt32 GL_PATH_GEN_MODE_NV = 0x90B0;
        public static UInt32 GL_PATH_GEN_COEFF_NV = 0x90B1;
        public static UInt32 GL_PATH_GEN_COMPONENTS_NV = 0x90B3;
        public static UInt32 GL_PATH_STENCIL_FUNC_NV = 0x90B7;
        public static UInt32 GL_PATH_STENCIL_REF_NV = 0x90B8;
        public static UInt32 GL_PATH_STENCIL_VALUE_MASK_NV = 0x90B9;
        public static UInt32 GL_PATH_STENCIL_DEPTH_OFFSET_FACTOR_NV = 0x90BD;
        public static UInt32 GL_PATH_STENCIL_DEPTH_OFFSET_UNITS_NV = 0x90BE;
        public static UInt32 GL_PATH_COVER_DEPTH_FUNC_NV = 0x90BF;
        public static UInt32 GL_PATH_DASH_OFFSET_RESET_NV = 0x90B4;
        public static UInt32 GL_MOVE_TO_RESETS_NV = 0x90B5;
        public static UInt32 GL_MOVE_TO_CONTINUES_NV = 0x90B6;
        public static UInt32 GL_CLOSE_PATH_NV = 0x00;
        public static UInt32 GL_MOVE_TO_NV = 0x02;
        public static UInt32 GL_RELATIVE_MOVE_TO_NV = 0x03;
        public static UInt32 GL_LINE_TO_NV = 0x04;
        public static UInt32 GL_RELATIVE_LINE_TO_NV = 0x05;
        public static UInt32 GL_HORIZONTAL_LINE_TO_NV = 0x06;
        public static UInt32 GL_RELATIVE_HORIZONTAL_LINE_TO_NV = 0x07;
        public static UInt32 GL_VERTICAL_LINE_TO_NV = 0x08;
        public static UInt32 GL_RELATIVE_VERTICAL_LINE_TO_NV = 0x09;
        public static UInt32 GL_QUADRATIC_CURVE_TO_NV = 0x0A;
        public static UInt32 GL_RELATIVE_QUADRATIC_CURVE_TO_NV = 0x0B;
        public static UInt32 GL_CUBIC_CURVE_TO_NV = 0x0C;
        public static UInt32 GL_RELATIVE_CUBIC_CURVE_TO_NV = 0x0D;
        public static UInt32 GL_SMOOTH_QUADRATIC_CURVE_TO_NV = 0x0E;
        public static UInt32 GL_RELATIVE_SMOOTH_QUADRATIC_CURVE_TO_NV = 0x0F;
        public static UInt32 GL_SMOOTH_CUBIC_CURVE_TO_NV = 0x10;
        public static UInt32 GL_RELATIVE_SMOOTH_CUBIC_CURVE_TO_NV = 0x11;
        public static UInt32 GL_SMALL_CCW_ARC_TO_NV = 0x12;
        public static UInt32 GL_RELATIVE_SMALL_CCW_ARC_TO_NV = 0x13;
        public static UInt32 GL_SMALL_CW_ARC_TO_NV = 0x14;
        public static UInt32 GL_RELATIVE_SMALL_CW_ARC_TO_NV = 0x15;
        public static UInt32 GL_LARGE_CCW_ARC_TO_NV = 0x16;
        public static UInt32 GL_RELATIVE_LARGE_CCW_ARC_TO_NV = 0x17;
        public static UInt32 GL_LARGE_CW_ARC_TO_NV = 0x18;
        public static UInt32 GL_RELATIVE_LARGE_CW_ARC_TO_NV = 0x19;
        public static UInt32 GL_RESTART_PATH_NV = 0xF0;
        public static UInt32 GL_DUP_FIRST_CUBIC_CURVE_TO_NV = 0xF2;
        public static UInt32 GL_DUP_LAST_CUBIC_CURVE_TO_NV = 0xF4;
        public static UInt32 GL_RECT_NV = 0xF6;
        public static UInt32 GL_CIRCULAR_CCW_ARC_TO_NV = 0xF8;
        public static UInt32 GL_CIRCULAR_CW_ARC_TO_NV = 0xFA;
        public static UInt32 GL_CIRCULAR_TANGENT_ARC_TO_NV = 0xFC;
        public static UInt32 GL_ARC_TO_NV = 0xFE;
        public static UInt32 GL_RELATIVE_ARC_TO_NV = 0xFF;
        public static UInt32 GL_BOLD_BIT_NV = 0x01;
        public static UInt32 GL_ITALIC_BIT_NV = 0x02;
        public static UInt32 GL_GLYPH_WIDTH_BIT_NV = 0x01;
        public static UInt32 GL_GLYPH_HEIGHT_BIT_NV = 0x02;
        public static UInt32 GL_GLYPH_HORIZONTAL_BEARING_X_BIT_NV = 0x04;
        public static UInt32 GL_GLYPH_HORIZONTAL_BEARING_Y_BIT_NV = 0x08;
        public static UInt32 GL_GLYPH_HORIZONTAL_BEARING_ADVANCE_BIT_NV = 0x10;
        public static UInt32 GL_GLYPH_VERTICAL_BEARING_X_BIT_NV = 0x20;
        public static UInt32 GL_GLYPH_VERTICAL_BEARING_Y_BIT_NV = 0x40;
        public static UInt32 GL_GLYPH_VERTICAL_BEARING_ADVANCE_BIT_NV = 0x80;
        public static UInt32 GL_GLYPH_HAS_KERNING_BIT_NV = 0x100;
        public static UInt32 GL_FONT_X_MIN_BOUNDS_BIT_NV = 0x00010000;
        public static UInt32 GL_FONT_Y_MIN_BOUNDS_BIT_NV = 0x00020000;
        public static UInt32 GL_FONT_X_MAX_BOUNDS_BIT_NV = 0x00040000;
        public static UInt32 GL_FONT_Y_MAX_BOUNDS_BIT_NV = 0x00080000;
        public static UInt32 GL_FONT_UNITS_PER_EM_BIT_NV = 0x00100000;
        public static UInt32 GL_FONT_ASCENDER_BIT_NV = 0x00200000;
        public static UInt32 GL_FONT_DESCENDER_BIT_NV = 0x00400000;
        public static UInt32 GL_FONT_HEIGHT_BIT_NV = 0x00800000;
        public static UInt32 GL_FONT_MAX_ADVANCE_WIDTH_BIT_NV = 0x01000000;
        public static UInt32 GL_FONT_MAX_ADVANCE_HEIGHT_BIT_NV = 0x02000000;
        public static UInt32 GL_FONT_UNDERLINE_POSITION_BIT_NV = 0x04000000;
        public static UInt32 GL_FONT_UNDERLINE_THICKNESS_BIT_NV = 0x08000000;
        public static UInt32 GL_FONT_HAS_KERNING_BIT_NV = 0x10000000;
        public static UInt32 GL_ROUNDED_RECT_NV = 0xE8;
        public static UInt32 GL_RELATIVE_ROUNDED_RECT_NV = 0xE9;
        public static UInt32 GL_ROUNDED_RECT2_NV = 0xEA;
        public static UInt32 GL_RELATIVE_ROUNDED_RECT2_NV = 0xEB;
        public static UInt32 GL_ROUNDED_RECT4_NV = 0xEC;
        public static UInt32 GL_RELATIVE_ROUNDED_RECT4_NV = 0xED;
        public static UInt32 GL_ROUNDED_RECT8_NV = 0xEE;
        public static UInt32 GL_RELATIVE_ROUNDED_RECT8_NV = 0xEF;
        public static UInt32 GL_RELATIVE_RECT_NV = 0xF7;
        public static UInt32 GL_FONT_GLYPHS_AVAILABLE_NV = 0x9368;
        public static UInt32 GL_FONT_TARGET_UNAVAILABLE_NV = 0x9369;
        public static UInt32 GL_FONT_UNAVAILABLE_NV = 0x936A;
        public static UInt32 GL_FONT_UNINTELLIGIBLE_NV = 0x936B;
        public static UInt32 GL_CONIC_CURVE_TO_NV = 0x1A;
        public static UInt32 GL_RELATIVE_CONIC_CURVE_TO_NV = 0x1B;
        public static UInt32 GL_FONT_NUM_GLYPH_INDICES_BIT_NV = 0x20000000;
        public static UInt32 GL_STANDARD_FONT_FORMAT_NV = 0x936C;
        public static UInt32 GL_2_BYTES_NV = 0x1407;
        public static UInt32 GL_3_BYTES_NV = 0x1408;
        public static UInt32 GL_4_BYTES_NV = 0x1409;
        public static UInt32 GL_EYE_LINEAR_NV = 0x2400;
        public static UInt32 GL_OBJECT_LINEAR_NV = 0x2401;
        public static UInt32 GL_CONSTANT_NV = 0x8576;
        public static UInt32 GL_PATH_FOG_GEN_MODE_NV = 0x90AC;
        public static UInt32 GL_PRIMARY_COLOR = 0x8577;
        public static UInt32 GL_PRIMARY_COLOR_NV = 0x852C;
        public static UInt32 GL_SECONDARY_COLOR_NV = 0x852D;
        public static UInt32 GL_PATH_GEN_COLOR_FORMAT_NV = 0x90B2;
        public static UInt32 GL_PATH_PROJECTION_NV = 0x1701;
        public static UInt32 GL_PATH_MODELVIEW_NV = 0x1700;
        public static UInt32 GL_PATH_MODELVIEW_STACK_DEPTH_NV = 0x0BA3;
        public static UInt32 GL_PATH_MODELVIEW_MATRIX_NV = 0x0BA6;
        public static UInt32 GL_PATH_MAX_MODELVIEW_STACK_DEPTH_NV = 0x0D36;
        public static UInt32 GL_PATH_TRANSPOSE_MODELVIEW_MATRIX_NV = 0x84E3;
        public static UInt32 GL_PATH_PROJECTION_STACK_DEPTH_NV = 0x0BA4;
        public static UInt32 GL_PATH_PROJECTION_MATRIX_NV = 0x0BA7;
        public static UInt32 GL_PATH_MAX_PROJECTION_STACK_DEPTH_NV = 0x0D38;
        public static UInt32 GL_PATH_TRANSPOSE_PROJECTION_MATRIX_NV = 0x84E4;
        public static UInt32 GL_FRAGMENT_INPUT_NV = 0x936D;
        #endregion

        #region Commands
        internal delegate GLuint glGenPathsNVFunc(GLsizei @range);
        internal static glGenPathsNVFunc glGenPathsNVPtr;
        internal static void loadGenPathsNV()
        {
            try
            {
                glGenPathsNVPtr = (glGenPathsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenPathsNV"), typeof(glGenPathsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenPathsNV'.");
            }
        }
        public static GLuint glGenPathsNV(GLsizei @range) => glGenPathsNVPtr(@range);

        internal delegate void glDeletePathsNVFunc(GLuint @path, GLsizei @range);
        internal static glDeletePathsNVFunc glDeletePathsNVPtr;
        internal static void loadDeletePathsNV()
        {
            try
            {
                glDeletePathsNVPtr = (glDeletePathsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeletePathsNV"), typeof(glDeletePathsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeletePathsNV'.");
            }
        }
        public static void glDeletePathsNV(GLuint @path, GLsizei @range) => glDeletePathsNVPtr(@path, @range);

        internal delegate GLboolean glIsPathNVFunc(GLuint @path);
        internal static glIsPathNVFunc glIsPathNVPtr;
        internal static void loadIsPathNV()
        {
            try
            {
                glIsPathNVPtr = (glIsPathNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsPathNV"), typeof(glIsPathNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsPathNV'.");
            }
        }
        public static GLboolean glIsPathNV(GLuint @path) => glIsPathNVPtr(@path);

        internal delegate void glPathCommandsNVFunc(GLuint @path, GLsizei @numCommands, const GLubyte * @commands, GLsizei @numCoords, GLenum @coordType, const void * @coords);
        internal static glPathCommandsNVFunc glPathCommandsNVPtr;
        internal static void loadPathCommandsNV()
        {
            try
            {
                glPathCommandsNVPtr = (glPathCommandsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathCommandsNV"), typeof(glPathCommandsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathCommandsNV'.");
            }
        }
        public static void glPathCommandsNV(GLuint @path, GLsizei @numCommands, const GLubyte * @commands, GLsizei @numCoords, GLenum @coordType, const void * @coords) => glPathCommandsNVPtr(@path, @numCommands, @commands, @numCoords, @coordType, @coords);

        internal delegate void glPathCoordsNVFunc(GLuint @path, GLsizei @numCoords, GLenum @coordType, const void * @coords);
        internal static glPathCoordsNVFunc glPathCoordsNVPtr;
        internal static void loadPathCoordsNV()
        {
            try
            {
                glPathCoordsNVPtr = (glPathCoordsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathCoordsNV"), typeof(glPathCoordsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathCoordsNV'.");
            }
        }
        public static void glPathCoordsNV(GLuint @path, GLsizei @numCoords, GLenum @coordType, const void * @coords) => glPathCoordsNVPtr(@path, @numCoords, @coordType, @coords);

        internal delegate void glPathSubCommandsNVFunc(GLuint @path, GLsizei @commandStart, GLsizei @commandsToDelete, GLsizei @numCommands, const GLubyte * @commands, GLsizei @numCoords, GLenum @coordType, const void * @coords);
        internal static glPathSubCommandsNVFunc glPathSubCommandsNVPtr;
        internal static void loadPathSubCommandsNV()
        {
            try
            {
                glPathSubCommandsNVPtr = (glPathSubCommandsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathSubCommandsNV"), typeof(glPathSubCommandsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathSubCommandsNV'.");
            }
        }
        public static void glPathSubCommandsNV(GLuint @path, GLsizei @commandStart, GLsizei @commandsToDelete, GLsizei @numCommands, const GLubyte * @commands, GLsizei @numCoords, GLenum @coordType, const void * @coords) => glPathSubCommandsNVPtr(@path, @commandStart, @commandsToDelete, @numCommands, @commands, @numCoords, @coordType, @coords);

        internal delegate void glPathSubCoordsNVFunc(GLuint @path, GLsizei @coordStart, GLsizei @numCoords, GLenum @coordType, const void * @coords);
        internal static glPathSubCoordsNVFunc glPathSubCoordsNVPtr;
        internal static void loadPathSubCoordsNV()
        {
            try
            {
                glPathSubCoordsNVPtr = (glPathSubCoordsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathSubCoordsNV"), typeof(glPathSubCoordsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathSubCoordsNV'.");
            }
        }
        public static void glPathSubCoordsNV(GLuint @path, GLsizei @coordStart, GLsizei @numCoords, GLenum @coordType, const void * @coords) => glPathSubCoordsNVPtr(@path, @coordStart, @numCoords, @coordType, @coords);

        internal delegate void glPathStringNVFunc(GLuint @path, GLenum @format, GLsizei @length, const void * @pathString);
        internal static glPathStringNVFunc glPathStringNVPtr;
        internal static void loadPathStringNV()
        {
            try
            {
                glPathStringNVPtr = (glPathStringNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathStringNV"), typeof(glPathStringNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathStringNV'.");
            }
        }
        public static void glPathStringNV(GLuint @path, GLenum @format, GLsizei @length, const void * @pathString) => glPathStringNVPtr(@path, @format, @length, @pathString);

        internal delegate void glPathGlyphsNVFunc(GLuint @firstPathName, GLenum @fontTarget, const void * @fontName, GLbitfield @fontStyle, GLsizei @numGlyphs, GLenum @type, const void * @charcodes, GLenum @handleMissingGlyphs, GLuint @pathParameterTemplate, GLfloat @emScale);
        internal static glPathGlyphsNVFunc glPathGlyphsNVPtr;
        internal static void loadPathGlyphsNV()
        {
            try
            {
                glPathGlyphsNVPtr = (glPathGlyphsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathGlyphsNV"), typeof(glPathGlyphsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathGlyphsNV'.");
            }
        }
        public static void glPathGlyphsNV(GLuint @firstPathName, GLenum @fontTarget, const void * @fontName, GLbitfield @fontStyle, GLsizei @numGlyphs, GLenum @type, const void * @charcodes, GLenum @handleMissingGlyphs, GLuint @pathParameterTemplate, GLfloat @emScale) => glPathGlyphsNVPtr(@firstPathName, @fontTarget, @fontName, @fontStyle, @numGlyphs, @type, @charcodes, @handleMissingGlyphs, @pathParameterTemplate, @emScale);

        internal delegate void glPathGlyphRangeNVFunc(GLuint @firstPathName, GLenum @fontTarget, const void * @fontName, GLbitfield @fontStyle, GLuint @firstGlyph, GLsizei @numGlyphs, GLenum @handleMissingGlyphs, GLuint @pathParameterTemplate, GLfloat @emScale);
        internal static glPathGlyphRangeNVFunc glPathGlyphRangeNVPtr;
        internal static void loadPathGlyphRangeNV()
        {
            try
            {
                glPathGlyphRangeNVPtr = (glPathGlyphRangeNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathGlyphRangeNV"), typeof(glPathGlyphRangeNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathGlyphRangeNV'.");
            }
        }
        public static void glPathGlyphRangeNV(GLuint @firstPathName, GLenum @fontTarget, const void * @fontName, GLbitfield @fontStyle, GLuint @firstGlyph, GLsizei @numGlyphs, GLenum @handleMissingGlyphs, GLuint @pathParameterTemplate, GLfloat @emScale) => glPathGlyphRangeNVPtr(@firstPathName, @fontTarget, @fontName, @fontStyle, @firstGlyph, @numGlyphs, @handleMissingGlyphs, @pathParameterTemplate, @emScale);

        internal delegate void glWeightPathsNVFunc(GLuint @resultPath, GLsizei @numPaths, const GLuint * @paths, const GLfloat * @weights);
        internal static glWeightPathsNVFunc glWeightPathsNVPtr;
        internal static void loadWeightPathsNV()
        {
            try
            {
                glWeightPathsNVPtr = (glWeightPathsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWeightPathsNV"), typeof(glWeightPathsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWeightPathsNV'.");
            }
        }
        public static void glWeightPathsNV(GLuint @resultPath, GLsizei @numPaths, const GLuint * @paths, const GLfloat * @weights) => glWeightPathsNVPtr(@resultPath, @numPaths, @paths, @weights);

        internal delegate void glCopyPathNVFunc(GLuint @resultPath, GLuint @srcPath);
        internal static glCopyPathNVFunc glCopyPathNVPtr;
        internal static void loadCopyPathNV()
        {
            try
            {
                glCopyPathNVPtr = (glCopyPathNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyPathNV"), typeof(glCopyPathNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyPathNV'.");
            }
        }
        public static void glCopyPathNV(GLuint @resultPath, GLuint @srcPath) => glCopyPathNVPtr(@resultPath, @srcPath);

        internal delegate void glInterpolatePathsNVFunc(GLuint @resultPath, GLuint @pathA, GLuint @pathB, GLfloat @weight);
        internal static glInterpolatePathsNVFunc glInterpolatePathsNVPtr;
        internal static void loadInterpolatePathsNV()
        {
            try
            {
                glInterpolatePathsNVPtr = (glInterpolatePathsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInterpolatePathsNV"), typeof(glInterpolatePathsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInterpolatePathsNV'.");
            }
        }
        public static void glInterpolatePathsNV(GLuint @resultPath, GLuint @pathA, GLuint @pathB, GLfloat @weight) => glInterpolatePathsNVPtr(@resultPath, @pathA, @pathB, @weight);

        internal delegate void glTransformPathNVFunc(GLuint @resultPath, GLuint @srcPath, GLenum @transformType, const GLfloat * @transformValues);
        internal static glTransformPathNVFunc glTransformPathNVPtr;
        internal static void loadTransformPathNV()
        {
            try
            {
                glTransformPathNVPtr = (glTransformPathNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTransformPathNV"), typeof(glTransformPathNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTransformPathNV'.");
            }
        }
        public static void glTransformPathNV(GLuint @resultPath, GLuint @srcPath, GLenum @transformType, const GLfloat * @transformValues) => glTransformPathNVPtr(@resultPath, @srcPath, @transformType, @transformValues);

        internal delegate void glPathParameterivNVFunc(GLuint @path, GLenum @pname, const GLint * @value);
        internal static glPathParameterivNVFunc glPathParameterivNVPtr;
        internal static void loadPathParameterivNV()
        {
            try
            {
                glPathParameterivNVPtr = (glPathParameterivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathParameterivNV"), typeof(glPathParameterivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathParameterivNV'.");
            }
        }
        public static void glPathParameterivNV(GLuint @path, GLenum @pname, const GLint * @value) => glPathParameterivNVPtr(@path, @pname, @value);

        internal delegate void glPathParameteriNVFunc(GLuint @path, GLenum @pname, GLint @value);
        internal static glPathParameteriNVFunc glPathParameteriNVPtr;
        internal static void loadPathParameteriNV()
        {
            try
            {
                glPathParameteriNVPtr = (glPathParameteriNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathParameteriNV"), typeof(glPathParameteriNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathParameteriNV'.");
            }
        }
        public static void glPathParameteriNV(GLuint @path, GLenum @pname, GLint @value) => glPathParameteriNVPtr(@path, @pname, @value);

        internal delegate void glPathParameterfvNVFunc(GLuint @path, GLenum @pname, const GLfloat * @value);
        internal static glPathParameterfvNVFunc glPathParameterfvNVPtr;
        internal static void loadPathParameterfvNV()
        {
            try
            {
                glPathParameterfvNVPtr = (glPathParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathParameterfvNV"), typeof(glPathParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathParameterfvNV'.");
            }
        }
        public static void glPathParameterfvNV(GLuint @path, GLenum @pname, const GLfloat * @value) => glPathParameterfvNVPtr(@path, @pname, @value);

        internal delegate void glPathParameterfNVFunc(GLuint @path, GLenum @pname, GLfloat @value);
        internal static glPathParameterfNVFunc glPathParameterfNVPtr;
        internal static void loadPathParameterfNV()
        {
            try
            {
                glPathParameterfNVPtr = (glPathParameterfNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathParameterfNV"), typeof(glPathParameterfNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathParameterfNV'.");
            }
        }
        public static void glPathParameterfNV(GLuint @path, GLenum @pname, GLfloat @value) => glPathParameterfNVPtr(@path, @pname, @value);

        internal delegate void glPathDashArrayNVFunc(GLuint @path, GLsizei @dashCount, const GLfloat * @dashArray);
        internal static glPathDashArrayNVFunc glPathDashArrayNVPtr;
        internal static void loadPathDashArrayNV()
        {
            try
            {
                glPathDashArrayNVPtr = (glPathDashArrayNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathDashArrayNV"), typeof(glPathDashArrayNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathDashArrayNV'.");
            }
        }
        public static void glPathDashArrayNV(GLuint @path, GLsizei @dashCount, const GLfloat * @dashArray) => glPathDashArrayNVPtr(@path, @dashCount, @dashArray);

        internal delegate void glPathStencilFuncNVFunc(GLenum @func, GLint @ref, GLuint @mask);
        internal static glPathStencilFuncNVFunc glPathStencilFuncNVPtr;
        internal static void loadPathStencilFuncNV()
        {
            try
            {
                glPathStencilFuncNVPtr = (glPathStencilFuncNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathStencilFuncNV"), typeof(glPathStencilFuncNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathStencilFuncNV'.");
            }
        }
        public static void glPathStencilFuncNV(GLenum @func, GLint @ref, GLuint @mask) => glPathStencilFuncNVPtr(@func, @ref, @mask);

        internal delegate void glPathStencilDepthOffsetNVFunc(GLfloat @factor, GLfloat @units);
        internal static glPathStencilDepthOffsetNVFunc glPathStencilDepthOffsetNVPtr;
        internal static void loadPathStencilDepthOffsetNV()
        {
            try
            {
                glPathStencilDepthOffsetNVPtr = (glPathStencilDepthOffsetNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathStencilDepthOffsetNV"), typeof(glPathStencilDepthOffsetNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathStencilDepthOffsetNV'.");
            }
        }
        public static void glPathStencilDepthOffsetNV(GLfloat @factor, GLfloat @units) => glPathStencilDepthOffsetNVPtr(@factor, @units);

        internal delegate void glStencilFillPathNVFunc(GLuint @path, GLenum @fillMode, GLuint @mask);
        internal static glStencilFillPathNVFunc glStencilFillPathNVPtr;
        internal static void loadStencilFillPathNV()
        {
            try
            {
                glStencilFillPathNVPtr = (glStencilFillPathNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilFillPathNV"), typeof(glStencilFillPathNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilFillPathNV'.");
            }
        }
        public static void glStencilFillPathNV(GLuint @path, GLenum @fillMode, GLuint @mask) => glStencilFillPathNVPtr(@path, @fillMode, @mask);

        internal delegate void glStencilStrokePathNVFunc(GLuint @path, GLint @reference, GLuint @mask);
        internal static glStencilStrokePathNVFunc glStencilStrokePathNVPtr;
        internal static void loadStencilStrokePathNV()
        {
            try
            {
                glStencilStrokePathNVPtr = (glStencilStrokePathNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilStrokePathNV"), typeof(glStencilStrokePathNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilStrokePathNV'.");
            }
        }
        public static void glStencilStrokePathNV(GLuint @path, GLint @reference, GLuint @mask) => glStencilStrokePathNVPtr(@path, @reference, @mask);

        internal delegate void glStencilFillPathInstancedNVFunc(GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLenum @fillMode, GLuint @mask, GLenum @transformType, const GLfloat * @transformValues);
        internal static glStencilFillPathInstancedNVFunc glStencilFillPathInstancedNVPtr;
        internal static void loadStencilFillPathInstancedNV()
        {
            try
            {
                glStencilFillPathInstancedNVPtr = (glStencilFillPathInstancedNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilFillPathInstancedNV"), typeof(glStencilFillPathInstancedNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilFillPathInstancedNV'.");
            }
        }
        public static void glStencilFillPathInstancedNV(GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLenum @fillMode, GLuint @mask, GLenum @transformType, const GLfloat * @transformValues) => glStencilFillPathInstancedNVPtr(@numPaths, @pathNameType, @paths, @pathBase, @fillMode, @mask, @transformType, @transformValues);

        internal delegate void glStencilStrokePathInstancedNVFunc(GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLint @reference, GLuint @mask, GLenum @transformType, const GLfloat * @transformValues);
        internal static glStencilStrokePathInstancedNVFunc glStencilStrokePathInstancedNVPtr;
        internal static void loadStencilStrokePathInstancedNV()
        {
            try
            {
                glStencilStrokePathInstancedNVPtr = (glStencilStrokePathInstancedNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilStrokePathInstancedNV"), typeof(glStencilStrokePathInstancedNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilStrokePathInstancedNV'.");
            }
        }
        public static void glStencilStrokePathInstancedNV(GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLint @reference, GLuint @mask, GLenum @transformType, const GLfloat * @transformValues) => glStencilStrokePathInstancedNVPtr(@numPaths, @pathNameType, @paths, @pathBase, @reference, @mask, @transformType, @transformValues);

        internal delegate void glPathCoverDepthFuncNVFunc(GLenum @func);
        internal static glPathCoverDepthFuncNVFunc glPathCoverDepthFuncNVPtr;
        internal static void loadPathCoverDepthFuncNV()
        {
            try
            {
                glPathCoverDepthFuncNVPtr = (glPathCoverDepthFuncNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathCoverDepthFuncNV"), typeof(glPathCoverDepthFuncNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathCoverDepthFuncNV'.");
            }
        }
        public static void glPathCoverDepthFuncNV(GLenum @func) => glPathCoverDepthFuncNVPtr(@func);

        internal delegate void glCoverFillPathNVFunc(GLuint @path, GLenum @coverMode);
        internal static glCoverFillPathNVFunc glCoverFillPathNVPtr;
        internal static void loadCoverFillPathNV()
        {
            try
            {
                glCoverFillPathNVPtr = (glCoverFillPathNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCoverFillPathNV"), typeof(glCoverFillPathNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCoverFillPathNV'.");
            }
        }
        public static void glCoverFillPathNV(GLuint @path, GLenum @coverMode) => glCoverFillPathNVPtr(@path, @coverMode);

        internal delegate void glCoverStrokePathNVFunc(GLuint @path, GLenum @coverMode);
        internal static glCoverStrokePathNVFunc glCoverStrokePathNVPtr;
        internal static void loadCoverStrokePathNV()
        {
            try
            {
                glCoverStrokePathNVPtr = (glCoverStrokePathNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCoverStrokePathNV"), typeof(glCoverStrokePathNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCoverStrokePathNV'.");
            }
        }
        public static void glCoverStrokePathNV(GLuint @path, GLenum @coverMode) => glCoverStrokePathNVPtr(@path, @coverMode);

        internal delegate void glCoverFillPathInstancedNVFunc(GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLenum @coverMode, GLenum @transformType, const GLfloat * @transformValues);
        internal static glCoverFillPathInstancedNVFunc glCoverFillPathInstancedNVPtr;
        internal static void loadCoverFillPathInstancedNV()
        {
            try
            {
                glCoverFillPathInstancedNVPtr = (glCoverFillPathInstancedNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCoverFillPathInstancedNV"), typeof(glCoverFillPathInstancedNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCoverFillPathInstancedNV'.");
            }
        }
        public static void glCoverFillPathInstancedNV(GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLenum @coverMode, GLenum @transformType, const GLfloat * @transformValues) => glCoverFillPathInstancedNVPtr(@numPaths, @pathNameType, @paths, @pathBase, @coverMode, @transformType, @transformValues);

        internal delegate void glCoverStrokePathInstancedNVFunc(GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLenum @coverMode, GLenum @transformType, const GLfloat * @transformValues);
        internal static glCoverStrokePathInstancedNVFunc glCoverStrokePathInstancedNVPtr;
        internal static void loadCoverStrokePathInstancedNV()
        {
            try
            {
                glCoverStrokePathInstancedNVPtr = (glCoverStrokePathInstancedNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCoverStrokePathInstancedNV"), typeof(glCoverStrokePathInstancedNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCoverStrokePathInstancedNV'.");
            }
        }
        public static void glCoverStrokePathInstancedNV(GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLenum @coverMode, GLenum @transformType, const GLfloat * @transformValues) => glCoverStrokePathInstancedNVPtr(@numPaths, @pathNameType, @paths, @pathBase, @coverMode, @transformType, @transformValues);

        internal delegate void glGetPathParameterivNVFunc(GLuint @path, GLenum @pname, GLint * @value);
        internal static glGetPathParameterivNVFunc glGetPathParameterivNVPtr;
        internal static void loadGetPathParameterivNV()
        {
            try
            {
                glGetPathParameterivNVPtr = (glGetPathParameterivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathParameterivNV"), typeof(glGetPathParameterivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathParameterivNV'.");
            }
        }
        public static void glGetPathParameterivNV(GLuint @path, GLenum @pname, GLint * @value) => glGetPathParameterivNVPtr(@path, @pname, @value);

        internal delegate void glGetPathParameterfvNVFunc(GLuint @path, GLenum @pname, GLfloat * @value);
        internal static glGetPathParameterfvNVFunc glGetPathParameterfvNVPtr;
        internal static void loadGetPathParameterfvNV()
        {
            try
            {
                glGetPathParameterfvNVPtr = (glGetPathParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathParameterfvNV"), typeof(glGetPathParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathParameterfvNV'.");
            }
        }
        public static void glGetPathParameterfvNV(GLuint @path, GLenum @pname, GLfloat * @value) => glGetPathParameterfvNVPtr(@path, @pname, @value);

        internal delegate void glGetPathCommandsNVFunc(GLuint @path, GLubyte * @commands);
        internal static glGetPathCommandsNVFunc glGetPathCommandsNVPtr;
        internal static void loadGetPathCommandsNV()
        {
            try
            {
                glGetPathCommandsNVPtr = (glGetPathCommandsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathCommandsNV"), typeof(glGetPathCommandsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathCommandsNV'.");
            }
        }
        public static void glGetPathCommandsNV(GLuint @path, GLubyte * @commands) => glGetPathCommandsNVPtr(@path, @commands);

        internal delegate void glGetPathCoordsNVFunc(GLuint @path, GLfloat * @coords);
        internal static glGetPathCoordsNVFunc glGetPathCoordsNVPtr;
        internal static void loadGetPathCoordsNV()
        {
            try
            {
                glGetPathCoordsNVPtr = (glGetPathCoordsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathCoordsNV"), typeof(glGetPathCoordsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathCoordsNV'.");
            }
        }
        public static void glGetPathCoordsNV(GLuint @path, GLfloat * @coords) => glGetPathCoordsNVPtr(@path, @coords);

        internal delegate void glGetPathDashArrayNVFunc(GLuint @path, GLfloat * @dashArray);
        internal static glGetPathDashArrayNVFunc glGetPathDashArrayNVPtr;
        internal static void loadGetPathDashArrayNV()
        {
            try
            {
                glGetPathDashArrayNVPtr = (glGetPathDashArrayNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathDashArrayNV"), typeof(glGetPathDashArrayNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathDashArrayNV'.");
            }
        }
        public static void glGetPathDashArrayNV(GLuint @path, GLfloat * @dashArray) => glGetPathDashArrayNVPtr(@path, @dashArray);

        internal delegate void glGetPathMetricsNVFunc(GLbitfield @metricQueryMask, GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLsizei @stride, GLfloat * @metrics);
        internal static glGetPathMetricsNVFunc glGetPathMetricsNVPtr;
        internal static void loadGetPathMetricsNV()
        {
            try
            {
                glGetPathMetricsNVPtr = (glGetPathMetricsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathMetricsNV"), typeof(glGetPathMetricsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathMetricsNV'.");
            }
        }
        public static void glGetPathMetricsNV(GLbitfield @metricQueryMask, GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLsizei @stride, GLfloat * @metrics) => glGetPathMetricsNVPtr(@metricQueryMask, @numPaths, @pathNameType, @paths, @pathBase, @stride, @metrics);

        internal delegate void glGetPathMetricRangeNVFunc(GLbitfield @metricQueryMask, GLuint @firstPathName, GLsizei @numPaths, GLsizei @stride, GLfloat * @metrics);
        internal static glGetPathMetricRangeNVFunc glGetPathMetricRangeNVPtr;
        internal static void loadGetPathMetricRangeNV()
        {
            try
            {
                glGetPathMetricRangeNVPtr = (glGetPathMetricRangeNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathMetricRangeNV"), typeof(glGetPathMetricRangeNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathMetricRangeNV'.");
            }
        }
        public static void glGetPathMetricRangeNV(GLbitfield @metricQueryMask, GLuint @firstPathName, GLsizei @numPaths, GLsizei @stride, GLfloat * @metrics) => glGetPathMetricRangeNVPtr(@metricQueryMask, @firstPathName, @numPaths, @stride, @metrics);

        internal delegate void glGetPathSpacingNVFunc(GLenum @pathListMode, GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLfloat @advanceScale, GLfloat @kerningScale, GLenum @transformType, GLfloat * @returnedSpacing);
        internal static glGetPathSpacingNVFunc glGetPathSpacingNVPtr;
        internal static void loadGetPathSpacingNV()
        {
            try
            {
                glGetPathSpacingNVPtr = (glGetPathSpacingNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathSpacingNV"), typeof(glGetPathSpacingNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathSpacingNV'.");
            }
        }
        public static void glGetPathSpacingNV(GLenum @pathListMode, GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLfloat @advanceScale, GLfloat @kerningScale, GLenum @transformType, GLfloat * @returnedSpacing) => glGetPathSpacingNVPtr(@pathListMode, @numPaths, @pathNameType, @paths, @pathBase, @advanceScale, @kerningScale, @transformType, @returnedSpacing);

        internal delegate GLboolean glIsPointInFillPathNVFunc(GLuint @path, GLuint @mask, GLfloat @x, GLfloat @y);
        internal static glIsPointInFillPathNVFunc glIsPointInFillPathNVPtr;
        internal static void loadIsPointInFillPathNV()
        {
            try
            {
                glIsPointInFillPathNVPtr = (glIsPointInFillPathNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsPointInFillPathNV"), typeof(glIsPointInFillPathNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsPointInFillPathNV'.");
            }
        }
        public static GLboolean glIsPointInFillPathNV(GLuint @path, GLuint @mask, GLfloat @x, GLfloat @y) => glIsPointInFillPathNVPtr(@path, @mask, @x, @y);

        internal delegate GLboolean glIsPointInStrokePathNVFunc(GLuint @path, GLfloat @x, GLfloat @y);
        internal static glIsPointInStrokePathNVFunc glIsPointInStrokePathNVPtr;
        internal static void loadIsPointInStrokePathNV()
        {
            try
            {
                glIsPointInStrokePathNVPtr = (glIsPointInStrokePathNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsPointInStrokePathNV"), typeof(glIsPointInStrokePathNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsPointInStrokePathNV'.");
            }
        }
        public static GLboolean glIsPointInStrokePathNV(GLuint @path, GLfloat @x, GLfloat @y) => glIsPointInStrokePathNVPtr(@path, @x, @y);

        internal delegate GLfloat glGetPathLengthNVFunc(GLuint @path, GLsizei @startSegment, GLsizei @numSegments);
        internal static glGetPathLengthNVFunc glGetPathLengthNVPtr;
        internal static void loadGetPathLengthNV()
        {
            try
            {
                glGetPathLengthNVPtr = (glGetPathLengthNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathLengthNV"), typeof(glGetPathLengthNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathLengthNV'.");
            }
        }
        public static GLfloat glGetPathLengthNV(GLuint @path, GLsizei @startSegment, GLsizei @numSegments) => glGetPathLengthNVPtr(@path, @startSegment, @numSegments);

        internal delegate GLboolean glPointAlongPathNVFunc(GLuint @path, GLsizei @startSegment, GLsizei @numSegments, GLfloat @distance, GLfloat * @x, GLfloat * @y, GLfloat * @tangentX, GLfloat * @tangentY);
        internal static glPointAlongPathNVFunc glPointAlongPathNVPtr;
        internal static void loadPointAlongPathNV()
        {
            try
            {
                glPointAlongPathNVPtr = (glPointAlongPathNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointAlongPathNV"), typeof(glPointAlongPathNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointAlongPathNV'.");
            }
        }
        public static GLboolean glPointAlongPathNV(GLuint @path, GLsizei @startSegment, GLsizei @numSegments, GLfloat @distance, GLfloat * @x, GLfloat * @y, GLfloat * @tangentX, GLfloat * @tangentY) => glPointAlongPathNVPtr(@path, @startSegment, @numSegments, @distance, @x, @y, @tangentX, @tangentY);

        internal delegate void glMatrixLoad3x2fNVFunc(GLenum @matrixMode, const GLfloat * @m);
        internal static glMatrixLoad3x2fNVFunc glMatrixLoad3x2fNVPtr;
        internal static void loadMatrixLoad3x2fNV()
        {
            try
            {
                glMatrixLoad3x2fNVPtr = (glMatrixLoad3x2fNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixLoad3x2fNV"), typeof(glMatrixLoad3x2fNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixLoad3x2fNV'.");
            }
        }
        public static void glMatrixLoad3x2fNV(GLenum @matrixMode, const GLfloat * @m) => glMatrixLoad3x2fNVPtr(@matrixMode, @m);

        internal delegate void glMatrixLoad3x3fNVFunc(GLenum @matrixMode, const GLfloat * @m);
        internal static glMatrixLoad3x3fNVFunc glMatrixLoad3x3fNVPtr;
        internal static void loadMatrixLoad3x3fNV()
        {
            try
            {
                glMatrixLoad3x3fNVPtr = (glMatrixLoad3x3fNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixLoad3x3fNV"), typeof(glMatrixLoad3x3fNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixLoad3x3fNV'.");
            }
        }
        public static void glMatrixLoad3x3fNV(GLenum @matrixMode, const GLfloat * @m) => glMatrixLoad3x3fNVPtr(@matrixMode, @m);

        internal delegate void glMatrixLoadTranspose3x3fNVFunc(GLenum @matrixMode, const GLfloat * @m);
        internal static glMatrixLoadTranspose3x3fNVFunc glMatrixLoadTranspose3x3fNVPtr;
        internal static void loadMatrixLoadTranspose3x3fNV()
        {
            try
            {
                glMatrixLoadTranspose3x3fNVPtr = (glMatrixLoadTranspose3x3fNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixLoadTranspose3x3fNV"), typeof(glMatrixLoadTranspose3x3fNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixLoadTranspose3x3fNV'.");
            }
        }
        public static void glMatrixLoadTranspose3x3fNV(GLenum @matrixMode, const GLfloat * @m) => glMatrixLoadTranspose3x3fNVPtr(@matrixMode, @m);

        internal delegate void glMatrixMult3x2fNVFunc(GLenum @matrixMode, const GLfloat * @m);
        internal static glMatrixMult3x2fNVFunc glMatrixMult3x2fNVPtr;
        internal static void loadMatrixMult3x2fNV()
        {
            try
            {
                glMatrixMult3x2fNVPtr = (glMatrixMult3x2fNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixMult3x2fNV"), typeof(glMatrixMult3x2fNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixMult3x2fNV'.");
            }
        }
        public static void glMatrixMult3x2fNV(GLenum @matrixMode, const GLfloat * @m) => glMatrixMult3x2fNVPtr(@matrixMode, @m);

        internal delegate void glMatrixMult3x3fNVFunc(GLenum @matrixMode, const GLfloat * @m);
        internal static glMatrixMult3x3fNVFunc glMatrixMult3x3fNVPtr;
        internal static void loadMatrixMult3x3fNV()
        {
            try
            {
                glMatrixMult3x3fNVPtr = (glMatrixMult3x3fNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixMult3x3fNV"), typeof(glMatrixMult3x3fNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixMult3x3fNV'.");
            }
        }
        public static void glMatrixMult3x3fNV(GLenum @matrixMode, const GLfloat * @m) => glMatrixMult3x3fNVPtr(@matrixMode, @m);

        internal delegate void glMatrixMultTranspose3x3fNVFunc(GLenum @matrixMode, const GLfloat * @m);
        internal static glMatrixMultTranspose3x3fNVFunc glMatrixMultTranspose3x3fNVPtr;
        internal static void loadMatrixMultTranspose3x3fNV()
        {
            try
            {
                glMatrixMultTranspose3x3fNVPtr = (glMatrixMultTranspose3x3fNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixMultTranspose3x3fNV"), typeof(glMatrixMultTranspose3x3fNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixMultTranspose3x3fNV'.");
            }
        }
        public static void glMatrixMultTranspose3x3fNV(GLenum @matrixMode, const GLfloat * @m) => glMatrixMultTranspose3x3fNVPtr(@matrixMode, @m);

        internal delegate void glStencilThenCoverFillPathNVFunc(GLuint @path, GLenum @fillMode, GLuint @mask, GLenum @coverMode);
        internal static glStencilThenCoverFillPathNVFunc glStencilThenCoverFillPathNVPtr;
        internal static void loadStencilThenCoverFillPathNV()
        {
            try
            {
                glStencilThenCoverFillPathNVPtr = (glStencilThenCoverFillPathNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilThenCoverFillPathNV"), typeof(glStencilThenCoverFillPathNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilThenCoverFillPathNV'.");
            }
        }
        public static void glStencilThenCoverFillPathNV(GLuint @path, GLenum @fillMode, GLuint @mask, GLenum @coverMode) => glStencilThenCoverFillPathNVPtr(@path, @fillMode, @mask, @coverMode);

        internal delegate void glStencilThenCoverStrokePathNVFunc(GLuint @path, GLint @reference, GLuint @mask, GLenum @coverMode);
        internal static glStencilThenCoverStrokePathNVFunc glStencilThenCoverStrokePathNVPtr;
        internal static void loadStencilThenCoverStrokePathNV()
        {
            try
            {
                glStencilThenCoverStrokePathNVPtr = (glStencilThenCoverStrokePathNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilThenCoverStrokePathNV"), typeof(glStencilThenCoverStrokePathNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilThenCoverStrokePathNV'.");
            }
        }
        public static void glStencilThenCoverStrokePathNV(GLuint @path, GLint @reference, GLuint @mask, GLenum @coverMode) => glStencilThenCoverStrokePathNVPtr(@path, @reference, @mask, @coverMode);

        internal delegate void glStencilThenCoverFillPathInstancedNVFunc(GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLenum @fillMode, GLuint @mask, GLenum @coverMode, GLenum @transformType, const GLfloat * @transformValues);
        internal static glStencilThenCoverFillPathInstancedNVFunc glStencilThenCoverFillPathInstancedNVPtr;
        internal static void loadStencilThenCoverFillPathInstancedNV()
        {
            try
            {
                glStencilThenCoverFillPathInstancedNVPtr = (glStencilThenCoverFillPathInstancedNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilThenCoverFillPathInstancedNV"), typeof(glStencilThenCoverFillPathInstancedNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilThenCoverFillPathInstancedNV'.");
            }
        }
        public static void glStencilThenCoverFillPathInstancedNV(GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLenum @fillMode, GLuint @mask, GLenum @coverMode, GLenum @transformType, const GLfloat * @transformValues) => glStencilThenCoverFillPathInstancedNVPtr(@numPaths, @pathNameType, @paths, @pathBase, @fillMode, @mask, @coverMode, @transformType, @transformValues);

        internal delegate void glStencilThenCoverStrokePathInstancedNVFunc(GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLint @reference, GLuint @mask, GLenum @coverMode, GLenum @transformType, const GLfloat * @transformValues);
        internal static glStencilThenCoverStrokePathInstancedNVFunc glStencilThenCoverStrokePathInstancedNVPtr;
        internal static void loadStencilThenCoverStrokePathInstancedNV()
        {
            try
            {
                glStencilThenCoverStrokePathInstancedNVPtr = (glStencilThenCoverStrokePathInstancedNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilThenCoverStrokePathInstancedNV"), typeof(glStencilThenCoverStrokePathInstancedNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilThenCoverStrokePathInstancedNV'.");
            }
        }
        public static void glStencilThenCoverStrokePathInstancedNV(GLsizei @numPaths, GLenum @pathNameType, const void * @paths, GLuint @pathBase, GLint @reference, GLuint @mask, GLenum @coverMode, GLenum @transformType, const GLfloat * @transformValues) => glStencilThenCoverStrokePathInstancedNVPtr(@numPaths, @pathNameType, @paths, @pathBase, @reference, @mask, @coverMode, @transformType, @transformValues);

        internal delegate GLenum glPathGlyphIndexRangeNVFunc(GLenum @fontTarget, const void * @fontName, GLbitfield @fontStyle, GLuint @pathParameterTemplate, GLfloat @emScale, GLuint[2] @baseAndCount);
        internal static glPathGlyphIndexRangeNVFunc glPathGlyphIndexRangeNVPtr;
        internal static void loadPathGlyphIndexRangeNV()
        {
            try
            {
                glPathGlyphIndexRangeNVPtr = (glPathGlyphIndexRangeNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathGlyphIndexRangeNV"), typeof(glPathGlyphIndexRangeNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathGlyphIndexRangeNV'.");
            }
        }
        public static GLenum glPathGlyphIndexRangeNV(GLenum @fontTarget, const void * @fontName, GLbitfield @fontStyle, GLuint @pathParameterTemplate, GLfloat @emScale, GLuint[2] @baseAndCount) => glPathGlyphIndexRangeNVPtr(@fontTarget, @fontName, @fontStyle, @pathParameterTemplate, @emScale, @baseAndCount);

        internal delegate GLenum glPathGlyphIndexArrayNVFunc(GLuint @firstPathName, GLenum @fontTarget, const void * @fontName, GLbitfield @fontStyle, GLuint @firstGlyphIndex, GLsizei @numGlyphs, GLuint @pathParameterTemplate, GLfloat @emScale);
        internal static glPathGlyphIndexArrayNVFunc glPathGlyphIndexArrayNVPtr;
        internal static void loadPathGlyphIndexArrayNV()
        {
            try
            {
                glPathGlyphIndexArrayNVPtr = (glPathGlyphIndexArrayNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathGlyphIndexArrayNV"), typeof(glPathGlyphIndexArrayNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathGlyphIndexArrayNV'.");
            }
        }
        public static GLenum glPathGlyphIndexArrayNV(GLuint @firstPathName, GLenum @fontTarget, const void * @fontName, GLbitfield @fontStyle, GLuint @firstGlyphIndex, GLsizei @numGlyphs, GLuint @pathParameterTemplate, GLfloat @emScale) => glPathGlyphIndexArrayNVPtr(@firstPathName, @fontTarget, @fontName, @fontStyle, @firstGlyphIndex, @numGlyphs, @pathParameterTemplate, @emScale);

        internal delegate GLenum glPathMemoryGlyphIndexArrayNVFunc(GLuint @firstPathName, GLenum @fontTarget, GLsizeiptr @fontSize, const void * @fontData, GLsizei @faceIndex, GLuint @firstGlyphIndex, GLsizei @numGlyphs, GLuint @pathParameterTemplate, GLfloat @emScale);
        internal static glPathMemoryGlyphIndexArrayNVFunc glPathMemoryGlyphIndexArrayNVPtr;
        internal static void loadPathMemoryGlyphIndexArrayNV()
        {
            try
            {
                glPathMemoryGlyphIndexArrayNVPtr = (glPathMemoryGlyphIndexArrayNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathMemoryGlyphIndexArrayNV"), typeof(glPathMemoryGlyphIndexArrayNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathMemoryGlyphIndexArrayNV'.");
            }
        }
        public static GLenum glPathMemoryGlyphIndexArrayNV(GLuint @firstPathName, GLenum @fontTarget, GLsizeiptr @fontSize, const void * @fontData, GLsizei @faceIndex, GLuint @firstGlyphIndex, GLsizei @numGlyphs, GLuint @pathParameterTemplate, GLfloat @emScale) => glPathMemoryGlyphIndexArrayNVPtr(@firstPathName, @fontTarget, @fontSize, @fontData, @faceIndex, @firstGlyphIndex, @numGlyphs, @pathParameterTemplate, @emScale);

        internal delegate void glProgramPathFragmentInputGenNVFunc(GLuint @program, GLint @location, GLenum @genMode, GLint @components, const GLfloat * @coeffs);
        internal static glProgramPathFragmentInputGenNVFunc glProgramPathFragmentInputGenNVPtr;
        internal static void loadProgramPathFragmentInputGenNV()
        {
            try
            {
                glProgramPathFragmentInputGenNVPtr = (glProgramPathFragmentInputGenNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramPathFragmentInputGenNV"), typeof(glProgramPathFragmentInputGenNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramPathFragmentInputGenNV'.");
            }
        }
        public static void glProgramPathFragmentInputGenNV(GLuint @program, GLint @location, GLenum @genMode, GLint @components, const GLfloat * @coeffs) => glProgramPathFragmentInputGenNVPtr(@program, @location, @genMode, @components, @coeffs);

        internal delegate void glGetProgramResourcefvNVFunc(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @propCount, const GLenum * @props, GLsizei @bufSize, GLsizei * @length, GLfloat * @params);
        internal static glGetProgramResourcefvNVFunc glGetProgramResourcefvNVPtr;
        internal static void loadGetProgramResourcefvNV()
        {
            try
            {
                glGetProgramResourcefvNVPtr = (glGetProgramResourcefvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourcefvNV"), typeof(glGetProgramResourcefvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourcefvNV'.");
            }
        }
        public static void glGetProgramResourcefvNV(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @propCount, const GLenum * @props, GLsizei @bufSize, GLsizei * @length, GLfloat * @params) => glGetProgramResourcefvNVPtr(@program, @programInterface, @index, @propCount, @props, @bufSize, @length, @params);

        internal delegate void glPathColorGenNVFunc(GLenum @color, GLenum @genMode, GLenum @colorFormat, const GLfloat * @coeffs);
        internal static glPathColorGenNVFunc glPathColorGenNVPtr;
        internal static void loadPathColorGenNV()
        {
            try
            {
                glPathColorGenNVPtr = (glPathColorGenNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathColorGenNV"), typeof(glPathColorGenNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathColorGenNV'.");
            }
        }
        public static void glPathColorGenNV(GLenum @color, GLenum @genMode, GLenum @colorFormat, const GLfloat * @coeffs) => glPathColorGenNVPtr(@color, @genMode, @colorFormat, @coeffs);

        internal delegate void glPathTexGenNVFunc(GLenum @texCoordSet, GLenum @genMode, GLint @components, const GLfloat * @coeffs);
        internal static glPathTexGenNVFunc glPathTexGenNVPtr;
        internal static void loadPathTexGenNV()
        {
            try
            {
                glPathTexGenNVPtr = (glPathTexGenNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathTexGenNV"), typeof(glPathTexGenNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathTexGenNV'.");
            }
        }
        public static void glPathTexGenNV(GLenum @texCoordSet, GLenum @genMode, GLint @components, const GLfloat * @coeffs) => glPathTexGenNVPtr(@texCoordSet, @genMode, @components, @coeffs);

        internal delegate void glPathFogGenNVFunc(GLenum @genMode);
        internal static glPathFogGenNVFunc glPathFogGenNVPtr;
        internal static void loadPathFogGenNV()
        {
            try
            {
                glPathFogGenNVPtr = (glPathFogGenNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPathFogGenNV"), typeof(glPathFogGenNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPathFogGenNV'.");
            }
        }
        public static void glPathFogGenNV(GLenum @genMode) => glPathFogGenNVPtr(@genMode);

        internal delegate void glGetPathColorGenivNVFunc(GLenum @color, GLenum @pname, GLint * @value);
        internal static glGetPathColorGenivNVFunc glGetPathColorGenivNVPtr;
        internal static void loadGetPathColorGenivNV()
        {
            try
            {
                glGetPathColorGenivNVPtr = (glGetPathColorGenivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathColorGenivNV"), typeof(glGetPathColorGenivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathColorGenivNV'.");
            }
        }
        public static void glGetPathColorGenivNV(GLenum @color, GLenum @pname, GLint * @value) => glGetPathColorGenivNVPtr(@color, @pname, @value);

        internal delegate void glGetPathColorGenfvNVFunc(GLenum @color, GLenum @pname, GLfloat * @value);
        internal static glGetPathColorGenfvNVFunc glGetPathColorGenfvNVPtr;
        internal static void loadGetPathColorGenfvNV()
        {
            try
            {
                glGetPathColorGenfvNVPtr = (glGetPathColorGenfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathColorGenfvNV"), typeof(glGetPathColorGenfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathColorGenfvNV'.");
            }
        }
        public static void glGetPathColorGenfvNV(GLenum @color, GLenum @pname, GLfloat * @value) => glGetPathColorGenfvNVPtr(@color, @pname, @value);

        internal delegate void glGetPathTexGenivNVFunc(GLenum @texCoordSet, GLenum @pname, GLint * @value);
        internal static glGetPathTexGenivNVFunc glGetPathTexGenivNVPtr;
        internal static void loadGetPathTexGenivNV()
        {
            try
            {
                glGetPathTexGenivNVPtr = (glGetPathTexGenivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathTexGenivNV"), typeof(glGetPathTexGenivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathTexGenivNV'.");
            }
        }
        public static void glGetPathTexGenivNV(GLenum @texCoordSet, GLenum @pname, GLint * @value) => glGetPathTexGenivNVPtr(@texCoordSet, @pname, @value);

        internal delegate void glGetPathTexGenfvNVFunc(GLenum @texCoordSet, GLenum @pname, GLfloat * @value);
        internal static glGetPathTexGenfvNVFunc glGetPathTexGenfvNVPtr;
        internal static void loadGetPathTexGenfvNV()
        {
            try
            {
                glGetPathTexGenfvNVPtr = (glGetPathTexGenfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPathTexGenfvNV"), typeof(glGetPathTexGenfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPathTexGenfvNV'.");
            }
        }
        public static void glGetPathTexGenfvNV(GLenum @texCoordSet, GLenum @pname, GLfloat * @value) => glGetPathTexGenfvNVPtr(@texCoordSet, @pname, @value);
        #endregion
    }
}
