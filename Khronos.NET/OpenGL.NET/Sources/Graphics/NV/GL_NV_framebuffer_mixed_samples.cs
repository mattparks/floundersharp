using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_framebuffer_mixed_samples
    {
        #region Interop
        static GL_NV_framebuffer_mixed_samples()
        {
            Console.WriteLine("Initalising GL_NV_framebuffer_mixed_samples interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadRasterSamplesEXT();
            loadCoverageModulationTableNV();
            loadGetCoverageModulationTableNV();
            loadCoverageModulationNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RASTER_MULTISAMPLE_EXT = 0x9327;
        public static UInt32 GL_COVERAGE_MODULATION_TABLE_NV = 0x9331;
        public static UInt32 GL_RASTER_SAMPLES_EXT = 0x9328;
        public static UInt32 GL_MAX_RASTER_SAMPLES_EXT = 0x9329;
        public static UInt32 GL_RASTER_FIXED_SAMPLE_LOCATIONS_EXT = 0x932A;
        public static UInt32 GL_MULTISAMPLE_RASTERIZATION_ALLOWED_EXT = 0x932B;
        public static UInt32 GL_EFFECTIVE_RASTER_SAMPLES_EXT = 0x932C;
        public static UInt32 GL_COLOR_SAMPLES_NV = 0x8E20;
        public static UInt32 GL_DEPTH_SAMPLES_NV = 0x932D;
        public static UInt32 GL_STENCIL_SAMPLES_NV = 0x932E;
        public static UInt32 GL_MIXED_DEPTH_SAMPLES_SUPPORTED_NV = 0x932F;
        public static UInt32 GL_MIXED_STENCIL_SAMPLES_SUPPORTED_NV = 0x9330;
        public static UInt32 GL_COVERAGE_MODULATION_NV = 0x9332;
        public static UInt32 GL_COVERAGE_MODULATION_TABLE_SIZE_NV = 0x9333;
        #endregion

        #region Commands
        internal delegate void glRasterSamplesEXTFunc(GLuint @samples, GLboolean @fixedsamplelocations);
        internal static glRasterSamplesEXTFunc glRasterSamplesEXTPtr;
        internal static void loadRasterSamplesEXT()
        {
            try
            {
                glRasterSamplesEXTPtr = (glRasterSamplesEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterSamplesEXT"), typeof(glRasterSamplesEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterSamplesEXT'.");
            }
        }
        public static void glRasterSamplesEXT(GLuint @samples, GLboolean @fixedsamplelocations) => glRasterSamplesEXTPtr(@samples, @fixedsamplelocations);

        internal delegate void glCoverageModulationTableNVFunc(GLsizei @n, const GLfloat * @v);
        internal static glCoverageModulationTableNVFunc glCoverageModulationTableNVPtr;
        internal static void loadCoverageModulationTableNV()
        {
            try
            {
                glCoverageModulationTableNVPtr = (glCoverageModulationTableNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCoverageModulationTableNV"), typeof(glCoverageModulationTableNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCoverageModulationTableNV'.");
            }
        }
        public static void glCoverageModulationTableNV(GLsizei @n, const GLfloat * @v) => glCoverageModulationTableNVPtr(@n, @v);

        internal delegate void glGetCoverageModulationTableNVFunc(GLsizei @bufsize, GLfloat * @v);
        internal static glGetCoverageModulationTableNVFunc glGetCoverageModulationTableNVPtr;
        internal static void loadGetCoverageModulationTableNV()
        {
            try
            {
                glGetCoverageModulationTableNVPtr = (glGetCoverageModulationTableNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCoverageModulationTableNV"), typeof(glGetCoverageModulationTableNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCoverageModulationTableNV'.");
            }
        }
        public static void glGetCoverageModulationTableNV(GLsizei @bufsize, GLfloat * @v) => glGetCoverageModulationTableNVPtr(@bufsize, @v);

        internal delegate void glCoverageModulationNVFunc(GLenum @components);
        internal static glCoverageModulationNVFunc glCoverageModulationNVPtr;
        internal static void loadCoverageModulationNV()
        {
            try
            {
                glCoverageModulationNVPtr = (glCoverageModulationNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCoverageModulationNV"), typeof(glCoverageModulationNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCoverageModulationNV'.");
            }
        }
        public static void glCoverageModulationNV(GLenum @components) => glCoverageModulationNVPtr(@components);
        #endregion
    }
}
