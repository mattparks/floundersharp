using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_shader_buffer_load
    {
        #region Interop
        static GL_NV_shader_buffer_load()
        {
            Console.WriteLine("Initalising GL_NV_shader_buffer_load interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMakeBufferResidentNV();
            loadMakeBufferNonResidentNV();
            loadIsBufferResidentNV();
            loadMakeNamedBufferResidentNV();
            loadMakeNamedBufferNonResidentNV();
            loadIsNamedBufferResidentNV();
            loadGetBufferParameterui64vNV();
            loadGetNamedBufferParameterui64vNV();
            loadGetIntegerui64vNV();
            loadUniformui64NV();
            loadUniformui64vNV();
            loadGetUniformui64vNV();
            loadProgramUniformui64NV();
            loadProgramUniformui64vNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BUFFER_GPU_ADDRESS_NV = 0x8F1D;
        public static UInt32 GL_GPU_ADDRESS_NV = 0x8F34;
        public static UInt32 GL_MAX_SHADER_BUFFER_ADDRESS_NV = 0x8F35;
        #endregion

        #region Commands
        internal delegate void glMakeBufferResidentNVFunc(GLenum @target, GLenum @access);
        internal static glMakeBufferResidentNVFunc glMakeBufferResidentNVPtr;
        internal static void loadMakeBufferResidentNV()
        {
            try
            {
                glMakeBufferResidentNVPtr = (glMakeBufferResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMakeBufferResidentNV"), typeof(glMakeBufferResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMakeBufferResidentNV'.");
            }
        }
        public static void glMakeBufferResidentNV(GLenum @target, GLenum @access) => glMakeBufferResidentNVPtr(@target, @access);

        internal delegate void glMakeBufferNonResidentNVFunc(GLenum @target);
        internal static glMakeBufferNonResidentNVFunc glMakeBufferNonResidentNVPtr;
        internal static void loadMakeBufferNonResidentNV()
        {
            try
            {
                glMakeBufferNonResidentNVPtr = (glMakeBufferNonResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMakeBufferNonResidentNV"), typeof(glMakeBufferNonResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMakeBufferNonResidentNV'.");
            }
        }
        public static void glMakeBufferNonResidentNV(GLenum @target) => glMakeBufferNonResidentNVPtr(@target);

        internal delegate GLboolean glIsBufferResidentNVFunc(GLenum @target);
        internal static glIsBufferResidentNVFunc glIsBufferResidentNVPtr;
        internal static void loadIsBufferResidentNV()
        {
            try
            {
                glIsBufferResidentNVPtr = (glIsBufferResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsBufferResidentNV"), typeof(glIsBufferResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsBufferResidentNV'.");
            }
        }
        public static GLboolean glIsBufferResidentNV(GLenum @target) => glIsBufferResidentNVPtr(@target);

        internal delegate void glMakeNamedBufferResidentNVFunc(GLuint @buffer, GLenum @access);
        internal static glMakeNamedBufferResidentNVFunc glMakeNamedBufferResidentNVPtr;
        internal static void loadMakeNamedBufferResidentNV()
        {
            try
            {
                glMakeNamedBufferResidentNVPtr = (glMakeNamedBufferResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMakeNamedBufferResidentNV"), typeof(glMakeNamedBufferResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMakeNamedBufferResidentNV'.");
            }
        }
        public static void glMakeNamedBufferResidentNV(GLuint @buffer, GLenum @access) => glMakeNamedBufferResidentNVPtr(@buffer, @access);

        internal delegate void glMakeNamedBufferNonResidentNVFunc(GLuint @buffer);
        internal static glMakeNamedBufferNonResidentNVFunc glMakeNamedBufferNonResidentNVPtr;
        internal static void loadMakeNamedBufferNonResidentNV()
        {
            try
            {
                glMakeNamedBufferNonResidentNVPtr = (glMakeNamedBufferNonResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMakeNamedBufferNonResidentNV"), typeof(glMakeNamedBufferNonResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMakeNamedBufferNonResidentNV'.");
            }
        }
        public static void glMakeNamedBufferNonResidentNV(GLuint @buffer) => glMakeNamedBufferNonResidentNVPtr(@buffer);

        internal delegate GLboolean glIsNamedBufferResidentNVFunc(GLuint @buffer);
        internal static glIsNamedBufferResidentNVFunc glIsNamedBufferResidentNVPtr;
        internal static void loadIsNamedBufferResidentNV()
        {
            try
            {
                glIsNamedBufferResidentNVPtr = (glIsNamedBufferResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsNamedBufferResidentNV"), typeof(glIsNamedBufferResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsNamedBufferResidentNV'.");
            }
        }
        public static GLboolean glIsNamedBufferResidentNV(GLuint @buffer) => glIsNamedBufferResidentNVPtr(@buffer);

        internal delegate void glGetBufferParameterui64vNVFunc(GLenum @target, GLenum @pname, GLuint64EXT * @params);
        internal static glGetBufferParameterui64vNVFunc glGetBufferParameterui64vNVPtr;
        internal static void loadGetBufferParameterui64vNV()
        {
            try
            {
                glGetBufferParameterui64vNVPtr = (glGetBufferParameterui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferParameterui64vNV"), typeof(glGetBufferParameterui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferParameterui64vNV'.");
            }
        }
        public static void glGetBufferParameterui64vNV(GLenum @target, GLenum @pname, GLuint64EXT * @params) => glGetBufferParameterui64vNVPtr(@target, @pname, @params);

        internal delegate void glGetNamedBufferParameterui64vNVFunc(GLuint @buffer, GLenum @pname, GLuint64EXT * @params);
        internal static glGetNamedBufferParameterui64vNVFunc glGetNamedBufferParameterui64vNVPtr;
        internal static void loadGetNamedBufferParameterui64vNV()
        {
            try
            {
                glGetNamedBufferParameterui64vNVPtr = (glGetNamedBufferParameterui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedBufferParameterui64vNV"), typeof(glGetNamedBufferParameterui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedBufferParameterui64vNV'.");
            }
        }
        public static void glGetNamedBufferParameterui64vNV(GLuint @buffer, GLenum @pname, GLuint64EXT * @params) => glGetNamedBufferParameterui64vNVPtr(@buffer, @pname, @params);

        internal delegate void glGetIntegerui64vNVFunc(GLenum @value, GLuint64EXT * @result);
        internal static glGetIntegerui64vNVFunc glGetIntegerui64vNVPtr;
        internal static void loadGetIntegerui64vNV()
        {
            try
            {
                glGetIntegerui64vNVPtr = (glGetIntegerui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetIntegerui64vNV"), typeof(glGetIntegerui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetIntegerui64vNV'.");
            }
        }
        public static void glGetIntegerui64vNV(GLenum @value, GLuint64EXT * @result) => glGetIntegerui64vNVPtr(@value, @result);

        internal delegate void glUniformui64NVFunc(GLint @location, GLuint64EXT @value);
        internal static glUniformui64NVFunc glUniformui64NVPtr;
        internal static void loadUniformui64NV()
        {
            try
            {
                glUniformui64NVPtr = (glUniformui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformui64NV"), typeof(glUniformui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformui64NV'.");
            }
        }
        public static void glUniformui64NV(GLint @location, GLuint64EXT @value) => glUniformui64NVPtr(@location, @value);

        internal delegate void glUniformui64vNVFunc(GLint @location, GLsizei @count, const GLuint64EXT * @value);
        internal static glUniformui64vNVFunc glUniformui64vNVPtr;
        internal static void loadUniformui64vNV()
        {
            try
            {
                glUniformui64vNVPtr = (glUniformui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformui64vNV"), typeof(glUniformui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformui64vNV'.");
            }
        }
        public static void glUniformui64vNV(GLint @location, GLsizei @count, const GLuint64EXT * @value) => glUniformui64vNVPtr(@location, @count, @value);

        internal delegate void glGetUniformui64vNVFunc(GLuint @program, GLint @location, GLuint64EXT * @params);
        internal static glGetUniformui64vNVFunc glGetUniformui64vNVPtr;
        internal static void loadGetUniformui64vNV()
        {
            try
            {
                glGetUniformui64vNVPtr = (glGetUniformui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformui64vNV"), typeof(glGetUniformui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformui64vNV'.");
            }
        }
        public static void glGetUniformui64vNV(GLuint @program, GLint @location, GLuint64EXT * @params) => glGetUniformui64vNVPtr(@program, @location, @params);

        internal delegate void glProgramUniformui64NVFunc(GLuint @program, GLint @location, GLuint64EXT @value);
        internal static glProgramUniformui64NVFunc glProgramUniformui64NVPtr;
        internal static void loadProgramUniformui64NV()
        {
            try
            {
                glProgramUniformui64NVPtr = (glProgramUniformui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformui64NV"), typeof(glProgramUniformui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformui64NV'.");
            }
        }
        public static void glProgramUniformui64NV(GLuint @program, GLint @location, GLuint64EXT @value) => glProgramUniformui64NVPtr(@program, @location, @value);

        internal delegate void glProgramUniformui64vNVFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint64EXT * @value);
        internal static glProgramUniformui64vNVFunc glProgramUniformui64vNVPtr;
        internal static void loadProgramUniformui64vNV()
        {
            try
            {
                glProgramUniformui64vNVPtr = (glProgramUniformui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformui64vNV"), typeof(glProgramUniformui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformui64vNV'.");
            }
        }
        public static void glProgramUniformui64vNV(GLuint @program, GLint @location, GLsizei @count, const GLuint64EXT * @value) => glProgramUniformui64vNVPtr(@program, @location, @count, @value);
        #endregion
    }
}
