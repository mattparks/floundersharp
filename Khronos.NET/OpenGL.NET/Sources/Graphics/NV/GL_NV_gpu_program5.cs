using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_gpu_program5
    {
        #region Interop
        static GL_NV_gpu_program5()
        {
            Console.WriteLine("Initalising GL_NV_gpu_program5 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadProgramSubroutineParametersuivNV();
            loadGetProgramSubroutineParameteruivNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_GEOMETRY_PROGRAM_INVOCATIONS_NV = 0x8E5A;
        public static UInt32 GL_MIN_FRAGMENT_INTERPOLATION_OFFSET_NV = 0x8E5B;
        public static UInt32 GL_MAX_FRAGMENT_INTERPOLATION_OFFSET_NV = 0x8E5C;
        public static UInt32 GL_FRAGMENT_PROGRAM_INTERPOLATION_OFFSET_BITS_NV = 0x8E5D;
        public static UInt32 GL_MIN_PROGRAM_TEXTURE_GATHER_OFFSET_NV = 0x8E5E;
        public static UInt32 GL_MAX_PROGRAM_TEXTURE_GATHER_OFFSET_NV = 0x8E5F;
        public static UInt32 GL_MAX_PROGRAM_SUBROUTINE_PARAMETERS_NV = 0x8F44;
        public static UInt32 GL_MAX_PROGRAM_SUBROUTINE_NUM_NV = 0x8F45;
        #endregion

        #region Commands
        internal delegate void glProgramSubroutineParametersuivNVFunc(GLenum @target, GLsizei @count, const GLuint * @params);
        internal static glProgramSubroutineParametersuivNVFunc glProgramSubroutineParametersuivNVPtr;
        internal static void loadProgramSubroutineParametersuivNV()
        {
            try
            {
                glProgramSubroutineParametersuivNVPtr = (glProgramSubroutineParametersuivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramSubroutineParametersuivNV"), typeof(glProgramSubroutineParametersuivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramSubroutineParametersuivNV'.");
            }
        }
        public static void glProgramSubroutineParametersuivNV(GLenum @target, GLsizei @count, const GLuint * @params) => glProgramSubroutineParametersuivNVPtr(@target, @count, @params);

        internal delegate void glGetProgramSubroutineParameteruivNVFunc(GLenum @target, GLuint @index, GLuint * @param);
        internal static glGetProgramSubroutineParameteruivNVFunc glGetProgramSubroutineParameteruivNVPtr;
        internal static void loadGetProgramSubroutineParameteruivNV()
        {
            try
            {
                glGetProgramSubroutineParameteruivNVPtr = (glGetProgramSubroutineParameteruivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramSubroutineParameteruivNV"), typeof(glGetProgramSubroutineParameteruivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramSubroutineParameteruivNV'.");
            }
        }
        public static void glGetProgramSubroutineParameteruivNV(GLenum @target, GLuint @index, GLuint * @param) => glGetProgramSubroutineParameteruivNVPtr(@target, @index, @param);
        #endregion
    }
}
