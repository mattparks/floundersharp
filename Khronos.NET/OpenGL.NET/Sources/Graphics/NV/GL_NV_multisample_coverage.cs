using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_multisample_coverage
    {
        #region Interop
        static GL_NV_multisample_coverage()
        {
            Console.WriteLine("Initalising GL_NV_multisample_coverage interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SAMPLES_ARB = 0x80A9;
        public static UInt32 GL_COLOR_SAMPLES_NV = 0x8E20;
        #endregion

        #region Commands
        #endregion
    }
}
