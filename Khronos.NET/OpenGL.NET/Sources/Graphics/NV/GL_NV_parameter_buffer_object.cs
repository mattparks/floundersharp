using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_parameter_buffer_object
    {
        #region Interop
        static GL_NV_parameter_buffer_object()
        {
            Console.WriteLine("Initalising GL_NV_parameter_buffer_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadProgramBufferParametersfvNV();
            loadProgramBufferParametersIivNV();
            loadProgramBufferParametersIuivNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_PROGRAM_PARAMETER_BUFFER_BINDINGS_NV = 0x8DA0;
        public static UInt32 GL_MAX_PROGRAM_PARAMETER_BUFFER_SIZE_NV = 0x8DA1;
        public static UInt32 GL_VERTEX_PROGRAM_PARAMETER_BUFFER_NV = 0x8DA2;
        public static UInt32 GL_GEOMETRY_PROGRAM_PARAMETER_BUFFER_NV = 0x8DA3;
        public static UInt32 GL_FRAGMENT_PROGRAM_PARAMETER_BUFFER_NV = 0x8DA4;
        #endregion

        #region Commands
        internal delegate void glProgramBufferParametersfvNVFunc(GLenum @target, GLuint @bindingIndex, GLuint @wordIndex, GLsizei @count, const GLfloat * @params);
        internal static glProgramBufferParametersfvNVFunc glProgramBufferParametersfvNVPtr;
        internal static void loadProgramBufferParametersfvNV()
        {
            try
            {
                glProgramBufferParametersfvNVPtr = (glProgramBufferParametersfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramBufferParametersfvNV"), typeof(glProgramBufferParametersfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramBufferParametersfvNV'.");
            }
        }
        public static void glProgramBufferParametersfvNV(GLenum @target, GLuint @bindingIndex, GLuint @wordIndex, GLsizei @count, const GLfloat * @params) => glProgramBufferParametersfvNVPtr(@target, @bindingIndex, @wordIndex, @count, @params);

        internal delegate void glProgramBufferParametersIivNVFunc(GLenum @target, GLuint @bindingIndex, GLuint @wordIndex, GLsizei @count, const GLint * @params);
        internal static glProgramBufferParametersIivNVFunc glProgramBufferParametersIivNVPtr;
        internal static void loadProgramBufferParametersIivNV()
        {
            try
            {
                glProgramBufferParametersIivNVPtr = (glProgramBufferParametersIivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramBufferParametersIivNV"), typeof(glProgramBufferParametersIivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramBufferParametersIivNV'.");
            }
        }
        public static void glProgramBufferParametersIivNV(GLenum @target, GLuint @bindingIndex, GLuint @wordIndex, GLsizei @count, const GLint * @params) => glProgramBufferParametersIivNVPtr(@target, @bindingIndex, @wordIndex, @count, @params);

        internal delegate void glProgramBufferParametersIuivNVFunc(GLenum @target, GLuint @bindingIndex, GLuint @wordIndex, GLsizei @count, const GLuint * @params);
        internal static glProgramBufferParametersIuivNVFunc glProgramBufferParametersIuivNVPtr;
        internal static void loadProgramBufferParametersIuivNV()
        {
            try
            {
                glProgramBufferParametersIuivNVPtr = (glProgramBufferParametersIuivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramBufferParametersIuivNV"), typeof(glProgramBufferParametersIuivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramBufferParametersIuivNV'.");
            }
        }
        public static void glProgramBufferParametersIuivNV(GLenum @target, GLuint @bindingIndex, GLuint @wordIndex, GLsizei @count, const GLuint * @params) => glProgramBufferParametersIuivNVPtr(@target, @bindingIndex, @wordIndex, @count, @params);
        #endregion
    }
}
