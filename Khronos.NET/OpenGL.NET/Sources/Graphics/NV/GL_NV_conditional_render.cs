using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_conditional_render
    {
        #region Interop
        static GL_NV_conditional_render()
        {
            Console.WriteLine("Initalising GL_NV_conditional_render interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBeginConditionalRenderNV();
            loadEndConditionalRenderNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_QUERY_WAIT_NV = 0x8E13;
        public static UInt32 GL_QUERY_NO_WAIT_NV = 0x8E14;
        public static UInt32 GL_QUERY_BY_REGION_WAIT_NV = 0x8E15;
        public static UInt32 GL_QUERY_BY_REGION_NO_WAIT_NV = 0x8E16;
        #endregion

        #region Commands
        internal delegate void glBeginConditionalRenderNVFunc(GLuint @id, GLenum @mode);
        internal static glBeginConditionalRenderNVFunc glBeginConditionalRenderNVPtr;
        internal static void loadBeginConditionalRenderNV()
        {
            try
            {
                glBeginConditionalRenderNVPtr = (glBeginConditionalRenderNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginConditionalRenderNV"), typeof(glBeginConditionalRenderNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginConditionalRenderNV'.");
            }
        }
        public static void glBeginConditionalRenderNV(GLuint @id, GLenum @mode) => glBeginConditionalRenderNVPtr(@id, @mode);

        internal delegate void glEndConditionalRenderNVFunc();
        internal static glEndConditionalRenderNVFunc glEndConditionalRenderNVPtr;
        internal static void loadEndConditionalRenderNV()
        {
            try
            {
                glEndConditionalRenderNVPtr = (glEndConditionalRenderNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndConditionalRenderNV"), typeof(glEndConditionalRenderNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndConditionalRenderNV'.");
            }
        }
        public static void glEndConditionalRenderNV() => glEndConditionalRenderNVPtr();
        #endregion
    }
}
