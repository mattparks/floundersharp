using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_vertex_program2_option
    {
        #region Interop
        static GL_NV_vertex_program2_option()
        {
            Console.WriteLine("Initalising GL_NV_vertex_program2_option interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_PROGRAM_EXEC_INSTRUCTIONS_NV = 0x88F4;
        public static UInt32 GL_MAX_PROGRAM_CALL_DEPTH_NV = 0x88F5;
        #endregion

        #region Commands
        #endregion
    }
}
