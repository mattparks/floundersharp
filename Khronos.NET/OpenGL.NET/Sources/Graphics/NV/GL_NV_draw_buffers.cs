using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_draw_buffers
    {
        #region Interop
        static GL_NV_draw_buffers()
        {
            Console.WriteLine("Initalising GL_NV_draw_buffers interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawBuffersNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_DRAW_BUFFERS_NV = 0x8824;
        public static UInt32 GL_DRAW_BUFFER0_NV = 0x8825;
        public static UInt32 GL_DRAW_BUFFER1_NV = 0x8826;
        public static UInt32 GL_DRAW_BUFFER2_NV = 0x8827;
        public static UInt32 GL_DRAW_BUFFER3_NV = 0x8828;
        public static UInt32 GL_DRAW_BUFFER4_NV = 0x8829;
        public static UInt32 GL_DRAW_BUFFER5_NV = 0x882A;
        public static UInt32 GL_DRAW_BUFFER6_NV = 0x882B;
        public static UInt32 GL_DRAW_BUFFER7_NV = 0x882C;
        public static UInt32 GL_DRAW_BUFFER8_NV = 0x882D;
        public static UInt32 GL_DRAW_BUFFER9_NV = 0x882E;
        public static UInt32 GL_DRAW_BUFFER10_NV = 0x882F;
        public static UInt32 GL_DRAW_BUFFER11_NV = 0x8830;
        public static UInt32 GL_DRAW_BUFFER12_NV = 0x8831;
        public static UInt32 GL_DRAW_BUFFER13_NV = 0x8832;
        public static UInt32 GL_DRAW_BUFFER14_NV = 0x8833;
        public static UInt32 GL_DRAW_BUFFER15_NV = 0x8834;
        public static UInt32 GL_COLOR_ATTACHMENT0_NV = 0x8CE0;
        public static UInt32 GL_COLOR_ATTACHMENT1_NV = 0x8CE1;
        public static UInt32 GL_COLOR_ATTACHMENT2_NV = 0x8CE2;
        public static UInt32 GL_COLOR_ATTACHMENT3_NV = 0x8CE3;
        public static UInt32 GL_COLOR_ATTACHMENT4_NV = 0x8CE4;
        public static UInt32 GL_COLOR_ATTACHMENT5_NV = 0x8CE5;
        public static UInt32 GL_COLOR_ATTACHMENT6_NV = 0x8CE6;
        public static UInt32 GL_COLOR_ATTACHMENT7_NV = 0x8CE7;
        public static UInt32 GL_COLOR_ATTACHMENT8_NV = 0x8CE8;
        public static UInt32 GL_COLOR_ATTACHMENT9_NV = 0x8CE9;
        public static UInt32 GL_COLOR_ATTACHMENT10_NV = 0x8CEA;
        public static UInt32 GL_COLOR_ATTACHMENT11_NV = 0x8CEB;
        public static UInt32 GL_COLOR_ATTACHMENT12_NV = 0x8CEC;
        public static UInt32 GL_COLOR_ATTACHMENT13_NV = 0x8CED;
        public static UInt32 GL_COLOR_ATTACHMENT14_NV = 0x8CEE;
        public static UInt32 GL_COLOR_ATTACHMENT15_NV = 0x8CEF;
        #endregion

        #region Commands
        internal delegate void glDrawBuffersNVFunc(GLsizei @n, const GLenum * @bufs);
        internal static glDrawBuffersNVFunc glDrawBuffersNVPtr;
        internal static void loadDrawBuffersNV()
        {
            try
            {
                glDrawBuffersNVPtr = (glDrawBuffersNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawBuffersNV"), typeof(glDrawBuffersNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawBuffersNV'.");
            }
        }
        public static void glDrawBuffersNV(GLsizei @n, const GLenum * @bufs) => glDrawBuffersNVPtr(@n, @bufs);
        #endregion
    }
}
