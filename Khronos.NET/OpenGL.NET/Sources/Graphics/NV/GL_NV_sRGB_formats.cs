using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_sRGB_formats
    {
        #region Interop
        static GL_NV_sRGB_formats()
        {
            Console.WriteLine("Initalising GL_NV_sRGB_formats interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SLUMINANCE_NV = 0x8C46;
        public static UInt32 GL_SLUMINANCE_ALPHA_NV = 0x8C44;
        public static UInt32 GL_SRGB8_NV = 0x8C41;
        public static UInt32 GL_SLUMINANCE8_NV = 0x8C47;
        public static UInt32 GL_SLUMINANCE8_ALPHA8_NV = 0x8C45;
        public static UInt32 GL_COMPRESSED_SRGB_S3TC_DXT1_NV = 0x8C4C;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_NV = 0x8C4D;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_NV = 0x8C4E;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_NV = 0x8C4F;
        public static UInt32 GL_ETC1_SRGB8_NV = 0x88EE;
        #endregion

        #region Commands
        #endregion
    }
}
