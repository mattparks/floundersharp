using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_occlusion_query
    {
        #region Interop
        static GL_NV_occlusion_query()
        {
            Console.WriteLine("Initalising GL_NV_occlusion_query interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGenOcclusionQueriesNV();
            loadDeleteOcclusionQueriesNV();
            loadIsOcclusionQueryNV();
            loadBeginOcclusionQueryNV();
            loadEndOcclusionQueryNV();
            loadGetOcclusionQueryivNV();
            loadGetOcclusionQueryuivNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PIXEL_COUNTER_BITS_NV = 0x8864;
        public static UInt32 GL_CURRENT_OCCLUSION_QUERY_ID_NV = 0x8865;
        public static UInt32 GL_PIXEL_COUNT_NV = 0x8866;
        public static UInt32 GL_PIXEL_COUNT_AVAILABLE_NV = 0x8867;
        #endregion

        #region Commands
        internal delegate void glGenOcclusionQueriesNVFunc(GLsizei @n, GLuint * @ids);
        internal static glGenOcclusionQueriesNVFunc glGenOcclusionQueriesNVPtr;
        internal static void loadGenOcclusionQueriesNV()
        {
            try
            {
                glGenOcclusionQueriesNVPtr = (glGenOcclusionQueriesNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenOcclusionQueriesNV"), typeof(glGenOcclusionQueriesNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenOcclusionQueriesNV'.");
            }
        }
        public static void glGenOcclusionQueriesNV(GLsizei @n, GLuint * @ids) => glGenOcclusionQueriesNVPtr(@n, @ids);

        internal delegate void glDeleteOcclusionQueriesNVFunc(GLsizei @n, const GLuint * @ids);
        internal static glDeleteOcclusionQueriesNVFunc glDeleteOcclusionQueriesNVPtr;
        internal static void loadDeleteOcclusionQueriesNV()
        {
            try
            {
                glDeleteOcclusionQueriesNVPtr = (glDeleteOcclusionQueriesNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteOcclusionQueriesNV"), typeof(glDeleteOcclusionQueriesNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteOcclusionQueriesNV'.");
            }
        }
        public static void glDeleteOcclusionQueriesNV(GLsizei @n, const GLuint * @ids) => glDeleteOcclusionQueriesNVPtr(@n, @ids);

        internal delegate GLboolean glIsOcclusionQueryNVFunc(GLuint @id);
        internal static glIsOcclusionQueryNVFunc glIsOcclusionQueryNVPtr;
        internal static void loadIsOcclusionQueryNV()
        {
            try
            {
                glIsOcclusionQueryNVPtr = (glIsOcclusionQueryNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsOcclusionQueryNV"), typeof(glIsOcclusionQueryNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsOcclusionQueryNV'.");
            }
        }
        public static GLboolean glIsOcclusionQueryNV(GLuint @id) => glIsOcclusionQueryNVPtr(@id);

        internal delegate void glBeginOcclusionQueryNVFunc(GLuint @id);
        internal static glBeginOcclusionQueryNVFunc glBeginOcclusionQueryNVPtr;
        internal static void loadBeginOcclusionQueryNV()
        {
            try
            {
                glBeginOcclusionQueryNVPtr = (glBeginOcclusionQueryNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginOcclusionQueryNV"), typeof(glBeginOcclusionQueryNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginOcclusionQueryNV'.");
            }
        }
        public static void glBeginOcclusionQueryNV(GLuint @id) => glBeginOcclusionQueryNVPtr(@id);

        internal delegate void glEndOcclusionQueryNVFunc();
        internal static glEndOcclusionQueryNVFunc glEndOcclusionQueryNVPtr;
        internal static void loadEndOcclusionQueryNV()
        {
            try
            {
                glEndOcclusionQueryNVPtr = (glEndOcclusionQueryNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndOcclusionQueryNV"), typeof(glEndOcclusionQueryNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndOcclusionQueryNV'.");
            }
        }
        public static void glEndOcclusionQueryNV() => glEndOcclusionQueryNVPtr();

        internal delegate void glGetOcclusionQueryivNVFunc(GLuint @id, GLenum @pname, GLint * @params);
        internal static glGetOcclusionQueryivNVFunc glGetOcclusionQueryivNVPtr;
        internal static void loadGetOcclusionQueryivNV()
        {
            try
            {
                glGetOcclusionQueryivNVPtr = (glGetOcclusionQueryivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetOcclusionQueryivNV"), typeof(glGetOcclusionQueryivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetOcclusionQueryivNV'.");
            }
        }
        public static void glGetOcclusionQueryivNV(GLuint @id, GLenum @pname, GLint * @params) => glGetOcclusionQueryivNVPtr(@id, @pname, @params);

        internal delegate void glGetOcclusionQueryuivNVFunc(GLuint @id, GLenum @pname, GLuint * @params);
        internal static glGetOcclusionQueryuivNVFunc glGetOcclusionQueryuivNVPtr;
        internal static void loadGetOcclusionQueryuivNV()
        {
            try
            {
                glGetOcclusionQueryuivNVPtr = (glGetOcclusionQueryuivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetOcclusionQueryuivNV"), typeof(glGetOcclusionQueryuivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetOcclusionQueryuivNV'.");
            }
        }
        public static void glGetOcclusionQueryuivNV(GLuint @id, GLenum @pname, GLuint * @params) => glGetOcclusionQueryuivNVPtr(@id, @pname, @params);
        #endregion
    }
}
