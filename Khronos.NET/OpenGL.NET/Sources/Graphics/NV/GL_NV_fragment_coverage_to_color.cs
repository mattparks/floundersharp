using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_fragment_coverage_to_color
    {
        #region Interop
        static GL_NV_fragment_coverage_to_color()
        {
            Console.WriteLine("Initalising GL_NV_fragment_coverage_to_color interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFragmentCoverageColorNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAGMENT_COVERAGE_TO_COLOR_NV = 0x92DD;
        public static UInt32 GL_FRAGMENT_COVERAGE_COLOR_NV = 0x92DE;
        #endregion

        #region Commands
        internal delegate void glFragmentCoverageColorNVFunc(GLuint @color);
        internal static glFragmentCoverageColorNVFunc glFragmentCoverageColorNVPtr;
        internal static void loadFragmentCoverageColorNV()
        {
            try
            {
                glFragmentCoverageColorNVPtr = (glFragmentCoverageColorNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentCoverageColorNV"), typeof(glFragmentCoverageColorNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentCoverageColorNV'.");
            }
        }
        public static void glFragmentCoverageColorNV(GLuint @color) => glFragmentCoverageColorNVPtr(@color);
        #endregion
    }
}
