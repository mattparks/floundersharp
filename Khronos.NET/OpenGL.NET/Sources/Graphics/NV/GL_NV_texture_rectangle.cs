using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_texture_rectangle
    {
        #region Interop
        static GL_NV_texture_rectangle()
        {
            Console.WriteLine("Initalising GL_NV_texture_rectangle interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_RECTANGLE_NV = 0x84F5;
        public static UInt32 GL_TEXTURE_BINDING_RECTANGLE_NV = 0x84F6;
        public static UInt32 GL_PROXY_TEXTURE_RECTANGLE_NV = 0x84F7;
        public static UInt32 GL_MAX_RECTANGLE_TEXTURE_SIZE_NV = 0x84F8;
        #endregion

        #region Commands
        #endregion
    }
}
