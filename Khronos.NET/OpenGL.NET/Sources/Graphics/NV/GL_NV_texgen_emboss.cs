using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_texgen_emboss
    {
        #region Interop
        static GL_NV_texgen_emboss()
        {
            Console.WriteLine("Initalising GL_NV_texgen_emboss interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_EMBOSS_LIGHT_NV = 0x855D;
        public static UInt32 GL_EMBOSS_CONSTANT_NV = 0x855E;
        public static UInt32 GL_EMBOSS_MAP_NV = 0x855F;
        #endregion

        #region Commands
        #endregion
    }
}
