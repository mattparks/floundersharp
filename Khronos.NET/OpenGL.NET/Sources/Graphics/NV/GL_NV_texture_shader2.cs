using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_texture_shader2
    {
        #region Interop
        static GL_NV_texture_shader2()
        {
            Console.WriteLine("Initalising GL_NV_texture_shader2 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DOT_PRODUCT_TEXTURE_3D_NV = 0x86EF;
        #endregion

        #region Commands
        #endregion
    }
}
