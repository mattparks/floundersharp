using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_fragment_program
    {
        #region Interop
        static GL_NV_fragment_program()
        {
            Console.WriteLine("Initalising GL_NV_fragment_program interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadProgramNamedParameter4fNV();
            loadProgramNamedParameter4fvNV();
            loadProgramNamedParameter4dNV();
            loadProgramNamedParameter4dvNV();
            loadGetProgramNamedParameterfvNV();
            loadGetProgramNamedParameterdvNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_FRAGMENT_PROGRAM_LOCAL_PARAMETERS_NV = 0x8868;
        public static UInt32 GL_FRAGMENT_PROGRAM_NV = 0x8870;
        public static UInt32 GL_MAX_TEXTURE_COORDS_NV = 0x8871;
        public static UInt32 GL_MAX_TEXTURE_IMAGE_UNITS_NV = 0x8872;
        public static UInt32 GL_FRAGMENT_PROGRAM_BINDING_NV = 0x8873;
        public static UInt32 GL_PROGRAM_ERROR_STRING_NV = 0x8874;
        #endregion

        #region Commands
        internal delegate void glProgramNamedParameter4fNVFunc(GLuint @id, GLsizei @len, const GLubyte * @name, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glProgramNamedParameter4fNVFunc glProgramNamedParameter4fNVPtr;
        internal static void loadProgramNamedParameter4fNV()
        {
            try
            {
                glProgramNamedParameter4fNVPtr = (glProgramNamedParameter4fNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramNamedParameter4fNV"), typeof(glProgramNamedParameter4fNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramNamedParameter4fNV'.");
            }
        }
        public static void glProgramNamedParameter4fNV(GLuint @id, GLsizei @len, const GLubyte * @name, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glProgramNamedParameter4fNVPtr(@id, @len, @name, @x, @y, @z, @w);

        internal delegate void glProgramNamedParameter4fvNVFunc(GLuint @id, GLsizei @len, const GLubyte * @name, const GLfloat * @v);
        internal static glProgramNamedParameter4fvNVFunc glProgramNamedParameter4fvNVPtr;
        internal static void loadProgramNamedParameter4fvNV()
        {
            try
            {
                glProgramNamedParameter4fvNVPtr = (glProgramNamedParameter4fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramNamedParameter4fvNV"), typeof(glProgramNamedParameter4fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramNamedParameter4fvNV'.");
            }
        }
        public static void glProgramNamedParameter4fvNV(GLuint @id, GLsizei @len, const GLubyte * @name, const GLfloat * @v) => glProgramNamedParameter4fvNVPtr(@id, @len, @name, @v);

        internal delegate void glProgramNamedParameter4dNVFunc(GLuint @id, GLsizei @len, const GLubyte * @name, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glProgramNamedParameter4dNVFunc glProgramNamedParameter4dNVPtr;
        internal static void loadProgramNamedParameter4dNV()
        {
            try
            {
                glProgramNamedParameter4dNVPtr = (glProgramNamedParameter4dNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramNamedParameter4dNV"), typeof(glProgramNamedParameter4dNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramNamedParameter4dNV'.");
            }
        }
        public static void glProgramNamedParameter4dNV(GLuint @id, GLsizei @len, const GLubyte * @name, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glProgramNamedParameter4dNVPtr(@id, @len, @name, @x, @y, @z, @w);

        internal delegate void glProgramNamedParameter4dvNVFunc(GLuint @id, GLsizei @len, const GLubyte * @name, const GLdouble * @v);
        internal static glProgramNamedParameter4dvNVFunc glProgramNamedParameter4dvNVPtr;
        internal static void loadProgramNamedParameter4dvNV()
        {
            try
            {
                glProgramNamedParameter4dvNVPtr = (glProgramNamedParameter4dvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramNamedParameter4dvNV"), typeof(glProgramNamedParameter4dvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramNamedParameter4dvNV'.");
            }
        }
        public static void glProgramNamedParameter4dvNV(GLuint @id, GLsizei @len, const GLubyte * @name, const GLdouble * @v) => glProgramNamedParameter4dvNVPtr(@id, @len, @name, @v);

        internal delegate void glGetProgramNamedParameterfvNVFunc(GLuint @id, GLsizei @len, const GLubyte * @name, GLfloat * @params);
        internal static glGetProgramNamedParameterfvNVFunc glGetProgramNamedParameterfvNVPtr;
        internal static void loadGetProgramNamedParameterfvNV()
        {
            try
            {
                glGetProgramNamedParameterfvNVPtr = (glGetProgramNamedParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramNamedParameterfvNV"), typeof(glGetProgramNamedParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramNamedParameterfvNV'.");
            }
        }
        public static void glGetProgramNamedParameterfvNV(GLuint @id, GLsizei @len, const GLubyte * @name, GLfloat * @params) => glGetProgramNamedParameterfvNVPtr(@id, @len, @name, @params);

        internal delegate void glGetProgramNamedParameterdvNVFunc(GLuint @id, GLsizei @len, const GLubyte * @name, GLdouble * @params);
        internal static glGetProgramNamedParameterdvNVFunc glGetProgramNamedParameterdvNVPtr;
        internal static void loadGetProgramNamedParameterdvNV()
        {
            try
            {
                glGetProgramNamedParameterdvNVPtr = (glGetProgramNamedParameterdvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramNamedParameterdvNV"), typeof(glGetProgramNamedParameterdvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramNamedParameterdvNV'.");
            }
        }
        public static void glGetProgramNamedParameterdvNV(GLuint @id, GLsizei @len, const GLubyte * @name, GLdouble * @params) => glGetProgramNamedParameterdvNVPtr(@id, @len, @name, @params);
        #endregion
    }
}
