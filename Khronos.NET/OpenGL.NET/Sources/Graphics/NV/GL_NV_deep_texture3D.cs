using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_deep_texture3D
    {
        #region Interop
        static GL_NV_deep_texture3D()
        {
            Console.WriteLine("Initalising GL_NV_deep_texture3D interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_DEEP_3D_TEXTURE_WIDTH_HEIGHT_NV = 0x90D0;
        public static UInt32 GL_MAX_DEEP_3D_TEXTURE_DEPTH_NV = 0x90D1;
        #endregion

        #region Commands
        #endregion
    }
}
