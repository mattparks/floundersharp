using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_register_combiners
    {
        #region Interop
        static GL_NV_register_combiners()
        {
            Console.WriteLine("Initalising GL_NV_register_combiners interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCombinerParameterfvNV();
            loadCombinerParameterfNV();
            loadCombinerParameterivNV();
            loadCombinerParameteriNV();
            loadCombinerInputNV();
            loadCombinerOutputNV();
            loadFinalCombinerInputNV();
            loadGetCombinerInputParameterfvNV();
            loadGetCombinerInputParameterivNV();
            loadGetCombinerOutputParameterfvNV();
            loadGetCombinerOutputParameterivNV();
            loadGetFinalCombinerInputParameterfvNV();
            loadGetFinalCombinerInputParameterivNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_REGISTER_COMBINERS_NV = 0x8522;
        public static UInt32 GL_VARIABLE_A_NV = 0x8523;
        public static UInt32 GL_VARIABLE_B_NV = 0x8524;
        public static UInt32 GL_VARIABLE_C_NV = 0x8525;
        public static UInt32 GL_VARIABLE_D_NV = 0x8526;
        public static UInt32 GL_VARIABLE_E_NV = 0x8527;
        public static UInt32 GL_VARIABLE_F_NV = 0x8528;
        public static UInt32 GL_VARIABLE_G_NV = 0x8529;
        public static UInt32 GL_CONSTANT_COLOR0_NV = 0x852A;
        public static UInt32 GL_CONSTANT_COLOR1_NV = 0x852B;
        public static UInt32 GL_PRIMARY_COLOR_NV = 0x852C;
        public static UInt32 GL_SECONDARY_COLOR_NV = 0x852D;
        public static UInt32 GL_SPARE0_NV = 0x852E;
        public static UInt32 GL_SPARE1_NV = 0x852F;
        public static UInt32 GL_DISCARD_NV = 0x8530;
        public static UInt32 GL_E_TIMES_F_NV = 0x8531;
        public static UInt32 GL_SPARE0_PLUS_SECONDARY_COLOR_NV = 0x8532;
        public static UInt32 GL_UNSIGNED_IDENTITY_NV = 0x8536;
        public static UInt32 GL_UNSIGNED_INVERT_NV = 0x8537;
        public static UInt32 GL_EXPAND_NORMAL_NV = 0x8538;
        public static UInt32 GL_EXPAND_NEGATE_NV = 0x8539;
        public static UInt32 GL_HALF_BIAS_NORMAL_NV = 0x853A;
        public static UInt32 GL_HALF_BIAS_NEGATE_NV = 0x853B;
        public static UInt32 GL_SIGNED_IDENTITY_NV = 0x853C;
        public static UInt32 GL_SIGNED_NEGATE_NV = 0x853D;
        public static UInt32 GL_SCALE_BY_TWO_NV = 0x853E;
        public static UInt32 GL_SCALE_BY_FOUR_NV = 0x853F;
        public static UInt32 GL_SCALE_BY_ONE_HALF_NV = 0x8540;
        public static UInt32 GL_BIAS_BY_NEGATIVE_ONE_HALF_NV = 0x8541;
        public static UInt32 GL_COMBINER_INPUT_NV = 0x8542;
        public static UInt32 GL_COMBINER_MAPPING_NV = 0x8543;
        public static UInt32 GL_COMBINER_COMPONENT_USAGE_NV = 0x8544;
        public static UInt32 GL_COMBINER_AB_DOT_PRODUCT_NV = 0x8545;
        public static UInt32 GL_COMBINER_CD_DOT_PRODUCT_NV = 0x8546;
        public static UInt32 GL_COMBINER_MUX_SUM_NV = 0x8547;
        public static UInt32 GL_COMBINER_SCALE_NV = 0x8548;
        public static UInt32 GL_COMBINER_BIAS_NV = 0x8549;
        public static UInt32 GL_COMBINER_AB_OUTPUT_NV = 0x854A;
        public static UInt32 GL_COMBINER_CD_OUTPUT_NV = 0x854B;
        public static UInt32 GL_COMBINER_SUM_OUTPUT_NV = 0x854C;
        public static UInt32 GL_MAX_GENERAL_COMBINERS_NV = 0x854D;
        public static UInt32 GL_NUM_GENERAL_COMBINERS_NV = 0x854E;
        public static UInt32 GL_COLOR_SUM_CLAMP_NV = 0x854F;
        public static UInt32 GL_COMBINER0_NV = 0x8550;
        public static UInt32 GL_COMBINER1_NV = 0x8551;
        public static UInt32 GL_COMBINER2_NV = 0x8552;
        public static UInt32 GL_COMBINER3_NV = 0x8553;
        public static UInt32 GL_COMBINER4_NV = 0x8554;
        public static UInt32 GL_COMBINER5_NV = 0x8555;
        public static UInt32 GL_COMBINER6_NV = 0x8556;
        public static UInt32 GL_COMBINER7_NV = 0x8557;
        public static UInt32 GL_TEXTURE0_ARB = 0x84C0;
        public static UInt32 GL_TEXTURE1_ARB = 0x84C1;
        public static UInt32 GL_ZERO = 0;
        public static UInt32 GL_NONE = 0;
        public static UInt32 GL_FOG = 0x0B60;
        #endregion

        #region Commands
        internal delegate void glCombinerParameterfvNVFunc(GLenum @pname, const GLfloat * @params);
        internal static glCombinerParameterfvNVFunc glCombinerParameterfvNVPtr;
        internal static void loadCombinerParameterfvNV()
        {
            try
            {
                glCombinerParameterfvNVPtr = (glCombinerParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCombinerParameterfvNV"), typeof(glCombinerParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCombinerParameterfvNV'.");
            }
        }
        public static void glCombinerParameterfvNV(GLenum @pname, const GLfloat * @params) => glCombinerParameterfvNVPtr(@pname, @params);

        internal delegate void glCombinerParameterfNVFunc(GLenum @pname, GLfloat @param);
        internal static glCombinerParameterfNVFunc glCombinerParameterfNVPtr;
        internal static void loadCombinerParameterfNV()
        {
            try
            {
                glCombinerParameterfNVPtr = (glCombinerParameterfNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCombinerParameterfNV"), typeof(glCombinerParameterfNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCombinerParameterfNV'.");
            }
        }
        public static void glCombinerParameterfNV(GLenum @pname, GLfloat @param) => glCombinerParameterfNVPtr(@pname, @param);

        internal delegate void glCombinerParameterivNVFunc(GLenum @pname, const GLint * @params);
        internal static glCombinerParameterivNVFunc glCombinerParameterivNVPtr;
        internal static void loadCombinerParameterivNV()
        {
            try
            {
                glCombinerParameterivNVPtr = (glCombinerParameterivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCombinerParameterivNV"), typeof(glCombinerParameterivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCombinerParameterivNV'.");
            }
        }
        public static void glCombinerParameterivNV(GLenum @pname, const GLint * @params) => glCombinerParameterivNVPtr(@pname, @params);

        internal delegate void glCombinerParameteriNVFunc(GLenum @pname, GLint @param);
        internal static glCombinerParameteriNVFunc glCombinerParameteriNVPtr;
        internal static void loadCombinerParameteriNV()
        {
            try
            {
                glCombinerParameteriNVPtr = (glCombinerParameteriNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCombinerParameteriNV"), typeof(glCombinerParameteriNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCombinerParameteriNV'.");
            }
        }
        public static void glCombinerParameteriNV(GLenum @pname, GLint @param) => glCombinerParameteriNVPtr(@pname, @param);

        internal delegate void glCombinerInputNVFunc(GLenum @stage, GLenum @portion, GLenum @variable, GLenum @input, GLenum @mapping, GLenum @componentUsage);
        internal static glCombinerInputNVFunc glCombinerInputNVPtr;
        internal static void loadCombinerInputNV()
        {
            try
            {
                glCombinerInputNVPtr = (glCombinerInputNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCombinerInputNV"), typeof(glCombinerInputNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCombinerInputNV'.");
            }
        }
        public static void glCombinerInputNV(GLenum @stage, GLenum @portion, GLenum @variable, GLenum @input, GLenum @mapping, GLenum @componentUsage) => glCombinerInputNVPtr(@stage, @portion, @variable, @input, @mapping, @componentUsage);

        internal delegate void glCombinerOutputNVFunc(GLenum @stage, GLenum @portion, GLenum @abOutput, GLenum @cdOutput, GLenum @sumOutput, GLenum @scale, GLenum @bias, GLboolean @abDotProduct, GLboolean @cdDotProduct, GLboolean @muxSum);
        internal static glCombinerOutputNVFunc glCombinerOutputNVPtr;
        internal static void loadCombinerOutputNV()
        {
            try
            {
                glCombinerOutputNVPtr = (glCombinerOutputNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCombinerOutputNV"), typeof(glCombinerOutputNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCombinerOutputNV'.");
            }
        }
        public static void glCombinerOutputNV(GLenum @stage, GLenum @portion, GLenum @abOutput, GLenum @cdOutput, GLenum @sumOutput, GLenum @scale, GLenum @bias, GLboolean @abDotProduct, GLboolean @cdDotProduct, GLboolean @muxSum) => glCombinerOutputNVPtr(@stage, @portion, @abOutput, @cdOutput, @sumOutput, @scale, @bias, @abDotProduct, @cdDotProduct, @muxSum);

        internal delegate void glFinalCombinerInputNVFunc(GLenum @variable, GLenum @input, GLenum @mapping, GLenum @componentUsage);
        internal static glFinalCombinerInputNVFunc glFinalCombinerInputNVPtr;
        internal static void loadFinalCombinerInputNV()
        {
            try
            {
                glFinalCombinerInputNVPtr = (glFinalCombinerInputNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFinalCombinerInputNV"), typeof(glFinalCombinerInputNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFinalCombinerInputNV'.");
            }
        }
        public static void glFinalCombinerInputNV(GLenum @variable, GLenum @input, GLenum @mapping, GLenum @componentUsage) => glFinalCombinerInputNVPtr(@variable, @input, @mapping, @componentUsage);

        internal delegate void glGetCombinerInputParameterfvNVFunc(GLenum @stage, GLenum @portion, GLenum @variable, GLenum @pname, GLfloat * @params);
        internal static glGetCombinerInputParameterfvNVFunc glGetCombinerInputParameterfvNVPtr;
        internal static void loadGetCombinerInputParameterfvNV()
        {
            try
            {
                glGetCombinerInputParameterfvNVPtr = (glGetCombinerInputParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCombinerInputParameterfvNV"), typeof(glGetCombinerInputParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCombinerInputParameterfvNV'.");
            }
        }
        public static void glGetCombinerInputParameterfvNV(GLenum @stage, GLenum @portion, GLenum @variable, GLenum @pname, GLfloat * @params) => glGetCombinerInputParameterfvNVPtr(@stage, @portion, @variable, @pname, @params);

        internal delegate void glGetCombinerInputParameterivNVFunc(GLenum @stage, GLenum @portion, GLenum @variable, GLenum @pname, GLint * @params);
        internal static glGetCombinerInputParameterivNVFunc glGetCombinerInputParameterivNVPtr;
        internal static void loadGetCombinerInputParameterivNV()
        {
            try
            {
                glGetCombinerInputParameterivNVPtr = (glGetCombinerInputParameterivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCombinerInputParameterivNV"), typeof(glGetCombinerInputParameterivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCombinerInputParameterivNV'.");
            }
        }
        public static void glGetCombinerInputParameterivNV(GLenum @stage, GLenum @portion, GLenum @variable, GLenum @pname, GLint * @params) => glGetCombinerInputParameterivNVPtr(@stage, @portion, @variable, @pname, @params);

        internal delegate void glGetCombinerOutputParameterfvNVFunc(GLenum @stage, GLenum @portion, GLenum @pname, GLfloat * @params);
        internal static glGetCombinerOutputParameterfvNVFunc glGetCombinerOutputParameterfvNVPtr;
        internal static void loadGetCombinerOutputParameterfvNV()
        {
            try
            {
                glGetCombinerOutputParameterfvNVPtr = (glGetCombinerOutputParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCombinerOutputParameterfvNV"), typeof(glGetCombinerOutputParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCombinerOutputParameterfvNV'.");
            }
        }
        public static void glGetCombinerOutputParameterfvNV(GLenum @stage, GLenum @portion, GLenum @pname, GLfloat * @params) => glGetCombinerOutputParameterfvNVPtr(@stage, @portion, @pname, @params);

        internal delegate void glGetCombinerOutputParameterivNVFunc(GLenum @stage, GLenum @portion, GLenum @pname, GLint * @params);
        internal static glGetCombinerOutputParameterivNVFunc glGetCombinerOutputParameterivNVPtr;
        internal static void loadGetCombinerOutputParameterivNV()
        {
            try
            {
                glGetCombinerOutputParameterivNVPtr = (glGetCombinerOutputParameterivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCombinerOutputParameterivNV"), typeof(glGetCombinerOutputParameterivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCombinerOutputParameterivNV'.");
            }
        }
        public static void glGetCombinerOutputParameterivNV(GLenum @stage, GLenum @portion, GLenum @pname, GLint * @params) => glGetCombinerOutputParameterivNVPtr(@stage, @portion, @pname, @params);

        internal delegate void glGetFinalCombinerInputParameterfvNVFunc(GLenum @variable, GLenum @pname, GLfloat * @params);
        internal static glGetFinalCombinerInputParameterfvNVFunc glGetFinalCombinerInputParameterfvNVPtr;
        internal static void loadGetFinalCombinerInputParameterfvNV()
        {
            try
            {
                glGetFinalCombinerInputParameterfvNVPtr = (glGetFinalCombinerInputParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFinalCombinerInputParameterfvNV"), typeof(glGetFinalCombinerInputParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFinalCombinerInputParameterfvNV'.");
            }
        }
        public static void glGetFinalCombinerInputParameterfvNV(GLenum @variable, GLenum @pname, GLfloat * @params) => glGetFinalCombinerInputParameterfvNVPtr(@variable, @pname, @params);

        internal delegate void glGetFinalCombinerInputParameterivNVFunc(GLenum @variable, GLenum @pname, GLint * @params);
        internal static glGetFinalCombinerInputParameterivNVFunc glGetFinalCombinerInputParameterivNVPtr;
        internal static void loadGetFinalCombinerInputParameterivNV()
        {
            try
            {
                glGetFinalCombinerInputParameterivNVPtr = (glGetFinalCombinerInputParameterivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFinalCombinerInputParameterivNV"), typeof(glGetFinalCombinerInputParameterivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFinalCombinerInputParameterivNV'.");
            }
        }
        public static void glGetFinalCombinerInputParameterivNV(GLenum @variable, GLenum @pname, GLint * @params) => glGetFinalCombinerInputParameterivNVPtr(@variable, @pname, @params);
        #endregion
    }
}
