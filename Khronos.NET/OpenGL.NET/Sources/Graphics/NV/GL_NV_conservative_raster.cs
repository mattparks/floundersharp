using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_conservative_raster
    {
        #region Interop
        static GL_NV_conservative_raster()
        {
            Console.WriteLine("Initalising GL_NV_conservative_raster interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadSubpixelPrecisionBiasNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_CONSERVATIVE_RASTERIZATION_NV = 0x9346;
        public static UInt32 GL_SUBPIXEL_PRECISION_BIAS_X_BITS_NV = 0x9347;
        public static UInt32 GL_SUBPIXEL_PRECISION_BIAS_Y_BITS_NV = 0x9348;
        public static UInt32 GL_MAX_SUBPIXEL_PRECISION_BIAS_BITS_NV = 0x9349;
        #endregion

        #region Commands
        internal delegate void glSubpixelPrecisionBiasNVFunc(GLuint @xbits, GLuint @ybits);
        internal static glSubpixelPrecisionBiasNVFunc glSubpixelPrecisionBiasNVPtr;
        internal static void loadSubpixelPrecisionBiasNV()
        {
            try
            {
                glSubpixelPrecisionBiasNVPtr = (glSubpixelPrecisionBiasNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSubpixelPrecisionBiasNV"), typeof(glSubpixelPrecisionBiasNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSubpixelPrecisionBiasNV'.");
            }
        }
        public static void glSubpixelPrecisionBiasNV(GLuint @xbits, GLuint @ybits) => glSubpixelPrecisionBiasNVPtr(@xbits, @ybits);
        #endregion
    }
}
