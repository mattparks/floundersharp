using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_shadow_samplers_array
    {
        #region Interop
        static GL_NV_shadow_samplers_array()
        {
            Console.WriteLine("Initalising GL_NV_shadow_samplers_array interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SAMPLER_2D_ARRAY_SHADOW_NV = 0x8DC4;
        #endregion

        #region Commands
        #endregion
    }
}
