using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_depth_nonlinear
    {
        #region Interop
        static GL_NV_depth_nonlinear()
        {
            Console.WriteLine("Initalising GL_NV_depth_nonlinear interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_COMPONENT16_NONLINEAR_NV = 0x8E2C;
        #endregion

        #region Commands
        #endregion
    }
}
