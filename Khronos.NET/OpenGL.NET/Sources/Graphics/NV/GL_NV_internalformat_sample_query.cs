using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_internalformat_sample_query
    {
        #region Interop
        static GL_NV_internalformat_sample_query()
        {
            Console.WriteLine("Initalising GL_NV_internalformat_sample_query interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetInternalformatSampleivNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RENDERBUFFER = 0x8D41;
        public static UInt32 GL_TEXTURE_2D_MULTISAMPLE = 0x9100;
        public static UInt32 GL_TEXTURE_2D_MULTISAMPLE_ARRAY = 0x9102;
        public static UInt32 GL_MULTISAMPLES_NV = 0x9371;
        public static UInt32 GL_SUPERSAMPLE_SCALE_X_NV = 0x9372;
        public static UInt32 GL_SUPERSAMPLE_SCALE_Y_NV = 0x9373;
        public static UInt32 GL_CONFORMANT_NV = 0x9374;
        #endregion

        #region Commands
        internal delegate void glGetInternalformatSampleivNVFunc(GLenum @target, GLenum @internalformat, GLsizei @samples, GLenum @pname, GLsizei @bufSize, GLint * @params);
        internal static glGetInternalformatSampleivNVFunc glGetInternalformatSampleivNVPtr;
        internal static void loadGetInternalformatSampleivNV()
        {
            try
            {
                glGetInternalformatSampleivNVPtr = (glGetInternalformatSampleivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInternalformatSampleivNV"), typeof(glGetInternalformatSampleivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInternalformatSampleivNV'.");
            }
        }
        public static void glGetInternalformatSampleivNV(GLenum @target, GLenum @internalformat, GLsizei @samples, GLenum @pname, GLsizei @bufSize, GLint * @params) => glGetInternalformatSampleivNVPtr(@target, @internalformat, @samples, @pname, @bufSize, @params);
        #endregion
    }
}
