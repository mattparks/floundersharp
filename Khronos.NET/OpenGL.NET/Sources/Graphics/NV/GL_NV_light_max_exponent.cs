using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_light_max_exponent
    {
        #region Interop
        static GL_NV_light_max_exponent()
        {
            Console.WriteLine("Initalising GL_NV_light_max_exponent interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_SHININESS_NV = 0x8504;
        public static UInt32 GL_MAX_SPOT_EXPONENT_NV = 0x8505;
        #endregion

        #region Commands
        #endregion
    }
}
