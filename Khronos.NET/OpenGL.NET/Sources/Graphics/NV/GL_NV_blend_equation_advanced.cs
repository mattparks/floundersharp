using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_blend_equation_advanced
    {
        #region Interop
        static GL_NV_blend_equation_advanced()
        {
            Console.WriteLine("Initalising GL_NV_blend_equation_advanced interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendParameteriNV();
            loadBlendBarrierNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BLEND_OVERLAP_NV = 0x9281;
        public static UInt32 GL_BLEND_PREMULTIPLIED_SRC_NV = 0x9280;
        public static UInt32 GL_BLUE_NV = 0x1905;
        public static UInt32 GL_COLORBURN_NV = 0x929A;
        public static UInt32 GL_COLORDODGE_NV = 0x9299;
        public static UInt32 GL_CONJOINT_NV = 0x9284;
        public static UInt32 GL_CONTRAST_NV = 0x92A1;
        public static UInt32 GL_DARKEN_NV = 0x9297;
        public static UInt32 GL_DIFFERENCE_NV = 0x929E;
        public static UInt32 GL_DISJOINT_NV = 0x9283;
        public static UInt32 GL_DST_ATOP_NV = 0x928F;
        public static UInt32 GL_DST_IN_NV = 0x928B;
        public static UInt32 GL_DST_NV = 0x9287;
        public static UInt32 GL_DST_OUT_NV = 0x928D;
        public static UInt32 GL_DST_OVER_NV = 0x9289;
        public static UInt32 GL_EXCLUSION_NV = 0x92A0;
        public static UInt32 GL_GREEN_NV = 0x1904;
        public static UInt32 GL_HARDLIGHT_NV = 0x929B;
        public static UInt32 GL_HARDMIX_NV = 0x92A9;
        public static UInt32 GL_HSL_COLOR_NV = 0x92AF;
        public static UInt32 GL_HSL_HUE_NV = 0x92AD;
        public static UInt32 GL_HSL_LUMINOSITY_NV = 0x92B0;
        public static UInt32 GL_HSL_SATURATION_NV = 0x92AE;
        public static UInt32 GL_INVERT = 0x150A;
        public static UInt32 GL_INVERT_OVG_NV = 0x92B4;
        public static UInt32 GL_INVERT_RGB_NV = 0x92A3;
        public static UInt32 GL_LIGHTEN_NV = 0x9298;
        public static UInt32 GL_LINEARBURN_NV = 0x92A5;
        public static UInt32 GL_LINEARDODGE_NV = 0x92A4;
        public static UInt32 GL_LINEARLIGHT_NV = 0x92A7;
        public static UInt32 GL_MINUS_CLAMPED_NV = 0x92B3;
        public static UInt32 GL_MINUS_NV = 0x929F;
        public static UInt32 GL_MULTIPLY_NV = 0x9294;
        public static UInt32 GL_OVERLAY_NV = 0x9296;
        public static UInt32 GL_PINLIGHT_NV = 0x92A8;
        public static UInt32 GL_PLUS_CLAMPED_ALPHA_NV = 0x92B2;
        public static UInt32 GL_PLUS_CLAMPED_NV = 0x92B1;
        public static UInt32 GL_PLUS_DARKER_NV = 0x9292;
        public static UInt32 GL_PLUS_NV = 0x9291;
        public static UInt32 GL_RED_NV = 0x1903;
        public static UInt32 GL_SCREEN_NV = 0x9295;
        public static UInt32 GL_SOFTLIGHT_NV = 0x929C;
        public static UInt32 GL_SRC_ATOP_NV = 0x928E;
        public static UInt32 GL_SRC_IN_NV = 0x928A;
        public static UInt32 GL_SRC_NV = 0x9286;
        public static UInt32 GL_SRC_OUT_NV = 0x928C;
        public static UInt32 GL_SRC_OVER_NV = 0x9288;
        public static UInt32 GL_UNCORRELATED_NV = 0x9282;
        public static UInt32 GL_VIVIDLIGHT_NV = 0x92A6;
        public static UInt32 GL_XOR_NV = 0x1506;
        public static UInt32 GL_ZERO = 0;
        #endregion

        #region Commands
        internal delegate void glBlendParameteriNVFunc(GLenum @pname, GLint @value);
        internal static glBlendParameteriNVFunc glBlendParameteriNVPtr;
        internal static void loadBlendParameteriNV()
        {
            try
            {
                glBlendParameteriNVPtr = (glBlendParameteriNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendParameteriNV"), typeof(glBlendParameteriNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendParameteriNV'.");
            }
        }
        public static void glBlendParameteriNV(GLenum @pname, GLint @value) => glBlendParameteriNVPtr(@pname, @value);

        internal delegate void glBlendBarrierNVFunc();
        internal static glBlendBarrierNVFunc glBlendBarrierNVPtr;
        internal static void loadBlendBarrierNV()
        {
            try
            {
                glBlendBarrierNVPtr = (glBlendBarrierNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendBarrierNV"), typeof(glBlendBarrierNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendBarrierNV'.");
            }
        }
        public static void glBlendBarrierNV() => glBlendBarrierNVPtr();
        #endregion
    }
}
