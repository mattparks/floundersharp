using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_copy_depth_to_color
    {
        #region Interop
        static GL_NV_copy_depth_to_color()
        {
            Console.WriteLine("Initalising GL_NV_copy_depth_to_color interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_STENCIL_TO_RGBA_NV = 0x886E;
        public static UInt32 GL_DEPTH_STENCIL_TO_BGRA_NV = 0x886F;
        #endregion

        #region Commands
        #endregion
    }
}
