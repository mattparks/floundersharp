using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_fog_distance
    {
        #region Interop
        static GL_NV_fog_distance()
        {
            Console.WriteLine("Initalising GL_NV_fog_distance interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FOG_DISTANCE_MODE_NV = 0x855A;
        public static UInt32 GL_EYE_RADIAL_NV = 0x855B;
        public static UInt32 GL_EYE_PLANE_ABSOLUTE_NV = 0x855C;
        public static UInt32 GL_EYE_PLANE = 0x2502;
        #endregion

        #region Commands
        #endregion
    }
}
