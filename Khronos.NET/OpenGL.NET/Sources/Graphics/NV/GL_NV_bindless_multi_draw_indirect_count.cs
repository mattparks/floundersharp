using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_bindless_multi_draw_indirect_count
    {
        #region Interop
        static GL_NV_bindless_multi_draw_indirect_count()
        {
            Console.WriteLine("Initalising GL_NV_bindless_multi_draw_indirect_count interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMultiDrawArraysIndirectBindlessCountNV();
            loadMultiDrawElementsIndirectBindlessCountNV();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glMultiDrawArraysIndirectBindlessCountNVFunc(GLenum @mode, const void * @indirect, GLsizei @drawCount, GLsizei @maxDrawCount, GLsizei @stride, GLint @vertexBufferCount);
        internal static glMultiDrawArraysIndirectBindlessCountNVFunc glMultiDrawArraysIndirectBindlessCountNVPtr;
        internal static void loadMultiDrawArraysIndirectBindlessCountNV()
        {
            try
            {
                glMultiDrawArraysIndirectBindlessCountNVPtr = (glMultiDrawArraysIndirectBindlessCountNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawArraysIndirectBindlessCountNV"), typeof(glMultiDrawArraysIndirectBindlessCountNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawArraysIndirectBindlessCountNV'.");
            }
        }
        public static void glMultiDrawArraysIndirectBindlessCountNV(GLenum @mode, const void * @indirect, GLsizei @drawCount, GLsizei @maxDrawCount, GLsizei @stride, GLint @vertexBufferCount) => glMultiDrawArraysIndirectBindlessCountNVPtr(@mode, @indirect, @drawCount, @maxDrawCount, @stride, @vertexBufferCount);

        internal delegate void glMultiDrawElementsIndirectBindlessCountNVFunc(GLenum @mode, GLenum @type, const void * @indirect, GLsizei @drawCount, GLsizei @maxDrawCount, GLsizei @stride, GLint @vertexBufferCount);
        internal static glMultiDrawElementsIndirectBindlessCountNVFunc glMultiDrawElementsIndirectBindlessCountNVPtr;
        internal static void loadMultiDrawElementsIndirectBindlessCountNV()
        {
            try
            {
                glMultiDrawElementsIndirectBindlessCountNVPtr = (glMultiDrawElementsIndirectBindlessCountNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementsIndirectBindlessCountNV"), typeof(glMultiDrawElementsIndirectBindlessCountNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementsIndirectBindlessCountNV'.");
            }
        }
        public static void glMultiDrawElementsIndirectBindlessCountNV(GLenum @mode, GLenum @type, const void * @indirect, GLsizei @drawCount, GLsizei @maxDrawCount, GLsizei @stride, GLint @vertexBufferCount) => glMultiDrawElementsIndirectBindlessCountNVPtr(@mode, @type, @indirect, @drawCount, @maxDrawCount, @stride, @vertexBufferCount);
        #endregion
    }
}
