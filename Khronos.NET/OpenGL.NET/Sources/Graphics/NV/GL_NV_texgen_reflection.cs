using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_texgen_reflection
    {
        #region Interop
        static GL_NV_texgen_reflection()
        {
            Console.WriteLine("Initalising GL_NV_texgen_reflection interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_NORMAL_MAP_NV = 0x8511;
        public static UInt32 GL_REFLECTION_MAP_NV = 0x8512;
        #endregion

        #region Commands
        #endregion
    }
}
