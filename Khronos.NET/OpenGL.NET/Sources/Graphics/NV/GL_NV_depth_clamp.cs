using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_depth_clamp
    {
        #region Interop
        static GL_NV_depth_clamp()
        {
            Console.WriteLine("Initalising GL_NV_depth_clamp interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_CLAMP_NV = 0x864F;
        #endregion

        #region Commands
        #endregion
    }
}
