using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_uniform_buffer_unified_memory
    {
        #region Interop
        static GL_NV_uniform_buffer_unified_memory()
        {
            Console.WriteLine("Initalising GL_NV_uniform_buffer_unified_memory interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_UNIFORM_BUFFER_UNIFIED_NV = 0x936E;
        public static UInt32 GL_UNIFORM_BUFFER_ADDRESS_NV = 0x936F;
        public static UInt32 GL_UNIFORM_BUFFER_LENGTH_NV = 0x9370;
        #endregion

        #region Commands
        #endregion
    }
}
