using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_polygon_mode
    {
        #region Interop
        static GL_NV_polygon_mode()
        {
            Console.WriteLine("Initalising GL_NV_polygon_mode interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPolygonModeNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_POLYGON_MODE_NV = 0x0B40;
        public static UInt32 GL_POLYGON_OFFSET_POINT_NV = 0x2A01;
        public static UInt32 GL_POLYGON_OFFSET_LINE_NV = 0x2A02;
        public static UInt32 GL_POINT_NV = 0x1B00;
        public static UInt32 GL_LINE_NV = 0x1B01;
        public static UInt32 GL_FILL_NV = 0x1B02;
        #endregion

        #region Commands
        internal delegate void glPolygonModeNVFunc(GLenum @face, GLenum @mode);
        internal static glPolygonModeNVFunc glPolygonModeNVPtr;
        internal static void loadPolygonModeNV()
        {
            try
            {
                glPolygonModeNVPtr = (glPolygonModeNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPolygonModeNV"), typeof(glPolygonModeNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPolygonModeNV'.");
            }
        }
        public static void glPolygonModeNV(GLenum @face, GLenum @mode) => glPolygonModeNVPtr(@face, @mode);
        #endregion
    }
}
