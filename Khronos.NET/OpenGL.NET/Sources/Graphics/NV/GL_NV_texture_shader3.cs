using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_texture_shader3
    {
        #region Interop
        static GL_NV_texture_shader3()
        {
            Console.WriteLine("Initalising GL_NV_texture_shader3 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_OFFSET_PROJECTIVE_TEXTURE_2D_NV = 0x8850;
        public static UInt32 GL_OFFSET_PROJECTIVE_TEXTURE_2D_SCALE_NV = 0x8851;
        public static UInt32 GL_OFFSET_PROJECTIVE_TEXTURE_RECTANGLE_NV = 0x8852;
        public static UInt32 GL_OFFSET_PROJECTIVE_TEXTURE_RECTANGLE_SCALE_NV = 0x8853;
        public static UInt32 GL_OFFSET_HILO_TEXTURE_2D_NV = 0x8854;
        public static UInt32 GL_OFFSET_HILO_TEXTURE_RECTANGLE_NV = 0x8855;
        public static UInt32 GL_OFFSET_HILO_PROJECTIVE_TEXTURE_2D_NV = 0x8856;
        public static UInt32 GL_OFFSET_HILO_PROJECTIVE_TEXTURE_RECTANGLE_NV = 0x8857;
        public static UInt32 GL_DEPENDENT_HILO_TEXTURE_2D_NV = 0x8858;
        public static UInt32 GL_DEPENDENT_RGB_TEXTURE_3D_NV = 0x8859;
        public static UInt32 GL_DEPENDENT_RGB_TEXTURE_CUBE_MAP_NV = 0x885A;
        public static UInt32 GL_DOT_PRODUCT_PASS_THROUGH_NV = 0x885B;
        public static UInt32 GL_DOT_PRODUCT_TEXTURE_1D_NV = 0x885C;
        public static UInt32 GL_DOT_PRODUCT_AFFINE_DEPTH_REPLACE_NV = 0x885D;
        public static UInt32 GL_HILO8_NV = 0x885E;
        public static UInt32 GL_SIGNED_HILO8_NV = 0x885F;
        public static UInt32 GL_FORCE_BLUE_TO_ONE_NV = 0x8860;
        #endregion

        #region Commands
        #endregion
    }
}
