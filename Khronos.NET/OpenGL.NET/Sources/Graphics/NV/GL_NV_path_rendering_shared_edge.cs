using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_path_rendering_shared_edge
    {
        #region Interop
        static GL_NV_path_rendering_shared_edge()
        {
            Console.WriteLine("Initalising GL_NV_path_rendering_shared_edge interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SHARED_EDGE_NV = 0xC0;
        #endregion

        #region Commands
        #endregion
    }
}
