using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_texture_expand_normal
    {
        #region Interop
        static GL_NV_texture_expand_normal()
        {
            Console.WriteLine("Initalising GL_NV_texture_expand_normal interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_UNSIGNED_REMAP_MODE_NV = 0x888F;
        #endregion

        #region Commands
        #endregion
    }
}
