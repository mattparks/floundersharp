using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_present_video
    {
        #region Interop
        static GL_NV_present_video()
        {
            Console.WriteLine("Initalising GL_NV_present_video interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPresentFrameKeyedNV();
            loadPresentFrameDualFillNV();
            loadGetVideoivNV();
            loadGetVideouivNV();
            loadGetVideoi64vNV();
            loadGetVideoui64vNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAME_NV = 0x8E26;
        public static UInt32 GL_FIELDS_NV = 0x8E27;
        public static UInt32 GL_CURRENT_TIME_NV = 0x8E28;
        public static UInt32 GL_NUM_FILL_STREAMS_NV = 0x8E29;
        public static UInt32 GL_PRESENT_TIME_NV = 0x8E2A;
        public static UInt32 GL_PRESENT_DURATION_NV = 0x8E2B;
        #endregion

        #region Commands
        internal delegate void glPresentFrameKeyedNVFunc(GLuint @video_slot, GLuint64EXT @minPresentTime, GLuint @beginPresentTimeId, GLuint @presentDurationId, GLenum @type, GLenum @target0, GLuint @fill0, GLuint @key0, GLenum @target1, GLuint @fill1, GLuint @key1);
        internal static glPresentFrameKeyedNVFunc glPresentFrameKeyedNVPtr;
        internal static void loadPresentFrameKeyedNV()
        {
            try
            {
                glPresentFrameKeyedNVPtr = (glPresentFrameKeyedNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPresentFrameKeyedNV"), typeof(glPresentFrameKeyedNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPresentFrameKeyedNV'.");
            }
        }
        public static void glPresentFrameKeyedNV(GLuint @video_slot, GLuint64EXT @minPresentTime, GLuint @beginPresentTimeId, GLuint @presentDurationId, GLenum @type, GLenum @target0, GLuint @fill0, GLuint @key0, GLenum @target1, GLuint @fill1, GLuint @key1) => glPresentFrameKeyedNVPtr(@video_slot, @minPresentTime, @beginPresentTimeId, @presentDurationId, @type, @target0, @fill0, @key0, @target1, @fill1, @key1);

        internal delegate void glPresentFrameDualFillNVFunc(GLuint @video_slot, GLuint64EXT @minPresentTime, GLuint @beginPresentTimeId, GLuint @presentDurationId, GLenum @type, GLenum @target0, GLuint @fill0, GLenum @target1, GLuint @fill1, GLenum @target2, GLuint @fill2, GLenum @target3, GLuint @fill3);
        internal static glPresentFrameDualFillNVFunc glPresentFrameDualFillNVPtr;
        internal static void loadPresentFrameDualFillNV()
        {
            try
            {
                glPresentFrameDualFillNVPtr = (glPresentFrameDualFillNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPresentFrameDualFillNV"), typeof(glPresentFrameDualFillNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPresentFrameDualFillNV'.");
            }
        }
        public static void glPresentFrameDualFillNV(GLuint @video_slot, GLuint64EXT @minPresentTime, GLuint @beginPresentTimeId, GLuint @presentDurationId, GLenum @type, GLenum @target0, GLuint @fill0, GLenum @target1, GLuint @fill1, GLenum @target2, GLuint @fill2, GLenum @target3, GLuint @fill3) => glPresentFrameDualFillNVPtr(@video_slot, @minPresentTime, @beginPresentTimeId, @presentDurationId, @type, @target0, @fill0, @target1, @fill1, @target2, @fill2, @target3, @fill3);

        internal delegate void glGetVideoivNVFunc(GLuint @video_slot, GLenum @pname, GLint * @params);
        internal static glGetVideoivNVFunc glGetVideoivNVPtr;
        internal static void loadGetVideoivNV()
        {
            try
            {
                glGetVideoivNVPtr = (glGetVideoivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVideoivNV"), typeof(glGetVideoivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVideoivNV'.");
            }
        }
        public static void glGetVideoivNV(GLuint @video_slot, GLenum @pname, GLint * @params) => glGetVideoivNVPtr(@video_slot, @pname, @params);

        internal delegate void glGetVideouivNVFunc(GLuint @video_slot, GLenum @pname, GLuint * @params);
        internal static glGetVideouivNVFunc glGetVideouivNVPtr;
        internal static void loadGetVideouivNV()
        {
            try
            {
                glGetVideouivNVPtr = (glGetVideouivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVideouivNV"), typeof(glGetVideouivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVideouivNV'.");
            }
        }
        public static void glGetVideouivNV(GLuint @video_slot, GLenum @pname, GLuint * @params) => glGetVideouivNVPtr(@video_slot, @pname, @params);

        internal delegate void glGetVideoi64vNVFunc(GLuint @video_slot, GLenum @pname, GLint64EXT * @params);
        internal static glGetVideoi64vNVFunc glGetVideoi64vNVPtr;
        internal static void loadGetVideoi64vNV()
        {
            try
            {
                glGetVideoi64vNVPtr = (glGetVideoi64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVideoi64vNV"), typeof(glGetVideoi64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVideoi64vNV'.");
            }
        }
        public static void glGetVideoi64vNV(GLuint @video_slot, GLenum @pname, GLint64EXT * @params) => glGetVideoi64vNVPtr(@video_slot, @pname, @params);

        internal delegate void glGetVideoui64vNVFunc(GLuint @video_slot, GLenum @pname, GLuint64EXT * @params);
        internal static glGetVideoui64vNVFunc glGetVideoui64vNVPtr;
        internal static void loadGetVideoui64vNV()
        {
            try
            {
                glGetVideoui64vNVPtr = (glGetVideoui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVideoui64vNV"), typeof(glGetVideoui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVideoui64vNV'.");
            }
        }
        public static void glGetVideoui64vNV(GLuint @video_slot, GLenum @pname, GLuint64EXT * @params) => glGetVideoui64vNVPtr(@video_slot, @pname, @params);
        #endregion
    }
}
