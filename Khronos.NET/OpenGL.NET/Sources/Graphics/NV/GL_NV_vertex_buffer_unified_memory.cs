using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_vertex_buffer_unified_memory
    {
        #region Interop
        static GL_NV_vertex_buffer_unified_memory()
        {
            Console.WriteLine("Initalising GL_NV_vertex_buffer_unified_memory interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBufferAddressRangeNV();
            loadVertexFormatNV();
            loadNormalFormatNV();
            loadColorFormatNV();
            loadIndexFormatNV();
            loadTexCoordFormatNV();
            loadEdgeFlagFormatNV();
            loadSecondaryColorFormatNV();
            loadFogCoordFormatNV();
            loadVertexAttribFormatNV();
            loadVertexAttribIFormatNV();
            loadGetIntegerui64i_vNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_UNIFIED_NV = 0x8F1E;
        public static UInt32 GL_ELEMENT_ARRAY_UNIFIED_NV = 0x8F1F;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_ADDRESS_NV = 0x8F20;
        public static UInt32 GL_VERTEX_ARRAY_ADDRESS_NV = 0x8F21;
        public static UInt32 GL_NORMAL_ARRAY_ADDRESS_NV = 0x8F22;
        public static UInt32 GL_COLOR_ARRAY_ADDRESS_NV = 0x8F23;
        public static UInt32 GL_INDEX_ARRAY_ADDRESS_NV = 0x8F24;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_ADDRESS_NV = 0x8F25;
        public static UInt32 GL_EDGE_FLAG_ARRAY_ADDRESS_NV = 0x8F26;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_ADDRESS_NV = 0x8F27;
        public static UInt32 GL_FOG_COORD_ARRAY_ADDRESS_NV = 0x8F28;
        public static UInt32 GL_ELEMENT_ARRAY_ADDRESS_NV = 0x8F29;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_LENGTH_NV = 0x8F2A;
        public static UInt32 GL_VERTEX_ARRAY_LENGTH_NV = 0x8F2B;
        public static UInt32 GL_NORMAL_ARRAY_LENGTH_NV = 0x8F2C;
        public static UInt32 GL_COLOR_ARRAY_LENGTH_NV = 0x8F2D;
        public static UInt32 GL_INDEX_ARRAY_LENGTH_NV = 0x8F2E;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_LENGTH_NV = 0x8F2F;
        public static UInt32 GL_EDGE_FLAG_ARRAY_LENGTH_NV = 0x8F30;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_LENGTH_NV = 0x8F31;
        public static UInt32 GL_FOG_COORD_ARRAY_LENGTH_NV = 0x8F32;
        public static UInt32 GL_ELEMENT_ARRAY_LENGTH_NV = 0x8F33;
        public static UInt32 GL_DRAW_INDIRECT_UNIFIED_NV = 0x8F40;
        public static UInt32 GL_DRAW_INDIRECT_ADDRESS_NV = 0x8F41;
        public static UInt32 GL_DRAW_INDIRECT_LENGTH_NV = 0x8F42;
        #endregion

        #region Commands
        internal delegate void glBufferAddressRangeNVFunc(GLenum @pname, GLuint @index, GLuint64EXT @address, GLsizeiptr @length);
        internal static glBufferAddressRangeNVFunc glBufferAddressRangeNVPtr;
        internal static void loadBufferAddressRangeNV()
        {
            try
            {
                glBufferAddressRangeNVPtr = (glBufferAddressRangeNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferAddressRangeNV"), typeof(glBufferAddressRangeNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferAddressRangeNV'.");
            }
        }
        public static void glBufferAddressRangeNV(GLenum @pname, GLuint @index, GLuint64EXT @address, GLsizeiptr @length) => glBufferAddressRangeNVPtr(@pname, @index, @address, @length);

        internal delegate void glVertexFormatNVFunc(GLint @size, GLenum @type, GLsizei @stride);
        internal static glVertexFormatNVFunc glVertexFormatNVPtr;
        internal static void loadVertexFormatNV()
        {
            try
            {
                glVertexFormatNVPtr = (glVertexFormatNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexFormatNV"), typeof(glVertexFormatNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexFormatNV'.");
            }
        }
        public static void glVertexFormatNV(GLint @size, GLenum @type, GLsizei @stride) => glVertexFormatNVPtr(@size, @type, @stride);

        internal delegate void glNormalFormatNVFunc(GLenum @type, GLsizei @stride);
        internal static glNormalFormatNVFunc glNormalFormatNVPtr;
        internal static void loadNormalFormatNV()
        {
            try
            {
                glNormalFormatNVPtr = (glNormalFormatNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalFormatNV"), typeof(glNormalFormatNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalFormatNV'.");
            }
        }
        public static void glNormalFormatNV(GLenum @type, GLsizei @stride) => glNormalFormatNVPtr(@type, @stride);

        internal delegate void glColorFormatNVFunc(GLint @size, GLenum @type, GLsizei @stride);
        internal static glColorFormatNVFunc glColorFormatNVPtr;
        internal static void loadColorFormatNV()
        {
            try
            {
                glColorFormatNVPtr = (glColorFormatNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorFormatNV"), typeof(glColorFormatNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorFormatNV'.");
            }
        }
        public static void glColorFormatNV(GLint @size, GLenum @type, GLsizei @stride) => glColorFormatNVPtr(@size, @type, @stride);

        internal delegate void glIndexFormatNVFunc(GLenum @type, GLsizei @stride);
        internal static glIndexFormatNVFunc glIndexFormatNVPtr;
        internal static void loadIndexFormatNV()
        {
            try
            {
                glIndexFormatNVPtr = (glIndexFormatNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexFormatNV"), typeof(glIndexFormatNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexFormatNV'.");
            }
        }
        public static void glIndexFormatNV(GLenum @type, GLsizei @stride) => glIndexFormatNVPtr(@type, @stride);

        internal delegate void glTexCoordFormatNVFunc(GLint @size, GLenum @type, GLsizei @stride);
        internal static glTexCoordFormatNVFunc glTexCoordFormatNVPtr;
        internal static void loadTexCoordFormatNV()
        {
            try
            {
                glTexCoordFormatNVPtr = (glTexCoordFormatNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordFormatNV"), typeof(glTexCoordFormatNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordFormatNV'.");
            }
        }
        public static void glTexCoordFormatNV(GLint @size, GLenum @type, GLsizei @stride) => glTexCoordFormatNVPtr(@size, @type, @stride);

        internal delegate void glEdgeFlagFormatNVFunc(GLsizei @stride);
        internal static glEdgeFlagFormatNVFunc glEdgeFlagFormatNVPtr;
        internal static void loadEdgeFlagFormatNV()
        {
            try
            {
                glEdgeFlagFormatNVPtr = (glEdgeFlagFormatNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEdgeFlagFormatNV"), typeof(glEdgeFlagFormatNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEdgeFlagFormatNV'.");
            }
        }
        public static void glEdgeFlagFormatNV(GLsizei @stride) => glEdgeFlagFormatNVPtr(@stride);

        internal delegate void glSecondaryColorFormatNVFunc(GLint @size, GLenum @type, GLsizei @stride);
        internal static glSecondaryColorFormatNVFunc glSecondaryColorFormatNVPtr;
        internal static void loadSecondaryColorFormatNV()
        {
            try
            {
                glSecondaryColorFormatNVPtr = (glSecondaryColorFormatNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColorFormatNV"), typeof(glSecondaryColorFormatNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColorFormatNV'.");
            }
        }
        public static void glSecondaryColorFormatNV(GLint @size, GLenum @type, GLsizei @stride) => glSecondaryColorFormatNVPtr(@size, @type, @stride);

        internal delegate void glFogCoordFormatNVFunc(GLenum @type, GLsizei @stride);
        internal static glFogCoordFormatNVFunc glFogCoordFormatNVPtr;
        internal static void loadFogCoordFormatNV()
        {
            try
            {
                glFogCoordFormatNVPtr = (glFogCoordFormatNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoordFormatNV"), typeof(glFogCoordFormatNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoordFormatNV'.");
            }
        }
        public static void glFogCoordFormatNV(GLenum @type, GLsizei @stride) => glFogCoordFormatNVPtr(@type, @stride);

        internal delegate void glVertexAttribFormatNVFunc(GLuint @index, GLint @size, GLenum @type, GLboolean @normalized, GLsizei @stride);
        internal static glVertexAttribFormatNVFunc glVertexAttribFormatNVPtr;
        internal static void loadVertexAttribFormatNV()
        {
            try
            {
                glVertexAttribFormatNVPtr = (glVertexAttribFormatNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribFormatNV"), typeof(glVertexAttribFormatNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribFormatNV'.");
            }
        }
        public static void glVertexAttribFormatNV(GLuint @index, GLint @size, GLenum @type, GLboolean @normalized, GLsizei @stride) => glVertexAttribFormatNVPtr(@index, @size, @type, @normalized, @stride);

        internal delegate void glVertexAttribIFormatNVFunc(GLuint @index, GLint @size, GLenum @type, GLsizei @stride);
        internal static glVertexAttribIFormatNVFunc glVertexAttribIFormatNVPtr;
        internal static void loadVertexAttribIFormatNV()
        {
            try
            {
                glVertexAttribIFormatNVPtr = (glVertexAttribIFormatNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribIFormatNV"), typeof(glVertexAttribIFormatNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribIFormatNV'.");
            }
        }
        public static void glVertexAttribIFormatNV(GLuint @index, GLint @size, GLenum @type, GLsizei @stride) => glVertexAttribIFormatNVPtr(@index, @size, @type, @stride);

        internal delegate void glGetIntegerui64i_vNVFunc(GLenum @value, GLuint @index, GLuint64EXT * @result);
        internal static glGetIntegerui64i_vNVFunc glGetIntegerui64i_vNVPtr;
        internal static void loadGetIntegerui64i_vNV()
        {
            try
            {
                glGetIntegerui64i_vNVPtr = (glGetIntegerui64i_vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetIntegerui64i_vNV"), typeof(glGetIntegerui64i_vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetIntegerui64i_vNV'.");
            }
        }
        public static void glGetIntegerui64i_vNV(GLenum @value, GLuint @index, GLuint64EXT * @result) => glGetIntegerui64i_vNVPtr(@value, @index, @result);
        #endregion
    }
}
