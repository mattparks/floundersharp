using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_read_buffer
    {
        #region Interop
        static GL_NV_read_buffer()
        {
            Console.WriteLine("Initalising GL_NV_read_buffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadReadBufferNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_READ_BUFFER_NV = 0x0C02;
        #endregion

        #region Commands
        internal delegate void glReadBufferNVFunc(GLenum @mode);
        internal static glReadBufferNVFunc glReadBufferNVPtr;
        internal static void loadReadBufferNV()
        {
            try
            {
                glReadBufferNVPtr = (glReadBufferNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadBufferNV"), typeof(glReadBufferNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadBufferNV'.");
            }
        }
        public static void glReadBufferNV(GLenum @mode) => glReadBufferNVPtr(@mode);
        #endregion
    }
}
