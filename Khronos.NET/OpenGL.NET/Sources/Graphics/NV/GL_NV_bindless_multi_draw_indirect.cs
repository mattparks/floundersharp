using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_bindless_multi_draw_indirect
    {
        #region Interop
        static GL_NV_bindless_multi_draw_indirect()
        {
            Console.WriteLine("Initalising GL_NV_bindless_multi_draw_indirect interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMultiDrawArraysIndirectBindlessNV();
            loadMultiDrawElementsIndirectBindlessNV();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glMultiDrawArraysIndirectBindlessNVFunc(GLenum @mode, const void * @indirect, GLsizei @drawCount, GLsizei @stride, GLint @vertexBufferCount);
        internal static glMultiDrawArraysIndirectBindlessNVFunc glMultiDrawArraysIndirectBindlessNVPtr;
        internal static void loadMultiDrawArraysIndirectBindlessNV()
        {
            try
            {
                glMultiDrawArraysIndirectBindlessNVPtr = (glMultiDrawArraysIndirectBindlessNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawArraysIndirectBindlessNV"), typeof(glMultiDrawArraysIndirectBindlessNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawArraysIndirectBindlessNV'.");
            }
        }
        public static void glMultiDrawArraysIndirectBindlessNV(GLenum @mode, const void * @indirect, GLsizei @drawCount, GLsizei @stride, GLint @vertexBufferCount) => glMultiDrawArraysIndirectBindlessNVPtr(@mode, @indirect, @drawCount, @stride, @vertexBufferCount);

        internal delegate void glMultiDrawElementsIndirectBindlessNVFunc(GLenum @mode, GLenum @type, const void * @indirect, GLsizei @drawCount, GLsizei @stride, GLint @vertexBufferCount);
        internal static glMultiDrawElementsIndirectBindlessNVFunc glMultiDrawElementsIndirectBindlessNVPtr;
        internal static void loadMultiDrawElementsIndirectBindlessNV()
        {
            try
            {
                glMultiDrawElementsIndirectBindlessNVPtr = (glMultiDrawElementsIndirectBindlessNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementsIndirectBindlessNV"), typeof(glMultiDrawElementsIndirectBindlessNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementsIndirectBindlessNV'.");
            }
        }
        public static void glMultiDrawElementsIndirectBindlessNV(GLenum @mode, GLenum @type, const void * @indirect, GLsizei @drawCount, GLsizei @stride, GLint @vertexBufferCount) => glMultiDrawElementsIndirectBindlessNVPtr(@mode, @type, @indirect, @drawCount, @stride, @vertexBufferCount);
        #endregion
    }
}
