using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_fill_rectangle
    {
        #region Interop
        static GL_NV_fill_rectangle()
        {
            Console.WriteLine("Initalising GL_NV_fill_rectangle interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FILL_RECTANGLE_NV = 0x933C;
        #endregion

        #region Commands
        #endregion
    }
}
