using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_transform_feedback2
    {
        #region Interop
        static GL_NV_transform_feedback2()
        {
            Console.WriteLine("Initalising GL_NV_transform_feedback2 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindTransformFeedbackNV();
            loadDeleteTransformFeedbacksNV();
            loadGenTransformFeedbacksNV();
            loadIsTransformFeedbackNV();
            loadPauseTransformFeedbackNV();
            loadResumeTransformFeedbackNV();
            loadDrawTransformFeedbackNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TRANSFORM_FEEDBACK_NV = 0x8E22;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_PAUSED_NV = 0x8E23;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_ACTIVE_NV = 0x8E24;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BINDING_NV = 0x8E25;
        #endregion

        #region Commands
        internal delegate void glBindTransformFeedbackNVFunc(GLenum @target, GLuint @id);
        internal static glBindTransformFeedbackNVFunc glBindTransformFeedbackNVPtr;
        internal static void loadBindTransformFeedbackNV()
        {
            try
            {
                glBindTransformFeedbackNVPtr = (glBindTransformFeedbackNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTransformFeedbackNV"), typeof(glBindTransformFeedbackNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTransformFeedbackNV'.");
            }
        }
        public static void glBindTransformFeedbackNV(GLenum @target, GLuint @id) => glBindTransformFeedbackNVPtr(@target, @id);

        internal delegate void glDeleteTransformFeedbacksNVFunc(GLsizei @n, const GLuint * @ids);
        internal static glDeleteTransformFeedbacksNVFunc glDeleteTransformFeedbacksNVPtr;
        internal static void loadDeleteTransformFeedbacksNV()
        {
            try
            {
                glDeleteTransformFeedbacksNVPtr = (glDeleteTransformFeedbacksNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteTransformFeedbacksNV"), typeof(glDeleteTransformFeedbacksNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteTransformFeedbacksNV'.");
            }
        }
        public static void glDeleteTransformFeedbacksNV(GLsizei @n, const GLuint * @ids) => glDeleteTransformFeedbacksNVPtr(@n, @ids);

        internal delegate void glGenTransformFeedbacksNVFunc(GLsizei @n, GLuint * @ids);
        internal static glGenTransformFeedbacksNVFunc glGenTransformFeedbacksNVPtr;
        internal static void loadGenTransformFeedbacksNV()
        {
            try
            {
                glGenTransformFeedbacksNVPtr = (glGenTransformFeedbacksNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenTransformFeedbacksNV"), typeof(glGenTransformFeedbacksNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenTransformFeedbacksNV'.");
            }
        }
        public static void glGenTransformFeedbacksNV(GLsizei @n, GLuint * @ids) => glGenTransformFeedbacksNVPtr(@n, @ids);

        internal delegate GLboolean glIsTransformFeedbackNVFunc(GLuint @id);
        internal static glIsTransformFeedbackNVFunc glIsTransformFeedbackNVPtr;
        internal static void loadIsTransformFeedbackNV()
        {
            try
            {
                glIsTransformFeedbackNVPtr = (glIsTransformFeedbackNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsTransformFeedbackNV"), typeof(glIsTransformFeedbackNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsTransformFeedbackNV'.");
            }
        }
        public static GLboolean glIsTransformFeedbackNV(GLuint @id) => glIsTransformFeedbackNVPtr(@id);

        internal delegate void glPauseTransformFeedbackNVFunc();
        internal static glPauseTransformFeedbackNVFunc glPauseTransformFeedbackNVPtr;
        internal static void loadPauseTransformFeedbackNV()
        {
            try
            {
                glPauseTransformFeedbackNVPtr = (glPauseTransformFeedbackNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPauseTransformFeedbackNV"), typeof(glPauseTransformFeedbackNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPauseTransformFeedbackNV'.");
            }
        }
        public static void glPauseTransformFeedbackNV() => glPauseTransformFeedbackNVPtr();

        internal delegate void glResumeTransformFeedbackNVFunc();
        internal static glResumeTransformFeedbackNVFunc glResumeTransformFeedbackNVPtr;
        internal static void loadResumeTransformFeedbackNV()
        {
            try
            {
                glResumeTransformFeedbackNVPtr = (glResumeTransformFeedbackNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glResumeTransformFeedbackNV"), typeof(glResumeTransformFeedbackNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glResumeTransformFeedbackNV'.");
            }
        }
        public static void glResumeTransformFeedbackNV() => glResumeTransformFeedbackNVPtr();

        internal delegate void glDrawTransformFeedbackNVFunc(GLenum @mode, GLuint @id);
        internal static glDrawTransformFeedbackNVFunc glDrawTransformFeedbackNVPtr;
        internal static void loadDrawTransformFeedbackNV()
        {
            try
            {
                glDrawTransformFeedbackNVPtr = (glDrawTransformFeedbackNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTransformFeedbackNV"), typeof(glDrawTransformFeedbackNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTransformFeedbackNV'.");
            }
        }
        public static void glDrawTransformFeedbackNV(GLenum @mode, GLuint @id) => glDrawTransformFeedbackNVPtr(@mode, @id);
        #endregion
    }
}
