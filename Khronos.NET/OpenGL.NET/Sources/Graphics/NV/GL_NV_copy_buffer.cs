using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_copy_buffer
    {
        #region Interop
        static GL_NV_copy_buffer()
        {
            Console.WriteLine("Initalising GL_NV_copy_buffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCopyBufferSubDataNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COPY_READ_BUFFER_NV = 0x8F36;
        public static UInt32 GL_COPY_WRITE_BUFFER_NV = 0x8F37;
        #endregion

        #region Commands
        internal delegate void glCopyBufferSubDataNVFunc(GLenum @readTarget, GLenum @writeTarget, GLintptr @readOffset, GLintptr @writeOffset, GLsizeiptr @size);
        internal static glCopyBufferSubDataNVFunc glCopyBufferSubDataNVPtr;
        internal static void loadCopyBufferSubDataNV()
        {
            try
            {
                glCopyBufferSubDataNVPtr = (glCopyBufferSubDataNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyBufferSubDataNV"), typeof(glCopyBufferSubDataNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyBufferSubDataNV'.");
            }
        }
        public static void glCopyBufferSubDataNV(GLenum @readTarget, GLenum @writeTarget, GLintptr @readOffset, GLintptr @writeOffset, GLsizeiptr @size) => glCopyBufferSubDataNVPtr(@readTarget, @writeTarget, @readOffset, @writeOffset, @size);
        #endregion
    }
}
