using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_fbo_color_attachments
    {
        #region Interop
        static GL_NV_fbo_color_attachments()
        {
            Console.WriteLine("Initalising GL_NV_fbo_color_attachments interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_COLOR_ATTACHMENTS_NV = 0x8CDF;
        public static UInt32 GL_COLOR_ATTACHMENT0_NV = 0x8CE0;
        public static UInt32 GL_COLOR_ATTACHMENT1_NV = 0x8CE1;
        public static UInt32 GL_COLOR_ATTACHMENT2_NV = 0x8CE2;
        public static UInt32 GL_COLOR_ATTACHMENT3_NV = 0x8CE3;
        public static UInt32 GL_COLOR_ATTACHMENT4_NV = 0x8CE4;
        public static UInt32 GL_COLOR_ATTACHMENT5_NV = 0x8CE5;
        public static UInt32 GL_COLOR_ATTACHMENT6_NV = 0x8CE6;
        public static UInt32 GL_COLOR_ATTACHMENT7_NV = 0x8CE7;
        public static UInt32 GL_COLOR_ATTACHMENT8_NV = 0x8CE8;
        public static UInt32 GL_COLOR_ATTACHMENT9_NV = 0x8CE9;
        public static UInt32 GL_COLOR_ATTACHMENT10_NV = 0x8CEA;
        public static UInt32 GL_COLOR_ATTACHMENT11_NV = 0x8CEB;
        public static UInt32 GL_COLOR_ATTACHMENT12_NV = 0x8CEC;
        public static UInt32 GL_COLOR_ATTACHMENT13_NV = 0x8CED;
        public static UInt32 GL_COLOR_ATTACHMENT14_NV = 0x8CEE;
        public static UInt32 GL_COLOR_ATTACHMENT15_NV = 0x8CEF;
        #endregion

        #region Commands
        #endregion
    }
}
