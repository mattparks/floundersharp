using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_texture_env_combine4
    {
        #region Interop
        static GL_NV_texture_env_combine4()
        {
            Console.WriteLine("Initalising GL_NV_texture_env_combine4 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMBINE4_NV = 0x8503;
        public static UInt32 GL_SOURCE3_RGB_NV = 0x8583;
        public static UInt32 GL_SOURCE3_ALPHA_NV = 0x858B;
        public static UInt32 GL_OPERAND3_RGB_NV = 0x8593;
        public static UInt32 GL_OPERAND3_ALPHA_NV = 0x859B;
        #endregion

        #region Commands
        #endregion
    }
}
