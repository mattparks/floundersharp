using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_non_square_matrices
    {
        #region Interop
        static GL_NV_non_square_matrices()
        {
            Console.WriteLine("Initalising GL_NV_non_square_matrices interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadUniformMatrix2x3fvNV();
            loadUniformMatrix3x2fvNV();
            loadUniformMatrix2x4fvNV();
            loadUniformMatrix4x2fvNV();
            loadUniformMatrix3x4fvNV();
            loadUniformMatrix4x3fvNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FLOAT_MAT2x3_NV = 0x8B65;
        public static UInt32 GL_FLOAT_MAT2x4_NV = 0x8B66;
        public static UInt32 GL_FLOAT_MAT3x2_NV = 0x8B67;
        public static UInt32 GL_FLOAT_MAT3x4_NV = 0x8B68;
        public static UInt32 GL_FLOAT_MAT4x2_NV = 0x8B69;
        public static UInt32 GL_FLOAT_MAT4x3_NV = 0x8B6A;
        #endregion

        #region Commands
        internal delegate void glUniformMatrix2x3fvNVFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix2x3fvNVFunc glUniformMatrix2x3fvNVPtr;
        internal static void loadUniformMatrix2x3fvNV()
        {
            try
            {
                glUniformMatrix2x3fvNVPtr = (glUniformMatrix2x3fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2x3fvNV"), typeof(glUniformMatrix2x3fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2x3fvNV'.");
            }
        }
        public static void glUniformMatrix2x3fvNV(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix2x3fvNVPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3x2fvNVFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix3x2fvNVFunc glUniformMatrix3x2fvNVPtr;
        internal static void loadUniformMatrix3x2fvNV()
        {
            try
            {
                glUniformMatrix3x2fvNVPtr = (glUniformMatrix3x2fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3x2fvNV"), typeof(glUniformMatrix3x2fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3x2fvNV'.");
            }
        }
        public static void glUniformMatrix3x2fvNV(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix3x2fvNVPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix2x4fvNVFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix2x4fvNVFunc glUniformMatrix2x4fvNVPtr;
        internal static void loadUniformMatrix2x4fvNV()
        {
            try
            {
                glUniformMatrix2x4fvNVPtr = (glUniformMatrix2x4fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2x4fvNV"), typeof(glUniformMatrix2x4fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2x4fvNV'.");
            }
        }
        public static void glUniformMatrix2x4fvNV(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix2x4fvNVPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4x2fvNVFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix4x2fvNVFunc glUniformMatrix4x2fvNVPtr;
        internal static void loadUniformMatrix4x2fvNV()
        {
            try
            {
                glUniformMatrix4x2fvNVPtr = (glUniformMatrix4x2fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4x2fvNV"), typeof(glUniformMatrix4x2fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4x2fvNV'.");
            }
        }
        public static void glUniformMatrix4x2fvNV(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix4x2fvNVPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3x4fvNVFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix3x4fvNVFunc glUniformMatrix3x4fvNVPtr;
        internal static void loadUniformMatrix3x4fvNV()
        {
            try
            {
                glUniformMatrix3x4fvNVPtr = (glUniformMatrix3x4fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3x4fvNV"), typeof(glUniformMatrix3x4fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3x4fvNV'.");
            }
        }
        public static void glUniformMatrix3x4fvNV(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix3x4fvNVPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4x3fvNVFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix4x3fvNVFunc glUniformMatrix4x3fvNVPtr;
        internal static void loadUniformMatrix4x3fvNV()
        {
            try
            {
                glUniformMatrix4x3fvNVPtr = (glUniformMatrix4x3fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4x3fvNV"), typeof(glUniformMatrix4x3fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4x3fvNV'.");
            }
        }
        public static void glUniformMatrix4x3fvNV(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix4x3fvNVPtr(@location, @count, @transpose, @value);
        #endregion
    }
}
