using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_vertex_program4
    {
        #region Interop
        static GL_NV_vertex_program4()
        {
            Console.WriteLine("Initalising GL_NV_vertex_program4 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexAttribI1iEXT();
            loadVertexAttribI2iEXT();
            loadVertexAttribI3iEXT();
            loadVertexAttribI4iEXT();
            loadVertexAttribI1uiEXT();
            loadVertexAttribI2uiEXT();
            loadVertexAttribI3uiEXT();
            loadVertexAttribI4uiEXT();
            loadVertexAttribI1ivEXT();
            loadVertexAttribI2ivEXT();
            loadVertexAttribI3ivEXT();
            loadVertexAttribI4ivEXT();
            loadVertexAttribI1uivEXT();
            loadVertexAttribI2uivEXT();
            loadVertexAttribI3uivEXT();
            loadVertexAttribI4uivEXT();
            loadVertexAttribI4bvEXT();
            loadVertexAttribI4svEXT();
            loadVertexAttribI4ubvEXT();
            loadVertexAttribI4usvEXT();
            loadVertexAttribIPointerEXT();
            loadGetVertexAttribIivEXT();
            loadGetVertexAttribIuivEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_INTEGER_NV = 0x88FD;
        #endregion

        #region Commands
        internal delegate void glVertexAttribI1iEXTFunc(GLuint @index, GLint @x);
        internal static glVertexAttribI1iEXTFunc glVertexAttribI1iEXTPtr;
        internal static void loadVertexAttribI1iEXT()
        {
            try
            {
                glVertexAttribI1iEXTPtr = (glVertexAttribI1iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI1iEXT"), typeof(glVertexAttribI1iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI1iEXT'.");
            }
        }
        public static void glVertexAttribI1iEXT(GLuint @index, GLint @x) => glVertexAttribI1iEXTPtr(@index, @x);

        internal delegate void glVertexAttribI2iEXTFunc(GLuint @index, GLint @x, GLint @y);
        internal static glVertexAttribI2iEXTFunc glVertexAttribI2iEXTPtr;
        internal static void loadVertexAttribI2iEXT()
        {
            try
            {
                glVertexAttribI2iEXTPtr = (glVertexAttribI2iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI2iEXT"), typeof(glVertexAttribI2iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI2iEXT'.");
            }
        }
        public static void glVertexAttribI2iEXT(GLuint @index, GLint @x, GLint @y) => glVertexAttribI2iEXTPtr(@index, @x, @y);

        internal delegate void glVertexAttribI3iEXTFunc(GLuint @index, GLint @x, GLint @y, GLint @z);
        internal static glVertexAttribI3iEXTFunc glVertexAttribI3iEXTPtr;
        internal static void loadVertexAttribI3iEXT()
        {
            try
            {
                glVertexAttribI3iEXTPtr = (glVertexAttribI3iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI3iEXT"), typeof(glVertexAttribI3iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI3iEXT'.");
            }
        }
        public static void glVertexAttribI3iEXT(GLuint @index, GLint @x, GLint @y, GLint @z) => glVertexAttribI3iEXTPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttribI4iEXTFunc(GLuint @index, GLint @x, GLint @y, GLint @z, GLint @w);
        internal static glVertexAttribI4iEXTFunc glVertexAttribI4iEXTPtr;
        internal static void loadVertexAttribI4iEXT()
        {
            try
            {
                glVertexAttribI4iEXTPtr = (glVertexAttribI4iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4iEXT"), typeof(glVertexAttribI4iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4iEXT'.");
            }
        }
        public static void glVertexAttribI4iEXT(GLuint @index, GLint @x, GLint @y, GLint @z, GLint @w) => glVertexAttribI4iEXTPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttribI1uiEXTFunc(GLuint @index, GLuint @x);
        internal static glVertexAttribI1uiEXTFunc glVertexAttribI1uiEXTPtr;
        internal static void loadVertexAttribI1uiEXT()
        {
            try
            {
                glVertexAttribI1uiEXTPtr = (glVertexAttribI1uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI1uiEXT"), typeof(glVertexAttribI1uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI1uiEXT'.");
            }
        }
        public static void glVertexAttribI1uiEXT(GLuint @index, GLuint @x) => glVertexAttribI1uiEXTPtr(@index, @x);

        internal delegate void glVertexAttribI2uiEXTFunc(GLuint @index, GLuint @x, GLuint @y);
        internal static glVertexAttribI2uiEXTFunc glVertexAttribI2uiEXTPtr;
        internal static void loadVertexAttribI2uiEXT()
        {
            try
            {
                glVertexAttribI2uiEXTPtr = (glVertexAttribI2uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI2uiEXT"), typeof(glVertexAttribI2uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI2uiEXT'.");
            }
        }
        public static void glVertexAttribI2uiEXT(GLuint @index, GLuint @x, GLuint @y) => glVertexAttribI2uiEXTPtr(@index, @x, @y);

        internal delegate void glVertexAttribI3uiEXTFunc(GLuint @index, GLuint @x, GLuint @y, GLuint @z);
        internal static glVertexAttribI3uiEXTFunc glVertexAttribI3uiEXTPtr;
        internal static void loadVertexAttribI3uiEXT()
        {
            try
            {
                glVertexAttribI3uiEXTPtr = (glVertexAttribI3uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI3uiEXT"), typeof(glVertexAttribI3uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI3uiEXT'.");
            }
        }
        public static void glVertexAttribI3uiEXT(GLuint @index, GLuint @x, GLuint @y, GLuint @z) => glVertexAttribI3uiEXTPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttribI4uiEXTFunc(GLuint @index, GLuint @x, GLuint @y, GLuint @z, GLuint @w);
        internal static glVertexAttribI4uiEXTFunc glVertexAttribI4uiEXTPtr;
        internal static void loadVertexAttribI4uiEXT()
        {
            try
            {
                glVertexAttribI4uiEXTPtr = (glVertexAttribI4uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4uiEXT"), typeof(glVertexAttribI4uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4uiEXT'.");
            }
        }
        public static void glVertexAttribI4uiEXT(GLuint @index, GLuint @x, GLuint @y, GLuint @z, GLuint @w) => glVertexAttribI4uiEXTPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttribI1ivEXTFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttribI1ivEXTFunc glVertexAttribI1ivEXTPtr;
        internal static void loadVertexAttribI1ivEXT()
        {
            try
            {
                glVertexAttribI1ivEXTPtr = (glVertexAttribI1ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI1ivEXT"), typeof(glVertexAttribI1ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI1ivEXT'.");
            }
        }
        public static void glVertexAttribI1ivEXT(GLuint @index, const GLint * @v) => glVertexAttribI1ivEXTPtr(@index, @v);

        internal delegate void glVertexAttribI2ivEXTFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttribI2ivEXTFunc glVertexAttribI2ivEXTPtr;
        internal static void loadVertexAttribI2ivEXT()
        {
            try
            {
                glVertexAttribI2ivEXTPtr = (glVertexAttribI2ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI2ivEXT"), typeof(glVertexAttribI2ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI2ivEXT'.");
            }
        }
        public static void glVertexAttribI2ivEXT(GLuint @index, const GLint * @v) => glVertexAttribI2ivEXTPtr(@index, @v);

        internal delegate void glVertexAttribI3ivEXTFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttribI3ivEXTFunc glVertexAttribI3ivEXTPtr;
        internal static void loadVertexAttribI3ivEXT()
        {
            try
            {
                glVertexAttribI3ivEXTPtr = (glVertexAttribI3ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI3ivEXT"), typeof(glVertexAttribI3ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI3ivEXT'.");
            }
        }
        public static void glVertexAttribI3ivEXT(GLuint @index, const GLint * @v) => glVertexAttribI3ivEXTPtr(@index, @v);

        internal delegate void glVertexAttribI4ivEXTFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttribI4ivEXTFunc glVertexAttribI4ivEXTPtr;
        internal static void loadVertexAttribI4ivEXT()
        {
            try
            {
                glVertexAttribI4ivEXTPtr = (glVertexAttribI4ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4ivEXT"), typeof(glVertexAttribI4ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4ivEXT'.");
            }
        }
        public static void glVertexAttribI4ivEXT(GLuint @index, const GLint * @v) => glVertexAttribI4ivEXTPtr(@index, @v);

        internal delegate void glVertexAttribI1uivEXTFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttribI1uivEXTFunc glVertexAttribI1uivEXTPtr;
        internal static void loadVertexAttribI1uivEXT()
        {
            try
            {
                glVertexAttribI1uivEXTPtr = (glVertexAttribI1uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI1uivEXT"), typeof(glVertexAttribI1uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI1uivEXT'.");
            }
        }
        public static void glVertexAttribI1uivEXT(GLuint @index, const GLuint * @v) => glVertexAttribI1uivEXTPtr(@index, @v);

        internal delegate void glVertexAttribI2uivEXTFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttribI2uivEXTFunc glVertexAttribI2uivEXTPtr;
        internal static void loadVertexAttribI2uivEXT()
        {
            try
            {
                glVertexAttribI2uivEXTPtr = (glVertexAttribI2uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI2uivEXT"), typeof(glVertexAttribI2uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI2uivEXT'.");
            }
        }
        public static void glVertexAttribI2uivEXT(GLuint @index, const GLuint * @v) => glVertexAttribI2uivEXTPtr(@index, @v);

        internal delegate void glVertexAttribI3uivEXTFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttribI3uivEXTFunc glVertexAttribI3uivEXTPtr;
        internal static void loadVertexAttribI3uivEXT()
        {
            try
            {
                glVertexAttribI3uivEXTPtr = (glVertexAttribI3uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI3uivEXT"), typeof(glVertexAttribI3uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI3uivEXT'.");
            }
        }
        public static void glVertexAttribI3uivEXT(GLuint @index, const GLuint * @v) => glVertexAttribI3uivEXTPtr(@index, @v);

        internal delegate void glVertexAttribI4uivEXTFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttribI4uivEXTFunc glVertexAttribI4uivEXTPtr;
        internal static void loadVertexAttribI4uivEXT()
        {
            try
            {
                glVertexAttribI4uivEXTPtr = (glVertexAttribI4uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4uivEXT"), typeof(glVertexAttribI4uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4uivEXT'.");
            }
        }
        public static void glVertexAttribI4uivEXT(GLuint @index, const GLuint * @v) => glVertexAttribI4uivEXTPtr(@index, @v);

        internal delegate void glVertexAttribI4bvEXTFunc(GLuint @index, const GLbyte * @v);
        internal static glVertexAttribI4bvEXTFunc glVertexAttribI4bvEXTPtr;
        internal static void loadVertexAttribI4bvEXT()
        {
            try
            {
                glVertexAttribI4bvEXTPtr = (glVertexAttribI4bvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4bvEXT"), typeof(glVertexAttribI4bvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4bvEXT'.");
            }
        }
        public static void glVertexAttribI4bvEXT(GLuint @index, const GLbyte * @v) => glVertexAttribI4bvEXTPtr(@index, @v);

        internal delegate void glVertexAttribI4svEXTFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttribI4svEXTFunc glVertexAttribI4svEXTPtr;
        internal static void loadVertexAttribI4svEXT()
        {
            try
            {
                glVertexAttribI4svEXTPtr = (glVertexAttribI4svEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4svEXT"), typeof(glVertexAttribI4svEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4svEXT'.");
            }
        }
        public static void glVertexAttribI4svEXT(GLuint @index, const GLshort * @v) => glVertexAttribI4svEXTPtr(@index, @v);

        internal delegate void glVertexAttribI4ubvEXTFunc(GLuint @index, const GLubyte * @v);
        internal static glVertexAttribI4ubvEXTFunc glVertexAttribI4ubvEXTPtr;
        internal static void loadVertexAttribI4ubvEXT()
        {
            try
            {
                glVertexAttribI4ubvEXTPtr = (glVertexAttribI4ubvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4ubvEXT"), typeof(glVertexAttribI4ubvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4ubvEXT'.");
            }
        }
        public static void glVertexAttribI4ubvEXT(GLuint @index, const GLubyte * @v) => glVertexAttribI4ubvEXTPtr(@index, @v);

        internal delegate void glVertexAttribI4usvEXTFunc(GLuint @index, const GLushort * @v);
        internal static glVertexAttribI4usvEXTFunc glVertexAttribI4usvEXTPtr;
        internal static void loadVertexAttribI4usvEXT()
        {
            try
            {
                glVertexAttribI4usvEXTPtr = (glVertexAttribI4usvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4usvEXT"), typeof(glVertexAttribI4usvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4usvEXT'.");
            }
        }
        public static void glVertexAttribI4usvEXT(GLuint @index, const GLushort * @v) => glVertexAttribI4usvEXTPtr(@index, @v);

        internal delegate void glVertexAttribIPointerEXTFunc(GLuint @index, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glVertexAttribIPointerEXTFunc glVertexAttribIPointerEXTPtr;
        internal static void loadVertexAttribIPointerEXT()
        {
            try
            {
                glVertexAttribIPointerEXTPtr = (glVertexAttribIPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribIPointerEXT"), typeof(glVertexAttribIPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribIPointerEXT'.");
            }
        }
        public static void glVertexAttribIPointerEXT(GLuint @index, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glVertexAttribIPointerEXTPtr(@index, @size, @type, @stride, @pointer);

        internal delegate void glGetVertexAttribIivEXTFunc(GLuint @index, GLenum @pname, GLint * @params);
        internal static glGetVertexAttribIivEXTFunc glGetVertexAttribIivEXTPtr;
        internal static void loadGetVertexAttribIivEXT()
        {
            try
            {
                glGetVertexAttribIivEXTPtr = (glGetVertexAttribIivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribIivEXT"), typeof(glGetVertexAttribIivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribIivEXT'.");
            }
        }
        public static void glGetVertexAttribIivEXT(GLuint @index, GLenum @pname, GLint * @params) => glGetVertexAttribIivEXTPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribIuivEXTFunc(GLuint @index, GLenum @pname, GLuint * @params);
        internal static glGetVertexAttribIuivEXTFunc glGetVertexAttribIuivEXTPtr;
        internal static void loadGetVertexAttribIuivEXT()
        {
            try
            {
                glGetVertexAttribIuivEXTPtr = (glGetVertexAttribIuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribIuivEXT"), typeof(glGetVertexAttribIuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribIuivEXT'.");
            }
        }
        public static void glGetVertexAttribIuivEXT(GLuint @index, GLenum @pname, GLuint * @params) => glGetVertexAttribIuivEXTPtr(@index, @pname, @params);
        #endregion
    }
}
