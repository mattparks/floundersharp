using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_gpu_program4
    {
        #region Interop
        static GL_NV_gpu_program4()
        {
            Console.WriteLine("Initalising GL_NV_gpu_program4 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadProgramLocalParameterI4iNV();
            loadProgramLocalParameterI4ivNV();
            loadProgramLocalParametersI4ivNV();
            loadProgramLocalParameterI4uiNV();
            loadProgramLocalParameterI4uivNV();
            loadProgramLocalParametersI4uivNV();
            loadProgramEnvParameterI4iNV();
            loadProgramEnvParameterI4ivNV();
            loadProgramEnvParametersI4ivNV();
            loadProgramEnvParameterI4uiNV();
            loadProgramEnvParameterI4uivNV();
            loadProgramEnvParametersI4uivNV();
            loadGetProgramLocalParameterIivNV();
            loadGetProgramLocalParameterIuivNV();
            loadGetProgramEnvParameterIivNV();
            loadGetProgramEnvParameterIuivNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MIN_PROGRAM_TEXEL_OFFSET_NV = 0x8904;
        public static UInt32 GL_MAX_PROGRAM_TEXEL_OFFSET_NV = 0x8905;
        public static UInt32 GL_PROGRAM_ATTRIB_COMPONENTS_NV = 0x8906;
        public static UInt32 GL_PROGRAM_RESULT_COMPONENTS_NV = 0x8907;
        public static UInt32 GL_MAX_PROGRAM_ATTRIB_COMPONENTS_NV = 0x8908;
        public static UInt32 GL_MAX_PROGRAM_RESULT_COMPONENTS_NV = 0x8909;
        public static UInt32 GL_MAX_PROGRAM_GENERIC_ATTRIBS_NV = 0x8DA5;
        public static UInt32 GL_MAX_PROGRAM_GENERIC_RESULTS_NV = 0x8DA6;
        #endregion

        #region Commands
        internal delegate void glProgramLocalParameterI4iNVFunc(GLenum @target, GLuint @index, GLint @x, GLint @y, GLint @z, GLint @w);
        internal static glProgramLocalParameterI4iNVFunc glProgramLocalParameterI4iNVPtr;
        internal static void loadProgramLocalParameterI4iNV()
        {
            try
            {
                glProgramLocalParameterI4iNVPtr = (glProgramLocalParameterI4iNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameterI4iNV"), typeof(glProgramLocalParameterI4iNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameterI4iNV'.");
            }
        }
        public static void glProgramLocalParameterI4iNV(GLenum @target, GLuint @index, GLint @x, GLint @y, GLint @z, GLint @w) => glProgramLocalParameterI4iNVPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramLocalParameterI4ivNVFunc(GLenum @target, GLuint @index, const GLint * @params);
        internal static glProgramLocalParameterI4ivNVFunc glProgramLocalParameterI4ivNVPtr;
        internal static void loadProgramLocalParameterI4ivNV()
        {
            try
            {
                glProgramLocalParameterI4ivNVPtr = (glProgramLocalParameterI4ivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameterI4ivNV"), typeof(glProgramLocalParameterI4ivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameterI4ivNV'.");
            }
        }
        public static void glProgramLocalParameterI4ivNV(GLenum @target, GLuint @index, const GLint * @params) => glProgramLocalParameterI4ivNVPtr(@target, @index, @params);

        internal delegate void glProgramLocalParametersI4ivNVFunc(GLenum @target, GLuint @index, GLsizei @count, const GLint * @params);
        internal static glProgramLocalParametersI4ivNVFunc glProgramLocalParametersI4ivNVPtr;
        internal static void loadProgramLocalParametersI4ivNV()
        {
            try
            {
                glProgramLocalParametersI4ivNVPtr = (glProgramLocalParametersI4ivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParametersI4ivNV"), typeof(glProgramLocalParametersI4ivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParametersI4ivNV'.");
            }
        }
        public static void glProgramLocalParametersI4ivNV(GLenum @target, GLuint @index, GLsizei @count, const GLint * @params) => glProgramLocalParametersI4ivNVPtr(@target, @index, @count, @params);

        internal delegate void glProgramLocalParameterI4uiNVFunc(GLenum @target, GLuint @index, GLuint @x, GLuint @y, GLuint @z, GLuint @w);
        internal static glProgramLocalParameterI4uiNVFunc glProgramLocalParameterI4uiNVPtr;
        internal static void loadProgramLocalParameterI4uiNV()
        {
            try
            {
                glProgramLocalParameterI4uiNVPtr = (glProgramLocalParameterI4uiNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameterI4uiNV"), typeof(glProgramLocalParameterI4uiNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameterI4uiNV'.");
            }
        }
        public static void glProgramLocalParameterI4uiNV(GLenum @target, GLuint @index, GLuint @x, GLuint @y, GLuint @z, GLuint @w) => glProgramLocalParameterI4uiNVPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramLocalParameterI4uivNVFunc(GLenum @target, GLuint @index, const GLuint * @params);
        internal static glProgramLocalParameterI4uivNVFunc glProgramLocalParameterI4uivNVPtr;
        internal static void loadProgramLocalParameterI4uivNV()
        {
            try
            {
                glProgramLocalParameterI4uivNVPtr = (glProgramLocalParameterI4uivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameterI4uivNV"), typeof(glProgramLocalParameterI4uivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameterI4uivNV'.");
            }
        }
        public static void glProgramLocalParameterI4uivNV(GLenum @target, GLuint @index, const GLuint * @params) => glProgramLocalParameterI4uivNVPtr(@target, @index, @params);

        internal delegate void glProgramLocalParametersI4uivNVFunc(GLenum @target, GLuint @index, GLsizei @count, const GLuint * @params);
        internal static glProgramLocalParametersI4uivNVFunc glProgramLocalParametersI4uivNVPtr;
        internal static void loadProgramLocalParametersI4uivNV()
        {
            try
            {
                glProgramLocalParametersI4uivNVPtr = (glProgramLocalParametersI4uivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParametersI4uivNV"), typeof(glProgramLocalParametersI4uivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParametersI4uivNV'.");
            }
        }
        public static void glProgramLocalParametersI4uivNV(GLenum @target, GLuint @index, GLsizei @count, const GLuint * @params) => glProgramLocalParametersI4uivNVPtr(@target, @index, @count, @params);

        internal delegate void glProgramEnvParameterI4iNVFunc(GLenum @target, GLuint @index, GLint @x, GLint @y, GLint @z, GLint @w);
        internal static glProgramEnvParameterI4iNVFunc glProgramEnvParameterI4iNVPtr;
        internal static void loadProgramEnvParameterI4iNV()
        {
            try
            {
                glProgramEnvParameterI4iNVPtr = (glProgramEnvParameterI4iNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameterI4iNV"), typeof(glProgramEnvParameterI4iNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameterI4iNV'.");
            }
        }
        public static void glProgramEnvParameterI4iNV(GLenum @target, GLuint @index, GLint @x, GLint @y, GLint @z, GLint @w) => glProgramEnvParameterI4iNVPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramEnvParameterI4ivNVFunc(GLenum @target, GLuint @index, const GLint * @params);
        internal static glProgramEnvParameterI4ivNVFunc glProgramEnvParameterI4ivNVPtr;
        internal static void loadProgramEnvParameterI4ivNV()
        {
            try
            {
                glProgramEnvParameterI4ivNVPtr = (glProgramEnvParameterI4ivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameterI4ivNV"), typeof(glProgramEnvParameterI4ivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameterI4ivNV'.");
            }
        }
        public static void glProgramEnvParameterI4ivNV(GLenum @target, GLuint @index, const GLint * @params) => glProgramEnvParameterI4ivNVPtr(@target, @index, @params);

        internal delegate void glProgramEnvParametersI4ivNVFunc(GLenum @target, GLuint @index, GLsizei @count, const GLint * @params);
        internal static glProgramEnvParametersI4ivNVFunc glProgramEnvParametersI4ivNVPtr;
        internal static void loadProgramEnvParametersI4ivNV()
        {
            try
            {
                glProgramEnvParametersI4ivNVPtr = (glProgramEnvParametersI4ivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParametersI4ivNV"), typeof(glProgramEnvParametersI4ivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParametersI4ivNV'.");
            }
        }
        public static void glProgramEnvParametersI4ivNV(GLenum @target, GLuint @index, GLsizei @count, const GLint * @params) => glProgramEnvParametersI4ivNVPtr(@target, @index, @count, @params);

        internal delegate void glProgramEnvParameterI4uiNVFunc(GLenum @target, GLuint @index, GLuint @x, GLuint @y, GLuint @z, GLuint @w);
        internal static glProgramEnvParameterI4uiNVFunc glProgramEnvParameterI4uiNVPtr;
        internal static void loadProgramEnvParameterI4uiNV()
        {
            try
            {
                glProgramEnvParameterI4uiNVPtr = (glProgramEnvParameterI4uiNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameterI4uiNV"), typeof(glProgramEnvParameterI4uiNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameterI4uiNV'.");
            }
        }
        public static void glProgramEnvParameterI4uiNV(GLenum @target, GLuint @index, GLuint @x, GLuint @y, GLuint @z, GLuint @w) => glProgramEnvParameterI4uiNVPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramEnvParameterI4uivNVFunc(GLenum @target, GLuint @index, const GLuint * @params);
        internal static glProgramEnvParameterI4uivNVFunc glProgramEnvParameterI4uivNVPtr;
        internal static void loadProgramEnvParameterI4uivNV()
        {
            try
            {
                glProgramEnvParameterI4uivNVPtr = (glProgramEnvParameterI4uivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameterI4uivNV"), typeof(glProgramEnvParameterI4uivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameterI4uivNV'.");
            }
        }
        public static void glProgramEnvParameterI4uivNV(GLenum @target, GLuint @index, const GLuint * @params) => glProgramEnvParameterI4uivNVPtr(@target, @index, @params);

        internal delegate void glProgramEnvParametersI4uivNVFunc(GLenum @target, GLuint @index, GLsizei @count, const GLuint * @params);
        internal static glProgramEnvParametersI4uivNVFunc glProgramEnvParametersI4uivNVPtr;
        internal static void loadProgramEnvParametersI4uivNV()
        {
            try
            {
                glProgramEnvParametersI4uivNVPtr = (glProgramEnvParametersI4uivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParametersI4uivNV"), typeof(glProgramEnvParametersI4uivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParametersI4uivNV'.");
            }
        }
        public static void glProgramEnvParametersI4uivNV(GLenum @target, GLuint @index, GLsizei @count, const GLuint * @params) => glProgramEnvParametersI4uivNVPtr(@target, @index, @count, @params);

        internal delegate void glGetProgramLocalParameterIivNVFunc(GLenum @target, GLuint @index, GLint * @params);
        internal static glGetProgramLocalParameterIivNVFunc glGetProgramLocalParameterIivNVPtr;
        internal static void loadGetProgramLocalParameterIivNV()
        {
            try
            {
                glGetProgramLocalParameterIivNVPtr = (glGetProgramLocalParameterIivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramLocalParameterIivNV"), typeof(glGetProgramLocalParameterIivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramLocalParameterIivNV'.");
            }
        }
        public static void glGetProgramLocalParameterIivNV(GLenum @target, GLuint @index, GLint * @params) => glGetProgramLocalParameterIivNVPtr(@target, @index, @params);

        internal delegate void glGetProgramLocalParameterIuivNVFunc(GLenum @target, GLuint @index, GLuint * @params);
        internal static glGetProgramLocalParameterIuivNVFunc glGetProgramLocalParameterIuivNVPtr;
        internal static void loadGetProgramLocalParameterIuivNV()
        {
            try
            {
                glGetProgramLocalParameterIuivNVPtr = (glGetProgramLocalParameterIuivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramLocalParameterIuivNV"), typeof(glGetProgramLocalParameterIuivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramLocalParameterIuivNV'.");
            }
        }
        public static void glGetProgramLocalParameterIuivNV(GLenum @target, GLuint @index, GLuint * @params) => glGetProgramLocalParameterIuivNVPtr(@target, @index, @params);

        internal delegate void glGetProgramEnvParameterIivNVFunc(GLenum @target, GLuint @index, GLint * @params);
        internal static glGetProgramEnvParameterIivNVFunc glGetProgramEnvParameterIivNVPtr;
        internal static void loadGetProgramEnvParameterIivNV()
        {
            try
            {
                glGetProgramEnvParameterIivNVPtr = (glGetProgramEnvParameterIivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramEnvParameterIivNV"), typeof(glGetProgramEnvParameterIivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramEnvParameterIivNV'.");
            }
        }
        public static void glGetProgramEnvParameterIivNV(GLenum @target, GLuint @index, GLint * @params) => glGetProgramEnvParameterIivNVPtr(@target, @index, @params);

        internal delegate void glGetProgramEnvParameterIuivNVFunc(GLenum @target, GLuint @index, GLuint * @params);
        internal static glGetProgramEnvParameterIuivNVFunc glGetProgramEnvParameterIuivNVPtr;
        internal static void loadGetProgramEnvParameterIuivNV()
        {
            try
            {
                glGetProgramEnvParameterIuivNVPtr = (glGetProgramEnvParameterIuivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramEnvParameterIuivNV"), typeof(glGetProgramEnvParameterIuivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramEnvParameterIuivNV'.");
            }
        }
        public static void glGetProgramEnvParameterIuivNV(GLenum @target, GLuint @index, GLuint * @params) => glGetProgramEnvParameterIuivNVPtr(@target, @index, @params);
        #endregion
    }
}
