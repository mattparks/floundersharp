using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_fence
    {
        #region Interop
        static GL_NV_fence()
        {
            Console.WriteLine("Initalising GL_NV_fence interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDeleteFencesNV();
            loadGenFencesNV();
            loadIsFenceNV();
            loadTestFenceNV();
            loadGetFenceivNV();
            loadFinishFenceNV();
            loadSetFenceNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_ALL_COMPLETED_NV = 0x84F2;
        public static UInt32 GL_FENCE_STATUS_NV = 0x84F3;
        public static UInt32 GL_FENCE_CONDITION_NV = 0x84F4;
        #endregion

        #region Commands
        internal delegate void glDeleteFencesNVFunc(GLsizei @n, const GLuint * @fences);
        internal static glDeleteFencesNVFunc glDeleteFencesNVPtr;
        internal static void loadDeleteFencesNV()
        {
            try
            {
                glDeleteFencesNVPtr = (glDeleteFencesNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteFencesNV"), typeof(glDeleteFencesNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteFencesNV'.");
            }
        }
        public static void glDeleteFencesNV(GLsizei @n, const GLuint * @fences) => glDeleteFencesNVPtr(@n, @fences);

        internal delegate void glGenFencesNVFunc(GLsizei @n, GLuint * @fences);
        internal static glGenFencesNVFunc glGenFencesNVPtr;
        internal static void loadGenFencesNV()
        {
            try
            {
                glGenFencesNVPtr = (glGenFencesNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenFencesNV"), typeof(glGenFencesNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenFencesNV'.");
            }
        }
        public static void glGenFencesNV(GLsizei @n, GLuint * @fences) => glGenFencesNVPtr(@n, @fences);

        internal delegate GLboolean glIsFenceNVFunc(GLuint @fence);
        internal static glIsFenceNVFunc glIsFenceNVPtr;
        internal static void loadIsFenceNV()
        {
            try
            {
                glIsFenceNVPtr = (glIsFenceNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsFenceNV"), typeof(glIsFenceNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsFenceNV'.");
            }
        }
        public static GLboolean glIsFenceNV(GLuint @fence) => glIsFenceNVPtr(@fence);

        internal delegate GLboolean glTestFenceNVFunc(GLuint @fence);
        internal static glTestFenceNVFunc glTestFenceNVPtr;
        internal static void loadTestFenceNV()
        {
            try
            {
                glTestFenceNVPtr = (glTestFenceNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTestFenceNV"), typeof(glTestFenceNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTestFenceNV'.");
            }
        }
        public static GLboolean glTestFenceNV(GLuint @fence) => glTestFenceNVPtr(@fence);

        internal delegate void glGetFenceivNVFunc(GLuint @fence, GLenum @pname, GLint * @params);
        internal static glGetFenceivNVFunc glGetFenceivNVPtr;
        internal static void loadGetFenceivNV()
        {
            try
            {
                glGetFenceivNVPtr = (glGetFenceivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFenceivNV"), typeof(glGetFenceivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFenceivNV'.");
            }
        }
        public static void glGetFenceivNV(GLuint @fence, GLenum @pname, GLint * @params) => glGetFenceivNVPtr(@fence, @pname, @params);

        internal delegate void glFinishFenceNVFunc(GLuint @fence);
        internal static glFinishFenceNVFunc glFinishFenceNVPtr;
        internal static void loadFinishFenceNV()
        {
            try
            {
                glFinishFenceNVPtr = (glFinishFenceNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFinishFenceNV"), typeof(glFinishFenceNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFinishFenceNV'.");
            }
        }
        public static void glFinishFenceNV(GLuint @fence) => glFinishFenceNVPtr(@fence);

        internal delegate void glSetFenceNVFunc(GLuint @fence, GLenum @condition);
        internal static glSetFenceNVFunc glSetFenceNVPtr;
        internal static void loadSetFenceNV()
        {
            try
            {
                glSetFenceNVPtr = (glSetFenceNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSetFenceNV"), typeof(glSetFenceNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSetFenceNV'.");
            }
        }
        public static void glSetFenceNV(GLuint @fence, GLenum @condition) => glSetFenceNVPtr(@fence, @condition);
        #endregion
    }
}
