using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_vdpau_interop
    {
        #region Interop
        static GL_NV_vdpau_interop()
        {
            Console.WriteLine("Initalising GL_NV_vdpau_interop interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVDPAUInitNV();
            loadVDPAUFiniNV();
            loadVDPAURegisterVideoSurfaceNV();
            loadVDPAURegisterOutputSurfaceNV();
            loadVDPAUIsSurfaceNV();
            loadVDPAUUnregisterSurfaceNV();
            loadVDPAUGetSurfaceivNV();
            loadVDPAUSurfaceAccessNV();
            loadVDPAUMapSurfacesNV();
            loadVDPAUUnmapSurfacesNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SURFACE_STATE_NV = 0x86EB;
        public static UInt32 GL_SURFACE_REGISTERED_NV = 0x86FD;
        public static UInt32 GL_SURFACE_MAPPED_NV = 0x8700;
        public static UInt32 GL_WRITE_DISCARD_NV = 0x88BE;
        #endregion

        #region Commands
        internal delegate void glVDPAUInitNVFunc(const void * @vdpDevice, const void * @getProcAddress);
        internal static glVDPAUInitNVFunc glVDPAUInitNVPtr;
        internal static void loadVDPAUInitNV()
        {
            try
            {
                glVDPAUInitNVPtr = (glVDPAUInitNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVDPAUInitNV"), typeof(glVDPAUInitNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVDPAUInitNV'.");
            }
        }
        public static void glVDPAUInitNV(const void * @vdpDevice, const void * @getProcAddress) => glVDPAUInitNVPtr(@vdpDevice, @getProcAddress);

        internal delegate void glVDPAUFiniNVFunc();
        internal static glVDPAUFiniNVFunc glVDPAUFiniNVPtr;
        internal static void loadVDPAUFiniNV()
        {
            try
            {
                glVDPAUFiniNVPtr = (glVDPAUFiniNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVDPAUFiniNV"), typeof(glVDPAUFiniNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVDPAUFiniNV'.");
            }
        }
        public static void glVDPAUFiniNV() => glVDPAUFiniNVPtr();

        internal delegate GLvdpauSurfaceNV glVDPAURegisterVideoSurfaceNVFunc(const void * @vdpSurface, GLenum @target, GLsizei @numTextureNames, const GLuint * @textureNames);
        internal static glVDPAURegisterVideoSurfaceNVFunc glVDPAURegisterVideoSurfaceNVPtr;
        internal static void loadVDPAURegisterVideoSurfaceNV()
        {
            try
            {
                glVDPAURegisterVideoSurfaceNVPtr = (glVDPAURegisterVideoSurfaceNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVDPAURegisterVideoSurfaceNV"), typeof(glVDPAURegisterVideoSurfaceNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVDPAURegisterVideoSurfaceNV'.");
            }
        }
        public static GLvdpauSurfaceNV glVDPAURegisterVideoSurfaceNV(const void * @vdpSurface, GLenum @target, GLsizei @numTextureNames, const GLuint * @textureNames) => glVDPAURegisterVideoSurfaceNVPtr(@vdpSurface, @target, @numTextureNames, @textureNames);

        internal delegate GLvdpauSurfaceNV glVDPAURegisterOutputSurfaceNVFunc(const void * @vdpSurface, GLenum @target, GLsizei @numTextureNames, const GLuint * @textureNames);
        internal static glVDPAURegisterOutputSurfaceNVFunc glVDPAURegisterOutputSurfaceNVPtr;
        internal static void loadVDPAURegisterOutputSurfaceNV()
        {
            try
            {
                glVDPAURegisterOutputSurfaceNVPtr = (glVDPAURegisterOutputSurfaceNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVDPAURegisterOutputSurfaceNV"), typeof(glVDPAURegisterOutputSurfaceNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVDPAURegisterOutputSurfaceNV'.");
            }
        }
        public static GLvdpauSurfaceNV glVDPAURegisterOutputSurfaceNV(const void * @vdpSurface, GLenum @target, GLsizei @numTextureNames, const GLuint * @textureNames) => glVDPAURegisterOutputSurfaceNVPtr(@vdpSurface, @target, @numTextureNames, @textureNames);

        internal delegate GLboolean glVDPAUIsSurfaceNVFunc(GLvdpauSurfaceNV @surface);
        internal static glVDPAUIsSurfaceNVFunc glVDPAUIsSurfaceNVPtr;
        internal static void loadVDPAUIsSurfaceNV()
        {
            try
            {
                glVDPAUIsSurfaceNVPtr = (glVDPAUIsSurfaceNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVDPAUIsSurfaceNV"), typeof(glVDPAUIsSurfaceNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVDPAUIsSurfaceNV'.");
            }
        }
        public static GLboolean glVDPAUIsSurfaceNV(GLvdpauSurfaceNV @surface) => glVDPAUIsSurfaceNVPtr(@surface);

        internal delegate void glVDPAUUnregisterSurfaceNVFunc(GLvdpauSurfaceNV @surface);
        internal static glVDPAUUnregisterSurfaceNVFunc glVDPAUUnregisterSurfaceNVPtr;
        internal static void loadVDPAUUnregisterSurfaceNV()
        {
            try
            {
                glVDPAUUnregisterSurfaceNVPtr = (glVDPAUUnregisterSurfaceNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVDPAUUnregisterSurfaceNV"), typeof(glVDPAUUnregisterSurfaceNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVDPAUUnregisterSurfaceNV'.");
            }
        }
        public static void glVDPAUUnregisterSurfaceNV(GLvdpauSurfaceNV @surface) => glVDPAUUnregisterSurfaceNVPtr(@surface);

        internal delegate void glVDPAUGetSurfaceivNVFunc(GLvdpauSurfaceNV @surface, GLenum @pname, GLsizei @bufSize, GLsizei * @length, GLint * @values);
        internal static glVDPAUGetSurfaceivNVFunc glVDPAUGetSurfaceivNVPtr;
        internal static void loadVDPAUGetSurfaceivNV()
        {
            try
            {
                glVDPAUGetSurfaceivNVPtr = (glVDPAUGetSurfaceivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVDPAUGetSurfaceivNV"), typeof(glVDPAUGetSurfaceivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVDPAUGetSurfaceivNV'.");
            }
        }
        public static void glVDPAUGetSurfaceivNV(GLvdpauSurfaceNV @surface, GLenum @pname, GLsizei @bufSize, GLsizei * @length, GLint * @values) => glVDPAUGetSurfaceivNVPtr(@surface, @pname, @bufSize, @length, @values);

        internal delegate void glVDPAUSurfaceAccessNVFunc(GLvdpauSurfaceNV @surface, GLenum @access);
        internal static glVDPAUSurfaceAccessNVFunc glVDPAUSurfaceAccessNVPtr;
        internal static void loadVDPAUSurfaceAccessNV()
        {
            try
            {
                glVDPAUSurfaceAccessNVPtr = (glVDPAUSurfaceAccessNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVDPAUSurfaceAccessNV"), typeof(glVDPAUSurfaceAccessNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVDPAUSurfaceAccessNV'.");
            }
        }
        public static void glVDPAUSurfaceAccessNV(GLvdpauSurfaceNV @surface, GLenum @access) => glVDPAUSurfaceAccessNVPtr(@surface, @access);

        internal delegate void glVDPAUMapSurfacesNVFunc(GLsizei @numSurfaces, const GLvdpauSurfaceNV * @surfaces);
        internal static glVDPAUMapSurfacesNVFunc glVDPAUMapSurfacesNVPtr;
        internal static void loadVDPAUMapSurfacesNV()
        {
            try
            {
                glVDPAUMapSurfacesNVPtr = (glVDPAUMapSurfacesNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVDPAUMapSurfacesNV"), typeof(glVDPAUMapSurfacesNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVDPAUMapSurfacesNV'.");
            }
        }
        public static void glVDPAUMapSurfacesNV(GLsizei @numSurfaces, const GLvdpauSurfaceNV * @surfaces) => glVDPAUMapSurfacesNVPtr(@numSurfaces, @surfaces);

        internal delegate void glVDPAUUnmapSurfacesNVFunc(GLsizei @numSurface, const GLvdpauSurfaceNV * @surfaces);
        internal static glVDPAUUnmapSurfacesNVFunc glVDPAUUnmapSurfacesNVPtr;
        internal static void loadVDPAUUnmapSurfacesNV()
        {
            try
            {
                glVDPAUUnmapSurfacesNVPtr = (glVDPAUUnmapSurfacesNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVDPAUUnmapSurfacesNV"), typeof(glVDPAUUnmapSurfacesNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVDPAUUnmapSurfacesNV'.");
            }
        }
        public static void glVDPAUUnmapSurfacesNV(GLsizei @numSurface, const GLvdpauSurfaceNV * @surfaces) => glVDPAUUnmapSurfacesNVPtr(@numSurface, @surfaces);
        #endregion
    }
}
