using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_video_capture
    {
        #region Interop
        static GL_NV_video_capture()
        {
            Console.WriteLine("Initalising GL_NV_video_capture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBeginVideoCaptureNV();
            loadBindVideoCaptureStreamBufferNV();
            loadBindVideoCaptureStreamTextureNV();
            loadEndVideoCaptureNV();
            loadGetVideoCaptureivNV();
            loadGetVideoCaptureStreamivNV();
            loadGetVideoCaptureStreamfvNV();
            loadGetVideoCaptureStreamdvNV();
            loadVideoCaptureNV();
            loadVideoCaptureStreamParameterivNV();
            loadVideoCaptureStreamParameterfvNV();
            loadVideoCaptureStreamParameterdvNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VIDEO_BUFFER_NV = 0x9020;
        public static UInt32 GL_VIDEO_BUFFER_BINDING_NV = 0x9021;
        public static UInt32 GL_FIELD_UPPER_NV = 0x9022;
        public static UInt32 GL_FIELD_LOWER_NV = 0x9023;
        public static UInt32 GL_NUM_VIDEO_CAPTURE_STREAMS_NV = 0x9024;
        public static UInt32 GL_NEXT_VIDEO_CAPTURE_BUFFER_STATUS_NV = 0x9025;
        public static UInt32 GL_VIDEO_CAPTURE_TO_422_SUPPORTED_NV = 0x9026;
        public static UInt32 GL_LAST_VIDEO_CAPTURE_STATUS_NV = 0x9027;
        public static UInt32 GL_VIDEO_BUFFER_PITCH_NV = 0x9028;
        public static UInt32 GL_VIDEO_COLOR_CONVERSION_MATRIX_NV = 0x9029;
        public static UInt32 GL_VIDEO_COLOR_CONVERSION_MAX_NV = 0x902A;
        public static UInt32 GL_VIDEO_COLOR_CONVERSION_MIN_NV = 0x902B;
        public static UInt32 GL_VIDEO_COLOR_CONVERSION_OFFSET_NV = 0x902C;
        public static UInt32 GL_VIDEO_BUFFER_INTERNAL_FORMAT_NV = 0x902D;
        public static UInt32 GL_PARTIAL_SUCCESS_NV = 0x902E;
        public static UInt32 GL_SUCCESS_NV = 0x902F;
        public static UInt32 GL_FAILURE_NV = 0x9030;
        public static UInt32 GL_YCBYCR8_422_NV = 0x9031;
        public static UInt32 GL_YCBAYCR8A_4224_NV = 0x9032;
        public static UInt32 GL_Z6Y10Z6CB10Z6Y10Z6CR10_422_NV = 0x9033;
        public static UInt32 GL_Z6Y10Z6CB10Z6A10Z6Y10Z6CR10Z6A10_4224_NV = 0x9034;
        public static UInt32 GL_Z4Y12Z4CB12Z4Y12Z4CR12_422_NV = 0x9035;
        public static UInt32 GL_Z4Y12Z4CB12Z4A12Z4Y12Z4CR12Z4A12_4224_NV = 0x9036;
        public static UInt32 GL_Z4Y12Z4CB12Z4CR12_444_NV = 0x9037;
        public static UInt32 GL_VIDEO_CAPTURE_FRAME_WIDTH_NV = 0x9038;
        public static UInt32 GL_VIDEO_CAPTURE_FRAME_HEIGHT_NV = 0x9039;
        public static UInt32 GL_VIDEO_CAPTURE_FIELD_UPPER_HEIGHT_NV = 0x903A;
        public static UInt32 GL_VIDEO_CAPTURE_FIELD_LOWER_HEIGHT_NV = 0x903B;
        public static UInt32 GL_VIDEO_CAPTURE_SURFACE_ORIGIN_NV = 0x903C;
        #endregion

        #region Commands
        internal delegate void glBeginVideoCaptureNVFunc(GLuint @video_capture_slot);
        internal static glBeginVideoCaptureNVFunc glBeginVideoCaptureNVPtr;
        internal static void loadBeginVideoCaptureNV()
        {
            try
            {
                glBeginVideoCaptureNVPtr = (glBeginVideoCaptureNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginVideoCaptureNV"), typeof(glBeginVideoCaptureNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginVideoCaptureNV'.");
            }
        }
        public static void glBeginVideoCaptureNV(GLuint @video_capture_slot) => glBeginVideoCaptureNVPtr(@video_capture_slot);

        internal delegate void glBindVideoCaptureStreamBufferNVFunc(GLuint @video_capture_slot, GLuint @stream, GLenum @frame_region, GLintptrARB @offset);
        internal static glBindVideoCaptureStreamBufferNVFunc glBindVideoCaptureStreamBufferNVPtr;
        internal static void loadBindVideoCaptureStreamBufferNV()
        {
            try
            {
                glBindVideoCaptureStreamBufferNVPtr = (glBindVideoCaptureStreamBufferNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVideoCaptureStreamBufferNV"), typeof(glBindVideoCaptureStreamBufferNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVideoCaptureStreamBufferNV'.");
            }
        }
        public static void glBindVideoCaptureStreamBufferNV(GLuint @video_capture_slot, GLuint @stream, GLenum @frame_region, GLintptrARB @offset) => glBindVideoCaptureStreamBufferNVPtr(@video_capture_slot, @stream, @frame_region, @offset);

        internal delegate void glBindVideoCaptureStreamTextureNVFunc(GLuint @video_capture_slot, GLuint @stream, GLenum @frame_region, GLenum @target, GLuint @texture);
        internal static glBindVideoCaptureStreamTextureNVFunc glBindVideoCaptureStreamTextureNVPtr;
        internal static void loadBindVideoCaptureStreamTextureNV()
        {
            try
            {
                glBindVideoCaptureStreamTextureNVPtr = (glBindVideoCaptureStreamTextureNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVideoCaptureStreamTextureNV"), typeof(glBindVideoCaptureStreamTextureNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVideoCaptureStreamTextureNV'.");
            }
        }
        public static void glBindVideoCaptureStreamTextureNV(GLuint @video_capture_slot, GLuint @stream, GLenum @frame_region, GLenum @target, GLuint @texture) => glBindVideoCaptureStreamTextureNVPtr(@video_capture_slot, @stream, @frame_region, @target, @texture);

        internal delegate void glEndVideoCaptureNVFunc(GLuint @video_capture_slot);
        internal static glEndVideoCaptureNVFunc glEndVideoCaptureNVPtr;
        internal static void loadEndVideoCaptureNV()
        {
            try
            {
                glEndVideoCaptureNVPtr = (glEndVideoCaptureNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndVideoCaptureNV"), typeof(glEndVideoCaptureNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndVideoCaptureNV'.");
            }
        }
        public static void glEndVideoCaptureNV(GLuint @video_capture_slot) => glEndVideoCaptureNVPtr(@video_capture_slot);

        internal delegate void glGetVideoCaptureivNVFunc(GLuint @video_capture_slot, GLenum @pname, GLint * @params);
        internal static glGetVideoCaptureivNVFunc glGetVideoCaptureivNVPtr;
        internal static void loadGetVideoCaptureivNV()
        {
            try
            {
                glGetVideoCaptureivNVPtr = (glGetVideoCaptureivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVideoCaptureivNV"), typeof(glGetVideoCaptureivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVideoCaptureivNV'.");
            }
        }
        public static void glGetVideoCaptureivNV(GLuint @video_capture_slot, GLenum @pname, GLint * @params) => glGetVideoCaptureivNVPtr(@video_capture_slot, @pname, @params);

        internal delegate void glGetVideoCaptureStreamivNVFunc(GLuint @video_capture_slot, GLuint @stream, GLenum @pname, GLint * @params);
        internal static glGetVideoCaptureStreamivNVFunc glGetVideoCaptureStreamivNVPtr;
        internal static void loadGetVideoCaptureStreamivNV()
        {
            try
            {
                glGetVideoCaptureStreamivNVPtr = (glGetVideoCaptureStreamivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVideoCaptureStreamivNV"), typeof(glGetVideoCaptureStreamivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVideoCaptureStreamivNV'.");
            }
        }
        public static void glGetVideoCaptureStreamivNV(GLuint @video_capture_slot, GLuint @stream, GLenum @pname, GLint * @params) => glGetVideoCaptureStreamivNVPtr(@video_capture_slot, @stream, @pname, @params);

        internal delegate void glGetVideoCaptureStreamfvNVFunc(GLuint @video_capture_slot, GLuint @stream, GLenum @pname, GLfloat * @params);
        internal static glGetVideoCaptureStreamfvNVFunc glGetVideoCaptureStreamfvNVPtr;
        internal static void loadGetVideoCaptureStreamfvNV()
        {
            try
            {
                glGetVideoCaptureStreamfvNVPtr = (glGetVideoCaptureStreamfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVideoCaptureStreamfvNV"), typeof(glGetVideoCaptureStreamfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVideoCaptureStreamfvNV'.");
            }
        }
        public static void glGetVideoCaptureStreamfvNV(GLuint @video_capture_slot, GLuint @stream, GLenum @pname, GLfloat * @params) => glGetVideoCaptureStreamfvNVPtr(@video_capture_slot, @stream, @pname, @params);

        internal delegate void glGetVideoCaptureStreamdvNVFunc(GLuint @video_capture_slot, GLuint @stream, GLenum @pname, GLdouble * @params);
        internal static glGetVideoCaptureStreamdvNVFunc glGetVideoCaptureStreamdvNVPtr;
        internal static void loadGetVideoCaptureStreamdvNV()
        {
            try
            {
                glGetVideoCaptureStreamdvNVPtr = (glGetVideoCaptureStreamdvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVideoCaptureStreamdvNV"), typeof(glGetVideoCaptureStreamdvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVideoCaptureStreamdvNV'.");
            }
        }
        public static void glGetVideoCaptureStreamdvNV(GLuint @video_capture_slot, GLuint @stream, GLenum @pname, GLdouble * @params) => glGetVideoCaptureStreamdvNVPtr(@video_capture_slot, @stream, @pname, @params);

        internal delegate GLenum glVideoCaptureNVFunc(GLuint @video_capture_slot, GLuint * @sequence_num, GLuint64EXT * @capture_time);
        internal static glVideoCaptureNVFunc glVideoCaptureNVPtr;
        internal static void loadVideoCaptureNV()
        {
            try
            {
                glVideoCaptureNVPtr = (glVideoCaptureNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVideoCaptureNV"), typeof(glVideoCaptureNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVideoCaptureNV'.");
            }
        }
        public static GLenum glVideoCaptureNV(GLuint @video_capture_slot, GLuint * @sequence_num, GLuint64EXT * @capture_time) => glVideoCaptureNVPtr(@video_capture_slot, @sequence_num, @capture_time);

        internal delegate void glVideoCaptureStreamParameterivNVFunc(GLuint @video_capture_slot, GLuint @stream, GLenum @pname, const GLint * @params);
        internal static glVideoCaptureStreamParameterivNVFunc glVideoCaptureStreamParameterivNVPtr;
        internal static void loadVideoCaptureStreamParameterivNV()
        {
            try
            {
                glVideoCaptureStreamParameterivNVPtr = (glVideoCaptureStreamParameterivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVideoCaptureStreamParameterivNV"), typeof(glVideoCaptureStreamParameterivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVideoCaptureStreamParameterivNV'.");
            }
        }
        public static void glVideoCaptureStreamParameterivNV(GLuint @video_capture_slot, GLuint @stream, GLenum @pname, const GLint * @params) => glVideoCaptureStreamParameterivNVPtr(@video_capture_slot, @stream, @pname, @params);

        internal delegate void glVideoCaptureStreamParameterfvNVFunc(GLuint @video_capture_slot, GLuint @stream, GLenum @pname, const GLfloat * @params);
        internal static glVideoCaptureStreamParameterfvNVFunc glVideoCaptureStreamParameterfvNVPtr;
        internal static void loadVideoCaptureStreamParameterfvNV()
        {
            try
            {
                glVideoCaptureStreamParameterfvNVPtr = (glVideoCaptureStreamParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVideoCaptureStreamParameterfvNV"), typeof(glVideoCaptureStreamParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVideoCaptureStreamParameterfvNV'.");
            }
        }
        public static void glVideoCaptureStreamParameterfvNV(GLuint @video_capture_slot, GLuint @stream, GLenum @pname, const GLfloat * @params) => glVideoCaptureStreamParameterfvNVPtr(@video_capture_slot, @stream, @pname, @params);

        internal delegate void glVideoCaptureStreamParameterdvNVFunc(GLuint @video_capture_slot, GLuint @stream, GLenum @pname, const GLdouble * @params);
        internal static glVideoCaptureStreamParameterdvNVFunc glVideoCaptureStreamParameterdvNVPtr;
        internal static void loadVideoCaptureStreamParameterdvNV()
        {
            try
            {
                glVideoCaptureStreamParameterdvNVPtr = (glVideoCaptureStreamParameterdvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVideoCaptureStreamParameterdvNV"), typeof(glVideoCaptureStreamParameterdvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVideoCaptureStreamParameterdvNV'.");
            }
        }
        public static void glVideoCaptureStreamParameterdvNV(GLuint @video_capture_slot, GLuint @stream, GLenum @pname, const GLdouble * @params) => glVideoCaptureStreamParameterdvNVPtr(@video_capture_slot, @stream, @pname, @params);
        #endregion
    }
}
