using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_transform_feedback
    {
        #region Interop
        static GL_NV_transform_feedback()
        {
            Console.WriteLine("Initalising GL_NV_transform_feedback interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBeginTransformFeedbackNV();
            loadEndTransformFeedbackNV();
            loadTransformFeedbackAttribsNV();
            loadBindBufferRangeNV();
            loadBindBufferOffsetNV();
            loadBindBufferBaseNV();
            loadTransformFeedbackVaryingsNV();
            loadActiveVaryingNV();
            loadGetVaryingLocationNV();
            loadGetActiveVaryingNV();
            loadGetTransformFeedbackVaryingNV();
            loadTransformFeedbackStreamAttribsNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BACK_PRIMARY_COLOR_NV = 0x8C77;
        public static UInt32 GL_BACK_SECONDARY_COLOR_NV = 0x8C78;
        public static UInt32 GL_TEXTURE_COORD_NV = 0x8C79;
        public static UInt32 GL_CLIP_DISTANCE_NV = 0x8C7A;
        public static UInt32 GL_VERTEX_ID_NV = 0x8C7B;
        public static UInt32 GL_PRIMITIVE_ID_NV = 0x8C7C;
        public static UInt32 GL_GENERIC_ATTRIB_NV = 0x8C7D;
        public static UInt32 GL_TRANSFORM_FEEDBACK_ATTRIBS_NV = 0x8C7E;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_MODE_NV = 0x8C7F;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS_NV = 0x8C80;
        public static UInt32 GL_ACTIVE_VARYINGS_NV = 0x8C81;
        public static UInt32 GL_ACTIVE_VARYING_MAX_LENGTH_NV = 0x8C82;
        public static UInt32 GL_TRANSFORM_FEEDBACK_VARYINGS_NV = 0x8C83;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_START_NV = 0x8C84;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_SIZE_NV = 0x8C85;
        public static UInt32 GL_TRANSFORM_FEEDBACK_RECORD_NV = 0x8C86;
        public static UInt32 GL_PRIMITIVES_GENERATED_NV = 0x8C87;
        public static UInt32 GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN_NV = 0x8C88;
        public static UInt32 GL_RASTERIZER_DISCARD_NV = 0x8C89;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS_NV = 0x8C8A;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS_NV = 0x8C8B;
        public static UInt32 GL_INTERLEAVED_ATTRIBS_NV = 0x8C8C;
        public static UInt32 GL_SEPARATE_ATTRIBS_NV = 0x8C8D;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_NV = 0x8C8E;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_BINDING_NV = 0x8C8F;
        public static UInt32 GL_LAYER_NV = 0x8DAA;
        public static Int32 GL_NEXT_BUFFER_NV = -2;
        public static Int32 GL_SKIP_COMPONENTS4_NV = -3;
        public static Int32 GL_SKIP_COMPONENTS3_NV = -4;
        public static Int32 GL_SKIP_COMPONENTS2_NV = -5;
        public static Int32 GL_SKIP_COMPONENTS1_NV = -6;
        #endregion

        #region Commands
        internal delegate void glBeginTransformFeedbackNVFunc(GLenum @primitiveMode);
        internal static glBeginTransformFeedbackNVFunc glBeginTransformFeedbackNVPtr;
        internal static void loadBeginTransformFeedbackNV()
        {
            try
            {
                glBeginTransformFeedbackNVPtr = (glBeginTransformFeedbackNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginTransformFeedbackNV"), typeof(glBeginTransformFeedbackNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginTransformFeedbackNV'.");
            }
        }
        public static void glBeginTransformFeedbackNV(GLenum @primitiveMode) => glBeginTransformFeedbackNVPtr(@primitiveMode);

        internal delegate void glEndTransformFeedbackNVFunc();
        internal static glEndTransformFeedbackNVFunc glEndTransformFeedbackNVPtr;
        internal static void loadEndTransformFeedbackNV()
        {
            try
            {
                glEndTransformFeedbackNVPtr = (glEndTransformFeedbackNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndTransformFeedbackNV"), typeof(glEndTransformFeedbackNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndTransformFeedbackNV'.");
            }
        }
        public static void glEndTransformFeedbackNV() => glEndTransformFeedbackNVPtr();

        internal delegate void glTransformFeedbackAttribsNVFunc(GLsizei @count, const GLint * @attribs, GLenum @bufferMode);
        internal static glTransformFeedbackAttribsNVFunc glTransformFeedbackAttribsNVPtr;
        internal static void loadTransformFeedbackAttribsNV()
        {
            try
            {
                glTransformFeedbackAttribsNVPtr = (glTransformFeedbackAttribsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTransformFeedbackAttribsNV"), typeof(glTransformFeedbackAttribsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTransformFeedbackAttribsNV'.");
            }
        }
        public static void glTransformFeedbackAttribsNV(GLsizei @count, const GLint * @attribs, GLenum @bufferMode) => glTransformFeedbackAttribsNVPtr(@count, @attribs, @bufferMode);

        internal delegate void glBindBufferRangeNVFunc(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glBindBufferRangeNVFunc glBindBufferRangeNVPtr;
        internal static void loadBindBufferRangeNV()
        {
            try
            {
                glBindBufferRangeNVPtr = (glBindBufferRangeNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferRangeNV"), typeof(glBindBufferRangeNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferRangeNV'.");
            }
        }
        public static void glBindBufferRangeNV(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glBindBufferRangeNVPtr(@target, @index, @buffer, @offset, @size);

        internal delegate void glBindBufferOffsetNVFunc(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset);
        internal static glBindBufferOffsetNVFunc glBindBufferOffsetNVPtr;
        internal static void loadBindBufferOffsetNV()
        {
            try
            {
                glBindBufferOffsetNVPtr = (glBindBufferOffsetNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferOffsetNV"), typeof(glBindBufferOffsetNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferOffsetNV'.");
            }
        }
        public static void glBindBufferOffsetNV(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset) => glBindBufferOffsetNVPtr(@target, @index, @buffer, @offset);

        internal delegate void glBindBufferBaseNVFunc(GLenum @target, GLuint @index, GLuint @buffer);
        internal static glBindBufferBaseNVFunc glBindBufferBaseNVPtr;
        internal static void loadBindBufferBaseNV()
        {
            try
            {
                glBindBufferBaseNVPtr = (glBindBufferBaseNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferBaseNV"), typeof(glBindBufferBaseNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferBaseNV'.");
            }
        }
        public static void glBindBufferBaseNV(GLenum @target, GLuint @index, GLuint @buffer) => glBindBufferBaseNVPtr(@target, @index, @buffer);

        internal delegate void glTransformFeedbackVaryingsNVFunc(GLuint @program, GLsizei @count, const GLint * @locations, GLenum @bufferMode);
        internal static glTransformFeedbackVaryingsNVFunc glTransformFeedbackVaryingsNVPtr;
        internal static void loadTransformFeedbackVaryingsNV()
        {
            try
            {
                glTransformFeedbackVaryingsNVPtr = (glTransformFeedbackVaryingsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTransformFeedbackVaryingsNV"), typeof(glTransformFeedbackVaryingsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTransformFeedbackVaryingsNV'.");
            }
        }
        public static void glTransformFeedbackVaryingsNV(GLuint @program, GLsizei @count, const GLint * @locations, GLenum @bufferMode) => glTransformFeedbackVaryingsNVPtr(@program, @count, @locations, @bufferMode);

        internal delegate void glActiveVaryingNVFunc(GLuint @program, const GLchar * @name);
        internal static glActiveVaryingNVFunc glActiveVaryingNVPtr;
        internal static void loadActiveVaryingNV()
        {
            try
            {
                glActiveVaryingNVPtr = (glActiveVaryingNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glActiveVaryingNV"), typeof(glActiveVaryingNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glActiveVaryingNV'.");
            }
        }
        public static void glActiveVaryingNV(GLuint @program, const GLchar * @name) => glActiveVaryingNVPtr(@program, @name);

        internal delegate GLint glGetVaryingLocationNVFunc(GLuint @program, const GLchar * @name);
        internal static glGetVaryingLocationNVFunc glGetVaryingLocationNVPtr;
        internal static void loadGetVaryingLocationNV()
        {
            try
            {
                glGetVaryingLocationNVPtr = (glGetVaryingLocationNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVaryingLocationNV"), typeof(glGetVaryingLocationNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVaryingLocationNV'.");
            }
        }
        public static GLint glGetVaryingLocationNV(GLuint @program, const GLchar * @name) => glGetVaryingLocationNVPtr(@program, @name);

        internal delegate void glGetActiveVaryingNVFunc(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLsizei * @size, GLenum * @type, GLchar * @name);
        internal static glGetActiveVaryingNVFunc glGetActiveVaryingNVPtr;
        internal static void loadGetActiveVaryingNV()
        {
            try
            {
                glGetActiveVaryingNVPtr = (glGetActiveVaryingNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveVaryingNV"), typeof(glGetActiveVaryingNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveVaryingNV'.");
            }
        }
        public static void glGetActiveVaryingNV(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLsizei * @size, GLenum * @type, GLchar * @name) => glGetActiveVaryingNVPtr(@program, @index, @bufSize, @length, @size, @type, @name);

        internal delegate void glGetTransformFeedbackVaryingNVFunc(GLuint @program, GLuint @index, GLint * @location);
        internal static glGetTransformFeedbackVaryingNVFunc glGetTransformFeedbackVaryingNVPtr;
        internal static void loadGetTransformFeedbackVaryingNV()
        {
            try
            {
                glGetTransformFeedbackVaryingNVPtr = (glGetTransformFeedbackVaryingNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTransformFeedbackVaryingNV"), typeof(glGetTransformFeedbackVaryingNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTransformFeedbackVaryingNV'.");
            }
        }
        public static void glGetTransformFeedbackVaryingNV(GLuint @program, GLuint @index, GLint * @location) => glGetTransformFeedbackVaryingNVPtr(@program, @index, @location);

        internal delegate void glTransformFeedbackStreamAttribsNVFunc(GLsizei @count, const GLint * @attribs, GLsizei @nbuffers, const GLint * @bufstreams, GLenum @bufferMode);
        internal static glTransformFeedbackStreamAttribsNVFunc glTransformFeedbackStreamAttribsNVPtr;
        internal static void loadTransformFeedbackStreamAttribsNV()
        {
            try
            {
                glTransformFeedbackStreamAttribsNVPtr = (glTransformFeedbackStreamAttribsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTransformFeedbackStreamAttribsNV"), typeof(glTransformFeedbackStreamAttribsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTransformFeedbackStreamAttribsNV'.");
            }
        }
        public static void glTransformFeedbackStreamAttribsNV(GLsizei @count, const GLint * @attribs, GLsizei @nbuffers, const GLint * @bufstreams, GLenum @bufferMode) => glTransformFeedbackStreamAttribsNVPtr(@count, @attribs, @nbuffers, @bufstreams, @bufferMode);
        #endregion
    }
}
