using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_copy_image
    {
        #region Interop
        static GL_NV_copy_image()
        {
            Console.WriteLine("Initalising GL_NV_copy_image interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCopyImageSubDataNV();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glCopyImageSubDataNVFunc(GLuint @srcName, GLenum @srcTarget, GLint @srcLevel, GLint @srcX, GLint @srcY, GLint @srcZ, GLuint @dstName, GLenum @dstTarget, GLint @dstLevel, GLint @dstX, GLint @dstY, GLint @dstZ, GLsizei @width, GLsizei @height, GLsizei @depth);
        internal static glCopyImageSubDataNVFunc glCopyImageSubDataNVPtr;
        internal static void loadCopyImageSubDataNV()
        {
            try
            {
                glCopyImageSubDataNVPtr = (glCopyImageSubDataNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyImageSubDataNV"), typeof(glCopyImageSubDataNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyImageSubDataNV'.");
            }
        }
        public static void glCopyImageSubDataNV(GLuint @srcName, GLenum @srcTarget, GLint @srcLevel, GLint @srcX, GLint @srcY, GLint @srcZ, GLuint @dstName, GLenum @dstTarget, GLint @dstLevel, GLint @dstX, GLint @dstY, GLint @dstZ, GLsizei @width, GLsizei @height, GLsizei @depth) => glCopyImageSubDataNVPtr(@srcName, @srcTarget, @srcLevel, @srcX, @srcY, @srcZ, @dstName, @dstTarget, @dstLevel, @dstX, @dstY, @dstZ, @width, @height, @depth);
        #endregion
    }
}
