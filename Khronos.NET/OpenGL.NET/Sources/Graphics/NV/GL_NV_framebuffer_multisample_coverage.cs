using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_framebuffer_multisample_coverage
    {
        #region Interop
        static GL_NV_framebuffer_multisample_coverage()
        {
            Console.WriteLine("Initalising GL_NV_framebuffer_multisample_coverage interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadRenderbufferStorageMultisampleCoverageNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RENDERBUFFER_COVERAGE_SAMPLES_NV = 0x8CAB;
        public static UInt32 GL_RENDERBUFFER_COLOR_SAMPLES_NV = 0x8E10;
        public static UInt32 GL_MAX_MULTISAMPLE_COVERAGE_MODES_NV = 0x8E11;
        public static UInt32 GL_MULTISAMPLE_COVERAGE_MODES_NV = 0x8E12;
        #endregion

        #region Commands
        internal delegate void glRenderbufferStorageMultisampleCoverageNVFunc(GLenum @target, GLsizei @coverageSamples, GLsizei @colorSamples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageMultisampleCoverageNVFunc glRenderbufferStorageMultisampleCoverageNVPtr;
        internal static void loadRenderbufferStorageMultisampleCoverageNV()
        {
            try
            {
                glRenderbufferStorageMultisampleCoverageNVPtr = (glRenderbufferStorageMultisampleCoverageNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorageMultisampleCoverageNV"), typeof(glRenderbufferStorageMultisampleCoverageNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorageMultisampleCoverageNV'.");
            }
        }
        public static void glRenderbufferStorageMultisampleCoverageNV(GLenum @target, GLsizei @coverageSamples, GLsizei @colorSamples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStorageMultisampleCoverageNVPtr(@target, @coverageSamples, @colorSamples, @internalformat, @width, @height);
        #endregion
    }
}
