using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_framebuffer_multisample
    {
        #region Interop
        static GL_NV_framebuffer_multisample()
        {
            Console.WriteLine("Initalising GL_NV_framebuffer_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadRenderbufferStorageMultisampleNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RENDERBUFFER_SAMPLES_NV = 0x8CAB;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_NV = 0x8D56;
        public static UInt32 GL_MAX_SAMPLES_NV = 0x8D57;
        #endregion

        #region Commands
        internal delegate void glRenderbufferStorageMultisampleNVFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageMultisampleNVFunc glRenderbufferStorageMultisampleNVPtr;
        internal static void loadRenderbufferStorageMultisampleNV()
        {
            try
            {
                glRenderbufferStorageMultisampleNVPtr = (glRenderbufferStorageMultisampleNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorageMultisampleNV"), typeof(glRenderbufferStorageMultisampleNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorageMultisampleNV'.");
            }
        }
        public static void glRenderbufferStorageMultisampleNV(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStorageMultisampleNVPtr(@target, @samples, @internalformat, @width, @height);
        #endregion
    }
}
