using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_texture_barrier
    {
        #region Interop
        static GL_NV_texture_barrier()
        {
            Console.WriteLine("Initalising GL_NV_texture_barrier interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTextureBarrierNV();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glTextureBarrierNVFunc();
        internal static glTextureBarrierNVFunc glTextureBarrierNVPtr;
        internal static void loadTextureBarrierNV()
        {
            try
            {
                glTextureBarrierNVPtr = (glTextureBarrierNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureBarrierNV"), typeof(glTextureBarrierNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureBarrierNV'.");
            }
        }
        public static void glTextureBarrierNV() => glTextureBarrierNVPtr();
        #endregion
    }
}
