using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_register_combiners2
    {
        #region Interop
        static GL_NV_register_combiners2()
        {
            Console.WriteLine("Initalising GL_NV_register_combiners2 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCombinerStageParameterfvNV();
            loadGetCombinerStageParameterfvNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PER_STAGE_CONSTANTS_NV = 0x8535;
        #endregion

        #region Commands
        internal delegate void glCombinerStageParameterfvNVFunc(GLenum @stage, GLenum @pname, const GLfloat * @params);
        internal static glCombinerStageParameterfvNVFunc glCombinerStageParameterfvNVPtr;
        internal static void loadCombinerStageParameterfvNV()
        {
            try
            {
                glCombinerStageParameterfvNVPtr = (glCombinerStageParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCombinerStageParameterfvNV"), typeof(glCombinerStageParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCombinerStageParameterfvNV'.");
            }
        }
        public static void glCombinerStageParameterfvNV(GLenum @stage, GLenum @pname, const GLfloat * @params) => glCombinerStageParameterfvNVPtr(@stage, @pname, @params);

        internal delegate void glGetCombinerStageParameterfvNVFunc(GLenum @stage, GLenum @pname, GLfloat * @params);
        internal static glGetCombinerStageParameterfvNVFunc glGetCombinerStageParameterfvNVPtr;
        internal static void loadGetCombinerStageParameterfvNV()
        {
            try
            {
                glGetCombinerStageParameterfvNVPtr = (glGetCombinerStageParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCombinerStageParameterfvNV"), typeof(glGetCombinerStageParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCombinerStageParameterfvNV'.");
            }
        }
        public static void glGetCombinerStageParameterfvNV(GLenum @stage, GLenum @pname, GLfloat * @params) => glGetCombinerStageParameterfvNVPtr(@stage, @pname, @params);
        #endregion
    }
}
