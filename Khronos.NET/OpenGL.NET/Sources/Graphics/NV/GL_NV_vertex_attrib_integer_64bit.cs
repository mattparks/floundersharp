using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_vertex_attrib_integer_64bit
    {
        #region Interop
        static GL_NV_vertex_attrib_integer_64bit()
        {
            Console.WriteLine("Initalising GL_NV_vertex_attrib_integer_64bit interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexAttribL1i64NV();
            loadVertexAttribL2i64NV();
            loadVertexAttribL3i64NV();
            loadVertexAttribL4i64NV();
            loadVertexAttribL1i64vNV();
            loadVertexAttribL2i64vNV();
            loadVertexAttribL3i64vNV();
            loadVertexAttribL4i64vNV();
            loadVertexAttribL1ui64NV();
            loadVertexAttribL2ui64NV();
            loadVertexAttribL3ui64NV();
            loadVertexAttribL4ui64NV();
            loadVertexAttribL1ui64vNV();
            loadVertexAttribL2ui64vNV();
            loadVertexAttribL3ui64vNV();
            loadVertexAttribL4ui64vNV();
            loadGetVertexAttribLi64vNV();
            loadGetVertexAttribLui64vNV();
            loadVertexAttribLFormatNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_INT64_NV = 0x140E;
        public static UInt32 GL_UNSIGNED_INT64_NV = 0x140F;
        #endregion

        #region Commands
        internal delegate void glVertexAttribL1i64NVFunc(GLuint @index, GLint64EXT @x);
        internal static glVertexAttribL1i64NVFunc glVertexAttribL1i64NVPtr;
        internal static void loadVertexAttribL1i64NV()
        {
            try
            {
                glVertexAttribL1i64NVPtr = (glVertexAttribL1i64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL1i64NV"), typeof(glVertexAttribL1i64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL1i64NV'.");
            }
        }
        public static void glVertexAttribL1i64NV(GLuint @index, GLint64EXT @x) => glVertexAttribL1i64NVPtr(@index, @x);

        internal delegate void glVertexAttribL2i64NVFunc(GLuint @index, GLint64EXT @x, GLint64EXT @y);
        internal static glVertexAttribL2i64NVFunc glVertexAttribL2i64NVPtr;
        internal static void loadVertexAttribL2i64NV()
        {
            try
            {
                glVertexAttribL2i64NVPtr = (glVertexAttribL2i64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL2i64NV"), typeof(glVertexAttribL2i64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL2i64NV'.");
            }
        }
        public static void glVertexAttribL2i64NV(GLuint @index, GLint64EXT @x, GLint64EXT @y) => glVertexAttribL2i64NVPtr(@index, @x, @y);

        internal delegate void glVertexAttribL3i64NVFunc(GLuint @index, GLint64EXT @x, GLint64EXT @y, GLint64EXT @z);
        internal static glVertexAttribL3i64NVFunc glVertexAttribL3i64NVPtr;
        internal static void loadVertexAttribL3i64NV()
        {
            try
            {
                glVertexAttribL3i64NVPtr = (glVertexAttribL3i64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL3i64NV"), typeof(glVertexAttribL3i64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL3i64NV'.");
            }
        }
        public static void glVertexAttribL3i64NV(GLuint @index, GLint64EXT @x, GLint64EXT @y, GLint64EXT @z) => glVertexAttribL3i64NVPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttribL4i64NVFunc(GLuint @index, GLint64EXT @x, GLint64EXT @y, GLint64EXT @z, GLint64EXT @w);
        internal static glVertexAttribL4i64NVFunc glVertexAttribL4i64NVPtr;
        internal static void loadVertexAttribL4i64NV()
        {
            try
            {
                glVertexAttribL4i64NVPtr = (glVertexAttribL4i64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL4i64NV"), typeof(glVertexAttribL4i64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL4i64NV'.");
            }
        }
        public static void glVertexAttribL4i64NV(GLuint @index, GLint64EXT @x, GLint64EXT @y, GLint64EXT @z, GLint64EXT @w) => glVertexAttribL4i64NVPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttribL1i64vNVFunc(GLuint @index, const GLint64EXT * @v);
        internal static glVertexAttribL1i64vNVFunc glVertexAttribL1i64vNVPtr;
        internal static void loadVertexAttribL1i64vNV()
        {
            try
            {
                glVertexAttribL1i64vNVPtr = (glVertexAttribL1i64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL1i64vNV"), typeof(glVertexAttribL1i64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL1i64vNV'.");
            }
        }
        public static void glVertexAttribL1i64vNV(GLuint @index, const GLint64EXT * @v) => glVertexAttribL1i64vNVPtr(@index, @v);

        internal delegate void glVertexAttribL2i64vNVFunc(GLuint @index, const GLint64EXT * @v);
        internal static glVertexAttribL2i64vNVFunc glVertexAttribL2i64vNVPtr;
        internal static void loadVertexAttribL2i64vNV()
        {
            try
            {
                glVertexAttribL2i64vNVPtr = (glVertexAttribL2i64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL2i64vNV"), typeof(glVertexAttribL2i64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL2i64vNV'.");
            }
        }
        public static void glVertexAttribL2i64vNV(GLuint @index, const GLint64EXT * @v) => glVertexAttribL2i64vNVPtr(@index, @v);

        internal delegate void glVertexAttribL3i64vNVFunc(GLuint @index, const GLint64EXT * @v);
        internal static glVertexAttribL3i64vNVFunc glVertexAttribL3i64vNVPtr;
        internal static void loadVertexAttribL3i64vNV()
        {
            try
            {
                glVertexAttribL3i64vNVPtr = (glVertexAttribL3i64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL3i64vNV"), typeof(glVertexAttribL3i64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL3i64vNV'.");
            }
        }
        public static void glVertexAttribL3i64vNV(GLuint @index, const GLint64EXT * @v) => glVertexAttribL3i64vNVPtr(@index, @v);

        internal delegate void glVertexAttribL4i64vNVFunc(GLuint @index, const GLint64EXT * @v);
        internal static glVertexAttribL4i64vNVFunc glVertexAttribL4i64vNVPtr;
        internal static void loadVertexAttribL4i64vNV()
        {
            try
            {
                glVertexAttribL4i64vNVPtr = (glVertexAttribL4i64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL4i64vNV"), typeof(glVertexAttribL4i64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL4i64vNV'.");
            }
        }
        public static void glVertexAttribL4i64vNV(GLuint @index, const GLint64EXT * @v) => glVertexAttribL4i64vNVPtr(@index, @v);

        internal delegate void glVertexAttribL1ui64NVFunc(GLuint @index, GLuint64EXT @x);
        internal static glVertexAttribL1ui64NVFunc glVertexAttribL1ui64NVPtr;
        internal static void loadVertexAttribL1ui64NV()
        {
            try
            {
                glVertexAttribL1ui64NVPtr = (glVertexAttribL1ui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL1ui64NV"), typeof(glVertexAttribL1ui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL1ui64NV'.");
            }
        }
        public static void glVertexAttribL1ui64NV(GLuint @index, GLuint64EXT @x) => glVertexAttribL1ui64NVPtr(@index, @x);

        internal delegate void glVertexAttribL2ui64NVFunc(GLuint @index, GLuint64EXT @x, GLuint64EXT @y);
        internal static glVertexAttribL2ui64NVFunc glVertexAttribL2ui64NVPtr;
        internal static void loadVertexAttribL2ui64NV()
        {
            try
            {
                glVertexAttribL2ui64NVPtr = (glVertexAttribL2ui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL2ui64NV"), typeof(glVertexAttribL2ui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL2ui64NV'.");
            }
        }
        public static void glVertexAttribL2ui64NV(GLuint @index, GLuint64EXT @x, GLuint64EXT @y) => glVertexAttribL2ui64NVPtr(@index, @x, @y);

        internal delegate void glVertexAttribL3ui64NVFunc(GLuint @index, GLuint64EXT @x, GLuint64EXT @y, GLuint64EXT @z);
        internal static glVertexAttribL3ui64NVFunc glVertexAttribL3ui64NVPtr;
        internal static void loadVertexAttribL3ui64NV()
        {
            try
            {
                glVertexAttribL3ui64NVPtr = (glVertexAttribL3ui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL3ui64NV"), typeof(glVertexAttribL3ui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL3ui64NV'.");
            }
        }
        public static void glVertexAttribL3ui64NV(GLuint @index, GLuint64EXT @x, GLuint64EXT @y, GLuint64EXT @z) => glVertexAttribL3ui64NVPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttribL4ui64NVFunc(GLuint @index, GLuint64EXT @x, GLuint64EXT @y, GLuint64EXT @z, GLuint64EXT @w);
        internal static glVertexAttribL4ui64NVFunc glVertexAttribL4ui64NVPtr;
        internal static void loadVertexAttribL4ui64NV()
        {
            try
            {
                glVertexAttribL4ui64NVPtr = (glVertexAttribL4ui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL4ui64NV"), typeof(glVertexAttribL4ui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL4ui64NV'.");
            }
        }
        public static void glVertexAttribL4ui64NV(GLuint @index, GLuint64EXT @x, GLuint64EXT @y, GLuint64EXT @z, GLuint64EXT @w) => glVertexAttribL4ui64NVPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttribL1ui64vNVFunc(GLuint @index, const GLuint64EXT * @v);
        internal static glVertexAttribL1ui64vNVFunc glVertexAttribL1ui64vNVPtr;
        internal static void loadVertexAttribL1ui64vNV()
        {
            try
            {
                glVertexAttribL1ui64vNVPtr = (glVertexAttribL1ui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL1ui64vNV"), typeof(glVertexAttribL1ui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL1ui64vNV'.");
            }
        }
        public static void glVertexAttribL1ui64vNV(GLuint @index, const GLuint64EXT * @v) => glVertexAttribL1ui64vNVPtr(@index, @v);

        internal delegate void glVertexAttribL2ui64vNVFunc(GLuint @index, const GLuint64EXT * @v);
        internal static glVertexAttribL2ui64vNVFunc glVertexAttribL2ui64vNVPtr;
        internal static void loadVertexAttribL2ui64vNV()
        {
            try
            {
                glVertexAttribL2ui64vNVPtr = (glVertexAttribL2ui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL2ui64vNV"), typeof(glVertexAttribL2ui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL2ui64vNV'.");
            }
        }
        public static void glVertexAttribL2ui64vNV(GLuint @index, const GLuint64EXT * @v) => glVertexAttribL2ui64vNVPtr(@index, @v);

        internal delegate void glVertexAttribL3ui64vNVFunc(GLuint @index, const GLuint64EXT * @v);
        internal static glVertexAttribL3ui64vNVFunc glVertexAttribL3ui64vNVPtr;
        internal static void loadVertexAttribL3ui64vNV()
        {
            try
            {
                glVertexAttribL3ui64vNVPtr = (glVertexAttribL3ui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL3ui64vNV"), typeof(glVertexAttribL3ui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL3ui64vNV'.");
            }
        }
        public static void glVertexAttribL3ui64vNV(GLuint @index, const GLuint64EXT * @v) => glVertexAttribL3ui64vNVPtr(@index, @v);

        internal delegate void glVertexAttribL4ui64vNVFunc(GLuint @index, const GLuint64EXT * @v);
        internal static glVertexAttribL4ui64vNVFunc glVertexAttribL4ui64vNVPtr;
        internal static void loadVertexAttribL4ui64vNV()
        {
            try
            {
                glVertexAttribL4ui64vNVPtr = (glVertexAttribL4ui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL4ui64vNV"), typeof(glVertexAttribL4ui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL4ui64vNV'.");
            }
        }
        public static void glVertexAttribL4ui64vNV(GLuint @index, const GLuint64EXT * @v) => glVertexAttribL4ui64vNVPtr(@index, @v);

        internal delegate void glGetVertexAttribLi64vNVFunc(GLuint @index, GLenum @pname, GLint64EXT * @params);
        internal static glGetVertexAttribLi64vNVFunc glGetVertexAttribLi64vNVPtr;
        internal static void loadGetVertexAttribLi64vNV()
        {
            try
            {
                glGetVertexAttribLi64vNVPtr = (glGetVertexAttribLi64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribLi64vNV"), typeof(glGetVertexAttribLi64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribLi64vNV'.");
            }
        }
        public static void glGetVertexAttribLi64vNV(GLuint @index, GLenum @pname, GLint64EXT * @params) => glGetVertexAttribLi64vNVPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribLui64vNVFunc(GLuint @index, GLenum @pname, GLuint64EXT * @params);
        internal static glGetVertexAttribLui64vNVFunc glGetVertexAttribLui64vNVPtr;
        internal static void loadGetVertexAttribLui64vNV()
        {
            try
            {
                glGetVertexAttribLui64vNVPtr = (glGetVertexAttribLui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribLui64vNV"), typeof(glGetVertexAttribLui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribLui64vNV'.");
            }
        }
        public static void glGetVertexAttribLui64vNV(GLuint @index, GLenum @pname, GLuint64EXT * @params) => glGetVertexAttribLui64vNVPtr(@index, @pname, @params);

        internal delegate void glVertexAttribLFormatNVFunc(GLuint @index, GLint @size, GLenum @type, GLsizei @stride);
        internal static glVertexAttribLFormatNVFunc glVertexAttribLFormatNVPtr;
        internal static void loadVertexAttribLFormatNV()
        {
            try
            {
                glVertexAttribLFormatNVPtr = (glVertexAttribLFormatNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribLFormatNV"), typeof(glVertexAttribLFormatNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribLFormatNV'.");
            }
        }
        public static void glVertexAttribLFormatNV(GLuint @index, GLint @size, GLenum @type, GLsizei @stride) => glVertexAttribLFormatNVPtr(@index, @size, @type, @stride);
        #endregion
    }
}
