using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_multisample_filter_hint
    {
        #region Interop
        static GL_NV_multisample_filter_hint()
        {
            Console.WriteLine("Initalising GL_NV_multisample_filter_hint interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MULTISAMPLE_FILTER_HINT_NV = 0x8534;
        #endregion

        #region Commands
        #endregion
    }
}
