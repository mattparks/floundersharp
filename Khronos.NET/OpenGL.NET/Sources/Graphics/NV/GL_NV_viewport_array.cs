using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_viewport_array
    {
        #region Interop
        static GL_NV_viewport_array()
        {
            Console.WriteLine("Initalising GL_NV_viewport_array interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadViewportArrayvNV();
            loadViewportIndexedfNV();
            loadViewportIndexedfvNV();
            loadScissorArrayvNV();
            loadScissorIndexedNV();
            loadScissorIndexedvNV();
            loadDepthRangeArrayfvNV();
            loadDepthRangeIndexedfNV();
            loadGetFloati_vNV();
            loadEnableiNV();
            loadDisableiNV();
            loadIsEnablediNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_VIEWPORTS_NV = 0x825B;
        public static UInt32 GL_VIEWPORT_SUBPIXEL_BITS_NV = 0x825C;
        public static UInt32 GL_VIEWPORT_BOUNDS_RANGE_NV = 0x825D;
        public static UInt32 GL_VIEWPORT_INDEX_PROVOKING_VERTEX_NV = 0x825F;
        public static UInt32 GL_SCISSOR_BOX = 0x0C10;
        public static UInt32 GL_VIEWPORT = 0x0BA2;
        public static UInt32 GL_DEPTH_RANGE = 0x0B70;
        public static UInt32 GL_SCISSOR_TEST = 0x0C11;
        #endregion

        #region Commands
        internal delegate void glViewportArrayvNVFunc(GLuint @first, GLsizei @count, const GLfloat * @v);
        internal static glViewportArrayvNVFunc glViewportArrayvNVPtr;
        internal static void loadViewportArrayvNV()
        {
            try
            {
                glViewportArrayvNVPtr = (glViewportArrayvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glViewportArrayvNV"), typeof(glViewportArrayvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glViewportArrayvNV'.");
            }
        }
        public static void glViewportArrayvNV(GLuint @first, GLsizei @count, const GLfloat * @v) => glViewportArrayvNVPtr(@first, @count, @v);

        internal delegate void glViewportIndexedfNVFunc(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @w, GLfloat @h);
        internal static glViewportIndexedfNVFunc glViewportIndexedfNVPtr;
        internal static void loadViewportIndexedfNV()
        {
            try
            {
                glViewportIndexedfNVPtr = (glViewportIndexedfNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glViewportIndexedfNV"), typeof(glViewportIndexedfNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glViewportIndexedfNV'.");
            }
        }
        public static void glViewportIndexedfNV(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @w, GLfloat @h) => glViewportIndexedfNVPtr(@index, @x, @y, @w, @h);

        internal delegate void glViewportIndexedfvNVFunc(GLuint @index, const GLfloat * @v);
        internal static glViewportIndexedfvNVFunc glViewportIndexedfvNVPtr;
        internal static void loadViewportIndexedfvNV()
        {
            try
            {
                glViewportIndexedfvNVPtr = (glViewportIndexedfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glViewportIndexedfvNV"), typeof(glViewportIndexedfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glViewportIndexedfvNV'.");
            }
        }
        public static void glViewportIndexedfvNV(GLuint @index, const GLfloat * @v) => glViewportIndexedfvNVPtr(@index, @v);

        internal delegate void glScissorArrayvNVFunc(GLuint @first, GLsizei @count, const GLint * @v);
        internal static glScissorArrayvNVFunc glScissorArrayvNVPtr;
        internal static void loadScissorArrayvNV()
        {
            try
            {
                glScissorArrayvNVPtr = (glScissorArrayvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScissorArrayvNV"), typeof(glScissorArrayvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScissorArrayvNV'.");
            }
        }
        public static void glScissorArrayvNV(GLuint @first, GLsizei @count, const GLint * @v) => glScissorArrayvNVPtr(@first, @count, @v);

        internal delegate void glScissorIndexedNVFunc(GLuint @index, GLint @left, GLint @bottom, GLsizei @width, GLsizei @height);
        internal static glScissorIndexedNVFunc glScissorIndexedNVPtr;
        internal static void loadScissorIndexedNV()
        {
            try
            {
                glScissorIndexedNVPtr = (glScissorIndexedNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScissorIndexedNV"), typeof(glScissorIndexedNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScissorIndexedNV'.");
            }
        }
        public static void glScissorIndexedNV(GLuint @index, GLint @left, GLint @bottom, GLsizei @width, GLsizei @height) => glScissorIndexedNVPtr(@index, @left, @bottom, @width, @height);

        internal delegate void glScissorIndexedvNVFunc(GLuint @index, const GLint * @v);
        internal static glScissorIndexedvNVFunc glScissorIndexedvNVPtr;
        internal static void loadScissorIndexedvNV()
        {
            try
            {
                glScissorIndexedvNVPtr = (glScissorIndexedvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScissorIndexedvNV"), typeof(glScissorIndexedvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScissorIndexedvNV'.");
            }
        }
        public static void glScissorIndexedvNV(GLuint @index, const GLint * @v) => glScissorIndexedvNVPtr(@index, @v);

        internal delegate void glDepthRangeArrayfvNVFunc(GLuint @first, GLsizei @count, const GLfloat * @v);
        internal static glDepthRangeArrayfvNVFunc glDepthRangeArrayfvNVPtr;
        internal static void loadDepthRangeArrayfvNV()
        {
            try
            {
                glDepthRangeArrayfvNVPtr = (glDepthRangeArrayfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangeArrayfvNV"), typeof(glDepthRangeArrayfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangeArrayfvNV'.");
            }
        }
        public static void glDepthRangeArrayfvNV(GLuint @first, GLsizei @count, const GLfloat * @v) => glDepthRangeArrayfvNVPtr(@first, @count, @v);

        internal delegate void glDepthRangeIndexedfNVFunc(GLuint @index, GLfloat @n, GLfloat @f);
        internal static glDepthRangeIndexedfNVFunc glDepthRangeIndexedfNVPtr;
        internal static void loadDepthRangeIndexedfNV()
        {
            try
            {
                glDepthRangeIndexedfNVPtr = (glDepthRangeIndexedfNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangeIndexedfNV"), typeof(glDepthRangeIndexedfNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangeIndexedfNV'.");
            }
        }
        public static void glDepthRangeIndexedfNV(GLuint @index, GLfloat @n, GLfloat @f) => glDepthRangeIndexedfNVPtr(@index, @n, @f);

        internal delegate void glGetFloati_vNVFunc(GLenum @target, GLuint @index, GLfloat * @data);
        internal static glGetFloati_vNVFunc glGetFloati_vNVPtr;
        internal static void loadGetFloati_vNV()
        {
            try
            {
                glGetFloati_vNVPtr = (glGetFloati_vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFloati_vNV"), typeof(glGetFloati_vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFloati_vNV'.");
            }
        }
        public static void glGetFloati_vNV(GLenum @target, GLuint @index, GLfloat * @data) => glGetFloati_vNVPtr(@target, @index, @data);

        internal delegate void glEnableiNVFunc(GLenum @target, GLuint @index);
        internal static glEnableiNVFunc glEnableiNVPtr;
        internal static void loadEnableiNV()
        {
            try
            {
                glEnableiNVPtr = (glEnableiNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableiNV"), typeof(glEnableiNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableiNV'.");
            }
        }
        public static void glEnableiNV(GLenum @target, GLuint @index) => glEnableiNVPtr(@target, @index);

        internal delegate void glDisableiNVFunc(GLenum @target, GLuint @index);
        internal static glDisableiNVFunc glDisableiNVPtr;
        internal static void loadDisableiNV()
        {
            try
            {
                glDisableiNVPtr = (glDisableiNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableiNV"), typeof(glDisableiNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableiNV'.");
            }
        }
        public static void glDisableiNV(GLenum @target, GLuint @index) => glDisableiNVPtr(@target, @index);

        internal delegate GLboolean glIsEnablediNVFunc(GLenum @target, GLuint @index);
        internal static glIsEnablediNVFunc glIsEnablediNVPtr;
        internal static void loadIsEnablediNV()
        {
            try
            {
                glIsEnablediNVPtr = (glIsEnablediNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsEnablediNV"), typeof(glIsEnablediNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsEnablediNV'.");
            }
        }
        public static GLboolean glIsEnablediNV(GLenum @target, GLuint @index) => glIsEnablediNVPtr(@target, @index);
        #endregion
    }
}
