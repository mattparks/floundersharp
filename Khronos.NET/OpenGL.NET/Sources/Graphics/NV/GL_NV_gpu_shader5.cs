using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_gpu_shader5
    {
        #region Interop
        static GL_NV_gpu_shader5()
        {
            Console.WriteLine("Initalising GL_NV_gpu_shader5 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadUniform1i64NV();
            loadUniform2i64NV();
            loadUniform3i64NV();
            loadUniform4i64NV();
            loadUniform1i64vNV();
            loadUniform2i64vNV();
            loadUniform3i64vNV();
            loadUniform4i64vNV();
            loadUniform1ui64NV();
            loadUniform2ui64NV();
            loadUniform3ui64NV();
            loadUniform4ui64NV();
            loadUniform1ui64vNV();
            loadUniform2ui64vNV();
            loadUniform3ui64vNV();
            loadUniform4ui64vNV();
            loadGetUniformi64vNV();
            loadProgramUniform1i64NV();
            loadProgramUniform2i64NV();
            loadProgramUniform3i64NV();
            loadProgramUniform4i64NV();
            loadProgramUniform1i64vNV();
            loadProgramUniform2i64vNV();
            loadProgramUniform3i64vNV();
            loadProgramUniform4i64vNV();
            loadProgramUniform1ui64NV();
            loadProgramUniform2ui64NV();
            loadProgramUniform3ui64NV();
            loadProgramUniform4ui64NV();
            loadProgramUniform1ui64vNV();
            loadProgramUniform2ui64vNV();
            loadProgramUniform3ui64vNV();
            loadProgramUniform4ui64vNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_INT64_NV = 0x140E;
        public static UInt32 GL_UNSIGNED_INT64_NV = 0x140F;
        public static UInt32 GL_INT8_NV = 0x8FE0;
        public static UInt32 GL_INT8_VEC2_NV = 0x8FE1;
        public static UInt32 GL_INT8_VEC3_NV = 0x8FE2;
        public static UInt32 GL_INT8_VEC4_NV = 0x8FE3;
        public static UInt32 GL_INT16_NV = 0x8FE4;
        public static UInt32 GL_INT16_VEC2_NV = 0x8FE5;
        public static UInt32 GL_INT16_VEC3_NV = 0x8FE6;
        public static UInt32 GL_INT16_VEC4_NV = 0x8FE7;
        public static UInt32 GL_INT64_VEC2_NV = 0x8FE9;
        public static UInt32 GL_INT64_VEC3_NV = 0x8FEA;
        public static UInt32 GL_INT64_VEC4_NV = 0x8FEB;
        public static UInt32 GL_UNSIGNED_INT8_NV = 0x8FEC;
        public static UInt32 GL_UNSIGNED_INT8_VEC2_NV = 0x8FED;
        public static UInt32 GL_UNSIGNED_INT8_VEC3_NV = 0x8FEE;
        public static UInt32 GL_UNSIGNED_INT8_VEC4_NV = 0x8FEF;
        public static UInt32 GL_UNSIGNED_INT16_NV = 0x8FF0;
        public static UInt32 GL_UNSIGNED_INT16_VEC2_NV = 0x8FF1;
        public static UInt32 GL_UNSIGNED_INT16_VEC3_NV = 0x8FF2;
        public static UInt32 GL_UNSIGNED_INT16_VEC4_NV = 0x8FF3;
        public static UInt32 GL_UNSIGNED_INT64_VEC2_NV = 0x8FF5;
        public static UInt32 GL_UNSIGNED_INT64_VEC3_NV = 0x8FF6;
        public static UInt32 GL_UNSIGNED_INT64_VEC4_NV = 0x8FF7;
        public static UInt32 GL_FLOAT16_NV = 0x8FF8;
        public static UInt32 GL_FLOAT16_VEC2_NV = 0x8FF9;
        public static UInt32 GL_FLOAT16_VEC3_NV = 0x8FFA;
        public static UInt32 GL_FLOAT16_VEC4_NV = 0x8FFB;
        public static UInt32 GL_PATCHES = 0x000E;
        #endregion

        #region Commands
        internal delegate void glUniform1i64NVFunc(GLint @location, GLint64EXT @x);
        internal static glUniform1i64NVFunc glUniform1i64NVPtr;
        internal static void loadUniform1i64NV()
        {
            try
            {
                glUniform1i64NVPtr = (glUniform1i64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1i64NV"), typeof(glUniform1i64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1i64NV'.");
            }
        }
        public static void glUniform1i64NV(GLint @location, GLint64EXT @x) => glUniform1i64NVPtr(@location, @x);

        internal delegate void glUniform2i64NVFunc(GLint @location, GLint64EXT @x, GLint64EXT @y);
        internal static glUniform2i64NVFunc glUniform2i64NVPtr;
        internal static void loadUniform2i64NV()
        {
            try
            {
                glUniform2i64NVPtr = (glUniform2i64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2i64NV"), typeof(glUniform2i64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2i64NV'.");
            }
        }
        public static void glUniform2i64NV(GLint @location, GLint64EXT @x, GLint64EXT @y) => glUniform2i64NVPtr(@location, @x, @y);

        internal delegate void glUniform3i64NVFunc(GLint @location, GLint64EXT @x, GLint64EXT @y, GLint64EXT @z);
        internal static glUniform3i64NVFunc glUniform3i64NVPtr;
        internal static void loadUniform3i64NV()
        {
            try
            {
                glUniform3i64NVPtr = (glUniform3i64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3i64NV"), typeof(glUniform3i64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3i64NV'.");
            }
        }
        public static void glUniform3i64NV(GLint @location, GLint64EXT @x, GLint64EXT @y, GLint64EXT @z) => glUniform3i64NVPtr(@location, @x, @y, @z);

        internal delegate void glUniform4i64NVFunc(GLint @location, GLint64EXT @x, GLint64EXT @y, GLint64EXT @z, GLint64EXT @w);
        internal static glUniform4i64NVFunc glUniform4i64NVPtr;
        internal static void loadUniform4i64NV()
        {
            try
            {
                glUniform4i64NVPtr = (glUniform4i64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4i64NV"), typeof(glUniform4i64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4i64NV'.");
            }
        }
        public static void glUniform4i64NV(GLint @location, GLint64EXT @x, GLint64EXT @y, GLint64EXT @z, GLint64EXT @w) => glUniform4i64NVPtr(@location, @x, @y, @z, @w);

        internal delegate void glUniform1i64vNVFunc(GLint @location, GLsizei @count, const GLint64EXT * @value);
        internal static glUniform1i64vNVFunc glUniform1i64vNVPtr;
        internal static void loadUniform1i64vNV()
        {
            try
            {
                glUniform1i64vNVPtr = (glUniform1i64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1i64vNV"), typeof(glUniform1i64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1i64vNV'.");
            }
        }
        public static void glUniform1i64vNV(GLint @location, GLsizei @count, const GLint64EXT * @value) => glUniform1i64vNVPtr(@location, @count, @value);

        internal delegate void glUniform2i64vNVFunc(GLint @location, GLsizei @count, const GLint64EXT * @value);
        internal static glUniform2i64vNVFunc glUniform2i64vNVPtr;
        internal static void loadUniform2i64vNV()
        {
            try
            {
                glUniform2i64vNVPtr = (glUniform2i64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2i64vNV"), typeof(glUniform2i64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2i64vNV'.");
            }
        }
        public static void glUniform2i64vNV(GLint @location, GLsizei @count, const GLint64EXT * @value) => glUniform2i64vNVPtr(@location, @count, @value);

        internal delegate void glUniform3i64vNVFunc(GLint @location, GLsizei @count, const GLint64EXT * @value);
        internal static glUniform3i64vNVFunc glUniform3i64vNVPtr;
        internal static void loadUniform3i64vNV()
        {
            try
            {
                glUniform3i64vNVPtr = (glUniform3i64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3i64vNV"), typeof(glUniform3i64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3i64vNV'.");
            }
        }
        public static void glUniform3i64vNV(GLint @location, GLsizei @count, const GLint64EXT * @value) => glUniform3i64vNVPtr(@location, @count, @value);

        internal delegate void glUniform4i64vNVFunc(GLint @location, GLsizei @count, const GLint64EXT * @value);
        internal static glUniform4i64vNVFunc glUniform4i64vNVPtr;
        internal static void loadUniform4i64vNV()
        {
            try
            {
                glUniform4i64vNVPtr = (glUniform4i64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4i64vNV"), typeof(glUniform4i64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4i64vNV'.");
            }
        }
        public static void glUniform4i64vNV(GLint @location, GLsizei @count, const GLint64EXT * @value) => glUniform4i64vNVPtr(@location, @count, @value);

        internal delegate void glUniform1ui64NVFunc(GLint @location, GLuint64EXT @x);
        internal static glUniform1ui64NVFunc glUniform1ui64NVPtr;
        internal static void loadUniform1ui64NV()
        {
            try
            {
                glUniform1ui64NVPtr = (glUniform1ui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1ui64NV"), typeof(glUniform1ui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1ui64NV'.");
            }
        }
        public static void glUniform1ui64NV(GLint @location, GLuint64EXT @x) => glUniform1ui64NVPtr(@location, @x);

        internal delegate void glUniform2ui64NVFunc(GLint @location, GLuint64EXT @x, GLuint64EXT @y);
        internal static glUniform2ui64NVFunc glUniform2ui64NVPtr;
        internal static void loadUniform2ui64NV()
        {
            try
            {
                glUniform2ui64NVPtr = (glUniform2ui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2ui64NV"), typeof(glUniform2ui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2ui64NV'.");
            }
        }
        public static void glUniform2ui64NV(GLint @location, GLuint64EXT @x, GLuint64EXT @y) => glUniform2ui64NVPtr(@location, @x, @y);

        internal delegate void glUniform3ui64NVFunc(GLint @location, GLuint64EXT @x, GLuint64EXT @y, GLuint64EXT @z);
        internal static glUniform3ui64NVFunc glUniform3ui64NVPtr;
        internal static void loadUniform3ui64NV()
        {
            try
            {
                glUniform3ui64NVPtr = (glUniform3ui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3ui64NV"), typeof(glUniform3ui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3ui64NV'.");
            }
        }
        public static void glUniform3ui64NV(GLint @location, GLuint64EXT @x, GLuint64EXT @y, GLuint64EXT @z) => glUniform3ui64NVPtr(@location, @x, @y, @z);

        internal delegate void glUniform4ui64NVFunc(GLint @location, GLuint64EXT @x, GLuint64EXT @y, GLuint64EXT @z, GLuint64EXT @w);
        internal static glUniform4ui64NVFunc glUniform4ui64NVPtr;
        internal static void loadUniform4ui64NV()
        {
            try
            {
                glUniform4ui64NVPtr = (glUniform4ui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4ui64NV"), typeof(glUniform4ui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4ui64NV'.");
            }
        }
        public static void glUniform4ui64NV(GLint @location, GLuint64EXT @x, GLuint64EXT @y, GLuint64EXT @z, GLuint64EXT @w) => glUniform4ui64NVPtr(@location, @x, @y, @z, @w);

        internal delegate void glUniform1ui64vNVFunc(GLint @location, GLsizei @count, const GLuint64EXT * @value);
        internal static glUniform1ui64vNVFunc glUniform1ui64vNVPtr;
        internal static void loadUniform1ui64vNV()
        {
            try
            {
                glUniform1ui64vNVPtr = (glUniform1ui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1ui64vNV"), typeof(glUniform1ui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1ui64vNV'.");
            }
        }
        public static void glUniform1ui64vNV(GLint @location, GLsizei @count, const GLuint64EXT * @value) => glUniform1ui64vNVPtr(@location, @count, @value);

        internal delegate void glUniform2ui64vNVFunc(GLint @location, GLsizei @count, const GLuint64EXT * @value);
        internal static glUniform2ui64vNVFunc glUniform2ui64vNVPtr;
        internal static void loadUniform2ui64vNV()
        {
            try
            {
                glUniform2ui64vNVPtr = (glUniform2ui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2ui64vNV"), typeof(glUniform2ui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2ui64vNV'.");
            }
        }
        public static void glUniform2ui64vNV(GLint @location, GLsizei @count, const GLuint64EXT * @value) => glUniform2ui64vNVPtr(@location, @count, @value);

        internal delegate void glUniform3ui64vNVFunc(GLint @location, GLsizei @count, const GLuint64EXT * @value);
        internal static glUniform3ui64vNVFunc glUniform3ui64vNVPtr;
        internal static void loadUniform3ui64vNV()
        {
            try
            {
                glUniform3ui64vNVPtr = (glUniform3ui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3ui64vNV"), typeof(glUniform3ui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3ui64vNV'.");
            }
        }
        public static void glUniform3ui64vNV(GLint @location, GLsizei @count, const GLuint64EXT * @value) => glUniform3ui64vNVPtr(@location, @count, @value);

        internal delegate void glUniform4ui64vNVFunc(GLint @location, GLsizei @count, const GLuint64EXT * @value);
        internal static glUniform4ui64vNVFunc glUniform4ui64vNVPtr;
        internal static void loadUniform4ui64vNV()
        {
            try
            {
                glUniform4ui64vNVPtr = (glUniform4ui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4ui64vNV"), typeof(glUniform4ui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4ui64vNV'.");
            }
        }
        public static void glUniform4ui64vNV(GLint @location, GLsizei @count, const GLuint64EXT * @value) => glUniform4ui64vNVPtr(@location, @count, @value);

        internal delegate void glGetUniformi64vNVFunc(GLuint @program, GLint @location, GLint64EXT * @params);
        internal static glGetUniformi64vNVFunc glGetUniformi64vNVPtr;
        internal static void loadGetUniformi64vNV()
        {
            try
            {
                glGetUniformi64vNVPtr = (glGetUniformi64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformi64vNV"), typeof(glGetUniformi64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformi64vNV'.");
            }
        }
        public static void glGetUniformi64vNV(GLuint @program, GLint @location, GLint64EXT * @params) => glGetUniformi64vNVPtr(@program, @location, @params);

        internal delegate void glProgramUniform1i64NVFunc(GLuint @program, GLint @location, GLint64EXT @x);
        internal static glProgramUniform1i64NVFunc glProgramUniform1i64NVPtr;
        internal static void loadProgramUniform1i64NV()
        {
            try
            {
                glProgramUniform1i64NVPtr = (glProgramUniform1i64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1i64NV"), typeof(glProgramUniform1i64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1i64NV'.");
            }
        }
        public static void glProgramUniform1i64NV(GLuint @program, GLint @location, GLint64EXT @x) => glProgramUniform1i64NVPtr(@program, @location, @x);

        internal delegate void glProgramUniform2i64NVFunc(GLuint @program, GLint @location, GLint64EXT @x, GLint64EXT @y);
        internal static glProgramUniform2i64NVFunc glProgramUniform2i64NVPtr;
        internal static void loadProgramUniform2i64NV()
        {
            try
            {
                glProgramUniform2i64NVPtr = (glProgramUniform2i64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2i64NV"), typeof(glProgramUniform2i64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2i64NV'.");
            }
        }
        public static void glProgramUniform2i64NV(GLuint @program, GLint @location, GLint64EXT @x, GLint64EXT @y) => glProgramUniform2i64NVPtr(@program, @location, @x, @y);

        internal delegate void glProgramUniform3i64NVFunc(GLuint @program, GLint @location, GLint64EXT @x, GLint64EXT @y, GLint64EXT @z);
        internal static glProgramUniform3i64NVFunc glProgramUniform3i64NVPtr;
        internal static void loadProgramUniform3i64NV()
        {
            try
            {
                glProgramUniform3i64NVPtr = (glProgramUniform3i64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3i64NV"), typeof(glProgramUniform3i64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3i64NV'.");
            }
        }
        public static void glProgramUniform3i64NV(GLuint @program, GLint @location, GLint64EXT @x, GLint64EXT @y, GLint64EXT @z) => glProgramUniform3i64NVPtr(@program, @location, @x, @y, @z);

        internal delegate void glProgramUniform4i64NVFunc(GLuint @program, GLint @location, GLint64EXT @x, GLint64EXT @y, GLint64EXT @z, GLint64EXT @w);
        internal static glProgramUniform4i64NVFunc glProgramUniform4i64NVPtr;
        internal static void loadProgramUniform4i64NV()
        {
            try
            {
                glProgramUniform4i64NVPtr = (glProgramUniform4i64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4i64NV"), typeof(glProgramUniform4i64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4i64NV'.");
            }
        }
        public static void glProgramUniform4i64NV(GLuint @program, GLint @location, GLint64EXT @x, GLint64EXT @y, GLint64EXT @z, GLint64EXT @w) => glProgramUniform4i64NVPtr(@program, @location, @x, @y, @z, @w);

        internal delegate void glProgramUniform1i64vNVFunc(GLuint @program, GLint @location, GLsizei @count, const GLint64EXT * @value);
        internal static glProgramUniform1i64vNVFunc glProgramUniform1i64vNVPtr;
        internal static void loadProgramUniform1i64vNV()
        {
            try
            {
                glProgramUniform1i64vNVPtr = (glProgramUniform1i64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1i64vNV"), typeof(glProgramUniform1i64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1i64vNV'.");
            }
        }
        public static void glProgramUniform1i64vNV(GLuint @program, GLint @location, GLsizei @count, const GLint64EXT * @value) => glProgramUniform1i64vNVPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2i64vNVFunc(GLuint @program, GLint @location, GLsizei @count, const GLint64EXT * @value);
        internal static glProgramUniform2i64vNVFunc glProgramUniform2i64vNVPtr;
        internal static void loadProgramUniform2i64vNV()
        {
            try
            {
                glProgramUniform2i64vNVPtr = (glProgramUniform2i64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2i64vNV"), typeof(glProgramUniform2i64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2i64vNV'.");
            }
        }
        public static void glProgramUniform2i64vNV(GLuint @program, GLint @location, GLsizei @count, const GLint64EXT * @value) => glProgramUniform2i64vNVPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3i64vNVFunc(GLuint @program, GLint @location, GLsizei @count, const GLint64EXT * @value);
        internal static glProgramUniform3i64vNVFunc glProgramUniform3i64vNVPtr;
        internal static void loadProgramUniform3i64vNV()
        {
            try
            {
                glProgramUniform3i64vNVPtr = (glProgramUniform3i64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3i64vNV"), typeof(glProgramUniform3i64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3i64vNV'.");
            }
        }
        public static void glProgramUniform3i64vNV(GLuint @program, GLint @location, GLsizei @count, const GLint64EXT * @value) => glProgramUniform3i64vNVPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4i64vNVFunc(GLuint @program, GLint @location, GLsizei @count, const GLint64EXT * @value);
        internal static glProgramUniform4i64vNVFunc glProgramUniform4i64vNVPtr;
        internal static void loadProgramUniform4i64vNV()
        {
            try
            {
                glProgramUniform4i64vNVPtr = (glProgramUniform4i64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4i64vNV"), typeof(glProgramUniform4i64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4i64vNV'.");
            }
        }
        public static void glProgramUniform4i64vNV(GLuint @program, GLint @location, GLsizei @count, const GLint64EXT * @value) => glProgramUniform4i64vNVPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform1ui64NVFunc(GLuint @program, GLint @location, GLuint64EXT @x);
        internal static glProgramUniform1ui64NVFunc glProgramUniform1ui64NVPtr;
        internal static void loadProgramUniform1ui64NV()
        {
            try
            {
                glProgramUniform1ui64NVPtr = (glProgramUniform1ui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1ui64NV"), typeof(glProgramUniform1ui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1ui64NV'.");
            }
        }
        public static void glProgramUniform1ui64NV(GLuint @program, GLint @location, GLuint64EXT @x) => glProgramUniform1ui64NVPtr(@program, @location, @x);

        internal delegate void glProgramUniform2ui64NVFunc(GLuint @program, GLint @location, GLuint64EXT @x, GLuint64EXT @y);
        internal static glProgramUniform2ui64NVFunc glProgramUniform2ui64NVPtr;
        internal static void loadProgramUniform2ui64NV()
        {
            try
            {
                glProgramUniform2ui64NVPtr = (glProgramUniform2ui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2ui64NV"), typeof(glProgramUniform2ui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2ui64NV'.");
            }
        }
        public static void glProgramUniform2ui64NV(GLuint @program, GLint @location, GLuint64EXT @x, GLuint64EXT @y) => glProgramUniform2ui64NVPtr(@program, @location, @x, @y);

        internal delegate void glProgramUniform3ui64NVFunc(GLuint @program, GLint @location, GLuint64EXT @x, GLuint64EXT @y, GLuint64EXT @z);
        internal static glProgramUniform3ui64NVFunc glProgramUniform3ui64NVPtr;
        internal static void loadProgramUniform3ui64NV()
        {
            try
            {
                glProgramUniform3ui64NVPtr = (glProgramUniform3ui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3ui64NV"), typeof(glProgramUniform3ui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3ui64NV'.");
            }
        }
        public static void glProgramUniform3ui64NV(GLuint @program, GLint @location, GLuint64EXT @x, GLuint64EXT @y, GLuint64EXT @z) => glProgramUniform3ui64NVPtr(@program, @location, @x, @y, @z);

        internal delegate void glProgramUniform4ui64NVFunc(GLuint @program, GLint @location, GLuint64EXT @x, GLuint64EXT @y, GLuint64EXT @z, GLuint64EXT @w);
        internal static glProgramUniform4ui64NVFunc glProgramUniform4ui64NVPtr;
        internal static void loadProgramUniform4ui64NV()
        {
            try
            {
                glProgramUniform4ui64NVPtr = (glProgramUniform4ui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4ui64NV"), typeof(glProgramUniform4ui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4ui64NV'.");
            }
        }
        public static void glProgramUniform4ui64NV(GLuint @program, GLint @location, GLuint64EXT @x, GLuint64EXT @y, GLuint64EXT @z, GLuint64EXT @w) => glProgramUniform4ui64NVPtr(@program, @location, @x, @y, @z, @w);

        internal delegate void glProgramUniform1ui64vNVFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint64EXT * @value);
        internal static glProgramUniform1ui64vNVFunc glProgramUniform1ui64vNVPtr;
        internal static void loadProgramUniform1ui64vNV()
        {
            try
            {
                glProgramUniform1ui64vNVPtr = (glProgramUniform1ui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1ui64vNV"), typeof(glProgramUniform1ui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1ui64vNV'.");
            }
        }
        public static void glProgramUniform1ui64vNV(GLuint @program, GLint @location, GLsizei @count, const GLuint64EXT * @value) => glProgramUniform1ui64vNVPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2ui64vNVFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint64EXT * @value);
        internal static glProgramUniform2ui64vNVFunc glProgramUniform2ui64vNVPtr;
        internal static void loadProgramUniform2ui64vNV()
        {
            try
            {
                glProgramUniform2ui64vNVPtr = (glProgramUniform2ui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2ui64vNV"), typeof(glProgramUniform2ui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2ui64vNV'.");
            }
        }
        public static void glProgramUniform2ui64vNV(GLuint @program, GLint @location, GLsizei @count, const GLuint64EXT * @value) => glProgramUniform2ui64vNVPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3ui64vNVFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint64EXT * @value);
        internal static glProgramUniform3ui64vNVFunc glProgramUniform3ui64vNVPtr;
        internal static void loadProgramUniform3ui64vNV()
        {
            try
            {
                glProgramUniform3ui64vNVPtr = (glProgramUniform3ui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3ui64vNV"), typeof(glProgramUniform3ui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3ui64vNV'.");
            }
        }
        public static void glProgramUniform3ui64vNV(GLuint @program, GLint @location, GLsizei @count, const GLuint64EXT * @value) => glProgramUniform3ui64vNVPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4ui64vNVFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint64EXT * @value);
        internal static glProgramUniform4ui64vNVFunc glProgramUniform4ui64vNVPtr;
        internal static void loadProgramUniform4ui64vNV()
        {
            try
            {
                glProgramUniform4ui64vNVPtr = (glProgramUniform4ui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4ui64vNV"), typeof(glProgramUniform4ui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4ui64vNV'.");
            }
        }
        public static void glProgramUniform4ui64vNV(GLuint @program, GLint @location, GLsizei @count, const GLuint64EXT * @value) => glProgramUniform4ui64vNVPtr(@program, @location, @count, @value);
        #endregion
    }
}
