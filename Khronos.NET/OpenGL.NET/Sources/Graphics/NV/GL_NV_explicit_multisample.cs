using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_explicit_multisample
    {
        #region Interop
        static GL_NV_explicit_multisample()
        {
            Console.WriteLine("Initalising GL_NV_explicit_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetMultisamplefvNV();
            loadSampleMaskIndexedNV();
            loadTexRenderbufferNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SAMPLE_POSITION_NV = 0x8E50;
        public static UInt32 GL_SAMPLE_MASK_NV = 0x8E51;
        public static UInt32 GL_SAMPLE_MASK_VALUE_NV = 0x8E52;
        public static UInt32 GL_TEXTURE_BINDING_RENDERBUFFER_NV = 0x8E53;
        public static UInt32 GL_TEXTURE_RENDERBUFFER_DATA_STORE_BINDING_NV = 0x8E54;
        public static UInt32 GL_TEXTURE_RENDERBUFFER_NV = 0x8E55;
        public static UInt32 GL_SAMPLER_RENDERBUFFER_NV = 0x8E56;
        public static UInt32 GL_INT_SAMPLER_RENDERBUFFER_NV = 0x8E57;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_RENDERBUFFER_NV = 0x8E58;
        public static UInt32 GL_MAX_SAMPLE_MASK_WORDS_NV = 0x8E59;
        #endregion

        #region Commands
        internal delegate void glGetMultisamplefvNVFunc(GLenum @pname, GLuint @index, GLfloat * @val);
        internal static glGetMultisamplefvNVFunc glGetMultisamplefvNVPtr;
        internal static void loadGetMultisamplefvNV()
        {
            try
            {
                glGetMultisamplefvNVPtr = (glGetMultisamplefvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultisamplefvNV"), typeof(glGetMultisamplefvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultisamplefvNV'.");
            }
        }
        public static void glGetMultisamplefvNV(GLenum @pname, GLuint @index, GLfloat * @val) => glGetMultisamplefvNVPtr(@pname, @index, @val);

        internal delegate void glSampleMaskIndexedNVFunc(GLuint @index, GLbitfield @mask);
        internal static glSampleMaskIndexedNVFunc glSampleMaskIndexedNVPtr;
        internal static void loadSampleMaskIndexedNV()
        {
            try
            {
                glSampleMaskIndexedNVPtr = (glSampleMaskIndexedNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleMaskIndexedNV"), typeof(glSampleMaskIndexedNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleMaskIndexedNV'.");
            }
        }
        public static void glSampleMaskIndexedNV(GLuint @index, GLbitfield @mask) => glSampleMaskIndexedNVPtr(@index, @mask);

        internal delegate void glTexRenderbufferNVFunc(GLenum @target, GLuint @renderbuffer);
        internal static glTexRenderbufferNVFunc glTexRenderbufferNVPtr;
        internal static void loadTexRenderbufferNV()
        {
            try
            {
                glTexRenderbufferNVPtr = (glTexRenderbufferNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexRenderbufferNV"), typeof(glTexRenderbufferNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexRenderbufferNV'.");
            }
        }
        public static void glTexRenderbufferNV(GLenum @target, GLuint @renderbuffer) => glTexRenderbufferNVPtr(@target, @renderbuffer);
        #endregion
    }
}
