using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_vertex_program
    {
        #region Interop
        static GL_NV_vertex_program()
        {
            Console.WriteLine("Initalising GL_NV_vertex_program interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadAreProgramsResidentNV();
            loadBindProgramNV();
            loadDeleteProgramsNV();
            loadExecuteProgramNV();
            loadGenProgramsNV();
            loadGetProgramParameterdvNV();
            loadGetProgramParameterfvNV();
            loadGetProgramivNV();
            loadGetProgramStringNV();
            loadGetTrackMatrixivNV();
            loadGetVertexAttribdvNV();
            loadGetVertexAttribfvNV();
            loadGetVertexAttribivNV();
            loadGetVertexAttribPointervNV();
            loadIsProgramNV();
            loadLoadProgramNV();
            loadProgramParameter4dNV();
            loadProgramParameter4dvNV();
            loadProgramParameter4fNV();
            loadProgramParameter4fvNV();
            loadProgramParameters4dvNV();
            loadProgramParameters4fvNV();
            loadRequestResidentProgramsNV();
            loadTrackMatrixNV();
            loadVertexAttribPointerNV();
            loadVertexAttrib1dNV();
            loadVertexAttrib1dvNV();
            loadVertexAttrib1fNV();
            loadVertexAttrib1fvNV();
            loadVertexAttrib1sNV();
            loadVertexAttrib1svNV();
            loadVertexAttrib2dNV();
            loadVertexAttrib2dvNV();
            loadVertexAttrib2fNV();
            loadVertexAttrib2fvNV();
            loadVertexAttrib2sNV();
            loadVertexAttrib2svNV();
            loadVertexAttrib3dNV();
            loadVertexAttrib3dvNV();
            loadVertexAttrib3fNV();
            loadVertexAttrib3fvNV();
            loadVertexAttrib3sNV();
            loadVertexAttrib3svNV();
            loadVertexAttrib4dNV();
            loadVertexAttrib4dvNV();
            loadVertexAttrib4fNV();
            loadVertexAttrib4fvNV();
            loadVertexAttrib4sNV();
            loadVertexAttrib4svNV();
            loadVertexAttrib4ubNV();
            loadVertexAttrib4ubvNV();
            loadVertexAttribs1dvNV();
            loadVertexAttribs1fvNV();
            loadVertexAttribs1svNV();
            loadVertexAttribs2dvNV();
            loadVertexAttribs2fvNV();
            loadVertexAttribs2svNV();
            loadVertexAttribs3dvNV();
            loadVertexAttribs3fvNV();
            loadVertexAttribs3svNV();
            loadVertexAttribs4dvNV();
            loadVertexAttribs4fvNV();
            loadVertexAttribs4svNV();
            loadVertexAttribs4ubvNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_PROGRAM_NV = 0x8620;
        public static UInt32 GL_VERTEX_STATE_PROGRAM_NV = 0x8621;
        public static UInt32 GL_ATTRIB_ARRAY_SIZE_NV = 0x8623;
        public static UInt32 GL_ATTRIB_ARRAY_STRIDE_NV = 0x8624;
        public static UInt32 GL_ATTRIB_ARRAY_TYPE_NV = 0x8625;
        public static UInt32 GL_CURRENT_ATTRIB_NV = 0x8626;
        public static UInt32 GL_PROGRAM_LENGTH_NV = 0x8627;
        public static UInt32 GL_PROGRAM_STRING_NV = 0x8628;
        public static UInt32 GL_MODELVIEW_PROJECTION_NV = 0x8629;
        public static UInt32 GL_IDENTITY_NV = 0x862A;
        public static UInt32 GL_INVERSE_NV = 0x862B;
        public static UInt32 GL_TRANSPOSE_NV = 0x862C;
        public static UInt32 GL_INVERSE_TRANSPOSE_NV = 0x862D;
        public static UInt32 GL_MAX_TRACK_MATRIX_STACK_DEPTH_NV = 0x862E;
        public static UInt32 GL_MAX_TRACK_MATRICES_NV = 0x862F;
        public static UInt32 GL_MATRIX0_NV = 0x8630;
        public static UInt32 GL_MATRIX1_NV = 0x8631;
        public static UInt32 GL_MATRIX2_NV = 0x8632;
        public static UInt32 GL_MATRIX3_NV = 0x8633;
        public static UInt32 GL_MATRIX4_NV = 0x8634;
        public static UInt32 GL_MATRIX5_NV = 0x8635;
        public static UInt32 GL_MATRIX6_NV = 0x8636;
        public static UInt32 GL_MATRIX7_NV = 0x8637;
        public static UInt32 GL_CURRENT_MATRIX_STACK_DEPTH_NV = 0x8640;
        public static UInt32 GL_CURRENT_MATRIX_NV = 0x8641;
        public static UInt32 GL_VERTEX_PROGRAM_POINT_SIZE_NV = 0x8642;
        public static UInt32 GL_VERTEX_PROGRAM_TWO_SIDE_NV = 0x8643;
        public static UInt32 GL_PROGRAM_PARAMETER_NV = 0x8644;
        public static UInt32 GL_ATTRIB_ARRAY_POINTER_NV = 0x8645;
        public static UInt32 GL_PROGRAM_TARGET_NV = 0x8646;
        public static UInt32 GL_PROGRAM_RESIDENT_NV = 0x8647;
        public static UInt32 GL_TRACK_MATRIX_NV = 0x8648;
        public static UInt32 GL_TRACK_MATRIX_TRANSFORM_NV = 0x8649;
        public static UInt32 GL_VERTEX_PROGRAM_BINDING_NV = 0x864A;
        public static UInt32 GL_PROGRAM_ERROR_POSITION_NV = 0x864B;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY0_NV = 0x8650;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY1_NV = 0x8651;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY2_NV = 0x8652;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY3_NV = 0x8653;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY4_NV = 0x8654;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY5_NV = 0x8655;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY6_NV = 0x8656;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY7_NV = 0x8657;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY8_NV = 0x8658;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY9_NV = 0x8659;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY10_NV = 0x865A;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY11_NV = 0x865B;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY12_NV = 0x865C;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY13_NV = 0x865D;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY14_NV = 0x865E;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY15_NV = 0x865F;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB0_4_NV = 0x8660;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB1_4_NV = 0x8661;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB2_4_NV = 0x8662;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB3_4_NV = 0x8663;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB4_4_NV = 0x8664;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB5_4_NV = 0x8665;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB6_4_NV = 0x8666;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB7_4_NV = 0x8667;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB8_4_NV = 0x8668;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB9_4_NV = 0x8669;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB10_4_NV = 0x866A;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB11_4_NV = 0x866B;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB12_4_NV = 0x866C;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB13_4_NV = 0x866D;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB14_4_NV = 0x866E;
        public static UInt32 GL_MAP1_VERTEX_ATTRIB15_4_NV = 0x866F;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB0_4_NV = 0x8670;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB1_4_NV = 0x8671;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB2_4_NV = 0x8672;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB3_4_NV = 0x8673;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB4_4_NV = 0x8674;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB5_4_NV = 0x8675;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB6_4_NV = 0x8676;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB7_4_NV = 0x8677;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB8_4_NV = 0x8678;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB9_4_NV = 0x8679;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB10_4_NV = 0x867A;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB11_4_NV = 0x867B;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB12_4_NV = 0x867C;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB13_4_NV = 0x867D;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB14_4_NV = 0x867E;
        public static UInt32 GL_MAP2_VERTEX_ATTRIB15_4_NV = 0x867F;
        #endregion

        #region Commands
        internal delegate GLboolean glAreProgramsResidentNVFunc(GLsizei @n, const GLuint * @programs, GLboolean * @residences);
        internal static glAreProgramsResidentNVFunc glAreProgramsResidentNVPtr;
        internal static void loadAreProgramsResidentNV()
        {
            try
            {
                glAreProgramsResidentNVPtr = (glAreProgramsResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAreProgramsResidentNV"), typeof(glAreProgramsResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAreProgramsResidentNV'.");
            }
        }
        public static GLboolean glAreProgramsResidentNV(GLsizei @n, const GLuint * @programs, GLboolean * @residences) => glAreProgramsResidentNVPtr(@n, @programs, @residences);

        internal delegate void glBindProgramNVFunc(GLenum @target, GLuint @id);
        internal static glBindProgramNVFunc glBindProgramNVPtr;
        internal static void loadBindProgramNV()
        {
            try
            {
                glBindProgramNVPtr = (glBindProgramNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindProgramNV"), typeof(glBindProgramNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindProgramNV'.");
            }
        }
        public static void glBindProgramNV(GLenum @target, GLuint @id) => glBindProgramNVPtr(@target, @id);

        internal delegate void glDeleteProgramsNVFunc(GLsizei @n, const GLuint * @programs);
        internal static glDeleteProgramsNVFunc glDeleteProgramsNVPtr;
        internal static void loadDeleteProgramsNV()
        {
            try
            {
                glDeleteProgramsNVPtr = (glDeleteProgramsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteProgramsNV"), typeof(glDeleteProgramsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteProgramsNV'.");
            }
        }
        public static void glDeleteProgramsNV(GLsizei @n, const GLuint * @programs) => glDeleteProgramsNVPtr(@n, @programs);

        internal delegate void glExecuteProgramNVFunc(GLenum @target, GLuint @id, const GLfloat * @params);
        internal static glExecuteProgramNVFunc glExecuteProgramNVPtr;
        internal static void loadExecuteProgramNV()
        {
            try
            {
                glExecuteProgramNVPtr = (glExecuteProgramNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExecuteProgramNV"), typeof(glExecuteProgramNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExecuteProgramNV'.");
            }
        }
        public static void glExecuteProgramNV(GLenum @target, GLuint @id, const GLfloat * @params) => glExecuteProgramNVPtr(@target, @id, @params);

        internal delegate void glGenProgramsNVFunc(GLsizei @n, GLuint * @programs);
        internal static glGenProgramsNVFunc glGenProgramsNVPtr;
        internal static void loadGenProgramsNV()
        {
            try
            {
                glGenProgramsNVPtr = (glGenProgramsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenProgramsNV"), typeof(glGenProgramsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenProgramsNV'.");
            }
        }
        public static void glGenProgramsNV(GLsizei @n, GLuint * @programs) => glGenProgramsNVPtr(@n, @programs);

        internal delegate void glGetProgramParameterdvNVFunc(GLenum @target, GLuint @index, GLenum @pname, GLdouble * @params);
        internal static glGetProgramParameterdvNVFunc glGetProgramParameterdvNVPtr;
        internal static void loadGetProgramParameterdvNV()
        {
            try
            {
                glGetProgramParameterdvNVPtr = (glGetProgramParameterdvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramParameterdvNV"), typeof(glGetProgramParameterdvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramParameterdvNV'.");
            }
        }
        public static void glGetProgramParameterdvNV(GLenum @target, GLuint @index, GLenum @pname, GLdouble * @params) => glGetProgramParameterdvNVPtr(@target, @index, @pname, @params);

        internal delegate void glGetProgramParameterfvNVFunc(GLenum @target, GLuint @index, GLenum @pname, GLfloat * @params);
        internal static glGetProgramParameterfvNVFunc glGetProgramParameterfvNVPtr;
        internal static void loadGetProgramParameterfvNV()
        {
            try
            {
                glGetProgramParameterfvNVPtr = (glGetProgramParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramParameterfvNV"), typeof(glGetProgramParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramParameterfvNV'.");
            }
        }
        public static void glGetProgramParameterfvNV(GLenum @target, GLuint @index, GLenum @pname, GLfloat * @params) => glGetProgramParameterfvNVPtr(@target, @index, @pname, @params);

        internal delegate void glGetProgramivNVFunc(GLuint @id, GLenum @pname, GLint * @params);
        internal static glGetProgramivNVFunc glGetProgramivNVPtr;
        internal static void loadGetProgramivNV()
        {
            try
            {
                glGetProgramivNVPtr = (glGetProgramivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramivNV"), typeof(glGetProgramivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramivNV'.");
            }
        }
        public static void glGetProgramivNV(GLuint @id, GLenum @pname, GLint * @params) => glGetProgramivNVPtr(@id, @pname, @params);

        internal delegate void glGetProgramStringNVFunc(GLuint @id, GLenum @pname, GLubyte * @program);
        internal static glGetProgramStringNVFunc glGetProgramStringNVPtr;
        internal static void loadGetProgramStringNV()
        {
            try
            {
                glGetProgramStringNVPtr = (glGetProgramStringNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramStringNV"), typeof(glGetProgramStringNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramStringNV'.");
            }
        }
        public static void glGetProgramStringNV(GLuint @id, GLenum @pname, GLubyte * @program) => glGetProgramStringNVPtr(@id, @pname, @program);

        internal delegate void glGetTrackMatrixivNVFunc(GLenum @target, GLuint @address, GLenum @pname, GLint * @params);
        internal static glGetTrackMatrixivNVFunc glGetTrackMatrixivNVPtr;
        internal static void loadGetTrackMatrixivNV()
        {
            try
            {
                glGetTrackMatrixivNVPtr = (glGetTrackMatrixivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTrackMatrixivNV"), typeof(glGetTrackMatrixivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTrackMatrixivNV'.");
            }
        }
        public static void glGetTrackMatrixivNV(GLenum @target, GLuint @address, GLenum @pname, GLint * @params) => glGetTrackMatrixivNVPtr(@target, @address, @pname, @params);

        internal delegate void glGetVertexAttribdvNVFunc(GLuint @index, GLenum @pname, GLdouble * @params);
        internal static glGetVertexAttribdvNVFunc glGetVertexAttribdvNVPtr;
        internal static void loadGetVertexAttribdvNV()
        {
            try
            {
                glGetVertexAttribdvNVPtr = (glGetVertexAttribdvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribdvNV"), typeof(glGetVertexAttribdvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribdvNV'.");
            }
        }
        public static void glGetVertexAttribdvNV(GLuint @index, GLenum @pname, GLdouble * @params) => glGetVertexAttribdvNVPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribfvNVFunc(GLuint @index, GLenum @pname, GLfloat * @params);
        internal static glGetVertexAttribfvNVFunc glGetVertexAttribfvNVPtr;
        internal static void loadGetVertexAttribfvNV()
        {
            try
            {
                glGetVertexAttribfvNVPtr = (glGetVertexAttribfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribfvNV"), typeof(glGetVertexAttribfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribfvNV'.");
            }
        }
        public static void glGetVertexAttribfvNV(GLuint @index, GLenum @pname, GLfloat * @params) => glGetVertexAttribfvNVPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribivNVFunc(GLuint @index, GLenum @pname, GLint * @params);
        internal static glGetVertexAttribivNVFunc glGetVertexAttribivNVPtr;
        internal static void loadGetVertexAttribivNV()
        {
            try
            {
                glGetVertexAttribivNVPtr = (glGetVertexAttribivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribivNV"), typeof(glGetVertexAttribivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribivNV'.");
            }
        }
        public static void glGetVertexAttribivNV(GLuint @index, GLenum @pname, GLint * @params) => glGetVertexAttribivNVPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribPointervNVFunc(GLuint @index, GLenum @pname, void ** @pointer);
        internal static glGetVertexAttribPointervNVFunc glGetVertexAttribPointervNVPtr;
        internal static void loadGetVertexAttribPointervNV()
        {
            try
            {
                glGetVertexAttribPointervNVPtr = (glGetVertexAttribPointervNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribPointervNV"), typeof(glGetVertexAttribPointervNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribPointervNV'.");
            }
        }
        public static void glGetVertexAttribPointervNV(GLuint @index, GLenum @pname, void ** @pointer) => glGetVertexAttribPointervNVPtr(@index, @pname, @pointer);

        internal delegate GLboolean glIsProgramNVFunc(GLuint @id);
        internal static glIsProgramNVFunc glIsProgramNVPtr;
        internal static void loadIsProgramNV()
        {
            try
            {
                glIsProgramNVPtr = (glIsProgramNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsProgramNV"), typeof(glIsProgramNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsProgramNV'.");
            }
        }
        public static GLboolean glIsProgramNV(GLuint @id) => glIsProgramNVPtr(@id);

        internal delegate void glLoadProgramNVFunc(GLenum @target, GLuint @id, GLsizei @len, const GLubyte * @program);
        internal static glLoadProgramNVFunc glLoadProgramNVPtr;
        internal static void loadLoadProgramNV()
        {
            try
            {
                glLoadProgramNVPtr = (glLoadProgramNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadProgramNV"), typeof(glLoadProgramNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadProgramNV'.");
            }
        }
        public static void glLoadProgramNV(GLenum @target, GLuint @id, GLsizei @len, const GLubyte * @program) => glLoadProgramNVPtr(@target, @id, @len, @program);

        internal delegate void glProgramParameter4dNVFunc(GLenum @target, GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glProgramParameter4dNVFunc glProgramParameter4dNVPtr;
        internal static void loadProgramParameter4dNV()
        {
            try
            {
                glProgramParameter4dNVPtr = (glProgramParameter4dNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramParameter4dNV"), typeof(glProgramParameter4dNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramParameter4dNV'.");
            }
        }
        public static void glProgramParameter4dNV(GLenum @target, GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glProgramParameter4dNVPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramParameter4dvNVFunc(GLenum @target, GLuint @index, const GLdouble * @v);
        internal static glProgramParameter4dvNVFunc glProgramParameter4dvNVPtr;
        internal static void loadProgramParameter4dvNV()
        {
            try
            {
                glProgramParameter4dvNVPtr = (glProgramParameter4dvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramParameter4dvNV"), typeof(glProgramParameter4dvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramParameter4dvNV'.");
            }
        }
        public static void glProgramParameter4dvNV(GLenum @target, GLuint @index, const GLdouble * @v) => glProgramParameter4dvNVPtr(@target, @index, @v);

        internal delegate void glProgramParameter4fNVFunc(GLenum @target, GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glProgramParameter4fNVFunc glProgramParameter4fNVPtr;
        internal static void loadProgramParameter4fNV()
        {
            try
            {
                glProgramParameter4fNVPtr = (glProgramParameter4fNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramParameter4fNV"), typeof(glProgramParameter4fNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramParameter4fNV'.");
            }
        }
        public static void glProgramParameter4fNV(GLenum @target, GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glProgramParameter4fNVPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramParameter4fvNVFunc(GLenum @target, GLuint @index, const GLfloat * @v);
        internal static glProgramParameter4fvNVFunc glProgramParameter4fvNVPtr;
        internal static void loadProgramParameter4fvNV()
        {
            try
            {
                glProgramParameter4fvNVPtr = (glProgramParameter4fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramParameter4fvNV"), typeof(glProgramParameter4fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramParameter4fvNV'.");
            }
        }
        public static void glProgramParameter4fvNV(GLenum @target, GLuint @index, const GLfloat * @v) => glProgramParameter4fvNVPtr(@target, @index, @v);

        internal delegate void glProgramParameters4dvNVFunc(GLenum @target, GLuint @index, GLsizei @count, const GLdouble * @v);
        internal static glProgramParameters4dvNVFunc glProgramParameters4dvNVPtr;
        internal static void loadProgramParameters4dvNV()
        {
            try
            {
                glProgramParameters4dvNVPtr = (glProgramParameters4dvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramParameters4dvNV"), typeof(glProgramParameters4dvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramParameters4dvNV'.");
            }
        }
        public static void glProgramParameters4dvNV(GLenum @target, GLuint @index, GLsizei @count, const GLdouble * @v) => glProgramParameters4dvNVPtr(@target, @index, @count, @v);

        internal delegate void glProgramParameters4fvNVFunc(GLenum @target, GLuint @index, GLsizei @count, const GLfloat * @v);
        internal static glProgramParameters4fvNVFunc glProgramParameters4fvNVPtr;
        internal static void loadProgramParameters4fvNV()
        {
            try
            {
                glProgramParameters4fvNVPtr = (glProgramParameters4fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramParameters4fvNV"), typeof(glProgramParameters4fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramParameters4fvNV'.");
            }
        }
        public static void glProgramParameters4fvNV(GLenum @target, GLuint @index, GLsizei @count, const GLfloat * @v) => glProgramParameters4fvNVPtr(@target, @index, @count, @v);

        internal delegate void glRequestResidentProgramsNVFunc(GLsizei @n, const GLuint * @programs);
        internal static glRequestResidentProgramsNVFunc glRequestResidentProgramsNVPtr;
        internal static void loadRequestResidentProgramsNV()
        {
            try
            {
                glRequestResidentProgramsNVPtr = (glRequestResidentProgramsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRequestResidentProgramsNV"), typeof(glRequestResidentProgramsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRequestResidentProgramsNV'.");
            }
        }
        public static void glRequestResidentProgramsNV(GLsizei @n, const GLuint * @programs) => glRequestResidentProgramsNVPtr(@n, @programs);

        internal delegate void glTrackMatrixNVFunc(GLenum @target, GLuint @address, GLenum @matrix, GLenum @transform);
        internal static glTrackMatrixNVFunc glTrackMatrixNVPtr;
        internal static void loadTrackMatrixNV()
        {
            try
            {
                glTrackMatrixNVPtr = (glTrackMatrixNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTrackMatrixNV"), typeof(glTrackMatrixNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTrackMatrixNV'.");
            }
        }
        public static void glTrackMatrixNV(GLenum @target, GLuint @address, GLenum @matrix, GLenum @transform) => glTrackMatrixNVPtr(@target, @address, @matrix, @transform);

        internal delegate void glVertexAttribPointerNVFunc(GLuint @index, GLint @fsize, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glVertexAttribPointerNVFunc glVertexAttribPointerNVPtr;
        internal static void loadVertexAttribPointerNV()
        {
            try
            {
                glVertexAttribPointerNVPtr = (glVertexAttribPointerNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribPointerNV"), typeof(glVertexAttribPointerNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribPointerNV'.");
            }
        }
        public static void glVertexAttribPointerNV(GLuint @index, GLint @fsize, GLenum @type, GLsizei @stride, const void * @pointer) => glVertexAttribPointerNVPtr(@index, @fsize, @type, @stride, @pointer);

        internal delegate void glVertexAttrib1dNVFunc(GLuint @index, GLdouble @x);
        internal static glVertexAttrib1dNVFunc glVertexAttrib1dNVPtr;
        internal static void loadVertexAttrib1dNV()
        {
            try
            {
                glVertexAttrib1dNVPtr = (glVertexAttrib1dNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1dNV"), typeof(glVertexAttrib1dNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1dNV'.");
            }
        }
        public static void glVertexAttrib1dNV(GLuint @index, GLdouble @x) => glVertexAttrib1dNVPtr(@index, @x);

        internal delegate void glVertexAttrib1dvNVFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttrib1dvNVFunc glVertexAttrib1dvNVPtr;
        internal static void loadVertexAttrib1dvNV()
        {
            try
            {
                glVertexAttrib1dvNVPtr = (glVertexAttrib1dvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1dvNV"), typeof(glVertexAttrib1dvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1dvNV'.");
            }
        }
        public static void glVertexAttrib1dvNV(GLuint @index, const GLdouble * @v) => glVertexAttrib1dvNVPtr(@index, @v);

        internal delegate void glVertexAttrib1fNVFunc(GLuint @index, GLfloat @x);
        internal static glVertexAttrib1fNVFunc glVertexAttrib1fNVPtr;
        internal static void loadVertexAttrib1fNV()
        {
            try
            {
                glVertexAttrib1fNVPtr = (glVertexAttrib1fNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1fNV"), typeof(glVertexAttrib1fNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1fNV'.");
            }
        }
        public static void glVertexAttrib1fNV(GLuint @index, GLfloat @x) => glVertexAttrib1fNVPtr(@index, @x);

        internal delegate void glVertexAttrib1fvNVFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib1fvNVFunc glVertexAttrib1fvNVPtr;
        internal static void loadVertexAttrib1fvNV()
        {
            try
            {
                glVertexAttrib1fvNVPtr = (glVertexAttrib1fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1fvNV"), typeof(glVertexAttrib1fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1fvNV'.");
            }
        }
        public static void glVertexAttrib1fvNV(GLuint @index, const GLfloat * @v) => glVertexAttrib1fvNVPtr(@index, @v);

        internal delegate void glVertexAttrib1sNVFunc(GLuint @index, GLshort @x);
        internal static glVertexAttrib1sNVFunc glVertexAttrib1sNVPtr;
        internal static void loadVertexAttrib1sNV()
        {
            try
            {
                glVertexAttrib1sNVPtr = (glVertexAttrib1sNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1sNV"), typeof(glVertexAttrib1sNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1sNV'.");
            }
        }
        public static void glVertexAttrib1sNV(GLuint @index, GLshort @x) => glVertexAttrib1sNVPtr(@index, @x);

        internal delegate void glVertexAttrib1svNVFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib1svNVFunc glVertexAttrib1svNVPtr;
        internal static void loadVertexAttrib1svNV()
        {
            try
            {
                glVertexAttrib1svNVPtr = (glVertexAttrib1svNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1svNV"), typeof(glVertexAttrib1svNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1svNV'.");
            }
        }
        public static void glVertexAttrib1svNV(GLuint @index, const GLshort * @v) => glVertexAttrib1svNVPtr(@index, @v);

        internal delegate void glVertexAttrib2dNVFunc(GLuint @index, GLdouble @x, GLdouble @y);
        internal static glVertexAttrib2dNVFunc glVertexAttrib2dNVPtr;
        internal static void loadVertexAttrib2dNV()
        {
            try
            {
                glVertexAttrib2dNVPtr = (glVertexAttrib2dNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2dNV"), typeof(glVertexAttrib2dNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2dNV'.");
            }
        }
        public static void glVertexAttrib2dNV(GLuint @index, GLdouble @x, GLdouble @y) => glVertexAttrib2dNVPtr(@index, @x, @y);

        internal delegate void glVertexAttrib2dvNVFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttrib2dvNVFunc glVertexAttrib2dvNVPtr;
        internal static void loadVertexAttrib2dvNV()
        {
            try
            {
                glVertexAttrib2dvNVPtr = (glVertexAttrib2dvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2dvNV"), typeof(glVertexAttrib2dvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2dvNV'.");
            }
        }
        public static void glVertexAttrib2dvNV(GLuint @index, const GLdouble * @v) => glVertexAttrib2dvNVPtr(@index, @v);

        internal delegate void glVertexAttrib2fNVFunc(GLuint @index, GLfloat @x, GLfloat @y);
        internal static glVertexAttrib2fNVFunc glVertexAttrib2fNVPtr;
        internal static void loadVertexAttrib2fNV()
        {
            try
            {
                glVertexAttrib2fNVPtr = (glVertexAttrib2fNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2fNV"), typeof(glVertexAttrib2fNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2fNV'.");
            }
        }
        public static void glVertexAttrib2fNV(GLuint @index, GLfloat @x, GLfloat @y) => glVertexAttrib2fNVPtr(@index, @x, @y);

        internal delegate void glVertexAttrib2fvNVFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib2fvNVFunc glVertexAttrib2fvNVPtr;
        internal static void loadVertexAttrib2fvNV()
        {
            try
            {
                glVertexAttrib2fvNVPtr = (glVertexAttrib2fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2fvNV"), typeof(glVertexAttrib2fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2fvNV'.");
            }
        }
        public static void glVertexAttrib2fvNV(GLuint @index, const GLfloat * @v) => glVertexAttrib2fvNVPtr(@index, @v);

        internal delegate void glVertexAttrib2sNVFunc(GLuint @index, GLshort @x, GLshort @y);
        internal static glVertexAttrib2sNVFunc glVertexAttrib2sNVPtr;
        internal static void loadVertexAttrib2sNV()
        {
            try
            {
                glVertexAttrib2sNVPtr = (glVertexAttrib2sNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2sNV"), typeof(glVertexAttrib2sNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2sNV'.");
            }
        }
        public static void glVertexAttrib2sNV(GLuint @index, GLshort @x, GLshort @y) => glVertexAttrib2sNVPtr(@index, @x, @y);

        internal delegate void glVertexAttrib2svNVFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib2svNVFunc glVertexAttrib2svNVPtr;
        internal static void loadVertexAttrib2svNV()
        {
            try
            {
                glVertexAttrib2svNVPtr = (glVertexAttrib2svNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2svNV"), typeof(glVertexAttrib2svNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2svNV'.");
            }
        }
        public static void glVertexAttrib2svNV(GLuint @index, const GLshort * @v) => glVertexAttrib2svNVPtr(@index, @v);

        internal delegate void glVertexAttrib3dNVFunc(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glVertexAttrib3dNVFunc glVertexAttrib3dNVPtr;
        internal static void loadVertexAttrib3dNV()
        {
            try
            {
                glVertexAttrib3dNVPtr = (glVertexAttrib3dNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3dNV"), typeof(glVertexAttrib3dNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3dNV'.");
            }
        }
        public static void glVertexAttrib3dNV(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z) => glVertexAttrib3dNVPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttrib3dvNVFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttrib3dvNVFunc glVertexAttrib3dvNVPtr;
        internal static void loadVertexAttrib3dvNV()
        {
            try
            {
                glVertexAttrib3dvNVPtr = (glVertexAttrib3dvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3dvNV"), typeof(glVertexAttrib3dvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3dvNV'.");
            }
        }
        public static void glVertexAttrib3dvNV(GLuint @index, const GLdouble * @v) => glVertexAttrib3dvNVPtr(@index, @v);

        internal delegate void glVertexAttrib3fNVFunc(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glVertexAttrib3fNVFunc glVertexAttrib3fNVPtr;
        internal static void loadVertexAttrib3fNV()
        {
            try
            {
                glVertexAttrib3fNVPtr = (glVertexAttrib3fNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3fNV"), typeof(glVertexAttrib3fNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3fNV'.");
            }
        }
        public static void glVertexAttrib3fNV(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z) => glVertexAttrib3fNVPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttrib3fvNVFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib3fvNVFunc glVertexAttrib3fvNVPtr;
        internal static void loadVertexAttrib3fvNV()
        {
            try
            {
                glVertexAttrib3fvNVPtr = (glVertexAttrib3fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3fvNV"), typeof(glVertexAttrib3fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3fvNV'.");
            }
        }
        public static void glVertexAttrib3fvNV(GLuint @index, const GLfloat * @v) => glVertexAttrib3fvNVPtr(@index, @v);

        internal delegate void glVertexAttrib3sNVFunc(GLuint @index, GLshort @x, GLshort @y, GLshort @z);
        internal static glVertexAttrib3sNVFunc glVertexAttrib3sNVPtr;
        internal static void loadVertexAttrib3sNV()
        {
            try
            {
                glVertexAttrib3sNVPtr = (glVertexAttrib3sNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3sNV"), typeof(glVertexAttrib3sNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3sNV'.");
            }
        }
        public static void glVertexAttrib3sNV(GLuint @index, GLshort @x, GLshort @y, GLshort @z) => glVertexAttrib3sNVPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttrib3svNVFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib3svNVFunc glVertexAttrib3svNVPtr;
        internal static void loadVertexAttrib3svNV()
        {
            try
            {
                glVertexAttrib3svNVPtr = (glVertexAttrib3svNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3svNV"), typeof(glVertexAttrib3svNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3svNV'.");
            }
        }
        public static void glVertexAttrib3svNV(GLuint @index, const GLshort * @v) => glVertexAttrib3svNVPtr(@index, @v);

        internal delegate void glVertexAttrib4dNVFunc(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glVertexAttrib4dNVFunc glVertexAttrib4dNVPtr;
        internal static void loadVertexAttrib4dNV()
        {
            try
            {
                glVertexAttrib4dNVPtr = (glVertexAttrib4dNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4dNV"), typeof(glVertexAttrib4dNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4dNV'.");
            }
        }
        public static void glVertexAttrib4dNV(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glVertexAttrib4dNVPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4dvNVFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttrib4dvNVFunc glVertexAttrib4dvNVPtr;
        internal static void loadVertexAttrib4dvNV()
        {
            try
            {
                glVertexAttrib4dvNVPtr = (glVertexAttrib4dvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4dvNV"), typeof(glVertexAttrib4dvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4dvNV'.");
            }
        }
        public static void glVertexAttrib4dvNV(GLuint @index, const GLdouble * @v) => glVertexAttrib4dvNVPtr(@index, @v);

        internal delegate void glVertexAttrib4fNVFunc(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glVertexAttrib4fNVFunc glVertexAttrib4fNVPtr;
        internal static void loadVertexAttrib4fNV()
        {
            try
            {
                glVertexAttrib4fNVPtr = (glVertexAttrib4fNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4fNV"), typeof(glVertexAttrib4fNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4fNV'.");
            }
        }
        public static void glVertexAttrib4fNV(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glVertexAttrib4fNVPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4fvNVFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib4fvNVFunc glVertexAttrib4fvNVPtr;
        internal static void loadVertexAttrib4fvNV()
        {
            try
            {
                glVertexAttrib4fvNVPtr = (glVertexAttrib4fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4fvNV"), typeof(glVertexAttrib4fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4fvNV'.");
            }
        }
        public static void glVertexAttrib4fvNV(GLuint @index, const GLfloat * @v) => glVertexAttrib4fvNVPtr(@index, @v);

        internal delegate void glVertexAttrib4sNVFunc(GLuint @index, GLshort @x, GLshort @y, GLshort @z, GLshort @w);
        internal static glVertexAttrib4sNVFunc glVertexAttrib4sNVPtr;
        internal static void loadVertexAttrib4sNV()
        {
            try
            {
                glVertexAttrib4sNVPtr = (glVertexAttrib4sNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4sNV"), typeof(glVertexAttrib4sNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4sNV'.");
            }
        }
        public static void glVertexAttrib4sNV(GLuint @index, GLshort @x, GLshort @y, GLshort @z, GLshort @w) => glVertexAttrib4sNVPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4svNVFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib4svNVFunc glVertexAttrib4svNVPtr;
        internal static void loadVertexAttrib4svNV()
        {
            try
            {
                glVertexAttrib4svNVPtr = (glVertexAttrib4svNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4svNV"), typeof(glVertexAttrib4svNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4svNV'.");
            }
        }
        public static void glVertexAttrib4svNV(GLuint @index, const GLshort * @v) => glVertexAttrib4svNVPtr(@index, @v);

        internal delegate void glVertexAttrib4ubNVFunc(GLuint @index, GLubyte @x, GLubyte @y, GLubyte @z, GLubyte @w);
        internal static glVertexAttrib4ubNVFunc glVertexAttrib4ubNVPtr;
        internal static void loadVertexAttrib4ubNV()
        {
            try
            {
                glVertexAttrib4ubNVPtr = (glVertexAttrib4ubNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4ubNV"), typeof(glVertexAttrib4ubNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4ubNV'.");
            }
        }
        public static void glVertexAttrib4ubNV(GLuint @index, GLubyte @x, GLubyte @y, GLubyte @z, GLubyte @w) => glVertexAttrib4ubNVPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4ubvNVFunc(GLuint @index, const GLubyte * @v);
        internal static glVertexAttrib4ubvNVFunc glVertexAttrib4ubvNVPtr;
        internal static void loadVertexAttrib4ubvNV()
        {
            try
            {
                glVertexAttrib4ubvNVPtr = (glVertexAttrib4ubvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4ubvNV"), typeof(glVertexAttrib4ubvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4ubvNV'.");
            }
        }
        public static void glVertexAttrib4ubvNV(GLuint @index, const GLubyte * @v) => glVertexAttrib4ubvNVPtr(@index, @v);

        internal delegate void glVertexAttribs1dvNVFunc(GLuint @index, GLsizei @count, const GLdouble * @v);
        internal static glVertexAttribs1dvNVFunc glVertexAttribs1dvNVPtr;
        internal static void loadVertexAttribs1dvNV()
        {
            try
            {
                glVertexAttribs1dvNVPtr = (glVertexAttribs1dvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs1dvNV"), typeof(glVertexAttribs1dvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs1dvNV'.");
            }
        }
        public static void glVertexAttribs1dvNV(GLuint @index, GLsizei @count, const GLdouble * @v) => glVertexAttribs1dvNVPtr(@index, @count, @v);

        internal delegate void glVertexAttribs1fvNVFunc(GLuint @index, GLsizei @count, const GLfloat * @v);
        internal static glVertexAttribs1fvNVFunc glVertexAttribs1fvNVPtr;
        internal static void loadVertexAttribs1fvNV()
        {
            try
            {
                glVertexAttribs1fvNVPtr = (glVertexAttribs1fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs1fvNV"), typeof(glVertexAttribs1fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs1fvNV'.");
            }
        }
        public static void glVertexAttribs1fvNV(GLuint @index, GLsizei @count, const GLfloat * @v) => glVertexAttribs1fvNVPtr(@index, @count, @v);

        internal delegate void glVertexAttribs1svNVFunc(GLuint @index, GLsizei @count, const GLshort * @v);
        internal static glVertexAttribs1svNVFunc glVertexAttribs1svNVPtr;
        internal static void loadVertexAttribs1svNV()
        {
            try
            {
                glVertexAttribs1svNVPtr = (glVertexAttribs1svNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs1svNV"), typeof(glVertexAttribs1svNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs1svNV'.");
            }
        }
        public static void glVertexAttribs1svNV(GLuint @index, GLsizei @count, const GLshort * @v) => glVertexAttribs1svNVPtr(@index, @count, @v);

        internal delegate void glVertexAttribs2dvNVFunc(GLuint @index, GLsizei @count, const GLdouble * @v);
        internal static glVertexAttribs2dvNVFunc glVertexAttribs2dvNVPtr;
        internal static void loadVertexAttribs2dvNV()
        {
            try
            {
                glVertexAttribs2dvNVPtr = (glVertexAttribs2dvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs2dvNV"), typeof(glVertexAttribs2dvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs2dvNV'.");
            }
        }
        public static void glVertexAttribs2dvNV(GLuint @index, GLsizei @count, const GLdouble * @v) => glVertexAttribs2dvNVPtr(@index, @count, @v);

        internal delegate void glVertexAttribs2fvNVFunc(GLuint @index, GLsizei @count, const GLfloat * @v);
        internal static glVertexAttribs2fvNVFunc glVertexAttribs2fvNVPtr;
        internal static void loadVertexAttribs2fvNV()
        {
            try
            {
                glVertexAttribs2fvNVPtr = (glVertexAttribs2fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs2fvNV"), typeof(glVertexAttribs2fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs2fvNV'.");
            }
        }
        public static void glVertexAttribs2fvNV(GLuint @index, GLsizei @count, const GLfloat * @v) => glVertexAttribs2fvNVPtr(@index, @count, @v);

        internal delegate void glVertexAttribs2svNVFunc(GLuint @index, GLsizei @count, const GLshort * @v);
        internal static glVertexAttribs2svNVFunc glVertexAttribs2svNVPtr;
        internal static void loadVertexAttribs2svNV()
        {
            try
            {
                glVertexAttribs2svNVPtr = (glVertexAttribs2svNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs2svNV"), typeof(glVertexAttribs2svNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs2svNV'.");
            }
        }
        public static void glVertexAttribs2svNV(GLuint @index, GLsizei @count, const GLshort * @v) => glVertexAttribs2svNVPtr(@index, @count, @v);

        internal delegate void glVertexAttribs3dvNVFunc(GLuint @index, GLsizei @count, const GLdouble * @v);
        internal static glVertexAttribs3dvNVFunc glVertexAttribs3dvNVPtr;
        internal static void loadVertexAttribs3dvNV()
        {
            try
            {
                glVertexAttribs3dvNVPtr = (glVertexAttribs3dvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs3dvNV"), typeof(glVertexAttribs3dvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs3dvNV'.");
            }
        }
        public static void glVertexAttribs3dvNV(GLuint @index, GLsizei @count, const GLdouble * @v) => glVertexAttribs3dvNVPtr(@index, @count, @v);

        internal delegate void glVertexAttribs3fvNVFunc(GLuint @index, GLsizei @count, const GLfloat * @v);
        internal static glVertexAttribs3fvNVFunc glVertexAttribs3fvNVPtr;
        internal static void loadVertexAttribs3fvNV()
        {
            try
            {
                glVertexAttribs3fvNVPtr = (glVertexAttribs3fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs3fvNV"), typeof(glVertexAttribs3fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs3fvNV'.");
            }
        }
        public static void glVertexAttribs3fvNV(GLuint @index, GLsizei @count, const GLfloat * @v) => glVertexAttribs3fvNVPtr(@index, @count, @v);

        internal delegate void glVertexAttribs3svNVFunc(GLuint @index, GLsizei @count, const GLshort * @v);
        internal static glVertexAttribs3svNVFunc glVertexAttribs3svNVPtr;
        internal static void loadVertexAttribs3svNV()
        {
            try
            {
                glVertexAttribs3svNVPtr = (glVertexAttribs3svNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs3svNV"), typeof(glVertexAttribs3svNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs3svNV'.");
            }
        }
        public static void glVertexAttribs3svNV(GLuint @index, GLsizei @count, const GLshort * @v) => glVertexAttribs3svNVPtr(@index, @count, @v);

        internal delegate void glVertexAttribs4dvNVFunc(GLuint @index, GLsizei @count, const GLdouble * @v);
        internal static glVertexAttribs4dvNVFunc glVertexAttribs4dvNVPtr;
        internal static void loadVertexAttribs4dvNV()
        {
            try
            {
                glVertexAttribs4dvNVPtr = (glVertexAttribs4dvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs4dvNV"), typeof(glVertexAttribs4dvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs4dvNV'.");
            }
        }
        public static void glVertexAttribs4dvNV(GLuint @index, GLsizei @count, const GLdouble * @v) => glVertexAttribs4dvNVPtr(@index, @count, @v);

        internal delegate void glVertexAttribs4fvNVFunc(GLuint @index, GLsizei @count, const GLfloat * @v);
        internal static glVertexAttribs4fvNVFunc glVertexAttribs4fvNVPtr;
        internal static void loadVertexAttribs4fvNV()
        {
            try
            {
                glVertexAttribs4fvNVPtr = (glVertexAttribs4fvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs4fvNV"), typeof(glVertexAttribs4fvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs4fvNV'.");
            }
        }
        public static void glVertexAttribs4fvNV(GLuint @index, GLsizei @count, const GLfloat * @v) => glVertexAttribs4fvNVPtr(@index, @count, @v);

        internal delegate void glVertexAttribs4svNVFunc(GLuint @index, GLsizei @count, const GLshort * @v);
        internal static glVertexAttribs4svNVFunc glVertexAttribs4svNVPtr;
        internal static void loadVertexAttribs4svNV()
        {
            try
            {
                glVertexAttribs4svNVPtr = (glVertexAttribs4svNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs4svNV"), typeof(glVertexAttribs4svNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs4svNV'.");
            }
        }
        public static void glVertexAttribs4svNV(GLuint @index, GLsizei @count, const GLshort * @v) => glVertexAttribs4svNVPtr(@index, @count, @v);

        internal delegate void glVertexAttribs4ubvNVFunc(GLuint @index, GLsizei @count, const GLubyte * @v);
        internal static glVertexAttribs4ubvNVFunc glVertexAttribs4ubvNVPtr;
        internal static void loadVertexAttribs4ubvNV()
        {
            try
            {
                glVertexAttribs4ubvNVPtr = (glVertexAttribs4ubvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribs4ubvNV"), typeof(glVertexAttribs4ubvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribs4ubvNV'.");
            }
        }
        public static void glVertexAttribs4ubvNV(GLuint @index, GLsizei @count, const GLubyte * @v) => glVertexAttribs4ubvNVPtr(@index, @count, @v);
        #endregion
    }
}
