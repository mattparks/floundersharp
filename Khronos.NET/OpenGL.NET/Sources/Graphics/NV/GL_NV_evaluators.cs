using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_evaluators
    {
        #region Interop
        static GL_NV_evaluators()
        {
            Console.WriteLine("Initalising GL_NV_evaluators interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMapControlPointsNV();
            loadMapParameterivNV();
            loadMapParameterfvNV();
            loadGetMapControlPointsNV();
            loadGetMapParameterivNV();
            loadGetMapParameterfvNV();
            loadGetMapAttribParameterivNV();
            loadGetMapAttribParameterfvNV();
            loadEvalMapsNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_EVAL_2D_NV = 0x86C0;
        public static UInt32 GL_EVAL_TRIANGULAR_2D_NV = 0x86C1;
        public static UInt32 GL_MAP_TESSELLATION_NV = 0x86C2;
        public static UInt32 GL_MAP_ATTRIB_U_ORDER_NV = 0x86C3;
        public static UInt32 GL_MAP_ATTRIB_V_ORDER_NV = 0x86C4;
        public static UInt32 GL_EVAL_FRACTIONAL_TESSELLATION_NV = 0x86C5;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB0_NV = 0x86C6;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB1_NV = 0x86C7;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB2_NV = 0x86C8;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB3_NV = 0x86C9;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB4_NV = 0x86CA;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB5_NV = 0x86CB;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB6_NV = 0x86CC;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB7_NV = 0x86CD;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB8_NV = 0x86CE;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB9_NV = 0x86CF;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB10_NV = 0x86D0;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB11_NV = 0x86D1;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB12_NV = 0x86D2;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB13_NV = 0x86D3;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB14_NV = 0x86D4;
        public static UInt32 GL_EVAL_VERTEX_ATTRIB15_NV = 0x86D5;
        public static UInt32 GL_MAX_MAP_TESSELLATION_NV = 0x86D6;
        public static UInt32 GL_MAX_RATIONAL_EVAL_ORDER_NV = 0x86D7;
        #endregion

        #region Commands
        internal delegate void glMapControlPointsNVFunc(GLenum @target, GLuint @index, GLenum @type, GLsizei @ustride, GLsizei @vstride, GLint @uorder, GLint @vorder, GLboolean @packed, const void * @points);
        internal static glMapControlPointsNVFunc glMapControlPointsNVPtr;
        internal static void loadMapControlPointsNV()
        {
            try
            {
                glMapControlPointsNVPtr = (glMapControlPointsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapControlPointsNV"), typeof(glMapControlPointsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapControlPointsNV'.");
            }
        }
        public static void glMapControlPointsNV(GLenum @target, GLuint @index, GLenum @type, GLsizei @ustride, GLsizei @vstride, GLint @uorder, GLint @vorder, GLboolean @packed, const void * @points) => glMapControlPointsNVPtr(@target, @index, @type, @ustride, @vstride, @uorder, @vorder, @packed, @points);

        internal delegate void glMapParameterivNVFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glMapParameterivNVFunc glMapParameterivNVPtr;
        internal static void loadMapParameterivNV()
        {
            try
            {
                glMapParameterivNVPtr = (glMapParameterivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapParameterivNV"), typeof(glMapParameterivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapParameterivNV'.");
            }
        }
        public static void glMapParameterivNV(GLenum @target, GLenum @pname, const GLint * @params) => glMapParameterivNVPtr(@target, @pname, @params);

        internal delegate void glMapParameterfvNVFunc(GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glMapParameterfvNVFunc glMapParameterfvNVPtr;
        internal static void loadMapParameterfvNV()
        {
            try
            {
                glMapParameterfvNVPtr = (glMapParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapParameterfvNV"), typeof(glMapParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapParameterfvNV'.");
            }
        }
        public static void glMapParameterfvNV(GLenum @target, GLenum @pname, const GLfloat * @params) => glMapParameterfvNVPtr(@target, @pname, @params);

        internal delegate void glGetMapControlPointsNVFunc(GLenum @target, GLuint @index, GLenum @type, GLsizei @ustride, GLsizei @vstride, GLboolean @packed, void * @points);
        internal static glGetMapControlPointsNVFunc glGetMapControlPointsNVPtr;
        internal static void loadGetMapControlPointsNV()
        {
            try
            {
                glGetMapControlPointsNVPtr = (glGetMapControlPointsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMapControlPointsNV"), typeof(glGetMapControlPointsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMapControlPointsNV'.");
            }
        }
        public static void glGetMapControlPointsNV(GLenum @target, GLuint @index, GLenum @type, GLsizei @ustride, GLsizei @vstride, GLboolean @packed, void * @points) => glGetMapControlPointsNVPtr(@target, @index, @type, @ustride, @vstride, @packed, @points);

        internal delegate void glGetMapParameterivNVFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetMapParameterivNVFunc glGetMapParameterivNVPtr;
        internal static void loadGetMapParameterivNV()
        {
            try
            {
                glGetMapParameterivNVPtr = (glGetMapParameterivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMapParameterivNV"), typeof(glGetMapParameterivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMapParameterivNV'.");
            }
        }
        public static void glGetMapParameterivNV(GLenum @target, GLenum @pname, GLint * @params) => glGetMapParameterivNVPtr(@target, @pname, @params);

        internal delegate void glGetMapParameterfvNVFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetMapParameterfvNVFunc glGetMapParameterfvNVPtr;
        internal static void loadGetMapParameterfvNV()
        {
            try
            {
                glGetMapParameterfvNVPtr = (glGetMapParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMapParameterfvNV"), typeof(glGetMapParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMapParameterfvNV'.");
            }
        }
        public static void glGetMapParameterfvNV(GLenum @target, GLenum @pname, GLfloat * @params) => glGetMapParameterfvNVPtr(@target, @pname, @params);

        internal delegate void glGetMapAttribParameterivNVFunc(GLenum @target, GLuint @index, GLenum @pname, GLint * @params);
        internal static glGetMapAttribParameterivNVFunc glGetMapAttribParameterivNVPtr;
        internal static void loadGetMapAttribParameterivNV()
        {
            try
            {
                glGetMapAttribParameterivNVPtr = (glGetMapAttribParameterivNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMapAttribParameterivNV"), typeof(glGetMapAttribParameterivNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMapAttribParameterivNV'.");
            }
        }
        public static void glGetMapAttribParameterivNV(GLenum @target, GLuint @index, GLenum @pname, GLint * @params) => glGetMapAttribParameterivNVPtr(@target, @index, @pname, @params);

        internal delegate void glGetMapAttribParameterfvNVFunc(GLenum @target, GLuint @index, GLenum @pname, GLfloat * @params);
        internal static glGetMapAttribParameterfvNVFunc glGetMapAttribParameterfvNVPtr;
        internal static void loadGetMapAttribParameterfvNV()
        {
            try
            {
                glGetMapAttribParameterfvNVPtr = (glGetMapAttribParameterfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMapAttribParameterfvNV"), typeof(glGetMapAttribParameterfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMapAttribParameterfvNV'.");
            }
        }
        public static void glGetMapAttribParameterfvNV(GLenum @target, GLuint @index, GLenum @pname, GLfloat * @params) => glGetMapAttribParameterfvNVPtr(@target, @index, @pname, @params);

        internal delegate void glEvalMapsNVFunc(GLenum @target, GLenum @mode);
        internal static glEvalMapsNVFunc glEvalMapsNVPtr;
        internal static void loadEvalMapsNV()
        {
            try
            {
                glEvalMapsNVPtr = (glEvalMapsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalMapsNV"), typeof(glEvalMapsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalMapsNV'.");
            }
        }
        public static void glEvalMapsNV(GLenum @target, GLenum @mode) => glEvalMapsNVPtr(@target, @mode);
        #endregion
    }
}
