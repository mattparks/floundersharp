using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_conservative_raster_dilate
    {
        #region Interop
        static GL_NV_conservative_raster_dilate()
        {
            Console.WriteLine("Initalising GL_NV_conservative_raster_dilate interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadConservativeRasterParameterfNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_CONSERVATIVE_RASTER_DILATE_NV = 0x9379;
        public static UInt32 GL_CONSERVATIVE_RASTER_DILATE_RANGE_NV = 0x937A;
        public static UInt32 GL_CONSERVATIVE_RASTER_DILATE_GRANULARITY_NV = 0x937B;
        #endregion

        #region Commands
        internal delegate void glConservativeRasterParameterfNVFunc(GLenum @pname, GLfloat @value);
        internal static glConservativeRasterParameterfNVFunc glConservativeRasterParameterfNVPtr;
        internal static void loadConservativeRasterParameterfNV()
        {
            try
            {
                glConservativeRasterParameterfNVPtr = (glConservativeRasterParameterfNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConservativeRasterParameterfNV"), typeof(glConservativeRasterParameterfNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConservativeRasterParameterfNV'.");
            }
        }
        public static void glConservativeRasterParameterfNV(GLenum @pname, GLfloat @value) => glConservativeRasterParameterfNVPtr(@pname, @value);
        #endregion
    }
}
