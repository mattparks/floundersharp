using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_sample_locations
    {
        #region Interop
        static GL_NV_sample_locations()
        {
            Console.WriteLine("Initalising GL_NV_sample_locations interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFramebufferSampleLocationsfvNV();
            loadNamedFramebufferSampleLocationsfvNV();
            loadResolveDepthValuesNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SAMPLE_LOCATION_SUBPIXEL_BITS_NV = 0x933D;
        public static UInt32 GL_SAMPLE_LOCATION_PIXEL_GRID_WIDTH_NV = 0x933E;
        public static UInt32 GL_SAMPLE_LOCATION_PIXEL_GRID_HEIGHT_NV = 0x933F;
        public static UInt32 GL_PROGRAMMABLE_SAMPLE_LOCATION_TABLE_SIZE_NV = 0x9340;
        public static UInt32 GL_SAMPLE_LOCATION_NV = 0x8E50;
        public static UInt32 GL_PROGRAMMABLE_SAMPLE_LOCATION_NV = 0x9341;
        public static UInt32 GL_FRAMEBUFFER_PROGRAMMABLE_SAMPLE_LOCATIONS_NV = 0x9342;
        public static UInt32 GL_FRAMEBUFFER_SAMPLE_LOCATION_PIXEL_GRID_NV = 0x9343;
        #endregion

        #region Commands
        internal delegate void glFramebufferSampleLocationsfvNVFunc(GLenum @target, GLuint @start, GLsizei @count, const GLfloat * @v);
        internal static glFramebufferSampleLocationsfvNVFunc glFramebufferSampleLocationsfvNVPtr;
        internal static void loadFramebufferSampleLocationsfvNV()
        {
            try
            {
                glFramebufferSampleLocationsfvNVPtr = (glFramebufferSampleLocationsfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferSampleLocationsfvNV"), typeof(glFramebufferSampleLocationsfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferSampleLocationsfvNV'.");
            }
        }
        public static void glFramebufferSampleLocationsfvNV(GLenum @target, GLuint @start, GLsizei @count, const GLfloat * @v) => glFramebufferSampleLocationsfvNVPtr(@target, @start, @count, @v);

        internal delegate void glNamedFramebufferSampleLocationsfvNVFunc(GLuint @framebuffer, GLuint @start, GLsizei @count, const GLfloat * @v);
        internal static glNamedFramebufferSampleLocationsfvNVFunc glNamedFramebufferSampleLocationsfvNVPtr;
        internal static void loadNamedFramebufferSampleLocationsfvNV()
        {
            try
            {
                glNamedFramebufferSampleLocationsfvNVPtr = (glNamedFramebufferSampleLocationsfvNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferSampleLocationsfvNV"), typeof(glNamedFramebufferSampleLocationsfvNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferSampleLocationsfvNV'.");
            }
        }
        public static void glNamedFramebufferSampleLocationsfvNV(GLuint @framebuffer, GLuint @start, GLsizei @count, const GLfloat * @v) => glNamedFramebufferSampleLocationsfvNVPtr(@framebuffer, @start, @count, @v);

        internal delegate void glResolveDepthValuesNVFunc();
        internal static glResolveDepthValuesNVFunc glResolveDepthValuesNVPtr;
        internal static void loadResolveDepthValuesNV()
        {
            try
            {
                glResolveDepthValuesNVPtr = (glResolveDepthValuesNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glResolveDepthValuesNV"), typeof(glResolveDepthValuesNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glResolveDepthValuesNV'.");
            }
        }
        public static void glResolveDepthValuesNV() => glResolveDepthValuesNVPtr();
        #endregion
    }
}
