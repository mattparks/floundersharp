using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_draw_texture
    {
        #region Interop
        static GL_NV_draw_texture()
        {
            Console.WriteLine("Initalising GL_NV_draw_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawTextureNV();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glDrawTextureNVFunc(GLuint @texture, GLuint @sampler, GLfloat @x0, GLfloat @y0, GLfloat @x1, GLfloat @y1, GLfloat @z, GLfloat @s0, GLfloat @t0, GLfloat @s1, GLfloat @t1);
        internal static glDrawTextureNVFunc glDrawTextureNVPtr;
        internal static void loadDrawTextureNV()
        {
            try
            {
                glDrawTextureNVPtr = (glDrawTextureNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTextureNV"), typeof(glDrawTextureNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTextureNV'.");
            }
        }
        public static void glDrawTextureNV(GLuint @texture, GLuint @sampler, GLfloat @x0, GLfloat @y0, GLfloat @x1, GLfloat @y1, GLfloat @z, GLfloat @s0, GLfloat @t0, GLfloat @s1, GLfloat @t1) => glDrawTextureNVPtr(@texture, @sampler, @x0, @y0, @x1, @y1, @z, @s0, @t0, @s1, @t1);
        #endregion
    }
}
