using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_compute_program5
    {
        #region Interop
        static GL_NV_compute_program5()
        {
            Console.WriteLine("Initalising GL_NV_compute_program5 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPUTE_PROGRAM_NV = 0x90FB;
        public static UInt32 GL_COMPUTE_PROGRAM_PARAMETER_BUFFER_NV = 0x90FC;
        #endregion

        #region Commands
        #endregion
    }
}
