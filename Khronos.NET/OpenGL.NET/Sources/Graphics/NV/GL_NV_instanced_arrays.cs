using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_instanced_arrays
    {
        #region Interop
        static GL_NV_instanced_arrays()
        {
            Console.WriteLine("Initalising GL_NV_instanced_arrays interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexAttribDivisorNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_DIVISOR_NV = 0x88FE;
        #endregion

        #region Commands
        internal delegate void glVertexAttribDivisorNVFunc(GLuint @index, GLuint @divisor);
        internal static glVertexAttribDivisorNVFunc glVertexAttribDivisorNVPtr;
        internal static void loadVertexAttribDivisorNV()
        {
            try
            {
                glVertexAttribDivisorNVPtr = (glVertexAttribDivisorNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribDivisorNV"), typeof(glVertexAttribDivisorNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribDivisorNV'.");
            }
        }
        public static void glVertexAttribDivisorNV(GLuint @index, GLuint @divisor) => glVertexAttribDivisorNVPtr(@index, @divisor);
        #endregion
    }
}
