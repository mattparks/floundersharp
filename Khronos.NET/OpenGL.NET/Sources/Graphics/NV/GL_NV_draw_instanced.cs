using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_draw_instanced
    {
        #region Interop
        static GL_NV_draw_instanced()
        {
            Console.WriteLine("Initalising GL_NV_draw_instanced interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawArraysInstancedNV();
            loadDrawElementsInstancedNV();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glDrawArraysInstancedNVFunc(GLenum @mode, GLint @first, GLsizei @count, GLsizei @primcount);
        internal static glDrawArraysInstancedNVFunc glDrawArraysInstancedNVPtr;
        internal static void loadDrawArraysInstancedNV()
        {
            try
            {
                glDrawArraysInstancedNVPtr = (glDrawArraysInstancedNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysInstancedNV"), typeof(glDrawArraysInstancedNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysInstancedNV'.");
            }
        }
        public static void glDrawArraysInstancedNV(GLenum @mode, GLint @first, GLsizei @count, GLsizei @primcount) => glDrawArraysInstancedNVPtr(@mode, @first, @count, @primcount);

        internal delegate void glDrawElementsInstancedNVFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @primcount);
        internal static glDrawElementsInstancedNVFunc glDrawElementsInstancedNVPtr;
        internal static void loadDrawElementsInstancedNV()
        {
            try
            {
                glDrawElementsInstancedNVPtr = (glDrawElementsInstancedNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedNV"), typeof(glDrawElementsInstancedNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedNV'.");
            }
        }
        public static void glDrawElementsInstancedNV(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @primcount) => glDrawElementsInstancedNVPtr(@mode, @count, @type, @indices, @primcount);
        #endregion
    }
}
