using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_bindless_texture
    {
        #region Interop
        static GL_NV_bindless_texture()
        {
            Console.WriteLine("Initalising GL_NV_bindless_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetTextureHandleNV();
            loadGetTextureSamplerHandleNV();
            loadMakeTextureHandleResidentNV();
            loadMakeTextureHandleNonResidentNV();
            loadGetImageHandleNV();
            loadMakeImageHandleResidentNV();
            loadMakeImageHandleNonResidentNV();
            loadUniformHandleui64NV();
            loadUniformHandleui64vNV();
            loadProgramUniformHandleui64NV();
            loadProgramUniformHandleui64vNV();
            loadIsTextureHandleResidentNV();
            loadIsImageHandleResidentNV();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate GLuint64 glGetTextureHandleNVFunc(GLuint @texture);
        internal static glGetTextureHandleNVFunc glGetTextureHandleNVPtr;
        internal static void loadGetTextureHandleNV()
        {
            try
            {
                glGetTextureHandleNVPtr = (glGetTextureHandleNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureHandleNV"), typeof(glGetTextureHandleNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureHandleNV'.");
            }
        }
        public static GLuint64 glGetTextureHandleNV(GLuint @texture) => glGetTextureHandleNVPtr(@texture);

        internal delegate GLuint64 glGetTextureSamplerHandleNVFunc(GLuint @texture, GLuint @sampler);
        internal static glGetTextureSamplerHandleNVFunc glGetTextureSamplerHandleNVPtr;
        internal static void loadGetTextureSamplerHandleNV()
        {
            try
            {
                glGetTextureSamplerHandleNVPtr = (glGetTextureSamplerHandleNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureSamplerHandleNV"), typeof(glGetTextureSamplerHandleNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureSamplerHandleNV'.");
            }
        }
        public static GLuint64 glGetTextureSamplerHandleNV(GLuint @texture, GLuint @sampler) => glGetTextureSamplerHandleNVPtr(@texture, @sampler);

        internal delegate void glMakeTextureHandleResidentNVFunc(GLuint64 @handle);
        internal static glMakeTextureHandleResidentNVFunc glMakeTextureHandleResidentNVPtr;
        internal static void loadMakeTextureHandleResidentNV()
        {
            try
            {
                glMakeTextureHandleResidentNVPtr = (glMakeTextureHandleResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMakeTextureHandleResidentNV"), typeof(glMakeTextureHandleResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMakeTextureHandleResidentNV'.");
            }
        }
        public static void glMakeTextureHandleResidentNV(GLuint64 @handle) => glMakeTextureHandleResidentNVPtr(@handle);

        internal delegate void glMakeTextureHandleNonResidentNVFunc(GLuint64 @handle);
        internal static glMakeTextureHandleNonResidentNVFunc glMakeTextureHandleNonResidentNVPtr;
        internal static void loadMakeTextureHandleNonResidentNV()
        {
            try
            {
                glMakeTextureHandleNonResidentNVPtr = (glMakeTextureHandleNonResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMakeTextureHandleNonResidentNV"), typeof(glMakeTextureHandleNonResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMakeTextureHandleNonResidentNV'.");
            }
        }
        public static void glMakeTextureHandleNonResidentNV(GLuint64 @handle) => glMakeTextureHandleNonResidentNVPtr(@handle);

        internal delegate GLuint64 glGetImageHandleNVFunc(GLuint @texture, GLint @level, GLboolean @layered, GLint @layer, GLenum @format);
        internal static glGetImageHandleNVFunc glGetImageHandleNVPtr;
        internal static void loadGetImageHandleNV()
        {
            try
            {
                glGetImageHandleNVPtr = (glGetImageHandleNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetImageHandleNV"), typeof(glGetImageHandleNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetImageHandleNV'.");
            }
        }
        public static GLuint64 glGetImageHandleNV(GLuint @texture, GLint @level, GLboolean @layered, GLint @layer, GLenum @format) => glGetImageHandleNVPtr(@texture, @level, @layered, @layer, @format);

        internal delegate void glMakeImageHandleResidentNVFunc(GLuint64 @handle, GLenum @access);
        internal static glMakeImageHandleResidentNVFunc glMakeImageHandleResidentNVPtr;
        internal static void loadMakeImageHandleResidentNV()
        {
            try
            {
                glMakeImageHandleResidentNVPtr = (glMakeImageHandleResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMakeImageHandleResidentNV"), typeof(glMakeImageHandleResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMakeImageHandleResidentNV'.");
            }
        }
        public static void glMakeImageHandleResidentNV(GLuint64 @handle, GLenum @access) => glMakeImageHandleResidentNVPtr(@handle, @access);

        internal delegate void glMakeImageHandleNonResidentNVFunc(GLuint64 @handle);
        internal static glMakeImageHandleNonResidentNVFunc glMakeImageHandleNonResidentNVPtr;
        internal static void loadMakeImageHandleNonResidentNV()
        {
            try
            {
                glMakeImageHandleNonResidentNVPtr = (glMakeImageHandleNonResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMakeImageHandleNonResidentNV"), typeof(glMakeImageHandleNonResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMakeImageHandleNonResidentNV'.");
            }
        }
        public static void glMakeImageHandleNonResidentNV(GLuint64 @handle) => glMakeImageHandleNonResidentNVPtr(@handle);

        internal delegate void glUniformHandleui64NVFunc(GLint @location, GLuint64 @value);
        internal static glUniformHandleui64NVFunc glUniformHandleui64NVPtr;
        internal static void loadUniformHandleui64NV()
        {
            try
            {
                glUniformHandleui64NVPtr = (glUniformHandleui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformHandleui64NV"), typeof(glUniformHandleui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformHandleui64NV'.");
            }
        }
        public static void glUniformHandleui64NV(GLint @location, GLuint64 @value) => glUniformHandleui64NVPtr(@location, @value);

        internal delegate void glUniformHandleui64vNVFunc(GLint @location, GLsizei @count, const GLuint64 * @value);
        internal static glUniformHandleui64vNVFunc glUniformHandleui64vNVPtr;
        internal static void loadUniformHandleui64vNV()
        {
            try
            {
                glUniformHandleui64vNVPtr = (glUniformHandleui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformHandleui64vNV"), typeof(glUniformHandleui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformHandleui64vNV'.");
            }
        }
        public static void glUniformHandleui64vNV(GLint @location, GLsizei @count, const GLuint64 * @value) => glUniformHandleui64vNVPtr(@location, @count, @value);

        internal delegate void glProgramUniformHandleui64NVFunc(GLuint @program, GLint @location, GLuint64 @value);
        internal static glProgramUniformHandleui64NVFunc glProgramUniformHandleui64NVPtr;
        internal static void loadProgramUniformHandleui64NV()
        {
            try
            {
                glProgramUniformHandleui64NVPtr = (glProgramUniformHandleui64NVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformHandleui64NV"), typeof(glProgramUniformHandleui64NVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformHandleui64NV'.");
            }
        }
        public static void glProgramUniformHandleui64NV(GLuint @program, GLint @location, GLuint64 @value) => glProgramUniformHandleui64NVPtr(@program, @location, @value);

        internal delegate void glProgramUniformHandleui64vNVFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint64 * @values);
        internal static glProgramUniformHandleui64vNVFunc glProgramUniformHandleui64vNVPtr;
        internal static void loadProgramUniformHandleui64vNV()
        {
            try
            {
                glProgramUniformHandleui64vNVPtr = (glProgramUniformHandleui64vNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformHandleui64vNV"), typeof(glProgramUniformHandleui64vNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformHandleui64vNV'.");
            }
        }
        public static void glProgramUniformHandleui64vNV(GLuint @program, GLint @location, GLsizei @count, const GLuint64 * @values) => glProgramUniformHandleui64vNVPtr(@program, @location, @count, @values);

        internal delegate GLboolean glIsTextureHandleResidentNVFunc(GLuint64 @handle);
        internal static glIsTextureHandleResidentNVFunc glIsTextureHandleResidentNVPtr;
        internal static void loadIsTextureHandleResidentNV()
        {
            try
            {
                glIsTextureHandleResidentNVPtr = (glIsTextureHandleResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsTextureHandleResidentNV"), typeof(glIsTextureHandleResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsTextureHandleResidentNV'.");
            }
        }
        public static GLboolean glIsTextureHandleResidentNV(GLuint64 @handle) => glIsTextureHandleResidentNVPtr(@handle);

        internal delegate GLboolean glIsImageHandleResidentNVFunc(GLuint64 @handle);
        internal static glIsImageHandleResidentNVFunc glIsImageHandleResidentNVPtr;
        internal static void loadIsImageHandleResidentNV()
        {
            try
            {
                glIsImageHandleResidentNVPtr = (glIsImageHandleResidentNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsImageHandleResidentNV"), typeof(glIsImageHandleResidentNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsImageHandleResidentNV'.");
            }
        }
        public static GLboolean glIsImageHandleResidentNV(GLuint64 @handle) => glIsImageHandleResidentNVPtr(@handle);
        #endregion
    }
}
