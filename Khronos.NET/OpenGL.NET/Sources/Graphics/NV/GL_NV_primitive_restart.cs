using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_primitive_restart
    {
        #region Interop
        static GL_NV_primitive_restart()
        {
            Console.WriteLine("Initalising GL_NV_primitive_restart interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPrimitiveRestartNV();
            loadPrimitiveRestartIndexNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PRIMITIVE_RESTART_NV = 0x8558;
        public static UInt32 GL_PRIMITIVE_RESTART_INDEX_NV = 0x8559;
        #endregion

        #region Commands
        internal delegate void glPrimitiveRestartNVFunc();
        internal static glPrimitiveRestartNVFunc glPrimitiveRestartNVPtr;
        internal static void loadPrimitiveRestartNV()
        {
            try
            {
                glPrimitiveRestartNVPtr = (glPrimitiveRestartNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPrimitiveRestartNV"), typeof(glPrimitiveRestartNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPrimitiveRestartNV'.");
            }
        }
        public static void glPrimitiveRestartNV() => glPrimitiveRestartNVPtr();

        internal delegate void glPrimitiveRestartIndexNVFunc(GLuint @index);
        internal static glPrimitiveRestartIndexNVFunc glPrimitiveRestartIndexNVPtr;
        internal static void loadPrimitiveRestartIndexNV()
        {
            try
            {
                glPrimitiveRestartIndexNVPtr = (glPrimitiveRestartIndexNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPrimitiveRestartIndexNV"), typeof(glPrimitiveRestartIndexNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPrimitiveRestartIndexNV'.");
            }
        }
        public static void glPrimitiveRestartIndexNV(GLuint @index) => glPrimitiveRestartIndexNVPtr(@index);
        #endregion
    }
}
