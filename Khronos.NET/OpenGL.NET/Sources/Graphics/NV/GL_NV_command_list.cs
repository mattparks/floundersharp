using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_command_list
    {
        #region Interop
        static GL_NV_command_list()
        {
            Console.WriteLine("Initalising GL_NV_command_list interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCreateStatesNV();
            loadDeleteStatesNV();
            loadIsStateNV();
            loadStateCaptureNV();
            loadGetCommandHeaderNV();
            loadGetStageIndexNV();
            loadDrawCommandsNV();
            loadDrawCommandsAddressNV();
            loadDrawCommandsStatesNV();
            loadDrawCommandsStatesAddressNV();
            loadCreateCommandListsNV();
            loadDeleteCommandListsNV();
            loadIsCommandListNV();
            loadListDrawCommandsStatesClientNV();
            loadCommandListSegmentsNV();
            loadCompileCommandListNV();
            loadCallCommandListNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TERMINATE_SEQUENCE_COMMAND_NV = 0x0000;
        public static UInt32 GL_NOP_COMMAND_NV = 0x0001;
        public static UInt32 GL_DRAW_ELEMENTS_COMMAND_NV = 0x0002;
        public static UInt32 GL_DRAW_ARRAYS_COMMAND_NV = 0x0003;
        public static UInt32 GL_DRAW_ELEMENTS_STRIP_COMMAND_NV = 0x0004;
        public static UInt32 GL_DRAW_ARRAYS_STRIP_COMMAND_NV = 0x0005;
        public static UInt32 GL_DRAW_ELEMENTS_INSTANCED_COMMAND_NV = 0x0006;
        public static UInt32 GL_DRAW_ARRAYS_INSTANCED_COMMAND_NV = 0x0007;
        public static UInt32 GL_ELEMENT_ADDRESS_COMMAND_NV = 0x0008;
        public static UInt32 GL_ATTRIBUTE_ADDRESS_COMMAND_NV = 0x0009;
        public static UInt32 GL_UNIFORM_ADDRESS_COMMAND_NV = 0x000A;
        public static UInt32 GL_BLEND_COLOR_COMMAND_NV = 0x000B;
        public static UInt32 GL_STENCIL_REF_COMMAND_NV = 0x000C;
        public static UInt32 GL_LINE_WIDTH_COMMAND_NV = 0x000D;
        public static UInt32 GL_POLYGON_OFFSET_COMMAND_NV = 0x000E;
        public static UInt32 GL_ALPHA_REF_COMMAND_NV = 0x000F;
        public static UInt32 GL_VIEWPORT_COMMAND_NV = 0x0010;
        public static UInt32 GL_SCISSOR_COMMAND_NV = 0x0011;
        public static UInt32 GL_FRONT_FACE_COMMAND_NV = 0x0012;
        #endregion

        #region Commands
        internal delegate void glCreateStatesNVFunc(GLsizei @n, GLuint * @states);
        internal static glCreateStatesNVFunc glCreateStatesNVPtr;
        internal static void loadCreateStatesNV()
        {
            try
            {
                glCreateStatesNVPtr = (glCreateStatesNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateStatesNV"), typeof(glCreateStatesNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateStatesNV'.");
            }
        }
        public static void glCreateStatesNV(GLsizei @n, GLuint * @states) => glCreateStatesNVPtr(@n, @states);

        internal delegate void glDeleteStatesNVFunc(GLsizei @n, const GLuint * @states);
        internal static glDeleteStatesNVFunc glDeleteStatesNVPtr;
        internal static void loadDeleteStatesNV()
        {
            try
            {
                glDeleteStatesNVPtr = (glDeleteStatesNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteStatesNV"), typeof(glDeleteStatesNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteStatesNV'.");
            }
        }
        public static void glDeleteStatesNV(GLsizei @n, const GLuint * @states) => glDeleteStatesNVPtr(@n, @states);

        internal delegate GLboolean glIsStateNVFunc(GLuint @state);
        internal static glIsStateNVFunc glIsStateNVPtr;
        internal static void loadIsStateNV()
        {
            try
            {
                glIsStateNVPtr = (glIsStateNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsStateNV"), typeof(glIsStateNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsStateNV'.");
            }
        }
        public static GLboolean glIsStateNV(GLuint @state) => glIsStateNVPtr(@state);

        internal delegate void glStateCaptureNVFunc(GLuint @state, GLenum @mode);
        internal static glStateCaptureNVFunc glStateCaptureNVPtr;
        internal static void loadStateCaptureNV()
        {
            try
            {
                glStateCaptureNVPtr = (glStateCaptureNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStateCaptureNV"), typeof(glStateCaptureNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStateCaptureNV'.");
            }
        }
        public static void glStateCaptureNV(GLuint @state, GLenum @mode) => glStateCaptureNVPtr(@state, @mode);

        internal delegate GLuint glGetCommandHeaderNVFunc(GLenum @tokenID, GLuint @size);
        internal static glGetCommandHeaderNVFunc glGetCommandHeaderNVPtr;
        internal static void loadGetCommandHeaderNV()
        {
            try
            {
                glGetCommandHeaderNVPtr = (glGetCommandHeaderNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCommandHeaderNV"), typeof(glGetCommandHeaderNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCommandHeaderNV'.");
            }
        }
        public static GLuint glGetCommandHeaderNV(GLenum @tokenID, GLuint @size) => glGetCommandHeaderNVPtr(@tokenID, @size);

        internal delegate GLushort glGetStageIndexNVFunc(GLenum @shadertype);
        internal static glGetStageIndexNVFunc glGetStageIndexNVPtr;
        internal static void loadGetStageIndexNV()
        {
            try
            {
                glGetStageIndexNVPtr = (glGetStageIndexNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetStageIndexNV"), typeof(glGetStageIndexNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetStageIndexNV'.");
            }
        }
        public static GLushort glGetStageIndexNV(GLenum @shadertype) => glGetStageIndexNVPtr(@shadertype);

        internal delegate void glDrawCommandsNVFunc(GLenum @primitiveMode, GLuint @buffer, const GLintptr * @indirects, const GLsizei * @sizes, GLuint @count);
        internal static glDrawCommandsNVFunc glDrawCommandsNVPtr;
        internal static void loadDrawCommandsNV()
        {
            try
            {
                glDrawCommandsNVPtr = (glDrawCommandsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawCommandsNV"), typeof(glDrawCommandsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawCommandsNV'.");
            }
        }
        public static void glDrawCommandsNV(GLenum @primitiveMode, GLuint @buffer, const GLintptr * @indirects, const GLsizei * @sizes, GLuint @count) => glDrawCommandsNVPtr(@primitiveMode, @buffer, @indirects, @sizes, @count);

        internal delegate void glDrawCommandsAddressNVFunc(GLenum @primitiveMode, const GLuint64 * @indirects, const GLsizei * @sizes, GLuint @count);
        internal static glDrawCommandsAddressNVFunc glDrawCommandsAddressNVPtr;
        internal static void loadDrawCommandsAddressNV()
        {
            try
            {
                glDrawCommandsAddressNVPtr = (glDrawCommandsAddressNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawCommandsAddressNV"), typeof(glDrawCommandsAddressNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawCommandsAddressNV'.");
            }
        }
        public static void glDrawCommandsAddressNV(GLenum @primitiveMode, const GLuint64 * @indirects, const GLsizei * @sizes, GLuint @count) => glDrawCommandsAddressNVPtr(@primitiveMode, @indirects, @sizes, @count);

        internal delegate void glDrawCommandsStatesNVFunc(GLuint @buffer, const GLintptr * @indirects, const GLsizei * @sizes, const GLuint * @states, const GLuint * @fbos, GLuint @count);
        internal static glDrawCommandsStatesNVFunc glDrawCommandsStatesNVPtr;
        internal static void loadDrawCommandsStatesNV()
        {
            try
            {
                glDrawCommandsStatesNVPtr = (glDrawCommandsStatesNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawCommandsStatesNV"), typeof(glDrawCommandsStatesNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawCommandsStatesNV'.");
            }
        }
        public static void glDrawCommandsStatesNV(GLuint @buffer, const GLintptr * @indirects, const GLsizei * @sizes, const GLuint * @states, const GLuint * @fbos, GLuint @count) => glDrawCommandsStatesNVPtr(@buffer, @indirects, @sizes, @states, @fbos, @count);

        internal delegate void glDrawCommandsStatesAddressNVFunc(const GLuint64 * @indirects, const GLsizei * @sizes, const GLuint * @states, const GLuint * @fbos, GLuint @count);
        internal static glDrawCommandsStatesAddressNVFunc glDrawCommandsStatesAddressNVPtr;
        internal static void loadDrawCommandsStatesAddressNV()
        {
            try
            {
                glDrawCommandsStatesAddressNVPtr = (glDrawCommandsStatesAddressNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawCommandsStatesAddressNV"), typeof(glDrawCommandsStatesAddressNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawCommandsStatesAddressNV'.");
            }
        }
        public static void glDrawCommandsStatesAddressNV(const GLuint64 * @indirects, const GLsizei * @sizes, const GLuint * @states, const GLuint * @fbos, GLuint @count) => glDrawCommandsStatesAddressNVPtr(@indirects, @sizes, @states, @fbos, @count);

        internal delegate void glCreateCommandListsNVFunc(GLsizei @n, GLuint * @lists);
        internal static glCreateCommandListsNVFunc glCreateCommandListsNVPtr;
        internal static void loadCreateCommandListsNV()
        {
            try
            {
                glCreateCommandListsNVPtr = (glCreateCommandListsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateCommandListsNV"), typeof(glCreateCommandListsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateCommandListsNV'.");
            }
        }
        public static void glCreateCommandListsNV(GLsizei @n, GLuint * @lists) => glCreateCommandListsNVPtr(@n, @lists);

        internal delegate void glDeleteCommandListsNVFunc(GLsizei @n, const GLuint * @lists);
        internal static glDeleteCommandListsNVFunc glDeleteCommandListsNVPtr;
        internal static void loadDeleteCommandListsNV()
        {
            try
            {
                glDeleteCommandListsNVPtr = (glDeleteCommandListsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteCommandListsNV"), typeof(glDeleteCommandListsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteCommandListsNV'.");
            }
        }
        public static void glDeleteCommandListsNV(GLsizei @n, const GLuint * @lists) => glDeleteCommandListsNVPtr(@n, @lists);

        internal delegate GLboolean glIsCommandListNVFunc(GLuint @list);
        internal static glIsCommandListNVFunc glIsCommandListNVPtr;
        internal static void loadIsCommandListNV()
        {
            try
            {
                glIsCommandListNVPtr = (glIsCommandListNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsCommandListNV"), typeof(glIsCommandListNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsCommandListNV'.");
            }
        }
        public static GLboolean glIsCommandListNV(GLuint @list) => glIsCommandListNVPtr(@list);

        internal delegate void glListDrawCommandsStatesClientNVFunc(GLuint @list, GLuint @segment, const void ** @indirects, const GLsizei * @sizes, const GLuint * @states, const GLuint * @fbos, GLuint @count);
        internal static glListDrawCommandsStatesClientNVFunc glListDrawCommandsStatesClientNVPtr;
        internal static void loadListDrawCommandsStatesClientNV()
        {
            try
            {
                glListDrawCommandsStatesClientNVPtr = (glListDrawCommandsStatesClientNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glListDrawCommandsStatesClientNV"), typeof(glListDrawCommandsStatesClientNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glListDrawCommandsStatesClientNV'.");
            }
        }
        public static void glListDrawCommandsStatesClientNV(GLuint @list, GLuint @segment, const void ** @indirects, const GLsizei * @sizes, const GLuint * @states, const GLuint * @fbos, GLuint @count) => glListDrawCommandsStatesClientNVPtr(@list, @segment, @indirects, @sizes, @states, @fbos, @count);

        internal delegate void glCommandListSegmentsNVFunc(GLuint @list, GLuint @segments);
        internal static glCommandListSegmentsNVFunc glCommandListSegmentsNVPtr;
        internal static void loadCommandListSegmentsNV()
        {
            try
            {
                glCommandListSegmentsNVPtr = (glCommandListSegmentsNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCommandListSegmentsNV"), typeof(glCommandListSegmentsNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCommandListSegmentsNV'.");
            }
        }
        public static void glCommandListSegmentsNV(GLuint @list, GLuint @segments) => glCommandListSegmentsNVPtr(@list, @segments);

        internal delegate void glCompileCommandListNVFunc(GLuint @list);
        internal static glCompileCommandListNVFunc glCompileCommandListNVPtr;
        internal static void loadCompileCommandListNV()
        {
            try
            {
                glCompileCommandListNVPtr = (glCompileCommandListNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompileCommandListNV"), typeof(glCompileCommandListNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompileCommandListNV'.");
            }
        }
        public static void glCompileCommandListNV(GLuint @list) => glCompileCommandListNVPtr(@list);

        internal delegate void glCallCommandListNVFunc(GLuint @list);
        internal static glCallCommandListNVFunc glCallCommandListNVPtr;
        internal static void loadCallCommandListNV()
        {
            try
            {
                glCallCommandListNVPtr = (glCallCommandListNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCallCommandListNV"), typeof(glCallCommandListNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCallCommandListNV'.");
            }
        }
        public static void glCallCommandListNV(GLuint @list) => glCallCommandListNVPtr(@list);
        #endregion
    }
}
