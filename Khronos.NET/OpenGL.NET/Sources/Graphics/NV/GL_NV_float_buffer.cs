using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_float_buffer
    {
        #region Interop
        static GL_NV_float_buffer()
        {
            Console.WriteLine("Initalising GL_NV_float_buffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FLOAT_R_NV = 0x8880;
        public static UInt32 GL_FLOAT_RG_NV = 0x8881;
        public static UInt32 GL_FLOAT_RGB_NV = 0x8882;
        public static UInt32 GL_FLOAT_RGBA_NV = 0x8883;
        public static UInt32 GL_FLOAT_R16_NV = 0x8884;
        public static UInt32 GL_FLOAT_R32_NV = 0x8885;
        public static UInt32 GL_FLOAT_RG16_NV = 0x8886;
        public static UInt32 GL_FLOAT_RG32_NV = 0x8887;
        public static UInt32 GL_FLOAT_RGB16_NV = 0x8888;
        public static UInt32 GL_FLOAT_RGB32_NV = 0x8889;
        public static UInt32 GL_FLOAT_RGBA16_NV = 0x888A;
        public static UInt32 GL_FLOAT_RGBA32_NV = 0x888B;
        public static UInt32 GL_TEXTURE_FLOAT_COMPONENTS_NV = 0x888C;
        public static UInt32 GL_FLOAT_CLEAR_COLOR_VALUE_NV = 0x888D;
        public static UInt32 GL_FLOAT_RGBA_MODE_NV = 0x888E;
        #endregion

        #region Commands
        #endregion
    }
}
