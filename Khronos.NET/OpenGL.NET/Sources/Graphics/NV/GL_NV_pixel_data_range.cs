using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_pixel_data_range
    {
        #region Interop
        static GL_NV_pixel_data_range()
        {
            Console.WriteLine("Initalising GL_NV_pixel_data_range interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPixelDataRangeNV();
            loadFlushPixelDataRangeNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_WRITE_PIXEL_DATA_RANGE_NV = 0x8878;
        public static UInt32 GL_READ_PIXEL_DATA_RANGE_NV = 0x8879;
        public static UInt32 GL_WRITE_PIXEL_DATA_RANGE_LENGTH_NV = 0x887A;
        public static UInt32 GL_READ_PIXEL_DATA_RANGE_LENGTH_NV = 0x887B;
        public static UInt32 GL_WRITE_PIXEL_DATA_RANGE_POINTER_NV = 0x887C;
        public static UInt32 GL_READ_PIXEL_DATA_RANGE_POINTER_NV = 0x887D;
        #endregion

        #region Commands
        internal delegate void glPixelDataRangeNVFunc(GLenum @target, GLsizei @length, const void * @pointer);
        internal static glPixelDataRangeNVFunc glPixelDataRangeNVPtr;
        internal static void loadPixelDataRangeNV()
        {
            try
            {
                glPixelDataRangeNVPtr = (glPixelDataRangeNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelDataRangeNV"), typeof(glPixelDataRangeNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelDataRangeNV'.");
            }
        }
        public static void glPixelDataRangeNV(GLenum @target, GLsizei @length, const void * @pointer) => glPixelDataRangeNVPtr(@target, @length, @pointer);

        internal delegate void glFlushPixelDataRangeNVFunc(GLenum @target);
        internal static glFlushPixelDataRangeNVFunc glFlushPixelDataRangeNVPtr;
        internal static void loadFlushPixelDataRangeNV()
        {
            try
            {
                glFlushPixelDataRangeNVPtr = (glFlushPixelDataRangeNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlushPixelDataRangeNV"), typeof(glFlushPixelDataRangeNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlushPixelDataRangeNV'.");
            }
        }
        public static void glFlushPixelDataRangeNV(GLenum @target) => glFlushPixelDataRangeNVPtr(@target);
        #endregion
    }
}
