using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_vertex_array_range
    {
        #region Interop
        static GL_NV_vertex_array_range()
        {
            Console.WriteLine("Initalising GL_NV_vertex_array_range interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFlushVertexArrayRangeNV();
            loadVertexArrayRangeNV();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ARRAY_RANGE_NV = 0x851D;
        public static UInt32 GL_VERTEX_ARRAY_RANGE_LENGTH_NV = 0x851E;
        public static UInt32 GL_VERTEX_ARRAY_RANGE_VALID_NV = 0x851F;
        public static UInt32 GL_MAX_VERTEX_ARRAY_RANGE_ELEMENT_NV = 0x8520;
        public static UInt32 GL_VERTEX_ARRAY_RANGE_POINTER_NV = 0x8521;
        #endregion

        #region Commands
        internal delegate void glFlushVertexArrayRangeNVFunc();
        internal static glFlushVertexArrayRangeNVFunc glFlushVertexArrayRangeNVPtr;
        internal static void loadFlushVertexArrayRangeNV()
        {
            try
            {
                glFlushVertexArrayRangeNVPtr = (glFlushVertexArrayRangeNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlushVertexArrayRangeNV"), typeof(glFlushVertexArrayRangeNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlushVertexArrayRangeNV'.");
            }
        }
        public static void glFlushVertexArrayRangeNV() => glFlushVertexArrayRangeNVPtr();

        internal delegate void glVertexArrayRangeNVFunc(GLsizei @length, const void * @pointer);
        internal static glVertexArrayRangeNVFunc glVertexArrayRangeNVPtr;
        internal static void loadVertexArrayRangeNV()
        {
            try
            {
                glVertexArrayRangeNVPtr = (glVertexArrayRangeNVFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayRangeNV"), typeof(glVertexArrayRangeNVFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayRangeNV'.");
            }
        }
        public static void glVertexArrayRangeNV(GLsizei @length, const void * @pointer) => glVertexArrayRangeNVPtr(@length, @pointer);
        #endregion
    }
}
