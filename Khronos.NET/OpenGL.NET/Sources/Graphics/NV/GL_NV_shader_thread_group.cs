using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_shader_thread_group
    {
        #region Interop
        static GL_NV_shader_thread_group()
        {
            Console.WriteLine("Initalising GL_NV_shader_thread_group interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_WARP_SIZE_NV = 0x9339;
        public static UInt32 GL_WARPS_PER_SM_NV = 0x933A;
        public static UInt32 GL_SM_COUNT_NV = 0x933B;
        #endregion

        #region Commands
        #endregion
    }
}
