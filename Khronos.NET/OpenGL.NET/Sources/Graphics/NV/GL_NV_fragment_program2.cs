using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_fragment_program2
    {
        #region Interop
        static GL_NV_fragment_program2()
        {
            Console.WriteLine("Initalising GL_NV_fragment_program2 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_PROGRAM_EXEC_INSTRUCTIONS_NV = 0x88F4;
        public static UInt32 GL_MAX_PROGRAM_CALL_DEPTH_NV = 0x88F5;
        public static UInt32 GL_MAX_PROGRAM_IF_DEPTH_NV = 0x88F6;
        public static UInt32 GL_MAX_PROGRAM_LOOP_DEPTH_NV = 0x88F7;
        public static UInt32 GL_MAX_PROGRAM_LOOP_COUNT_NV = 0x88F8;
        #endregion

        #region Commands
        #endregion
    }
}
