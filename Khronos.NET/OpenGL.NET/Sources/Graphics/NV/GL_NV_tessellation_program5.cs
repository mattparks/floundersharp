using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NV_tessellation_program5
    {
        #region Interop
        static GL_NV_tessellation_program5()
        {
            Console.WriteLine("Initalising GL_NV_tessellation_program5 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_PROGRAM_PATCH_ATTRIBS_NV = 0x86D8;
        public static UInt32 GL_TESS_CONTROL_PROGRAM_NV = 0x891E;
        public static UInt32 GL_TESS_EVALUATION_PROGRAM_NV = 0x891F;
        public static UInt32 GL_TESS_CONTROL_PROGRAM_PARAMETER_BUFFER_NV = 0x8C74;
        public static UInt32 GL_TESS_EVALUATION_PROGRAM_PARAMETER_BUFFER_NV = 0x8C75;
        #endregion

        #region Commands
        #endregion
    }
}
