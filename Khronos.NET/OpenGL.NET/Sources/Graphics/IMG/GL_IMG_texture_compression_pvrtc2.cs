using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IMG_texture_compression_pvrtc2
    {
        #region Interop
        static GL_IMG_texture_compression_pvrtc2()
        {
            Console.WriteLine("Initalising GL_IMG_texture_compression_pvrtc2 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_RGBA_PVRTC_2BPPV2_IMG = 0x9137;
        public static UInt32 GL_COMPRESSED_RGBA_PVRTC_4BPPV2_IMG = 0x9138;
        #endregion

        #region Commands
        #endregion
    }
}
