using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IMG_read_format
    {
        #region Interop
        static GL_IMG_read_format()
        {
            Console.WriteLine("Initalising GL_IMG_read_format interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_BGRA_IMG = 0x80E1;
        public static UInt32 GL_UNSIGNED_SHORT_4_4_4_4_REV_IMG = 0x8365;
        #endregion

        #region Commands
        #endregion
    }
}
