using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IMG_shader_binary
    {
        #region Interop
        static GL_IMG_shader_binary()
        {
            Console.WriteLine("Initalising GL_IMG_shader_binary interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SGX_BINARY_IMG = 0x8C0A;
        #endregion

        #region Commands
        #endregion
    }
}
