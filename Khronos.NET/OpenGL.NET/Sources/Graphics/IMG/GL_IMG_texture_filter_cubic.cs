using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IMG_texture_filter_cubic
    {
        #region Interop
        static GL_IMG_texture_filter_cubic()
        {
            Console.WriteLine("Initalising GL_IMG_texture_filter_cubic interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_CUBIC_IMG = 0x9139;
        public static UInt32 GL_CUBIC_MIPMAP_NEAREST_IMG = 0x913A;
        public static UInt32 GL_CUBIC_MIPMAP_LINEAR_IMG = 0x913B;
        #endregion

        #region Commands
        #endregion
    }
}
