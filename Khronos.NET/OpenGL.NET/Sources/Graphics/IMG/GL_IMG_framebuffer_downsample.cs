using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IMG_framebuffer_downsample
    {
        #region Interop
        static GL_IMG_framebuffer_downsample()
        {
            Console.WriteLine("Initalising GL_IMG_framebuffer_downsample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFramebufferTexture2DDownsampleIMG();
            loadFramebufferTextureLayerDownsampleIMG();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_AND_DOWNSAMPLE_IMG = 0x913C;
        public static UInt32 GL_NUM_DOWNSAMPLE_SCALES_IMG = 0x913D;
        public static UInt32 GL_DOWNSAMPLE_SCALES_IMG = 0x913E;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_SCALE_IMG = 0x913F;
        #endregion

        #region Commands
        internal delegate void glFramebufferTexture2DDownsampleIMGFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLint @xscale, GLint @yscale);
        internal static glFramebufferTexture2DDownsampleIMGFunc glFramebufferTexture2DDownsampleIMGPtr;
        internal static void loadFramebufferTexture2DDownsampleIMG()
        {
            try
            {
                glFramebufferTexture2DDownsampleIMGPtr = (glFramebufferTexture2DDownsampleIMGFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture2DDownsampleIMG"), typeof(glFramebufferTexture2DDownsampleIMGFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture2DDownsampleIMG'.");
            }
        }
        public static void glFramebufferTexture2DDownsampleIMG(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLint @xscale, GLint @yscale) => glFramebufferTexture2DDownsampleIMGPtr(@target, @attachment, @textarget, @texture, @level, @xscale, @yscale);

        internal delegate void glFramebufferTextureLayerDownsampleIMGFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer, GLint @xscale, GLint @yscale);
        internal static glFramebufferTextureLayerDownsampleIMGFunc glFramebufferTextureLayerDownsampleIMGPtr;
        internal static void loadFramebufferTextureLayerDownsampleIMG()
        {
            try
            {
                glFramebufferTextureLayerDownsampleIMGPtr = (glFramebufferTextureLayerDownsampleIMGFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureLayerDownsampleIMG"), typeof(glFramebufferTextureLayerDownsampleIMGFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureLayerDownsampleIMG'.");
            }
        }
        public static void glFramebufferTextureLayerDownsampleIMG(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer, GLint @xscale, GLint @yscale) => glFramebufferTextureLayerDownsampleIMGPtr(@target, @attachment, @texture, @level, @layer, @xscale, @yscale);
        #endregion
    }
}
