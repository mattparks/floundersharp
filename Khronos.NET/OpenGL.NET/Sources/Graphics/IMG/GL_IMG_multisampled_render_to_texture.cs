using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IMG_multisampled_render_to_texture
    {
        #region Interop
        static GL_IMG_multisampled_render_to_texture()
        {
            Console.WriteLine("Initalising GL_IMG_multisampled_render_to_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadRenderbufferStorageMultisampleIMG();
            loadFramebufferTexture2DMultisampleIMG();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RENDERBUFFER_SAMPLES_IMG = 0x9133;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_IMG = 0x9134;
        public static UInt32 GL_MAX_SAMPLES_IMG = 0x9135;
        public static UInt32 GL_TEXTURE_SAMPLES_IMG = 0x9136;
        #endregion

        #region Commands
        internal delegate void glRenderbufferStorageMultisampleIMGFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageMultisampleIMGFunc glRenderbufferStorageMultisampleIMGPtr;
        internal static void loadRenderbufferStorageMultisampleIMG()
        {
            try
            {
                glRenderbufferStorageMultisampleIMGPtr = (glRenderbufferStorageMultisampleIMGFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorageMultisampleIMG"), typeof(glRenderbufferStorageMultisampleIMGFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorageMultisampleIMG'.");
            }
        }
        public static void glRenderbufferStorageMultisampleIMG(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStorageMultisampleIMGPtr(@target, @samples, @internalformat, @width, @height);

        internal delegate void glFramebufferTexture2DMultisampleIMGFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLsizei @samples);
        internal static glFramebufferTexture2DMultisampleIMGFunc glFramebufferTexture2DMultisampleIMGPtr;
        internal static void loadFramebufferTexture2DMultisampleIMG()
        {
            try
            {
                glFramebufferTexture2DMultisampleIMGPtr = (glFramebufferTexture2DMultisampleIMGFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture2DMultisampleIMG"), typeof(glFramebufferTexture2DMultisampleIMGFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture2DMultisampleIMG'.");
            }
        }
        public static void glFramebufferTexture2DMultisampleIMG(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLsizei @samples) => glFramebufferTexture2DMultisampleIMGPtr(@target, @attachment, @textarget, @texture, @level, @samples);
        #endregion
    }
}
