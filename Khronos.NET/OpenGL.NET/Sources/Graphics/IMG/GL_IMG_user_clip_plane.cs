using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IMG_user_clip_plane
    {
        #region Interop
        static GL_IMG_user_clip_plane()
        {
            Console.WriteLine("Initalising GL_IMG_user_clip_plane interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadClipPlanefIMG();
            loadClipPlanexIMG();
        }
        #endregion

        #region Enums
        public static UInt32 GL_CLIP_PLANE0_IMG = 0x3000;
        public static UInt32 GL_CLIP_PLANE1_IMG = 0x3001;
        public static UInt32 GL_CLIP_PLANE2_IMG = 0x3002;
        public static UInt32 GL_CLIP_PLANE3_IMG = 0x3003;
        public static UInt32 GL_CLIP_PLANE4_IMG = 0x3004;
        public static UInt32 GL_CLIP_PLANE5_IMG = 0x3005;
        public static UInt32 GL_MAX_CLIP_PLANES_IMG = 0x0D32;
        #endregion

        #region Commands
        internal delegate void glClipPlanefIMGFunc(GLenum @p, const GLfloat * @eqn);
        internal static glClipPlanefIMGFunc glClipPlanefIMGPtr;
        internal static void loadClipPlanefIMG()
        {
            try
            {
                glClipPlanefIMGPtr = (glClipPlanefIMGFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClipPlanefIMG"), typeof(glClipPlanefIMGFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClipPlanefIMG'.");
            }
        }
        public static void glClipPlanefIMG(GLenum @p, const GLfloat * @eqn) => glClipPlanefIMGPtr(@p, @eqn);

        internal delegate void glClipPlanexIMGFunc(GLenum @p, const GLfixed * @eqn);
        internal static glClipPlanexIMGFunc glClipPlanexIMGPtr;
        internal static void loadClipPlanexIMG()
        {
            try
            {
                glClipPlanexIMGPtr = (glClipPlanexIMGFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClipPlanexIMG"), typeof(glClipPlanexIMGFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClipPlanexIMG'.");
            }
        }
        public static void glClipPlanexIMG(GLenum @p, const GLfixed * @eqn) => glClipPlanexIMGPtr(@p, @eqn);
        #endregion
    }
}
