using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IMG_texture_env_enhanced_fixed_function
    {
        #region Interop
        static GL_IMG_texture_env_enhanced_fixed_function()
        {
            Console.WriteLine("Initalising GL_IMG_texture_env_enhanced_fixed_function interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MODULATE_COLOR_IMG = 0x8C04;
        public static UInt32 GL_RECIP_ADD_SIGNED_ALPHA_IMG = 0x8C05;
        public static UInt32 GL_TEXTURE_ALPHA_MODULATE_IMG = 0x8C06;
        public static UInt32 GL_FACTOR_ALPHA_MODULATE_IMG = 0x8C07;
        public static UInt32 GL_FRAGMENT_ALPHA_MODULATE_IMG = 0x8C08;
        public static UInt32 GL_ADD_BLEND_IMG = 0x8C09;
        public static UInt32 GL_DOT3_RGBA_IMG = 0x86AF;
        #endregion

        #region Commands
        #endregion
    }
}
