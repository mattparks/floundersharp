using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IMG_texture_compression_pvrtc
    {
        #region Interop
        static GL_IMG_texture_compression_pvrtc()
        {
            Console.WriteLine("Initalising GL_IMG_texture_compression_pvrtc interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG = 0x8C00;
        public static UInt32 GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG = 0x8C01;
        public static UInt32 GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG = 0x8C02;
        public static UInt32 GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG = 0x8C03;
        #endregion

        #region Commands
        #endregion
    }
}
