using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NVX_conditional_render
    {
        #region Interop
        static GL_NVX_conditional_render()
        {
            Console.WriteLine("Initalising GL_NVX_conditional_render interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBeginConditionalRenderNVX();
            loadEndConditionalRenderNVX();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glBeginConditionalRenderNVXFunc(GLuint @id);
        internal static glBeginConditionalRenderNVXFunc glBeginConditionalRenderNVXPtr;
        internal static void loadBeginConditionalRenderNVX()
        {
            try
            {
                glBeginConditionalRenderNVXPtr = (glBeginConditionalRenderNVXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginConditionalRenderNVX"), typeof(glBeginConditionalRenderNVXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginConditionalRenderNVX'.");
            }
        }
        public static void glBeginConditionalRenderNVX(GLuint @id) => glBeginConditionalRenderNVXPtr(@id);

        internal delegate void glEndConditionalRenderNVXFunc();
        internal static glEndConditionalRenderNVXFunc glEndConditionalRenderNVXPtr;
        internal static void loadEndConditionalRenderNVX()
        {
            try
            {
                glEndConditionalRenderNVXPtr = (glEndConditionalRenderNVXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndConditionalRenderNVX"), typeof(glEndConditionalRenderNVXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndConditionalRenderNVX'.");
            }
        }
        public static void glEndConditionalRenderNVX() => glEndConditionalRenderNVXPtr();
        #endregion
    }
}
