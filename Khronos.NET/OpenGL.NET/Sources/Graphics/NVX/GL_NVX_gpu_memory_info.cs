using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_NVX_gpu_memory_info
    {
        #region Interop
        static GL_NVX_gpu_memory_info()
        {
            Console.WriteLine("Initalising GL_NVX_gpu_memory_info interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX = 0x9047;
        public static UInt32 GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX = 0x9048;
        public static UInt32 GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX = 0x9049;
        public static UInt32 GL_GPU_MEMORY_INFO_EVICTION_COUNT_NVX = 0x904A;
        public static UInt32 GL_GPU_MEMORY_INFO_EVICTED_MEMORY_NVX = 0x904B;
        #endregion

        #region Commands
        #endregion
    }
}
