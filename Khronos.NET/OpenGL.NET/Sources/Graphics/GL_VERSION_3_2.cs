using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL32
    {
        #region Interop
        static GL32()
        {
            Console.WriteLine("Initalising GL32 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawElementsBaseVertex();
            loadDrawRangeElementsBaseVertex();
            loadDrawElementsInstancedBaseVertex();
            loadMultiDrawElementsBaseVertex();
            loadProvokingVertex();
            loadFenceSync();
            loadIsSync();
            loadDeleteSync();
            loadClientWaitSync();
            loadWaitSync();
            loadGetInteger64v();
            loadGetSynciv();
            loadGetInteger64i_v();
            loadGetBufferParameteri64v();
            loadFramebufferTexture();
            loadTexImage2DMultisample();
            loadTexImage3DMultisample();
            loadGetMultisamplefv();
            loadSampleMaski();
        }
        #endregion

        #region Enums
        public static UInt32 GL_CONTEXT_CORE_PROFILE_BIT = 0x00000001;
        public static UInt32 GL_CONTEXT_COMPATIBILITY_PROFILE_BIT = 0x00000002;
        public static UInt32 GL_LINES_ADJACENCY = 0x000A;
        public static UInt32 GL_LINE_STRIP_ADJACENCY = 0x000B;
        public static UInt32 GL_TRIANGLES_ADJACENCY = 0x000C;
        public static UInt32 GL_TRIANGLE_STRIP_ADJACENCY = 0x000D;
        public static UInt32 GL_PROGRAM_POINT_SIZE = 0x8642;
        public static UInt32 GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS = 0x8C29;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_LAYERED = 0x8DA7;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS = 0x8DA8;
        public static UInt32 GL_GEOMETRY_SHADER = 0x8DD9;
        public static UInt32 GL_GEOMETRY_VERTICES_OUT = 0x8916;
        public static UInt32 GL_GEOMETRY_INPUT_TYPE = 0x8917;
        public static UInt32 GL_GEOMETRY_OUTPUT_TYPE = 0x8918;
        public static UInt32 GL_MAX_GEOMETRY_UNIFORM_COMPONENTS = 0x8DDF;
        public static UInt32 GL_MAX_GEOMETRY_OUTPUT_VERTICES = 0x8DE0;
        public static UInt32 GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS = 0x8DE1;
        public static UInt32 GL_MAX_VERTEX_OUTPUT_COMPONENTS = 0x9122;
        public static UInt32 GL_MAX_GEOMETRY_INPUT_COMPONENTS = 0x9123;
        public static UInt32 GL_MAX_GEOMETRY_OUTPUT_COMPONENTS = 0x9124;
        public static UInt32 GL_MAX_FRAGMENT_INPUT_COMPONENTS = 0x9125;
        public static UInt32 GL_CONTEXT_PROFILE_MASK = 0x9126;
        public static UInt32 GL_DEPTH_CLAMP = 0x864F;
        public static UInt32 GL_QUADS_FOLLOW_PROVOKING_VERTEX_CONVENTION = 0x8E4C;
        public static UInt32 GL_FIRST_VERTEX_CONVENTION = 0x8E4D;
        public static UInt32 GL_LAST_VERTEX_CONVENTION = 0x8E4E;
        public static UInt32 GL_PROVOKING_VERTEX = 0x8E4F;
        public static UInt32 GL_TEXTURE_CUBE_MAP_SEAMLESS = 0x884F;
        public static UInt32 GL_MAX_SERVER_WAIT_TIMEOUT = 0x9111;
        public static UInt32 GL_OBJECT_TYPE = 0x9112;
        public static UInt32 GL_SYNC_CONDITION = 0x9113;
        public static UInt32 GL_SYNC_STATUS = 0x9114;
        public static UInt32 GL_SYNC_FLAGS = 0x9115;
        public static UInt32 GL_SYNC_FENCE = 0x9116;
        public static UInt32 GL_SYNC_GPU_COMMANDS_COMPLETE = 0x9117;
        public static UInt32 GL_UNSIGNALED = 0x9118;
        public static UInt32 GL_SIGNALED = 0x9119;
        public static UInt32 GL_ALREADY_SIGNALED = 0x911A;
        public static UInt32 GL_TIMEOUT_EXPIRED = 0x911B;
        public static UInt32 GL_CONDITION_SATISFIED = 0x911C;
        public static UInt32 GL_WAIT_FAILED = 0x911D;
        public static UInt64 GL_TIMEOUT_IGNORED = 0xFFFFFFFFFFFFFFFF;
        public static UInt32 GL_SYNC_FLUSH_COMMANDS_BIT = 0x00000001;
        public static UInt32 GL_SAMPLE_POSITION = 0x8E50;
        public static UInt32 GL_SAMPLE_MASK = 0x8E51;
        public static UInt32 GL_SAMPLE_MASK_VALUE = 0x8E52;
        public static UInt32 GL_MAX_SAMPLE_MASK_WORDS = 0x8E59;
        public static UInt32 GL_TEXTURE_2D_MULTISAMPLE = 0x9100;
        public static UInt32 GL_PROXY_TEXTURE_2D_MULTISAMPLE = 0x9101;
        public static UInt32 GL_TEXTURE_2D_MULTISAMPLE_ARRAY = 0x9102;
        public static UInt32 GL_PROXY_TEXTURE_2D_MULTISAMPLE_ARRAY = 0x9103;
        public static UInt32 GL_TEXTURE_BINDING_2D_MULTISAMPLE = 0x9104;
        public static UInt32 GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY = 0x9105;
        public static UInt32 GL_TEXTURE_SAMPLES = 0x9106;
        public static UInt32 GL_TEXTURE_FIXED_SAMPLE_LOCATIONS = 0x9107;
        public static UInt32 GL_SAMPLER_2D_MULTISAMPLE = 0x9108;
        public static UInt32 GL_INT_SAMPLER_2D_MULTISAMPLE = 0x9109;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE = 0x910A;
        public static UInt32 GL_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910B;
        public static UInt32 GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910C;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910D;
        public static UInt32 GL_MAX_COLOR_TEXTURE_SAMPLES = 0x910E;
        public static UInt32 GL_MAX_DEPTH_TEXTURE_SAMPLES = 0x910F;
        public static UInt32 GL_MAX_INTEGER_SAMPLES = 0x9110;
        #endregion

        #region Commands
        internal delegate void glDrawElementsBaseVertexFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex);
        internal static glDrawElementsBaseVertexFunc glDrawElementsBaseVertexPtr;
        internal static void loadDrawElementsBaseVertex()
        {
            try
            {
                glDrawElementsBaseVertexPtr = (glDrawElementsBaseVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsBaseVertex"), typeof(glDrawElementsBaseVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsBaseVertex'.");
            }
        }
        public static void glDrawElementsBaseVertex(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex) => glDrawElementsBaseVertexPtr(@mode, @count, @type, @indices, @basevertex);

        internal delegate void glDrawRangeElementsBaseVertexFunc(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex);
        internal static glDrawRangeElementsBaseVertexFunc glDrawRangeElementsBaseVertexPtr;
        internal static void loadDrawRangeElementsBaseVertex()
        {
            try
            {
                glDrawRangeElementsBaseVertexPtr = (glDrawRangeElementsBaseVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawRangeElementsBaseVertex"), typeof(glDrawRangeElementsBaseVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawRangeElementsBaseVertex'.");
            }
        }
        public static void glDrawRangeElementsBaseVertex(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex) => glDrawRangeElementsBaseVertexPtr(@mode, @start, @end, @count, @type, @indices, @basevertex);

        internal delegate void glDrawElementsInstancedBaseVertexFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex);
        internal static glDrawElementsInstancedBaseVertexFunc glDrawElementsInstancedBaseVertexPtr;
        internal static void loadDrawElementsInstancedBaseVertex()
        {
            try
            {
                glDrawElementsInstancedBaseVertexPtr = (glDrawElementsInstancedBaseVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedBaseVertex"), typeof(glDrawElementsInstancedBaseVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedBaseVertex'.");
            }
        }
        public static void glDrawElementsInstancedBaseVertex(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex) => glDrawElementsInstancedBaseVertexPtr(@mode, @count, @type, @indices, @instancecount, @basevertex);

        internal delegate void glMultiDrawElementsBaseVertexFunc(GLenum @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @drawcount, const GLint * @basevertex);
        internal static glMultiDrawElementsBaseVertexFunc glMultiDrawElementsBaseVertexPtr;
        internal static void loadMultiDrawElementsBaseVertex()
        {
            try
            {
                glMultiDrawElementsBaseVertexPtr = (glMultiDrawElementsBaseVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementsBaseVertex"), typeof(glMultiDrawElementsBaseVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementsBaseVertex'.");
            }
        }
        public static void glMultiDrawElementsBaseVertex(GLenum @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @drawcount, const GLint * @basevertex) => glMultiDrawElementsBaseVertexPtr(@mode, @count, @type, @indices, @drawcount, @basevertex);

        internal delegate void glProvokingVertexFunc(GLenum @mode);
        internal static glProvokingVertexFunc glProvokingVertexPtr;
        internal static void loadProvokingVertex()
        {
            try
            {
                glProvokingVertexPtr = (glProvokingVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProvokingVertex"), typeof(glProvokingVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProvokingVertex'.");
            }
        }
        public static void glProvokingVertex(GLenum @mode) => glProvokingVertexPtr(@mode);

        internal delegate GLsync glFenceSyncFunc(GLenum @condition, GLbitfield @flags);
        internal static glFenceSyncFunc glFenceSyncPtr;
        internal static void loadFenceSync()
        {
            try
            {
                glFenceSyncPtr = (glFenceSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFenceSync"), typeof(glFenceSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFenceSync'.");
            }
        }
        public static GLsync glFenceSync(GLenum @condition, GLbitfield @flags) => glFenceSyncPtr(@condition, @flags);

        internal delegate GLboolean glIsSyncFunc(GLsync @sync);
        internal static glIsSyncFunc glIsSyncPtr;
        internal static void loadIsSync()
        {
            try
            {
                glIsSyncPtr = (glIsSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsSync"), typeof(glIsSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsSync'.");
            }
        }
        public static GLboolean glIsSync(GLsync @sync) => glIsSyncPtr(@sync);

        internal delegate void glDeleteSyncFunc(GLsync @sync);
        internal static glDeleteSyncFunc glDeleteSyncPtr;
        internal static void loadDeleteSync()
        {
            try
            {
                glDeleteSyncPtr = (glDeleteSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteSync"), typeof(glDeleteSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteSync'.");
            }
        }
        public static void glDeleteSync(GLsync @sync) => glDeleteSyncPtr(@sync);

        internal delegate GLenum glClientWaitSyncFunc(GLsync @sync, GLbitfield @flags, GLuint64 @timeout);
        internal static glClientWaitSyncFunc glClientWaitSyncPtr;
        internal static void loadClientWaitSync()
        {
            try
            {
                glClientWaitSyncPtr = (glClientWaitSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClientWaitSync"), typeof(glClientWaitSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClientWaitSync'.");
            }
        }
        public static GLenum glClientWaitSync(GLsync @sync, GLbitfield @flags, GLuint64 @timeout) => glClientWaitSyncPtr(@sync, @flags, @timeout);

        internal delegate void glWaitSyncFunc(GLsync @sync, GLbitfield @flags, GLuint64 @timeout);
        internal static glWaitSyncFunc glWaitSyncPtr;
        internal static void loadWaitSync()
        {
            try
            {
                glWaitSyncPtr = (glWaitSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWaitSync"), typeof(glWaitSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWaitSync'.");
            }
        }
        public static void glWaitSync(GLsync @sync, GLbitfield @flags, GLuint64 @timeout) => glWaitSyncPtr(@sync, @flags, @timeout);

        internal delegate void glGetInteger64vFunc(GLenum @pname, GLint64 * @data);
        internal static glGetInteger64vFunc glGetInteger64vPtr;
        internal static void loadGetInteger64v()
        {
            try
            {
                glGetInteger64vPtr = (glGetInteger64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInteger64v"), typeof(glGetInteger64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInteger64v'.");
            }
        }
        public static void glGetInteger64v(GLenum @pname, GLint64 * @data) => glGetInteger64vPtr(@pname, @data);

        internal delegate void glGetSyncivFunc(GLsync @sync, GLenum @pname, GLsizei @bufSize, GLsizei * @length, GLint * @values);
        internal static glGetSyncivFunc glGetSyncivPtr;
        internal static void loadGetSynciv()
        {
            try
            {
                glGetSyncivPtr = (glGetSyncivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSynciv"), typeof(glGetSyncivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSynciv'.");
            }
        }
        public static void glGetSynciv(GLsync @sync, GLenum @pname, GLsizei @bufSize, GLsizei * @length, GLint * @values) => glGetSyncivPtr(@sync, @pname, @bufSize, @length, @values);

        internal delegate void glGetInteger64i_vFunc(GLenum @target, GLuint @index, GLint64 * @data);
        internal static glGetInteger64i_vFunc glGetInteger64i_vPtr;
        internal static void loadGetInteger64i_v()
        {
            try
            {
                glGetInteger64i_vPtr = (glGetInteger64i_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInteger64i_v"), typeof(glGetInteger64i_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInteger64i_v'.");
            }
        }
        public static void glGetInteger64i_v(GLenum @target, GLuint @index, GLint64 * @data) => glGetInteger64i_vPtr(@target, @index, @data);

        internal delegate void glGetBufferParameteri64vFunc(GLenum @target, GLenum @pname, GLint64 * @params);
        internal static glGetBufferParameteri64vFunc glGetBufferParameteri64vPtr;
        internal static void loadGetBufferParameteri64v()
        {
            try
            {
                glGetBufferParameteri64vPtr = (glGetBufferParameteri64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferParameteri64v"), typeof(glGetBufferParameteri64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferParameteri64v'.");
            }
        }
        public static void glGetBufferParameteri64v(GLenum @target, GLenum @pname, GLint64 * @params) => glGetBufferParameteri64vPtr(@target, @pname, @params);

        internal delegate void glFramebufferTextureFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level);
        internal static glFramebufferTextureFunc glFramebufferTexturePtr;
        internal static void loadFramebufferTexture()
        {
            try
            {
                glFramebufferTexturePtr = (glFramebufferTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture"), typeof(glFramebufferTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture'.");
            }
        }
        public static void glFramebufferTexture(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level) => glFramebufferTexturePtr(@target, @attachment, @texture, @level);

        internal delegate void glTexImage2DMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations);
        internal static glTexImage2DMultisampleFunc glTexImage2DMultisamplePtr;
        internal static void loadTexImage2DMultisample()
        {
            try
            {
                glTexImage2DMultisamplePtr = (glTexImage2DMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage2DMultisample"), typeof(glTexImage2DMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage2DMultisample'.");
            }
        }
        public static void glTexImage2DMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations) => glTexImage2DMultisamplePtr(@target, @samples, @internalformat, @width, @height, @fixedsamplelocations);

        internal delegate void glTexImage3DMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations);
        internal static glTexImage3DMultisampleFunc glTexImage3DMultisamplePtr;
        internal static void loadTexImage3DMultisample()
        {
            try
            {
                glTexImage3DMultisamplePtr = (glTexImage3DMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage3DMultisample"), typeof(glTexImage3DMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage3DMultisample'.");
            }
        }
        public static void glTexImage3DMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations) => glTexImage3DMultisamplePtr(@target, @samples, @internalformat, @width, @height, @depth, @fixedsamplelocations);

        internal delegate void glGetMultisamplefvFunc(GLenum @pname, GLuint @index, GLfloat * @val);
        internal static glGetMultisamplefvFunc glGetMultisamplefvPtr;
        internal static void loadGetMultisamplefv()
        {
            try
            {
                glGetMultisamplefvPtr = (glGetMultisamplefvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultisamplefv"), typeof(glGetMultisamplefvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultisamplefv'.");
            }
        }
        public static void glGetMultisamplefv(GLenum @pname, GLuint @index, GLfloat * @val) => glGetMultisamplefvPtr(@pname, @index, @val);

        internal delegate void glSampleMaskiFunc(GLuint @maskNumber, GLbitfield @mask);
        internal static glSampleMaskiFunc glSampleMaskiPtr;
        internal static void loadSampleMaski()
        {
            try
            {
                glSampleMaskiPtr = (glSampleMaskiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleMaski"), typeof(glSampleMaskiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleMaski'.");
            }
        }
        public static void glSampleMaski(GLuint @maskNumber, GLbitfield @mask) => glSampleMaskiPtr(@maskNumber, @mask);
        #endregion
    }
}
