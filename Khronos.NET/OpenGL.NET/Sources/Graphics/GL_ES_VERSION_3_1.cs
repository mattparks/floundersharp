using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GLES31
    {
        #region Interop
        static GLES31()
        {
            Console.WriteLine("Initalising GLES31 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDispatchCompute();
            loadDispatchComputeIndirect();
            loadDrawArraysIndirect();
            loadDrawElementsIndirect();
            loadFramebufferParameteri();
            loadGetFramebufferParameteriv();
            loadGetProgramInterfaceiv();
            loadGetProgramResourceIndex();
            loadGetProgramResourceName();
            loadGetProgramResourceiv();
            loadGetProgramResourceLocation();
            loadUseProgramStages();
            loadActiveShaderProgram();
            loadCreateShaderProgramv();
            loadBindProgramPipeline();
            loadDeleteProgramPipelines();
            loadGenProgramPipelines();
            loadIsProgramPipeline();
            loadGetProgramPipelineiv();
            loadProgramUniform1i();
            loadProgramUniform2i();
            loadProgramUniform3i();
            loadProgramUniform4i();
            loadProgramUniform1ui();
            loadProgramUniform2ui();
            loadProgramUniform3ui();
            loadProgramUniform4ui();
            loadProgramUniform1f();
            loadProgramUniform2f();
            loadProgramUniform3f();
            loadProgramUniform4f();
            loadProgramUniform1iv();
            loadProgramUniform2iv();
            loadProgramUniform3iv();
            loadProgramUniform4iv();
            loadProgramUniform1uiv();
            loadProgramUniform2uiv();
            loadProgramUniform3uiv();
            loadProgramUniform4uiv();
            loadProgramUniform1fv();
            loadProgramUniform2fv();
            loadProgramUniform3fv();
            loadProgramUniform4fv();
            loadProgramUniformMatrix2fv();
            loadProgramUniformMatrix3fv();
            loadProgramUniformMatrix4fv();
            loadProgramUniformMatrix2x3fv();
            loadProgramUniformMatrix3x2fv();
            loadProgramUniformMatrix2x4fv();
            loadProgramUniformMatrix4x2fv();
            loadProgramUniformMatrix3x4fv();
            loadProgramUniformMatrix4x3fv();
            loadValidateProgramPipeline();
            loadGetProgramPipelineInfoLog();
            loadBindImageTexture();
            loadGetBooleani_v();
            loadMemoryBarrier();
            loadMemoryBarrierByRegion();
            loadTexStorage2DMultisample();
            loadGetMultisamplefv();
            loadSampleMaski();
            loadGetTexLevelParameteriv();
            loadGetTexLevelParameterfv();
            loadBindVertexBuffer();
            loadVertexAttribFormat();
            loadVertexAttribIFormat();
            loadVertexAttribBinding();
            loadVertexBindingDivisor();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPUTE_SHADER = 0x91B9;
        public static UInt32 GL_MAX_COMPUTE_UNIFORM_BLOCKS = 0x91BB;
        public static UInt32 GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS = 0x91BC;
        public static UInt32 GL_MAX_COMPUTE_IMAGE_UNIFORMS = 0x91BD;
        public static UInt32 GL_MAX_COMPUTE_SHARED_MEMORY_SIZE = 0x8262;
        public static UInt32 GL_MAX_COMPUTE_UNIFORM_COMPONENTS = 0x8263;
        public static UInt32 GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS = 0x8264;
        public static UInt32 GL_MAX_COMPUTE_ATOMIC_COUNTERS = 0x8265;
        public static UInt32 GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS = 0x8266;
        public static UInt32 GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS = 0x90EB;
        public static UInt32 GL_MAX_COMPUTE_WORK_GROUP_COUNT = 0x91BE;
        public static UInt32 GL_MAX_COMPUTE_WORK_GROUP_SIZE = 0x91BF;
        public static UInt32 GL_COMPUTE_WORK_GROUP_SIZE = 0x8267;
        public static UInt32 GL_DISPATCH_INDIRECT_BUFFER = 0x90EE;
        public static UInt32 GL_DISPATCH_INDIRECT_BUFFER_BINDING = 0x90EF;
        public static UInt32 GL_COMPUTE_SHADER_BIT = 0x00000020;
        public static UInt32 GL_DRAW_INDIRECT_BUFFER = 0x8F3F;
        public static UInt32 GL_DRAW_INDIRECT_BUFFER_BINDING = 0x8F43;
        public static UInt32 GL_MAX_UNIFORM_LOCATIONS = 0x826E;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_WIDTH = 0x9310;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_HEIGHT = 0x9311;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_SAMPLES = 0x9313;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS = 0x9314;
        public static UInt32 GL_MAX_FRAMEBUFFER_WIDTH = 0x9315;
        public static UInt32 GL_MAX_FRAMEBUFFER_HEIGHT = 0x9316;
        public static UInt32 GL_MAX_FRAMEBUFFER_SAMPLES = 0x9318;
        public static UInt32 GL_UNIFORM = 0x92E1;
        public static UInt32 GL_UNIFORM_BLOCK = 0x92E2;
        public static UInt32 GL_PROGRAM_INPUT = 0x92E3;
        public static UInt32 GL_PROGRAM_OUTPUT = 0x92E4;
        public static UInt32 GL_BUFFER_VARIABLE = 0x92E5;
        public static UInt32 GL_SHADER_STORAGE_BLOCK = 0x92E6;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER = 0x92C0;
        public static UInt32 GL_TRANSFORM_FEEDBACK_VARYING = 0x92F4;
        public static UInt32 GL_ACTIVE_RESOURCES = 0x92F5;
        public static UInt32 GL_MAX_NAME_LENGTH = 0x92F6;
        public static UInt32 GL_MAX_NUM_ACTIVE_VARIABLES = 0x92F7;
        public static UInt32 GL_NAME_LENGTH = 0x92F9;
        public static UInt32 GL_TYPE = 0x92FA;
        public static UInt32 GL_ARRAY_SIZE = 0x92FB;
        public static UInt32 GL_OFFSET = 0x92FC;
        public static UInt32 GL_BLOCK_INDEX = 0x92FD;
        public static UInt32 GL_ARRAY_STRIDE = 0x92FE;
        public static UInt32 GL_MATRIX_STRIDE = 0x92FF;
        public static UInt32 GL_IS_ROW_MAJOR = 0x9300;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_INDEX = 0x9301;
        public static UInt32 GL_BUFFER_BINDING = 0x9302;
        public static UInt32 GL_BUFFER_DATA_SIZE = 0x9303;
        public static UInt32 GL_NUM_ACTIVE_VARIABLES = 0x9304;
        public static UInt32 GL_ACTIVE_VARIABLES = 0x9305;
        public static UInt32 GL_REFERENCED_BY_VERTEX_SHADER = 0x9306;
        public static UInt32 GL_REFERENCED_BY_FRAGMENT_SHADER = 0x930A;
        public static UInt32 GL_REFERENCED_BY_COMPUTE_SHADER = 0x930B;
        public static UInt32 GL_TOP_LEVEL_ARRAY_SIZE = 0x930C;
        public static UInt32 GL_TOP_LEVEL_ARRAY_STRIDE = 0x930D;
        public static UInt32 GL_LOCATION = 0x930E;
        public static UInt32 GL_VERTEX_SHADER_BIT = 0x00000001;
        public static UInt32 GL_FRAGMENT_SHADER_BIT = 0x00000002;
        public static UInt32 GL_ALL_SHADER_BITS = 0xFFFFFFFF;
        public static UInt32 GL_PROGRAM_SEPARABLE = 0x8258;
        public static UInt32 GL_ACTIVE_PROGRAM = 0x8259;
        public static UInt32 GL_PROGRAM_PIPELINE_BINDING = 0x825A;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_BINDING = 0x92C1;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_START = 0x92C2;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_SIZE = 0x92C3;
        public static UInt32 GL_MAX_VERTEX_ATOMIC_COUNTER_BUFFERS = 0x92CC;
        public static UInt32 GL_MAX_FRAGMENT_ATOMIC_COUNTER_BUFFERS = 0x92D0;
        public static UInt32 GL_MAX_COMBINED_ATOMIC_COUNTER_BUFFERS = 0x92D1;
        public static UInt32 GL_MAX_VERTEX_ATOMIC_COUNTERS = 0x92D2;
        public static UInt32 GL_MAX_FRAGMENT_ATOMIC_COUNTERS = 0x92D6;
        public static UInt32 GL_MAX_COMBINED_ATOMIC_COUNTERS = 0x92D7;
        public static UInt32 GL_MAX_ATOMIC_COUNTER_BUFFER_SIZE = 0x92D8;
        public static UInt32 GL_MAX_ATOMIC_COUNTER_BUFFER_BINDINGS = 0x92DC;
        public static UInt32 GL_ACTIVE_ATOMIC_COUNTER_BUFFERS = 0x92D9;
        public static UInt32 GL_UNSIGNED_INT_ATOMIC_COUNTER = 0x92DB;
        public static UInt32 GL_MAX_IMAGE_UNITS = 0x8F38;
        public static UInt32 GL_MAX_VERTEX_IMAGE_UNIFORMS = 0x90CA;
        public static UInt32 GL_MAX_FRAGMENT_IMAGE_UNIFORMS = 0x90CE;
        public static UInt32 GL_MAX_COMBINED_IMAGE_UNIFORMS = 0x90CF;
        public static UInt32 GL_IMAGE_BINDING_NAME = 0x8F3A;
        public static UInt32 GL_IMAGE_BINDING_LEVEL = 0x8F3B;
        public static UInt32 GL_IMAGE_BINDING_LAYERED = 0x8F3C;
        public static UInt32 GL_IMAGE_BINDING_LAYER = 0x8F3D;
        public static UInt32 GL_IMAGE_BINDING_ACCESS = 0x8F3E;
        public static UInt32 GL_IMAGE_BINDING_FORMAT = 0x906E;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT = 0x00000001;
        public static UInt32 GL_ELEMENT_ARRAY_BARRIER_BIT = 0x00000002;
        public static UInt32 GL_UNIFORM_BARRIER_BIT = 0x00000004;
        public static UInt32 GL_TEXTURE_FETCH_BARRIER_BIT = 0x00000008;
        public static UInt32 GL_SHADER_IMAGE_ACCESS_BARRIER_BIT = 0x00000020;
        public static UInt32 GL_COMMAND_BARRIER_BIT = 0x00000040;
        public static UInt32 GL_PIXEL_BUFFER_BARRIER_BIT = 0x00000080;
        public static UInt32 GL_TEXTURE_UPDATE_BARRIER_BIT = 0x00000100;
        public static UInt32 GL_BUFFER_UPDATE_BARRIER_BIT = 0x00000200;
        public static UInt32 GL_FRAMEBUFFER_BARRIER_BIT = 0x00000400;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BARRIER_BIT = 0x00000800;
        public static UInt32 GL_ATOMIC_COUNTER_BARRIER_BIT = 0x00001000;
        public static UInt32 GL_ALL_BARRIER_BITS = 0xFFFFFFFF;
        public static UInt32 GL_IMAGE_2D = 0x904D;
        public static UInt32 GL_IMAGE_3D = 0x904E;
        public static UInt32 GL_IMAGE_CUBE = 0x9050;
        public static UInt32 GL_IMAGE_2D_ARRAY = 0x9053;
        public static UInt32 GL_INT_IMAGE_2D = 0x9058;
        public static UInt32 GL_INT_IMAGE_3D = 0x9059;
        public static UInt32 GL_INT_IMAGE_CUBE = 0x905B;
        public static UInt32 GL_INT_IMAGE_2D_ARRAY = 0x905E;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D = 0x9063;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_3D = 0x9064;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_CUBE = 0x9066;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_ARRAY = 0x9069;
        public static UInt32 GL_IMAGE_FORMAT_COMPATIBILITY_TYPE = 0x90C7;
        public static UInt32 GL_IMAGE_FORMAT_COMPATIBILITY_BY_SIZE = 0x90C8;
        public static UInt32 GL_IMAGE_FORMAT_COMPATIBILITY_BY_CLASS = 0x90C9;
        public static UInt32 GL_READ_ONLY = 0x88B8;
        public static UInt32 GL_WRITE_ONLY = 0x88B9;
        public static UInt32 GL_READ_WRITE = 0x88BA;
        public static UInt32 GL_SHADER_STORAGE_BUFFER = 0x90D2;
        public static UInt32 GL_SHADER_STORAGE_BUFFER_BINDING = 0x90D3;
        public static UInt32 GL_SHADER_STORAGE_BUFFER_START = 0x90D4;
        public static UInt32 GL_SHADER_STORAGE_BUFFER_SIZE = 0x90D5;
        public static UInt32 GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS = 0x90D6;
        public static UInt32 GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS = 0x90DA;
        public static UInt32 GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS = 0x90DB;
        public static UInt32 GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS = 0x90DC;
        public static UInt32 GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS = 0x90DD;
        public static UInt32 GL_MAX_SHADER_STORAGE_BLOCK_SIZE = 0x90DE;
        public static UInt32 GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT = 0x90DF;
        public static UInt32 GL_SHADER_STORAGE_BARRIER_BIT = 0x00002000;
        public static UInt32 GL_MAX_COMBINED_SHADER_OUTPUT_RESOURCES = 0x8F39;
        public static UInt32 GL_DEPTH_STENCIL_TEXTURE_MODE = 0x90EA;
        public static UInt32 GL_STENCIL_INDEX = 0x1901;
        public static UInt32 GL_MIN_PROGRAM_TEXTURE_GATHER_OFFSET = 0x8E5E;
        public static UInt32 GL_MAX_PROGRAM_TEXTURE_GATHER_OFFSET = 0x8E5F;
        public static UInt32 GL_SAMPLE_POSITION = 0x8E50;
        public static UInt32 GL_SAMPLE_MASK = 0x8E51;
        public static UInt32 GL_SAMPLE_MASK_VALUE = 0x8E52;
        public static UInt32 GL_TEXTURE_2D_MULTISAMPLE = 0x9100;
        public static UInt32 GL_MAX_SAMPLE_MASK_WORDS = 0x8E59;
        public static UInt32 GL_MAX_COLOR_TEXTURE_SAMPLES = 0x910E;
        public static UInt32 GL_MAX_DEPTH_TEXTURE_SAMPLES = 0x910F;
        public static UInt32 GL_MAX_INTEGER_SAMPLES = 0x9110;
        public static UInt32 GL_TEXTURE_BINDING_2D_MULTISAMPLE = 0x9104;
        public static UInt32 GL_TEXTURE_SAMPLES = 0x9106;
        public static UInt32 GL_TEXTURE_FIXED_SAMPLE_LOCATIONS = 0x9107;
        public static UInt32 GL_TEXTURE_WIDTH = 0x1000;
        public static UInt32 GL_TEXTURE_HEIGHT = 0x1001;
        public static UInt32 GL_TEXTURE_DEPTH = 0x8071;
        public static UInt32 GL_TEXTURE_INTERNAL_FORMAT = 0x1003;
        public static UInt32 GL_TEXTURE_RED_SIZE = 0x805C;
        public static UInt32 GL_TEXTURE_GREEN_SIZE = 0x805D;
        public static UInt32 GL_TEXTURE_BLUE_SIZE = 0x805E;
        public static UInt32 GL_TEXTURE_ALPHA_SIZE = 0x805F;
        public static UInt32 GL_TEXTURE_DEPTH_SIZE = 0x884A;
        public static UInt32 GL_TEXTURE_STENCIL_SIZE = 0x88F1;
        public static UInt32 GL_TEXTURE_SHARED_SIZE = 0x8C3F;
        public static UInt32 GL_TEXTURE_RED_TYPE = 0x8C10;
        public static UInt32 GL_TEXTURE_GREEN_TYPE = 0x8C11;
        public static UInt32 GL_TEXTURE_BLUE_TYPE = 0x8C12;
        public static UInt32 GL_TEXTURE_ALPHA_TYPE = 0x8C13;
        public static UInt32 GL_TEXTURE_DEPTH_TYPE = 0x8C16;
        public static UInt32 GL_TEXTURE_COMPRESSED = 0x86A1;
        public static UInt32 GL_SAMPLER_2D_MULTISAMPLE = 0x9108;
        public static UInt32 GL_INT_SAMPLER_2D_MULTISAMPLE = 0x9109;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE = 0x910A;
        public static UInt32 GL_VERTEX_ATTRIB_BINDING = 0x82D4;
        public static UInt32 GL_VERTEX_ATTRIB_RELATIVE_OFFSET = 0x82D5;
        public static UInt32 GL_VERTEX_BINDING_DIVISOR = 0x82D6;
        public static UInt32 GL_VERTEX_BINDING_OFFSET = 0x82D7;
        public static UInt32 GL_VERTEX_BINDING_STRIDE = 0x82D8;
        public static UInt32 GL_VERTEX_BINDING_BUFFER = 0x8F4F;
        public static UInt32 GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET = 0x82D9;
        public static UInt32 GL_MAX_VERTEX_ATTRIB_BINDINGS = 0x82DA;
        public static UInt32 GL_MAX_VERTEX_ATTRIB_STRIDE = 0x82E5;
        #endregion

        #region Commands
        internal delegate void glDispatchComputeFunc(GLuint @num_groups_x, GLuint @num_groups_y, GLuint @num_groups_z);
        internal static glDispatchComputeFunc glDispatchComputePtr;
        internal static void loadDispatchCompute()
        {
            try
            {
                glDispatchComputePtr = (glDispatchComputeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDispatchCompute"), typeof(glDispatchComputeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDispatchCompute'.");
            }
        }
        public static void glDispatchCompute(GLuint @num_groups_x, GLuint @num_groups_y, GLuint @num_groups_z) => glDispatchComputePtr(@num_groups_x, @num_groups_y, @num_groups_z);

        internal delegate void glDispatchComputeIndirectFunc(GLintptr @indirect);
        internal static glDispatchComputeIndirectFunc glDispatchComputeIndirectPtr;
        internal static void loadDispatchComputeIndirect()
        {
            try
            {
                glDispatchComputeIndirectPtr = (glDispatchComputeIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDispatchComputeIndirect"), typeof(glDispatchComputeIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDispatchComputeIndirect'.");
            }
        }
        public static void glDispatchComputeIndirect(GLintptr @indirect) => glDispatchComputeIndirectPtr(@indirect);

        internal delegate void glDrawArraysIndirectFunc(GLenum @mode, const void * @indirect);
        internal static glDrawArraysIndirectFunc glDrawArraysIndirectPtr;
        internal static void loadDrawArraysIndirect()
        {
            try
            {
                glDrawArraysIndirectPtr = (glDrawArraysIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysIndirect"), typeof(glDrawArraysIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysIndirect'.");
            }
        }
        public static void glDrawArraysIndirect(GLenum @mode, const void * @indirect) => glDrawArraysIndirectPtr(@mode, @indirect);

        internal delegate void glDrawElementsIndirectFunc(GLenum @mode, GLenum @type, const void * @indirect);
        internal static glDrawElementsIndirectFunc glDrawElementsIndirectPtr;
        internal static void loadDrawElementsIndirect()
        {
            try
            {
                glDrawElementsIndirectPtr = (glDrawElementsIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsIndirect"), typeof(glDrawElementsIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsIndirect'.");
            }
        }
        public static void glDrawElementsIndirect(GLenum @mode, GLenum @type, const void * @indirect) => glDrawElementsIndirectPtr(@mode, @type, @indirect);

        internal delegate void glFramebufferParameteriFunc(GLenum @target, GLenum @pname, GLint @param);
        internal static glFramebufferParameteriFunc glFramebufferParameteriPtr;
        internal static void loadFramebufferParameteri()
        {
            try
            {
                glFramebufferParameteriPtr = (glFramebufferParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferParameteri"), typeof(glFramebufferParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferParameteri'.");
            }
        }
        public static void glFramebufferParameteri(GLenum @target, GLenum @pname, GLint @param) => glFramebufferParameteriPtr(@target, @pname, @param);

        internal delegate void glGetFramebufferParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetFramebufferParameterivFunc glGetFramebufferParameterivPtr;
        internal static void loadGetFramebufferParameteriv()
        {
            try
            {
                glGetFramebufferParameterivPtr = (glGetFramebufferParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFramebufferParameteriv"), typeof(glGetFramebufferParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFramebufferParameteriv'.");
            }
        }
        public static void glGetFramebufferParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetFramebufferParameterivPtr(@target, @pname, @params);

        internal delegate void glGetProgramInterfaceivFunc(GLuint @program, GLenum @programInterface, GLenum @pname, GLint * @params);
        internal static glGetProgramInterfaceivFunc glGetProgramInterfaceivPtr;
        internal static void loadGetProgramInterfaceiv()
        {
            try
            {
                glGetProgramInterfaceivPtr = (glGetProgramInterfaceivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramInterfaceiv"), typeof(glGetProgramInterfaceivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramInterfaceiv'.");
            }
        }
        public static void glGetProgramInterfaceiv(GLuint @program, GLenum @programInterface, GLenum @pname, GLint * @params) => glGetProgramInterfaceivPtr(@program, @programInterface, @pname, @params);

        internal delegate GLuint glGetProgramResourceIndexFunc(GLuint @program, GLenum @programInterface, const GLchar * @name);
        internal static glGetProgramResourceIndexFunc glGetProgramResourceIndexPtr;
        internal static void loadGetProgramResourceIndex()
        {
            try
            {
                glGetProgramResourceIndexPtr = (glGetProgramResourceIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceIndex"), typeof(glGetProgramResourceIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceIndex'.");
            }
        }
        public static GLuint glGetProgramResourceIndex(GLuint @program, GLenum @programInterface, const GLchar * @name) => glGetProgramResourceIndexPtr(@program, @programInterface, @name);

        internal delegate void glGetProgramResourceNameFunc(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLchar * @name);
        internal static glGetProgramResourceNameFunc glGetProgramResourceNamePtr;
        internal static void loadGetProgramResourceName()
        {
            try
            {
                glGetProgramResourceNamePtr = (glGetProgramResourceNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceName"), typeof(glGetProgramResourceNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceName'.");
            }
        }
        public static void glGetProgramResourceName(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLchar * @name) => glGetProgramResourceNamePtr(@program, @programInterface, @index, @bufSize, @length, @name);

        internal delegate void glGetProgramResourceivFunc(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @propCount, const GLenum * @props, GLsizei @bufSize, GLsizei * @length, GLint * @params);
        internal static glGetProgramResourceivFunc glGetProgramResourceivPtr;
        internal static void loadGetProgramResourceiv()
        {
            try
            {
                glGetProgramResourceivPtr = (glGetProgramResourceivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceiv"), typeof(glGetProgramResourceivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceiv'.");
            }
        }
        public static void glGetProgramResourceiv(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @propCount, const GLenum * @props, GLsizei @bufSize, GLsizei * @length, GLint * @params) => glGetProgramResourceivPtr(@program, @programInterface, @index, @propCount, @props, @bufSize, @length, @params);

        internal delegate GLint glGetProgramResourceLocationFunc(GLuint @program, GLenum @programInterface, const GLchar * @name);
        internal static glGetProgramResourceLocationFunc glGetProgramResourceLocationPtr;
        internal static void loadGetProgramResourceLocation()
        {
            try
            {
                glGetProgramResourceLocationPtr = (glGetProgramResourceLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceLocation"), typeof(glGetProgramResourceLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceLocation'.");
            }
        }
        public static GLint glGetProgramResourceLocation(GLuint @program, GLenum @programInterface, const GLchar * @name) => glGetProgramResourceLocationPtr(@program, @programInterface, @name);

        internal delegate void glUseProgramStagesFunc(GLuint @pipeline, GLbitfield @stages, GLuint @program);
        internal static glUseProgramStagesFunc glUseProgramStagesPtr;
        internal static void loadUseProgramStages()
        {
            try
            {
                glUseProgramStagesPtr = (glUseProgramStagesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUseProgramStages"), typeof(glUseProgramStagesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUseProgramStages'.");
            }
        }
        public static void glUseProgramStages(GLuint @pipeline, GLbitfield @stages, GLuint @program) => glUseProgramStagesPtr(@pipeline, @stages, @program);

        internal delegate void glActiveShaderProgramFunc(GLuint @pipeline, GLuint @program);
        internal static glActiveShaderProgramFunc glActiveShaderProgramPtr;
        internal static void loadActiveShaderProgram()
        {
            try
            {
                glActiveShaderProgramPtr = (glActiveShaderProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glActiveShaderProgram"), typeof(glActiveShaderProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glActiveShaderProgram'.");
            }
        }
        public static void glActiveShaderProgram(GLuint @pipeline, GLuint @program) => glActiveShaderProgramPtr(@pipeline, @program);

        internal delegate GLuint glCreateShaderProgramvFunc(GLenum @type, GLsizei @count, const GLchar *const* @strings);
        internal static glCreateShaderProgramvFunc glCreateShaderProgramvPtr;
        internal static void loadCreateShaderProgramv()
        {
            try
            {
                glCreateShaderProgramvPtr = (glCreateShaderProgramvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateShaderProgramv"), typeof(glCreateShaderProgramvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateShaderProgramv'.");
            }
        }
        public static GLuint glCreateShaderProgramv(GLenum @type, GLsizei @count, const GLchar *const* @strings) => glCreateShaderProgramvPtr(@type, @count, @strings);

        internal delegate void glBindProgramPipelineFunc(GLuint @pipeline);
        internal static glBindProgramPipelineFunc glBindProgramPipelinePtr;
        internal static void loadBindProgramPipeline()
        {
            try
            {
                glBindProgramPipelinePtr = (glBindProgramPipelineFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindProgramPipeline"), typeof(glBindProgramPipelineFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindProgramPipeline'.");
            }
        }
        public static void glBindProgramPipeline(GLuint @pipeline) => glBindProgramPipelinePtr(@pipeline);

        internal delegate void glDeleteProgramPipelinesFunc(GLsizei @n, const GLuint * @pipelines);
        internal static glDeleteProgramPipelinesFunc glDeleteProgramPipelinesPtr;
        internal static void loadDeleteProgramPipelines()
        {
            try
            {
                glDeleteProgramPipelinesPtr = (glDeleteProgramPipelinesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteProgramPipelines"), typeof(glDeleteProgramPipelinesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteProgramPipelines'.");
            }
        }
        public static void glDeleteProgramPipelines(GLsizei @n, const GLuint * @pipelines) => glDeleteProgramPipelinesPtr(@n, @pipelines);

        internal delegate void glGenProgramPipelinesFunc(GLsizei @n, GLuint * @pipelines);
        internal static glGenProgramPipelinesFunc glGenProgramPipelinesPtr;
        internal static void loadGenProgramPipelines()
        {
            try
            {
                glGenProgramPipelinesPtr = (glGenProgramPipelinesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenProgramPipelines"), typeof(glGenProgramPipelinesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenProgramPipelines'.");
            }
        }
        public static void glGenProgramPipelines(GLsizei @n, GLuint * @pipelines) => glGenProgramPipelinesPtr(@n, @pipelines);

        internal delegate GLboolean glIsProgramPipelineFunc(GLuint @pipeline);
        internal static glIsProgramPipelineFunc glIsProgramPipelinePtr;
        internal static void loadIsProgramPipeline()
        {
            try
            {
                glIsProgramPipelinePtr = (glIsProgramPipelineFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsProgramPipeline"), typeof(glIsProgramPipelineFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsProgramPipeline'.");
            }
        }
        public static GLboolean glIsProgramPipeline(GLuint @pipeline) => glIsProgramPipelinePtr(@pipeline);

        internal delegate void glGetProgramPipelineivFunc(GLuint @pipeline, GLenum @pname, GLint * @params);
        internal static glGetProgramPipelineivFunc glGetProgramPipelineivPtr;
        internal static void loadGetProgramPipelineiv()
        {
            try
            {
                glGetProgramPipelineivPtr = (glGetProgramPipelineivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramPipelineiv"), typeof(glGetProgramPipelineivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramPipelineiv'.");
            }
        }
        public static void glGetProgramPipelineiv(GLuint @pipeline, GLenum @pname, GLint * @params) => glGetProgramPipelineivPtr(@pipeline, @pname, @params);

        internal delegate void glProgramUniform1iFunc(GLuint @program, GLint @location, GLint @v0);
        internal static glProgramUniform1iFunc glProgramUniform1iPtr;
        internal static void loadProgramUniform1i()
        {
            try
            {
                glProgramUniform1iPtr = (glProgramUniform1iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1i"), typeof(glProgramUniform1iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1i'.");
            }
        }
        public static void glProgramUniform1i(GLuint @program, GLint @location, GLint @v0) => glProgramUniform1iPtr(@program, @location, @v0);

        internal delegate void glProgramUniform2iFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1);
        internal static glProgramUniform2iFunc glProgramUniform2iPtr;
        internal static void loadProgramUniform2i()
        {
            try
            {
                glProgramUniform2iPtr = (glProgramUniform2iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2i"), typeof(glProgramUniform2iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2i'.");
            }
        }
        public static void glProgramUniform2i(GLuint @program, GLint @location, GLint @v0, GLint @v1) => glProgramUniform2iPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform3iFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2);
        internal static glProgramUniform3iFunc glProgramUniform3iPtr;
        internal static void loadProgramUniform3i()
        {
            try
            {
                glProgramUniform3iPtr = (glProgramUniform3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3i"), typeof(glProgramUniform3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3i'.");
            }
        }
        public static void glProgramUniform3i(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2) => glProgramUniform3iPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform4iFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3);
        internal static glProgramUniform4iFunc glProgramUniform4iPtr;
        internal static void loadProgramUniform4i()
        {
            try
            {
                glProgramUniform4iPtr = (glProgramUniform4iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4i"), typeof(glProgramUniform4iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4i'.");
            }
        }
        public static void glProgramUniform4i(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3) => glProgramUniform4iPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform1uiFunc(GLuint @program, GLint @location, GLuint @v0);
        internal static glProgramUniform1uiFunc glProgramUniform1uiPtr;
        internal static void loadProgramUniform1ui()
        {
            try
            {
                glProgramUniform1uiPtr = (glProgramUniform1uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1ui"), typeof(glProgramUniform1uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1ui'.");
            }
        }
        public static void glProgramUniform1ui(GLuint @program, GLint @location, GLuint @v0) => glProgramUniform1uiPtr(@program, @location, @v0);

        internal delegate void glProgramUniform2uiFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1);
        internal static glProgramUniform2uiFunc glProgramUniform2uiPtr;
        internal static void loadProgramUniform2ui()
        {
            try
            {
                glProgramUniform2uiPtr = (glProgramUniform2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2ui"), typeof(glProgramUniform2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2ui'.");
            }
        }
        public static void glProgramUniform2ui(GLuint @program, GLint @location, GLuint @v0, GLuint @v1) => glProgramUniform2uiPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform3uiFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2);
        internal static glProgramUniform3uiFunc glProgramUniform3uiPtr;
        internal static void loadProgramUniform3ui()
        {
            try
            {
                glProgramUniform3uiPtr = (glProgramUniform3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3ui"), typeof(glProgramUniform3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3ui'.");
            }
        }
        public static void glProgramUniform3ui(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2) => glProgramUniform3uiPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform4uiFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3);
        internal static glProgramUniform4uiFunc glProgramUniform4uiPtr;
        internal static void loadProgramUniform4ui()
        {
            try
            {
                glProgramUniform4uiPtr = (glProgramUniform4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4ui"), typeof(glProgramUniform4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4ui'.");
            }
        }
        public static void glProgramUniform4ui(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3) => glProgramUniform4uiPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform1fFunc(GLuint @program, GLint @location, GLfloat @v0);
        internal static glProgramUniform1fFunc glProgramUniform1fPtr;
        internal static void loadProgramUniform1f()
        {
            try
            {
                glProgramUniform1fPtr = (glProgramUniform1fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1f"), typeof(glProgramUniform1fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1f'.");
            }
        }
        public static void glProgramUniform1f(GLuint @program, GLint @location, GLfloat @v0) => glProgramUniform1fPtr(@program, @location, @v0);

        internal delegate void glProgramUniform2fFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1);
        internal static glProgramUniform2fFunc glProgramUniform2fPtr;
        internal static void loadProgramUniform2f()
        {
            try
            {
                glProgramUniform2fPtr = (glProgramUniform2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2f"), typeof(glProgramUniform2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2f'.");
            }
        }
        public static void glProgramUniform2f(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1) => glProgramUniform2fPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform3fFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2);
        internal static glProgramUniform3fFunc glProgramUniform3fPtr;
        internal static void loadProgramUniform3f()
        {
            try
            {
                glProgramUniform3fPtr = (glProgramUniform3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3f"), typeof(glProgramUniform3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3f'.");
            }
        }
        public static void glProgramUniform3f(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2) => glProgramUniform3fPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform4fFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3);
        internal static glProgramUniform4fFunc glProgramUniform4fPtr;
        internal static void loadProgramUniform4f()
        {
            try
            {
                glProgramUniform4fPtr = (glProgramUniform4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4f"), typeof(glProgramUniform4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4f'.");
            }
        }
        public static void glProgramUniform4f(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3) => glProgramUniform4fPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform1ivFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform1ivFunc glProgramUniform1ivPtr;
        internal static void loadProgramUniform1iv()
        {
            try
            {
                glProgramUniform1ivPtr = (glProgramUniform1ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1iv"), typeof(glProgramUniform1ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1iv'.");
            }
        }
        public static void glProgramUniform1iv(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform1ivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2ivFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform2ivFunc glProgramUniform2ivPtr;
        internal static void loadProgramUniform2iv()
        {
            try
            {
                glProgramUniform2ivPtr = (glProgramUniform2ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2iv"), typeof(glProgramUniform2ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2iv'.");
            }
        }
        public static void glProgramUniform2iv(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform2ivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3ivFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform3ivFunc glProgramUniform3ivPtr;
        internal static void loadProgramUniform3iv()
        {
            try
            {
                glProgramUniform3ivPtr = (glProgramUniform3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3iv"), typeof(glProgramUniform3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3iv'.");
            }
        }
        public static void glProgramUniform3iv(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform3ivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4ivFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform4ivFunc glProgramUniform4ivPtr;
        internal static void loadProgramUniform4iv()
        {
            try
            {
                glProgramUniform4ivPtr = (glProgramUniform4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4iv"), typeof(glProgramUniform4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4iv'.");
            }
        }
        public static void glProgramUniform4iv(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform4ivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform1uivFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform1uivFunc glProgramUniform1uivPtr;
        internal static void loadProgramUniform1uiv()
        {
            try
            {
                glProgramUniform1uivPtr = (glProgramUniform1uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1uiv"), typeof(glProgramUniform1uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1uiv'.");
            }
        }
        public static void glProgramUniform1uiv(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform1uivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2uivFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform2uivFunc glProgramUniform2uivPtr;
        internal static void loadProgramUniform2uiv()
        {
            try
            {
                glProgramUniform2uivPtr = (glProgramUniform2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2uiv"), typeof(glProgramUniform2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2uiv'.");
            }
        }
        public static void glProgramUniform2uiv(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform2uivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3uivFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform3uivFunc glProgramUniform3uivPtr;
        internal static void loadProgramUniform3uiv()
        {
            try
            {
                glProgramUniform3uivPtr = (glProgramUniform3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3uiv"), typeof(glProgramUniform3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3uiv'.");
            }
        }
        public static void glProgramUniform3uiv(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform3uivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4uivFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform4uivFunc glProgramUniform4uivPtr;
        internal static void loadProgramUniform4uiv()
        {
            try
            {
                glProgramUniform4uivPtr = (glProgramUniform4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4uiv"), typeof(glProgramUniform4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4uiv'.");
            }
        }
        public static void glProgramUniform4uiv(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform4uivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform1fvFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform1fvFunc glProgramUniform1fvPtr;
        internal static void loadProgramUniform1fv()
        {
            try
            {
                glProgramUniform1fvPtr = (glProgramUniform1fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1fv"), typeof(glProgramUniform1fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1fv'.");
            }
        }
        public static void glProgramUniform1fv(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform1fvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2fvFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform2fvFunc glProgramUniform2fvPtr;
        internal static void loadProgramUniform2fv()
        {
            try
            {
                glProgramUniform2fvPtr = (glProgramUniform2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2fv"), typeof(glProgramUniform2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2fv'.");
            }
        }
        public static void glProgramUniform2fv(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform2fvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3fvFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform3fvFunc glProgramUniform3fvPtr;
        internal static void loadProgramUniform3fv()
        {
            try
            {
                glProgramUniform3fvPtr = (glProgramUniform3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3fv"), typeof(glProgramUniform3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3fv'.");
            }
        }
        public static void glProgramUniform3fv(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform3fvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4fvFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform4fvFunc glProgramUniform4fvPtr;
        internal static void loadProgramUniform4fv()
        {
            try
            {
                glProgramUniform4fvPtr = (glProgramUniform4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4fv"), typeof(glProgramUniform4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4fv'.");
            }
        }
        public static void glProgramUniform4fv(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform4fvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniformMatrix2fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2fvFunc glProgramUniformMatrix2fvPtr;
        internal static void loadProgramUniformMatrix2fv()
        {
            try
            {
                glProgramUniformMatrix2fvPtr = (glProgramUniformMatrix2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2fv"), typeof(glProgramUniformMatrix2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2fv'.");
            }
        }
        public static void glProgramUniformMatrix2fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3fvFunc glProgramUniformMatrix3fvPtr;
        internal static void loadProgramUniformMatrix3fv()
        {
            try
            {
                glProgramUniformMatrix3fvPtr = (glProgramUniformMatrix3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3fv"), typeof(glProgramUniformMatrix3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3fv'.");
            }
        }
        public static void glProgramUniformMatrix3fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4fvFunc glProgramUniformMatrix4fvPtr;
        internal static void loadProgramUniformMatrix4fv()
        {
            try
            {
                glProgramUniformMatrix4fvPtr = (glProgramUniformMatrix4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4fv"), typeof(glProgramUniformMatrix4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4fv'.");
            }
        }
        public static void glProgramUniformMatrix4fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x3fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2x3fvFunc glProgramUniformMatrix2x3fvPtr;
        internal static void loadProgramUniformMatrix2x3fv()
        {
            try
            {
                glProgramUniformMatrix2x3fvPtr = (glProgramUniformMatrix2x3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x3fv"), typeof(glProgramUniformMatrix2x3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x3fv'.");
            }
        }
        public static void glProgramUniformMatrix2x3fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2x3fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x2fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3x2fvFunc glProgramUniformMatrix3x2fvPtr;
        internal static void loadProgramUniformMatrix3x2fv()
        {
            try
            {
                glProgramUniformMatrix3x2fvPtr = (glProgramUniformMatrix3x2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x2fv"), typeof(glProgramUniformMatrix3x2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x2fv'.");
            }
        }
        public static void glProgramUniformMatrix3x2fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3x2fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x4fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2x4fvFunc glProgramUniformMatrix2x4fvPtr;
        internal static void loadProgramUniformMatrix2x4fv()
        {
            try
            {
                glProgramUniformMatrix2x4fvPtr = (glProgramUniformMatrix2x4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x4fv"), typeof(glProgramUniformMatrix2x4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x4fv'.");
            }
        }
        public static void glProgramUniformMatrix2x4fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2x4fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x2fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4x2fvFunc glProgramUniformMatrix4x2fvPtr;
        internal static void loadProgramUniformMatrix4x2fv()
        {
            try
            {
                glProgramUniformMatrix4x2fvPtr = (glProgramUniformMatrix4x2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x2fv"), typeof(glProgramUniformMatrix4x2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x2fv'.");
            }
        }
        public static void glProgramUniformMatrix4x2fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4x2fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x4fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3x4fvFunc glProgramUniformMatrix3x4fvPtr;
        internal static void loadProgramUniformMatrix3x4fv()
        {
            try
            {
                glProgramUniformMatrix3x4fvPtr = (glProgramUniformMatrix3x4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x4fv"), typeof(glProgramUniformMatrix3x4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x4fv'.");
            }
        }
        public static void glProgramUniformMatrix3x4fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3x4fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x3fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4x3fvFunc glProgramUniformMatrix4x3fvPtr;
        internal static void loadProgramUniformMatrix4x3fv()
        {
            try
            {
                glProgramUniformMatrix4x3fvPtr = (glProgramUniformMatrix4x3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x3fv"), typeof(glProgramUniformMatrix4x3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x3fv'.");
            }
        }
        public static void glProgramUniformMatrix4x3fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4x3fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glValidateProgramPipelineFunc(GLuint @pipeline);
        internal static glValidateProgramPipelineFunc glValidateProgramPipelinePtr;
        internal static void loadValidateProgramPipeline()
        {
            try
            {
                glValidateProgramPipelinePtr = (glValidateProgramPipelineFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glValidateProgramPipeline"), typeof(glValidateProgramPipelineFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glValidateProgramPipeline'.");
            }
        }
        public static void glValidateProgramPipeline(GLuint @pipeline) => glValidateProgramPipelinePtr(@pipeline);

        internal delegate void glGetProgramPipelineInfoLogFunc(GLuint @pipeline, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog);
        internal static glGetProgramPipelineInfoLogFunc glGetProgramPipelineInfoLogPtr;
        internal static void loadGetProgramPipelineInfoLog()
        {
            try
            {
                glGetProgramPipelineInfoLogPtr = (glGetProgramPipelineInfoLogFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramPipelineInfoLog"), typeof(glGetProgramPipelineInfoLogFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramPipelineInfoLog'.");
            }
        }
        public static void glGetProgramPipelineInfoLog(GLuint @pipeline, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog) => glGetProgramPipelineInfoLogPtr(@pipeline, @bufSize, @length, @infoLog);

        internal delegate void glBindImageTextureFunc(GLuint @unit, GLuint @texture, GLint @level, GLboolean @layered, GLint @layer, GLenum @access, GLenum @format);
        internal static glBindImageTextureFunc glBindImageTexturePtr;
        internal static void loadBindImageTexture()
        {
            try
            {
                glBindImageTexturePtr = (glBindImageTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindImageTexture"), typeof(glBindImageTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindImageTexture'.");
            }
        }
        public static void glBindImageTexture(GLuint @unit, GLuint @texture, GLint @level, GLboolean @layered, GLint @layer, GLenum @access, GLenum @format) => glBindImageTexturePtr(@unit, @texture, @level, @layered, @layer, @access, @format);

        internal delegate void glGetBooleani_vFunc(GLenum @target, GLuint @index, GLboolean * @data);
        internal static glGetBooleani_vFunc glGetBooleani_vPtr;
        internal static void loadGetBooleani_v()
        {
            try
            {
                glGetBooleani_vPtr = (glGetBooleani_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBooleani_v"), typeof(glGetBooleani_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBooleani_v'.");
            }
        }
        public static void glGetBooleani_v(GLenum @target, GLuint @index, GLboolean * @data) => glGetBooleani_vPtr(@target, @index, @data);

        internal delegate void glMemoryBarrierFunc(GLbitfield @barriers);
        internal static glMemoryBarrierFunc glMemoryBarrierPtr;
        internal static void loadMemoryBarrier()
        {
            try
            {
                glMemoryBarrierPtr = (glMemoryBarrierFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMemoryBarrier"), typeof(glMemoryBarrierFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMemoryBarrier'.");
            }
        }
        public static void glMemoryBarrier(GLbitfield @barriers) => glMemoryBarrierPtr(@barriers);

        internal delegate void glMemoryBarrierByRegionFunc(GLbitfield @barriers);
        internal static glMemoryBarrierByRegionFunc glMemoryBarrierByRegionPtr;
        internal static void loadMemoryBarrierByRegion()
        {
            try
            {
                glMemoryBarrierByRegionPtr = (glMemoryBarrierByRegionFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMemoryBarrierByRegion"), typeof(glMemoryBarrierByRegionFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMemoryBarrierByRegion'.");
            }
        }
        public static void glMemoryBarrierByRegion(GLbitfield @barriers) => glMemoryBarrierByRegionPtr(@barriers);

        internal delegate void glTexStorage2DMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations);
        internal static glTexStorage2DMultisampleFunc glTexStorage2DMultisamplePtr;
        internal static void loadTexStorage2DMultisample()
        {
            try
            {
                glTexStorage2DMultisamplePtr = (glTexStorage2DMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage2DMultisample"), typeof(glTexStorage2DMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage2DMultisample'.");
            }
        }
        public static void glTexStorage2DMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations) => glTexStorage2DMultisamplePtr(@target, @samples, @internalformat, @width, @height, @fixedsamplelocations);

        internal delegate void glGetMultisamplefvFunc(GLenum @pname, GLuint @index, GLfloat * @val);
        internal static glGetMultisamplefvFunc glGetMultisamplefvPtr;
        internal static void loadGetMultisamplefv()
        {
            try
            {
                glGetMultisamplefvPtr = (glGetMultisamplefvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultisamplefv"), typeof(glGetMultisamplefvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultisamplefv'.");
            }
        }
        public static void glGetMultisamplefv(GLenum @pname, GLuint @index, GLfloat * @val) => glGetMultisamplefvPtr(@pname, @index, @val);

        internal delegate void glSampleMaskiFunc(GLuint @maskNumber, GLbitfield @mask);
        internal static glSampleMaskiFunc glSampleMaskiPtr;
        internal static void loadSampleMaski()
        {
            try
            {
                glSampleMaskiPtr = (glSampleMaskiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleMaski"), typeof(glSampleMaskiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleMaski'.");
            }
        }
        public static void glSampleMaski(GLuint @maskNumber, GLbitfield @mask) => glSampleMaskiPtr(@maskNumber, @mask);

        internal delegate void glGetTexLevelParameterivFunc(GLenum @target, GLint @level, GLenum @pname, GLint * @params);
        internal static glGetTexLevelParameterivFunc glGetTexLevelParameterivPtr;
        internal static void loadGetTexLevelParameteriv()
        {
            try
            {
                glGetTexLevelParameterivPtr = (glGetTexLevelParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexLevelParameteriv"), typeof(glGetTexLevelParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexLevelParameteriv'.");
            }
        }
        public static void glGetTexLevelParameteriv(GLenum @target, GLint @level, GLenum @pname, GLint * @params) => glGetTexLevelParameterivPtr(@target, @level, @pname, @params);

        internal delegate void glGetTexLevelParameterfvFunc(GLenum @target, GLint @level, GLenum @pname, GLfloat * @params);
        internal static glGetTexLevelParameterfvFunc glGetTexLevelParameterfvPtr;
        internal static void loadGetTexLevelParameterfv()
        {
            try
            {
                glGetTexLevelParameterfvPtr = (glGetTexLevelParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexLevelParameterfv"), typeof(glGetTexLevelParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexLevelParameterfv'.");
            }
        }
        public static void glGetTexLevelParameterfv(GLenum @target, GLint @level, GLenum @pname, GLfloat * @params) => glGetTexLevelParameterfvPtr(@target, @level, @pname, @params);

        internal delegate void glBindVertexBufferFunc(GLuint @bindingindex, GLuint @buffer, GLintptr @offset, GLsizei @stride);
        internal static glBindVertexBufferFunc glBindVertexBufferPtr;
        internal static void loadBindVertexBuffer()
        {
            try
            {
                glBindVertexBufferPtr = (glBindVertexBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVertexBuffer"), typeof(glBindVertexBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVertexBuffer'.");
            }
        }
        public static void glBindVertexBuffer(GLuint @bindingindex, GLuint @buffer, GLintptr @offset, GLsizei @stride) => glBindVertexBufferPtr(@bindingindex, @buffer, @offset, @stride);

        internal delegate void glVertexAttribFormatFunc(GLuint @attribindex, GLint @size, GLenum @type, GLboolean @normalized, GLuint @relativeoffset);
        internal static glVertexAttribFormatFunc glVertexAttribFormatPtr;
        internal static void loadVertexAttribFormat()
        {
            try
            {
                glVertexAttribFormatPtr = (glVertexAttribFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribFormat"), typeof(glVertexAttribFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribFormat'.");
            }
        }
        public static void glVertexAttribFormat(GLuint @attribindex, GLint @size, GLenum @type, GLboolean @normalized, GLuint @relativeoffset) => glVertexAttribFormatPtr(@attribindex, @size, @type, @normalized, @relativeoffset);

        internal delegate void glVertexAttribIFormatFunc(GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset);
        internal static glVertexAttribIFormatFunc glVertexAttribIFormatPtr;
        internal static void loadVertexAttribIFormat()
        {
            try
            {
                glVertexAttribIFormatPtr = (glVertexAttribIFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribIFormat"), typeof(glVertexAttribIFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribIFormat'.");
            }
        }
        public static void glVertexAttribIFormat(GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset) => glVertexAttribIFormatPtr(@attribindex, @size, @type, @relativeoffset);

        internal delegate void glVertexAttribBindingFunc(GLuint @attribindex, GLuint @bindingindex);
        internal static glVertexAttribBindingFunc glVertexAttribBindingPtr;
        internal static void loadVertexAttribBinding()
        {
            try
            {
                glVertexAttribBindingPtr = (glVertexAttribBindingFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribBinding"), typeof(glVertexAttribBindingFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribBinding'.");
            }
        }
        public static void glVertexAttribBinding(GLuint @attribindex, GLuint @bindingindex) => glVertexAttribBindingPtr(@attribindex, @bindingindex);

        internal delegate void glVertexBindingDivisorFunc(GLuint @bindingindex, GLuint @divisor);
        internal static glVertexBindingDivisorFunc glVertexBindingDivisorPtr;
        internal static void loadVertexBindingDivisor()
        {
            try
            {
                glVertexBindingDivisorPtr = (glVertexBindingDivisorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexBindingDivisor"), typeof(glVertexBindingDivisorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexBindingDivisor'.");
            }
        }
        public static void glVertexBindingDivisor(GLuint @bindingindex, GLuint @divisor) => glVertexBindingDivisorPtr(@bindingindex, @divisor);
        #endregion
    }
}
