using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_DMP_shader_binary
    {
        #region Interop
        static GL_DMP_shader_binary()
        {
            Console.WriteLine("Initalising GL_DMP_shader_binary interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SHADER_BINARY_DMP = 0x9250;
        #endregion

        #region Commands
        #endregion
    }
}
