using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_DMP_program_binary
    {
        #region Interop
        static GL_DMP_program_binary()
        {
            Console.WriteLine("Initalising GL_DMP_program_binary interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SMAPHS30_PROGRAM_BINARY_DMP = 0x9251;
        public static UInt32 GL_SMAPHS_PROGRAM_BINARY_DMP = 0x9252;
        public static UInt32 GL_DMP_PROGRAM_BINARY_DMP = 0x9253;
        #endregion

        #region Commands
        #endregion
    }
}
