using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL31
    {
        #region Interop
        static GL31()
        {
            Console.WriteLine("Initalising GL31 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawArraysInstanced();
            loadDrawElementsInstanced();
            loadTexBuffer();
            loadPrimitiveRestartIndex();
            loadCopyBufferSubData();
            loadGetUniformIndices();
            loadGetActiveUniformsiv();
            loadGetActiveUniformName();
            loadGetUniformBlockIndex();
            loadGetActiveUniformBlockiv();
            loadGetActiveUniformBlockName();
            loadUniformBlockBinding();
            loadBindBufferRange();
            loadBindBufferBase();
            loadGetIntegeri_v();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SAMPLER_2D_RECT = 0x8B63;
        public static UInt32 GL_SAMPLER_2D_RECT_SHADOW = 0x8B64;
        public static UInt32 GL_SAMPLER_BUFFER = 0x8DC2;
        public static UInt32 GL_INT_SAMPLER_2D_RECT = 0x8DCD;
        public static UInt32 GL_INT_SAMPLER_BUFFER = 0x8DD0;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_RECT = 0x8DD5;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_BUFFER = 0x8DD8;
        public static UInt32 GL_TEXTURE_BUFFER = 0x8C2A;
        public static UInt32 GL_MAX_TEXTURE_BUFFER_SIZE = 0x8C2B;
        public static UInt32 GL_TEXTURE_BINDING_BUFFER = 0x8C2C;
        public static UInt32 GL_TEXTURE_BUFFER_DATA_STORE_BINDING = 0x8C2D;
        public static UInt32 GL_TEXTURE_RECTANGLE = 0x84F5;
        public static UInt32 GL_TEXTURE_BINDING_RECTANGLE = 0x84F6;
        public static UInt32 GL_PROXY_TEXTURE_RECTANGLE = 0x84F7;
        public static UInt32 GL_MAX_RECTANGLE_TEXTURE_SIZE = 0x84F8;
        public static UInt32 GL_R8_SNORM = 0x8F94;
        public static UInt32 GL_RG8_SNORM = 0x8F95;
        public static UInt32 GL_RGB8_SNORM = 0x8F96;
        public static UInt32 GL_RGBA8_SNORM = 0x8F97;
        public static UInt32 GL_R16_SNORM = 0x8F98;
        public static UInt32 GL_RG16_SNORM = 0x8F99;
        public static UInt32 GL_RGB16_SNORM = 0x8F9A;
        public static UInt32 GL_RGBA16_SNORM = 0x8F9B;
        public static UInt32 GL_SIGNED_NORMALIZED = 0x8F9C;
        public static UInt32 GL_PRIMITIVE_RESTART = 0x8F9D;
        public static UInt32 GL_PRIMITIVE_RESTART_INDEX = 0x8F9E;
        public static UInt32 GL_COPY_READ_BUFFER = 0x8F36;
        public static UInt32 GL_COPY_WRITE_BUFFER = 0x8F37;
        public static UInt32 GL_UNIFORM_BUFFER = 0x8A11;
        public static UInt32 GL_UNIFORM_BUFFER_BINDING = 0x8A28;
        public static UInt32 GL_UNIFORM_BUFFER_START = 0x8A29;
        public static UInt32 GL_UNIFORM_BUFFER_SIZE = 0x8A2A;
        public static UInt32 GL_MAX_VERTEX_UNIFORM_BLOCKS = 0x8A2B;
        public static UInt32 GL_MAX_GEOMETRY_UNIFORM_BLOCKS = 0x8A2C;
        public static UInt32 GL_MAX_FRAGMENT_UNIFORM_BLOCKS = 0x8A2D;
        public static UInt32 GL_MAX_COMBINED_UNIFORM_BLOCKS = 0x8A2E;
        public static UInt32 GL_MAX_UNIFORM_BUFFER_BINDINGS = 0x8A2F;
        public static UInt32 GL_MAX_UNIFORM_BLOCK_SIZE = 0x8A30;
        public static UInt32 GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS = 0x8A31;
        public static UInt32 GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS = 0x8A32;
        public static UInt32 GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS = 0x8A33;
        public static UInt32 GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT = 0x8A34;
        public static UInt32 GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH = 0x8A35;
        public static UInt32 GL_ACTIVE_UNIFORM_BLOCKS = 0x8A36;
        public static UInt32 GL_UNIFORM_TYPE = 0x8A37;
        public static UInt32 GL_UNIFORM_SIZE = 0x8A38;
        public static UInt32 GL_UNIFORM_NAME_LENGTH = 0x8A39;
        public static UInt32 GL_UNIFORM_BLOCK_INDEX = 0x8A3A;
        public static UInt32 GL_UNIFORM_OFFSET = 0x8A3B;
        public static UInt32 GL_UNIFORM_ARRAY_STRIDE = 0x8A3C;
        public static UInt32 GL_UNIFORM_MATRIX_STRIDE = 0x8A3D;
        public static UInt32 GL_UNIFORM_IS_ROW_MAJOR = 0x8A3E;
        public static UInt32 GL_UNIFORM_BLOCK_BINDING = 0x8A3F;
        public static UInt32 GL_UNIFORM_BLOCK_DATA_SIZE = 0x8A40;
        public static UInt32 GL_UNIFORM_BLOCK_NAME_LENGTH = 0x8A41;
        public static UInt32 GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS = 0x8A42;
        public static UInt32 GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES = 0x8A43;
        public static UInt32 GL_UNIFORM_BLOCK_REFERENCED_BY_VERTEX_SHADER = 0x8A44;
        public static UInt32 GL_UNIFORM_BLOCK_REFERENCED_BY_GEOMETRY_SHADER = 0x8A45;
        public static UInt32 GL_UNIFORM_BLOCK_REFERENCED_BY_FRAGMENT_SHADER = 0x8A46;
        public static UInt32 GL_INVALID_INDEX = 0xFFFFFFFF;
        #endregion

        #region Commands
        internal delegate void glDrawArraysInstancedFunc(GLenum @mode, GLint @first, GLsizei @count, GLsizei @instancecount);
        internal static glDrawArraysInstancedFunc glDrawArraysInstancedPtr;
        internal static void loadDrawArraysInstanced()
        {
            try
            {
                glDrawArraysInstancedPtr = (glDrawArraysInstancedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysInstanced"), typeof(glDrawArraysInstancedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysInstanced'.");
            }
        }
        public static void glDrawArraysInstanced(GLenum @mode, GLint @first, GLsizei @count, GLsizei @instancecount) => glDrawArraysInstancedPtr(@mode, @first, @count, @instancecount);

        internal delegate void glDrawElementsInstancedFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount);
        internal static glDrawElementsInstancedFunc glDrawElementsInstancedPtr;
        internal static void loadDrawElementsInstanced()
        {
            try
            {
                glDrawElementsInstancedPtr = (glDrawElementsInstancedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstanced"), typeof(glDrawElementsInstancedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstanced'.");
            }
        }
        public static void glDrawElementsInstanced(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount) => glDrawElementsInstancedPtr(@mode, @count, @type, @indices, @instancecount);

        internal delegate void glTexBufferFunc(GLenum @target, GLenum @internalformat, GLuint @buffer);
        internal static glTexBufferFunc glTexBufferPtr;
        internal static void loadTexBuffer()
        {
            try
            {
                glTexBufferPtr = (glTexBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBuffer"), typeof(glTexBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBuffer'.");
            }
        }
        public static void glTexBuffer(GLenum @target, GLenum @internalformat, GLuint @buffer) => glTexBufferPtr(@target, @internalformat, @buffer);

        internal delegate void glPrimitiveRestartIndexFunc(GLuint @index);
        internal static glPrimitiveRestartIndexFunc glPrimitiveRestartIndexPtr;
        internal static void loadPrimitiveRestartIndex()
        {
            try
            {
                glPrimitiveRestartIndexPtr = (glPrimitiveRestartIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPrimitiveRestartIndex"), typeof(glPrimitiveRestartIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPrimitiveRestartIndex'.");
            }
        }
        public static void glPrimitiveRestartIndex(GLuint @index) => glPrimitiveRestartIndexPtr(@index);

        internal delegate void glCopyBufferSubDataFunc(GLenum @readTarget, GLenum @writeTarget, GLintptr @readOffset, GLintptr @writeOffset, GLsizeiptr @size);
        internal static glCopyBufferSubDataFunc glCopyBufferSubDataPtr;
        internal static void loadCopyBufferSubData()
        {
            try
            {
                glCopyBufferSubDataPtr = (glCopyBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyBufferSubData"), typeof(glCopyBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyBufferSubData'.");
            }
        }
        public static void glCopyBufferSubData(GLenum @readTarget, GLenum @writeTarget, GLintptr @readOffset, GLintptr @writeOffset, GLsizeiptr @size) => glCopyBufferSubDataPtr(@readTarget, @writeTarget, @readOffset, @writeOffset, @size);

        internal delegate void glGetUniformIndicesFunc(GLuint @program, GLsizei @uniformCount, const GLchar *const* @uniformNames, GLuint * @uniformIndices);
        internal static glGetUniformIndicesFunc glGetUniformIndicesPtr;
        internal static void loadGetUniformIndices()
        {
            try
            {
                glGetUniformIndicesPtr = (glGetUniformIndicesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformIndices"), typeof(glGetUniformIndicesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformIndices'.");
            }
        }
        public static void glGetUniformIndices(GLuint @program, GLsizei @uniformCount, const GLchar *const* @uniformNames, GLuint * @uniformIndices) => glGetUniformIndicesPtr(@program, @uniformCount, @uniformNames, @uniformIndices);

        internal delegate void glGetActiveUniformsivFunc(GLuint @program, GLsizei @uniformCount, const GLuint * @uniformIndices, GLenum @pname, GLint * @params);
        internal static glGetActiveUniformsivFunc glGetActiveUniformsivPtr;
        internal static void loadGetActiveUniformsiv()
        {
            try
            {
                glGetActiveUniformsivPtr = (glGetActiveUniformsivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveUniformsiv"), typeof(glGetActiveUniformsivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveUniformsiv'.");
            }
        }
        public static void glGetActiveUniformsiv(GLuint @program, GLsizei @uniformCount, const GLuint * @uniformIndices, GLenum @pname, GLint * @params) => glGetActiveUniformsivPtr(@program, @uniformCount, @uniformIndices, @pname, @params);

        internal delegate void glGetActiveUniformNameFunc(GLuint @program, GLuint @uniformIndex, GLsizei @bufSize, GLsizei * @length, GLchar * @uniformName);
        internal static glGetActiveUniformNameFunc glGetActiveUniformNamePtr;
        internal static void loadGetActiveUniformName()
        {
            try
            {
                glGetActiveUniformNamePtr = (glGetActiveUniformNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveUniformName"), typeof(glGetActiveUniformNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveUniformName'.");
            }
        }
        public static void glGetActiveUniformName(GLuint @program, GLuint @uniformIndex, GLsizei @bufSize, GLsizei * @length, GLchar * @uniformName) => glGetActiveUniformNamePtr(@program, @uniformIndex, @bufSize, @length, @uniformName);

        internal delegate GLuint glGetUniformBlockIndexFunc(GLuint @program, const GLchar * @uniformBlockName);
        internal static glGetUniformBlockIndexFunc glGetUniformBlockIndexPtr;
        internal static void loadGetUniformBlockIndex()
        {
            try
            {
                glGetUniformBlockIndexPtr = (glGetUniformBlockIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformBlockIndex"), typeof(glGetUniformBlockIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformBlockIndex'.");
            }
        }
        public static GLuint glGetUniformBlockIndex(GLuint @program, const GLchar * @uniformBlockName) => glGetUniformBlockIndexPtr(@program, @uniformBlockName);

        internal delegate void glGetActiveUniformBlockivFunc(GLuint @program, GLuint @uniformBlockIndex, GLenum @pname, GLint * @params);
        internal static glGetActiveUniformBlockivFunc glGetActiveUniformBlockivPtr;
        internal static void loadGetActiveUniformBlockiv()
        {
            try
            {
                glGetActiveUniformBlockivPtr = (glGetActiveUniformBlockivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveUniformBlockiv"), typeof(glGetActiveUniformBlockivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveUniformBlockiv'.");
            }
        }
        public static void glGetActiveUniformBlockiv(GLuint @program, GLuint @uniformBlockIndex, GLenum @pname, GLint * @params) => glGetActiveUniformBlockivPtr(@program, @uniformBlockIndex, @pname, @params);

        internal delegate void glGetActiveUniformBlockNameFunc(GLuint @program, GLuint @uniformBlockIndex, GLsizei @bufSize, GLsizei * @length, GLchar * @uniformBlockName);
        internal static glGetActiveUniformBlockNameFunc glGetActiveUniformBlockNamePtr;
        internal static void loadGetActiveUniformBlockName()
        {
            try
            {
                glGetActiveUniformBlockNamePtr = (glGetActiveUniformBlockNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveUniformBlockName"), typeof(glGetActiveUniformBlockNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveUniformBlockName'.");
            }
        }
        public static void glGetActiveUniformBlockName(GLuint @program, GLuint @uniformBlockIndex, GLsizei @bufSize, GLsizei * @length, GLchar * @uniformBlockName) => glGetActiveUniformBlockNamePtr(@program, @uniformBlockIndex, @bufSize, @length, @uniformBlockName);

        internal delegate void glUniformBlockBindingFunc(GLuint @program, GLuint @uniformBlockIndex, GLuint @uniformBlockBinding);
        internal static glUniformBlockBindingFunc glUniformBlockBindingPtr;
        internal static void loadUniformBlockBinding()
        {
            try
            {
                glUniformBlockBindingPtr = (glUniformBlockBindingFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformBlockBinding"), typeof(glUniformBlockBindingFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformBlockBinding'.");
            }
        }
        public static void glUniformBlockBinding(GLuint @program, GLuint @uniformBlockIndex, GLuint @uniformBlockBinding) => glUniformBlockBindingPtr(@program, @uniformBlockIndex, @uniformBlockBinding);

        internal delegate void glBindBufferRangeFunc(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glBindBufferRangeFunc glBindBufferRangePtr;
        internal static void loadBindBufferRange()
        {
            try
            {
                glBindBufferRangePtr = (glBindBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferRange"), typeof(glBindBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferRange'.");
            }
        }
        public static void glBindBufferRange(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glBindBufferRangePtr(@target, @index, @buffer, @offset, @size);

        internal delegate void glBindBufferBaseFunc(GLenum @target, GLuint @index, GLuint @buffer);
        internal static glBindBufferBaseFunc glBindBufferBasePtr;
        internal static void loadBindBufferBase()
        {
            try
            {
                glBindBufferBasePtr = (glBindBufferBaseFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferBase"), typeof(glBindBufferBaseFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferBase'.");
            }
        }
        public static void glBindBufferBase(GLenum @target, GLuint @index, GLuint @buffer) => glBindBufferBasePtr(@target, @index, @buffer);

        internal delegate void glGetIntegeri_vFunc(GLenum @target, GLuint @index, GLint * @data);
        internal static glGetIntegeri_vFunc glGetIntegeri_vPtr;
        internal static void loadGetIntegeri_v()
        {
            try
            {
                glGetIntegeri_vPtr = (glGetIntegeri_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetIntegeri_v"), typeof(glGetIntegeri_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetIntegeri_v'.");
            }
        }
        public static void glGetIntegeri_v(GLenum @target, GLuint @index, GLint * @data) => glGetIntegeri_vPtr(@target, @index, @data);
        #endregion
    }
}
