using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GLES20
    {
        #region Interop
        static GLES20()
        {
            Console.WriteLine("Initalising GLES20 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadActiveTexture();
            loadAttachShader();
            loadBindAttribLocation();
            loadBindBuffer();
            loadBindFramebuffer();
            loadBindRenderbuffer();
            loadBindTexture();
            loadBlendColor();
            loadBlendEquation();
            loadBlendEquationSeparate();
            loadBlendFunc();
            loadBlendFuncSeparate();
            loadBufferData();
            loadBufferSubData();
            loadCheckFramebufferStatus();
            loadClear();
            loadClearColor();
            loadClearDepthf();
            loadClearStencil();
            loadColorMask();
            loadCompileShader();
            loadCompressedTexImage2D();
            loadCompressedTexSubImage2D();
            loadCopyTexImage2D();
            loadCopyTexSubImage2D();
            loadCreateProgram();
            loadCreateShader();
            loadCullFace();
            loadDeleteBuffers();
            loadDeleteFramebuffers();
            loadDeleteProgram();
            loadDeleteRenderbuffers();
            loadDeleteShader();
            loadDeleteTextures();
            loadDepthFunc();
            loadDepthMask();
            loadDepthRangef();
            loadDetachShader();
            loadDisable();
            loadDisableVertexAttribArray();
            loadDrawArrays();
            loadDrawElements();
            loadEnable();
            loadEnableVertexAttribArray();
            loadFinish();
            loadFlush();
            loadFramebufferRenderbuffer();
            loadFramebufferTexture2D();
            loadFrontFace();
            loadGenBuffers();
            loadGenerateMipmap();
            loadGenFramebuffers();
            loadGenRenderbuffers();
            loadGenTextures();
            loadGetActiveAttrib();
            loadGetActiveUniform();
            loadGetAttachedShaders();
            loadGetAttribLocation();
            loadGetBooleanv();
            loadGetBufferParameteriv();
            loadGetError();
            loadGetFloatv();
            loadGetFramebufferAttachmentParameteriv();
            loadGetIntegerv();
            loadGetProgramiv();
            loadGetProgramInfoLog();
            loadGetRenderbufferParameteriv();
            loadGetShaderiv();
            loadGetShaderInfoLog();
            loadGetShaderPrecisionFormat();
            loadGetShaderSource();
            loadGetString();
            loadGetTexParameterfv();
            loadGetTexParameteriv();
            loadGetUniformfv();
            loadGetUniformiv();
            loadGetUniformLocation();
            loadGetVertexAttribfv();
            loadGetVertexAttribiv();
            loadGetVertexAttribPointerv();
            loadHint();
            loadIsBuffer();
            loadIsEnabled();
            loadIsFramebuffer();
            loadIsProgram();
            loadIsRenderbuffer();
            loadIsShader();
            loadIsTexture();
            loadLineWidth();
            loadLinkProgram();
            loadPixelStorei();
            loadPolygonOffset();
            loadReadPixels();
            loadReleaseShaderCompiler();
            loadRenderbufferStorage();
            loadSampleCoverage();
            loadScissor();
            loadShaderBinary();
            loadShaderSource();
            loadStencilFunc();
            loadStencilFuncSeparate();
            loadStencilMask();
            loadStencilMaskSeparate();
            loadStencilOp();
            loadStencilOpSeparate();
            loadTexImage2D();
            loadTexParameterf();
            loadTexParameterfv();
            loadTexParameteri();
            loadTexParameteriv();
            loadTexSubImage2D();
            loadUniform1f();
            loadUniform1fv();
            loadUniform1i();
            loadUniform1iv();
            loadUniform2f();
            loadUniform2fv();
            loadUniform2i();
            loadUniform2iv();
            loadUniform3f();
            loadUniform3fv();
            loadUniform3i();
            loadUniform3iv();
            loadUniform4f();
            loadUniform4fv();
            loadUniform4i();
            loadUniform4iv();
            loadUniformMatrix2fv();
            loadUniformMatrix3fv();
            loadUniformMatrix4fv();
            loadUseProgram();
            loadValidateProgram();
            loadVertexAttrib1f();
            loadVertexAttrib1fv();
            loadVertexAttrib2f();
            loadVertexAttrib2fv();
            loadVertexAttrib3f();
            loadVertexAttrib3fv();
            loadVertexAttrib4f();
            loadVertexAttrib4fv();
            loadVertexAttribPointer();
            loadViewport();
        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_BUFFER_BIT = 0x00000100;
        public static UInt32 GL_STENCIL_BUFFER_BIT = 0x00000400;
        public static UInt32 GL_COLOR_BUFFER_BIT = 0x00004000;
        public static UInt32 GL_FALSE = 0;
        public static UInt32 GL_TRUE = 1;
        public static UInt32 GL_POINTS = 0x0000;
        public static UInt32 GL_LINES = 0x0001;
        public static UInt32 GL_LINE_LOOP = 0x0002;
        public static UInt32 GL_LINE_STRIP = 0x0003;
        public static UInt32 GL_TRIANGLES = 0x0004;
        public static UInt32 GL_TRIANGLE_STRIP = 0x0005;
        public static UInt32 GL_TRIANGLE_FAN = 0x0006;
        public static UInt32 GL_ZERO = 0;
        public static UInt32 GL_ONE = 1;
        public static UInt32 GL_SRC_COLOR = 0x0300;
        public static UInt32 GL_ONE_MINUS_SRC_COLOR = 0x0301;
        public static UInt32 GL_SRC_ALPHA = 0x0302;
        public static UInt32 GL_ONE_MINUS_SRC_ALPHA = 0x0303;
        public static UInt32 GL_DST_ALPHA = 0x0304;
        public static UInt32 GL_ONE_MINUS_DST_ALPHA = 0x0305;
        public static UInt32 GL_DST_COLOR = 0x0306;
        public static UInt32 GL_ONE_MINUS_DST_COLOR = 0x0307;
        public static UInt32 GL_SRC_ALPHA_SATURATE = 0x0308;
        public static UInt32 GL_FUNC_ADD = 0x8006;
        public static UInt32 GL_BLEND_EQUATION = 0x8009;
        public static UInt32 GL_BLEND_EQUATION_RGB = 0x8009;
        public static UInt32 GL_BLEND_EQUATION_ALPHA = 0x883D;
        public static UInt32 GL_FUNC_SUBTRACT = 0x800A;
        public static UInt32 GL_FUNC_REVERSE_SUBTRACT = 0x800B;
        public static UInt32 GL_BLEND_DST_RGB = 0x80C8;
        public static UInt32 GL_BLEND_SRC_RGB = 0x80C9;
        public static UInt32 GL_BLEND_DST_ALPHA = 0x80CA;
        public static UInt32 GL_BLEND_SRC_ALPHA = 0x80CB;
        public static UInt32 GL_CONSTANT_COLOR = 0x8001;
        public static UInt32 GL_ONE_MINUS_CONSTANT_COLOR = 0x8002;
        public static UInt32 GL_CONSTANT_ALPHA = 0x8003;
        public static UInt32 GL_ONE_MINUS_CONSTANT_ALPHA = 0x8004;
        public static UInt32 GL_BLEND_COLOR = 0x8005;
        public static UInt32 GL_ARRAY_BUFFER = 0x8892;
        public static UInt32 GL_ELEMENT_ARRAY_BUFFER = 0x8893;
        public static UInt32 GL_ARRAY_BUFFER_BINDING = 0x8894;
        public static UInt32 GL_ELEMENT_ARRAY_BUFFER_BINDING = 0x8895;
        public static UInt32 GL_STREAM_DRAW = 0x88E0;
        public static UInt32 GL_STATIC_DRAW = 0x88E4;
        public static UInt32 GL_DYNAMIC_DRAW = 0x88E8;
        public static UInt32 GL_BUFFER_SIZE = 0x8764;
        public static UInt32 GL_BUFFER_USAGE = 0x8765;
        public static UInt32 GL_CURRENT_VERTEX_ATTRIB = 0x8626;
        public static UInt32 GL_FRONT = 0x0404;
        public static UInt32 GL_BACK = 0x0405;
        public static UInt32 GL_FRONT_AND_BACK = 0x0408;
        public static UInt32 GL_TEXTURE_2D = 0x0DE1;
        public static UInt32 GL_CULL_FACE = 0x0B44;
        public static UInt32 GL_BLEND = 0x0BE2;
        public static UInt32 GL_DITHER = 0x0BD0;
        public static UInt32 GL_STENCIL_TEST = 0x0B90;
        public static UInt32 GL_DEPTH_TEST = 0x0B71;
        public static UInt32 GL_SCISSOR_TEST = 0x0C11;
        public static UInt32 GL_POLYGON_OFFSET_FILL = 0x8037;
        public static UInt32 GL_SAMPLE_ALPHA_TO_COVERAGE = 0x809E;
        public static UInt32 GL_SAMPLE_COVERAGE = 0x80A0;
        public static UInt32 GL_NO_ERROR = 0;
        public static UInt32 GL_INVALID_ENUM = 0x0500;
        public static UInt32 GL_INVALID_VALUE = 0x0501;
        public static UInt32 GL_INVALID_OPERATION = 0x0502;
        public static UInt32 GL_OUT_OF_MEMORY = 0x0505;
        public static UInt32 GL_CW = 0x0900;
        public static UInt32 GL_CCW = 0x0901;
        public static UInt32 GL_LINE_WIDTH = 0x0B21;
        public static UInt32 GL_ALIASED_POINT_SIZE_RANGE = 0x846D;
        public static UInt32 GL_ALIASED_LINE_WIDTH_RANGE = 0x846E;
        public static UInt32 GL_CULL_FACE_MODE = 0x0B45;
        public static UInt32 GL_FRONT_FACE = 0x0B46;
        public static UInt32 GL_DEPTH_RANGE = 0x0B70;
        public static UInt32 GL_DEPTH_WRITEMASK = 0x0B72;
        public static UInt32 GL_DEPTH_CLEAR_VALUE = 0x0B73;
        public static UInt32 GL_DEPTH_FUNC = 0x0B74;
        public static UInt32 GL_STENCIL_CLEAR_VALUE = 0x0B91;
        public static UInt32 GL_STENCIL_FUNC = 0x0B92;
        public static UInt32 GL_STENCIL_FAIL = 0x0B94;
        public static UInt32 GL_STENCIL_PASS_DEPTH_FAIL = 0x0B95;
        public static UInt32 GL_STENCIL_PASS_DEPTH_PASS = 0x0B96;
        public static UInt32 GL_STENCIL_REF = 0x0B97;
        public static UInt32 GL_STENCIL_VALUE_MASK = 0x0B93;
        public static UInt32 GL_STENCIL_WRITEMASK = 0x0B98;
        public static UInt32 GL_STENCIL_BACK_FUNC = 0x8800;
        public static UInt32 GL_STENCIL_BACK_FAIL = 0x8801;
        public static UInt32 GL_STENCIL_BACK_PASS_DEPTH_FAIL = 0x8802;
        public static UInt32 GL_STENCIL_BACK_PASS_DEPTH_PASS = 0x8803;
        public static UInt32 GL_STENCIL_BACK_REF = 0x8CA3;
        public static UInt32 GL_STENCIL_BACK_VALUE_MASK = 0x8CA4;
        public static UInt32 GL_STENCIL_BACK_WRITEMASK = 0x8CA5;
        public static UInt32 GL_VIEWPORT = 0x0BA2;
        public static UInt32 GL_SCISSOR_BOX = 0x0C10;
        public static UInt32 GL_COLOR_CLEAR_VALUE = 0x0C22;
        public static UInt32 GL_COLOR_WRITEMASK = 0x0C23;
        public static UInt32 GL_UNPACK_ALIGNMENT = 0x0CF5;
        public static UInt32 GL_PACK_ALIGNMENT = 0x0D05;
        public static UInt32 GL_MAX_TEXTURE_SIZE = 0x0D33;
        public static UInt32 GL_MAX_VIEWPORT_DIMS = 0x0D3A;
        public static UInt32 GL_SUBPIXEL_BITS = 0x0D50;
        public static UInt32 GL_RED_BITS = 0x0D52;
        public static UInt32 GL_GREEN_BITS = 0x0D53;
        public static UInt32 GL_BLUE_BITS = 0x0D54;
        public static UInt32 GL_ALPHA_BITS = 0x0D55;
        public static UInt32 GL_DEPTH_BITS = 0x0D56;
        public static UInt32 GL_STENCIL_BITS = 0x0D57;
        public static UInt32 GL_POLYGON_OFFSET_UNITS = 0x2A00;
        public static UInt32 GL_POLYGON_OFFSET_FACTOR = 0x8038;
        public static UInt32 GL_TEXTURE_BINDING_2D = 0x8069;
        public static UInt32 GL_SAMPLE_BUFFERS = 0x80A8;
        public static UInt32 GL_SAMPLES = 0x80A9;
        public static UInt32 GL_SAMPLE_COVERAGE_VALUE = 0x80AA;
        public static UInt32 GL_SAMPLE_COVERAGE_INVERT = 0x80AB;
        public static UInt32 GL_NUM_COMPRESSED_TEXTURE_FORMATS = 0x86A2;
        public static UInt32 GL_COMPRESSED_TEXTURE_FORMATS = 0x86A3;
        public static UInt32 GL_DONT_CARE = 0x1100;
        public static UInt32 GL_FASTEST = 0x1101;
        public static UInt32 GL_NICEST = 0x1102;
        public static UInt32 GL_GENERATE_MIPMAP_HINT = 0x8192;
        public static UInt32 GL_BYTE = 0x1400;
        public static UInt32 GL_UNSIGNED_BYTE = 0x1401;
        public static UInt32 GL_SHORT = 0x1402;
        public static UInt32 GL_UNSIGNED_SHORT = 0x1403;
        public static UInt32 GL_INT = 0x1404;
        public static UInt32 GL_UNSIGNED_INT = 0x1405;
        public static UInt32 GL_FLOAT = 0x1406;
        public static UInt32 GL_FIXED = 0x140C;
        public static UInt32 GL_DEPTH_COMPONENT = 0x1902;
        public static UInt32 GL_ALPHA = 0x1906;
        public static UInt32 GL_RGB = 0x1907;
        public static UInt32 GL_RGBA = 0x1908;
        public static UInt32 GL_LUMINANCE = 0x1909;
        public static UInt32 GL_LUMINANCE_ALPHA = 0x190A;
        public static UInt32 GL_UNSIGNED_SHORT_4_4_4_4 = 0x8033;
        public static UInt32 GL_UNSIGNED_SHORT_5_5_5_1 = 0x8034;
        public static UInt32 GL_UNSIGNED_SHORT_5_6_5 = 0x8363;
        public static UInt32 GL_FRAGMENT_SHADER = 0x8B30;
        public static UInt32 GL_VERTEX_SHADER = 0x8B31;
        public static UInt32 GL_MAX_VERTEX_ATTRIBS = 0x8869;
        public static UInt32 GL_MAX_VERTEX_UNIFORM_VECTORS = 0x8DFB;
        public static UInt32 GL_MAX_VARYING_VECTORS = 0x8DFC;
        public static UInt32 GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS = 0x8B4D;
        public static UInt32 GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS = 0x8B4C;
        public static UInt32 GL_MAX_TEXTURE_IMAGE_UNITS = 0x8872;
        public static UInt32 GL_MAX_FRAGMENT_UNIFORM_VECTORS = 0x8DFD;
        public static UInt32 GL_SHADER_TYPE = 0x8B4F;
        public static UInt32 GL_DELETE_STATUS = 0x8B80;
        public static UInt32 GL_LINK_STATUS = 0x8B82;
        public static UInt32 GL_VALIDATE_STATUS = 0x8B83;
        public static UInt32 GL_ATTACHED_SHADERS = 0x8B85;
        public static UInt32 GL_ACTIVE_UNIFORMS = 0x8B86;
        public static UInt32 GL_ACTIVE_UNIFORM_MAX_LENGTH = 0x8B87;
        public static UInt32 GL_ACTIVE_ATTRIBUTES = 0x8B89;
        public static UInt32 GL_ACTIVE_ATTRIBUTE_MAX_LENGTH = 0x8B8A;
        public static UInt32 GL_SHADING_LANGUAGE_VERSION = 0x8B8C;
        public static UInt32 GL_CURRENT_PROGRAM = 0x8B8D;
        public static UInt32 GL_NEVER = 0x0200;
        public static UInt32 GL_LESS = 0x0201;
        public static UInt32 GL_EQUAL = 0x0202;
        public static UInt32 GL_LEQUAL = 0x0203;
        public static UInt32 GL_GREATER = 0x0204;
        public static UInt32 GL_NOTEQUAL = 0x0205;
        public static UInt32 GL_GEQUAL = 0x0206;
        public static UInt32 GL_ALWAYS = 0x0207;
        public static UInt32 GL_KEEP = 0x1E00;
        public static UInt32 GL_REPLACE = 0x1E01;
        public static UInt32 GL_INCR = 0x1E02;
        public static UInt32 GL_DECR = 0x1E03;
        public static UInt32 GL_INVERT = 0x150A;
        public static UInt32 GL_INCR_WRAP = 0x8507;
        public static UInt32 GL_DECR_WRAP = 0x8508;
        public static UInt32 GL_VENDOR = 0x1F00;
        public static UInt32 GL_RENDERER = 0x1F01;
        public static UInt32 GL_VERSION = 0x1F02;
        public static UInt32 GL_EXTENSIONS = 0x1F03;
        public static UInt32 GL_NEAREST = 0x2600;
        public static UInt32 GL_LINEAR = 0x2601;
        public static UInt32 GL_NEAREST_MIPMAP_NEAREST = 0x2700;
        public static UInt32 GL_LINEAR_MIPMAP_NEAREST = 0x2701;
        public static UInt32 GL_NEAREST_MIPMAP_LINEAR = 0x2702;
        public static UInt32 GL_LINEAR_MIPMAP_LINEAR = 0x2703;
        public static UInt32 GL_TEXTURE_MAG_FILTER = 0x2800;
        public static UInt32 GL_TEXTURE_MIN_FILTER = 0x2801;
        public static UInt32 GL_TEXTURE_WRAP_S = 0x2802;
        public static UInt32 GL_TEXTURE_WRAP_T = 0x2803;
        public static UInt32 GL_TEXTURE = 0x1702;
        public static UInt32 GL_TEXTURE_CUBE_MAP = 0x8513;
        public static UInt32 GL_TEXTURE_BINDING_CUBE_MAP = 0x8514;
        public static UInt32 GL_TEXTURE_CUBE_MAP_POSITIVE_X = 0x8515;
        public static UInt32 GL_TEXTURE_CUBE_MAP_NEGATIVE_X = 0x8516;
        public static UInt32 GL_TEXTURE_CUBE_MAP_POSITIVE_Y = 0x8517;
        public static UInt32 GL_TEXTURE_CUBE_MAP_NEGATIVE_Y = 0x8518;
        public static UInt32 GL_TEXTURE_CUBE_MAP_POSITIVE_Z = 0x8519;
        public static UInt32 GL_TEXTURE_CUBE_MAP_NEGATIVE_Z = 0x851A;
        public static UInt32 GL_MAX_CUBE_MAP_TEXTURE_SIZE = 0x851C;
        public static UInt32 GL_TEXTURE0 = 0x84C0;
        public static UInt32 GL_TEXTURE1 = 0x84C1;
        public static UInt32 GL_TEXTURE2 = 0x84C2;
        public static UInt32 GL_TEXTURE3 = 0x84C3;
        public static UInt32 GL_TEXTURE4 = 0x84C4;
        public static UInt32 GL_TEXTURE5 = 0x84C5;
        public static UInt32 GL_TEXTURE6 = 0x84C6;
        public static UInt32 GL_TEXTURE7 = 0x84C7;
        public static UInt32 GL_TEXTURE8 = 0x84C8;
        public static UInt32 GL_TEXTURE9 = 0x84C9;
        public static UInt32 GL_TEXTURE10 = 0x84CA;
        public static UInt32 GL_TEXTURE11 = 0x84CB;
        public static UInt32 GL_TEXTURE12 = 0x84CC;
        public static UInt32 GL_TEXTURE13 = 0x84CD;
        public static UInt32 GL_TEXTURE14 = 0x84CE;
        public static UInt32 GL_TEXTURE15 = 0x84CF;
        public static UInt32 GL_TEXTURE16 = 0x84D0;
        public static UInt32 GL_TEXTURE17 = 0x84D1;
        public static UInt32 GL_TEXTURE18 = 0x84D2;
        public static UInt32 GL_TEXTURE19 = 0x84D3;
        public static UInt32 GL_TEXTURE20 = 0x84D4;
        public static UInt32 GL_TEXTURE21 = 0x84D5;
        public static UInt32 GL_TEXTURE22 = 0x84D6;
        public static UInt32 GL_TEXTURE23 = 0x84D7;
        public static UInt32 GL_TEXTURE24 = 0x84D8;
        public static UInt32 GL_TEXTURE25 = 0x84D9;
        public static UInt32 GL_TEXTURE26 = 0x84DA;
        public static UInt32 GL_TEXTURE27 = 0x84DB;
        public static UInt32 GL_TEXTURE28 = 0x84DC;
        public static UInt32 GL_TEXTURE29 = 0x84DD;
        public static UInt32 GL_TEXTURE30 = 0x84DE;
        public static UInt32 GL_TEXTURE31 = 0x84DF;
        public static UInt32 GL_ACTIVE_TEXTURE = 0x84E0;
        public static UInt32 GL_REPEAT = 0x2901;
        public static UInt32 GL_CLAMP_TO_EDGE = 0x812F;
        public static UInt32 GL_MIRRORED_REPEAT = 0x8370;
        public static UInt32 GL_FLOAT_VEC2 = 0x8B50;
        public static UInt32 GL_FLOAT_VEC3 = 0x8B51;
        public static UInt32 GL_FLOAT_VEC4 = 0x8B52;
        public static UInt32 GL_INT_VEC2 = 0x8B53;
        public static UInt32 GL_INT_VEC3 = 0x8B54;
        public static UInt32 GL_INT_VEC4 = 0x8B55;
        public static UInt32 GL_BOOL = 0x8B56;
        public static UInt32 GL_BOOL_VEC2 = 0x8B57;
        public static UInt32 GL_BOOL_VEC3 = 0x8B58;
        public static UInt32 GL_BOOL_VEC4 = 0x8B59;
        public static UInt32 GL_FLOAT_MAT2 = 0x8B5A;
        public static UInt32 GL_FLOAT_MAT3 = 0x8B5B;
        public static UInt32 GL_FLOAT_MAT4 = 0x8B5C;
        public static UInt32 GL_SAMPLER_2D = 0x8B5E;
        public static UInt32 GL_SAMPLER_CUBE = 0x8B60;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_ENABLED = 0x8622;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_SIZE = 0x8623;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_STRIDE = 0x8624;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_TYPE = 0x8625;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_NORMALIZED = 0x886A;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_POINTER = 0x8645;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING = 0x889F;
        public static UInt32 GL_IMPLEMENTATION_COLOR_READ_TYPE = 0x8B9A;
        public static UInt32 GL_IMPLEMENTATION_COLOR_READ_FORMAT = 0x8B9B;
        public static UInt32 GL_COMPILE_STATUS = 0x8B81;
        public static UInt32 GL_INFO_LOG_LENGTH = 0x8B84;
        public static UInt32 GL_SHADER_SOURCE_LENGTH = 0x8B88;
        public static UInt32 GL_SHADER_COMPILER = 0x8DFA;
        public static UInt32 GL_SHADER_BINARY_FORMATS = 0x8DF8;
        public static UInt32 GL_NUM_SHADER_BINARY_FORMATS = 0x8DF9;
        public static UInt32 GL_LOW_FLOAT = 0x8DF0;
        public static UInt32 GL_MEDIUM_FLOAT = 0x8DF1;
        public static UInt32 GL_HIGH_FLOAT = 0x8DF2;
        public static UInt32 GL_LOW_INT = 0x8DF3;
        public static UInt32 GL_MEDIUM_INT = 0x8DF4;
        public static UInt32 GL_HIGH_INT = 0x8DF5;
        public static UInt32 GL_FRAMEBUFFER = 0x8D40;
        public static UInt32 GL_RENDERBUFFER = 0x8D41;
        public static UInt32 GL_RGBA4 = 0x8056;
        public static UInt32 GL_RGB5_A1 = 0x8057;
        public static UInt32 GL_RGB565 = 0x8D62;
        public static UInt32 GL_DEPTH_COMPONENT16 = 0x81A5;
        public static UInt32 GL_STENCIL_INDEX8 = 0x8D48;
        public static UInt32 GL_RENDERBUFFER_WIDTH = 0x8D42;
        public static UInt32 GL_RENDERBUFFER_HEIGHT = 0x8D43;
        public static UInt32 GL_RENDERBUFFER_INTERNAL_FORMAT = 0x8D44;
        public static UInt32 GL_RENDERBUFFER_RED_SIZE = 0x8D50;
        public static UInt32 GL_RENDERBUFFER_GREEN_SIZE = 0x8D51;
        public static UInt32 GL_RENDERBUFFER_BLUE_SIZE = 0x8D52;
        public static UInt32 GL_RENDERBUFFER_ALPHA_SIZE = 0x8D53;
        public static UInt32 GL_RENDERBUFFER_DEPTH_SIZE = 0x8D54;
        public static UInt32 GL_RENDERBUFFER_STENCIL_SIZE = 0x8D55;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE = 0x8CD0;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME = 0x8CD1;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL = 0x8CD2;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE = 0x8CD3;
        public static UInt32 GL_COLOR_ATTACHMENT0 = 0x8CE0;
        public static UInt32 GL_DEPTH_ATTACHMENT = 0x8D00;
        public static UInt32 GL_STENCIL_ATTACHMENT = 0x8D20;
        public static UInt32 GL_NONE = 0;
        public static UInt32 GL_FRAMEBUFFER_COMPLETE = 0x8CD5;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT = 0x8CD6;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT = 0x8CD7;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS = 0x8CD9;
        public static UInt32 GL_FRAMEBUFFER_UNSUPPORTED = 0x8CDD;
        public static UInt32 GL_FRAMEBUFFER_BINDING = 0x8CA6;
        public static UInt32 GL_RENDERBUFFER_BINDING = 0x8CA7;
        public static UInt32 GL_MAX_RENDERBUFFER_SIZE = 0x84E8;
        public static UInt32 GL_INVALID_FRAMEBUFFER_OPERATION = 0x0506;
        #endregion

        #region Commands
        internal delegate void glActiveTextureFunc(GLenum @texture);
        internal static glActiveTextureFunc glActiveTexturePtr;
        internal static void loadActiveTexture()
        {
            try
            {
                glActiveTexturePtr = (glActiveTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glActiveTexture"), typeof(glActiveTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glActiveTexture'.");
            }
        }
        public static void glActiveTexture(GLenum @texture) => glActiveTexturePtr(@texture);

        internal delegate void glAttachShaderFunc(GLuint @program, GLuint @shader);
        internal static glAttachShaderFunc glAttachShaderPtr;
        internal static void loadAttachShader()
        {
            try
            {
                glAttachShaderPtr = (glAttachShaderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAttachShader"), typeof(glAttachShaderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAttachShader'.");
            }
        }
        public static void glAttachShader(GLuint @program, GLuint @shader) => glAttachShaderPtr(@program, @shader);

        internal delegate void glBindAttribLocationFunc(GLuint @program, GLuint @index, const GLchar * @name);
        internal static glBindAttribLocationFunc glBindAttribLocationPtr;
        internal static void loadBindAttribLocation()
        {
            try
            {
                glBindAttribLocationPtr = (glBindAttribLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindAttribLocation"), typeof(glBindAttribLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindAttribLocation'.");
            }
        }
        public static void glBindAttribLocation(GLuint @program, GLuint @index, const GLchar * @name) => glBindAttribLocationPtr(@program, @index, @name);

        internal delegate void glBindBufferFunc(GLenum @target, GLuint @buffer);
        internal static glBindBufferFunc glBindBufferPtr;
        internal static void loadBindBuffer()
        {
            try
            {
                glBindBufferPtr = (glBindBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBuffer"), typeof(glBindBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBuffer'.");
            }
        }
        public static void glBindBuffer(GLenum @target, GLuint @buffer) => glBindBufferPtr(@target, @buffer);

        internal delegate void glBindFramebufferFunc(GLenum @target, GLuint @framebuffer);
        internal static glBindFramebufferFunc glBindFramebufferPtr;
        internal static void loadBindFramebuffer()
        {
            try
            {
                glBindFramebufferPtr = (glBindFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindFramebuffer"), typeof(glBindFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindFramebuffer'.");
            }
        }
        public static void glBindFramebuffer(GLenum @target, GLuint @framebuffer) => glBindFramebufferPtr(@target, @framebuffer);

        internal delegate void glBindRenderbufferFunc(GLenum @target, GLuint @renderbuffer);
        internal static glBindRenderbufferFunc glBindRenderbufferPtr;
        internal static void loadBindRenderbuffer()
        {
            try
            {
                glBindRenderbufferPtr = (glBindRenderbufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindRenderbuffer"), typeof(glBindRenderbufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindRenderbuffer'.");
            }
        }
        public static void glBindRenderbuffer(GLenum @target, GLuint @renderbuffer) => glBindRenderbufferPtr(@target, @renderbuffer);

        internal delegate void glBindTextureFunc(GLenum @target, GLuint @texture);
        internal static glBindTextureFunc glBindTexturePtr;
        internal static void loadBindTexture()
        {
            try
            {
                glBindTexturePtr = (glBindTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTexture"), typeof(glBindTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTexture'.");
            }
        }
        public static void glBindTexture(GLenum @target, GLuint @texture) => glBindTexturePtr(@target, @texture);

        internal delegate void glBlendColorFunc(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha);
        internal static glBlendColorFunc glBlendColorPtr;
        internal static void loadBlendColor()
        {
            try
            {
                glBlendColorPtr = (glBlendColorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendColor"), typeof(glBlendColorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendColor'.");
            }
        }
        public static void glBlendColor(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha) => glBlendColorPtr(@red, @green, @blue, @alpha);

        internal delegate void glBlendEquationFunc(GLenum @mode);
        internal static glBlendEquationFunc glBlendEquationPtr;
        internal static void loadBlendEquation()
        {
            try
            {
                glBlendEquationPtr = (glBlendEquationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquation"), typeof(glBlendEquationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquation'.");
            }
        }
        public static void glBlendEquation(GLenum @mode) => glBlendEquationPtr(@mode);

        internal delegate void glBlendEquationSeparateFunc(GLenum @modeRGB, GLenum @modeAlpha);
        internal static glBlendEquationSeparateFunc glBlendEquationSeparatePtr;
        internal static void loadBlendEquationSeparate()
        {
            try
            {
                glBlendEquationSeparatePtr = (glBlendEquationSeparateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationSeparate"), typeof(glBlendEquationSeparateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationSeparate'.");
            }
        }
        public static void glBlendEquationSeparate(GLenum @modeRGB, GLenum @modeAlpha) => glBlendEquationSeparatePtr(@modeRGB, @modeAlpha);

        internal delegate void glBlendFuncFunc(GLenum @sfactor, GLenum @dfactor);
        internal static glBlendFuncFunc glBlendFuncPtr;
        internal static void loadBlendFunc()
        {
            try
            {
                glBlendFuncPtr = (glBlendFuncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFunc"), typeof(glBlendFuncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFunc'.");
            }
        }
        public static void glBlendFunc(GLenum @sfactor, GLenum @dfactor) => glBlendFuncPtr(@sfactor, @dfactor);

        internal delegate void glBlendFuncSeparateFunc(GLenum @sfactorRGB, GLenum @dfactorRGB, GLenum @sfactorAlpha, GLenum @dfactorAlpha);
        internal static glBlendFuncSeparateFunc glBlendFuncSeparatePtr;
        internal static void loadBlendFuncSeparate()
        {
            try
            {
                glBlendFuncSeparatePtr = (glBlendFuncSeparateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFuncSeparate"), typeof(glBlendFuncSeparateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFuncSeparate'.");
            }
        }
        public static void glBlendFuncSeparate(GLenum @sfactorRGB, GLenum @dfactorRGB, GLenum @sfactorAlpha, GLenum @dfactorAlpha) => glBlendFuncSeparatePtr(@sfactorRGB, @dfactorRGB, @sfactorAlpha, @dfactorAlpha);

        internal delegate void glBufferDataFunc(GLenum @target, GLsizeiptr @size, const void * @data, GLenum @usage);
        internal static glBufferDataFunc glBufferDataPtr;
        internal static void loadBufferData()
        {
            try
            {
                glBufferDataPtr = (glBufferDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferData"), typeof(glBufferDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferData'.");
            }
        }
        public static void glBufferData(GLenum @target, GLsizeiptr @size, const void * @data, GLenum @usage) => glBufferDataPtr(@target, @size, @data, @usage);

        internal delegate void glBufferSubDataFunc(GLenum @target, GLintptr @offset, GLsizeiptr @size, const void * @data);
        internal static glBufferSubDataFunc glBufferSubDataPtr;
        internal static void loadBufferSubData()
        {
            try
            {
                glBufferSubDataPtr = (glBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferSubData"), typeof(glBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferSubData'.");
            }
        }
        public static void glBufferSubData(GLenum @target, GLintptr @offset, GLsizeiptr @size, const void * @data) => glBufferSubDataPtr(@target, @offset, @size, @data);

        internal delegate GLenum glCheckFramebufferStatusFunc(GLenum @target);
        internal static glCheckFramebufferStatusFunc glCheckFramebufferStatusPtr;
        internal static void loadCheckFramebufferStatus()
        {
            try
            {
                glCheckFramebufferStatusPtr = (glCheckFramebufferStatusFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCheckFramebufferStatus"), typeof(glCheckFramebufferStatusFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCheckFramebufferStatus'.");
            }
        }
        public static GLenum glCheckFramebufferStatus(GLenum @target) => glCheckFramebufferStatusPtr(@target);

        internal delegate void glClearFunc(GLbitfield @mask);
        internal static glClearFunc glClearPtr;
        internal static void loadClear()
        {
            try
            {
                glClearPtr = (glClearFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClear"), typeof(glClearFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClear'.");
            }
        }
        public static void glClear(GLbitfield @mask) => glClearPtr(@mask);

        internal delegate void glClearColorFunc(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha);
        internal static glClearColorFunc glClearColorPtr;
        internal static void loadClearColor()
        {
            try
            {
                glClearColorPtr = (glClearColorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearColor"), typeof(glClearColorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearColor'.");
            }
        }
        public static void glClearColor(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha) => glClearColorPtr(@red, @green, @blue, @alpha);

        internal delegate void glClearDepthfFunc(GLfloat @d);
        internal static glClearDepthfFunc glClearDepthfPtr;
        internal static void loadClearDepthf()
        {
            try
            {
                glClearDepthfPtr = (glClearDepthfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearDepthf"), typeof(glClearDepthfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearDepthf'.");
            }
        }
        public static void glClearDepthf(GLfloat @d) => glClearDepthfPtr(@d);

        internal delegate void glClearStencilFunc(GLint @s);
        internal static glClearStencilFunc glClearStencilPtr;
        internal static void loadClearStencil()
        {
            try
            {
                glClearStencilPtr = (glClearStencilFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearStencil"), typeof(glClearStencilFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearStencil'.");
            }
        }
        public static void glClearStencil(GLint @s) => glClearStencilPtr(@s);

        internal delegate void glColorMaskFunc(GLboolean @red, GLboolean @green, GLboolean @blue, GLboolean @alpha);
        internal static glColorMaskFunc glColorMaskPtr;
        internal static void loadColorMask()
        {
            try
            {
                glColorMaskPtr = (glColorMaskFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorMask"), typeof(glColorMaskFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorMask'.");
            }
        }
        public static void glColorMask(GLboolean @red, GLboolean @green, GLboolean @blue, GLboolean @alpha) => glColorMaskPtr(@red, @green, @blue, @alpha);

        internal delegate void glCompileShaderFunc(GLuint @shader);
        internal static glCompileShaderFunc glCompileShaderPtr;
        internal static void loadCompileShader()
        {
            try
            {
                glCompileShaderPtr = (glCompileShaderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompileShader"), typeof(glCompileShaderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompileShader'.");
            }
        }
        public static void glCompileShader(GLuint @shader) => glCompileShaderPtr(@shader);

        internal delegate void glCompressedTexImage2DFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexImage2DFunc glCompressedTexImage2DPtr;
        internal static void loadCompressedTexImage2D()
        {
            try
            {
                glCompressedTexImage2DPtr = (glCompressedTexImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexImage2D"), typeof(glCompressedTexImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexImage2D'.");
            }
        }
        public static void glCompressedTexImage2D(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLsizei @imageSize, const void * @data) => glCompressedTexImage2DPtr(@target, @level, @internalformat, @width, @height, @border, @imageSize, @data);

        internal delegate void glCompressedTexSubImage2DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexSubImage2DFunc glCompressedTexSubImage2DPtr;
        internal static void loadCompressedTexSubImage2D()
        {
            try
            {
                glCompressedTexSubImage2DPtr = (glCompressedTexSubImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexSubImage2D"), typeof(glCompressedTexSubImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexSubImage2D'.");
            }
        }
        public static void glCompressedTexSubImage2D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTexSubImage2DPtr(@target, @level, @xoffset, @yoffset, @width, @height, @format, @imageSize, @data);

        internal delegate void glCopyTexImage2DFunc(GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLint @border);
        internal static glCopyTexImage2DFunc glCopyTexImage2DPtr;
        internal static void loadCopyTexImage2D()
        {
            try
            {
                glCopyTexImage2DPtr = (glCopyTexImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexImage2D"), typeof(glCopyTexImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexImage2D'.");
            }
        }
        public static void glCopyTexImage2D(GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLint @border) => glCopyTexImage2DPtr(@target, @level, @internalformat, @x, @y, @width, @height, @border);

        internal delegate void glCopyTexSubImage2DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyTexSubImage2DFunc glCopyTexSubImage2DPtr;
        internal static void loadCopyTexSubImage2D()
        {
            try
            {
                glCopyTexSubImage2DPtr = (glCopyTexSubImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexSubImage2D"), typeof(glCopyTexSubImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexSubImage2D'.");
            }
        }
        public static void glCopyTexSubImage2D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyTexSubImage2DPtr(@target, @level, @xoffset, @yoffset, @x, @y, @width, @height);

        internal delegate GLuint glCreateProgramFunc();
        internal static glCreateProgramFunc glCreateProgramPtr;
        internal static void loadCreateProgram()
        {
            try
            {
                glCreateProgramPtr = (glCreateProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateProgram"), typeof(glCreateProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateProgram'.");
            }
        }
        public static GLuint glCreateProgram() => glCreateProgramPtr();

        internal delegate GLuint glCreateShaderFunc(GLenum @type);
        internal static glCreateShaderFunc glCreateShaderPtr;
        internal static void loadCreateShader()
        {
            try
            {
                glCreateShaderPtr = (glCreateShaderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateShader"), typeof(glCreateShaderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateShader'.");
            }
        }
        public static GLuint glCreateShader(GLenum @type) => glCreateShaderPtr(@type);

        internal delegate void glCullFaceFunc(GLenum @mode);
        internal static glCullFaceFunc glCullFacePtr;
        internal static void loadCullFace()
        {
            try
            {
                glCullFacePtr = (glCullFaceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCullFace"), typeof(glCullFaceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCullFace'.");
            }
        }
        public static void glCullFace(GLenum @mode) => glCullFacePtr(@mode);

        internal delegate void glDeleteBuffersFunc(GLsizei @n, const GLuint * @buffers);
        internal static glDeleteBuffersFunc glDeleteBuffersPtr;
        internal static void loadDeleteBuffers()
        {
            try
            {
                glDeleteBuffersPtr = (glDeleteBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteBuffers"), typeof(glDeleteBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteBuffers'.");
            }
        }
        public static void glDeleteBuffers(GLsizei @n, const GLuint * @buffers) => glDeleteBuffersPtr(@n, @buffers);

        internal delegate void glDeleteFramebuffersFunc(GLsizei @n, const GLuint * @framebuffers);
        internal static glDeleteFramebuffersFunc glDeleteFramebuffersPtr;
        internal static void loadDeleteFramebuffers()
        {
            try
            {
                glDeleteFramebuffersPtr = (glDeleteFramebuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteFramebuffers"), typeof(glDeleteFramebuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteFramebuffers'.");
            }
        }
        public static void glDeleteFramebuffers(GLsizei @n, const GLuint * @framebuffers) => glDeleteFramebuffersPtr(@n, @framebuffers);

        internal delegate void glDeleteProgramFunc(GLuint @program);
        internal static glDeleteProgramFunc glDeleteProgramPtr;
        internal static void loadDeleteProgram()
        {
            try
            {
                glDeleteProgramPtr = (glDeleteProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteProgram"), typeof(glDeleteProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteProgram'.");
            }
        }
        public static void glDeleteProgram(GLuint @program) => glDeleteProgramPtr(@program);

        internal delegate void glDeleteRenderbuffersFunc(GLsizei @n, const GLuint * @renderbuffers);
        internal static glDeleteRenderbuffersFunc glDeleteRenderbuffersPtr;
        internal static void loadDeleteRenderbuffers()
        {
            try
            {
                glDeleteRenderbuffersPtr = (glDeleteRenderbuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteRenderbuffers"), typeof(glDeleteRenderbuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteRenderbuffers'.");
            }
        }
        public static void glDeleteRenderbuffers(GLsizei @n, const GLuint * @renderbuffers) => glDeleteRenderbuffersPtr(@n, @renderbuffers);

        internal delegate void glDeleteShaderFunc(GLuint @shader);
        internal static glDeleteShaderFunc glDeleteShaderPtr;
        internal static void loadDeleteShader()
        {
            try
            {
                glDeleteShaderPtr = (glDeleteShaderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteShader"), typeof(glDeleteShaderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteShader'.");
            }
        }
        public static void glDeleteShader(GLuint @shader) => glDeleteShaderPtr(@shader);

        internal delegate void glDeleteTexturesFunc(GLsizei @n, const GLuint * @textures);
        internal static glDeleteTexturesFunc glDeleteTexturesPtr;
        internal static void loadDeleteTextures()
        {
            try
            {
                glDeleteTexturesPtr = (glDeleteTexturesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteTextures"), typeof(glDeleteTexturesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteTextures'.");
            }
        }
        public static void glDeleteTextures(GLsizei @n, const GLuint * @textures) => glDeleteTexturesPtr(@n, @textures);

        internal delegate void glDepthFuncFunc(GLenum @func);
        internal static glDepthFuncFunc glDepthFuncPtr;
        internal static void loadDepthFunc()
        {
            try
            {
                glDepthFuncPtr = (glDepthFuncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthFunc"), typeof(glDepthFuncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthFunc'.");
            }
        }
        public static void glDepthFunc(GLenum @func) => glDepthFuncPtr(@func);

        internal delegate void glDepthMaskFunc(GLboolean @flag);
        internal static glDepthMaskFunc glDepthMaskPtr;
        internal static void loadDepthMask()
        {
            try
            {
                glDepthMaskPtr = (glDepthMaskFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthMask"), typeof(glDepthMaskFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthMask'.");
            }
        }
        public static void glDepthMask(GLboolean @flag) => glDepthMaskPtr(@flag);

        internal delegate void glDepthRangefFunc(GLfloat @n, GLfloat @f);
        internal static glDepthRangefFunc glDepthRangefPtr;
        internal static void loadDepthRangef()
        {
            try
            {
                glDepthRangefPtr = (glDepthRangefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangef"), typeof(glDepthRangefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangef'.");
            }
        }
        public static void glDepthRangef(GLfloat @n, GLfloat @f) => glDepthRangefPtr(@n, @f);

        internal delegate void glDetachShaderFunc(GLuint @program, GLuint @shader);
        internal static glDetachShaderFunc glDetachShaderPtr;
        internal static void loadDetachShader()
        {
            try
            {
                glDetachShaderPtr = (glDetachShaderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDetachShader"), typeof(glDetachShaderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDetachShader'.");
            }
        }
        public static void glDetachShader(GLuint @program, GLuint @shader) => glDetachShaderPtr(@program, @shader);

        internal delegate void glDisableFunc(GLenum @cap);
        internal static glDisableFunc glDisablePtr;
        internal static void loadDisable()
        {
            try
            {
                glDisablePtr = (glDisableFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisable"), typeof(glDisableFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisable'.");
            }
        }
        public static void glDisable(GLenum @cap) => glDisablePtr(@cap);

        internal delegate void glDisableVertexAttribArrayFunc(GLuint @index);
        internal static glDisableVertexAttribArrayFunc glDisableVertexAttribArrayPtr;
        internal static void loadDisableVertexAttribArray()
        {
            try
            {
                glDisableVertexAttribArrayPtr = (glDisableVertexAttribArrayFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableVertexAttribArray"), typeof(glDisableVertexAttribArrayFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableVertexAttribArray'.");
            }
        }
        public static void glDisableVertexAttribArray(GLuint @index) => glDisableVertexAttribArrayPtr(@index);

        internal delegate void glDrawArraysFunc(GLenum @mode, GLint @first, GLsizei @count);
        internal static glDrawArraysFunc glDrawArraysPtr;
        internal static void loadDrawArrays()
        {
            try
            {
                glDrawArraysPtr = (glDrawArraysFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArrays"), typeof(glDrawArraysFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArrays'.");
            }
        }
        public static void glDrawArrays(GLenum @mode, GLint @first, GLsizei @count) => glDrawArraysPtr(@mode, @first, @count);

        internal delegate void glDrawElementsFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices);
        internal static glDrawElementsFunc glDrawElementsPtr;
        internal static void loadDrawElements()
        {
            try
            {
                glDrawElementsPtr = (glDrawElementsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElements"), typeof(glDrawElementsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElements'.");
            }
        }
        public static void glDrawElements(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices) => glDrawElementsPtr(@mode, @count, @type, @indices);

        internal delegate void glEnableFunc(GLenum @cap);
        internal static glEnableFunc glEnablePtr;
        internal static void loadEnable()
        {
            try
            {
                glEnablePtr = (glEnableFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnable"), typeof(glEnableFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnable'.");
            }
        }
        public static void glEnable(GLenum @cap) => glEnablePtr(@cap);

        internal delegate void glEnableVertexAttribArrayFunc(GLuint @index);
        internal static glEnableVertexAttribArrayFunc glEnableVertexAttribArrayPtr;
        internal static void loadEnableVertexAttribArray()
        {
            try
            {
                glEnableVertexAttribArrayPtr = (glEnableVertexAttribArrayFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableVertexAttribArray"), typeof(glEnableVertexAttribArrayFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableVertexAttribArray'.");
            }
        }
        public static void glEnableVertexAttribArray(GLuint @index) => glEnableVertexAttribArrayPtr(@index);

        internal delegate void glFinishFunc();
        internal static glFinishFunc glFinishPtr;
        internal static void loadFinish()
        {
            try
            {
                glFinishPtr = (glFinishFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFinish"), typeof(glFinishFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFinish'.");
            }
        }
        public static void glFinish() => glFinishPtr();

        internal delegate void glFlushFunc();
        internal static glFlushFunc glFlushPtr;
        internal static void loadFlush()
        {
            try
            {
                glFlushPtr = (glFlushFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlush"), typeof(glFlushFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlush'.");
            }
        }
        public static void glFlush() => glFlushPtr();

        internal delegate void glFramebufferRenderbufferFunc(GLenum @target, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer);
        internal static glFramebufferRenderbufferFunc glFramebufferRenderbufferPtr;
        internal static void loadFramebufferRenderbuffer()
        {
            try
            {
                glFramebufferRenderbufferPtr = (glFramebufferRenderbufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferRenderbuffer"), typeof(glFramebufferRenderbufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferRenderbuffer'.");
            }
        }
        public static void glFramebufferRenderbuffer(GLenum @target, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer) => glFramebufferRenderbufferPtr(@target, @attachment, @renderbuffertarget, @renderbuffer);

        internal delegate void glFramebufferTexture2DFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level);
        internal static glFramebufferTexture2DFunc glFramebufferTexture2DPtr;
        internal static void loadFramebufferTexture2D()
        {
            try
            {
                glFramebufferTexture2DPtr = (glFramebufferTexture2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture2D"), typeof(glFramebufferTexture2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture2D'.");
            }
        }
        public static void glFramebufferTexture2D(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level) => glFramebufferTexture2DPtr(@target, @attachment, @textarget, @texture, @level);

        internal delegate void glFrontFaceFunc(GLenum @mode);
        internal static glFrontFaceFunc glFrontFacePtr;
        internal static void loadFrontFace()
        {
            try
            {
                glFrontFacePtr = (glFrontFaceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFrontFace"), typeof(glFrontFaceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFrontFace'.");
            }
        }
        public static void glFrontFace(GLenum @mode) => glFrontFacePtr(@mode);

        internal delegate void glGenBuffersFunc(GLsizei @n, GLuint * @buffers);
        internal static glGenBuffersFunc glGenBuffersPtr;
        internal static void loadGenBuffers()
        {
            try
            {
                glGenBuffersPtr = (glGenBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenBuffers"), typeof(glGenBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenBuffers'.");
            }
        }
        public static void glGenBuffers(GLsizei @n, GLuint * @buffers) => glGenBuffersPtr(@n, @buffers);

        internal delegate void glGenerateMipmapFunc(GLenum @target);
        internal static glGenerateMipmapFunc glGenerateMipmapPtr;
        internal static void loadGenerateMipmap()
        {
            try
            {
                glGenerateMipmapPtr = (glGenerateMipmapFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenerateMipmap"), typeof(glGenerateMipmapFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenerateMipmap'.");
            }
        }
        public static void glGenerateMipmap(GLenum @target) => glGenerateMipmapPtr(@target);

        internal delegate void glGenFramebuffersFunc(GLsizei @n, GLuint * @framebuffers);
        internal static glGenFramebuffersFunc glGenFramebuffersPtr;
        internal static void loadGenFramebuffers()
        {
            try
            {
                glGenFramebuffersPtr = (glGenFramebuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenFramebuffers"), typeof(glGenFramebuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenFramebuffers'.");
            }
        }
        public static void glGenFramebuffers(GLsizei @n, GLuint * @framebuffers) => glGenFramebuffersPtr(@n, @framebuffers);

        internal delegate void glGenRenderbuffersFunc(GLsizei @n, GLuint * @renderbuffers);
        internal static glGenRenderbuffersFunc glGenRenderbuffersPtr;
        internal static void loadGenRenderbuffers()
        {
            try
            {
                glGenRenderbuffersPtr = (glGenRenderbuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenRenderbuffers"), typeof(glGenRenderbuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenRenderbuffers'.");
            }
        }
        public static void glGenRenderbuffers(GLsizei @n, GLuint * @renderbuffers) => glGenRenderbuffersPtr(@n, @renderbuffers);

        internal delegate void glGenTexturesFunc(GLsizei @n, GLuint * @textures);
        internal static glGenTexturesFunc glGenTexturesPtr;
        internal static void loadGenTextures()
        {
            try
            {
                glGenTexturesPtr = (glGenTexturesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenTextures"), typeof(glGenTexturesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenTextures'.");
            }
        }
        public static void glGenTextures(GLsizei @n, GLuint * @textures) => glGenTexturesPtr(@n, @textures);

        internal delegate void glGetActiveAttribFunc(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLint * @size, GLenum * @type, GLchar * @name);
        internal static glGetActiveAttribFunc glGetActiveAttribPtr;
        internal static void loadGetActiveAttrib()
        {
            try
            {
                glGetActiveAttribPtr = (glGetActiveAttribFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveAttrib"), typeof(glGetActiveAttribFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveAttrib'.");
            }
        }
        public static void glGetActiveAttrib(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLint * @size, GLenum * @type, GLchar * @name) => glGetActiveAttribPtr(@program, @index, @bufSize, @length, @size, @type, @name);

        internal delegate void glGetActiveUniformFunc(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLint * @size, GLenum * @type, GLchar * @name);
        internal static glGetActiveUniformFunc glGetActiveUniformPtr;
        internal static void loadGetActiveUniform()
        {
            try
            {
                glGetActiveUniformPtr = (glGetActiveUniformFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveUniform"), typeof(glGetActiveUniformFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveUniform'.");
            }
        }
        public static void glGetActiveUniform(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLint * @size, GLenum * @type, GLchar * @name) => glGetActiveUniformPtr(@program, @index, @bufSize, @length, @size, @type, @name);

        internal delegate void glGetAttachedShadersFunc(GLuint @program, GLsizei @maxCount, GLsizei * @count, GLuint * @shaders);
        internal static glGetAttachedShadersFunc glGetAttachedShadersPtr;
        internal static void loadGetAttachedShaders()
        {
            try
            {
                glGetAttachedShadersPtr = (glGetAttachedShadersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetAttachedShaders"), typeof(glGetAttachedShadersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetAttachedShaders'.");
            }
        }
        public static void glGetAttachedShaders(GLuint @program, GLsizei @maxCount, GLsizei * @count, GLuint * @shaders) => glGetAttachedShadersPtr(@program, @maxCount, @count, @shaders);

        internal delegate GLint glGetAttribLocationFunc(GLuint @program, const GLchar * @name);
        internal static glGetAttribLocationFunc glGetAttribLocationPtr;
        internal static void loadGetAttribLocation()
        {
            try
            {
                glGetAttribLocationPtr = (glGetAttribLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetAttribLocation"), typeof(glGetAttribLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetAttribLocation'.");
            }
        }
        public static GLint glGetAttribLocation(GLuint @program, const GLchar * @name) => glGetAttribLocationPtr(@program, @name);

        internal delegate void glGetBooleanvFunc(GLenum @pname, GLboolean * @data);
        internal static glGetBooleanvFunc glGetBooleanvPtr;
        internal static void loadGetBooleanv()
        {
            try
            {
                glGetBooleanvPtr = (glGetBooleanvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBooleanv"), typeof(glGetBooleanvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBooleanv'.");
            }
        }
        public static void glGetBooleanv(GLenum @pname, GLboolean * @data) => glGetBooleanvPtr(@pname, @data);

        internal delegate void glGetBufferParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetBufferParameterivFunc glGetBufferParameterivPtr;
        internal static void loadGetBufferParameteriv()
        {
            try
            {
                glGetBufferParameterivPtr = (glGetBufferParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferParameteriv"), typeof(glGetBufferParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferParameteriv'.");
            }
        }
        public static void glGetBufferParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetBufferParameterivPtr(@target, @pname, @params);

        internal delegate GLenum glGetErrorFunc();
        internal static glGetErrorFunc glGetErrorPtr;
        internal static void loadGetError()
        {
            try
            {
                glGetErrorPtr = (glGetErrorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetError"), typeof(glGetErrorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetError'.");
            }
        }
        public static GLenum glGetError() => glGetErrorPtr();

        internal delegate void glGetFloatvFunc(GLenum @pname, GLfloat * @data);
        internal static glGetFloatvFunc glGetFloatvPtr;
        internal static void loadGetFloatv()
        {
            try
            {
                glGetFloatvPtr = (glGetFloatvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFloatv"), typeof(glGetFloatvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFloatv'.");
            }
        }
        public static void glGetFloatv(GLenum @pname, GLfloat * @data) => glGetFloatvPtr(@pname, @data);

        internal delegate void glGetFramebufferAttachmentParameterivFunc(GLenum @target, GLenum @attachment, GLenum @pname, GLint * @params);
        internal static glGetFramebufferAttachmentParameterivFunc glGetFramebufferAttachmentParameterivPtr;
        internal static void loadGetFramebufferAttachmentParameteriv()
        {
            try
            {
                glGetFramebufferAttachmentParameterivPtr = (glGetFramebufferAttachmentParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFramebufferAttachmentParameteriv"), typeof(glGetFramebufferAttachmentParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFramebufferAttachmentParameteriv'.");
            }
        }
        public static void glGetFramebufferAttachmentParameteriv(GLenum @target, GLenum @attachment, GLenum @pname, GLint * @params) => glGetFramebufferAttachmentParameterivPtr(@target, @attachment, @pname, @params);

        internal delegate void glGetIntegervFunc(GLenum @pname, GLint * @data);
        internal static glGetIntegervFunc glGetIntegervPtr;
        internal static void loadGetIntegerv()
        {
            try
            {
                glGetIntegervPtr = (glGetIntegervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetIntegerv"), typeof(glGetIntegervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetIntegerv'.");
            }
        }
        public static void glGetIntegerv(GLenum @pname, GLint * @data) => glGetIntegervPtr(@pname, @data);

        internal delegate void glGetProgramivFunc(GLuint @program, GLenum @pname, GLint * @params);
        internal static glGetProgramivFunc glGetProgramivPtr;
        internal static void loadGetProgramiv()
        {
            try
            {
                glGetProgramivPtr = (glGetProgramivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramiv"), typeof(glGetProgramivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramiv'.");
            }
        }
        public static void glGetProgramiv(GLuint @program, GLenum @pname, GLint * @params) => glGetProgramivPtr(@program, @pname, @params);

        internal delegate void glGetProgramInfoLogFunc(GLuint @program, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog);
        internal static glGetProgramInfoLogFunc glGetProgramInfoLogPtr;
        internal static void loadGetProgramInfoLog()
        {
            try
            {
                glGetProgramInfoLogPtr = (glGetProgramInfoLogFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramInfoLog"), typeof(glGetProgramInfoLogFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramInfoLog'.");
            }
        }
        public static void glGetProgramInfoLog(GLuint @program, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog) => glGetProgramInfoLogPtr(@program, @bufSize, @length, @infoLog);

        internal delegate void glGetRenderbufferParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetRenderbufferParameterivFunc glGetRenderbufferParameterivPtr;
        internal static void loadGetRenderbufferParameteriv()
        {
            try
            {
                glGetRenderbufferParameterivPtr = (glGetRenderbufferParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetRenderbufferParameteriv"), typeof(glGetRenderbufferParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetRenderbufferParameteriv'.");
            }
        }
        public static void glGetRenderbufferParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetRenderbufferParameterivPtr(@target, @pname, @params);

        internal delegate void glGetShaderivFunc(GLuint @shader, GLenum @pname, GLint * @params);
        internal static glGetShaderivFunc glGetShaderivPtr;
        internal static void loadGetShaderiv()
        {
            try
            {
                glGetShaderivPtr = (glGetShaderivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetShaderiv"), typeof(glGetShaderivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetShaderiv'.");
            }
        }
        public static void glGetShaderiv(GLuint @shader, GLenum @pname, GLint * @params) => glGetShaderivPtr(@shader, @pname, @params);

        internal delegate void glGetShaderInfoLogFunc(GLuint @shader, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog);
        internal static glGetShaderInfoLogFunc glGetShaderInfoLogPtr;
        internal static void loadGetShaderInfoLog()
        {
            try
            {
                glGetShaderInfoLogPtr = (glGetShaderInfoLogFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetShaderInfoLog"), typeof(glGetShaderInfoLogFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetShaderInfoLog'.");
            }
        }
        public static void glGetShaderInfoLog(GLuint @shader, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog) => glGetShaderInfoLogPtr(@shader, @bufSize, @length, @infoLog);

        internal delegate void glGetShaderPrecisionFormatFunc(GLenum @shadertype, GLenum @precisiontype, GLint * @range, GLint * @precision);
        internal static glGetShaderPrecisionFormatFunc glGetShaderPrecisionFormatPtr;
        internal static void loadGetShaderPrecisionFormat()
        {
            try
            {
                glGetShaderPrecisionFormatPtr = (glGetShaderPrecisionFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetShaderPrecisionFormat"), typeof(glGetShaderPrecisionFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetShaderPrecisionFormat'.");
            }
        }
        public static void glGetShaderPrecisionFormat(GLenum @shadertype, GLenum @precisiontype, GLint * @range, GLint * @precision) => glGetShaderPrecisionFormatPtr(@shadertype, @precisiontype, @range, @precision);

        internal delegate void glGetShaderSourceFunc(GLuint @shader, GLsizei @bufSize, GLsizei * @length, GLchar * @source);
        internal static glGetShaderSourceFunc glGetShaderSourcePtr;
        internal static void loadGetShaderSource()
        {
            try
            {
                glGetShaderSourcePtr = (glGetShaderSourceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetShaderSource"), typeof(glGetShaderSourceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetShaderSource'.");
            }
        }
        public static void glGetShaderSource(GLuint @shader, GLsizei @bufSize, GLsizei * @length, GLchar * @source) => glGetShaderSourcePtr(@shader, @bufSize, @length, @source);

        internal delegate constGLubyte* glGetStringFunc(GLenum @name);
        internal static glGetStringFunc glGetStringPtr;
        internal static void loadGetString()
        {
            try
            {
                glGetStringPtr = (glGetStringFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetString"), typeof(glGetStringFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetString'.");
            }
        }
        public static constGLubyte* glGetString(GLenum @name) => glGetStringPtr(@name);

        internal delegate void glGetTexParameterfvFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetTexParameterfvFunc glGetTexParameterfvPtr;
        internal static void loadGetTexParameterfv()
        {
            try
            {
                glGetTexParameterfvPtr = (glGetTexParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterfv"), typeof(glGetTexParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterfv'.");
            }
        }
        public static void glGetTexParameterfv(GLenum @target, GLenum @pname, GLfloat * @params) => glGetTexParameterfvPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetTexParameterivFunc glGetTexParameterivPtr;
        internal static void loadGetTexParameteriv()
        {
            try
            {
                glGetTexParameterivPtr = (glGetTexParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameteriv"), typeof(glGetTexParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameteriv'.");
            }
        }
        public static void glGetTexParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetTexParameterivPtr(@target, @pname, @params);

        internal delegate void glGetUniformfvFunc(GLuint @program, GLint @location, GLfloat * @params);
        internal static glGetUniformfvFunc glGetUniformfvPtr;
        internal static void loadGetUniformfv()
        {
            try
            {
                glGetUniformfvPtr = (glGetUniformfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformfv"), typeof(glGetUniformfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformfv'.");
            }
        }
        public static void glGetUniformfv(GLuint @program, GLint @location, GLfloat * @params) => glGetUniformfvPtr(@program, @location, @params);

        internal delegate void glGetUniformivFunc(GLuint @program, GLint @location, GLint * @params);
        internal static glGetUniformivFunc glGetUniformivPtr;
        internal static void loadGetUniformiv()
        {
            try
            {
                glGetUniformivPtr = (glGetUniformivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformiv"), typeof(glGetUniformivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformiv'.");
            }
        }
        public static void glGetUniformiv(GLuint @program, GLint @location, GLint * @params) => glGetUniformivPtr(@program, @location, @params);

        internal delegate GLint glGetUniformLocationFunc(GLuint @program, const GLchar * @name);
        internal static glGetUniformLocationFunc glGetUniformLocationPtr;
        internal static void loadGetUniformLocation()
        {
            try
            {
                glGetUniformLocationPtr = (glGetUniformLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformLocation"), typeof(glGetUniformLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformLocation'.");
            }
        }
        public static GLint glGetUniformLocation(GLuint @program, const GLchar * @name) => glGetUniformLocationPtr(@program, @name);

        internal delegate void glGetVertexAttribfvFunc(GLuint @index, GLenum @pname, GLfloat * @params);
        internal static glGetVertexAttribfvFunc glGetVertexAttribfvPtr;
        internal static void loadGetVertexAttribfv()
        {
            try
            {
                glGetVertexAttribfvPtr = (glGetVertexAttribfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribfv"), typeof(glGetVertexAttribfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribfv'.");
            }
        }
        public static void glGetVertexAttribfv(GLuint @index, GLenum @pname, GLfloat * @params) => glGetVertexAttribfvPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribivFunc(GLuint @index, GLenum @pname, GLint * @params);
        internal static glGetVertexAttribivFunc glGetVertexAttribivPtr;
        internal static void loadGetVertexAttribiv()
        {
            try
            {
                glGetVertexAttribivPtr = (glGetVertexAttribivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribiv"), typeof(glGetVertexAttribivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribiv'.");
            }
        }
        public static void glGetVertexAttribiv(GLuint @index, GLenum @pname, GLint * @params) => glGetVertexAttribivPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribPointervFunc(GLuint @index, GLenum @pname, void ** @pointer);
        internal static glGetVertexAttribPointervFunc glGetVertexAttribPointervPtr;
        internal static void loadGetVertexAttribPointerv()
        {
            try
            {
                glGetVertexAttribPointervPtr = (glGetVertexAttribPointervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribPointerv"), typeof(glGetVertexAttribPointervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribPointerv'.");
            }
        }
        public static void glGetVertexAttribPointerv(GLuint @index, GLenum @pname, void ** @pointer) => glGetVertexAttribPointervPtr(@index, @pname, @pointer);

        internal delegate void glHintFunc(GLenum @target, GLenum @mode);
        internal static glHintFunc glHintPtr;
        internal static void loadHint()
        {
            try
            {
                glHintPtr = (glHintFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glHint"), typeof(glHintFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glHint'.");
            }
        }
        public static void glHint(GLenum @target, GLenum @mode) => glHintPtr(@target, @mode);

        internal delegate GLboolean glIsBufferFunc(GLuint @buffer);
        internal static glIsBufferFunc glIsBufferPtr;
        internal static void loadIsBuffer()
        {
            try
            {
                glIsBufferPtr = (glIsBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsBuffer"), typeof(glIsBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsBuffer'.");
            }
        }
        public static GLboolean glIsBuffer(GLuint @buffer) => glIsBufferPtr(@buffer);

        internal delegate GLboolean glIsEnabledFunc(GLenum @cap);
        internal static glIsEnabledFunc glIsEnabledPtr;
        internal static void loadIsEnabled()
        {
            try
            {
                glIsEnabledPtr = (glIsEnabledFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsEnabled"), typeof(glIsEnabledFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsEnabled'.");
            }
        }
        public static GLboolean glIsEnabled(GLenum @cap) => glIsEnabledPtr(@cap);

        internal delegate GLboolean glIsFramebufferFunc(GLuint @framebuffer);
        internal static glIsFramebufferFunc glIsFramebufferPtr;
        internal static void loadIsFramebuffer()
        {
            try
            {
                glIsFramebufferPtr = (glIsFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsFramebuffer"), typeof(glIsFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsFramebuffer'.");
            }
        }
        public static GLboolean glIsFramebuffer(GLuint @framebuffer) => glIsFramebufferPtr(@framebuffer);

        internal delegate GLboolean glIsProgramFunc(GLuint @program);
        internal static glIsProgramFunc glIsProgramPtr;
        internal static void loadIsProgram()
        {
            try
            {
                glIsProgramPtr = (glIsProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsProgram"), typeof(glIsProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsProgram'.");
            }
        }
        public static GLboolean glIsProgram(GLuint @program) => glIsProgramPtr(@program);

        internal delegate GLboolean glIsRenderbufferFunc(GLuint @renderbuffer);
        internal static glIsRenderbufferFunc glIsRenderbufferPtr;
        internal static void loadIsRenderbuffer()
        {
            try
            {
                glIsRenderbufferPtr = (glIsRenderbufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsRenderbuffer"), typeof(glIsRenderbufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsRenderbuffer'.");
            }
        }
        public static GLboolean glIsRenderbuffer(GLuint @renderbuffer) => glIsRenderbufferPtr(@renderbuffer);

        internal delegate GLboolean glIsShaderFunc(GLuint @shader);
        internal static glIsShaderFunc glIsShaderPtr;
        internal static void loadIsShader()
        {
            try
            {
                glIsShaderPtr = (glIsShaderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsShader"), typeof(glIsShaderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsShader'.");
            }
        }
        public static GLboolean glIsShader(GLuint @shader) => glIsShaderPtr(@shader);

        internal delegate GLboolean glIsTextureFunc(GLuint @texture);
        internal static glIsTextureFunc glIsTexturePtr;
        internal static void loadIsTexture()
        {
            try
            {
                glIsTexturePtr = (glIsTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsTexture"), typeof(glIsTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsTexture'.");
            }
        }
        public static GLboolean glIsTexture(GLuint @texture) => glIsTexturePtr(@texture);

        internal delegate void glLineWidthFunc(GLfloat @width);
        internal static glLineWidthFunc glLineWidthPtr;
        internal static void loadLineWidth()
        {
            try
            {
                glLineWidthPtr = (glLineWidthFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLineWidth"), typeof(glLineWidthFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLineWidth'.");
            }
        }
        public static void glLineWidth(GLfloat @width) => glLineWidthPtr(@width);

        internal delegate void glLinkProgramFunc(GLuint @program);
        internal static glLinkProgramFunc glLinkProgramPtr;
        internal static void loadLinkProgram()
        {
            try
            {
                glLinkProgramPtr = (glLinkProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLinkProgram"), typeof(glLinkProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLinkProgram'.");
            }
        }
        public static void glLinkProgram(GLuint @program) => glLinkProgramPtr(@program);

        internal delegate void glPixelStoreiFunc(GLenum @pname, GLint @param);
        internal static glPixelStoreiFunc glPixelStoreiPtr;
        internal static void loadPixelStorei()
        {
            try
            {
                glPixelStoreiPtr = (glPixelStoreiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelStorei"), typeof(glPixelStoreiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelStorei'.");
            }
        }
        public static void glPixelStorei(GLenum @pname, GLint @param) => glPixelStoreiPtr(@pname, @param);

        internal delegate void glPolygonOffsetFunc(GLfloat @factor, GLfloat @units);
        internal static glPolygonOffsetFunc glPolygonOffsetPtr;
        internal static void loadPolygonOffset()
        {
            try
            {
                glPolygonOffsetPtr = (glPolygonOffsetFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPolygonOffset"), typeof(glPolygonOffsetFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPolygonOffset'.");
            }
        }
        public static void glPolygonOffset(GLfloat @factor, GLfloat @units) => glPolygonOffsetPtr(@factor, @units);

        internal delegate void glReadPixelsFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, void * @pixels);
        internal static glReadPixelsFunc glReadPixelsPtr;
        internal static void loadReadPixels()
        {
            try
            {
                glReadPixelsPtr = (glReadPixelsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadPixels"), typeof(glReadPixelsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadPixels'.");
            }
        }
        public static void glReadPixels(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, void * @pixels) => glReadPixelsPtr(@x, @y, @width, @height, @format, @type, @pixels);

        internal delegate void glReleaseShaderCompilerFunc();
        internal static glReleaseShaderCompilerFunc glReleaseShaderCompilerPtr;
        internal static void loadReleaseShaderCompiler()
        {
            try
            {
                glReleaseShaderCompilerPtr = (glReleaseShaderCompilerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReleaseShaderCompiler"), typeof(glReleaseShaderCompilerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReleaseShaderCompiler'.");
            }
        }
        public static void glReleaseShaderCompiler() => glReleaseShaderCompilerPtr();

        internal delegate void glRenderbufferStorageFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageFunc glRenderbufferStoragePtr;
        internal static void loadRenderbufferStorage()
        {
            try
            {
                glRenderbufferStoragePtr = (glRenderbufferStorageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorage"), typeof(glRenderbufferStorageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorage'.");
            }
        }
        public static void glRenderbufferStorage(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStoragePtr(@target, @internalformat, @width, @height);

        internal delegate void glSampleCoverageFunc(GLfloat @value, GLboolean @invert);
        internal static glSampleCoverageFunc glSampleCoveragePtr;
        internal static void loadSampleCoverage()
        {
            try
            {
                glSampleCoveragePtr = (glSampleCoverageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleCoverage"), typeof(glSampleCoverageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleCoverage'.");
            }
        }
        public static void glSampleCoverage(GLfloat @value, GLboolean @invert) => glSampleCoveragePtr(@value, @invert);

        internal delegate void glScissorFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glScissorFunc glScissorPtr;
        internal static void loadScissor()
        {
            try
            {
                glScissorPtr = (glScissorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScissor"), typeof(glScissorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScissor'.");
            }
        }
        public static void glScissor(GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glScissorPtr(@x, @y, @width, @height);

        internal delegate void glShaderBinaryFunc(GLsizei @count, const GLuint * @shaders, GLenum @binaryformat, const void * @binary, GLsizei @length);
        internal static glShaderBinaryFunc glShaderBinaryPtr;
        internal static void loadShaderBinary()
        {
            try
            {
                glShaderBinaryPtr = (glShaderBinaryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShaderBinary"), typeof(glShaderBinaryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShaderBinary'.");
            }
        }
        public static void glShaderBinary(GLsizei @count, const GLuint * @shaders, GLenum @binaryformat, const void * @binary, GLsizei @length) => glShaderBinaryPtr(@count, @shaders, @binaryformat, @binary, @length);

        internal delegate void glShaderSourceFunc(GLuint @shader, GLsizei @count, const GLchar *const* @string, const GLint * @length);
        internal static glShaderSourceFunc glShaderSourcePtr;
        internal static void loadShaderSource()
        {
            try
            {
                glShaderSourcePtr = (glShaderSourceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShaderSource"), typeof(glShaderSourceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShaderSource'.");
            }
        }
        public static void glShaderSource(GLuint @shader, GLsizei @count, const GLchar *const* @string, const GLint * @length) => glShaderSourcePtr(@shader, @count, @string, @length);

        internal delegate void glStencilFuncFunc(GLenum @func, GLint @ref, GLuint @mask);
        internal static glStencilFuncFunc glStencilFuncPtr;
        internal static void loadStencilFunc()
        {
            try
            {
                glStencilFuncPtr = (glStencilFuncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilFunc"), typeof(glStencilFuncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilFunc'.");
            }
        }
        public static void glStencilFunc(GLenum @func, GLint @ref, GLuint @mask) => glStencilFuncPtr(@func, @ref, @mask);

        internal delegate void glStencilFuncSeparateFunc(GLenum @face, GLenum @func, GLint @ref, GLuint @mask);
        internal static glStencilFuncSeparateFunc glStencilFuncSeparatePtr;
        internal static void loadStencilFuncSeparate()
        {
            try
            {
                glStencilFuncSeparatePtr = (glStencilFuncSeparateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilFuncSeparate"), typeof(glStencilFuncSeparateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilFuncSeparate'.");
            }
        }
        public static void glStencilFuncSeparate(GLenum @face, GLenum @func, GLint @ref, GLuint @mask) => glStencilFuncSeparatePtr(@face, @func, @ref, @mask);

        internal delegate void glStencilMaskFunc(GLuint @mask);
        internal static glStencilMaskFunc glStencilMaskPtr;
        internal static void loadStencilMask()
        {
            try
            {
                glStencilMaskPtr = (glStencilMaskFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilMask"), typeof(glStencilMaskFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilMask'.");
            }
        }
        public static void glStencilMask(GLuint @mask) => glStencilMaskPtr(@mask);

        internal delegate void glStencilMaskSeparateFunc(GLenum @face, GLuint @mask);
        internal static glStencilMaskSeparateFunc glStencilMaskSeparatePtr;
        internal static void loadStencilMaskSeparate()
        {
            try
            {
                glStencilMaskSeparatePtr = (glStencilMaskSeparateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilMaskSeparate"), typeof(glStencilMaskSeparateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilMaskSeparate'.");
            }
        }
        public static void glStencilMaskSeparate(GLenum @face, GLuint @mask) => glStencilMaskSeparatePtr(@face, @mask);

        internal delegate void glStencilOpFunc(GLenum @fail, GLenum @zfail, GLenum @zpass);
        internal static glStencilOpFunc glStencilOpPtr;
        internal static void loadStencilOp()
        {
            try
            {
                glStencilOpPtr = (glStencilOpFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilOp"), typeof(glStencilOpFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilOp'.");
            }
        }
        public static void glStencilOp(GLenum @fail, GLenum @zfail, GLenum @zpass) => glStencilOpPtr(@fail, @zfail, @zpass);

        internal delegate void glStencilOpSeparateFunc(GLenum @face, GLenum @sfail, GLenum @dpfail, GLenum @dppass);
        internal static glStencilOpSeparateFunc glStencilOpSeparatePtr;
        internal static void loadStencilOpSeparate()
        {
            try
            {
                glStencilOpSeparatePtr = (glStencilOpSeparateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilOpSeparate"), typeof(glStencilOpSeparateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilOpSeparate'.");
            }
        }
        public static void glStencilOpSeparate(GLenum @face, GLenum @sfail, GLenum @dpfail, GLenum @dppass) => glStencilOpSeparatePtr(@face, @sfail, @dpfail, @dppass);

        internal delegate void glTexImage2DFunc(GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexImage2DFunc glTexImage2DPtr;
        internal static void loadTexImage2D()
        {
            try
            {
                glTexImage2DPtr = (glTexImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage2D"), typeof(glTexImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage2D'.");
            }
        }
        public static void glTexImage2D(GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glTexImage2DPtr(@target, @level, @internalformat, @width, @height, @border, @format, @type, @pixels);

        internal delegate void glTexParameterfFunc(GLenum @target, GLenum @pname, GLfloat @param);
        internal static glTexParameterfFunc glTexParameterfPtr;
        internal static void loadTexParameterf()
        {
            try
            {
                glTexParameterfPtr = (glTexParameterfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterf"), typeof(glTexParameterfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterf'.");
            }
        }
        public static void glTexParameterf(GLenum @target, GLenum @pname, GLfloat @param) => glTexParameterfPtr(@target, @pname, @param);

        internal delegate void glTexParameterfvFunc(GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glTexParameterfvFunc glTexParameterfvPtr;
        internal static void loadTexParameterfv()
        {
            try
            {
                glTexParameterfvPtr = (glTexParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterfv"), typeof(glTexParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterfv'.");
            }
        }
        public static void glTexParameterfv(GLenum @target, GLenum @pname, const GLfloat * @params) => glTexParameterfvPtr(@target, @pname, @params);

        internal delegate void glTexParameteriFunc(GLenum @target, GLenum @pname, GLint @param);
        internal static glTexParameteriFunc glTexParameteriPtr;
        internal static void loadTexParameteri()
        {
            try
            {
                glTexParameteriPtr = (glTexParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameteri"), typeof(glTexParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameteri'.");
            }
        }
        public static void glTexParameteri(GLenum @target, GLenum @pname, GLint @param) => glTexParameteriPtr(@target, @pname, @param);

        internal delegate void glTexParameterivFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glTexParameterivFunc glTexParameterivPtr;
        internal static void loadTexParameteriv()
        {
            try
            {
                glTexParameterivPtr = (glTexParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameteriv"), typeof(glTexParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameteriv'.");
            }
        }
        public static void glTexParameteriv(GLenum @target, GLenum @pname, const GLint * @params) => glTexParameterivPtr(@target, @pname, @params);

        internal delegate void glTexSubImage2DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexSubImage2DFunc glTexSubImage2DPtr;
        internal static void loadTexSubImage2D()
        {
            try
            {
                glTexSubImage2DPtr = (glTexSubImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexSubImage2D"), typeof(glTexSubImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexSubImage2D'.");
            }
        }
        public static void glTexSubImage2D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels) => glTexSubImage2DPtr(@target, @level, @xoffset, @yoffset, @width, @height, @format, @type, @pixels);

        internal delegate void glUniform1fFunc(GLint @location, GLfloat @v0);
        internal static glUniform1fFunc glUniform1fPtr;
        internal static void loadUniform1f()
        {
            try
            {
                glUniform1fPtr = (glUniform1fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1f"), typeof(glUniform1fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1f'.");
            }
        }
        public static void glUniform1f(GLint @location, GLfloat @v0) => glUniform1fPtr(@location, @v0);

        internal delegate void glUniform1fvFunc(GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glUniform1fvFunc glUniform1fvPtr;
        internal static void loadUniform1fv()
        {
            try
            {
                glUniform1fvPtr = (glUniform1fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1fv"), typeof(glUniform1fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1fv'.");
            }
        }
        public static void glUniform1fv(GLint @location, GLsizei @count, const GLfloat * @value) => glUniform1fvPtr(@location, @count, @value);

        internal delegate void glUniform1iFunc(GLint @location, GLint @v0);
        internal static glUniform1iFunc glUniform1iPtr;
        internal static void loadUniform1i()
        {
            try
            {
                glUniform1iPtr = (glUniform1iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1i"), typeof(glUniform1iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1i'.");
            }
        }
        public static void glUniform1i(GLint @location, GLint @v0) => glUniform1iPtr(@location, @v0);

        internal delegate void glUniform1ivFunc(GLint @location, GLsizei @count, const GLint * @value);
        internal static glUniform1ivFunc glUniform1ivPtr;
        internal static void loadUniform1iv()
        {
            try
            {
                glUniform1ivPtr = (glUniform1ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1iv"), typeof(glUniform1ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1iv'.");
            }
        }
        public static void glUniform1iv(GLint @location, GLsizei @count, const GLint * @value) => glUniform1ivPtr(@location, @count, @value);

        internal delegate void glUniform2fFunc(GLint @location, GLfloat @v0, GLfloat @v1);
        internal static glUniform2fFunc glUniform2fPtr;
        internal static void loadUniform2f()
        {
            try
            {
                glUniform2fPtr = (glUniform2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2f"), typeof(glUniform2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2f'.");
            }
        }
        public static void glUniform2f(GLint @location, GLfloat @v0, GLfloat @v1) => glUniform2fPtr(@location, @v0, @v1);

        internal delegate void glUniform2fvFunc(GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glUniform2fvFunc glUniform2fvPtr;
        internal static void loadUniform2fv()
        {
            try
            {
                glUniform2fvPtr = (glUniform2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2fv"), typeof(glUniform2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2fv'.");
            }
        }
        public static void glUniform2fv(GLint @location, GLsizei @count, const GLfloat * @value) => glUniform2fvPtr(@location, @count, @value);

        internal delegate void glUniform2iFunc(GLint @location, GLint @v0, GLint @v1);
        internal static glUniform2iFunc glUniform2iPtr;
        internal static void loadUniform2i()
        {
            try
            {
                glUniform2iPtr = (glUniform2iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2i"), typeof(glUniform2iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2i'.");
            }
        }
        public static void glUniform2i(GLint @location, GLint @v0, GLint @v1) => glUniform2iPtr(@location, @v0, @v1);

        internal delegate void glUniform2ivFunc(GLint @location, GLsizei @count, const GLint * @value);
        internal static glUniform2ivFunc glUniform2ivPtr;
        internal static void loadUniform2iv()
        {
            try
            {
                glUniform2ivPtr = (glUniform2ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2iv"), typeof(glUniform2ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2iv'.");
            }
        }
        public static void glUniform2iv(GLint @location, GLsizei @count, const GLint * @value) => glUniform2ivPtr(@location, @count, @value);

        internal delegate void glUniform3fFunc(GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2);
        internal static glUniform3fFunc glUniform3fPtr;
        internal static void loadUniform3f()
        {
            try
            {
                glUniform3fPtr = (glUniform3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3f"), typeof(glUniform3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3f'.");
            }
        }
        public static void glUniform3f(GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2) => glUniform3fPtr(@location, @v0, @v1, @v2);

        internal delegate void glUniform3fvFunc(GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glUniform3fvFunc glUniform3fvPtr;
        internal static void loadUniform3fv()
        {
            try
            {
                glUniform3fvPtr = (glUniform3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3fv"), typeof(glUniform3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3fv'.");
            }
        }
        public static void glUniform3fv(GLint @location, GLsizei @count, const GLfloat * @value) => glUniform3fvPtr(@location, @count, @value);

        internal delegate void glUniform3iFunc(GLint @location, GLint @v0, GLint @v1, GLint @v2);
        internal static glUniform3iFunc glUniform3iPtr;
        internal static void loadUniform3i()
        {
            try
            {
                glUniform3iPtr = (glUniform3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3i"), typeof(glUniform3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3i'.");
            }
        }
        public static void glUniform3i(GLint @location, GLint @v0, GLint @v1, GLint @v2) => glUniform3iPtr(@location, @v0, @v1, @v2);

        internal delegate void glUniform3ivFunc(GLint @location, GLsizei @count, const GLint * @value);
        internal static glUniform3ivFunc glUniform3ivPtr;
        internal static void loadUniform3iv()
        {
            try
            {
                glUniform3ivPtr = (glUniform3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3iv"), typeof(glUniform3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3iv'.");
            }
        }
        public static void glUniform3iv(GLint @location, GLsizei @count, const GLint * @value) => glUniform3ivPtr(@location, @count, @value);

        internal delegate void glUniform4fFunc(GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3);
        internal static glUniform4fFunc glUniform4fPtr;
        internal static void loadUniform4f()
        {
            try
            {
                glUniform4fPtr = (glUniform4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4f"), typeof(glUniform4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4f'.");
            }
        }
        public static void glUniform4f(GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3) => glUniform4fPtr(@location, @v0, @v1, @v2, @v3);

        internal delegate void glUniform4fvFunc(GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glUniform4fvFunc glUniform4fvPtr;
        internal static void loadUniform4fv()
        {
            try
            {
                glUniform4fvPtr = (glUniform4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4fv"), typeof(glUniform4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4fv'.");
            }
        }
        public static void glUniform4fv(GLint @location, GLsizei @count, const GLfloat * @value) => glUniform4fvPtr(@location, @count, @value);

        internal delegate void glUniform4iFunc(GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3);
        internal static glUniform4iFunc glUniform4iPtr;
        internal static void loadUniform4i()
        {
            try
            {
                glUniform4iPtr = (glUniform4iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4i"), typeof(glUniform4iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4i'.");
            }
        }
        public static void glUniform4i(GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3) => glUniform4iPtr(@location, @v0, @v1, @v2, @v3);

        internal delegate void glUniform4ivFunc(GLint @location, GLsizei @count, const GLint * @value);
        internal static glUniform4ivFunc glUniform4ivPtr;
        internal static void loadUniform4iv()
        {
            try
            {
                glUniform4ivPtr = (glUniform4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4iv"), typeof(glUniform4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4iv'.");
            }
        }
        public static void glUniform4iv(GLint @location, GLsizei @count, const GLint * @value) => glUniform4ivPtr(@location, @count, @value);

        internal delegate void glUniformMatrix2fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix2fvFunc glUniformMatrix2fvPtr;
        internal static void loadUniformMatrix2fv()
        {
            try
            {
                glUniformMatrix2fvPtr = (glUniformMatrix2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2fv"), typeof(glUniformMatrix2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2fv'.");
            }
        }
        public static void glUniformMatrix2fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix2fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix3fvFunc glUniformMatrix3fvPtr;
        internal static void loadUniformMatrix3fv()
        {
            try
            {
                glUniformMatrix3fvPtr = (glUniformMatrix3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3fv"), typeof(glUniformMatrix3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3fv'.");
            }
        }
        public static void glUniformMatrix3fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix3fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix4fvFunc glUniformMatrix4fvPtr;
        internal static void loadUniformMatrix4fv()
        {
            try
            {
                glUniformMatrix4fvPtr = (glUniformMatrix4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4fv"), typeof(glUniformMatrix4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4fv'.");
            }
        }
        public static void glUniformMatrix4fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix4fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUseProgramFunc(GLuint @program);
        internal static glUseProgramFunc glUseProgramPtr;
        internal static void loadUseProgram()
        {
            try
            {
                glUseProgramPtr = (glUseProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUseProgram"), typeof(glUseProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUseProgram'.");
            }
        }
        public static void glUseProgram(GLuint @program) => glUseProgramPtr(@program);

        internal delegate void glValidateProgramFunc(GLuint @program);
        internal static glValidateProgramFunc glValidateProgramPtr;
        internal static void loadValidateProgram()
        {
            try
            {
                glValidateProgramPtr = (glValidateProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glValidateProgram"), typeof(glValidateProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glValidateProgram'.");
            }
        }
        public static void glValidateProgram(GLuint @program) => glValidateProgramPtr(@program);

        internal delegate void glVertexAttrib1fFunc(GLuint @index, GLfloat @x);
        internal static glVertexAttrib1fFunc glVertexAttrib1fPtr;
        internal static void loadVertexAttrib1f()
        {
            try
            {
                glVertexAttrib1fPtr = (glVertexAttrib1fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1f"), typeof(glVertexAttrib1fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1f'.");
            }
        }
        public static void glVertexAttrib1f(GLuint @index, GLfloat @x) => glVertexAttrib1fPtr(@index, @x);

        internal delegate void glVertexAttrib1fvFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib1fvFunc glVertexAttrib1fvPtr;
        internal static void loadVertexAttrib1fv()
        {
            try
            {
                glVertexAttrib1fvPtr = (glVertexAttrib1fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1fv"), typeof(glVertexAttrib1fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1fv'.");
            }
        }
        public static void glVertexAttrib1fv(GLuint @index, const GLfloat * @v) => glVertexAttrib1fvPtr(@index, @v);

        internal delegate void glVertexAttrib2fFunc(GLuint @index, GLfloat @x, GLfloat @y);
        internal static glVertexAttrib2fFunc glVertexAttrib2fPtr;
        internal static void loadVertexAttrib2f()
        {
            try
            {
                glVertexAttrib2fPtr = (glVertexAttrib2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2f"), typeof(glVertexAttrib2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2f'.");
            }
        }
        public static void glVertexAttrib2f(GLuint @index, GLfloat @x, GLfloat @y) => glVertexAttrib2fPtr(@index, @x, @y);

        internal delegate void glVertexAttrib2fvFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib2fvFunc glVertexAttrib2fvPtr;
        internal static void loadVertexAttrib2fv()
        {
            try
            {
                glVertexAttrib2fvPtr = (glVertexAttrib2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2fv"), typeof(glVertexAttrib2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2fv'.");
            }
        }
        public static void glVertexAttrib2fv(GLuint @index, const GLfloat * @v) => glVertexAttrib2fvPtr(@index, @v);

        internal delegate void glVertexAttrib3fFunc(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glVertexAttrib3fFunc glVertexAttrib3fPtr;
        internal static void loadVertexAttrib3f()
        {
            try
            {
                glVertexAttrib3fPtr = (glVertexAttrib3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3f"), typeof(glVertexAttrib3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3f'.");
            }
        }
        public static void glVertexAttrib3f(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z) => glVertexAttrib3fPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttrib3fvFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib3fvFunc glVertexAttrib3fvPtr;
        internal static void loadVertexAttrib3fv()
        {
            try
            {
                glVertexAttrib3fvPtr = (glVertexAttrib3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3fv"), typeof(glVertexAttrib3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3fv'.");
            }
        }
        public static void glVertexAttrib3fv(GLuint @index, const GLfloat * @v) => glVertexAttrib3fvPtr(@index, @v);

        internal delegate void glVertexAttrib4fFunc(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glVertexAttrib4fFunc glVertexAttrib4fPtr;
        internal static void loadVertexAttrib4f()
        {
            try
            {
                glVertexAttrib4fPtr = (glVertexAttrib4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4f"), typeof(glVertexAttrib4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4f'.");
            }
        }
        public static void glVertexAttrib4f(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glVertexAttrib4fPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4fvFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib4fvFunc glVertexAttrib4fvPtr;
        internal static void loadVertexAttrib4fv()
        {
            try
            {
                glVertexAttrib4fvPtr = (glVertexAttrib4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4fv"), typeof(glVertexAttrib4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4fv'.");
            }
        }
        public static void glVertexAttrib4fv(GLuint @index, const GLfloat * @v) => glVertexAttrib4fvPtr(@index, @v);

        internal delegate void glVertexAttribPointerFunc(GLuint @index, GLint @size, GLenum @type, GLboolean @normalized, GLsizei @stride, const void * @pointer);
        internal static glVertexAttribPointerFunc glVertexAttribPointerPtr;
        internal static void loadVertexAttribPointer()
        {
            try
            {
                glVertexAttribPointerPtr = (glVertexAttribPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribPointer"), typeof(glVertexAttribPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribPointer'.");
            }
        }
        public static void glVertexAttribPointer(GLuint @index, GLint @size, GLenum @type, GLboolean @normalized, GLsizei @stride, const void * @pointer) => glVertexAttribPointerPtr(@index, @size, @type, @normalized, @stride, @pointer);

        internal delegate void glViewportFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glViewportFunc glViewportPtr;
        internal static void loadViewport()
        {
            try
            {
                glViewportPtr = (glViewportFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glViewport"), typeof(glViewportFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glViewport'.");
            }
        }
        public static void glViewport(GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glViewportPtr(@x, @y, @width, @height);
        #endregion
    }
}
