using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL10
    {
        #region Interop
        static GL10()
        {
            Console.WriteLine("Initalising GL10 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCullFace();
            loadFrontFace();
            loadHint();
            loadLineWidth();
            loadPointSize();
            loadPolygonMode();
            loadScissor();
            loadTexParameterf();
            loadTexParameterfv();
            loadTexParameteri();
            loadTexParameteriv();
            loadTexImage1D();
            loadTexImage2D();
            loadDrawBuffer();
            loadClear();
            loadClearColor();
            loadClearStencil();
            loadClearDepth();
            loadStencilMask();
            loadColorMask();
            loadDepthMask();
            loadDisable();
            loadEnable();
            loadFinish();
            loadFlush();
            loadBlendFunc();
            loadLogicOp();
            loadStencilFunc();
            loadStencilOp();
            loadDepthFunc();
            loadPixelStoref();
            loadPixelStorei();
            loadReadBuffer();
            loadReadPixels();
            loadGetBooleanv();
            loadGetDoublev();
            loadGetError();
            loadGetFloatv();
            loadGetIntegerv();
            loadGetString();
            loadGetTexImage();
            loadGetTexParameterfv();
            loadGetTexParameteriv();
            loadGetTexLevelParameterfv();
            loadGetTexLevelParameteriv();
            loadIsEnabled();
            loadDepthRange();
            loadViewport();
            loadNewList();
            loadEndList();
            loadCallList();
            loadCallLists();
            loadDeleteLists();
            loadGenLists();
            loadListBase();
            loadBegin();
            loadBitmap();
            loadColor3b();
            loadColor3bv();
            loadColor3d();
            loadColor3dv();
            loadColor3f();
            loadColor3fv();
            loadColor3i();
            loadColor3iv();
            loadColor3s();
            loadColor3sv();
            loadColor3ub();
            loadColor3ubv();
            loadColor3ui();
            loadColor3uiv();
            loadColor3us();
            loadColor3usv();
            loadColor4b();
            loadColor4bv();
            loadColor4d();
            loadColor4dv();
            loadColor4f();
            loadColor4fv();
            loadColor4i();
            loadColor4iv();
            loadColor4s();
            loadColor4sv();
            loadColor4ub();
            loadColor4ubv();
            loadColor4ui();
            loadColor4uiv();
            loadColor4us();
            loadColor4usv();
            loadEdgeFlag();
            loadEdgeFlagv();
            loadEnd();
            loadIndexd();
            loadIndexdv();
            loadIndexf();
            loadIndexfv();
            loadIndexi();
            loadIndexiv();
            loadIndexs();
            loadIndexsv();
            loadNormal3b();
            loadNormal3bv();
            loadNormal3d();
            loadNormal3dv();
            loadNormal3f();
            loadNormal3fv();
            loadNormal3i();
            loadNormal3iv();
            loadNormal3s();
            loadNormal3sv();
            loadRasterPos2d();
            loadRasterPos2dv();
            loadRasterPos2f();
            loadRasterPos2fv();
            loadRasterPos2i();
            loadRasterPos2iv();
            loadRasterPos2s();
            loadRasterPos2sv();
            loadRasterPos3d();
            loadRasterPos3dv();
            loadRasterPos3f();
            loadRasterPos3fv();
            loadRasterPos3i();
            loadRasterPos3iv();
            loadRasterPos3s();
            loadRasterPos3sv();
            loadRasterPos4d();
            loadRasterPos4dv();
            loadRasterPos4f();
            loadRasterPos4fv();
            loadRasterPos4i();
            loadRasterPos4iv();
            loadRasterPos4s();
            loadRasterPos4sv();
            loadRectd();
            loadRectdv();
            loadRectf();
            loadRectfv();
            loadRecti();
            loadRectiv();
            loadRects();
            loadRectsv();
            loadTexCoord1d();
            loadTexCoord1dv();
            loadTexCoord1f();
            loadTexCoord1fv();
            loadTexCoord1i();
            loadTexCoord1iv();
            loadTexCoord1s();
            loadTexCoord1sv();
            loadTexCoord2d();
            loadTexCoord2dv();
            loadTexCoord2f();
            loadTexCoord2fv();
            loadTexCoord2i();
            loadTexCoord2iv();
            loadTexCoord2s();
            loadTexCoord2sv();
            loadTexCoord3d();
            loadTexCoord3dv();
            loadTexCoord3f();
            loadTexCoord3fv();
            loadTexCoord3i();
            loadTexCoord3iv();
            loadTexCoord3s();
            loadTexCoord3sv();
            loadTexCoord4d();
            loadTexCoord4dv();
            loadTexCoord4f();
            loadTexCoord4fv();
            loadTexCoord4i();
            loadTexCoord4iv();
            loadTexCoord4s();
            loadTexCoord4sv();
            loadVertex2d();
            loadVertex2dv();
            loadVertex2f();
            loadVertex2fv();
            loadVertex2i();
            loadVertex2iv();
            loadVertex2s();
            loadVertex2sv();
            loadVertex3d();
            loadVertex3dv();
            loadVertex3f();
            loadVertex3fv();
            loadVertex3i();
            loadVertex3iv();
            loadVertex3s();
            loadVertex3sv();
            loadVertex4d();
            loadVertex4dv();
            loadVertex4f();
            loadVertex4fv();
            loadVertex4i();
            loadVertex4iv();
            loadVertex4s();
            loadVertex4sv();
            loadClipPlane();
            loadColorMaterial();
            loadFogf();
            loadFogfv();
            loadFogi();
            loadFogiv();
            loadLightf();
            loadLightfv();
            loadLighti();
            loadLightiv();
            loadLightModelf();
            loadLightModelfv();
            loadLightModeli();
            loadLightModeliv();
            loadLineStipple();
            loadMaterialf();
            loadMaterialfv();
            loadMateriali();
            loadMaterialiv();
            loadPolygonStipple();
            loadShadeModel();
            loadTexEnvf();
            loadTexEnvfv();
            loadTexEnvi();
            loadTexEnviv();
            loadTexGend();
            loadTexGendv();
            loadTexGenf();
            loadTexGenfv();
            loadTexGeni();
            loadTexGeniv();
            loadFeedbackBuffer();
            loadSelectBuffer();
            loadRenderMode();
            loadInitNames();
            loadLoadName();
            loadPassThrough();
            loadPopName();
            loadPushName();
            loadClearAccum();
            loadClearIndex();
            loadIndexMask();
            loadAccum();
            loadPopAttrib();
            loadPushAttrib();
            loadMap1d();
            loadMap1f();
            loadMap2d();
            loadMap2f();
            loadMapGrid1d();
            loadMapGrid1f();
            loadMapGrid2d();
            loadMapGrid2f();
            loadEvalCoord1d();
            loadEvalCoord1dv();
            loadEvalCoord1f();
            loadEvalCoord1fv();
            loadEvalCoord2d();
            loadEvalCoord2dv();
            loadEvalCoord2f();
            loadEvalCoord2fv();
            loadEvalMesh1();
            loadEvalPoint1();
            loadEvalMesh2();
            loadEvalPoint2();
            loadAlphaFunc();
            loadPixelZoom();
            loadPixelTransferf();
            loadPixelTransferi();
            loadPixelMapfv();
            loadPixelMapuiv();
            loadPixelMapusv();
            loadCopyPixels();
            loadDrawPixels();
            loadGetClipPlane();
            loadGetLightfv();
            loadGetLightiv();
            loadGetMapdv();
            loadGetMapfv();
            loadGetMapiv();
            loadGetMaterialfv();
            loadGetMaterialiv();
            loadGetPixelMapfv();
            loadGetPixelMapuiv();
            loadGetPixelMapusv();
            loadGetPolygonStipple();
            loadGetTexEnvfv();
            loadGetTexEnviv();
            loadGetTexGendv();
            loadGetTexGenfv();
            loadGetTexGeniv();
            loadIsList();
            loadFrustum();
            loadLoadIdentity();
            loadLoadMatrixf();
            loadLoadMatrixd();
            loadMatrixMode();
            loadMultMatrixf();
            loadMultMatrixd();
            loadOrtho();
            loadPopMatrix();
            loadPushMatrix();
            loadRotated();
            loadRotatef();
            loadScaled();
            loadScalef();
            loadTranslated();
            loadTranslatef();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glCullFaceFunc(GLenum @mode);
        internal static glCullFaceFunc glCullFacePtr;
        internal static void loadCullFace()
        {
            try
            {
                glCullFacePtr = (glCullFaceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCullFace"), typeof(glCullFaceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCullFace'.");
            }
        }
        public static void glCullFace(GLenum @mode) => glCullFacePtr(@mode);

        internal delegate void glFrontFaceFunc(GLenum @mode);
        internal static glFrontFaceFunc glFrontFacePtr;
        internal static void loadFrontFace()
        {
            try
            {
                glFrontFacePtr = (glFrontFaceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFrontFace"), typeof(glFrontFaceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFrontFace'.");
            }
        }
        public static void glFrontFace(GLenum @mode) => glFrontFacePtr(@mode);

        internal delegate void glHintFunc(GLenum @target, GLenum @mode);
        internal static glHintFunc glHintPtr;
        internal static void loadHint()
        {
            try
            {
                glHintPtr = (glHintFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glHint"), typeof(glHintFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glHint'.");
            }
        }
        public static void glHint(GLenum @target, GLenum @mode) => glHintPtr(@target, @mode);

        internal delegate void glLineWidthFunc(GLfloat @width);
        internal static glLineWidthFunc glLineWidthPtr;
        internal static void loadLineWidth()
        {
            try
            {
                glLineWidthPtr = (glLineWidthFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLineWidth"), typeof(glLineWidthFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLineWidth'.");
            }
        }
        public static void glLineWidth(GLfloat @width) => glLineWidthPtr(@width);

        internal delegate void glPointSizeFunc(GLfloat @size);
        internal static glPointSizeFunc glPointSizePtr;
        internal static void loadPointSize()
        {
            try
            {
                glPointSizePtr = (glPointSizeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointSize"), typeof(glPointSizeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointSize'.");
            }
        }
        public static void glPointSize(GLfloat @size) => glPointSizePtr(@size);

        internal delegate void glPolygonModeFunc(GLenum @face, GLenum @mode);
        internal static glPolygonModeFunc glPolygonModePtr;
        internal static void loadPolygonMode()
        {
            try
            {
                glPolygonModePtr = (glPolygonModeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPolygonMode"), typeof(glPolygonModeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPolygonMode'.");
            }
        }
        public static void glPolygonMode(GLenum @face, GLenum @mode) => glPolygonModePtr(@face, @mode);

        internal delegate void glScissorFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glScissorFunc glScissorPtr;
        internal static void loadScissor()
        {
            try
            {
                glScissorPtr = (glScissorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScissor"), typeof(glScissorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScissor'.");
            }
        }
        public static void glScissor(GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glScissorPtr(@x, @y, @width, @height);

        internal delegate void glTexParameterfFunc(GLenum @target, GLenum @pname, GLfloat @param);
        internal static glTexParameterfFunc glTexParameterfPtr;
        internal static void loadTexParameterf()
        {
            try
            {
                glTexParameterfPtr = (glTexParameterfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterf"), typeof(glTexParameterfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterf'.");
            }
        }
        public static void glTexParameterf(GLenum @target, GLenum @pname, GLfloat @param) => glTexParameterfPtr(@target, @pname, @param);

        internal delegate void glTexParameterfvFunc(GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glTexParameterfvFunc glTexParameterfvPtr;
        internal static void loadTexParameterfv()
        {
            try
            {
                glTexParameterfvPtr = (glTexParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterfv"), typeof(glTexParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterfv'.");
            }
        }
        public static void glTexParameterfv(GLenum @target, GLenum @pname, const GLfloat * @params) => glTexParameterfvPtr(@target, @pname, @params);

        internal delegate void glTexParameteriFunc(GLenum @target, GLenum @pname, GLint @param);
        internal static glTexParameteriFunc glTexParameteriPtr;
        internal static void loadTexParameteri()
        {
            try
            {
                glTexParameteriPtr = (glTexParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameteri"), typeof(glTexParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameteri'.");
            }
        }
        public static void glTexParameteri(GLenum @target, GLenum @pname, GLint @param) => glTexParameteriPtr(@target, @pname, @param);

        internal delegate void glTexParameterivFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glTexParameterivFunc glTexParameterivPtr;
        internal static void loadTexParameteriv()
        {
            try
            {
                glTexParameterivPtr = (glTexParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameteriv"), typeof(glTexParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameteriv'.");
            }
        }
        public static void glTexParameteriv(GLenum @target, GLenum @pname, const GLint * @params) => glTexParameterivPtr(@target, @pname, @params);

        internal delegate void glTexImage1DFunc(GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexImage1DFunc glTexImage1DPtr;
        internal static void loadTexImage1D()
        {
            try
            {
                glTexImage1DPtr = (glTexImage1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage1D"), typeof(glTexImage1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage1D'.");
            }
        }
        public static void glTexImage1D(GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glTexImage1DPtr(@target, @level, @internalformat, @width, @border, @format, @type, @pixels);

        internal delegate void glTexImage2DFunc(GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexImage2DFunc glTexImage2DPtr;
        internal static void loadTexImage2D()
        {
            try
            {
                glTexImage2DPtr = (glTexImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage2D"), typeof(glTexImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage2D'.");
            }
        }
        public static void glTexImage2D(GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glTexImage2DPtr(@target, @level, @internalformat, @width, @height, @border, @format, @type, @pixels);

        internal delegate void glDrawBufferFunc(GLenum @buf);
        internal static glDrawBufferFunc glDrawBufferPtr;
        internal static void loadDrawBuffer()
        {
            try
            {
                glDrawBufferPtr = (glDrawBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawBuffer"), typeof(glDrawBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawBuffer'.");
            }
        }
        public static void glDrawBuffer(GLenum @buf) => glDrawBufferPtr(@buf);

        internal delegate void glClearFunc(GLbitfield @mask);
        internal static glClearFunc glClearPtr;
        internal static void loadClear()
        {
            try
            {
                glClearPtr = (glClearFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClear"), typeof(glClearFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClear'.");
            }
        }
        public static void glClear(GLbitfield @mask) => glClearPtr(@mask);

        internal delegate void glClearColorFunc(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha);
        internal static glClearColorFunc glClearColorPtr;
        internal static void loadClearColor()
        {
            try
            {
                glClearColorPtr = (glClearColorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearColor"), typeof(glClearColorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearColor'.");
            }
        }
        public static void glClearColor(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha) => glClearColorPtr(@red, @green, @blue, @alpha);

        internal delegate void glClearStencilFunc(GLint @s);
        internal static glClearStencilFunc glClearStencilPtr;
        internal static void loadClearStencil()
        {
            try
            {
                glClearStencilPtr = (glClearStencilFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearStencil"), typeof(glClearStencilFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearStencil'.");
            }
        }
        public static void glClearStencil(GLint @s) => glClearStencilPtr(@s);

        internal delegate void glClearDepthFunc(GLdouble @depth);
        internal static glClearDepthFunc glClearDepthPtr;
        internal static void loadClearDepth()
        {
            try
            {
                glClearDepthPtr = (glClearDepthFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearDepth"), typeof(glClearDepthFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearDepth'.");
            }
        }
        public static void glClearDepth(GLdouble @depth) => glClearDepthPtr(@depth);

        internal delegate void glStencilMaskFunc(GLuint @mask);
        internal static glStencilMaskFunc glStencilMaskPtr;
        internal static void loadStencilMask()
        {
            try
            {
                glStencilMaskPtr = (glStencilMaskFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilMask"), typeof(glStencilMaskFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilMask'.");
            }
        }
        public static void glStencilMask(GLuint @mask) => glStencilMaskPtr(@mask);

        internal delegate void glColorMaskFunc(GLboolean @red, GLboolean @green, GLboolean @blue, GLboolean @alpha);
        internal static glColorMaskFunc glColorMaskPtr;
        internal static void loadColorMask()
        {
            try
            {
                glColorMaskPtr = (glColorMaskFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorMask"), typeof(glColorMaskFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorMask'.");
            }
        }
        public static void glColorMask(GLboolean @red, GLboolean @green, GLboolean @blue, GLboolean @alpha) => glColorMaskPtr(@red, @green, @blue, @alpha);

        internal delegate void glDepthMaskFunc(GLboolean @flag);
        internal static glDepthMaskFunc glDepthMaskPtr;
        internal static void loadDepthMask()
        {
            try
            {
                glDepthMaskPtr = (glDepthMaskFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthMask"), typeof(glDepthMaskFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthMask'.");
            }
        }
        public static void glDepthMask(GLboolean @flag) => glDepthMaskPtr(@flag);

        internal delegate void glDisableFunc(GLenum @cap);
        internal static glDisableFunc glDisablePtr;
        internal static void loadDisable()
        {
            try
            {
                glDisablePtr = (glDisableFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisable"), typeof(glDisableFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisable'.");
            }
        }
        public static void glDisable(GLenum @cap) => glDisablePtr(@cap);

        internal delegate void glEnableFunc(GLenum @cap);
        internal static glEnableFunc glEnablePtr;
        internal static void loadEnable()
        {
            try
            {
                glEnablePtr = (glEnableFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnable"), typeof(glEnableFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnable'.");
            }
        }
        public static void glEnable(GLenum @cap) => glEnablePtr(@cap);

        internal delegate void glFinishFunc();
        internal static glFinishFunc glFinishPtr;
        internal static void loadFinish()
        {
            try
            {
                glFinishPtr = (glFinishFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFinish"), typeof(glFinishFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFinish'.");
            }
        }
        public static void glFinish() => glFinishPtr();

        internal delegate void glFlushFunc();
        internal static glFlushFunc glFlushPtr;
        internal static void loadFlush()
        {
            try
            {
                glFlushPtr = (glFlushFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlush"), typeof(glFlushFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlush'.");
            }
        }
        public static void glFlush() => glFlushPtr();

        internal delegate void glBlendFuncFunc(GLenum @sfactor, GLenum @dfactor);
        internal static glBlendFuncFunc glBlendFuncPtr;
        internal static void loadBlendFunc()
        {
            try
            {
                glBlendFuncPtr = (glBlendFuncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFunc"), typeof(glBlendFuncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFunc'.");
            }
        }
        public static void glBlendFunc(GLenum @sfactor, GLenum @dfactor) => glBlendFuncPtr(@sfactor, @dfactor);

        internal delegate void glLogicOpFunc(GLenum @opcode);
        internal static glLogicOpFunc glLogicOpPtr;
        internal static void loadLogicOp()
        {
            try
            {
                glLogicOpPtr = (glLogicOpFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLogicOp"), typeof(glLogicOpFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLogicOp'.");
            }
        }
        public static void glLogicOp(GLenum @opcode) => glLogicOpPtr(@opcode);

        internal delegate void glStencilFuncFunc(GLenum @func, GLint @ref, GLuint @mask);
        internal static glStencilFuncFunc glStencilFuncPtr;
        internal static void loadStencilFunc()
        {
            try
            {
                glStencilFuncPtr = (glStencilFuncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilFunc"), typeof(glStencilFuncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilFunc'.");
            }
        }
        public static void glStencilFunc(GLenum @func, GLint @ref, GLuint @mask) => glStencilFuncPtr(@func, @ref, @mask);

        internal delegate void glStencilOpFunc(GLenum @fail, GLenum @zfail, GLenum @zpass);
        internal static glStencilOpFunc glStencilOpPtr;
        internal static void loadStencilOp()
        {
            try
            {
                glStencilOpPtr = (glStencilOpFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilOp"), typeof(glStencilOpFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilOp'.");
            }
        }
        public static void glStencilOp(GLenum @fail, GLenum @zfail, GLenum @zpass) => glStencilOpPtr(@fail, @zfail, @zpass);

        internal delegate void glDepthFuncFunc(GLenum @func);
        internal static glDepthFuncFunc glDepthFuncPtr;
        internal static void loadDepthFunc()
        {
            try
            {
                glDepthFuncPtr = (glDepthFuncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthFunc"), typeof(glDepthFuncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthFunc'.");
            }
        }
        public static void glDepthFunc(GLenum @func) => glDepthFuncPtr(@func);

        internal delegate void glPixelStorefFunc(GLenum @pname, GLfloat @param);
        internal static glPixelStorefFunc glPixelStorefPtr;
        internal static void loadPixelStoref()
        {
            try
            {
                glPixelStorefPtr = (glPixelStorefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelStoref"), typeof(glPixelStorefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelStoref'.");
            }
        }
        public static void glPixelStoref(GLenum @pname, GLfloat @param) => glPixelStorefPtr(@pname, @param);

        internal delegate void glPixelStoreiFunc(GLenum @pname, GLint @param);
        internal static glPixelStoreiFunc glPixelStoreiPtr;
        internal static void loadPixelStorei()
        {
            try
            {
                glPixelStoreiPtr = (glPixelStoreiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelStorei"), typeof(glPixelStoreiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelStorei'.");
            }
        }
        public static void glPixelStorei(GLenum @pname, GLint @param) => glPixelStoreiPtr(@pname, @param);

        internal delegate void glReadBufferFunc(GLenum @src);
        internal static glReadBufferFunc glReadBufferPtr;
        internal static void loadReadBuffer()
        {
            try
            {
                glReadBufferPtr = (glReadBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadBuffer"), typeof(glReadBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadBuffer'.");
            }
        }
        public static void glReadBuffer(GLenum @src) => glReadBufferPtr(@src);

        internal delegate void glReadPixelsFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, void * @pixels);
        internal static glReadPixelsFunc glReadPixelsPtr;
        internal static void loadReadPixels()
        {
            try
            {
                glReadPixelsPtr = (glReadPixelsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadPixels"), typeof(glReadPixelsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadPixels'.");
            }
        }
        public static void glReadPixels(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, void * @pixels) => glReadPixelsPtr(@x, @y, @width, @height, @format, @type, @pixels);

        internal delegate void glGetBooleanvFunc(GLenum @pname, GLboolean * @data);
        internal static glGetBooleanvFunc glGetBooleanvPtr;
        internal static void loadGetBooleanv()
        {
            try
            {
                glGetBooleanvPtr = (glGetBooleanvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBooleanv"), typeof(glGetBooleanvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBooleanv'.");
            }
        }
        public static void glGetBooleanv(GLenum @pname, GLboolean * @data) => glGetBooleanvPtr(@pname, @data);

        internal delegate void glGetDoublevFunc(GLenum @pname, GLdouble * @data);
        internal static glGetDoublevFunc glGetDoublevPtr;
        internal static void loadGetDoublev()
        {
            try
            {
                glGetDoublevPtr = (glGetDoublevFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDoublev"), typeof(glGetDoublevFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDoublev'.");
            }
        }
        public static void glGetDoublev(GLenum @pname, GLdouble * @data) => glGetDoublevPtr(@pname, @data);

        internal delegate GLenum glGetErrorFunc();
        internal static glGetErrorFunc glGetErrorPtr;
        internal static void loadGetError()
        {
            try
            {
                glGetErrorPtr = (glGetErrorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetError"), typeof(glGetErrorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetError'.");
            }
        }
        public static GLenum glGetError() => glGetErrorPtr();

        internal delegate void glGetFloatvFunc(GLenum @pname, GLfloat * @data);
        internal static glGetFloatvFunc glGetFloatvPtr;
        internal static void loadGetFloatv()
        {
            try
            {
                glGetFloatvPtr = (glGetFloatvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFloatv"), typeof(glGetFloatvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFloatv'.");
            }
        }
        public static void glGetFloatv(GLenum @pname, GLfloat * @data) => glGetFloatvPtr(@pname, @data);

        internal delegate void glGetIntegervFunc(GLenum @pname, GLint * @data);
        internal static glGetIntegervFunc glGetIntegervPtr;
        internal static void loadGetIntegerv()
        {
            try
            {
                glGetIntegervPtr = (glGetIntegervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetIntegerv"), typeof(glGetIntegervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetIntegerv'.");
            }
        }
        public static void glGetIntegerv(GLenum @pname, GLint * @data) => glGetIntegervPtr(@pname, @data);

        internal delegate constGLubyte* glGetStringFunc(GLenum @name);
        internal static glGetStringFunc glGetStringPtr;
        internal static void loadGetString()
        {
            try
            {
                glGetStringPtr = (glGetStringFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetString"), typeof(glGetStringFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetString'.");
            }
        }
        public static constGLubyte* glGetString(GLenum @name) => glGetStringPtr(@name);

        internal delegate void glGetTexImageFunc(GLenum @target, GLint @level, GLenum @format, GLenum @type, void * @pixels);
        internal static glGetTexImageFunc glGetTexImagePtr;
        internal static void loadGetTexImage()
        {
            try
            {
                glGetTexImagePtr = (glGetTexImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexImage"), typeof(glGetTexImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexImage'.");
            }
        }
        public static void glGetTexImage(GLenum @target, GLint @level, GLenum @format, GLenum @type, void * @pixels) => glGetTexImagePtr(@target, @level, @format, @type, @pixels);

        internal delegate void glGetTexParameterfvFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetTexParameterfvFunc glGetTexParameterfvPtr;
        internal static void loadGetTexParameterfv()
        {
            try
            {
                glGetTexParameterfvPtr = (glGetTexParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterfv"), typeof(glGetTexParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterfv'.");
            }
        }
        public static void glGetTexParameterfv(GLenum @target, GLenum @pname, GLfloat * @params) => glGetTexParameterfvPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetTexParameterivFunc glGetTexParameterivPtr;
        internal static void loadGetTexParameteriv()
        {
            try
            {
                glGetTexParameterivPtr = (glGetTexParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameteriv"), typeof(glGetTexParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameteriv'.");
            }
        }
        public static void glGetTexParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetTexParameterivPtr(@target, @pname, @params);

        internal delegate void glGetTexLevelParameterfvFunc(GLenum @target, GLint @level, GLenum @pname, GLfloat * @params);
        internal static glGetTexLevelParameterfvFunc glGetTexLevelParameterfvPtr;
        internal static void loadGetTexLevelParameterfv()
        {
            try
            {
                glGetTexLevelParameterfvPtr = (glGetTexLevelParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexLevelParameterfv"), typeof(glGetTexLevelParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexLevelParameterfv'.");
            }
        }
        public static void glGetTexLevelParameterfv(GLenum @target, GLint @level, GLenum @pname, GLfloat * @params) => glGetTexLevelParameterfvPtr(@target, @level, @pname, @params);

        internal delegate void glGetTexLevelParameterivFunc(GLenum @target, GLint @level, GLenum @pname, GLint * @params);
        internal static glGetTexLevelParameterivFunc glGetTexLevelParameterivPtr;
        internal static void loadGetTexLevelParameteriv()
        {
            try
            {
                glGetTexLevelParameterivPtr = (glGetTexLevelParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexLevelParameteriv"), typeof(glGetTexLevelParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexLevelParameteriv'.");
            }
        }
        public static void glGetTexLevelParameteriv(GLenum @target, GLint @level, GLenum @pname, GLint * @params) => glGetTexLevelParameterivPtr(@target, @level, @pname, @params);

        internal delegate GLboolean glIsEnabledFunc(GLenum @cap);
        internal static glIsEnabledFunc glIsEnabledPtr;
        internal static void loadIsEnabled()
        {
            try
            {
                glIsEnabledPtr = (glIsEnabledFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsEnabled"), typeof(glIsEnabledFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsEnabled'.");
            }
        }
        public static GLboolean glIsEnabled(GLenum @cap) => glIsEnabledPtr(@cap);

        internal delegate void glDepthRangeFunc(GLdouble @near, GLdouble @far);
        internal static glDepthRangeFunc glDepthRangePtr;
        internal static void loadDepthRange()
        {
            try
            {
                glDepthRangePtr = (glDepthRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRange"), typeof(glDepthRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRange'.");
            }
        }
        public static void glDepthRange(GLdouble @near, GLdouble @far) => glDepthRangePtr(@near, @far);

        internal delegate void glViewportFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glViewportFunc glViewportPtr;
        internal static void loadViewport()
        {
            try
            {
                glViewportPtr = (glViewportFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glViewport"), typeof(glViewportFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glViewport'.");
            }
        }
        public static void glViewport(GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glViewportPtr(@x, @y, @width, @height);

        internal delegate void glNewListFunc(GLuint @list, GLenum @mode);
        internal static glNewListFunc glNewListPtr;
        internal static void loadNewList()
        {
            try
            {
                glNewListPtr = (glNewListFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNewList"), typeof(glNewListFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNewList'.");
            }
        }
        public static void glNewList(GLuint @list, GLenum @mode) => glNewListPtr(@list, @mode);

        internal delegate void glEndListFunc();
        internal static glEndListFunc glEndListPtr;
        internal static void loadEndList()
        {
            try
            {
                glEndListPtr = (glEndListFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndList"), typeof(glEndListFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndList'.");
            }
        }
        public static void glEndList() => glEndListPtr();

        internal delegate void glCallListFunc(GLuint @list);
        internal static glCallListFunc glCallListPtr;
        internal static void loadCallList()
        {
            try
            {
                glCallListPtr = (glCallListFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCallList"), typeof(glCallListFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCallList'.");
            }
        }
        public static void glCallList(GLuint @list) => glCallListPtr(@list);

        internal delegate void glCallListsFunc(GLsizei @n, GLenum @type, const void * @lists);
        internal static glCallListsFunc glCallListsPtr;
        internal static void loadCallLists()
        {
            try
            {
                glCallListsPtr = (glCallListsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCallLists"), typeof(glCallListsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCallLists'.");
            }
        }
        public static void glCallLists(GLsizei @n, GLenum @type, const void * @lists) => glCallListsPtr(@n, @type, @lists);

        internal delegate void glDeleteListsFunc(GLuint @list, GLsizei @range);
        internal static glDeleteListsFunc glDeleteListsPtr;
        internal static void loadDeleteLists()
        {
            try
            {
                glDeleteListsPtr = (glDeleteListsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteLists"), typeof(glDeleteListsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteLists'.");
            }
        }
        public static void glDeleteLists(GLuint @list, GLsizei @range) => glDeleteListsPtr(@list, @range);

        internal delegate GLuint glGenListsFunc(GLsizei @range);
        internal static glGenListsFunc glGenListsPtr;
        internal static void loadGenLists()
        {
            try
            {
                glGenListsPtr = (glGenListsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenLists"), typeof(glGenListsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenLists'.");
            }
        }
        public static GLuint glGenLists(GLsizei @range) => glGenListsPtr(@range);

        internal delegate void glListBaseFunc(GLuint @base);
        internal static glListBaseFunc glListBasePtr;
        internal static void loadListBase()
        {
            try
            {
                glListBasePtr = (glListBaseFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glListBase"), typeof(glListBaseFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glListBase'.");
            }
        }
        public static void glListBase(GLuint @base) => glListBasePtr(@base);

        internal delegate void glBeginFunc(GLenum @mode);
        internal static glBeginFunc glBeginPtr;
        internal static void loadBegin()
        {
            try
            {
                glBeginPtr = (glBeginFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBegin"), typeof(glBeginFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBegin'.");
            }
        }
        public static void glBegin(GLenum @mode) => glBeginPtr(@mode);

        internal delegate void glBitmapFunc(GLsizei @width, GLsizei @height, GLfloat @xorig, GLfloat @yorig, GLfloat @xmove, GLfloat @ymove, const GLubyte * @bitmap);
        internal static glBitmapFunc glBitmapPtr;
        internal static void loadBitmap()
        {
            try
            {
                glBitmapPtr = (glBitmapFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBitmap"), typeof(glBitmapFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBitmap'.");
            }
        }
        public static void glBitmap(GLsizei @width, GLsizei @height, GLfloat @xorig, GLfloat @yorig, GLfloat @xmove, GLfloat @ymove, const GLubyte * @bitmap) => glBitmapPtr(@width, @height, @xorig, @yorig, @xmove, @ymove, @bitmap);

        internal delegate void glColor3bFunc(GLbyte @red, GLbyte @green, GLbyte @blue);
        internal static glColor3bFunc glColor3bPtr;
        internal static void loadColor3b()
        {
            try
            {
                glColor3bPtr = (glColor3bFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3b"), typeof(glColor3bFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3b'.");
            }
        }
        public static void glColor3b(GLbyte @red, GLbyte @green, GLbyte @blue) => glColor3bPtr(@red, @green, @blue);

        internal delegate void glColor3bvFunc(const GLbyte * @v);
        internal static glColor3bvFunc glColor3bvPtr;
        internal static void loadColor3bv()
        {
            try
            {
                glColor3bvPtr = (glColor3bvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3bv"), typeof(glColor3bvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3bv'.");
            }
        }
        public static void glColor3bv(const GLbyte * @v) => glColor3bvPtr(@v);

        internal delegate void glColor3dFunc(GLdouble @red, GLdouble @green, GLdouble @blue);
        internal static glColor3dFunc glColor3dPtr;
        internal static void loadColor3d()
        {
            try
            {
                glColor3dPtr = (glColor3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3d"), typeof(glColor3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3d'.");
            }
        }
        public static void glColor3d(GLdouble @red, GLdouble @green, GLdouble @blue) => glColor3dPtr(@red, @green, @blue);

        internal delegate void glColor3dvFunc(const GLdouble * @v);
        internal static glColor3dvFunc glColor3dvPtr;
        internal static void loadColor3dv()
        {
            try
            {
                glColor3dvPtr = (glColor3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3dv"), typeof(glColor3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3dv'.");
            }
        }
        public static void glColor3dv(const GLdouble * @v) => glColor3dvPtr(@v);

        internal delegate void glColor3fFunc(GLfloat @red, GLfloat @green, GLfloat @blue);
        internal static glColor3fFunc glColor3fPtr;
        internal static void loadColor3f()
        {
            try
            {
                glColor3fPtr = (glColor3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3f"), typeof(glColor3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3f'.");
            }
        }
        public static void glColor3f(GLfloat @red, GLfloat @green, GLfloat @blue) => glColor3fPtr(@red, @green, @blue);

        internal delegate void glColor3fvFunc(const GLfloat * @v);
        internal static glColor3fvFunc glColor3fvPtr;
        internal static void loadColor3fv()
        {
            try
            {
                glColor3fvPtr = (glColor3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3fv"), typeof(glColor3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3fv'.");
            }
        }
        public static void glColor3fv(const GLfloat * @v) => glColor3fvPtr(@v);

        internal delegate void glColor3iFunc(GLint @red, GLint @green, GLint @blue);
        internal static glColor3iFunc glColor3iPtr;
        internal static void loadColor3i()
        {
            try
            {
                glColor3iPtr = (glColor3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3i"), typeof(glColor3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3i'.");
            }
        }
        public static void glColor3i(GLint @red, GLint @green, GLint @blue) => glColor3iPtr(@red, @green, @blue);

        internal delegate void glColor3ivFunc(const GLint * @v);
        internal static glColor3ivFunc glColor3ivPtr;
        internal static void loadColor3iv()
        {
            try
            {
                glColor3ivPtr = (glColor3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3iv"), typeof(glColor3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3iv'.");
            }
        }
        public static void glColor3iv(const GLint * @v) => glColor3ivPtr(@v);

        internal delegate void glColor3sFunc(GLshort @red, GLshort @green, GLshort @blue);
        internal static glColor3sFunc glColor3sPtr;
        internal static void loadColor3s()
        {
            try
            {
                glColor3sPtr = (glColor3sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3s"), typeof(glColor3sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3s'.");
            }
        }
        public static void glColor3s(GLshort @red, GLshort @green, GLshort @blue) => glColor3sPtr(@red, @green, @blue);

        internal delegate void glColor3svFunc(const GLshort * @v);
        internal static glColor3svFunc glColor3svPtr;
        internal static void loadColor3sv()
        {
            try
            {
                glColor3svPtr = (glColor3svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3sv"), typeof(glColor3svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3sv'.");
            }
        }
        public static void glColor3sv(const GLshort * @v) => glColor3svPtr(@v);

        internal delegate void glColor3ubFunc(GLubyte @red, GLubyte @green, GLubyte @blue);
        internal static glColor3ubFunc glColor3ubPtr;
        internal static void loadColor3ub()
        {
            try
            {
                glColor3ubPtr = (glColor3ubFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3ub"), typeof(glColor3ubFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3ub'.");
            }
        }
        public static void glColor3ub(GLubyte @red, GLubyte @green, GLubyte @blue) => glColor3ubPtr(@red, @green, @blue);

        internal delegate void glColor3ubvFunc(const GLubyte * @v);
        internal static glColor3ubvFunc glColor3ubvPtr;
        internal static void loadColor3ubv()
        {
            try
            {
                glColor3ubvPtr = (glColor3ubvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3ubv"), typeof(glColor3ubvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3ubv'.");
            }
        }
        public static void glColor3ubv(const GLubyte * @v) => glColor3ubvPtr(@v);

        internal delegate void glColor3uiFunc(GLuint @red, GLuint @green, GLuint @blue);
        internal static glColor3uiFunc glColor3uiPtr;
        internal static void loadColor3ui()
        {
            try
            {
                glColor3uiPtr = (glColor3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3ui"), typeof(glColor3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3ui'.");
            }
        }
        public static void glColor3ui(GLuint @red, GLuint @green, GLuint @blue) => glColor3uiPtr(@red, @green, @blue);

        internal delegate void glColor3uivFunc(const GLuint * @v);
        internal static glColor3uivFunc glColor3uivPtr;
        internal static void loadColor3uiv()
        {
            try
            {
                glColor3uivPtr = (glColor3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3uiv"), typeof(glColor3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3uiv'.");
            }
        }
        public static void glColor3uiv(const GLuint * @v) => glColor3uivPtr(@v);

        internal delegate void glColor3usFunc(GLushort @red, GLushort @green, GLushort @blue);
        internal static glColor3usFunc glColor3usPtr;
        internal static void loadColor3us()
        {
            try
            {
                glColor3usPtr = (glColor3usFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3us"), typeof(glColor3usFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3us'.");
            }
        }
        public static void glColor3us(GLushort @red, GLushort @green, GLushort @blue) => glColor3usPtr(@red, @green, @blue);

        internal delegate void glColor3usvFunc(const GLushort * @v);
        internal static glColor3usvFunc glColor3usvPtr;
        internal static void loadColor3usv()
        {
            try
            {
                glColor3usvPtr = (glColor3usvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3usv"), typeof(glColor3usvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3usv'.");
            }
        }
        public static void glColor3usv(const GLushort * @v) => glColor3usvPtr(@v);

        internal delegate void glColor4bFunc(GLbyte @red, GLbyte @green, GLbyte @blue, GLbyte @alpha);
        internal static glColor4bFunc glColor4bPtr;
        internal static void loadColor4b()
        {
            try
            {
                glColor4bPtr = (glColor4bFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4b"), typeof(glColor4bFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4b'.");
            }
        }
        public static void glColor4b(GLbyte @red, GLbyte @green, GLbyte @blue, GLbyte @alpha) => glColor4bPtr(@red, @green, @blue, @alpha);

        internal delegate void glColor4bvFunc(const GLbyte * @v);
        internal static glColor4bvFunc glColor4bvPtr;
        internal static void loadColor4bv()
        {
            try
            {
                glColor4bvPtr = (glColor4bvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4bv"), typeof(glColor4bvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4bv'.");
            }
        }
        public static void glColor4bv(const GLbyte * @v) => glColor4bvPtr(@v);

        internal delegate void glColor4dFunc(GLdouble @red, GLdouble @green, GLdouble @blue, GLdouble @alpha);
        internal static glColor4dFunc glColor4dPtr;
        internal static void loadColor4d()
        {
            try
            {
                glColor4dPtr = (glColor4dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4d"), typeof(glColor4dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4d'.");
            }
        }
        public static void glColor4d(GLdouble @red, GLdouble @green, GLdouble @blue, GLdouble @alpha) => glColor4dPtr(@red, @green, @blue, @alpha);

        internal delegate void glColor4dvFunc(const GLdouble * @v);
        internal static glColor4dvFunc glColor4dvPtr;
        internal static void loadColor4dv()
        {
            try
            {
                glColor4dvPtr = (glColor4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4dv"), typeof(glColor4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4dv'.");
            }
        }
        public static void glColor4dv(const GLdouble * @v) => glColor4dvPtr(@v);

        internal delegate void glColor4fFunc(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha);
        internal static glColor4fFunc glColor4fPtr;
        internal static void loadColor4f()
        {
            try
            {
                glColor4fPtr = (glColor4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4f"), typeof(glColor4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4f'.");
            }
        }
        public static void glColor4f(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha) => glColor4fPtr(@red, @green, @blue, @alpha);

        internal delegate void glColor4fvFunc(const GLfloat * @v);
        internal static glColor4fvFunc glColor4fvPtr;
        internal static void loadColor4fv()
        {
            try
            {
                glColor4fvPtr = (glColor4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4fv"), typeof(glColor4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4fv'.");
            }
        }
        public static void glColor4fv(const GLfloat * @v) => glColor4fvPtr(@v);

        internal delegate void glColor4iFunc(GLint @red, GLint @green, GLint @blue, GLint @alpha);
        internal static glColor4iFunc glColor4iPtr;
        internal static void loadColor4i()
        {
            try
            {
                glColor4iPtr = (glColor4iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4i"), typeof(glColor4iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4i'.");
            }
        }
        public static void glColor4i(GLint @red, GLint @green, GLint @blue, GLint @alpha) => glColor4iPtr(@red, @green, @blue, @alpha);

        internal delegate void glColor4ivFunc(const GLint * @v);
        internal static glColor4ivFunc glColor4ivPtr;
        internal static void loadColor4iv()
        {
            try
            {
                glColor4ivPtr = (glColor4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4iv"), typeof(glColor4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4iv'.");
            }
        }
        public static void glColor4iv(const GLint * @v) => glColor4ivPtr(@v);

        internal delegate void glColor4sFunc(GLshort @red, GLshort @green, GLshort @blue, GLshort @alpha);
        internal static glColor4sFunc glColor4sPtr;
        internal static void loadColor4s()
        {
            try
            {
                glColor4sPtr = (glColor4sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4s"), typeof(glColor4sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4s'.");
            }
        }
        public static void glColor4s(GLshort @red, GLshort @green, GLshort @blue, GLshort @alpha) => glColor4sPtr(@red, @green, @blue, @alpha);

        internal delegate void glColor4svFunc(const GLshort * @v);
        internal static glColor4svFunc glColor4svPtr;
        internal static void loadColor4sv()
        {
            try
            {
                glColor4svPtr = (glColor4svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4sv"), typeof(glColor4svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4sv'.");
            }
        }
        public static void glColor4sv(const GLshort * @v) => glColor4svPtr(@v);

        internal delegate void glColor4ubFunc(GLubyte @red, GLubyte @green, GLubyte @blue, GLubyte @alpha);
        internal static glColor4ubFunc glColor4ubPtr;
        internal static void loadColor4ub()
        {
            try
            {
                glColor4ubPtr = (glColor4ubFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4ub"), typeof(glColor4ubFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4ub'.");
            }
        }
        public static void glColor4ub(GLubyte @red, GLubyte @green, GLubyte @blue, GLubyte @alpha) => glColor4ubPtr(@red, @green, @blue, @alpha);

        internal delegate void glColor4ubvFunc(const GLubyte * @v);
        internal static glColor4ubvFunc glColor4ubvPtr;
        internal static void loadColor4ubv()
        {
            try
            {
                glColor4ubvPtr = (glColor4ubvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4ubv"), typeof(glColor4ubvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4ubv'.");
            }
        }
        public static void glColor4ubv(const GLubyte * @v) => glColor4ubvPtr(@v);

        internal delegate void glColor4uiFunc(GLuint @red, GLuint @green, GLuint @blue, GLuint @alpha);
        internal static glColor4uiFunc glColor4uiPtr;
        internal static void loadColor4ui()
        {
            try
            {
                glColor4uiPtr = (glColor4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4ui"), typeof(glColor4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4ui'.");
            }
        }
        public static void glColor4ui(GLuint @red, GLuint @green, GLuint @blue, GLuint @alpha) => glColor4uiPtr(@red, @green, @blue, @alpha);

        internal delegate void glColor4uivFunc(const GLuint * @v);
        internal static glColor4uivFunc glColor4uivPtr;
        internal static void loadColor4uiv()
        {
            try
            {
                glColor4uivPtr = (glColor4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4uiv"), typeof(glColor4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4uiv'.");
            }
        }
        public static void glColor4uiv(const GLuint * @v) => glColor4uivPtr(@v);

        internal delegate void glColor4usFunc(GLushort @red, GLushort @green, GLushort @blue, GLushort @alpha);
        internal static glColor4usFunc glColor4usPtr;
        internal static void loadColor4us()
        {
            try
            {
                glColor4usPtr = (glColor4usFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4us"), typeof(glColor4usFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4us'.");
            }
        }
        public static void glColor4us(GLushort @red, GLushort @green, GLushort @blue, GLushort @alpha) => glColor4usPtr(@red, @green, @blue, @alpha);

        internal delegate void glColor4usvFunc(const GLushort * @v);
        internal static glColor4usvFunc glColor4usvPtr;
        internal static void loadColor4usv()
        {
            try
            {
                glColor4usvPtr = (glColor4usvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4usv"), typeof(glColor4usvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4usv'.");
            }
        }
        public static void glColor4usv(const GLushort * @v) => glColor4usvPtr(@v);

        internal delegate void glEdgeFlagFunc(GLboolean @flag);
        internal static glEdgeFlagFunc glEdgeFlagPtr;
        internal static void loadEdgeFlag()
        {
            try
            {
                glEdgeFlagPtr = (glEdgeFlagFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEdgeFlag"), typeof(glEdgeFlagFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEdgeFlag'.");
            }
        }
        public static void glEdgeFlag(GLboolean @flag) => glEdgeFlagPtr(@flag);

        internal delegate void glEdgeFlagvFunc(const GLboolean * @flag);
        internal static glEdgeFlagvFunc glEdgeFlagvPtr;
        internal static void loadEdgeFlagv()
        {
            try
            {
                glEdgeFlagvPtr = (glEdgeFlagvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEdgeFlagv"), typeof(glEdgeFlagvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEdgeFlagv'.");
            }
        }
        public static void glEdgeFlagv(const GLboolean * @flag) => glEdgeFlagvPtr(@flag);

        internal delegate void glEndFunc();
        internal static glEndFunc glEndPtr;
        internal static void loadEnd()
        {
            try
            {
                glEndPtr = (glEndFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnd"), typeof(glEndFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnd'.");
            }
        }
        public static void glEnd() => glEndPtr();

        internal delegate void glIndexdFunc(GLdouble @c);
        internal static glIndexdFunc glIndexdPtr;
        internal static void loadIndexd()
        {
            try
            {
                glIndexdPtr = (glIndexdFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexd"), typeof(glIndexdFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexd'.");
            }
        }
        public static void glIndexd(GLdouble @c) => glIndexdPtr(@c);

        internal delegate void glIndexdvFunc(const GLdouble * @c);
        internal static glIndexdvFunc glIndexdvPtr;
        internal static void loadIndexdv()
        {
            try
            {
                glIndexdvPtr = (glIndexdvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexdv"), typeof(glIndexdvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexdv'.");
            }
        }
        public static void glIndexdv(const GLdouble * @c) => glIndexdvPtr(@c);

        internal delegate void glIndexfFunc(GLfloat @c);
        internal static glIndexfFunc glIndexfPtr;
        internal static void loadIndexf()
        {
            try
            {
                glIndexfPtr = (glIndexfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexf"), typeof(glIndexfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexf'.");
            }
        }
        public static void glIndexf(GLfloat @c) => glIndexfPtr(@c);

        internal delegate void glIndexfvFunc(const GLfloat * @c);
        internal static glIndexfvFunc glIndexfvPtr;
        internal static void loadIndexfv()
        {
            try
            {
                glIndexfvPtr = (glIndexfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexfv"), typeof(glIndexfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexfv'.");
            }
        }
        public static void glIndexfv(const GLfloat * @c) => glIndexfvPtr(@c);

        internal delegate void glIndexiFunc(GLint @c);
        internal static glIndexiFunc glIndexiPtr;
        internal static void loadIndexi()
        {
            try
            {
                glIndexiPtr = (glIndexiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexi"), typeof(glIndexiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexi'.");
            }
        }
        public static void glIndexi(GLint @c) => glIndexiPtr(@c);

        internal delegate void glIndexivFunc(const GLint * @c);
        internal static glIndexivFunc glIndexivPtr;
        internal static void loadIndexiv()
        {
            try
            {
                glIndexivPtr = (glIndexivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexiv"), typeof(glIndexivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexiv'.");
            }
        }
        public static void glIndexiv(const GLint * @c) => glIndexivPtr(@c);

        internal delegate void glIndexsFunc(GLshort @c);
        internal static glIndexsFunc glIndexsPtr;
        internal static void loadIndexs()
        {
            try
            {
                glIndexsPtr = (glIndexsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexs"), typeof(glIndexsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexs'.");
            }
        }
        public static void glIndexs(GLshort @c) => glIndexsPtr(@c);

        internal delegate void glIndexsvFunc(const GLshort * @c);
        internal static glIndexsvFunc glIndexsvPtr;
        internal static void loadIndexsv()
        {
            try
            {
                glIndexsvPtr = (glIndexsvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexsv"), typeof(glIndexsvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexsv'.");
            }
        }
        public static void glIndexsv(const GLshort * @c) => glIndexsvPtr(@c);

        internal delegate void glNormal3bFunc(GLbyte @nx, GLbyte @ny, GLbyte @nz);
        internal static glNormal3bFunc glNormal3bPtr;
        internal static void loadNormal3b()
        {
            try
            {
                glNormal3bPtr = (glNormal3bFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3b"), typeof(glNormal3bFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3b'.");
            }
        }
        public static void glNormal3b(GLbyte @nx, GLbyte @ny, GLbyte @nz) => glNormal3bPtr(@nx, @ny, @nz);

        internal delegate void glNormal3bvFunc(const GLbyte * @v);
        internal static glNormal3bvFunc glNormal3bvPtr;
        internal static void loadNormal3bv()
        {
            try
            {
                glNormal3bvPtr = (glNormal3bvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3bv"), typeof(glNormal3bvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3bv'.");
            }
        }
        public static void glNormal3bv(const GLbyte * @v) => glNormal3bvPtr(@v);

        internal delegate void glNormal3dFunc(GLdouble @nx, GLdouble @ny, GLdouble @nz);
        internal static glNormal3dFunc glNormal3dPtr;
        internal static void loadNormal3d()
        {
            try
            {
                glNormal3dPtr = (glNormal3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3d"), typeof(glNormal3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3d'.");
            }
        }
        public static void glNormal3d(GLdouble @nx, GLdouble @ny, GLdouble @nz) => glNormal3dPtr(@nx, @ny, @nz);

        internal delegate void glNormal3dvFunc(const GLdouble * @v);
        internal static glNormal3dvFunc glNormal3dvPtr;
        internal static void loadNormal3dv()
        {
            try
            {
                glNormal3dvPtr = (glNormal3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3dv"), typeof(glNormal3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3dv'.");
            }
        }
        public static void glNormal3dv(const GLdouble * @v) => glNormal3dvPtr(@v);

        internal delegate void glNormal3fFunc(GLfloat @nx, GLfloat @ny, GLfloat @nz);
        internal static glNormal3fFunc glNormal3fPtr;
        internal static void loadNormal3f()
        {
            try
            {
                glNormal3fPtr = (glNormal3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3f"), typeof(glNormal3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3f'.");
            }
        }
        public static void glNormal3f(GLfloat @nx, GLfloat @ny, GLfloat @nz) => glNormal3fPtr(@nx, @ny, @nz);

        internal delegate void glNormal3fvFunc(const GLfloat * @v);
        internal static glNormal3fvFunc glNormal3fvPtr;
        internal static void loadNormal3fv()
        {
            try
            {
                glNormal3fvPtr = (glNormal3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3fv"), typeof(glNormal3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3fv'.");
            }
        }
        public static void glNormal3fv(const GLfloat * @v) => glNormal3fvPtr(@v);

        internal delegate void glNormal3iFunc(GLint @nx, GLint @ny, GLint @nz);
        internal static glNormal3iFunc glNormal3iPtr;
        internal static void loadNormal3i()
        {
            try
            {
                glNormal3iPtr = (glNormal3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3i"), typeof(glNormal3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3i'.");
            }
        }
        public static void glNormal3i(GLint @nx, GLint @ny, GLint @nz) => glNormal3iPtr(@nx, @ny, @nz);

        internal delegate void glNormal3ivFunc(const GLint * @v);
        internal static glNormal3ivFunc glNormal3ivPtr;
        internal static void loadNormal3iv()
        {
            try
            {
                glNormal3ivPtr = (glNormal3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3iv"), typeof(glNormal3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3iv'.");
            }
        }
        public static void glNormal3iv(const GLint * @v) => glNormal3ivPtr(@v);

        internal delegate void glNormal3sFunc(GLshort @nx, GLshort @ny, GLshort @nz);
        internal static glNormal3sFunc glNormal3sPtr;
        internal static void loadNormal3s()
        {
            try
            {
                glNormal3sPtr = (glNormal3sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3s"), typeof(glNormal3sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3s'.");
            }
        }
        public static void glNormal3s(GLshort @nx, GLshort @ny, GLshort @nz) => glNormal3sPtr(@nx, @ny, @nz);

        internal delegate void glNormal3svFunc(const GLshort * @v);
        internal static glNormal3svFunc glNormal3svPtr;
        internal static void loadNormal3sv()
        {
            try
            {
                glNormal3svPtr = (glNormal3svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3sv"), typeof(glNormal3svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3sv'.");
            }
        }
        public static void glNormal3sv(const GLshort * @v) => glNormal3svPtr(@v);

        internal delegate void glRasterPos2dFunc(GLdouble @x, GLdouble @y);
        internal static glRasterPos2dFunc glRasterPos2dPtr;
        internal static void loadRasterPos2d()
        {
            try
            {
                glRasterPos2dPtr = (glRasterPos2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos2d"), typeof(glRasterPos2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos2d'.");
            }
        }
        public static void glRasterPos2d(GLdouble @x, GLdouble @y) => glRasterPos2dPtr(@x, @y);

        internal delegate void glRasterPos2dvFunc(const GLdouble * @v);
        internal static glRasterPos2dvFunc glRasterPos2dvPtr;
        internal static void loadRasterPos2dv()
        {
            try
            {
                glRasterPos2dvPtr = (glRasterPos2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos2dv"), typeof(glRasterPos2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos2dv'.");
            }
        }
        public static void glRasterPos2dv(const GLdouble * @v) => glRasterPos2dvPtr(@v);

        internal delegate void glRasterPos2fFunc(GLfloat @x, GLfloat @y);
        internal static glRasterPos2fFunc glRasterPos2fPtr;
        internal static void loadRasterPos2f()
        {
            try
            {
                glRasterPos2fPtr = (glRasterPos2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos2f"), typeof(glRasterPos2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos2f'.");
            }
        }
        public static void glRasterPos2f(GLfloat @x, GLfloat @y) => glRasterPos2fPtr(@x, @y);

        internal delegate void glRasterPos2fvFunc(const GLfloat * @v);
        internal static glRasterPos2fvFunc glRasterPos2fvPtr;
        internal static void loadRasterPos2fv()
        {
            try
            {
                glRasterPos2fvPtr = (glRasterPos2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos2fv"), typeof(glRasterPos2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos2fv'.");
            }
        }
        public static void glRasterPos2fv(const GLfloat * @v) => glRasterPos2fvPtr(@v);

        internal delegate void glRasterPos2iFunc(GLint @x, GLint @y);
        internal static glRasterPos2iFunc glRasterPos2iPtr;
        internal static void loadRasterPos2i()
        {
            try
            {
                glRasterPos2iPtr = (glRasterPos2iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos2i"), typeof(glRasterPos2iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos2i'.");
            }
        }
        public static void glRasterPos2i(GLint @x, GLint @y) => glRasterPos2iPtr(@x, @y);

        internal delegate void glRasterPos2ivFunc(const GLint * @v);
        internal static glRasterPos2ivFunc glRasterPos2ivPtr;
        internal static void loadRasterPos2iv()
        {
            try
            {
                glRasterPos2ivPtr = (glRasterPos2ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos2iv"), typeof(glRasterPos2ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos2iv'.");
            }
        }
        public static void glRasterPos2iv(const GLint * @v) => glRasterPos2ivPtr(@v);

        internal delegate void glRasterPos2sFunc(GLshort @x, GLshort @y);
        internal static glRasterPos2sFunc glRasterPos2sPtr;
        internal static void loadRasterPos2s()
        {
            try
            {
                glRasterPos2sPtr = (glRasterPos2sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos2s"), typeof(glRasterPos2sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos2s'.");
            }
        }
        public static void glRasterPos2s(GLshort @x, GLshort @y) => glRasterPos2sPtr(@x, @y);

        internal delegate void glRasterPos2svFunc(const GLshort * @v);
        internal static glRasterPos2svFunc glRasterPos2svPtr;
        internal static void loadRasterPos2sv()
        {
            try
            {
                glRasterPos2svPtr = (glRasterPos2svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos2sv"), typeof(glRasterPos2svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos2sv'.");
            }
        }
        public static void glRasterPos2sv(const GLshort * @v) => glRasterPos2svPtr(@v);

        internal delegate void glRasterPos3dFunc(GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glRasterPos3dFunc glRasterPos3dPtr;
        internal static void loadRasterPos3d()
        {
            try
            {
                glRasterPos3dPtr = (glRasterPos3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos3d"), typeof(glRasterPos3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos3d'.");
            }
        }
        public static void glRasterPos3d(GLdouble @x, GLdouble @y, GLdouble @z) => glRasterPos3dPtr(@x, @y, @z);

        internal delegate void glRasterPos3dvFunc(const GLdouble * @v);
        internal static glRasterPos3dvFunc glRasterPos3dvPtr;
        internal static void loadRasterPos3dv()
        {
            try
            {
                glRasterPos3dvPtr = (glRasterPos3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos3dv"), typeof(glRasterPos3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos3dv'.");
            }
        }
        public static void glRasterPos3dv(const GLdouble * @v) => glRasterPos3dvPtr(@v);

        internal delegate void glRasterPos3fFunc(GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glRasterPos3fFunc glRasterPos3fPtr;
        internal static void loadRasterPos3f()
        {
            try
            {
                glRasterPos3fPtr = (glRasterPos3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos3f"), typeof(glRasterPos3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos3f'.");
            }
        }
        public static void glRasterPos3f(GLfloat @x, GLfloat @y, GLfloat @z) => glRasterPos3fPtr(@x, @y, @z);

        internal delegate void glRasterPos3fvFunc(const GLfloat * @v);
        internal static glRasterPos3fvFunc glRasterPos3fvPtr;
        internal static void loadRasterPos3fv()
        {
            try
            {
                glRasterPos3fvPtr = (glRasterPos3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos3fv"), typeof(glRasterPos3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos3fv'.");
            }
        }
        public static void glRasterPos3fv(const GLfloat * @v) => glRasterPos3fvPtr(@v);

        internal delegate void glRasterPos3iFunc(GLint @x, GLint @y, GLint @z);
        internal static glRasterPos3iFunc glRasterPos3iPtr;
        internal static void loadRasterPos3i()
        {
            try
            {
                glRasterPos3iPtr = (glRasterPos3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos3i"), typeof(glRasterPos3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos3i'.");
            }
        }
        public static void glRasterPos3i(GLint @x, GLint @y, GLint @z) => glRasterPos3iPtr(@x, @y, @z);

        internal delegate void glRasterPos3ivFunc(const GLint * @v);
        internal static glRasterPos3ivFunc glRasterPos3ivPtr;
        internal static void loadRasterPos3iv()
        {
            try
            {
                glRasterPos3ivPtr = (glRasterPos3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos3iv"), typeof(glRasterPos3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos3iv'.");
            }
        }
        public static void glRasterPos3iv(const GLint * @v) => glRasterPos3ivPtr(@v);

        internal delegate void glRasterPos3sFunc(GLshort @x, GLshort @y, GLshort @z);
        internal static glRasterPos3sFunc glRasterPos3sPtr;
        internal static void loadRasterPos3s()
        {
            try
            {
                glRasterPos3sPtr = (glRasterPos3sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos3s"), typeof(glRasterPos3sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos3s'.");
            }
        }
        public static void glRasterPos3s(GLshort @x, GLshort @y, GLshort @z) => glRasterPos3sPtr(@x, @y, @z);

        internal delegate void glRasterPos3svFunc(const GLshort * @v);
        internal static glRasterPos3svFunc glRasterPos3svPtr;
        internal static void loadRasterPos3sv()
        {
            try
            {
                glRasterPos3svPtr = (glRasterPos3svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos3sv"), typeof(glRasterPos3svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos3sv'.");
            }
        }
        public static void glRasterPos3sv(const GLshort * @v) => glRasterPos3svPtr(@v);

        internal delegate void glRasterPos4dFunc(GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glRasterPos4dFunc glRasterPos4dPtr;
        internal static void loadRasterPos4d()
        {
            try
            {
                glRasterPos4dPtr = (glRasterPos4dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos4d"), typeof(glRasterPos4dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos4d'.");
            }
        }
        public static void glRasterPos4d(GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glRasterPos4dPtr(@x, @y, @z, @w);

        internal delegate void glRasterPos4dvFunc(const GLdouble * @v);
        internal static glRasterPos4dvFunc glRasterPos4dvPtr;
        internal static void loadRasterPos4dv()
        {
            try
            {
                glRasterPos4dvPtr = (glRasterPos4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos4dv"), typeof(glRasterPos4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos4dv'.");
            }
        }
        public static void glRasterPos4dv(const GLdouble * @v) => glRasterPos4dvPtr(@v);

        internal delegate void glRasterPos4fFunc(GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glRasterPos4fFunc glRasterPos4fPtr;
        internal static void loadRasterPos4f()
        {
            try
            {
                glRasterPos4fPtr = (glRasterPos4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos4f"), typeof(glRasterPos4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos4f'.");
            }
        }
        public static void glRasterPos4f(GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glRasterPos4fPtr(@x, @y, @z, @w);

        internal delegate void glRasterPos4fvFunc(const GLfloat * @v);
        internal static glRasterPos4fvFunc glRasterPos4fvPtr;
        internal static void loadRasterPos4fv()
        {
            try
            {
                glRasterPos4fvPtr = (glRasterPos4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos4fv"), typeof(glRasterPos4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos4fv'.");
            }
        }
        public static void glRasterPos4fv(const GLfloat * @v) => glRasterPos4fvPtr(@v);

        internal delegate void glRasterPos4iFunc(GLint @x, GLint @y, GLint @z, GLint @w);
        internal static glRasterPos4iFunc glRasterPos4iPtr;
        internal static void loadRasterPos4i()
        {
            try
            {
                glRasterPos4iPtr = (glRasterPos4iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos4i"), typeof(glRasterPos4iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos4i'.");
            }
        }
        public static void glRasterPos4i(GLint @x, GLint @y, GLint @z, GLint @w) => glRasterPos4iPtr(@x, @y, @z, @w);

        internal delegate void glRasterPos4ivFunc(const GLint * @v);
        internal static glRasterPos4ivFunc glRasterPos4ivPtr;
        internal static void loadRasterPos4iv()
        {
            try
            {
                glRasterPos4ivPtr = (glRasterPos4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos4iv"), typeof(glRasterPos4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos4iv'.");
            }
        }
        public static void glRasterPos4iv(const GLint * @v) => glRasterPos4ivPtr(@v);

        internal delegate void glRasterPos4sFunc(GLshort @x, GLshort @y, GLshort @z, GLshort @w);
        internal static glRasterPos4sFunc glRasterPos4sPtr;
        internal static void loadRasterPos4s()
        {
            try
            {
                glRasterPos4sPtr = (glRasterPos4sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos4s"), typeof(glRasterPos4sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos4s'.");
            }
        }
        public static void glRasterPos4s(GLshort @x, GLshort @y, GLshort @z, GLshort @w) => glRasterPos4sPtr(@x, @y, @z, @w);

        internal delegate void glRasterPos4svFunc(const GLshort * @v);
        internal static glRasterPos4svFunc glRasterPos4svPtr;
        internal static void loadRasterPos4sv()
        {
            try
            {
                glRasterPos4svPtr = (glRasterPos4svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos4sv"), typeof(glRasterPos4svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos4sv'.");
            }
        }
        public static void glRasterPos4sv(const GLshort * @v) => glRasterPos4svPtr(@v);

        internal delegate void glRectdFunc(GLdouble @x1, GLdouble @y1, GLdouble @x2, GLdouble @y2);
        internal static glRectdFunc glRectdPtr;
        internal static void loadRectd()
        {
            try
            {
                glRectdPtr = (glRectdFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRectd"), typeof(glRectdFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRectd'.");
            }
        }
        public static void glRectd(GLdouble @x1, GLdouble @y1, GLdouble @x2, GLdouble @y2) => glRectdPtr(@x1, @y1, @x2, @y2);

        internal delegate void glRectdvFunc(const GLdouble * @v1, const GLdouble * @v2);
        internal static glRectdvFunc glRectdvPtr;
        internal static void loadRectdv()
        {
            try
            {
                glRectdvPtr = (glRectdvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRectdv"), typeof(glRectdvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRectdv'.");
            }
        }
        public static void glRectdv(const GLdouble * @v1, const GLdouble * @v2) => glRectdvPtr(@v1, @v2);

        internal delegate void glRectfFunc(GLfloat @x1, GLfloat @y1, GLfloat @x2, GLfloat @y2);
        internal static glRectfFunc glRectfPtr;
        internal static void loadRectf()
        {
            try
            {
                glRectfPtr = (glRectfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRectf"), typeof(glRectfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRectf'.");
            }
        }
        public static void glRectf(GLfloat @x1, GLfloat @y1, GLfloat @x2, GLfloat @y2) => glRectfPtr(@x1, @y1, @x2, @y2);

        internal delegate void glRectfvFunc(const GLfloat * @v1, const GLfloat * @v2);
        internal static glRectfvFunc glRectfvPtr;
        internal static void loadRectfv()
        {
            try
            {
                glRectfvPtr = (glRectfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRectfv"), typeof(glRectfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRectfv'.");
            }
        }
        public static void glRectfv(const GLfloat * @v1, const GLfloat * @v2) => glRectfvPtr(@v1, @v2);

        internal delegate void glRectiFunc(GLint @x1, GLint @y1, GLint @x2, GLint @y2);
        internal static glRectiFunc glRectiPtr;
        internal static void loadRecti()
        {
            try
            {
                glRectiPtr = (glRectiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRecti"), typeof(glRectiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRecti'.");
            }
        }
        public static void glRecti(GLint @x1, GLint @y1, GLint @x2, GLint @y2) => glRectiPtr(@x1, @y1, @x2, @y2);

        internal delegate void glRectivFunc(const GLint * @v1, const GLint * @v2);
        internal static glRectivFunc glRectivPtr;
        internal static void loadRectiv()
        {
            try
            {
                glRectivPtr = (glRectivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRectiv"), typeof(glRectivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRectiv'.");
            }
        }
        public static void glRectiv(const GLint * @v1, const GLint * @v2) => glRectivPtr(@v1, @v2);

        internal delegate void glRectsFunc(GLshort @x1, GLshort @y1, GLshort @x2, GLshort @y2);
        internal static glRectsFunc glRectsPtr;
        internal static void loadRects()
        {
            try
            {
                glRectsPtr = (glRectsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRects"), typeof(glRectsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRects'.");
            }
        }
        public static void glRects(GLshort @x1, GLshort @y1, GLshort @x2, GLshort @y2) => glRectsPtr(@x1, @y1, @x2, @y2);

        internal delegate void glRectsvFunc(const GLshort * @v1, const GLshort * @v2);
        internal static glRectsvFunc glRectsvPtr;
        internal static void loadRectsv()
        {
            try
            {
                glRectsvPtr = (glRectsvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRectsv"), typeof(glRectsvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRectsv'.");
            }
        }
        public static void glRectsv(const GLshort * @v1, const GLshort * @v2) => glRectsvPtr(@v1, @v2);

        internal delegate void glTexCoord1dFunc(GLdouble @s);
        internal static glTexCoord1dFunc glTexCoord1dPtr;
        internal static void loadTexCoord1d()
        {
            try
            {
                glTexCoord1dPtr = (glTexCoord1dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1d"), typeof(glTexCoord1dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1d'.");
            }
        }
        public static void glTexCoord1d(GLdouble @s) => glTexCoord1dPtr(@s);

        internal delegate void glTexCoord1dvFunc(const GLdouble * @v);
        internal static glTexCoord1dvFunc glTexCoord1dvPtr;
        internal static void loadTexCoord1dv()
        {
            try
            {
                glTexCoord1dvPtr = (glTexCoord1dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1dv"), typeof(glTexCoord1dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1dv'.");
            }
        }
        public static void glTexCoord1dv(const GLdouble * @v) => glTexCoord1dvPtr(@v);

        internal delegate void glTexCoord1fFunc(GLfloat @s);
        internal static glTexCoord1fFunc glTexCoord1fPtr;
        internal static void loadTexCoord1f()
        {
            try
            {
                glTexCoord1fPtr = (glTexCoord1fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1f"), typeof(glTexCoord1fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1f'.");
            }
        }
        public static void glTexCoord1f(GLfloat @s) => glTexCoord1fPtr(@s);

        internal delegate void glTexCoord1fvFunc(const GLfloat * @v);
        internal static glTexCoord1fvFunc glTexCoord1fvPtr;
        internal static void loadTexCoord1fv()
        {
            try
            {
                glTexCoord1fvPtr = (glTexCoord1fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1fv"), typeof(glTexCoord1fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1fv'.");
            }
        }
        public static void glTexCoord1fv(const GLfloat * @v) => glTexCoord1fvPtr(@v);

        internal delegate void glTexCoord1iFunc(GLint @s);
        internal static glTexCoord1iFunc glTexCoord1iPtr;
        internal static void loadTexCoord1i()
        {
            try
            {
                glTexCoord1iPtr = (glTexCoord1iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1i"), typeof(glTexCoord1iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1i'.");
            }
        }
        public static void glTexCoord1i(GLint @s) => glTexCoord1iPtr(@s);

        internal delegate void glTexCoord1ivFunc(const GLint * @v);
        internal static glTexCoord1ivFunc glTexCoord1ivPtr;
        internal static void loadTexCoord1iv()
        {
            try
            {
                glTexCoord1ivPtr = (glTexCoord1ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1iv"), typeof(glTexCoord1ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1iv'.");
            }
        }
        public static void glTexCoord1iv(const GLint * @v) => glTexCoord1ivPtr(@v);

        internal delegate void glTexCoord1sFunc(GLshort @s);
        internal static glTexCoord1sFunc glTexCoord1sPtr;
        internal static void loadTexCoord1s()
        {
            try
            {
                glTexCoord1sPtr = (glTexCoord1sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1s"), typeof(glTexCoord1sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1s'.");
            }
        }
        public static void glTexCoord1s(GLshort @s) => glTexCoord1sPtr(@s);

        internal delegate void glTexCoord1svFunc(const GLshort * @v);
        internal static glTexCoord1svFunc glTexCoord1svPtr;
        internal static void loadTexCoord1sv()
        {
            try
            {
                glTexCoord1svPtr = (glTexCoord1svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1sv"), typeof(glTexCoord1svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1sv'.");
            }
        }
        public static void glTexCoord1sv(const GLshort * @v) => glTexCoord1svPtr(@v);

        internal delegate void glTexCoord2dFunc(GLdouble @s, GLdouble @t);
        internal static glTexCoord2dFunc glTexCoord2dPtr;
        internal static void loadTexCoord2d()
        {
            try
            {
                glTexCoord2dPtr = (glTexCoord2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2d"), typeof(glTexCoord2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2d'.");
            }
        }
        public static void glTexCoord2d(GLdouble @s, GLdouble @t) => glTexCoord2dPtr(@s, @t);

        internal delegate void glTexCoord2dvFunc(const GLdouble * @v);
        internal static glTexCoord2dvFunc glTexCoord2dvPtr;
        internal static void loadTexCoord2dv()
        {
            try
            {
                glTexCoord2dvPtr = (glTexCoord2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2dv"), typeof(glTexCoord2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2dv'.");
            }
        }
        public static void glTexCoord2dv(const GLdouble * @v) => glTexCoord2dvPtr(@v);

        internal delegate void glTexCoord2fFunc(GLfloat @s, GLfloat @t);
        internal static glTexCoord2fFunc glTexCoord2fPtr;
        internal static void loadTexCoord2f()
        {
            try
            {
                glTexCoord2fPtr = (glTexCoord2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2f"), typeof(glTexCoord2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2f'.");
            }
        }
        public static void glTexCoord2f(GLfloat @s, GLfloat @t) => glTexCoord2fPtr(@s, @t);

        internal delegate void glTexCoord2fvFunc(const GLfloat * @v);
        internal static glTexCoord2fvFunc glTexCoord2fvPtr;
        internal static void loadTexCoord2fv()
        {
            try
            {
                glTexCoord2fvPtr = (glTexCoord2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2fv"), typeof(glTexCoord2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2fv'.");
            }
        }
        public static void glTexCoord2fv(const GLfloat * @v) => glTexCoord2fvPtr(@v);

        internal delegate void glTexCoord2iFunc(GLint @s, GLint @t);
        internal static glTexCoord2iFunc glTexCoord2iPtr;
        internal static void loadTexCoord2i()
        {
            try
            {
                glTexCoord2iPtr = (glTexCoord2iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2i"), typeof(glTexCoord2iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2i'.");
            }
        }
        public static void glTexCoord2i(GLint @s, GLint @t) => glTexCoord2iPtr(@s, @t);

        internal delegate void glTexCoord2ivFunc(const GLint * @v);
        internal static glTexCoord2ivFunc glTexCoord2ivPtr;
        internal static void loadTexCoord2iv()
        {
            try
            {
                glTexCoord2ivPtr = (glTexCoord2ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2iv"), typeof(glTexCoord2ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2iv'.");
            }
        }
        public static void glTexCoord2iv(const GLint * @v) => glTexCoord2ivPtr(@v);

        internal delegate void glTexCoord2sFunc(GLshort @s, GLshort @t);
        internal static glTexCoord2sFunc glTexCoord2sPtr;
        internal static void loadTexCoord2s()
        {
            try
            {
                glTexCoord2sPtr = (glTexCoord2sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2s"), typeof(glTexCoord2sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2s'.");
            }
        }
        public static void glTexCoord2s(GLshort @s, GLshort @t) => glTexCoord2sPtr(@s, @t);

        internal delegate void glTexCoord2svFunc(const GLshort * @v);
        internal static glTexCoord2svFunc glTexCoord2svPtr;
        internal static void loadTexCoord2sv()
        {
            try
            {
                glTexCoord2svPtr = (glTexCoord2svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2sv"), typeof(glTexCoord2svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2sv'.");
            }
        }
        public static void glTexCoord2sv(const GLshort * @v) => glTexCoord2svPtr(@v);

        internal delegate void glTexCoord3dFunc(GLdouble @s, GLdouble @t, GLdouble @r);
        internal static glTexCoord3dFunc glTexCoord3dPtr;
        internal static void loadTexCoord3d()
        {
            try
            {
                glTexCoord3dPtr = (glTexCoord3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3d"), typeof(glTexCoord3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3d'.");
            }
        }
        public static void glTexCoord3d(GLdouble @s, GLdouble @t, GLdouble @r) => glTexCoord3dPtr(@s, @t, @r);

        internal delegate void glTexCoord3dvFunc(const GLdouble * @v);
        internal static glTexCoord3dvFunc glTexCoord3dvPtr;
        internal static void loadTexCoord3dv()
        {
            try
            {
                glTexCoord3dvPtr = (glTexCoord3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3dv"), typeof(glTexCoord3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3dv'.");
            }
        }
        public static void glTexCoord3dv(const GLdouble * @v) => glTexCoord3dvPtr(@v);

        internal delegate void glTexCoord3fFunc(GLfloat @s, GLfloat @t, GLfloat @r);
        internal static glTexCoord3fFunc glTexCoord3fPtr;
        internal static void loadTexCoord3f()
        {
            try
            {
                glTexCoord3fPtr = (glTexCoord3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3f"), typeof(glTexCoord3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3f'.");
            }
        }
        public static void glTexCoord3f(GLfloat @s, GLfloat @t, GLfloat @r) => glTexCoord3fPtr(@s, @t, @r);

        internal delegate void glTexCoord3fvFunc(const GLfloat * @v);
        internal static glTexCoord3fvFunc glTexCoord3fvPtr;
        internal static void loadTexCoord3fv()
        {
            try
            {
                glTexCoord3fvPtr = (glTexCoord3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3fv"), typeof(glTexCoord3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3fv'.");
            }
        }
        public static void glTexCoord3fv(const GLfloat * @v) => glTexCoord3fvPtr(@v);

        internal delegate void glTexCoord3iFunc(GLint @s, GLint @t, GLint @r);
        internal static glTexCoord3iFunc glTexCoord3iPtr;
        internal static void loadTexCoord3i()
        {
            try
            {
                glTexCoord3iPtr = (glTexCoord3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3i"), typeof(glTexCoord3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3i'.");
            }
        }
        public static void glTexCoord3i(GLint @s, GLint @t, GLint @r) => glTexCoord3iPtr(@s, @t, @r);

        internal delegate void glTexCoord3ivFunc(const GLint * @v);
        internal static glTexCoord3ivFunc glTexCoord3ivPtr;
        internal static void loadTexCoord3iv()
        {
            try
            {
                glTexCoord3ivPtr = (glTexCoord3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3iv"), typeof(glTexCoord3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3iv'.");
            }
        }
        public static void glTexCoord3iv(const GLint * @v) => glTexCoord3ivPtr(@v);

        internal delegate void glTexCoord3sFunc(GLshort @s, GLshort @t, GLshort @r);
        internal static glTexCoord3sFunc glTexCoord3sPtr;
        internal static void loadTexCoord3s()
        {
            try
            {
                glTexCoord3sPtr = (glTexCoord3sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3s"), typeof(glTexCoord3sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3s'.");
            }
        }
        public static void glTexCoord3s(GLshort @s, GLshort @t, GLshort @r) => glTexCoord3sPtr(@s, @t, @r);

        internal delegate void glTexCoord3svFunc(const GLshort * @v);
        internal static glTexCoord3svFunc glTexCoord3svPtr;
        internal static void loadTexCoord3sv()
        {
            try
            {
                glTexCoord3svPtr = (glTexCoord3svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3sv"), typeof(glTexCoord3svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3sv'.");
            }
        }
        public static void glTexCoord3sv(const GLshort * @v) => glTexCoord3svPtr(@v);

        internal delegate void glTexCoord4dFunc(GLdouble @s, GLdouble @t, GLdouble @r, GLdouble @q);
        internal static glTexCoord4dFunc glTexCoord4dPtr;
        internal static void loadTexCoord4d()
        {
            try
            {
                glTexCoord4dPtr = (glTexCoord4dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4d"), typeof(glTexCoord4dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4d'.");
            }
        }
        public static void glTexCoord4d(GLdouble @s, GLdouble @t, GLdouble @r, GLdouble @q) => glTexCoord4dPtr(@s, @t, @r, @q);

        internal delegate void glTexCoord4dvFunc(const GLdouble * @v);
        internal static glTexCoord4dvFunc glTexCoord4dvPtr;
        internal static void loadTexCoord4dv()
        {
            try
            {
                glTexCoord4dvPtr = (glTexCoord4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4dv"), typeof(glTexCoord4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4dv'.");
            }
        }
        public static void glTexCoord4dv(const GLdouble * @v) => glTexCoord4dvPtr(@v);

        internal delegate void glTexCoord4fFunc(GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @q);
        internal static glTexCoord4fFunc glTexCoord4fPtr;
        internal static void loadTexCoord4f()
        {
            try
            {
                glTexCoord4fPtr = (glTexCoord4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4f"), typeof(glTexCoord4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4f'.");
            }
        }
        public static void glTexCoord4f(GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @q) => glTexCoord4fPtr(@s, @t, @r, @q);

        internal delegate void glTexCoord4fvFunc(const GLfloat * @v);
        internal static glTexCoord4fvFunc glTexCoord4fvPtr;
        internal static void loadTexCoord4fv()
        {
            try
            {
                glTexCoord4fvPtr = (glTexCoord4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4fv"), typeof(glTexCoord4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4fv'.");
            }
        }
        public static void glTexCoord4fv(const GLfloat * @v) => glTexCoord4fvPtr(@v);

        internal delegate void glTexCoord4iFunc(GLint @s, GLint @t, GLint @r, GLint @q);
        internal static glTexCoord4iFunc glTexCoord4iPtr;
        internal static void loadTexCoord4i()
        {
            try
            {
                glTexCoord4iPtr = (glTexCoord4iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4i"), typeof(glTexCoord4iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4i'.");
            }
        }
        public static void glTexCoord4i(GLint @s, GLint @t, GLint @r, GLint @q) => glTexCoord4iPtr(@s, @t, @r, @q);

        internal delegate void glTexCoord4ivFunc(const GLint * @v);
        internal static glTexCoord4ivFunc glTexCoord4ivPtr;
        internal static void loadTexCoord4iv()
        {
            try
            {
                glTexCoord4ivPtr = (glTexCoord4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4iv"), typeof(glTexCoord4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4iv'.");
            }
        }
        public static void glTexCoord4iv(const GLint * @v) => glTexCoord4ivPtr(@v);

        internal delegate void glTexCoord4sFunc(GLshort @s, GLshort @t, GLshort @r, GLshort @q);
        internal static glTexCoord4sFunc glTexCoord4sPtr;
        internal static void loadTexCoord4s()
        {
            try
            {
                glTexCoord4sPtr = (glTexCoord4sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4s"), typeof(glTexCoord4sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4s'.");
            }
        }
        public static void glTexCoord4s(GLshort @s, GLshort @t, GLshort @r, GLshort @q) => glTexCoord4sPtr(@s, @t, @r, @q);

        internal delegate void glTexCoord4svFunc(const GLshort * @v);
        internal static glTexCoord4svFunc glTexCoord4svPtr;
        internal static void loadTexCoord4sv()
        {
            try
            {
                glTexCoord4svPtr = (glTexCoord4svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4sv"), typeof(glTexCoord4svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4sv'.");
            }
        }
        public static void glTexCoord4sv(const GLshort * @v) => glTexCoord4svPtr(@v);

        internal delegate void glVertex2dFunc(GLdouble @x, GLdouble @y);
        internal static glVertex2dFunc glVertex2dPtr;
        internal static void loadVertex2d()
        {
            try
            {
                glVertex2dPtr = (glVertex2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2d"), typeof(glVertex2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2d'.");
            }
        }
        public static void glVertex2d(GLdouble @x, GLdouble @y) => glVertex2dPtr(@x, @y);

        internal delegate void glVertex2dvFunc(const GLdouble * @v);
        internal static glVertex2dvFunc glVertex2dvPtr;
        internal static void loadVertex2dv()
        {
            try
            {
                glVertex2dvPtr = (glVertex2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2dv"), typeof(glVertex2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2dv'.");
            }
        }
        public static void glVertex2dv(const GLdouble * @v) => glVertex2dvPtr(@v);

        internal delegate void glVertex2fFunc(GLfloat @x, GLfloat @y);
        internal static glVertex2fFunc glVertex2fPtr;
        internal static void loadVertex2f()
        {
            try
            {
                glVertex2fPtr = (glVertex2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2f"), typeof(glVertex2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2f'.");
            }
        }
        public static void glVertex2f(GLfloat @x, GLfloat @y) => glVertex2fPtr(@x, @y);

        internal delegate void glVertex2fvFunc(const GLfloat * @v);
        internal static glVertex2fvFunc glVertex2fvPtr;
        internal static void loadVertex2fv()
        {
            try
            {
                glVertex2fvPtr = (glVertex2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2fv"), typeof(glVertex2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2fv'.");
            }
        }
        public static void glVertex2fv(const GLfloat * @v) => glVertex2fvPtr(@v);

        internal delegate void glVertex2iFunc(GLint @x, GLint @y);
        internal static glVertex2iFunc glVertex2iPtr;
        internal static void loadVertex2i()
        {
            try
            {
                glVertex2iPtr = (glVertex2iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2i"), typeof(glVertex2iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2i'.");
            }
        }
        public static void glVertex2i(GLint @x, GLint @y) => glVertex2iPtr(@x, @y);

        internal delegate void glVertex2ivFunc(const GLint * @v);
        internal static glVertex2ivFunc glVertex2ivPtr;
        internal static void loadVertex2iv()
        {
            try
            {
                glVertex2ivPtr = (glVertex2ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2iv"), typeof(glVertex2ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2iv'.");
            }
        }
        public static void glVertex2iv(const GLint * @v) => glVertex2ivPtr(@v);

        internal delegate void glVertex2sFunc(GLshort @x, GLshort @y);
        internal static glVertex2sFunc glVertex2sPtr;
        internal static void loadVertex2s()
        {
            try
            {
                glVertex2sPtr = (glVertex2sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2s"), typeof(glVertex2sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2s'.");
            }
        }
        public static void glVertex2s(GLshort @x, GLshort @y) => glVertex2sPtr(@x, @y);

        internal delegate void glVertex2svFunc(const GLshort * @v);
        internal static glVertex2svFunc glVertex2svPtr;
        internal static void loadVertex2sv()
        {
            try
            {
                glVertex2svPtr = (glVertex2svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2sv"), typeof(glVertex2svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2sv'.");
            }
        }
        public static void glVertex2sv(const GLshort * @v) => glVertex2svPtr(@v);

        internal delegate void glVertex3dFunc(GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glVertex3dFunc glVertex3dPtr;
        internal static void loadVertex3d()
        {
            try
            {
                glVertex3dPtr = (glVertex3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3d"), typeof(glVertex3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3d'.");
            }
        }
        public static void glVertex3d(GLdouble @x, GLdouble @y, GLdouble @z) => glVertex3dPtr(@x, @y, @z);

        internal delegate void glVertex3dvFunc(const GLdouble * @v);
        internal static glVertex3dvFunc glVertex3dvPtr;
        internal static void loadVertex3dv()
        {
            try
            {
                glVertex3dvPtr = (glVertex3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3dv"), typeof(glVertex3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3dv'.");
            }
        }
        public static void glVertex3dv(const GLdouble * @v) => glVertex3dvPtr(@v);

        internal delegate void glVertex3fFunc(GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glVertex3fFunc glVertex3fPtr;
        internal static void loadVertex3f()
        {
            try
            {
                glVertex3fPtr = (glVertex3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3f"), typeof(glVertex3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3f'.");
            }
        }
        public static void glVertex3f(GLfloat @x, GLfloat @y, GLfloat @z) => glVertex3fPtr(@x, @y, @z);

        internal delegate void glVertex3fvFunc(const GLfloat * @v);
        internal static glVertex3fvFunc glVertex3fvPtr;
        internal static void loadVertex3fv()
        {
            try
            {
                glVertex3fvPtr = (glVertex3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3fv"), typeof(glVertex3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3fv'.");
            }
        }
        public static void glVertex3fv(const GLfloat * @v) => glVertex3fvPtr(@v);

        internal delegate void glVertex3iFunc(GLint @x, GLint @y, GLint @z);
        internal static glVertex3iFunc glVertex3iPtr;
        internal static void loadVertex3i()
        {
            try
            {
                glVertex3iPtr = (glVertex3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3i"), typeof(glVertex3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3i'.");
            }
        }
        public static void glVertex3i(GLint @x, GLint @y, GLint @z) => glVertex3iPtr(@x, @y, @z);

        internal delegate void glVertex3ivFunc(const GLint * @v);
        internal static glVertex3ivFunc glVertex3ivPtr;
        internal static void loadVertex3iv()
        {
            try
            {
                glVertex3ivPtr = (glVertex3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3iv"), typeof(glVertex3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3iv'.");
            }
        }
        public static void glVertex3iv(const GLint * @v) => glVertex3ivPtr(@v);

        internal delegate void glVertex3sFunc(GLshort @x, GLshort @y, GLshort @z);
        internal static glVertex3sFunc glVertex3sPtr;
        internal static void loadVertex3s()
        {
            try
            {
                glVertex3sPtr = (glVertex3sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3s"), typeof(glVertex3sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3s'.");
            }
        }
        public static void glVertex3s(GLshort @x, GLshort @y, GLshort @z) => glVertex3sPtr(@x, @y, @z);

        internal delegate void glVertex3svFunc(const GLshort * @v);
        internal static glVertex3svFunc glVertex3svPtr;
        internal static void loadVertex3sv()
        {
            try
            {
                glVertex3svPtr = (glVertex3svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3sv"), typeof(glVertex3svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3sv'.");
            }
        }
        public static void glVertex3sv(const GLshort * @v) => glVertex3svPtr(@v);

        internal delegate void glVertex4dFunc(GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glVertex4dFunc glVertex4dPtr;
        internal static void loadVertex4d()
        {
            try
            {
                glVertex4dPtr = (glVertex4dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4d"), typeof(glVertex4dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4d'.");
            }
        }
        public static void glVertex4d(GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glVertex4dPtr(@x, @y, @z, @w);

        internal delegate void glVertex4dvFunc(const GLdouble * @v);
        internal static glVertex4dvFunc glVertex4dvPtr;
        internal static void loadVertex4dv()
        {
            try
            {
                glVertex4dvPtr = (glVertex4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4dv"), typeof(glVertex4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4dv'.");
            }
        }
        public static void glVertex4dv(const GLdouble * @v) => glVertex4dvPtr(@v);

        internal delegate void glVertex4fFunc(GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glVertex4fFunc glVertex4fPtr;
        internal static void loadVertex4f()
        {
            try
            {
                glVertex4fPtr = (glVertex4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4f"), typeof(glVertex4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4f'.");
            }
        }
        public static void glVertex4f(GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glVertex4fPtr(@x, @y, @z, @w);

        internal delegate void glVertex4fvFunc(const GLfloat * @v);
        internal static glVertex4fvFunc glVertex4fvPtr;
        internal static void loadVertex4fv()
        {
            try
            {
                glVertex4fvPtr = (glVertex4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4fv"), typeof(glVertex4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4fv'.");
            }
        }
        public static void glVertex4fv(const GLfloat * @v) => glVertex4fvPtr(@v);

        internal delegate void glVertex4iFunc(GLint @x, GLint @y, GLint @z, GLint @w);
        internal static glVertex4iFunc glVertex4iPtr;
        internal static void loadVertex4i()
        {
            try
            {
                glVertex4iPtr = (glVertex4iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4i"), typeof(glVertex4iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4i'.");
            }
        }
        public static void glVertex4i(GLint @x, GLint @y, GLint @z, GLint @w) => glVertex4iPtr(@x, @y, @z, @w);

        internal delegate void glVertex4ivFunc(const GLint * @v);
        internal static glVertex4ivFunc glVertex4ivPtr;
        internal static void loadVertex4iv()
        {
            try
            {
                glVertex4ivPtr = (glVertex4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4iv"), typeof(glVertex4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4iv'.");
            }
        }
        public static void glVertex4iv(const GLint * @v) => glVertex4ivPtr(@v);

        internal delegate void glVertex4sFunc(GLshort @x, GLshort @y, GLshort @z, GLshort @w);
        internal static glVertex4sFunc glVertex4sPtr;
        internal static void loadVertex4s()
        {
            try
            {
                glVertex4sPtr = (glVertex4sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4s"), typeof(glVertex4sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4s'.");
            }
        }
        public static void glVertex4s(GLshort @x, GLshort @y, GLshort @z, GLshort @w) => glVertex4sPtr(@x, @y, @z, @w);

        internal delegate void glVertex4svFunc(const GLshort * @v);
        internal static glVertex4svFunc glVertex4svPtr;
        internal static void loadVertex4sv()
        {
            try
            {
                glVertex4svPtr = (glVertex4svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4sv"), typeof(glVertex4svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4sv'.");
            }
        }
        public static void glVertex4sv(const GLshort * @v) => glVertex4svPtr(@v);

        internal delegate void glClipPlaneFunc(GLenum @plane, const GLdouble * @equation);
        internal static glClipPlaneFunc glClipPlanePtr;
        internal static void loadClipPlane()
        {
            try
            {
                glClipPlanePtr = (glClipPlaneFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClipPlane"), typeof(glClipPlaneFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClipPlane'.");
            }
        }
        public static void glClipPlane(GLenum @plane, const GLdouble * @equation) => glClipPlanePtr(@plane, @equation);

        internal delegate void glColorMaterialFunc(GLenum @face, GLenum @mode);
        internal static glColorMaterialFunc glColorMaterialPtr;
        internal static void loadColorMaterial()
        {
            try
            {
                glColorMaterialPtr = (glColorMaterialFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorMaterial"), typeof(glColorMaterialFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorMaterial'.");
            }
        }
        public static void glColorMaterial(GLenum @face, GLenum @mode) => glColorMaterialPtr(@face, @mode);

        internal delegate void glFogfFunc(GLenum @pname, GLfloat @param);
        internal static glFogfFunc glFogfPtr;
        internal static void loadFogf()
        {
            try
            {
                glFogfPtr = (glFogfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogf"), typeof(glFogfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogf'.");
            }
        }
        public static void glFogf(GLenum @pname, GLfloat @param) => glFogfPtr(@pname, @param);

        internal delegate void glFogfvFunc(GLenum @pname, const GLfloat * @params);
        internal static glFogfvFunc glFogfvPtr;
        internal static void loadFogfv()
        {
            try
            {
                glFogfvPtr = (glFogfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogfv"), typeof(glFogfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogfv'.");
            }
        }
        public static void glFogfv(GLenum @pname, const GLfloat * @params) => glFogfvPtr(@pname, @params);

        internal delegate void glFogiFunc(GLenum @pname, GLint @param);
        internal static glFogiFunc glFogiPtr;
        internal static void loadFogi()
        {
            try
            {
                glFogiPtr = (glFogiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogi"), typeof(glFogiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogi'.");
            }
        }
        public static void glFogi(GLenum @pname, GLint @param) => glFogiPtr(@pname, @param);

        internal delegate void glFogivFunc(GLenum @pname, const GLint * @params);
        internal static glFogivFunc glFogivPtr;
        internal static void loadFogiv()
        {
            try
            {
                glFogivPtr = (glFogivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogiv"), typeof(glFogivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogiv'.");
            }
        }
        public static void glFogiv(GLenum @pname, const GLint * @params) => glFogivPtr(@pname, @params);

        internal delegate void glLightfFunc(GLenum @light, GLenum @pname, GLfloat @param);
        internal static glLightfFunc glLightfPtr;
        internal static void loadLightf()
        {
            try
            {
                glLightfPtr = (glLightfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightf"), typeof(glLightfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightf'.");
            }
        }
        public static void glLightf(GLenum @light, GLenum @pname, GLfloat @param) => glLightfPtr(@light, @pname, @param);

        internal delegate void glLightfvFunc(GLenum @light, GLenum @pname, const GLfloat * @params);
        internal static glLightfvFunc glLightfvPtr;
        internal static void loadLightfv()
        {
            try
            {
                glLightfvPtr = (glLightfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightfv"), typeof(glLightfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightfv'.");
            }
        }
        public static void glLightfv(GLenum @light, GLenum @pname, const GLfloat * @params) => glLightfvPtr(@light, @pname, @params);

        internal delegate void glLightiFunc(GLenum @light, GLenum @pname, GLint @param);
        internal static glLightiFunc glLightiPtr;
        internal static void loadLighti()
        {
            try
            {
                glLightiPtr = (glLightiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLighti"), typeof(glLightiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLighti'.");
            }
        }
        public static void glLighti(GLenum @light, GLenum @pname, GLint @param) => glLightiPtr(@light, @pname, @param);

        internal delegate void glLightivFunc(GLenum @light, GLenum @pname, const GLint * @params);
        internal static glLightivFunc glLightivPtr;
        internal static void loadLightiv()
        {
            try
            {
                glLightivPtr = (glLightivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightiv"), typeof(glLightivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightiv'.");
            }
        }
        public static void glLightiv(GLenum @light, GLenum @pname, const GLint * @params) => glLightivPtr(@light, @pname, @params);

        internal delegate void glLightModelfFunc(GLenum @pname, GLfloat @param);
        internal static glLightModelfFunc glLightModelfPtr;
        internal static void loadLightModelf()
        {
            try
            {
                glLightModelfPtr = (glLightModelfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightModelf"), typeof(glLightModelfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightModelf'.");
            }
        }
        public static void glLightModelf(GLenum @pname, GLfloat @param) => glLightModelfPtr(@pname, @param);

        internal delegate void glLightModelfvFunc(GLenum @pname, const GLfloat * @params);
        internal static glLightModelfvFunc glLightModelfvPtr;
        internal static void loadLightModelfv()
        {
            try
            {
                glLightModelfvPtr = (glLightModelfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightModelfv"), typeof(glLightModelfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightModelfv'.");
            }
        }
        public static void glLightModelfv(GLenum @pname, const GLfloat * @params) => glLightModelfvPtr(@pname, @params);

        internal delegate void glLightModeliFunc(GLenum @pname, GLint @param);
        internal static glLightModeliFunc glLightModeliPtr;
        internal static void loadLightModeli()
        {
            try
            {
                glLightModeliPtr = (glLightModeliFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightModeli"), typeof(glLightModeliFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightModeli'.");
            }
        }
        public static void glLightModeli(GLenum @pname, GLint @param) => glLightModeliPtr(@pname, @param);

        internal delegate void glLightModelivFunc(GLenum @pname, const GLint * @params);
        internal static glLightModelivFunc glLightModelivPtr;
        internal static void loadLightModeliv()
        {
            try
            {
                glLightModelivPtr = (glLightModelivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightModeliv"), typeof(glLightModelivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightModeliv'.");
            }
        }
        public static void glLightModeliv(GLenum @pname, const GLint * @params) => glLightModelivPtr(@pname, @params);

        internal delegate void glLineStippleFunc(GLint @factor, GLushort @pattern);
        internal static glLineStippleFunc glLineStipplePtr;
        internal static void loadLineStipple()
        {
            try
            {
                glLineStipplePtr = (glLineStippleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLineStipple"), typeof(glLineStippleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLineStipple'.");
            }
        }
        public static void glLineStipple(GLint @factor, GLushort @pattern) => glLineStipplePtr(@factor, @pattern);

        internal delegate void glMaterialfFunc(GLenum @face, GLenum @pname, GLfloat @param);
        internal static glMaterialfFunc glMaterialfPtr;
        internal static void loadMaterialf()
        {
            try
            {
                glMaterialfPtr = (glMaterialfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMaterialf"), typeof(glMaterialfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMaterialf'.");
            }
        }
        public static void glMaterialf(GLenum @face, GLenum @pname, GLfloat @param) => glMaterialfPtr(@face, @pname, @param);

        internal delegate void glMaterialfvFunc(GLenum @face, GLenum @pname, const GLfloat * @params);
        internal static glMaterialfvFunc glMaterialfvPtr;
        internal static void loadMaterialfv()
        {
            try
            {
                glMaterialfvPtr = (glMaterialfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMaterialfv"), typeof(glMaterialfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMaterialfv'.");
            }
        }
        public static void glMaterialfv(GLenum @face, GLenum @pname, const GLfloat * @params) => glMaterialfvPtr(@face, @pname, @params);

        internal delegate void glMaterialiFunc(GLenum @face, GLenum @pname, GLint @param);
        internal static glMaterialiFunc glMaterialiPtr;
        internal static void loadMateriali()
        {
            try
            {
                glMaterialiPtr = (glMaterialiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMateriali"), typeof(glMaterialiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMateriali'.");
            }
        }
        public static void glMateriali(GLenum @face, GLenum @pname, GLint @param) => glMaterialiPtr(@face, @pname, @param);

        internal delegate void glMaterialivFunc(GLenum @face, GLenum @pname, const GLint * @params);
        internal static glMaterialivFunc glMaterialivPtr;
        internal static void loadMaterialiv()
        {
            try
            {
                glMaterialivPtr = (glMaterialivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMaterialiv"), typeof(glMaterialivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMaterialiv'.");
            }
        }
        public static void glMaterialiv(GLenum @face, GLenum @pname, const GLint * @params) => glMaterialivPtr(@face, @pname, @params);

        internal delegate void glPolygonStippleFunc(const GLubyte * @mask);
        internal static glPolygonStippleFunc glPolygonStipplePtr;
        internal static void loadPolygonStipple()
        {
            try
            {
                glPolygonStipplePtr = (glPolygonStippleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPolygonStipple"), typeof(glPolygonStippleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPolygonStipple'.");
            }
        }
        public static void glPolygonStipple(const GLubyte * @mask) => glPolygonStipplePtr(@mask);

        internal delegate void glShadeModelFunc(GLenum @mode);
        internal static glShadeModelFunc glShadeModelPtr;
        internal static void loadShadeModel()
        {
            try
            {
                glShadeModelPtr = (glShadeModelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShadeModel"), typeof(glShadeModelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShadeModel'.");
            }
        }
        public static void glShadeModel(GLenum @mode) => glShadeModelPtr(@mode);

        internal delegate void glTexEnvfFunc(GLenum @target, GLenum @pname, GLfloat @param);
        internal static glTexEnvfFunc glTexEnvfPtr;
        internal static void loadTexEnvf()
        {
            try
            {
                glTexEnvfPtr = (glTexEnvfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexEnvf"), typeof(glTexEnvfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexEnvf'.");
            }
        }
        public static void glTexEnvf(GLenum @target, GLenum @pname, GLfloat @param) => glTexEnvfPtr(@target, @pname, @param);

        internal delegate void glTexEnvfvFunc(GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glTexEnvfvFunc glTexEnvfvPtr;
        internal static void loadTexEnvfv()
        {
            try
            {
                glTexEnvfvPtr = (glTexEnvfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexEnvfv"), typeof(glTexEnvfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexEnvfv'.");
            }
        }
        public static void glTexEnvfv(GLenum @target, GLenum @pname, const GLfloat * @params) => glTexEnvfvPtr(@target, @pname, @params);

        internal delegate void glTexEnviFunc(GLenum @target, GLenum @pname, GLint @param);
        internal static glTexEnviFunc glTexEnviPtr;
        internal static void loadTexEnvi()
        {
            try
            {
                glTexEnviPtr = (glTexEnviFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexEnvi"), typeof(glTexEnviFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexEnvi'.");
            }
        }
        public static void glTexEnvi(GLenum @target, GLenum @pname, GLint @param) => glTexEnviPtr(@target, @pname, @param);

        internal delegate void glTexEnvivFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glTexEnvivFunc glTexEnvivPtr;
        internal static void loadTexEnviv()
        {
            try
            {
                glTexEnvivPtr = (glTexEnvivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexEnviv"), typeof(glTexEnvivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexEnviv'.");
            }
        }
        public static void glTexEnviv(GLenum @target, GLenum @pname, const GLint * @params) => glTexEnvivPtr(@target, @pname, @params);

        internal delegate void glTexGendFunc(GLenum @coord, GLenum @pname, GLdouble @param);
        internal static glTexGendFunc glTexGendPtr;
        internal static void loadTexGend()
        {
            try
            {
                glTexGendPtr = (glTexGendFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGend"), typeof(glTexGendFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGend'.");
            }
        }
        public static void glTexGend(GLenum @coord, GLenum @pname, GLdouble @param) => glTexGendPtr(@coord, @pname, @param);

        internal delegate void glTexGendvFunc(GLenum @coord, GLenum @pname, const GLdouble * @params);
        internal static glTexGendvFunc glTexGendvPtr;
        internal static void loadTexGendv()
        {
            try
            {
                glTexGendvPtr = (glTexGendvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGendv"), typeof(glTexGendvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGendv'.");
            }
        }
        public static void glTexGendv(GLenum @coord, GLenum @pname, const GLdouble * @params) => glTexGendvPtr(@coord, @pname, @params);

        internal delegate void glTexGenfFunc(GLenum @coord, GLenum @pname, GLfloat @param);
        internal static glTexGenfFunc glTexGenfPtr;
        internal static void loadTexGenf()
        {
            try
            {
                glTexGenfPtr = (glTexGenfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGenf"), typeof(glTexGenfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGenf'.");
            }
        }
        public static void glTexGenf(GLenum @coord, GLenum @pname, GLfloat @param) => glTexGenfPtr(@coord, @pname, @param);

        internal delegate void glTexGenfvFunc(GLenum @coord, GLenum @pname, const GLfloat * @params);
        internal static glTexGenfvFunc glTexGenfvPtr;
        internal static void loadTexGenfv()
        {
            try
            {
                glTexGenfvPtr = (glTexGenfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGenfv"), typeof(glTexGenfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGenfv'.");
            }
        }
        public static void glTexGenfv(GLenum @coord, GLenum @pname, const GLfloat * @params) => glTexGenfvPtr(@coord, @pname, @params);

        internal delegate void glTexGeniFunc(GLenum @coord, GLenum @pname, GLint @param);
        internal static glTexGeniFunc glTexGeniPtr;
        internal static void loadTexGeni()
        {
            try
            {
                glTexGeniPtr = (glTexGeniFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGeni"), typeof(glTexGeniFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGeni'.");
            }
        }
        public static void glTexGeni(GLenum @coord, GLenum @pname, GLint @param) => glTexGeniPtr(@coord, @pname, @param);

        internal delegate void glTexGenivFunc(GLenum @coord, GLenum @pname, const GLint * @params);
        internal static glTexGenivFunc glTexGenivPtr;
        internal static void loadTexGeniv()
        {
            try
            {
                glTexGenivPtr = (glTexGenivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGeniv"), typeof(glTexGenivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGeniv'.");
            }
        }
        public static void glTexGeniv(GLenum @coord, GLenum @pname, const GLint * @params) => glTexGenivPtr(@coord, @pname, @params);

        internal delegate void glFeedbackBufferFunc(GLsizei @size, GLenum @type, GLfloat * @buffer);
        internal static glFeedbackBufferFunc glFeedbackBufferPtr;
        internal static void loadFeedbackBuffer()
        {
            try
            {
                glFeedbackBufferPtr = (glFeedbackBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFeedbackBuffer"), typeof(glFeedbackBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFeedbackBuffer'.");
            }
        }
        public static void glFeedbackBuffer(GLsizei @size, GLenum @type, GLfloat * @buffer) => glFeedbackBufferPtr(@size, @type, @buffer);

        internal delegate void glSelectBufferFunc(GLsizei @size, GLuint * @buffer);
        internal static glSelectBufferFunc glSelectBufferPtr;
        internal static void loadSelectBuffer()
        {
            try
            {
                glSelectBufferPtr = (glSelectBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSelectBuffer"), typeof(glSelectBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSelectBuffer'.");
            }
        }
        public static void glSelectBuffer(GLsizei @size, GLuint * @buffer) => glSelectBufferPtr(@size, @buffer);

        internal delegate GLint glRenderModeFunc(GLenum @mode);
        internal static glRenderModeFunc glRenderModePtr;
        internal static void loadRenderMode()
        {
            try
            {
                glRenderModePtr = (glRenderModeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderMode"), typeof(glRenderModeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderMode'.");
            }
        }
        public static GLint glRenderMode(GLenum @mode) => glRenderModePtr(@mode);

        internal delegate void glInitNamesFunc();
        internal static glInitNamesFunc glInitNamesPtr;
        internal static void loadInitNames()
        {
            try
            {
                glInitNamesPtr = (glInitNamesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInitNames"), typeof(glInitNamesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInitNames'.");
            }
        }
        public static void glInitNames() => glInitNamesPtr();

        internal delegate void glLoadNameFunc(GLuint @name);
        internal static glLoadNameFunc glLoadNamePtr;
        internal static void loadLoadName()
        {
            try
            {
                glLoadNamePtr = (glLoadNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadName"), typeof(glLoadNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadName'.");
            }
        }
        public static void glLoadName(GLuint @name) => glLoadNamePtr(@name);

        internal delegate void glPassThroughFunc(GLfloat @token);
        internal static glPassThroughFunc glPassThroughPtr;
        internal static void loadPassThrough()
        {
            try
            {
                glPassThroughPtr = (glPassThroughFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPassThrough"), typeof(glPassThroughFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPassThrough'.");
            }
        }
        public static void glPassThrough(GLfloat @token) => glPassThroughPtr(@token);

        internal delegate void glPopNameFunc();
        internal static glPopNameFunc glPopNamePtr;
        internal static void loadPopName()
        {
            try
            {
                glPopNamePtr = (glPopNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPopName"), typeof(glPopNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPopName'.");
            }
        }
        public static void glPopName() => glPopNamePtr();

        internal delegate void glPushNameFunc(GLuint @name);
        internal static glPushNameFunc glPushNamePtr;
        internal static void loadPushName()
        {
            try
            {
                glPushNamePtr = (glPushNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPushName"), typeof(glPushNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPushName'.");
            }
        }
        public static void glPushName(GLuint @name) => glPushNamePtr(@name);

        internal delegate void glClearAccumFunc(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha);
        internal static glClearAccumFunc glClearAccumPtr;
        internal static void loadClearAccum()
        {
            try
            {
                glClearAccumPtr = (glClearAccumFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearAccum"), typeof(glClearAccumFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearAccum'.");
            }
        }
        public static void glClearAccum(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha) => glClearAccumPtr(@red, @green, @blue, @alpha);

        internal delegate void glClearIndexFunc(GLfloat @c);
        internal static glClearIndexFunc glClearIndexPtr;
        internal static void loadClearIndex()
        {
            try
            {
                glClearIndexPtr = (glClearIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearIndex"), typeof(glClearIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearIndex'.");
            }
        }
        public static void glClearIndex(GLfloat @c) => glClearIndexPtr(@c);

        internal delegate void glIndexMaskFunc(GLuint @mask);
        internal static glIndexMaskFunc glIndexMaskPtr;
        internal static void loadIndexMask()
        {
            try
            {
                glIndexMaskPtr = (glIndexMaskFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexMask"), typeof(glIndexMaskFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexMask'.");
            }
        }
        public static void glIndexMask(GLuint @mask) => glIndexMaskPtr(@mask);

        internal delegate void glAccumFunc(GLenum @op, GLfloat @value);
        internal static glAccumFunc glAccumPtr;
        internal static void loadAccum()
        {
            try
            {
                glAccumPtr = (glAccumFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAccum"), typeof(glAccumFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAccum'.");
            }
        }
        public static void glAccum(GLenum @op, GLfloat @value) => glAccumPtr(@op, @value);

        internal delegate void glPopAttribFunc();
        internal static glPopAttribFunc glPopAttribPtr;
        internal static void loadPopAttrib()
        {
            try
            {
                glPopAttribPtr = (glPopAttribFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPopAttrib"), typeof(glPopAttribFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPopAttrib'.");
            }
        }
        public static void glPopAttrib() => glPopAttribPtr();

        internal delegate void glPushAttribFunc(GLbitfield @mask);
        internal static glPushAttribFunc glPushAttribPtr;
        internal static void loadPushAttrib()
        {
            try
            {
                glPushAttribPtr = (glPushAttribFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPushAttrib"), typeof(glPushAttribFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPushAttrib'.");
            }
        }
        public static void glPushAttrib(GLbitfield @mask) => glPushAttribPtr(@mask);

        internal delegate void glMap1dFunc(GLenum @target, GLdouble @u1, GLdouble @u2, GLint @stride, GLint @order, const GLdouble * @points);
        internal static glMap1dFunc glMap1dPtr;
        internal static void loadMap1d()
        {
            try
            {
                glMap1dPtr = (glMap1dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMap1d"), typeof(glMap1dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMap1d'.");
            }
        }
        public static void glMap1d(GLenum @target, GLdouble @u1, GLdouble @u2, GLint @stride, GLint @order, const GLdouble * @points) => glMap1dPtr(@target, @u1, @u2, @stride, @order, @points);

        internal delegate void glMap1fFunc(GLenum @target, GLfloat @u1, GLfloat @u2, GLint @stride, GLint @order, const GLfloat * @points);
        internal static glMap1fFunc glMap1fPtr;
        internal static void loadMap1f()
        {
            try
            {
                glMap1fPtr = (glMap1fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMap1f"), typeof(glMap1fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMap1f'.");
            }
        }
        public static void glMap1f(GLenum @target, GLfloat @u1, GLfloat @u2, GLint @stride, GLint @order, const GLfloat * @points) => glMap1fPtr(@target, @u1, @u2, @stride, @order, @points);

        internal delegate void glMap2dFunc(GLenum @target, GLdouble @u1, GLdouble @u2, GLint @ustride, GLint @uorder, GLdouble @v1, GLdouble @v2, GLint @vstride, GLint @vorder, const GLdouble * @points);
        internal static glMap2dFunc glMap2dPtr;
        internal static void loadMap2d()
        {
            try
            {
                glMap2dPtr = (glMap2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMap2d"), typeof(glMap2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMap2d'.");
            }
        }
        public static void glMap2d(GLenum @target, GLdouble @u1, GLdouble @u2, GLint @ustride, GLint @uorder, GLdouble @v1, GLdouble @v2, GLint @vstride, GLint @vorder, const GLdouble * @points) => glMap2dPtr(@target, @u1, @u2, @ustride, @uorder, @v1, @v2, @vstride, @vorder, @points);

        internal delegate void glMap2fFunc(GLenum @target, GLfloat @u1, GLfloat @u2, GLint @ustride, GLint @uorder, GLfloat @v1, GLfloat @v2, GLint @vstride, GLint @vorder, const GLfloat * @points);
        internal static glMap2fFunc glMap2fPtr;
        internal static void loadMap2f()
        {
            try
            {
                glMap2fPtr = (glMap2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMap2f"), typeof(glMap2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMap2f'.");
            }
        }
        public static void glMap2f(GLenum @target, GLfloat @u1, GLfloat @u2, GLint @ustride, GLint @uorder, GLfloat @v1, GLfloat @v2, GLint @vstride, GLint @vorder, const GLfloat * @points) => glMap2fPtr(@target, @u1, @u2, @ustride, @uorder, @v1, @v2, @vstride, @vorder, @points);

        internal delegate void glMapGrid1dFunc(GLint @un, GLdouble @u1, GLdouble @u2);
        internal static glMapGrid1dFunc glMapGrid1dPtr;
        internal static void loadMapGrid1d()
        {
            try
            {
                glMapGrid1dPtr = (glMapGrid1dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapGrid1d"), typeof(glMapGrid1dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapGrid1d'.");
            }
        }
        public static void glMapGrid1d(GLint @un, GLdouble @u1, GLdouble @u2) => glMapGrid1dPtr(@un, @u1, @u2);

        internal delegate void glMapGrid1fFunc(GLint @un, GLfloat @u1, GLfloat @u2);
        internal static glMapGrid1fFunc glMapGrid1fPtr;
        internal static void loadMapGrid1f()
        {
            try
            {
                glMapGrid1fPtr = (glMapGrid1fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapGrid1f"), typeof(glMapGrid1fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapGrid1f'.");
            }
        }
        public static void glMapGrid1f(GLint @un, GLfloat @u1, GLfloat @u2) => glMapGrid1fPtr(@un, @u1, @u2);

        internal delegate void glMapGrid2dFunc(GLint @un, GLdouble @u1, GLdouble @u2, GLint @vn, GLdouble @v1, GLdouble @v2);
        internal static glMapGrid2dFunc glMapGrid2dPtr;
        internal static void loadMapGrid2d()
        {
            try
            {
                glMapGrid2dPtr = (glMapGrid2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapGrid2d"), typeof(glMapGrid2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapGrid2d'.");
            }
        }
        public static void glMapGrid2d(GLint @un, GLdouble @u1, GLdouble @u2, GLint @vn, GLdouble @v1, GLdouble @v2) => glMapGrid2dPtr(@un, @u1, @u2, @vn, @v1, @v2);

        internal delegate void glMapGrid2fFunc(GLint @un, GLfloat @u1, GLfloat @u2, GLint @vn, GLfloat @v1, GLfloat @v2);
        internal static glMapGrid2fFunc glMapGrid2fPtr;
        internal static void loadMapGrid2f()
        {
            try
            {
                glMapGrid2fPtr = (glMapGrid2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapGrid2f"), typeof(glMapGrid2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapGrid2f'.");
            }
        }
        public static void glMapGrid2f(GLint @un, GLfloat @u1, GLfloat @u2, GLint @vn, GLfloat @v1, GLfloat @v2) => glMapGrid2fPtr(@un, @u1, @u2, @vn, @v1, @v2);

        internal delegate void glEvalCoord1dFunc(GLdouble @u);
        internal static glEvalCoord1dFunc glEvalCoord1dPtr;
        internal static void loadEvalCoord1d()
        {
            try
            {
                glEvalCoord1dPtr = (glEvalCoord1dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalCoord1d"), typeof(glEvalCoord1dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalCoord1d'.");
            }
        }
        public static void glEvalCoord1d(GLdouble @u) => glEvalCoord1dPtr(@u);

        internal delegate void glEvalCoord1dvFunc(const GLdouble * @u);
        internal static glEvalCoord1dvFunc glEvalCoord1dvPtr;
        internal static void loadEvalCoord1dv()
        {
            try
            {
                glEvalCoord1dvPtr = (glEvalCoord1dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalCoord1dv"), typeof(glEvalCoord1dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalCoord1dv'.");
            }
        }
        public static void glEvalCoord1dv(const GLdouble * @u) => glEvalCoord1dvPtr(@u);

        internal delegate void glEvalCoord1fFunc(GLfloat @u);
        internal static glEvalCoord1fFunc glEvalCoord1fPtr;
        internal static void loadEvalCoord1f()
        {
            try
            {
                glEvalCoord1fPtr = (glEvalCoord1fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalCoord1f"), typeof(glEvalCoord1fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalCoord1f'.");
            }
        }
        public static void glEvalCoord1f(GLfloat @u) => glEvalCoord1fPtr(@u);

        internal delegate void glEvalCoord1fvFunc(const GLfloat * @u);
        internal static glEvalCoord1fvFunc glEvalCoord1fvPtr;
        internal static void loadEvalCoord1fv()
        {
            try
            {
                glEvalCoord1fvPtr = (glEvalCoord1fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalCoord1fv"), typeof(glEvalCoord1fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalCoord1fv'.");
            }
        }
        public static void glEvalCoord1fv(const GLfloat * @u) => glEvalCoord1fvPtr(@u);

        internal delegate void glEvalCoord2dFunc(GLdouble @u, GLdouble @v);
        internal static glEvalCoord2dFunc glEvalCoord2dPtr;
        internal static void loadEvalCoord2d()
        {
            try
            {
                glEvalCoord2dPtr = (glEvalCoord2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalCoord2d"), typeof(glEvalCoord2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalCoord2d'.");
            }
        }
        public static void glEvalCoord2d(GLdouble @u, GLdouble @v) => glEvalCoord2dPtr(@u, @v);

        internal delegate void glEvalCoord2dvFunc(const GLdouble * @u);
        internal static glEvalCoord2dvFunc glEvalCoord2dvPtr;
        internal static void loadEvalCoord2dv()
        {
            try
            {
                glEvalCoord2dvPtr = (glEvalCoord2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalCoord2dv"), typeof(glEvalCoord2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalCoord2dv'.");
            }
        }
        public static void glEvalCoord2dv(const GLdouble * @u) => glEvalCoord2dvPtr(@u);

        internal delegate void glEvalCoord2fFunc(GLfloat @u, GLfloat @v);
        internal static glEvalCoord2fFunc glEvalCoord2fPtr;
        internal static void loadEvalCoord2f()
        {
            try
            {
                glEvalCoord2fPtr = (glEvalCoord2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalCoord2f"), typeof(glEvalCoord2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalCoord2f'.");
            }
        }
        public static void glEvalCoord2f(GLfloat @u, GLfloat @v) => glEvalCoord2fPtr(@u, @v);

        internal delegate void glEvalCoord2fvFunc(const GLfloat * @u);
        internal static glEvalCoord2fvFunc glEvalCoord2fvPtr;
        internal static void loadEvalCoord2fv()
        {
            try
            {
                glEvalCoord2fvPtr = (glEvalCoord2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalCoord2fv"), typeof(glEvalCoord2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalCoord2fv'.");
            }
        }
        public static void glEvalCoord2fv(const GLfloat * @u) => glEvalCoord2fvPtr(@u);

        internal delegate void glEvalMesh1Func(GLenum @mode, GLint @i1, GLint @i2);
        internal static glEvalMesh1Func glEvalMesh1Ptr;
        internal static void loadEvalMesh1()
        {
            try
            {
                glEvalMesh1Ptr = (glEvalMesh1Func)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalMesh1"), typeof(glEvalMesh1Func));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalMesh1'.");
            }
        }
        public static void glEvalMesh1(GLenum @mode, GLint @i1, GLint @i2) => glEvalMesh1Ptr(@mode, @i1, @i2);

        internal delegate void glEvalPoint1Func(GLint @i);
        internal static glEvalPoint1Func glEvalPoint1Ptr;
        internal static void loadEvalPoint1()
        {
            try
            {
                glEvalPoint1Ptr = (glEvalPoint1Func)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalPoint1"), typeof(glEvalPoint1Func));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalPoint1'.");
            }
        }
        public static void glEvalPoint1(GLint @i) => glEvalPoint1Ptr(@i);

        internal delegate void glEvalMesh2Func(GLenum @mode, GLint @i1, GLint @i2, GLint @j1, GLint @j2);
        internal static glEvalMesh2Func glEvalMesh2Ptr;
        internal static void loadEvalMesh2()
        {
            try
            {
                glEvalMesh2Ptr = (glEvalMesh2Func)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalMesh2"), typeof(glEvalMesh2Func));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalMesh2'.");
            }
        }
        public static void glEvalMesh2(GLenum @mode, GLint @i1, GLint @i2, GLint @j1, GLint @j2) => glEvalMesh2Ptr(@mode, @i1, @i2, @j1, @j2);

        internal delegate void glEvalPoint2Func(GLint @i, GLint @j);
        internal static glEvalPoint2Func glEvalPoint2Ptr;
        internal static void loadEvalPoint2()
        {
            try
            {
                glEvalPoint2Ptr = (glEvalPoint2Func)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalPoint2"), typeof(glEvalPoint2Func));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalPoint2'.");
            }
        }
        public static void glEvalPoint2(GLint @i, GLint @j) => glEvalPoint2Ptr(@i, @j);

        internal delegate void glAlphaFuncFunc(GLenum @func, GLfloat @ref);
        internal static glAlphaFuncFunc glAlphaFuncPtr;
        internal static void loadAlphaFunc()
        {
            try
            {
                glAlphaFuncPtr = (glAlphaFuncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAlphaFunc"), typeof(glAlphaFuncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAlphaFunc'.");
            }
        }
        public static void glAlphaFunc(GLenum @func, GLfloat @ref) => glAlphaFuncPtr(@func, @ref);

        internal delegate void glPixelZoomFunc(GLfloat @xfactor, GLfloat @yfactor);
        internal static glPixelZoomFunc glPixelZoomPtr;
        internal static void loadPixelZoom()
        {
            try
            {
                glPixelZoomPtr = (glPixelZoomFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelZoom"), typeof(glPixelZoomFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelZoom'.");
            }
        }
        public static void glPixelZoom(GLfloat @xfactor, GLfloat @yfactor) => glPixelZoomPtr(@xfactor, @yfactor);

        internal delegate void glPixelTransferfFunc(GLenum @pname, GLfloat @param);
        internal static glPixelTransferfFunc glPixelTransferfPtr;
        internal static void loadPixelTransferf()
        {
            try
            {
                glPixelTransferfPtr = (glPixelTransferfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelTransferf"), typeof(glPixelTransferfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelTransferf'.");
            }
        }
        public static void glPixelTransferf(GLenum @pname, GLfloat @param) => glPixelTransferfPtr(@pname, @param);

        internal delegate void glPixelTransferiFunc(GLenum @pname, GLint @param);
        internal static glPixelTransferiFunc glPixelTransferiPtr;
        internal static void loadPixelTransferi()
        {
            try
            {
                glPixelTransferiPtr = (glPixelTransferiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelTransferi"), typeof(glPixelTransferiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelTransferi'.");
            }
        }
        public static void glPixelTransferi(GLenum @pname, GLint @param) => glPixelTransferiPtr(@pname, @param);

        internal delegate void glPixelMapfvFunc(GLenum @map, GLsizei @mapsize, const GLfloat * @values);
        internal static glPixelMapfvFunc glPixelMapfvPtr;
        internal static void loadPixelMapfv()
        {
            try
            {
                glPixelMapfvPtr = (glPixelMapfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelMapfv"), typeof(glPixelMapfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelMapfv'.");
            }
        }
        public static void glPixelMapfv(GLenum @map, GLsizei @mapsize, const GLfloat * @values) => glPixelMapfvPtr(@map, @mapsize, @values);

        internal delegate void glPixelMapuivFunc(GLenum @map, GLsizei @mapsize, const GLuint * @values);
        internal static glPixelMapuivFunc glPixelMapuivPtr;
        internal static void loadPixelMapuiv()
        {
            try
            {
                glPixelMapuivPtr = (glPixelMapuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelMapuiv"), typeof(glPixelMapuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelMapuiv'.");
            }
        }
        public static void glPixelMapuiv(GLenum @map, GLsizei @mapsize, const GLuint * @values) => glPixelMapuivPtr(@map, @mapsize, @values);

        internal delegate void glPixelMapusvFunc(GLenum @map, GLsizei @mapsize, const GLushort * @values);
        internal static glPixelMapusvFunc glPixelMapusvPtr;
        internal static void loadPixelMapusv()
        {
            try
            {
                glPixelMapusvPtr = (glPixelMapusvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelMapusv"), typeof(glPixelMapusvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelMapusv'.");
            }
        }
        public static void glPixelMapusv(GLenum @map, GLsizei @mapsize, const GLushort * @values) => glPixelMapusvPtr(@map, @mapsize, @values);

        internal delegate void glCopyPixelsFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @type);
        internal static glCopyPixelsFunc glCopyPixelsPtr;
        internal static void loadCopyPixels()
        {
            try
            {
                glCopyPixelsPtr = (glCopyPixelsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyPixels"), typeof(glCopyPixelsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyPixels'.");
            }
        }
        public static void glCopyPixels(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @type) => glCopyPixelsPtr(@x, @y, @width, @height, @type);

        internal delegate void glDrawPixelsFunc(GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels);
        internal static glDrawPixelsFunc glDrawPixelsPtr;
        internal static void loadDrawPixels()
        {
            try
            {
                glDrawPixelsPtr = (glDrawPixelsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawPixels"), typeof(glDrawPixelsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawPixels'.");
            }
        }
        public static void glDrawPixels(GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels) => glDrawPixelsPtr(@width, @height, @format, @type, @pixels);

        internal delegate void glGetClipPlaneFunc(GLenum @plane, GLdouble * @equation);
        internal static glGetClipPlaneFunc glGetClipPlanePtr;
        internal static void loadGetClipPlane()
        {
            try
            {
                glGetClipPlanePtr = (glGetClipPlaneFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetClipPlane"), typeof(glGetClipPlaneFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetClipPlane'.");
            }
        }
        public static void glGetClipPlane(GLenum @plane, GLdouble * @equation) => glGetClipPlanePtr(@plane, @equation);

        internal delegate void glGetLightfvFunc(GLenum @light, GLenum @pname, GLfloat * @params);
        internal static glGetLightfvFunc glGetLightfvPtr;
        internal static void loadGetLightfv()
        {
            try
            {
                glGetLightfvPtr = (glGetLightfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetLightfv"), typeof(glGetLightfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetLightfv'.");
            }
        }
        public static void glGetLightfv(GLenum @light, GLenum @pname, GLfloat * @params) => glGetLightfvPtr(@light, @pname, @params);

        internal delegate void glGetLightivFunc(GLenum @light, GLenum @pname, GLint * @params);
        internal static glGetLightivFunc glGetLightivPtr;
        internal static void loadGetLightiv()
        {
            try
            {
                glGetLightivPtr = (glGetLightivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetLightiv"), typeof(glGetLightivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetLightiv'.");
            }
        }
        public static void glGetLightiv(GLenum @light, GLenum @pname, GLint * @params) => glGetLightivPtr(@light, @pname, @params);

        internal delegate void glGetMapdvFunc(GLenum @target, GLenum @query, GLdouble * @v);
        internal static glGetMapdvFunc glGetMapdvPtr;
        internal static void loadGetMapdv()
        {
            try
            {
                glGetMapdvPtr = (glGetMapdvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMapdv"), typeof(glGetMapdvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMapdv'.");
            }
        }
        public static void glGetMapdv(GLenum @target, GLenum @query, GLdouble * @v) => glGetMapdvPtr(@target, @query, @v);

        internal delegate void glGetMapfvFunc(GLenum @target, GLenum @query, GLfloat * @v);
        internal static glGetMapfvFunc glGetMapfvPtr;
        internal static void loadGetMapfv()
        {
            try
            {
                glGetMapfvPtr = (glGetMapfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMapfv"), typeof(glGetMapfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMapfv'.");
            }
        }
        public static void glGetMapfv(GLenum @target, GLenum @query, GLfloat * @v) => glGetMapfvPtr(@target, @query, @v);

        internal delegate void glGetMapivFunc(GLenum @target, GLenum @query, GLint * @v);
        internal static glGetMapivFunc glGetMapivPtr;
        internal static void loadGetMapiv()
        {
            try
            {
                glGetMapivPtr = (glGetMapivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMapiv"), typeof(glGetMapivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMapiv'.");
            }
        }
        public static void glGetMapiv(GLenum @target, GLenum @query, GLint * @v) => glGetMapivPtr(@target, @query, @v);

        internal delegate void glGetMaterialfvFunc(GLenum @face, GLenum @pname, GLfloat * @params);
        internal static glGetMaterialfvFunc glGetMaterialfvPtr;
        internal static void loadGetMaterialfv()
        {
            try
            {
                glGetMaterialfvPtr = (glGetMaterialfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMaterialfv"), typeof(glGetMaterialfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMaterialfv'.");
            }
        }
        public static void glGetMaterialfv(GLenum @face, GLenum @pname, GLfloat * @params) => glGetMaterialfvPtr(@face, @pname, @params);

        internal delegate void glGetMaterialivFunc(GLenum @face, GLenum @pname, GLint * @params);
        internal static glGetMaterialivFunc glGetMaterialivPtr;
        internal static void loadGetMaterialiv()
        {
            try
            {
                glGetMaterialivPtr = (glGetMaterialivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMaterialiv"), typeof(glGetMaterialivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMaterialiv'.");
            }
        }
        public static void glGetMaterialiv(GLenum @face, GLenum @pname, GLint * @params) => glGetMaterialivPtr(@face, @pname, @params);

        internal delegate void glGetPixelMapfvFunc(GLenum @map, GLfloat * @values);
        internal static glGetPixelMapfvFunc glGetPixelMapfvPtr;
        internal static void loadGetPixelMapfv()
        {
            try
            {
                glGetPixelMapfvPtr = (glGetPixelMapfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPixelMapfv"), typeof(glGetPixelMapfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPixelMapfv'.");
            }
        }
        public static void glGetPixelMapfv(GLenum @map, GLfloat * @values) => glGetPixelMapfvPtr(@map, @values);

        internal delegate void glGetPixelMapuivFunc(GLenum @map, GLuint * @values);
        internal static glGetPixelMapuivFunc glGetPixelMapuivPtr;
        internal static void loadGetPixelMapuiv()
        {
            try
            {
                glGetPixelMapuivPtr = (glGetPixelMapuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPixelMapuiv"), typeof(glGetPixelMapuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPixelMapuiv'.");
            }
        }
        public static void glGetPixelMapuiv(GLenum @map, GLuint * @values) => glGetPixelMapuivPtr(@map, @values);

        internal delegate void glGetPixelMapusvFunc(GLenum @map, GLushort * @values);
        internal static glGetPixelMapusvFunc glGetPixelMapusvPtr;
        internal static void loadGetPixelMapusv()
        {
            try
            {
                glGetPixelMapusvPtr = (glGetPixelMapusvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPixelMapusv"), typeof(glGetPixelMapusvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPixelMapusv'.");
            }
        }
        public static void glGetPixelMapusv(GLenum @map, GLushort * @values) => glGetPixelMapusvPtr(@map, @values);

        internal delegate void glGetPolygonStippleFunc(GLubyte * @mask);
        internal static glGetPolygonStippleFunc glGetPolygonStipplePtr;
        internal static void loadGetPolygonStipple()
        {
            try
            {
                glGetPolygonStipplePtr = (glGetPolygonStippleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPolygonStipple"), typeof(glGetPolygonStippleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPolygonStipple'.");
            }
        }
        public static void glGetPolygonStipple(GLubyte * @mask) => glGetPolygonStipplePtr(@mask);

        internal delegate void glGetTexEnvfvFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetTexEnvfvFunc glGetTexEnvfvPtr;
        internal static void loadGetTexEnvfv()
        {
            try
            {
                glGetTexEnvfvPtr = (glGetTexEnvfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexEnvfv"), typeof(glGetTexEnvfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexEnvfv'.");
            }
        }
        public static void glGetTexEnvfv(GLenum @target, GLenum @pname, GLfloat * @params) => glGetTexEnvfvPtr(@target, @pname, @params);

        internal delegate void glGetTexEnvivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetTexEnvivFunc glGetTexEnvivPtr;
        internal static void loadGetTexEnviv()
        {
            try
            {
                glGetTexEnvivPtr = (glGetTexEnvivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexEnviv"), typeof(glGetTexEnvivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexEnviv'.");
            }
        }
        public static void glGetTexEnviv(GLenum @target, GLenum @pname, GLint * @params) => glGetTexEnvivPtr(@target, @pname, @params);

        internal delegate void glGetTexGendvFunc(GLenum @coord, GLenum @pname, GLdouble * @params);
        internal static glGetTexGendvFunc glGetTexGendvPtr;
        internal static void loadGetTexGendv()
        {
            try
            {
                glGetTexGendvPtr = (glGetTexGendvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexGendv"), typeof(glGetTexGendvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexGendv'.");
            }
        }
        public static void glGetTexGendv(GLenum @coord, GLenum @pname, GLdouble * @params) => glGetTexGendvPtr(@coord, @pname, @params);

        internal delegate void glGetTexGenfvFunc(GLenum @coord, GLenum @pname, GLfloat * @params);
        internal static glGetTexGenfvFunc glGetTexGenfvPtr;
        internal static void loadGetTexGenfv()
        {
            try
            {
                glGetTexGenfvPtr = (glGetTexGenfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexGenfv"), typeof(glGetTexGenfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexGenfv'.");
            }
        }
        public static void glGetTexGenfv(GLenum @coord, GLenum @pname, GLfloat * @params) => glGetTexGenfvPtr(@coord, @pname, @params);

        internal delegate void glGetTexGenivFunc(GLenum @coord, GLenum @pname, GLint * @params);
        internal static glGetTexGenivFunc glGetTexGenivPtr;
        internal static void loadGetTexGeniv()
        {
            try
            {
                glGetTexGenivPtr = (glGetTexGenivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexGeniv"), typeof(glGetTexGenivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexGeniv'.");
            }
        }
        public static void glGetTexGeniv(GLenum @coord, GLenum @pname, GLint * @params) => glGetTexGenivPtr(@coord, @pname, @params);

        internal delegate GLboolean glIsListFunc(GLuint @list);
        internal static glIsListFunc glIsListPtr;
        internal static void loadIsList()
        {
            try
            {
                glIsListPtr = (glIsListFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsList"), typeof(glIsListFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsList'.");
            }
        }
        public static GLboolean glIsList(GLuint @list) => glIsListPtr(@list);

        internal delegate void glFrustumFunc(GLdouble @left, GLdouble @right, GLdouble @bottom, GLdouble @top, GLdouble @zNear, GLdouble @zFar);
        internal static glFrustumFunc glFrustumPtr;
        internal static void loadFrustum()
        {
            try
            {
                glFrustumPtr = (glFrustumFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFrustum"), typeof(glFrustumFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFrustum'.");
            }
        }
        public static void glFrustum(GLdouble @left, GLdouble @right, GLdouble @bottom, GLdouble @top, GLdouble @zNear, GLdouble @zFar) => glFrustumPtr(@left, @right, @bottom, @top, @zNear, @zFar);

        internal delegate void glLoadIdentityFunc();
        internal static glLoadIdentityFunc glLoadIdentityPtr;
        internal static void loadLoadIdentity()
        {
            try
            {
                glLoadIdentityPtr = (glLoadIdentityFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadIdentity"), typeof(glLoadIdentityFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadIdentity'.");
            }
        }
        public static void glLoadIdentity() => glLoadIdentityPtr();

        internal delegate void glLoadMatrixfFunc(const GLfloat * @m);
        internal static glLoadMatrixfFunc glLoadMatrixfPtr;
        internal static void loadLoadMatrixf()
        {
            try
            {
                glLoadMatrixfPtr = (glLoadMatrixfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadMatrixf"), typeof(glLoadMatrixfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadMatrixf'.");
            }
        }
        public static void glLoadMatrixf(const GLfloat * @m) => glLoadMatrixfPtr(@m);

        internal delegate void glLoadMatrixdFunc(const GLdouble * @m);
        internal static glLoadMatrixdFunc glLoadMatrixdPtr;
        internal static void loadLoadMatrixd()
        {
            try
            {
                glLoadMatrixdPtr = (glLoadMatrixdFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadMatrixd"), typeof(glLoadMatrixdFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadMatrixd'.");
            }
        }
        public static void glLoadMatrixd(const GLdouble * @m) => glLoadMatrixdPtr(@m);

        internal delegate void glMatrixModeFunc(GLenum @mode);
        internal static glMatrixModeFunc glMatrixModePtr;
        internal static void loadMatrixMode()
        {
            try
            {
                glMatrixModePtr = (glMatrixModeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixMode"), typeof(glMatrixModeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixMode'.");
            }
        }
        public static void glMatrixMode(GLenum @mode) => glMatrixModePtr(@mode);

        internal delegate void glMultMatrixfFunc(const GLfloat * @m);
        internal static glMultMatrixfFunc glMultMatrixfPtr;
        internal static void loadMultMatrixf()
        {
            try
            {
                glMultMatrixfPtr = (glMultMatrixfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultMatrixf"), typeof(glMultMatrixfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultMatrixf'.");
            }
        }
        public static void glMultMatrixf(const GLfloat * @m) => glMultMatrixfPtr(@m);

        internal delegate void glMultMatrixdFunc(const GLdouble * @m);
        internal static glMultMatrixdFunc glMultMatrixdPtr;
        internal static void loadMultMatrixd()
        {
            try
            {
                glMultMatrixdPtr = (glMultMatrixdFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultMatrixd"), typeof(glMultMatrixdFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultMatrixd'.");
            }
        }
        public static void glMultMatrixd(const GLdouble * @m) => glMultMatrixdPtr(@m);

        internal delegate void glOrthoFunc(GLdouble @left, GLdouble @right, GLdouble @bottom, GLdouble @top, GLdouble @zNear, GLdouble @zFar);
        internal static glOrthoFunc glOrthoPtr;
        internal static void loadOrtho()
        {
            try
            {
                glOrthoPtr = (glOrthoFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glOrtho"), typeof(glOrthoFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glOrtho'.");
            }
        }
        public static void glOrtho(GLdouble @left, GLdouble @right, GLdouble @bottom, GLdouble @top, GLdouble @zNear, GLdouble @zFar) => glOrthoPtr(@left, @right, @bottom, @top, @zNear, @zFar);

        internal delegate void glPopMatrixFunc();
        internal static glPopMatrixFunc glPopMatrixPtr;
        internal static void loadPopMatrix()
        {
            try
            {
                glPopMatrixPtr = (glPopMatrixFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPopMatrix"), typeof(glPopMatrixFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPopMatrix'.");
            }
        }
        public static void glPopMatrix() => glPopMatrixPtr();

        internal delegate void glPushMatrixFunc();
        internal static glPushMatrixFunc glPushMatrixPtr;
        internal static void loadPushMatrix()
        {
            try
            {
                glPushMatrixPtr = (glPushMatrixFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPushMatrix"), typeof(glPushMatrixFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPushMatrix'.");
            }
        }
        public static void glPushMatrix() => glPushMatrixPtr();

        internal delegate void glRotatedFunc(GLdouble @angle, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glRotatedFunc glRotatedPtr;
        internal static void loadRotated()
        {
            try
            {
                glRotatedPtr = (glRotatedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRotated"), typeof(glRotatedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRotated'.");
            }
        }
        public static void glRotated(GLdouble @angle, GLdouble @x, GLdouble @y, GLdouble @z) => glRotatedPtr(@angle, @x, @y, @z);

        internal delegate void glRotatefFunc(GLfloat @angle, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glRotatefFunc glRotatefPtr;
        internal static void loadRotatef()
        {
            try
            {
                glRotatefPtr = (glRotatefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRotatef"), typeof(glRotatefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRotatef'.");
            }
        }
        public static void glRotatef(GLfloat @angle, GLfloat @x, GLfloat @y, GLfloat @z) => glRotatefPtr(@angle, @x, @y, @z);

        internal delegate void glScaledFunc(GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glScaledFunc glScaledPtr;
        internal static void loadScaled()
        {
            try
            {
                glScaledPtr = (glScaledFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScaled"), typeof(glScaledFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScaled'.");
            }
        }
        public static void glScaled(GLdouble @x, GLdouble @y, GLdouble @z) => glScaledPtr(@x, @y, @z);

        internal delegate void glScalefFunc(GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glScalefFunc glScalefPtr;
        internal static void loadScalef()
        {
            try
            {
                glScalefPtr = (glScalefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScalef"), typeof(glScalefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScalef'.");
            }
        }
        public static void glScalef(GLfloat @x, GLfloat @y, GLfloat @z) => glScalefPtr(@x, @y, @z);

        internal delegate void glTranslatedFunc(GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glTranslatedFunc glTranslatedPtr;
        internal static void loadTranslated()
        {
            try
            {
                glTranslatedPtr = (glTranslatedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTranslated"), typeof(glTranslatedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTranslated'.");
            }
        }
        public static void glTranslated(GLdouble @x, GLdouble @y, GLdouble @z) => glTranslatedPtr(@x, @y, @z);

        internal delegate void glTranslatefFunc(GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glTranslatefFunc glTranslatefPtr;
        internal static void loadTranslatef()
        {
            try
            {
                glTranslatefPtr = (glTranslatefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTranslatef"), typeof(glTranslatefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTranslatef'.");
            }
        }
        public static void glTranslatef(GLfloat @x, GLfloat @y, GLfloat @z) => glTranslatefPtr(@x, @y, @z);
        #endregion
    }
}
