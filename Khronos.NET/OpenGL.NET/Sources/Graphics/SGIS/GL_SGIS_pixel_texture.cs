using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_pixel_texture
    {
        #region Interop
        static GL_SGIS_pixel_texture()
        {
            Console.WriteLine("Initalising GL_SGIS_pixel_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPixelTexGenParameteriSGIS();
            loadPixelTexGenParameterivSGIS();
            loadPixelTexGenParameterfSGIS();
            loadPixelTexGenParameterfvSGIS();
            loadGetPixelTexGenParameterivSGIS();
            loadGetPixelTexGenParameterfvSGIS();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PIXEL_TEXTURE_SGIS = 0x8353;
        public static UInt32 GL_PIXEL_FRAGMENT_RGB_SOURCE_SGIS = 0x8354;
        public static UInt32 GL_PIXEL_FRAGMENT_ALPHA_SOURCE_SGIS = 0x8355;
        public static UInt32 GL_PIXEL_GROUP_COLOR_SGIS = 0x8356;
        #endregion

        #region Commands
        internal delegate void glPixelTexGenParameteriSGISFunc(GLenum @pname, GLint @param);
        internal static glPixelTexGenParameteriSGISFunc glPixelTexGenParameteriSGISPtr;
        internal static void loadPixelTexGenParameteriSGIS()
        {
            try
            {
                glPixelTexGenParameteriSGISPtr = (glPixelTexGenParameteriSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelTexGenParameteriSGIS"), typeof(glPixelTexGenParameteriSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelTexGenParameteriSGIS'.");
            }
        }
        public static void glPixelTexGenParameteriSGIS(GLenum @pname, GLint @param) => glPixelTexGenParameteriSGISPtr(@pname, @param);

        internal delegate void glPixelTexGenParameterivSGISFunc(GLenum @pname, const GLint * @params);
        internal static glPixelTexGenParameterivSGISFunc glPixelTexGenParameterivSGISPtr;
        internal static void loadPixelTexGenParameterivSGIS()
        {
            try
            {
                glPixelTexGenParameterivSGISPtr = (glPixelTexGenParameterivSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelTexGenParameterivSGIS"), typeof(glPixelTexGenParameterivSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelTexGenParameterivSGIS'.");
            }
        }
        public static void glPixelTexGenParameterivSGIS(GLenum @pname, const GLint * @params) => glPixelTexGenParameterivSGISPtr(@pname, @params);

        internal delegate void glPixelTexGenParameterfSGISFunc(GLenum @pname, GLfloat @param);
        internal static glPixelTexGenParameterfSGISFunc glPixelTexGenParameterfSGISPtr;
        internal static void loadPixelTexGenParameterfSGIS()
        {
            try
            {
                glPixelTexGenParameterfSGISPtr = (glPixelTexGenParameterfSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelTexGenParameterfSGIS"), typeof(glPixelTexGenParameterfSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelTexGenParameterfSGIS'.");
            }
        }
        public static void glPixelTexGenParameterfSGIS(GLenum @pname, GLfloat @param) => glPixelTexGenParameterfSGISPtr(@pname, @param);

        internal delegate void glPixelTexGenParameterfvSGISFunc(GLenum @pname, const GLfloat * @params);
        internal static glPixelTexGenParameterfvSGISFunc glPixelTexGenParameterfvSGISPtr;
        internal static void loadPixelTexGenParameterfvSGIS()
        {
            try
            {
                glPixelTexGenParameterfvSGISPtr = (glPixelTexGenParameterfvSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelTexGenParameterfvSGIS"), typeof(glPixelTexGenParameterfvSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelTexGenParameterfvSGIS'.");
            }
        }
        public static void glPixelTexGenParameterfvSGIS(GLenum @pname, const GLfloat * @params) => glPixelTexGenParameterfvSGISPtr(@pname, @params);

        internal delegate void glGetPixelTexGenParameterivSGISFunc(GLenum @pname, GLint * @params);
        internal static glGetPixelTexGenParameterivSGISFunc glGetPixelTexGenParameterivSGISPtr;
        internal static void loadGetPixelTexGenParameterivSGIS()
        {
            try
            {
                glGetPixelTexGenParameterivSGISPtr = (glGetPixelTexGenParameterivSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPixelTexGenParameterivSGIS"), typeof(glGetPixelTexGenParameterivSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPixelTexGenParameterivSGIS'.");
            }
        }
        public static void glGetPixelTexGenParameterivSGIS(GLenum @pname, GLint * @params) => glGetPixelTexGenParameterivSGISPtr(@pname, @params);

        internal delegate void glGetPixelTexGenParameterfvSGISFunc(GLenum @pname, GLfloat * @params);
        internal static glGetPixelTexGenParameterfvSGISFunc glGetPixelTexGenParameterfvSGISPtr;
        internal static void loadGetPixelTexGenParameterfvSGIS()
        {
            try
            {
                glGetPixelTexGenParameterfvSGISPtr = (glGetPixelTexGenParameterfvSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPixelTexGenParameterfvSGIS"), typeof(glGetPixelTexGenParameterfvSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPixelTexGenParameterfvSGIS'.");
            }
        }
        public static void glGetPixelTexGenParameterfvSGIS(GLenum @pname, GLfloat * @params) => glGetPixelTexGenParameterfvSGISPtr(@pname, @params);
        #endregion
    }
}
