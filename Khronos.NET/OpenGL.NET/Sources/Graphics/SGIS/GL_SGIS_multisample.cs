using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_multisample
    {
        #region Interop
        static GL_SGIS_multisample()
        {
            Console.WriteLine("Initalising GL_SGIS_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadSampleMaskSGIS();
            loadSamplePatternSGIS();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MULTISAMPLE_SGIS = 0x809D;
        public static UInt32 GL_SAMPLE_ALPHA_TO_MASK_SGIS = 0x809E;
        public static UInt32 GL_SAMPLE_ALPHA_TO_ONE_SGIS = 0x809F;
        public static UInt32 GL_SAMPLE_MASK_SGIS = 0x80A0;
        public static UInt32 GL_1PASS_SGIS = 0x80A1;
        public static UInt32 GL_2PASS_0_SGIS = 0x80A2;
        public static UInt32 GL_2PASS_1_SGIS = 0x80A3;
        public static UInt32 GL_4PASS_0_SGIS = 0x80A4;
        public static UInt32 GL_4PASS_1_SGIS = 0x80A5;
        public static UInt32 GL_4PASS_2_SGIS = 0x80A6;
        public static UInt32 GL_4PASS_3_SGIS = 0x80A7;
        public static UInt32 GL_SAMPLE_BUFFERS_SGIS = 0x80A8;
        public static UInt32 GL_SAMPLES_SGIS = 0x80A9;
        public static UInt32 GL_SAMPLE_MASK_VALUE_SGIS = 0x80AA;
        public static UInt32 GL_SAMPLE_MASK_INVERT_SGIS = 0x80AB;
        public static UInt32 GL_SAMPLE_PATTERN_SGIS = 0x80AC;
        #endregion

        #region Commands
        internal delegate void glSampleMaskSGISFunc(GLclampf @value, GLboolean @invert);
        internal static glSampleMaskSGISFunc glSampleMaskSGISPtr;
        internal static void loadSampleMaskSGIS()
        {
            try
            {
                glSampleMaskSGISPtr = (glSampleMaskSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleMaskSGIS"), typeof(glSampleMaskSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleMaskSGIS'.");
            }
        }
        public static void glSampleMaskSGIS(GLclampf @value, GLboolean @invert) => glSampleMaskSGISPtr(@value, @invert);

        internal delegate void glSamplePatternSGISFunc(GLenum @pattern);
        internal static glSamplePatternSGISFunc glSamplePatternSGISPtr;
        internal static void loadSamplePatternSGIS()
        {
            try
            {
                glSamplePatternSGISPtr = (glSamplePatternSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplePatternSGIS"), typeof(glSamplePatternSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplePatternSGIS'.");
            }
        }
        public static void glSamplePatternSGIS(GLenum @pattern) => glSamplePatternSGISPtr(@pattern);
        #endregion
    }
}
