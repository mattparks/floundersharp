using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_texture_color_mask
    {
        #region Interop
        static GL_SGIS_texture_color_mask()
        {
            Console.WriteLine("Initalising GL_SGIS_texture_color_mask interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTextureColorMaskSGIS();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_COLOR_WRITEMASK_SGIS = 0x81EF;
        #endregion

        #region Commands
        internal delegate void glTextureColorMaskSGISFunc(GLboolean @red, GLboolean @green, GLboolean @blue, GLboolean @alpha);
        internal static glTextureColorMaskSGISFunc glTextureColorMaskSGISPtr;
        internal static void loadTextureColorMaskSGIS()
        {
            try
            {
                glTextureColorMaskSGISPtr = (glTextureColorMaskSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureColorMaskSGIS"), typeof(glTextureColorMaskSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureColorMaskSGIS'.");
            }
        }
        public static void glTextureColorMaskSGIS(GLboolean @red, GLboolean @green, GLboolean @blue, GLboolean @alpha) => glTextureColorMaskSGISPtr(@red, @green, @blue, @alpha);
        #endregion
    }
}
