using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_point_line_texgen
    {
        #region Interop
        static GL_SGIS_point_line_texgen()
        {
            Console.WriteLine("Initalising GL_SGIS_point_line_texgen interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_EYE_DISTANCE_TO_POINT_SGIS = 0x81F0;
        public static UInt32 GL_OBJECT_DISTANCE_TO_POINT_SGIS = 0x81F1;
        public static UInt32 GL_EYE_DISTANCE_TO_LINE_SGIS = 0x81F2;
        public static UInt32 GL_OBJECT_DISTANCE_TO_LINE_SGIS = 0x81F3;
        public static UInt32 GL_EYE_POINT_SGIS = 0x81F4;
        public static UInt32 GL_OBJECT_POINT_SGIS = 0x81F5;
        public static UInt32 GL_EYE_LINE_SGIS = 0x81F6;
        public static UInt32 GL_OBJECT_LINE_SGIS = 0x81F7;
        #endregion

        #region Commands
        #endregion
    }
}
