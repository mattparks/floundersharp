using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_texture4D
    {
        #region Interop
        static GL_SGIS_texture4D()
        {
            Console.WriteLine("Initalising GL_SGIS_texture4D interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexImage4DSGIS();
            loadTexSubImage4DSGIS();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PACK_SKIP_VOLUMES_SGIS = 0x8130;
        public static UInt32 GL_PACK_IMAGE_DEPTH_SGIS = 0x8131;
        public static UInt32 GL_UNPACK_SKIP_VOLUMES_SGIS = 0x8132;
        public static UInt32 GL_UNPACK_IMAGE_DEPTH_SGIS = 0x8133;
        public static UInt32 GL_TEXTURE_4D_SGIS = 0x8134;
        public static UInt32 GL_PROXY_TEXTURE_4D_SGIS = 0x8135;
        public static UInt32 GL_TEXTURE_4DSIZE_SGIS = 0x8136;
        public static UInt32 GL_TEXTURE_WRAP_Q_SGIS = 0x8137;
        public static UInt32 GL_MAX_4D_TEXTURE_SIZE_SGIS = 0x8138;
        public static UInt32 GL_TEXTURE_4D_BINDING_SGIS = 0x814F;
        #endregion

        #region Commands
        internal delegate void glTexImage4DSGISFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLsizei @size4d, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexImage4DSGISFunc glTexImage4DSGISPtr;
        internal static void loadTexImage4DSGIS()
        {
            try
            {
                glTexImage4DSGISPtr = (glTexImage4DSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage4DSGIS"), typeof(glTexImage4DSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage4DSGIS'.");
            }
        }
        public static void glTexImage4DSGIS(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLsizei @size4d, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glTexImage4DSGISPtr(@target, @level, @internalformat, @width, @height, @depth, @size4d, @border, @format, @type, @pixels);

        internal delegate void glTexSubImage4DSGISFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @woffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLsizei @size4d, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexSubImage4DSGISFunc glTexSubImage4DSGISPtr;
        internal static void loadTexSubImage4DSGIS()
        {
            try
            {
                glTexSubImage4DSGISPtr = (glTexSubImage4DSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexSubImage4DSGIS"), typeof(glTexSubImage4DSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexSubImage4DSGIS'.");
            }
        }
        public static void glTexSubImage4DSGIS(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @woffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLsizei @size4d, GLenum @format, GLenum @type, const void * @pixels) => glTexSubImage4DSGISPtr(@target, @level, @xoffset, @yoffset, @zoffset, @woffset, @width, @height, @depth, @size4d, @format, @type, @pixels);
        #endregion
    }
}
