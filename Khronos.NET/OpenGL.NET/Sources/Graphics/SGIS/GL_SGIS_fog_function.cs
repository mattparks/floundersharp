using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_fog_function
    {
        #region Interop
        static GL_SGIS_fog_function()
        {
            Console.WriteLine("Initalising GL_SGIS_fog_function interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFogFuncSGIS();
            loadGetFogFuncSGIS();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FOG_FUNC_SGIS = 0x812A;
        public static UInt32 GL_FOG_FUNC_POINTS_SGIS = 0x812B;
        public static UInt32 GL_MAX_FOG_FUNC_POINTS_SGIS = 0x812C;
        #endregion

        #region Commands
        internal delegate void glFogFuncSGISFunc(GLsizei @n, const GLfloat * @points);
        internal static glFogFuncSGISFunc glFogFuncSGISPtr;
        internal static void loadFogFuncSGIS()
        {
            try
            {
                glFogFuncSGISPtr = (glFogFuncSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogFuncSGIS"), typeof(glFogFuncSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogFuncSGIS'.");
            }
        }
        public static void glFogFuncSGIS(GLsizei @n, const GLfloat * @points) => glFogFuncSGISPtr(@n, @points);

        internal delegate void glGetFogFuncSGISFunc(GLfloat * @points);
        internal static glGetFogFuncSGISFunc glGetFogFuncSGISPtr;
        internal static void loadGetFogFuncSGIS()
        {
            try
            {
                glGetFogFuncSGISPtr = (glGetFogFuncSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFogFuncSGIS"), typeof(glGetFogFuncSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFogFuncSGIS'.");
            }
        }
        public static void glGetFogFuncSGIS(GLfloat * @points) => glGetFogFuncSGISPtr(@points);
        #endregion
    }
}
