using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_texture_filter4
    {
        #region Interop
        static GL_SGIS_texture_filter4()
        {
            Console.WriteLine("Initalising GL_SGIS_texture_filter4 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetTexFilterFuncSGIS();
            loadTexFilterFuncSGIS();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FILTER4_SGIS = 0x8146;
        public static UInt32 GL_TEXTURE_FILTER4_SIZE_SGIS = 0x8147;
        #endregion

        #region Commands
        internal delegate void glGetTexFilterFuncSGISFunc(GLenum @target, GLenum @filter, GLfloat * @weights);
        internal static glGetTexFilterFuncSGISFunc glGetTexFilterFuncSGISPtr;
        internal static void loadGetTexFilterFuncSGIS()
        {
            try
            {
                glGetTexFilterFuncSGISPtr = (glGetTexFilterFuncSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexFilterFuncSGIS"), typeof(glGetTexFilterFuncSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexFilterFuncSGIS'.");
            }
        }
        public static void glGetTexFilterFuncSGIS(GLenum @target, GLenum @filter, GLfloat * @weights) => glGetTexFilterFuncSGISPtr(@target, @filter, @weights);

        internal delegate void glTexFilterFuncSGISFunc(GLenum @target, GLenum @filter, GLsizei @n, const GLfloat * @weights);
        internal static glTexFilterFuncSGISFunc glTexFilterFuncSGISPtr;
        internal static void loadTexFilterFuncSGIS()
        {
            try
            {
                glTexFilterFuncSGISPtr = (glTexFilterFuncSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexFilterFuncSGIS"), typeof(glTexFilterFuncSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexFilterFuncSGIS'.");
            }
        }
        public static void glTexFilterFuncSGIS(GLenum @target, GLenum @filter, GLsizei @n, const GLfloat * @weights) => glTexFilterFuncSGISPtr(@target, @filter, @n, @weights);
        #endregion
    }
}
