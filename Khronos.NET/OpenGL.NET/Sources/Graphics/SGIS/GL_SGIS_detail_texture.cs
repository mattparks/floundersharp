using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_detail_texture
    {
        #region Interop
        static GL_SGIS_detail_texture()
        {
            Console.WriteLine("Initalising GL_SGIS_detail_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDetailTexFuncSGIS();
            loadGetDetailTexFuncSGIS();
        }
        #endregion

        #region Enums
        public static UInt32 GL_DETAIL_TEXTURE_2D_SGIS = 0x8095;
        public static UInt32 GL_DETAIL_TEXTURE_2D_BINDING_SGIS = 0x8096;
        public static UInt32 GL_LINEAR_DETAIL_SGIS = 0x8097;
        public static UInt32 GL_LINEAR_DETAIL_ALPHA_SGIS = 0x8098;
        public static UInt32 GL_LINEAR_DETAIL_COLOR_SGIS = 0x8099;
        public static UInt32 GL_DETAIL_TEXTURE_LEVEL_SGIS = 0x809A;
        public static UInt32 GL_DETAIL_TEXTURE_MODE_SGIS = 0x809B;
        public static UInt32 GL_DETAIL_TEXTURE_FUNC_POINTS_SGIS = 0x809C;
        #endregion

        #region Commands
        internal delegate void glDetailTexFuncSGISFunc(GLenum @target, GLsizei @n, const GLfloat * @points);
        internal static glDetailTexFuncSGISFunc glDetailTexFuncSGISPtr;
        internal static void loadDetailTexFuncSGIS()
        {
            try
            {
                glDetailTexFuncSGISPtr = (glDetailTexFuncSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDetailTexFuncSGIS"), typeof(glDetailTexFuncSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDetailTexFuncSGIS'.");
            }
        }
        public static void glDetailTexFuncSGIS(GLenum @target, GLsizei @n, const GLfloat * @points) => glDetailTexFuncSGISPtr(@target, @n, @points);

        internal delegate void glGetDetailTexFuncSGISFunc(GLenum @target, GLfloat * @points);
        internal static glGetDetailTexFuncSGISFunc glGetDetailTexFuncSGISPtr;
        internal static void loadGetDetailTexFuncSGIS()
        {
            try
            {
                glGetDetailTexFuncSGISPtr = (glGetDetailTexFuncSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDetailTexFuncSGIS"), typeof(glGetDetailTexFuncSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDetailTexFuncSGIS'.");
            }
        }
        public static void glGetDetailTexFuncSGIS(GLenum @target, GLfloat * @points) => glGetDetailTexFuncSGISPtr(@target, @points);
        #endregion
    }
}
