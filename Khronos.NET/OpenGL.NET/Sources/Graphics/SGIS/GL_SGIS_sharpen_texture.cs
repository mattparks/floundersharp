using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_sharpen_texture
    {
        #region Interop
        static GL_SGIS_sharpen_texture()
        {
            Console.WriteLine("Initalising GL_SGIS_sharpen_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadSharpenTexFuncSGIS();
            loadGetSharpenTexFuncSGIS();
        }
        #endregion

        #region Enums
        public static UInt32 GL_LINEAR_SHARPEN_SGIS = 0x80AD;
        public static UInt32 GL_LINEAR_SHARPEN_ALPHA_SGIS = 0x80AE;
        public static UInt32 GL_LINEAR_SHARPEN_COLOR_SGIS = 0x80AF;
        public static UInt32 GL_SHARPEN_TEXTURE_FUNC_POINTS_SGIS = 0x80B0;
        #endregion

        #region Commands
        internal delegate void glSharpenTexFuncSGISFunc(GLenum @target, GLsizei @n, const GLfloat * @points);
        internal static glSharpenTexFuncSGISFunc glSharpenTexFuncSGISPtr;
        internal static void loadSharpenTexFuncSGIS()
        {
            try
            {
                glSharpenTexFuncSGISPtr = (glSharpenTexFuncSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSharpenTexFuncSGIS"), typeof(glSharpenTexFuncSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSharpenTexFuncSGIS'.");
            }
        }
        public static void glSharpenTexFuncSGIS(GLenum @target, GLsizei @n, const GLfloat * @points) => glSharpenTexFuncSGISPtr(@target, @n, @points);

        internal delegate void glGetSharpenTexFuncSGISFunc(GLenum @target, GLfloat * @points);
        internal static glGetSharpenTexFuncSGISFunc glGetSharpenTexFuncSGISPtr;
        internal static void loadGetSharpenTexFuncSGIS()
        {
            try
            {
                glGetSharpenTexFuncSGISPtr = (glGetSharpenTexFuncSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSharpenTexFuncSGIS"), typeof(glGetSharpenTexFuncSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSharpenTexFuncSGIS'.");
            }
        }
        public static void glGetSharpenTexFuncSGIS(GLenum @target, GLfloat * @points) => glGetSharpenTexFuncSGISPtr(@target, @points);
        #endregion
    }
}
