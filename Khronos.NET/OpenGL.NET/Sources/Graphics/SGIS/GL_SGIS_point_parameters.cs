using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_point_parameters
    {
        #region Interop
        static GL_SGIS_point_parameters()
        {
            Console.WriteLine("Initalising GL_SGIS_point_parameters interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPointParameterfSGIS();
            loadPointParameterfvSGIS();
        }
        #endregion

        #region Enums
        public static UInt32 GL_POINT_SIZE_MIN_SGIS = 0x8126;
        public static UInt32 GL_POINT_SIZE_MAX_SGIS = 0x8127;
        public static UInt32 GL_POINT_FADE_THRESHOLD_SIZE_SGIS = 0x8128;
        public static UInt32 GL_DISTANCE_ATTENUATION_SGIS = 0x8129;
        #endregion

        #region Commands
        internal delegate void glPointParameterfSGISFunc(GLenum @pname, GLfloat @param);
        internal static glPointParameterfSGISFunc glPointParameterfSGISPtr;
        internal static void loadPointParameterfSGIS()
        {
            try
            {
                glPointParameterfSGISPtr = (glPointParameterfSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterfSGIS"), typeof(glPointParameterfSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterfSGIS'.");
            }
        }
        public static void glPointParameterfSGIS(GLenum @pname, GLfloat @param) => glPointParameterfSGISPtr(@pname, @param);

        internal delegate void glPointParameterfvSGISFunc(GLenum @pname, const GLfloat * @params);
        internal static glPointParameterfvSGISFunc glPointParameterfvSGISPtr;
        internal static void loadPointParameterfvSGIS()
        {
            try
            {
                glPointParameterfvSGISPtr = (glPointParameterfvSGISFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterfvSGIS"), typeof(glPointParameterfvSGISFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterfvSGIS'.");
            }
        }
        public static void glPointParameterfvSGIS(GLenum @pname, const GLfloat * @params) => glPointParameterfvSGISPtr(@pname, @params);
        #endregion
    }
}
