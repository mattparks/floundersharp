using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_texture_lod
    {
        #region Interop
        static GL_SGIS_texture_lod()
        {
            Console.WriteLine("Initalising GL_SGIS_texture_lod interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_MIN_LOD_SGIS = 0x813A;
        public static UInt32 GL_TEXTURE_MAX_LOD_SGIS = 0x813B;
        public static UInt32 GL_TEXTURE_BASE_LEVEL_SGIS = 0x813C;
        public static UInt32 GL_TEXTURE_MAX_LEVEL_SGIS = 0x813D;
        #endregion

        #region Commands
        #endregion
    }
}
