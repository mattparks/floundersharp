using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_generate_mipmap
    {
        #region Interop
        static GL_SGIS_generate_mipmap()
        {
            Console.WriteLine("Initalising GL_SGIS_generate_mipmap interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_GENERATE_MIPMAP_SGIS = 0x8191;
        public static UInt32 GL_GENERATE_MIPMAP_HINT_SGIS = 0x8192;
        #endregion

        #region Commands
        #endregion
    }
}
