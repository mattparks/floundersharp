using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_texture_select
    {
        #region Interop
        static GL_SGIS_texture_select()
        {
            Console.WriteLine("Initalising GL_SGIS_texture_select interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DUAL_ALPHA4_SGIS = 0x8110;
        public static UInt32 GL_DUAL_ALPHA8_SGIS = 0x8111;
        public static UInt32 GL_DUAL_ALPHA12_SGIS = 0x8112;
        public static UInt32 GL_DUAL_ALPHA16_SGIS = 0x8113;
        public static UInt32 GL_DUAL_LUMINANCE4_SGIS = 0x8114;
        public static UInt32 GL_DUAL_LUMINANCE8_SGIS = 0x8115;
        public static UInt32 GL_DUAL_LUMINANCE12_SGIS = 0x8116;
        public static UInt32 GL_DUAL_LUMINANCE16_SGIS = 0x8117;
        public static UInt32 GL_DUAL_INTENSITY4_SGIS = 0x8118;
        public static UInt32 GL_DUAL_INTENSITY8_SGIS = 0x8119;
        public static UInt32 GL_DUAL_INTENSITY12_SGIS = 0x811A;
        public static UInt32 GL_DUAL_INTENSITY16_SGIS = 0x811B;
        public static UInt32 GL_DUAL_LUMINANCE_ALPHA4_SGIS = 0x811C;
        public static UInt32 GL_DUAL_LUMINANCE_ALPHA8_SGIS = 0x811D;
        public static UInt32 GL_QUAD_ALPHA4_SGIS = 0x811E;
        public static UInt32 GL_QUAD_ALPHA8_SGIS = 0x811F;
        public static UInt32 GL_QUAD_LUMINANCE4_SGIS = 0x8120;
        public static UInt32 GL_QUAD_LUMINANCE8_SGIS = 0x8121;
        public static UInt32 GL_QUAD_INTENSITY4_SGIS = 0x8122;
        public static UInt32 GL_QUAD_INTENSITY8_SGIS = 0x8123;
        public static UInt32 GL_DUAL_TEXTURE_SELECT_SGIS = 0x8124;
        public static UInt32 GL_QUAD_TEXTURE_SELECT_SGIS = 0x8125;
        #endregion

        #region Commands
        #endregion
    }
}
