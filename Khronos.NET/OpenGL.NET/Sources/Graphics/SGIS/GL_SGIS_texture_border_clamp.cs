using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIS_texture_border_clamp
    {
        #region Interop
        static GL_SGIS_texture_border_clamp()
        {
            Console.WriteLine("Initalising GL_SGIS_texture_border_clamp interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_CLAMP_TO_BORDER_SGIS = 0x812D;
        #endregion

        #region Commands
        #endregion
    }
}
