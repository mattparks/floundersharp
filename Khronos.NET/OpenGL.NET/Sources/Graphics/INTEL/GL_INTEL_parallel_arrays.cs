using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_INTEL_parallel_arrays
    {
        #region Interop
        static GL_INTEL_parallel_arrays()
        {
            Console.WriteLine("Initalising GL_INTEL_parallel_arrays interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexPointervINTEL();
            loadNormalPointervINTEL();
            loadColorPointervINTEL();
            loadTexCoordPointervINTEL();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PARALLEL_ARRAYS_INTEL = 0x83F4;
        public static UInt32 GL_VERTEX_ARRAY_PARALLEL_POINTERS_INTEL = 0x83F5;
        public static UInt32 GL_NORMAL_ARRAY_PARALLEL_POINTERS_INTEL = 0x83F6;
        public static UInt32 GL_COLOR_ARRAY_PARALLEL_POINTERS_INTEL = 0x83F7;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_PARALLEL_POINTERS_INTEL = 0x83F8;
        #endregion

        #region Commands
        internal delegate void glVertexPointervINTELFunc(GLint @size, GLenum @type, const void ** @pointer);
        internal static glVertexPointervINTELFunc glVertexPointervINTELPtr;
        internal static void loadVertexPointervINTEL()
        {
            try
            {
                glVertexPointervINTELPtr = (glVertexPointervINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexPointervINTEL"), typeof(glVertexPointervINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexPointervINTEL'.");
            }
        }
        public static void glVertexPointervINTEL(GLint @size, GLenum @type, const void ** @pointer) => glVertexPointervINTELPtr(@size, @type, @pointer);

        internal delegate void glNormalPointervINTELFunc(GLenum @type, const void ** @pointer);
        internal static glNormalPointervINTELFunc glNormalPointervINTELPtr;
        internal static void loadNormalPointervINTEL()
        {
            try
            {
                glNormalPointervINTELPtr = (glNormalPointervINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalPointervINTEL"), typeof(glNormalPointervINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalPointervINTEL'.");
            }
        }
        public static void glNormalPointervINTEL(GLenum @type, const void ** @pointer) => glNormalPointervINTELPtr(@type, @pointer);

        internal delegate void glColorPointervINTELFunc(GLint @size, GLenum @type, const void ** @pointer);
        internal static glColorPointervINTELFunc glColorPointervINTELPtr;
        internal static void loadColorPointervINTEL()
        {
            try
            {
                glColorPointervINTELPtr = (glColorPointervINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorPointervINTEL"), typeof(glColorPointervINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorPointervINTEL'.");
            }
        }
        public static void glColorPointervINTEL(GLint @size, GLenum @type, const void ** @pointer) => glColorPointervINTELPtr(@size, @type, @pointer);

        internal delegate void glTexCoordPointervINTELFunc(GLint @size, GLenum @type, const void ** @pointer);
        internal static glTexCoordPointervINTELFunc glTexCoordPointervINTELPtr;
        internal static void loadTexCoordPointervINTEL()
        {
            try
            {
                glTexCoordPointervINTELPtr = (glTexCoordPointervINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordPointervINTEL"), typeof(glTexCoordPointervINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordPointervINTEL'.");
            }
        }
        public static void glTexCoordPointervINTEL(GLint @size, GLenum @type, const void ** @pointer) => glTexCoordPointervINTELPtr(@size, @type, @pointer);
        #endregion
    }
}
