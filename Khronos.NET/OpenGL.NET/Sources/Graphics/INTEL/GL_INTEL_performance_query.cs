using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_INTEL_performance_query
    {
        #region Interop
        static GL_INTEL_performance_query()
        {
            Console.WriteLine("Initalising GL_INTEL_performance_query interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBeginPerfQueryINTEL();
            loadCreatePerfQueryINTEL();
            loadDeletePerfQueryINTEL();
            loadEndPerfQueryINTEL();
            loadGetFirstPerfQueryIdINTEL();
            loadGetNextPerfQueryIdINTEL();
            loadGetPerfCounterInfoINTEL();
            loadGetPerfQueryDataINTEL();
            loadGetPerfQueryIdByNameINTEL();
            loadGetPerfQueryInfoINTEL();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PERFQUERY_SINGLE_CONTEXT_INTEL = 0x00000000;
        public static UInt32 GL_PERFQUERY_GLOBAL_CONTEXT_INTEL = 0x00000001;
        public static UInt32 GL_PERFQUERY_WAIT_INTEL = 0x83FB;
        public static UInt32 GL_PERFQUERY_FLUSH_INTEL = 0x83FA;
        public static UInt32 GL_PERFQUERY_DONOT_FLUSH_INTEL = 0x83F9;
        public static UInt32 GL_PERFQUERY_COUNTER_EVENT_INTEL = 0x94F0;
        public static UInt32 GL_PERFQUERY_COUNTER_DURATION_NORM_INTEL = 0x94F1;
        public static UInt32 GL_PERFQUERY_COUNTER_DURATION_RAW_INTEL = 0x94F2;
        public static UInt32 GL_PERFQUERY_COUNTER_THROUGHPUT_INTEL = 0x94F3;
        public static UInt32 GL_PERFQUERY_COUNTER_RAW_INTEL = 0x94F4;
        public static UInt32 GL_PERFQUERY_COUNTER_TIMESTAMP_INTEL = 0x94F5;
        public static UInt32 GL_PERFQUERY_COUNTER_DATA_UINT32_INTEL = 0x94F8;
        public static UInt32 GL_PERFQUERY_COUNTER_DATA_UINT64_INTEL = 0x94F9;
        public static UInt32 GL_PERFQUERY_COUNTER_DATA_FLOAT_INTEL = 0x94FA;
        public static UInt32 GL_PERFQUERY_COUNTER_DATA_DOUBLE_INTEL = 0x94FB;
        public static UInt32 GL_PERFQUERY_COUNTER_DATA_BOOL32_INTEL = 0x94FC;
        public static UInt32 GL_PERFQUERY_QUERY_NAME_LENGTH_MAX_INTEL = 0x94FD;
        public static UInt32 GL_PERFQUERY_COUNTER_NAME_LENGTH_MAX_INTEL = 0x94FE;
        public static UInt32 GL_PERFQUERY_COUNTER_DESC_LENGTH_MAX_INTEL = 0x94FF;
        public static UInt32 GL_PERFQUERY_GPA_EXTENDED_COUNTERS_INTEL = 0x9500;
        #endregion

        #region Commands
        internal delegate void glBeginPerfQueryINTELFunc(GLuint @queryHandle);
        internal static glBeginPerfQueryINTELFunc glBeginPerfQueryINTELPtr;
        internal static void loadBeginPerfQueryINTEL()
        {
            try
            {
                glBeginPerfQueryINTELPtr = (glBeginPerfQueryINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginPerfQueryINTEL"), typeof(glBeginPerfQueryINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginPerfQueryINTEL'.");
            }
        }
        public static void glBeginPerfQueryINTEL(GLuint @queryHandle) => glBeginPerfQueryINTELPtr(@queryHandle);

        internal delegate void glCreatePerfQueryINTELFunc(GLuint @queryId, GLuint * @queryHandle);
        internal static glCreatePerfQueryINTELFunc glCreatePerfQueryINTELPtr;
        internal static void loadCreatePerfQueryINTEL()
        {
            try
            {
                glCreatePerfQueryINTELPtr = (glCreatePerfQueryINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreatePerfQueryINTEL"), typeof(glCreatePerfQueryINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreatePerfQueryINTEL'.");
            }
        }
        public static void glCreatePerfQueryINTEL(GLuint @queryId, GLuint * @queryHandle) => glCreatePerfQueryINTELPtr(@queryId, @queryHandle);

        internal delegate void glDeletePerfQueryINTELFunc(GLuint @queryHandle);
        internal static glDeletePerfQueryINTELFunc glDeletePerfQueryINTELPtr;
        internal static void loadDeletePerfQueryINTEL()
        {
            try
            {
                glDeletePerfQueryINTELPtr = (glDeletePerfQueryINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeletePerfQueryINTEL"), typeof(glDeletePerfQueryINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeletePerfQueryINTEL'.");
            }
        }
        public static void glDeletePerfQueryINTEL(GLuint @queryHandle) => glDeletePerfQueryINTELPtr(@queryHandle);

        internal delegate void glEndPerfQueryINTELFunc(GLuint @queryHandle);
        internal static glEndPerfQueryINTELFunc glEndPerfQueryINTELPtr;
        internal static void loadEndPerfQueryINTEL()
        {
            try
            {
                glEndPerfQueryINTELPtr = (glEndPerfQueryINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndPerfQueryINTEL"), typeof(glEndPerfQueryINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndPerfQueryINTEL'.");
            }
        }
        public static void glEndPerfQueryINTEL(GLuint @queryHandle) => glEndPerfQueryINTELPtr(@queryHandle);

        internal delegate void glGetFirstPerfQueryIdINTELFunc(GLuint * @queryId);
        internal static glGetFirstPerfQueryIdINTELFunc glGetFirstPerfQueryIdINTELPtr;
        internal static void loadGetFirstPerfQueryIdINTEL()
        {
            try
            {
                glGetFirstPerfQueryIdINTELPtr = (glGetFirstPerfQueryIdINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFirstPerfQueryIdINTEL"), typeof(glGetFirstPerfQueryIdINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFirstPerfQueryIdINTEL'.");
            }
        }
        public static void glGetFirstPerfQueryIdINTEL(GLuint * @queryId) => glGetFirstPerfQueryIdINTELPtr(@queryId);

        internal delegate void glGetNextPerfQueryIdINTELFunc(GLuint @queryId, GLuint * @nextQueryId);
        internal static glGetNextPerfQueryIdINTELFunc glGetNextPerfQueryIdINTELPtr;
        internal static void loadGetNextPerfQueryIdINTEL()
        {
            try
            {
                glGetNextPerfQueryIdINTELPtr = (glGetNextPerfQueryIdINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNextPerfQueryIdINTEL"), typeof(glGetNextPerfQueryIdINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNextPerfQueryIdINTEL'.");
            }
        }
        public static void glGetNextPerfQueryIdINTEL(GLuint @queryId, GLuint * @nextQueryId) => glGetNextPerfQueryIdINTELPtr(@queryId, @nextQueryId);

        internal delegate void glGetPerfCounterInfoINTELFunc(GLuint @queryId, GLuint @counterId, GLuint @counterNameLength, GLchar * @counterName, GLuint @counterDescLength, GLchar * @counterDesc, GLuint * @counterOffset, GLuint * @counterDataSize, GLuint * @counterTypeEnum, GLuint * @counterDataTypeEnum, GLuint64 * @rawCounterMaxValue);
        internal static glGetPerfCounterInfoINTELFunc glGetPerfCounterInfoINTELPtr;
        internal static void loadGetPerfCounterInfoINTEL()
        {
            try
            {
                glGetPerfCounterInfoINTELPtr = (glGetPerfCounterInfoINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPerfCounterInfoINTEL"), typeof(glGetPerfCounterInfoINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPerfCounterInfoINTEL'.");
            }
        }
        public static void glGetPerfCounterInfoINTEL(GLuint @queryId, GLuint @counterId, GLuint @counterNameLength, GLchar * @counterName, GLuint @counterDescLength, GLchar * @counterDesc, GLuint * @counterOffset, GLuint * @counterDataSize, GLuint * @counterTypeEnum, GLuint * @counterDataTypeEnum, GLuint64 * @rawCounterMaxValue) => glGetPerfCounterInfoINTELPtr(@queryId, @counterId, @counterNameLength, @counterName, @counterDescLength, @counterDesc, @counterOffset, @counterDataSize, @counterTypeEnum, @counterDataTypeEnum, @rawCounterMaxValue);

        internal delegate void glGetPerfQueryDataINTELFunc(GLuint @queryHandle, GLuint @flags, GLsizei @dataSize, GLvoid * @data, GLuint * @bytesWritten);
        internal static glGetPerfQueryDataINTELFunc glGetPerfQueryDataINTELPtr;
        internal static void loadGetPerfQueryDataINTEL()
        {
            try
            {
                glGetPerfQueryDataINTELPtr = (glGetPerfQueryDataINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPerfQueryDataINTEL"), typeof(glGetPerfQueryDataINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPerfQueryDataINTEL'.");
            }
        }
        public static void glGetPerfQueryDataINTEL(GLuint @queryHandle, GLuint @flags, GLsizei @dataSize, GLvoid * @data, GLuint * @bytesWritten) => glGetPerfQueryDataINTELPtr(@queryHandle, @flags, @dataSize, @data, @bytesWritten);

        internal delegate void glGetPerfQueryIdByNameINTELFunc(GLchar * @queryName, GLuint * @queryId);
        internal static glGetPerfQueryIdByNameINTELFunc glGetPerfQueryIdByNameINTELPtr;
        internal static void loadGetPerfQueryIdByNameINTEL()
        {
            try
            {
                glGetPerfQueryIdByNameINTELPtr = (glGetPerfQueryIdByNameINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPerfQueryIdByNameINTEL"), typeof(glGetPerfQueryIdByNameINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPerfQueryIdByNameINTEL'.");
            }
        }
        public static void glGetPerfQueryIdByNameINTEL(GLchar * @queryName, GLuint * @queryId) => glGetPerfQueryIdByNameINTELPtr(@queryName, @queryId);

        internal delegate void glGetPerfQueryInfoINTELFunc(GLuint @queryId, GLuint @queryNameLength, GLchar * @queryName, GLuint * @dataSize, GLuint * @noCounters, GLuint * @noInstances, GLuint * @capsMask);
        internal static glGetPerfQueryInfoINTELFunc glGetPerfQueryInfoINTELPtr;
        internal static void loadGetPerfQueryInfoINTEL()
        {
            try
            {
                glGetPerfQueryInfoINTELPtr = (glGetPerfQueryInfoINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPerfQueryInfoINTEL"), typeof(glGetPerfQueryInfoINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPerfQueryInfoINTEL'.");
            }
        }
        public static void glGetPerfQueryInfoINTEL(GLuint @queryId, GLuint @queryNameLength, GLchar * @queryName, GLuint * @dataSize, GLuint * @noCounters, GLuint * @noInstances, GLuint * @capsMask) => glGetPerfQueryInfoINTELPtr(@queryId, @queryNameLength, @queryName, @dataSize, @noCounters, @noInstances, @capsMask);
        #endregion
    }
}
