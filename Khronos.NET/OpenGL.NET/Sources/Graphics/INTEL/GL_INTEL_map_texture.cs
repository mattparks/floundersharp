using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_INTEL_map_texture
    {
        #region Interop
        static GL_INTEL_map_texture()
        {
            Console.WriteLine("Initalising GL_INTEL_map_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadSyncTextureINTEL();
            loadUnmapTexture2DINTEL();
            loadMapTexture2DINTEL();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_MEMORY_LAYOUT_INTEL = 0x83FF;
        public static UInt32 GL_LAYOUT_DEFAULT_INTEL = 0;
        public static UInt32 GL_LAYOUT_LINEAR_INTEL = 1;
        public static UInt32 GL_LAYOUT_LINEAR_CPU_CACHED_INTEL = 2;
        #endregion

        #region Commands
        internal delegate void glSyncTextureINTELFunc(GLuint @texture);
        internal static glSyncTextureINTELFunc glSyncTextureINTELPtr;
        internal static void loadSyncTextureINTEL()
        {
            try
            {
                glSyncTextureINTELPtr = (glSyncTextureINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSyncTextureINTEL"), typeof(glSyncTextureINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSyncTextureINTEL'.");
            }
        }
        public static void glSyncTextureINTEL(GLuint @texture) => glSyncTextureINTELPtr(@texture);

        internal delegate void glUnmapTexture2DINTELFunc(GLuint @texture, GLint @level);
        internal static glUnmapTexture2DINTELFunc glUnmapTexture2DINTELPtr;
        internal static void loadUnmapTexture2DINTEL()
        {
            try
            {
                glUnmapTexture2DINTELPtr = (glUnmapTexture2DINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUnmapTexture2DINTEL"), typeof(glUnmapTexture2DINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUnmapTexture2DINTEL'.");
            }
        }
        public static void glUnmapTexture2DINTEL(GLuint @texture, GLint @level) => glUnmapTexture2DINTELPtr(@texture, @level);

        internal delegate void * glMapTexture2DINTELFunc(GLuint @texture, GLint @level, GLbitfield @access, GLint * @stride, GLenum * @layout);
        internal static glMapTexture2DINTELFunc glMapTexture2DINTELPtr;
        internal static void loadMapTexture2DINTEL()
        {
            try
            {
                glMapTexture2DINTELPtr = (glMapTexture2DINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapTexture2DINTEL"), typeof(glMapTexture2DINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapTexture2DINTEL'.");
            }
        }
        public static void * glMapTexture2DINTEL(GLuint @texture, GLint @level, GLbitfield @access, GLint * @stride, GLenum * @layout) => glMapTexture2DINTELPtr(@texture, @level, @access, @stride, @layout);
        #endregion
    }
}
