using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_INTEL_framebuffer_CMAA
    {
        #region Interop
        static GL_INTEL_framebuffer_CMAA()
        {
            Console.WriteLine("Initalising GL_INTEL_framebuffer_CMAA interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadApplyFramebufferAttachmentCMAAINTEL();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glApplyFramebufferAttachmentCMAAINTELFunc();
        internal static glApplyFramebufferAttachmentCMAAINTELFunc glApplyFramebufferAttachmentCMAAINTELPtr;
        internal static void loadApplyFramebufferAttachmentCMAAINTEL()
        {
            try
            {
                glApplyFramebufferAttachmentCMAAINTELPtr = (glApplyFramebufferAttachmentCMAAINTELFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glApplyFramebufferAttachmentCMAAINTEL"), typeof(glApplyFramebufferAttachmentCMAAINTELFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glApplyFramebufferAttachmentCMAAINTEL'.");
            }
        }
        public static void glApplyFramebufferAttachmentCMAAINTEL() => glApplyFramebufferAttachmentCMAAINTELPtr();
        #endregion
    }
}
