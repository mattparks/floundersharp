using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL33
    {
        #region Interop
        static GL33()
        {
            Console.WriteLine("Initalising GL33 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindFragDataLocationIndexed();
            loadGetFragDataIndex();
            loadGenSamplers();
            loadDeleteSamplers();
            loadIsSampler();
            loadBindSampler();
            loadSamplerParameteri();
            loadSamplerParameteriv();
            loadSamplerParameterf();
            loadSamplerParameterfv();
            loadSamplerParameterIiv();
            loadSamplerParameterIuiv();
            loadGetSamplerParameteriv();
            loadGetSamplerParameterIiv();
            loadGetSamplerParameterfv();
            loadGetSamplerParameterIuiv();
            loadQueryCounter();
            loadGetQueryObjecti64v();
            loadGetQueryObjectui64v();
            loadVertexAttribDivisor();
            loadVertexAttribP1ui();
            loadVertexAttribP1uiv();
            loadVertexAttribP2ui();
            loadVertexAttribP2uiv();
            loadVertexAttribP3ui();
            loadVertexAttribP3uiv();
            loadVertexAttribP4ui();
            loadVertexAttribP4uiv();
            loadVertexP2ui();
            loadVertexP2uiv();
            loadVertexP3ui();
            loadVertexP3uiv();
            loadVertexP4ui();
            loadVertexP4uiv();
            loadTexCoordP1ui();
            loadTexCoordP1uiv();
            loadTexCoordP2ui();
            loadTexCoordP2uiv();
            loadTexCoordP3ui();
            loadTexCoordP3uiv();
            loadTexCoordP4ui();
            loadTexCoordP4uiv();
            loadMultiTexCoordP1ui();
            loadMultiTexCoordP1uiv();
            loadMultiTexCoordP2ui();
            loadMultiTexCoordP2uiv();
            loadMultiTexCoordP3ui();
            loadMultiTexCoordP3uiv();
            loadMultiTexCoordP4ui();
            loadMultiTexCoordP4uiv();
            loadNormalP3ui();
            loadNormalP3uiv();
            loadColorP3ui();
            loadColorP3uiv();
            loadColorP4ui();
            loadColorP4uiv();
            loadSecondaryColorP3ui();
            loadSecondaryColorP3uiv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_DIVISOR = 0x88FE;
        public static UInt32 GL_SRC1_COLOR = 0x88F9;
        public static UInt32 GL_ONE_MINUS_SRC1_COLOR = 0x88FA;
        public static UInt32 GL_ONE_MINUS_SRC1_ALPHA = 0x88FB;
        public static UInt32 GL_MAX_DUAL_SOURCE_DRAW_BUFFERS = 0x88FC;
        public static UInt32 GL_ANY_SAMPLES_PASSED = 0x8C2F;
        public static UInt32 GL_SAMPLER_BINDING = 0x8919;
        public static UInt32 GL_RGB10_A2UI = 0x906F;
        public static UInt32 GL_TEXTURE_SWIZZLE_R = 0x8E42;
        public static UInt32 GL_TEXTURE_SWIZZLE_G = 0x8E43;
        public static UInt32 GL_TEXTURE_SWIZZLE_B = 0x8E44;
        public static UInt32 GL_TEXTURE_SWIZZLE_A = 0x8E45;
        public static UInt32 GL_TEXTURE_SWIZZLE_RGBA = 0x8E46;
        public static UInt32 GL_TIME_ELAPSED = 0x88BF;
        public static UInt32 GL_TIMESTAMP = 0x8E28;
        public static UInt32 GL_INT_2_10_10_10_REV = 0x8D9F;
        #endregion

        #region Commands
        internal delegate void glBindFragDataLocationIndexedFunc(GLuint @program, GLuint @colorNumber, GLuint @index, const GLchar * @name);
        internal static glBindFragDataLocationIndexedFunc glBindFragDataLocationIndexedPtr;
        internal static void loadBindFragDataLocationIndexed()
        {
            try
            {
                glBindFragDataLocationIndexedPtr = (glBindFragDataLocationIndexedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindFragDataLocationIndexed"), typeof(glBindFragDataLocationIndexedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindFragDataLocationIndexed'.");
            }
        }
        public static void glBindFragDataLocationIndexed(GLuint @program, GLuint @colorNumber, GLuint @index, const GLchar * @name) => glBindFragDataLocationIndexedPtr(@program, @colorNumber, @index, @name);

        internal delegate GLint glGetFragDataIndexFunc(GLuint @program, const GLchar * @name);
        internal static glGetFragDataIndexFunc glGetFragDataIndexPtr;
        internal static void loadGetFragDataIndex()
        {
            try
            {
                glGetFragDataIndexPtr = (glGetFragDataIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFragDataIndex"), typeof(glGetFragDataIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFragDataIndex'.");
            }
        }
        public static GLint glGetFragDataIndex(GLuint @program, const GLchar * @name) => glGetFragDataIndexPtr(@program, @name);

        internal delegate void glGenSamplersFunc(GLsizei @count, GLuint * @samplers);
        internal static glGenSamplersFunc glGenSamplersPtr;
        internal static void loadGenSamplers()
        {
            try
            {
                glGenSamplersPtr = (glGenSamplersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenSamplers"), typeof(glGenSamplersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenSamplers'.");
            }
        }
        public static void glGenSamplers(GLsizei @count, GLuint * @samplers) => glGenSamplersPtr(@count, @samplers);

        internal delegate void glDeleteSamplersFunc(GLsizei @count, const GLuint * @samplers);
        internal static glDeleteSamplersFunc glDeleteSamplersPtr;
        internal static void loadDeleteSamplers()
        {
            try
            {
                glDeleteSamplersPtr = (glDeleteSamplersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteSamplers"), typeof(glDeleteSamplersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteSamplers'.");
            }
        }
        public static void glDeleteSamplers(GLsizei @count, const GLuint * @samplers) => glDeleteSamplersPtr(@count, @samplers);

        internal delegate GLboolean glIsSamplerFunc(GLuint @sampler);
        internal static glIsSamplerFunc glIsSamplerPtr;
        internal static void loadIsSampler()
        {
            try
            {
                glIsSamplerPtr = (glIsSamplerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsSampler"), typeof(glIsSamplerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsSampler'.");
            }
        }
        public static GLboolean glIsSampler(GLuint @sampler) => glIsSamplerPtr(@sampler);

        internal delegate void glBindSamplerFunc(GLuint @unit, GLuint @sampler);
        internal static glBindSamplerFunc glBindSamplerPtr;
        internal static void loadBindSampler()
        {
            try
            {
                glBindSamplerPtr = (glBindSamplerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindSampler"), typeof(glBindSamplerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindSampler'.");
            }
        }
        public static void glBindSampler(GLuint @unit, GLuint @sampler) => glBindSamplerPtr(@unit, @sampler);

        internal delegate void glSamplerParameteriFunc(GLuint @sampler, GLenum @pname, GLint @param);
        internal static glSamplerParameteriFunc glSamplerParameteriPtr;
        internal static void loadSamplerParameteri()
        {
            try
            {
                glSamplerParameteriPtr = (glSamplerParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameteri"), typeof(glSamplerParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameteri'.");
            }
        }
        public static void glSamplerParameteri(GLuint @sampler, GLenum @pname, GLint @param) => glSamplerParameteriPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterivFunc(GLuint @sampler, GLenum @pname, const GLint * @param);
        internal static glSamplerParameterivFunc glSamplerParameterivPtr;
        internal static void loadSamplerParameteriv()
        {
            try
            {
                glSamplerParameterivPtr = (glSamplerParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameteriv"), typeof(glSamplerParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameteriv'.");
            }
        }
        public static void glSamplerParameteriv(GLuint @sampler, GLenum @pname, const GLint * @param) => glSamplerParameterivPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterfFunc(GLuint @sampler, GLenum @pname, GLfloat @param);
        internal static glSamplerParameterfFunc glSamplerParameterfPtr;
        internal static void loadSamplerParameterf()
        {
            try
            {
                glSamplerParameterfPtr = (glSamplerParameterfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterf"), typeof(glSamplerParameterfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterf'.");
            }
        }
        public static void glSamplerParameterf(GLuint @sampler, GLenum @pname, GLfloat @param) => glSamplerParameterfPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterfvFunc(GLuint @sampler, GLenum @pname, const GLfloat * @param);
        internal static glSamplerParameterfvFunc glSamplerParameterfvPtr;
        internal static void loadSamplerParameterfv()
        {
            try
            {
                glSamplerParameterfvPtr = (glSamplerParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterfv"), typeof(glSamplerParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterfv'.");
            }
        }
        public static void glSamplerParameterfv(GLuint @sampler, GLenum @pname, const GLfloat * @param) => glSamplerParameterfvPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterIivFunc(GLuint @sampler, GLenum @pname, const GLint * @param);
        internal static glSamplerParameterIivFunc glSamplerParameterIivPtr;
        internal static void loadSamplerParameterIiv()
        {
            try
            {
                glSamplerParameterIivPtr = (glSamplerParameterIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterIiv"), typeof(glSamplerParameterIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterIiv'.");
            }
        }
        public static void glSamplerParameterIiv(GLuint @sampler, GLenum @pname, const GLint * @param) => glSamplerParameterIivPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterIuivFunc(GLuint @sampler, GLenum @pname, const GLuint * @param);
        internal static glSamplerParameterIuivFunc glSamplerParameterIuivPtr;
        internal static void loadSamplerParameterIuiv()
        {
            try
            {
                glSamplerParameterIuivPtr = (glSamplerParameterIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterIuiv"), typeof(glSamplerParameterIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterIuiv'.");
            }
        }
        public static void glSamplerParameterIuiv(GLuint @sampler, GLenum @pname, const GLuint * @param) => glSamplerParameterIuivPtr(@sampler, @pname, @param);

        internal delegate void glGetSamplerParameterivFunc(GLuint @sampler, GLenum @pname, GLint * @params);
        internal static glGetSamplerParameterivFunc glGetSamplerParameterivPtr;
        internal static void loadGetSamplerParameteriv()
        {
            try
            {
                glGetSamplerParameterivPtr = (glGetSamplerParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameteriv"), typeof(glGetSamplerParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameteriv'.");
            }
        }
        public static void glGetSamplerParameteriv(GLuint @sampler, GLenum @pname, GLint * @params) => glGetSamplerParameterivPtr(@sampler, @pname, @params);

        internal delegate void glGetSamplerParameterIivFunc(GLuint @sampler, GLenum @pname, GLint * @params);
        internal static glGetSamplerParameterIivFunc glGetSamplerParameterIivPtr;
        internal static void loadGetSamplerParameterIiv()
        {
            try
            {
                glGetSamplerParameterIivPtr = (glGetSamplerParameterIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterIiv"), typeof(glGetSamplerParameterIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterIiv'.");
            }
        }
        public static void glGetSamplerParameterIiv(GLuint @sampler, GLenum @pname, GLint * @params) => glGetSamplerParameterIivPtr(@sampler, @pname, @params);

        internal delegate void glGetSamplerParameterfvFunc(GLuint @sampler, GLenum @pname, GLfloat * @params);
        internal static glGetSamplerParameterfvFunc glGetSamplerParameterfvPtr;
        internal static void loadGetSamplerParameterfv()
        {
            try
            {
                glGetSamplerParameterfvPtr = (glGetSamplerParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterfv"), typeof(glGetSamplerParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterfv'.");
            }
        }
        public static void glGetSamplerParameterfv(GLuint @sampler, GLenum @pname, GLfloat * @params) => glGetSamplerParameterfvPtr(@sampler, @pname, @params);

        internal delegate void glGetSamplerParameterIuivFunc(GLuint @sampler, GLenum @pname, GLuint * @params);
        internal static glGetSamplerParameterIuivFunc glGetSamplerParameterIuivPtr;
        internal static void loadGetSamplerParameterIuiv()
        {
            try
            {
                glGetSamplerParameterIuivPtr = (glGetSamplerParameterIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterIuiv"), typeof(glGetSamplerParameterIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterIuiv'.");
            }
        }
        public static void glGetSamplerParameterIuiv(GLuint @sampler, GLenum @pname, GLuint * @params) => glGetSamplerParameterIuivPtr(@sampler, @pname, @params);

        internal delegate void glQueryCounterFunc(GLuint @id, GLenum @target);
        internal static glQueryCounterFunc glQueryCounterPtr;
        internal static void loadQueryCounter()
        {
            try
            {
                glQueryCounterPtr = (glQueryCounterFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glQueryCounter"), typeof(glQueryCounterFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glQueryCounter'.");
            }
        }
        public static void glQueryCounter(GLuint @id, GLenum @target) => glQueryCounterPtr(@id, @target);

        internal delegate void glGetQueryObjecti64vFunc(GLuint @id, GLenum @pname, GLint64 * @params);
        internal static glGetQueryObjecti64vFunc glGetQueryObjecti64vPtr;
        internal static void loadGetQueryObjecti64v()
        {
            try
            {
                glGetQueryObjecti64vPtr = (glGetQueryObjecti64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjecti64v"), typeof(glGetQueryObjecti64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjecti64v'.");
            }
        }
        public static void glGetQueryObjecti64v(GLuint @id, GLenum @pname, GLint64 * @params) => glGetQueryObjecti64vPtr(@id, @pname, @params);

        internal delegate void glGetQueryObjectui64vFunc(GLuint @id, GLenum @pname, GLuint64 * @params);
        internal static glGetQueryObjectui64vFunc glGetQueryObjectui64vPtr;
        internal static void loadGetQueryObjectui64v()
        {
            try
            {
                glGetQueryObjectui64vPtr = (glGetQueryObjectui64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjectui64v"), typeof(glGetQueryObjectui64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjectui64v'.");
            }
        }
        public static void glGetQueryObjectui64v(GLuint @id, GLenum @pname, GLuint64 * @params) => glGetQueryObjectui64vPtr(@id, @pname, @params);

        internal delegate void glVertexAttribDivisorFunc(GLuint @index, GLuint @divisor);
        internal static glVertexAttribDivisorFunc glVertexAttribDivisorPtr;
        internal static void loadVertexAttribDivisor()
        {
            try
            {
                glVertexAttribDivisorPtr = (glVertexAttribDivisorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribDivisor"), typeof(glVertexAttribDivisorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribDivisor'.");
            }
        }
        public static void glVertexAttribDivisor(GLuint @index, GLuint @divisor) => glVertexAttribDivisorPtr(@index, @divisor);

        internal delegate void glVertexAttribP1uiFunc(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value);
        internal static glVertexAttribP1uiFunc glVertexAttribP1uiPtr;
        internal static void loadVertexAttribP1ui()
        {
            try
            {
                glVertexAttribP1uiPtr = (glVertexAttribP1uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP1ui"), typeof(glVertexAttribP1uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP1ui'.");
            }
        }
        public static void glVertexAttribP1ui(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value) => glVertexAttribP1uiPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP1uivFunc(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value);
        internal static glVertexAttribP1uivFunc glVertexAttribP1uivPtr;
        internal static void loadVertexAttribP1uiv()
        {
            try
            {
                glVertexAttribP1uivPtr = (glVertexAttribP1uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP1uiv"), typeof(glVertexAttribP1uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP1uiv'.");
            }
        }
        public static void glVertexAttribP1uiv(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value) => glVertexAttribP1uivPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP2uiFunc(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value);
        internal static glVertexAttribP2uiFunc glVertexAttribP2uiPtr;
        internal static void loadVertexAttribP2ui()
        {
            try
            {
                glVertexAttribP2uiPtr = (glVertexAttribP2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP2ui"), typeof(glVertexAttribP2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP2ui'.");
            }
        }
        public static void glVertexAttribP2ui(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value) => glVertexAttribP2uiPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP2uivFunc(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value);
        internal static glVertexAttribP2uivFunc glVertexAttribP2uivPtr;
        internal static void loadVertexAttribP2uiv()
        {
            try
            {
                glVertexAttribP2uivPtr = (glVertexAttribP2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP2uiv"), typeof(glVertexAttribP2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP2uiv'.");
            }
        }
        public static void glVertexAttribP2uiv(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value) => glVertexAttribP2uivPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP3uiFunc(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value);
        internal static glVertexAttribP3uiFunc glVertexAttribP3uiPtr;
        internal static void loadVertexAttribP3ui()
        {
            try
            {
                glVertexAttribP3uiPtr = (glVertexAttribP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP3ui"), typeof(glVertexAttribP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP3ui'.");
            }
        }
        public static void glVertexAttribP3ui(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value) => glVertexAttribP3uiPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP3uivFunc(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value);
        internal static glVertexAttribP3uivFunc glVertexAttribP3uivPtr;
        internal static void loadVertexAttribP3uiv()
        {
            try
            {
                glVertexAttribP3uivPtr = (glVertexAttribP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP3uiv"), typeof(glVertexAttribP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP3uiv'.");
            }
        }
        public static void glVertexAttribP3uiv(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value) => glVertexAttribP3uivPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP4uiFunc(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value);
        internal static glVertexAttribP4uiFunc glVertexAttribP4uiPtr;
        internal static void loadVertexAttribP4ui()
        {
            try
            {
                glVertexAttribP4uiPtr = (glVertexAttribP4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP4ui"), typeof(glVertexAttribP4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP4ui'.");
            }
        }
        public static void glVertexAttribP4ui(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value) => glVertexAttribP4uiPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP4uivFunc(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value);
        internal static glVertexAttribP4uivFunc glVertexAttribP4uivPtr;
        internal static void loadVertexAttribP4uiv()
        {
            try
            {
                glVertexAttribP4uivPtr = (glVertexAttribP4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP4uiv"), typeof(glVertexAttribP4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP4uiv'.");
            }
        }
        public static void glVertexAttribP4uiv(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value) => glVertexAttribP4uivPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexP2uiFunc(GLenum @type, GLuint @value);
        internal static glVertexP2uiFunc glVertexP2uiPtr;
        internal static void loadVertexP2ui()
        {
            try
            {
                glVertexP2uiPtr = (glVertexP2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexP2ui"), typeof(glVertexP2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexP2ui'.");
            }
        }
        public static void glVertexP2ui(GLenum @type, GLuint @value) => glVertexP2uiPtr(@type, @value);

        internal delegate void glVertexP2uivFunc(GLenum @type, const GLuint * @value);
        internal static glVertexP2uivFunc glVertexP2uivPtr;
        internal static void loadVertexP2uiv()
        {
            try
            {
                glVertexP2uivPtr = (glVertexP2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexP2uiv"), typeof(glVertexP2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexP2uiv'.");
            }
        }
        public static void glVertexP2uiv(GLenum @type, const GLuint * @value) => glVertexP2uivPtr(@type, @value);

        internal delegate void glVertexP3uiFunc(GLenum @type, GLuint @value);
        internal static glVertexP3uiFunc glVertexP3uiPtr;
        internal static void loadVertexP3ui()
        {
            try
            {
                glVertexP3uiPtr = (glVertexP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexP3ui"), typeof(glVertexP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexP3ui'.");
            }
        }
        public static void glVertexP3ui(GLenum @type, GLuint @value) => glVertexP3uiPtr(@type, @value);

        internal delegate void glVertexP3uivFunc(GLenum @type, const GLuint * @value);
        internal static glVertexP3uivFunc glVertexP3uivPtr;
        internal static void loadVertexP3uiv()
        {
            try
            {
                glVertexP3uivPtr = (glVertexP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexP3uiv"), typeof(glVertexP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexP3uiv'.");
            }
        }
        public static void glVertexP3uiv(GLenum @type, const GLuint * @value) => glVertexP3uivPtr(@type, @value);

        internal delegate void glVertexP4uiFunc(GLenum @type, GLuint @value);
        internal static glVertexP4uiFunc glVertexP4uiPtr;
        internal static void loadVertexP4ui()
        {
            try
            {
                glVertexP4uiPtr = (glVertexP4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexP4ui"), typeof(glVertexP4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexP4ui'.");
            }
        }
        public static void glVertexP4ui(GLenum @type, GLuint @value) => glVertexP4uiPtr(@type, @value);

        internal delegate void glVertexP4uivFunc(GLenum @type, const GLuint * @value);
        internal static glVertexP4uivFunc glVertexP4uivPtr;
        internal static void loadVertexP4uiv()
        {
            try
            {
                glVertexP4uivPtr = (glVertexP4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexP4uiv"), typeof(glVertexP4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexP4uiv'.");
            }
        }
        public static void glVertexP4uiv(GLenum @type, const GLuint * @value) => glVertexP4uivPtr(@type, @value);

        internal delegate void glTexCoordP1uiFunc(GLenum @type, GLuint @coords);
        internal static glTexCoordP1uiFunc glTexCoordP1uiPtr;
        internal static void loadTexCoordP1ui()
        {
            try
            {
                glTexCoordP1uiPtr = (glTexCoordP1uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP1ui"), typeof(glTexCoordP1uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP1ui'.");
            }
        }
        public static void glTexCoordP1ui(GLenum @type, GLuint @coords) => glTexCoordP1uiPtr(@type, @coords);

        internal delegate void glTexCoordP1uivFunc(GLenum @type, const GLuint * @coords);
        internal static glTexCoordP1uivFunc glTexCoordP1uivPtr;
        internal static void loadTexCoordP1uiv()
        {
            try
            {
                glTexCoordP1uivPtr = (glTexCoordP1uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP1uiv"), typeof(glTexCoordP1uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP1uiv'.");
            }
        }
        public static void glTexCoordP1uiv(GLenum @type, const GLuint * @coords) => glTexCoordP1uivPtr(@type, @coords);

        internal delegate void glTexCoordP2uiFunc(GLenum @type, GLuint @coords);
        internal static glTexCoordP2uiFunc glTexCoordP2uiPtr;
        internal static void loadTexCoordP2ui()
        {
            try
            {
                glTexCoordP2uiPtr = (glTexCoordP2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP2ui"), typeof(glTexCoordP2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP2ui'.");
            }
        }
        public static void glTexCoordP2ui(GLenum @type, GLuint @coords) => glTexCoordP2uiPtr(@type, @coords);

        internal delegate void glTexCoordP2uivFunc(GLenum @type, const GLuint * @coords);
        internal static glTexCoordP2uivFunc glTexCoordP2uivPtr;
        internal static void loadTexCoordP2uiv()
        {
            try
            {
                glTexCoordP2uivPtr = (glTexCoordP2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP2uiv"), typeof(glTexCoordP2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP2uiv'.");
            }
        }
        public static void glTexCoordP2uiv(GLenum @type, const GLuint * @coords) => glTexCoordP2uivPtr(@type, @coords);

        internal delegate void glTexCoordP3uiFunc(GLenum @type, GLuint @coords);
        internal static glTexCoordP3uiFunc glTexCoordP3uiPtr;
        internal static void loadTexCoordP3ui()
        {
            try
            {
                glTexCoordP3uiPtr = (glTexCoordP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP3ui"), typeof(glTexCoordP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP3ui'.");
            }
        }
        public static void glTexCoordP3ui(GLenum @type, GLuint @coords) => glTexCoordP3uiPtr(@type, @coords);

        internal delegate void glTexCoordP3uivFunc(GLenum @type, const GLuint * @coords);
        internal static glTexCoordP3uivFunc glTexCoordP3uivPtr;
        internal static void loadTexCoordP3uiv()
        {
            try
            {
                glTexCoordP3uivPtr = (glTexCoordP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP3uiv"), typeof(glTexCoordP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP3uiv'.");
            }
        }
        public static void glTexCoordP3uiv(GLenum @type, const GLuint * @coords) => glTexCoordP3uivPtr(@type, @coords);

        internal delegate void glTexCoordP4uiFunc(GLenum @type, GLuint @coords);
        internal static glTexCoordP4uiFunc glTexCoordP4uiPtr;
        internal static void loadTexCoordP4ui()
        {
            try
            {
                glTexCoordP4uiPtr = (glTexCoordP4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP4ui"), typeof(glTexCoordP4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP4ui'.");
            }
        }
        public static void glTexCoordP4ui(GLenum @type, GLuint @coords) => glTexCoordP4uiPtr(@type, @coords);

        internal delegate void glTexCoordP4uivFunc(GLenum @type, const GLuint * @coords);
        internal static glTexCoordP4uivFunc glTexCoordP4uivPtr;
        internal static void loadTexCoordP4uiv()
        {
            try
            {
                glTexCoordP4uivPtr = (glTexCoordP4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP4uiv"), typeof(glTexCoordP4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP4uiv'.");
            }
        }
        public static void glTexCoordP4uiv(GLenum @type, const GLuint * @coords) => glTexCoordP4uivPtr(@type, @coords);

        internal delegate void glMultiTexCoordP1uiFunc(GLenum @texture, GLenum @type, GLuint @coords);
        internal static glMultiTexCoordP1uiFunc glMultiTexCoordP1uiPtr;
        internal static void loadMultiTexCoordP1ui()
        {
            try
            {
                glMultiTexCoordP1uiPtr = (glMultiTexCoordP1uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP1ui"), typeof(glMultiTexCoordP1uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP1ui'.");
            }
        }
        public static void glMultiTexCoordP1ui(GLenum @texture, GLenum @type, GLuint @coords) => glMultiTexCoordP1uiPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP1uivFunc(GLenum @texture, GLenum @type, const GLuint * @coords);
        internal static glMultiTexCoordP1uivFunc glMultiTexCoordP1uivPtr;
        internal static void loadMultiTexCoordP1uiv()
        {
            try
            {
                glMultiTexCoordP1uivPtr = (glMultiTexCoordP1uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP1uiv"), typeof(glMultiTexCoordP1uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP1uiv'.");
            }
        }
        public static void glMultiTexCoordP1uiv(GLenum @texture, GLenum @type, const GLuint * @coords) => glMultiTexCoordP1uivPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP2uiFunc(GLenum @texture, GLenum @type, GLuint @coords);
        internal static glMultiTexCoordP2uiFunc glMultiTexCoordP2uiPtr;
        internal static void loadMultiTexCoordP2ui()
        {
            try
            {
                glMultiTexCoordP2uiPtr = (glMultiTexCoordP2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP2ui"), typeof(glMultiTexCoordP2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP2ui'.");
            }
        }
        public static void glMultiTexCoordP2ui(GLenum @texture, GLenum @type, GLuint @coords) => glMultiTexCoordP2uiPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP2uivFunc(GLenum @texture, GLenum @type, const GLuint * @coords);
        internal static glMultiTexCoordP2uivFunc glMultiTexCoordP2uivPtr;
        internal static void loadMultiTexCoordP2uiv()
        {
            try
            {
                glMultiTexCoordP2uivPtr = (glMultiTexCoordP2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP2uiv"), typeof(glMultiTexCoordP2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP2uiv'.");
            }
        }
        public static void glMultiTexCoordP2uiv(GLenum @texture, GLenum @type, const GLuint * @coords) => glMultiTexCoordP2uivPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP3uiFunc(GLenum @texture, GLenum @type, GLuint @coords);
        internal static glMultiTexCoordP3uiFunc glMultiTexCoordP3uiPtr;
        internal static void loadMultiTexCoordP3ui()
        {
            try
            {
                glMultiTexCoordP3uiPtr = (glMultiTexCoordP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP3ui"), typeof(glMultiTexCoordP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP3ui'.");
            }
        }
        public static void glMultiTexCoordP3ui(GLenum @texture, GLenum @type, GLuint @coords) => glMultiTexCoordP3uiPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP3uivFunc(GLenum @texture, GLenum @type, const GLuint * @coords);
        internal static glMultiTexCoordP3uivFunc glMultiTexCoordP3uivPtr;
        internal static void loadMultiTexCoordP3uiv()
        {
            try
            {
                glMultiTexCoordP3uivPtr = (glMultiTexCoordP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP3uiv"), typeof(glMultiTexCoordP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP3uiv'.");
            }
        }
        public static void glMultiTexCoordP3uiv(GLenum @texture, GLenum @type, const GLuint * @coords) => glMultiTexCoordP3uivPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP4uiFunc(GLenum @texture, GLenum @type, GLuint @coords);
        internal static glMultiTexCoordP4uiFunc glMultiTexCoordP4uiPtr;
        internal static void loadMultiTexCoordP4ui()
        {
            try
            {
                glMultiTexCoordP4uiPtr = (glMultiTexCoordP4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP4ui"), typeof(glMultiTexCoordP4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP4ui'.");
            }
        }
        public static void glMultiTexCoordP4ui(GLenum @texture, GLenum @type, GLuint @coords) => glMultiTexCoordP4uiPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP4uivFunc(GLenum @texture, GLenum @type, const GLuint * @coords);
        internal static glMultiTexCoordP4uivFunc glMultiTexCoordP4uivPtr;
        internal static void loadMultiTexCoordP4uiv()
        {
            try
            {
                glMultiTexCoordP4uivPtr = (glMultiTexCoordP4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP4uiv"), typeof(glMultiTexCoordP4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP4uiv'.");
            }
        }
        public static void glMultiTexCoordP4uiv(GLenum @texture, GLenum @type, const GLuint * @coords) => glMultiTexCoordP4uivPtr(@texture, @type, @coords);

        internal delegate void glNormalP3uiFunc(GLenum @type, GLuint @coords);
        internal static glNormalP3uiFunc glNormalP3uiPtr;
        internal static void loadNormalP3ui()
        {
            try
            {
                glNormalP3uiPtr = (glNormalP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalP3ui"), typeof(glNormalP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalP3ui'.");
            }
        }
        public static void glNormalP3ui(GLenum @type, GLuint @coords) => glNormalP3uiPtr(@type, @coords);

        internal delegate void glNormalP3uivFunc(GLenum @type, const GLuint * @coords);
        internal static glNormalP3uivFunc glNormalP3uivPtr;
        internal static void loadNormalP3uiv()
        {
            try
            {
                glNormalP3uivPtr = (glNormalP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalP3uiv"), typeof(glNormalP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalP3uiv'.");
            }
        }
        public static void glNormalP3uiv(GLenum @type, const GLuint * @coords) => glNormalP3uivPtr(@type, @coords);

        internal delegate void glColorP3uiFunc(GLenum @type, GLuint @color);
        internal static glColorP3uiFunc glColorP3uiPtr;
        internal static void loadColorP3ui()
        {
            try
            {
                glColorP3uiPtr = (glColorP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorP3ui"), typeof(glColorP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorP3ui'.");
            }
        }
        public static void glColorP3ui(GLenum @type, GLuint @color) => glColorP3uiPtr(@type, @color);

        internal delegate void glColorP3uivFunc(GLenum @type, const GLuint * @color);
        internal static glColorP3uivFunc glColorP3uivPtr;
        internal static void loadColorP3uiv()
        {
            try
            {
                glColorP3uivPtr = (glColorP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorP3uiv"), typeof(glColorP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorP3uiv'.");
            }
        }
        public static void glColorP3uiv(GLenum @type, const GLuint * @color) => glColorP3uivPtr(@type, @color);

        internal delegate void glColorP4uiFunc(GLenum @type, GLuint @color);
        internal static glColorP4uiFunc glColorP4uiPtr;
        internal static void loadColorP4ui()
        {
            try
            {
                glColorP4uiPtr = (glColorP4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorP4ui"), typeof(glColorP4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorP4ui'.");
            }
        }
        public static void glColorP4ui(GLenum @type, GLuint @color) => glColorP4uiPtr(@type, @color);

        internal delegate void glColorP4uivFunc(GLenum @type, const GLuint * @color);
        internal static glColorP4uivFunc glColorP4uivPtr;
        internal static void loadColorP4uiv()
        {
            try
            {
                glColorP4uivPtr = (glColorP4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorP4uiv"), typeof(glColorP4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorP4uiv'.");
            }
        }
        public static void glColorP4uiv(GLenum @type, const GLuint * @color) => glColorP4uivPtr(@type, @color);

        internal delegate void glSecondaryColorP3uiFunc(GLenum @type, GLuint @color);
        internal static glSecondaryColorP3uiFunc glSecondaryColorP3uiPtr;
        internal static void loadSecondaryColorP3ui()
        {
            try
            {
                glSecondaryColorP3uiPtr = (glSecondaryColorP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColorP3ui"), typeof(glSecondaryColorP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColorP3ui'.");
            }
        }
        public static void glSecondaryColorP3ui(GLenum @type, GLuint @color) => glSecondaryColorP3uiPtr(@type, @color);

        internal delegate void glSecondaryColorP3uivFunc(GLenum @type, const GLuint * @color);
        internal static glSecondaryColorP3uivFunc glSecondaryColorP3uivPtr;
        internal static void loadSecondaryColorP3uiv()
        {
            try
            {
                glSecondaryColorP3uivPtr = (glSecondaryColorP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColorP3uiv"), typeof(glSecondaryColorP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColorP3uiv'.");
            }
        }
        public static void glSecondaryColorP3uiv(GLenum @type, const GLuint * @color) => glSecondaryColorP3uivPtr(@type, @color);
        #endregion
    }
}
