using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_MESAX_texture_stack
    {
        #region Interop
        static GL_MESAX_texture_stack()
        {
            Console.WriteLine("Initalising GL_MESAX_texture_stack interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_1D_STACK_MESAX = 0x8759;
        public static UInt32 GL_TEXTURE_2D_STACK_MESAX = 0x875A;
        public static UInt32 GL_PROXY_TEXTURE_1D_STACK_MESAX = 0x875B;
        public static UInt32 GL_PROXY_TEXTURE_2D_STACK_MESAX = 0x875C;
        public static UInt32 GL_TEXTURE_1D_STACK_BINDING_MESAX = 0x875D;
        public static UInt32 GL_TEXTURE_2D_STACK_BINDING_MESAX = 0x875E;
        #endregion

        #region Commands
        #endregion
    }
}
