using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_REND_screen_coordinates
    {
        #region Interop
        static GL_REND_screen_coordinates()
        {
            Console.WriteLine("Initalising GL_REND_screen_coordinates interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SCREEN_COORDINATES_REND = 0x8490;
        public static UInt32 GL_INVERTED_SCREEN_W_REND = 0x8491;
        #endregion

        #region Commands
        #endregion
    }
}
