using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GLES32
    {
        #region Interop
        static GLES32()
        {
            Console.WriteLine("Initalising GLES32 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendBarrier();
            loadCopyImageSubData();
            loadDebugMessageControl();
            loadDebugMessageInsert();
            loadDebugMessageCallback();
            loadGetDebugMessageLog();
            loadPushDebugGroup();
            loadPopDebugGroup();
            loadObjectLabel();
            loadGetObjectLabel();
            loadObjectPtrLabel();
            loadGetObjectPtrLabel();
            loadGetPointerv();
            loadEnablei();
            loadDisablei();
            loadBlendEquationi();
            loadBlendEquationSeparatei();
            loadBlendFunci();
            loadBlendFuncSeparatei();
            loadColorMaski();
            loadIsEnabledi();
            loadDrawElementsBaseVertex();
            loadDrawRangeElementsBaseVertex();
            loadDrawElementsInstancedBaseVertex();
            loadFramebufferTexture();
            loadPrimitiveBoundingBox();
            loadGetGraphicsResetStatus();
            loadReadnPixels();
            loadGetnUniformfv();
            loadGetnUniformiv();
            loadGetnUniformuiv();
            loadMinSampleShading();
            loadPatchParameteri();
            loadTexParameterIiv();
            loadTexParameterIuiv();
            loadGetTexParameterIiv();
            loadGetTexParameterIuiv();
            loadSamplerParameterIiv();
            loadSamplerParameterIuiv();
            loadGetSamplerParameterIiv();
            loadGetSamplerParameterIuiv();
            loadTexBuffer();
            loadTexBufferRange();
            loadTexStorage3DMultisample();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MULTISAMPLE_LINE_WIDTH_RANGE = 0x9381;
        public static UInt32 GL_MULTISAMPLE_LINE_WIDTH_GRANULARITY = 0x9382;
        public static UInt32 GL_MULTIPLY = 0x9294;
        public static UInt32 GL_SCREEN = 0x9295;
        public static UInt32 GL_OVERLAY = 0x9296;
        public static UInt32 GL_DARKEN = 0x9297;
        public static UInt32 GL_LIGHTEN = 0x9298;
        public static UInt32 GL_COLORDODGE = 0x9299;
        public static UInt32 GL_COLORBURN = 0x929A;
        public static UInt32 GL_HARDLIGHT = 0x929B;
        public static UInt32 GL_SOFTLIGHT = 0x929C;
        public static UInt32 GL_DIFFERENCE = 0x929E;
        public static UInt32 GL_EXCLUSION = 0x92A0;
        public static UInt32 GL_HSL_HUE = 0x92AD;
        public static UInt32 GL_HSL_SATURATION = 0x92AE;
        public static UInt32 GL_HSL_COLOR = 0x92AF;
        public static UInt32 GL_HSL_LUMINOSITY = 0x92B0;
        public static UInt32 GL_DEBUG_OUTPUT_SYNCHRONOUS = 0x8242;
        public static UInt32 GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH = 0x8243;
        public static UInt32 GL_DEBUG_CALLBACK_FUNCTION = 0x8244;
        public static UInt32 GL_DEBUG_CALLBACK_USER_PARAM = 0x8245;
        public static UInt32 GL_DEBUG_SOURCE_API = 0x8246;
        public static UInt32 GL_DEBUG_SOURCE_WINDOW_SYSTEM = 0x8247;
        public static UInt32 GL_DEBUG_SOURCE_SHADER_COMPILER = 0x8248;
        public static UInt32 GL_DEBUG_SOURCE_THIRD_PARTY = 0x8249;
        public static UInt32 GL_DEBUG_SOURCE_APPLICATION = 0x824A;
        public static UInt32 GL_DEBUG_SOURCE_OTHER = 0x824B;
        public static UInt32 GL_DEBUG_TYPE_ERROR = 0x824C;
        public static UInt32 GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR = 0x824D;
        public static UInt32 GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR = 0x824E;
        public static UInt32 GL_DEBUG_TYPE_PORTABILITY = 0x824F;
        public static UInt32 GL_DEBUG_TYPE_PERFORMANCE = 0x8250;
        public static UInt32 GL_DEBUG_TYPE_OTHER = 0x8251;
        public static UInt32 GL_DEBUG_TYPE_MARKER = 0x8268;
        public static UInt32 GL_DEBUG_TYPE_PUSH_GROUP = 0x8269;
        public static UInt32 GL_DEBUG_TYPE_POP_GROUP = 0x826A;
        public static UInt32 GL_DEBUG_SEVERITY_NOTIFICATION = 0x826B;
        public static UInt32 GL_MAX_DEBUG_GROUP_STACK_DEPTH = 0x826C;
        public static UInt32 GL_DEBUG_GROUP_STACK_DEPTH = 0x826D;
        public static UInt32 GL_BUFFER = 0x82E0;
        public static UInt32 GL_SHADER = 0x82E1;
        public static UInt32 GL_PROGRAM = 0x82E2;
        public static UInt32 GL_VERTEX_ARRAY = 0x8074;
        public static UInt32 GL_QUERY = 0x82E3;
        public static UInt32 GL_PROGRAM_PIPELINE = 0x82E4;
        public static UInt32 GL_SAMPLER = 0x82E6;
        public static UInt32 GL_MAX_LABEL_LENGTH = 0x82E8;
        public static UInt32 GL_MAX_DEBUG_MESSAGE_LENGTH = 0x9143;
        public static UInt32 GL_MAX_DEBUG_LOGGED_MESSAGES = 0x9144;
        public static UInt32 GL_DEBUG_LOGGED_MESSAGES = 0x9145;
        public static UInt32 GL_DEBUG_SEVERITY_HIGH = 0x9146;
        public static UInt32 GL_DEBUG_SEVERITY_MEDIUM = 0x9147;
        public static UInt32 GL_DEBUG_SEVERITY_LOW = 0x9148;
        public static UInt32 GL_DEBUG_OUTPUT = 0x92E0;
        public static UInt32 GL_CONTEXT_FLAG_DEBUG_BIT = 0x00000002;
        public static UInt32 GL_STACK_OVERFLOW = 0x0503;
        public static UInt32 GL_STACK_UNDERFLOW = 0x0504;
        public static UInt32 GL_GEOMETRY_SHADER = 0x8DD9;
        public static UInt32 GL_GEOMETRY_SHADER_BIT = 0x00000004;
        public static UInt32 GL_GEOMETRY_VERTICES_OUT = 0x8916;
        public static UInt32 GL_GEOMETRY_INPUT_TYPE = 0x8917;
        public static UInt32 GL_GEOMETRY_OUTPUT_TYPE = 0x8918;
        public static UInt32 GL_GEOMETRY_SHADER_INVOCATIONS = 0x887F;
        public static UInt32 GL_LAYER_PROVOKING_VERTEX = 0x825E;
        public static UInt32 GL_LINES_ADJACENCY = 0x000A;
        public static UInt32 GL_LINE_STRIP_ADJACENCY = 0x000B;
        public static UInt32 GL_TRIANGLES_ADJACENCY = 0x000C;
        public static UInt32 GL_TRIANGLE_STRIP_ADJACENCY = 0x000D;
        public static UInt32 GL_MAX_GEOMETRY_UNIFORM_COMPONENTS = 0x8DDF;
        public static UInt32 GL_MAX_GEOMETRY_UNIFORM_BLOCKS = 0x8A2C;
        public static UInt32 GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS = 0x8A32;
        public static UInt32 GL_MAX_GEOMETRY_INPUT_COMPONENTS = 0x9123;
        public static UInt32 GL_MAX_GEOMETRY_OUTPUT_COMPONENTS = 0x9124;
        public static UInt32 GL_MAX_GEOMETRY_OUTPUT_VERTICES = 0x8DE0;
        public static UInt32 GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS = 0x8DE1;
        public static UInt32 GL_MAX_GEOMETRY_SHADER_INVOCATIONS = 0x8E5A;
        public static UInt32 GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS = 0x8C29;
        public static UInt32 GL_MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS = 0x92CF;
        public static UInt32 GL_MAX_GEOMETRY_ATOMIC_COUNTERS = 0x92D5;
        public static UInt32 GL_MAX_GEOMETRY_IMAGE_UNIFORMS = 0x90CD;
        public static UInt32 GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS = 0x90D7;
        public static UInt32 GL_FIRST_VERTEX_CONVENTION = 0x8E4D;
        public static UInt32 GL_LAST_VERTEX_CONVENTION = 0x8E4E;
        public static UInt32 GL_UNDEFINED_VERTEX = 0x8260;
        public static UInt32 GL_PRIMITIVES_GENERATED = 0x8C87;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_LAYERS = 0x9312;
        public static UInt32 GL_MAX_FRAMEBUFFER_LAYERS = 0x9317;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS = 0x8DA8;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_LAYERED = 0x8DA7;
        public static UInt32 GL_REFERENCED_BY_GEOMETRY_SHADER = 0x9309;
        public static UInt32 GL_PRIMITIVE_BOUNDING_BOX = 0x92BE;
        public static UInt32 GL_NO_ERROR = 0;
        public static UInt32 GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT = 0x00000004;
        public static UInt32 GL_CONTEXT_FLAGS = 0x821E;
        public static UInt32 GL_LOSE_CONTEXT_ON_RESET = 0x8252;
        public static UInt32 GL_GUILTY_CONTEXT_RESET = 0x8253;
        public static UInt32 GL_INNOCENT_CONTEXT_RESET = 0x8254;
        public static UInt32 GL_UNKNOWN_CONTEXT_RESET = 0x8255;
        public static UInt32 GL_RESET_NOTIFICATION_STRATEGY = 0x8256;
        public static UInt32 GL_NO_RESET_NOTIFICATION = 0x8261;
        public static UInt32 GL_CONTEXT_LOST = 0x0507;
        public static UInt32 GL_SAMPLE_SHADING = 0x8C36;
        public static UInt32 GL_MIN_SAMPLE_SHADING_VALUE = 0x8C37;
        public static UInt32 GL_MIN_FRAGMENT_INTERPOLATION_OFFSET = 0x8E5B;
        public static UInt32 GL_MAX_FRAGMENT_INTERPOLATION_OFFSET = 0x8E5C;
        public static UInt32 GL_FRAGMENT_INTERPOLATION_OFFSET_BITS = 0x8E5D;
        public static UInt32 GL_PATCHES = 0x000E;
        public static UInt32 GL_PATCH_VERTICES = 0x8E72;
        public static UInt32 GL_TESS_CONTROL_OUTPUT_VERTICES = 0x8E75;
        public static UInt32 GL_TESS_GEN_MODE = 0x8E76;
        public static UInt32 GL_TESS_GEN_SPACING = 0x8E77;
        public static UInt32 GL_TESS_GEN_VERTEX_ORDER = 0x8E78;
        public static UInt32 GL_TESS_GEN_POINT_MODE = 0x8E79;
        public static UInt32 GL_TRIANGLES = 0x0004;
        public static UInt32 GL_ISOLINES = 0x8E7A;
        public static UInt32 GL_QUADS = 0x0007;
        public static UInt32 GL_EQUAL = 0x0202;
        public static UInt32 GL_FRACTIONAL_ODD = 0x8E7B;
        public static UInt32 GL_FRACTIONAL_EVEN = 0x8E7C;
        public static UInt32 GL_CCW = 0x0901;
        public static UInt32 GL_CW = 0x0900;
        public static UInt32 GL_MAX_PATCH_VERTICES = 0x8E7D;
        public static UInt32 GL_MAX_TESS_GEN_LEVEL = 0x8E7E;
        public static UInt32 GL_MAX_TESS_CONTROL_UNIFORM_COMPONENTS = 0x8E7F;
        public static UInt32 GL_MAX_TESS_EVALUATION_UNIFORM_COMPONENTS = 0x8E80;
        public static UInt32 GL_MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS = 0x8E81;
        public static UInt32 GL_MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS = 0x8E82;
        public static UInt32 GL_MAX_TESS_CONTROL_OUTPUT_COMPONENTS = 0x8E83;
        public static UInt32 GL_MAX_TESS_PATCH_COMPONENTS = 0x8E84;
        public static UInt32 GL_MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS = 0x8E85;
        public static UInt32 GL_MAX_TESS_EVALUATION_OUTPUT_COMPONENTS = 0x8E86;
        public static UInt32 GL_MAX_TESS_CONTROL_UNIFORM_BLOCKS = 0x8E89;
        public static UInt32 GL_MAX_TESS_EVALUATION_UNIFORM_BLOCKS = 0x8E8A;
        public static UInt32 GL_MAX_TESS_CONTROL_INPUT_COMPONENTS = 0x886C;
        public static UInt32 GL_MAX_TESS_EVALUATION_INPUT_COMPONENTS = 0x886D;
        public static UInt32 GL_MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS = 0x8E1E;
        public static UInt32 GL_MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS = 0x8E1F;
        public static UInt32 GL_MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS = 0x92CD;
        public static UInt32 GL_MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS = 0x92CE;
        public static UInt32 GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS = 0x92D3;
        public static UInt32 GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS = 0x92D4;
        public static UInt32 GL_MAX_TESS_CONTROL_IMAGE_UNIFORMS = 0x90CB;
        public static UInt32 GL_MAX_TESS_EVALUATION_IMAGE_UNIFORMS = 0x90CC;
        public static UInt32 GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS = 0x90D8;
        public static UInt32 GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS = 0x90D9;
        public static UInt32 GL_PRIMITIVE_RESTART_FOR_PATCHES_SUPPORTED = 0x8221;
        public static UInt32 GL_IS_PER_PATCH = 0x92E7;
        public static UInt32 GL_REFERENCED_BY_TESS_CONTROL_SHADER = 0x9307;
        public static UInt32 GL_REFERENCED_BY_TESS_EVALUATION_SHADER = 0x9308;
        public static UInt32 GL_TESS_CONTROL_SHADER = 0x8E88;
        public static UInt32 GL_TESS_EVALUATION_SHADER = 0x8E87;
        public static UInt32 GL_TESS_CONTROL_SHADER_BIT = 0x00000008;
        public static UInt32 GL_TESS_EVALUATION_SHADER_BIT = 0x00000010;
        public static UInt32 GL_TEXTURE_BORDER_COLOR = 0x1004;
        public static UInt32 GL_CLAMP_TO_BORDER = 0x812D;
        public static UInt32 GL_TEXTURE_BUFFER = 0x8C2A;
        public static UInt32 GL_TEXTURE_BUFFER_BINDING = 0x8C2A;
        public static UInt32 GL_MAX_TEXTURE_BUFFER_SIZE = 0x8C2B;
        public static UInt32 GL_TEXTURE_BINDING_BUFFER = 0x8C2C;
        public static UInt32 GL_TEXTURE_BUFFER_DATA_STORE_BINDING = 0x8C2D;
        public static UInt32 GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT = 0x919F;
        public static UInt32 GL_SAMPLER_BUFFER = 0x8DC2;
        public static UInt32 GL_INT_SAMPLER_BUFFER = 0x8DD0;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_BUFFER = 0x8DD8;
        public static UInt32 GL_IMAGE_BUFFER = 0x9051;
        public static UInt32 GL_INT_IMAGE_BUFFER = 0x905C;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_BUFFER = 0x9067;
        public static UInt32 GL_TEXTURE_BUFFER_OFFSET = 0x919D;
        public static UInt32 GL_TEXTURE_BUFFER_SIZE = 0x919E;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_4x4 = 0x93B0;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_5x4 = 0x93B1;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_5x5 = 0x93B2;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_6x5 = 0x93B3;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_6x6 = 0x93B4;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_8x5 = 0x93B5;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_8x6 = 0x93B6;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_8x8 = 0x93B7;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_10x5 = 0x93B8;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_10x6 = 0x93B9;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_10x8 = 0x93BA;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_10x10 = 0x93BB;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_12x10 = 0x93BC;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_12x12 = 0x93BD;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_4x4 = 0x93D0;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x4 = 0x93D1;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x5 = 0x93D2;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x5 = 0x93D3;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x6 = 0x93D4;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x5 = 0x93D5;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x6 = 0x93D6;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x8 = 0x93D7;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x5 = 0x93D8;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x6 = 0x93D9;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x8 = 0x93DA;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x10 = 0x93DB;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x10 = 0x93DC;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x12 = 0x93DD;
        public static UInt32 GL_TEXTURE_CUBE_MAP_ARRAY = 0x9009;
        public static UInt32 GL_TEXTURE_BINDING_CUBE_MAP_ARRAY = 0x900A;
        public static UInt32 GL_SAMPLER_CUBE_MAP_ARRAY = 0x900C;
        public static UInt32 GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW = 0x900D;
        public static UInt32 GL_INT_SAMPLER_CUBE_MAP_ARRAY = 0x900E;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY = 0x900F;
        public static UInt32 GL_IMAGE_CUBE_MAP_ARRAY = 0x9054;
        public static UInt32 GL_INT_IMAGE_CUBE_MAP_ARRAY = 0x905F;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_CUBE_MAP_ARRAY = 0x906A;
        public static UInt32 GL_STENCIL_INDEX = 0x1901;
        public static UInt32 GL_STENCIL_INDEX8 = 0x8D48;
        public static UInt32 GL_TEXTURE_2D_MULTISAMPLE_ARRAY = 0x9102;
        public static UInt32 GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY = 0x9105;
        public static UInt32 GL_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910B;
        public static UInt32 GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910C;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910D;
        #endregion

        #region Commands
        internal delegate void glBlendBarrierFunc();
        internal static glBlendBarrierFunc glBlendBarrierPtr;
        internal static void loadBlendBarrier()
        {
            try
            {
                glBlendBarrierPtr = (glBlendBarrierFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendBarrier"), typeof(glBlendBarrierFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendBarrier'.");
            }
        }
        public static void glBlendBarrier() => glBlendBarrierPtr();

        internal delegate void glCopyImageSubDataFunc(GLuint @srcName, GLenum @srcTarget, GLint @srcLevel, GLint @srcX, GLint @srcY, GLint @srcZ, GLuint @dstName, GLenum @dstTarget, GLint @dstLevel, GLint @dstX, GLint @dstY, GLint @dstZ, GLsizei @srcWidth, GLsizei @srcHeight, GLsizei @srcDepth);
        internal static glCopyImageSubDataFunc glCopyImageSubDataPtr;
        internal static void loadCopyImageSubData()
        {
            try
            {
                glCopyImageSubDataPtr = (glCopyImageSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyImageSubData"), typeof(glCopyImageSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyImageSubData'.");
            }
        }
        public static void glCopyImageSubData(GLuint @srcName, GLenum @srcTarget, GLint @srcLevel, GLint @srcX, GLint @srcY, GLint @srcZ, GLuint @dstName, GLenum @dstTarget, GLint @dstLevel, GLint @dstX, GLint @dstY, GLint @dstZ, GLsizei @srcWidth, GLsizei @srcHeight, GLsizei @srcDepth) => glCopyImageSubDataPtr(@srcName, @srcTarget, @srcLevel, @srcX, @srcY, @srcZ, @dstName, @dstTarget, @dstLevel, @dstX, @dstY, @dstZ, @srcWidth, @srcHeight, @srcDepth);

        internal delegate void glDebugMessageControlFunc(GLenum @source, GLenum @type, GLenum @severity, GLsizei @count, const GLuint * @ids, GLboolean @enabled);
        internal static glDebugMessageControlFunc glDebugMessageControlPtr;
        internal static void loadDebugMessageControl()
        {
            try
            {
                glDebugMessageControlPtr = (glDebugMessageControlFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageControl"), typeof(glDebugMessageControlFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageControl'.");
            }
        }
        public static void glDebugMessageControl(GLenum @source, GLenum @type, GLenum @severity, GLsizei @count, const GLuint * @ids, GLboolean @enabled) => glDebugMessageControlPtr(@source, @type, @severity, @count, @ids, @enabled);

        internal delegate void glDebugMessageInsertFunc(GLenum @source, GLenum @type, GLuint @id, GLenum @severity, GLsizei @length, const GLchar * @buf);
        internal static glDebugMessageInsertFunc glDebugMessageInsertPtr;
        internal static void loadDebugMessageInsert()
        {
            try
            {
                glDebugMessageInsertPtr = (glDebugMessageInsertFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageInsert"), typeof(glDebugMessageInsertFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageInsert'.");
            }
        }
        public static void glDebugMessageInsert(GLenum @source, GLenum @type, GLuint @id, GLenum @severity, GLsizei @length, const GLchar * @buf) => glDebugMessageInsertPtr(@source, @type, @id, @severity, @length, @buf);

        internal delegate void glDebugMessageCallbackFunc(GLDEBUGPROC @callback, const void * @userParam);
        internal static glDebugMessageCallbackFunc glDebugMessageCallbackPtr;
        internal static void loadDebugMessageCallback()
        {
            try
            {
                glDebugMessageCallbackPtr = (glDebugMessageCallbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageCallback"), typeof(glDebugMessageCallbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageCallback'.");
            }
        }
        public static void glDebugMessageCallback(GLDEBUGPROC @callback, const void * @userParam) => glDebugMessageCallbackPtr(@callback, @userParam);

        internal delegate GLuint glGetDebugMessageLogFunc(GLuint @count, GLsizei @bufSize, GLenum * @sources, GLenum * @types, GLuint * @ids, GLenum * @severities, GLsizei * @lengths, GLchar * @messageLog);
        internal static glGetDebugMessageLogFunc glGetDebugMessageLogPtr;
        internal static void loadGetDebugMessageLog()
        {
            try
            {
                glGetDebugMessageLogPtr = (glGetDebugMessageLogFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDebugMessageLog"), typeof(glGetDebugMessageLogFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDebugMessageLog'.");
            }
        }
        public static GLuint glGetDebugMessageLog(GLuint @count, GLsizei @bufSize, GLenum * @sources, GLenum * @types, GLuint * @ids, GLenum * @severities, GLsizei * @lengths, GLchar * @messageLog) => glGetDebugMessageLogPtr(@count, @bufSize, @sources, @types, @ids, @severities, @lengths, @messageLog);

        internal delegate void glPushDebugGroupFunc(GLenum @source, GLuint @id, GLsizei @length, const GLchar * @message);
        internal static glPushDebugGroupFunc glPushDebugGroupPtr;
        internal static void loadPushDebugGroup()
        {
            try
            {
                glPushDebugGroupPtr = (glPushDebugGroupFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPushDebugGroup"), typeof(glPushDebugGroupFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPushDebugGroup'.");
            }
        }
        public static void glPushDebugGroup(GLenum @source, GLuint @id, GLsizei @length, const GLchar * @message) => glPushDebugGroupPtr(@source, @id, @length, @message);

        internal delegate void glPopDebugGroupFunc();
        internal static glPopDebugGroupFunc glPopDebugGroupPtr;
        internal static void loadPopDebugGroup()
        {
            try
            {
                glPopDebugGroupPtr = (glPopDebugGroupFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPopDebugGroup"), typeof(glPopDebugGroupFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPopDebugGroup'.");
            }
        }
        public static void glPopDebugGroup() => glPopDebugGroupPtr();

        internal delegate void glObjectLabelFunc(GLenum @identifier, GLuint @name, GLsizei @length, const GLchar * @label);
        internal static glObjectLabelFunc glObjectLabelPtr;
        internal static void loadObjectLabel()
        {
            try
            {
                glObjectLabelPtr = (glObjectLabelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glObjectLabel"), typeof(glObjectLabelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glObjectLabel'.");
            }
        }
        public static void glObjectLabel(GLenum @identifier, GLuint @name, GLsizei @length, const GLchar * @label) => glObjectLabelPtr(@identifier, @name, @length, @label);

        internal delegate void glGetObjectLabelFunc(GLenum @identifier, GLuint @name, GLsizei @bufSize, GLsizei * @length, GLchar * @label);
        internal static glGetObjectLabelFunc glGetObjectLabelPtr;
        internal static void loadGetObjectLabel()
        {
            try
            {
                glGetObjectLabelPtr = (glGetObjectLabelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectLabel"), typeof(glGetObjectLabelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectLabel'.");
            }
        }
        public static void glGetObjectLabel(GLenum @identifier, GLuint @name, GLsizei @bufSize, GLsizei * @length, GLchar * @label) => glGetObjectLabelPtr(@identifier, @name, @bufSize, @length, @label);

        internal delegate void glObjectPtrLabelFunc(const void * @ptr, GLsizei @length, const GLchar * @label);
        internal static glObjectPtrLabelFunc glObjectPtrLabelPtr;
        internal static void loadObjectPtrLabel()
        {
            try
            {
                glObjectPtrLabelPtr = (glObjectPtrLabelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glObjectPtrLabel"), typeof(glObjectPtrLabelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glObjectPtrLabel'.");
            }
        }
        public static void glObjectPtrLabel(const void * @ptr, GLsizei @length, const GLchar * @label) => glObjectPtrLabelPtr(@ptr, @length, @label);

        internal delegate void glGetObjectPtrLabelFunc(const void * @ptr, GLsizei @bufSize, GLsizei * @length, GLchar * @label);
        internal static glGetObjectPtrLabelFunc glGetObjectPtrLabelPtr;
        internal static void loadGetObjectPtrLabel()
        {
            try
            {
                glGetObjectPtrLabelPtr = (glGetObjectPtrLabelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectPtrLabel"), typeof(glGetObjectPtrLabelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectPtrLabel'.");
            }
        }
        public static void glGetObjectPtrLabel(const void * @ptr, GLsizei @bufSize, GLsizei * @length, GLchar * @label) => glGetObjectPtrLabelPtr(@ptr, @bufSize, @length, @label);

        internal delegate void glGetPointervFunc(GLenum @pname, void ** @params);
        internal static glGetPointervFunc glGetPointervPtr;
        internal static void loadGetPointerv()
        {
            try
            {
                glGetPointervPtr = (glGetPointervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPointerv"), typeof(glGetPointervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPointerv'.");
            }
        }
        public static void glGetPointerv(GLenum @pname, void ** @params) => glGetPointervPtr(@pname, @params);

        internal delegate void glEnableiFunc(GLenum @target, GLuint @index);
        internal static glEnableiFunc glEnableiPtr;
        internal static void loadEnablei()
        {
            try
            {
                glEnableiPtr = (glEnableiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnablei"), typeof(glEnableiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnablei'.");
            }
        }
        public static void glEnablei(GLenum @target, GLuint @index) => glEnableiPtr(@target, @index);

        internal delegate void glDisableiFunc(GLenum @target, GLuint @index);
        internal static glDisableiFunc glDisableiPtr;
        internal static void loadDisablei()
        {
            try
            {
                glDisableiPtr = (glDisableiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisablei"), typeof(glDisableiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisablei'.");
            }
        }
        public static void glDisablei(GLenum @target, GLuint @index) => glDisableiPtr(@target, @index);

        internal delegate void glBlendEquationiFunc(GLuint @buf, GLenum @mode);
        internal static glBlendEquationiFunc glBlendEquationiPtr;
        internal static void loadBlendEquationi()
        {
            try
            {
                glBlendEquationiPtr = (glBlendEquationiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationi"), typeof(glBlendEquationiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationi'.");
            }
        }
        public static void glBlendEquationi(GLuint @buf, GLenum @mode) => glBlendEquationiPtr(@buf, @mode);

        internal delegate void glBlendEquationSeparateiFunc(GLuint @buf, GLenum @modeRGB, GLenum @modeAlpha);
        internal static glBlendEquationSeparateiFunc glBlendEquationSeparateiPtr;
        internal static void loadBlendEquationSeparatei()
        {
            try
            {
                glBlendEquationSeparateiPtr = (glBlendEquationSeparateiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationSeparatei"), typeof(glBlendEquationSeparateiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationSeparatei'.");
            }
        }
        public static void glBlendEquationSeparatei(GLuint @buf, GLenum @modeRGB, GLenum @modeAlpha) => glBlendEquationSeparateiPtr(@buf, @modeRGB, @modeAlpha);

        internal delegate void glBlendFunciFunc(GLuint @buf, GLenum @src, GLenum @dst);
        internal static glBlendFunciFunc glBlendFunciPtr;
        internal static void loadBlendFunci()
        {
            try
            {
                glBlendFunciPtr = (glBlendFunciFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFunci"), typeof(glBlendFunciFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFunci'.");
            }
        }
        public static void glBlendFunci(GLuint @buf, GLenum @src, GLenum @dst) => glBlendFunciPtr(@buf, @src, @dst);

        internal delegate void glBlendFuncSeparateiFunc(GLuint @buf, GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha);
        internal static glBlendFuncSeparateiFunc glBlendFuncSeparateiPtr;
        internal static void loadBlendFuncSeparatei()
        {
            try
            {
                glBlendFuncSeparateiPtr = (glBlendFuncSeparateiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFuncSeparatei"), typeof(glBlendFuncSeparateiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFuncSeparatei'.");
            }
        }
        public static void glBlendFuncSeparatei(GLuint @buf, GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha) => glBlendFuncSeparateiPtr(@buf, @srcRGB, @dstRGB, @srcAlpha, @dstAlpha);

        internal delegate void glColorMaskiFunc(GLuint @index, GLboolean @r, GLboolean @g, GLboolean @b, GLboolean @a);
        internal static glColorMaskiFunc glColorMaskiPtr;
        internal static void loadColorMaski()
        {
            try
            {
                glColorMaskiPtr = (glColorMaskiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorMaski"), typeof(glColorMaskiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorMaski'.");
            }
        }
        public static void glColorMaski(GLuint @index, GLboolean @r, GLboolean @g, GLboolean @b, GLboolean @a) => glColorMaskiPtr(@index, @r, @g, @b, @a);

        internal delegate GLboolean glIsEnablediFunc(GLenum @target, GLuint @index);
        internal static glIsEnablediFunc glIsEnablediPtr;
        internal static void loadIsEnabledi()
        {
            try
            {
                glIsEnablediPtr = (glIsEnablediFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsEnabledi"), typeof(glIsEnablediFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsEnabledi'.");
            }
        }
        public static GLboolean glIsEnabledi(GLenum @target, GLuint @index) => glIsEnablediPtr(@target, @index);

        internal delegate void glDrawElementsBaseVertexFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex);
        internal static glDrawElementsBaseVertexFunc glDrawElementsBaseVertexPtr;
        internal static void loadDrawElementsBaseVertex()
        {
            try
            {
                glDrawElementsBaseVertexPtr = (glDrawElementsBaseVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsBaseVertex"), typeof(glDrawElementsBaseVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsBaseVertex'.");
            }
        }
        public static void glDrawElementsBaseVertex(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex) => glDrawElementsBaseVertexPtr(@mode, @count, @type, @indices, @basevertex);

        internal delegate void glDrawRangeElementsBaseVertexFunc(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex);
        internal static glDrawRangeElementsBaseVertexFunc glDrawRangeElementsBaseVertexPtr;
        internal static void loadDrawRangeElementsBaseVertex()
        {
            try
            {
                glDrawRangeElementsBaseVertexPtr = (glDrawRangeElementsBaseVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawRangeElementsBaseVertex"), typeof(glDrawRangeElementsBaseVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawRangeElementsBaseVertex'.");
            }
        }
        public static void glDrawRangeElementsBaseVertex(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex) => glDrawRangeElementsBaseVertexPtr(@mode, @start, @end, @count, @type, @indices, @basevertex);

        internal delegate void glDrawElementsInstancedBaseVertexFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex);
        internal static glDrawElementsInstancedBaseVertexFunc glDrawElementsInstancedBaseVertexPtr;
        internal static void loadDrawElementsInstancedBaseVertex()
        {
            try
            {
                glDrawElementsInstancedBaseVertexPtr = (glDrawElementsInstancedBaseVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedBaseVertex"), typeof(glDrawElementsInstancedBaseVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedBaseVertex'.");
            }
        }
        public static void glDrawElementsInstancedBaseVertex(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex) => glDrawElementsInstancedBaseVertexPtr(@mode, @count, @type, @indices, @instancecount, @basevertex);

        internal delegate void glFramebufferTextureFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level);
        internal static glFramebufferTextureFunc glFramebufferTexturePtr;
        internal static void loadFramebufferTexture()
        {
            try
            {
                glFramebufferTexturePtr = (glFramebufferTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture"), typeof(glFramebufferTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture'.");
            }
        }
        public static void glFramebufferTexture(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level) => glFramebufferTexturePtr(@target, @attachment, @texture, @level);

        internal delegate void glPrimitiveBoundingBoxFunc(GLfloat @minX, GLfloat @minY, GLfloat @minZ, GLfloat @minW, GLfloat @maxX, GLfloat @maxY, GLfloat @maxZ, GLfloat @maxW);
        internal static glPrimitiveBoundingBoxFunc glPrimitiveBoundingBoxPtr;
        internal static void loadPrimitiveBoundingBox()
        {
            try
            {
                glPrimitiveBoundingBoxPtr = (glPrimitiveBoundingBoxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPrimitiveBoundingBox"), typeof(glPrimitiveBoundingBoxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPrimitiveBoundingBox'.");
            }
        }
        public static void glPrimitiveBoundingBox(GLfloat @minX, GLfloat @minY, GLfloat @minZ, GLfloat @minW, GLfloat @maxX, GLfloat @maxY, GLfloat @maxZ, GLfloat @maxW) => glPrimitiveBoundingBoxPtr(@minX, @minY, @minZ, @minW, @maxX, @maxY, @maxZ, @maxW);

        internal delegate GLenum glGetGraphicsResetStatusFunc();
        internal static glGetGraphicsResetStatusFunc glGetGraphicsResetStatusPtr;
        internal static void loadGetGraphicsResetStatus()
        {
            try
            {
                glGetGraphicsResetStatusPtr = (glGetGraphicsResetStatusFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetGraphicsResetStatus"), typeof(glGetGraphicsResetStatusFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetGraphicsResetStatus'.");
            }
        }
        public static GLenum glGetGraphicsResetStatus() => glGetGraphicsResetStatusPtr();

        internal delegate void glReadnPixelsFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, GLsizei @bufSize, void * @data);
        internal static glReadnPixelsFunc glReadnPixelsPtr;
        internal static void loadReadnPixels()
        {
            try
            {
                glReadnPixelsPtr = (glReadnPixelsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadnPixels"), typeof(glReadnPixelsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadnPixels'.");
            }
        }
        public static void glReadnPixels(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, GLsizei @bufSize, void * @data) => glReadnPixelsPtr(@x, @y, @width, @height, @format, @type, @bufSize, @data);

        internal delegate void glGetnUniformfvFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLfloat * @params);
        internal static glGetnUniformfvFunc glGetnUniformfvPtr;
        internal static void loadGetnUniformfv()
        {
            try
            {
                glGetnUniformfvPtr = (glGetnUniformfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformfv"), typeof(glGetnUniformfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformfv'.");
            }
        }
        public static void glGetnUniformfv(GLuint @program, GLint @location, GLsizei @bufSize, GLfloat * @params) => glGetnUniformfvPtr(@program, @location, @bufSize, @params);

        internal delegate void glGetnUniformivFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLint * @params);
        internal static glGetnUniformivFunc glGetnUniformivPtr;
        internal static void loadGetnUniformiv()
        {
            try
            {
                glGetnUniformivPtr = (glGetnUniformivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformiv"), typeof(glGetnUniformivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformiv'.");
            }
        }
        public static void glGetnUniformiv(GLuint @program, GLint @location, GLsizei @bufSize, GLint * @params) => glGetnUniformivPtr(@program, @location, @bufSize, @params);

        internal delegate void glGetnUniformuivFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLuint * @params);
        internal static glGetnUniformuivFunc glGetnUniformuivPtr;
        internal static void loadGetnUniformuiv()
        {
            try
            {
                glGetnUniformuivPtr = (glGetnUniformuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformuiv"), typeof(glGetnUniformuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformuiv'.");
            }
        }
        public static void glGetnUniformuiv(GLuint @program, GLint @location, GLsizei @bufSize, GLuint * @params) => glGetnUniformuivPtr(@program, @location, @bufSize, @params);

        internal delegate void glMinSampleShadingFunc(GLfloat @value);
        internal static glMinSampleShadingFunc glMinSampleShadingPtr;
        internal static void loadMinSampleShading()
        {
            try
            {
                glMinSampleShadingPtr = (glMinSampleShadingFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMinSampleShading"), typeof(glMinSampleShadingFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMinSampleShading'.");
            }
        }
        public static void glMinSampleShading(GLfloat @value) => glMinSampleShadingPtr(@value);

        internal delegate void glPatchParameteriFunc(GLenum @pname, GLint @value);
        internal static glPatchParameteriFunc glPatchParameteriPtr;
        internal static void loadPatchParameteri()
        {
            try
            {
                glPatchParameteriPtr = (glPatchParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPatchParameteri"), typeof(glPatchParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPatchParameteri'.");
            }
        }
        public static void glPatchParameteri(GLenum @pname, GLint @value) => glPatchParameteriPtr(@pname, @value);

        internal delegate void glTexParameterIivFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glTexParameterIivFunc glTexParameterIivPtr;
        internal static void loadTexParameterIiv()
        {
            try
            {
                glTexParameterIivPtr = (glTexParameterIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterIiv"), typeof(glTexParameterIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterIiv'.");
            }
        }
        public static void glTexParameterIiv(GLenum @target, GLenum @pname, const GLint * @params) => glTexParameterIivPtr(@target, @pname, @params);

        internal delegate void glTexParameterIuivFunc(GLenum @target, GLenum @pname, const GLuint * @params);
        internal static glTexParameterIuivFunc glTexParameterIuivPtr;
        internal static void loadTexParameterIuiv()
        {
            try
            {
                glTexParameterIuivPtr = (glTexParameterIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterIuiv"), typeof(glTexParameterIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterIuiv'.");
            }
        }
        public static void glTexParameterIuiv(GLenum @target, GLenum @pname, const GLuint * @params) => glTexParameterIuivPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterIivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetTexParameterIivFunc glGetTexParameterIivPtr;
        internal static void loadGetTexParameterIiv()
        {
            try
            {
                glGetTexParameterIivPtr = (glGetTexParameterIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterIiv"), typeof(glGetTexParameterIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterIiv'.");
            }
        }
        public static void glGetTexParameterIiv(GLenum @target, GLenum @pname, GLint * @params) => glGetTexParameterIivPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterIuivFunc(GLenum @target, GLenum @pname, GLuint * @params);
        internal static glGetTexParameterIuivFunc glGetTexParameterIuivPtr;
        internal static void loadGetTexParameterIuiv()
        {
            try
            {
                glGetTexParameterIuivPtr = (glGetTexParameterIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterIuiv"), typeof(glGetTexParameterIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterIuiv'.");
            }
        }
        public static void glGetTexParameterIuiv(GLenum @target, GLenum @pname, GLuint * @params) => glGetTexParameterIuivPtr(@target, @pname, @params);

        internal delegate void glSamplerParameterIivFunc(GLuint @sampler, GLenum @pname, const GLint * @param);
        internal static glSamplerParameterIivFunc glSamplerParameterIivPtr;
        internal static void loadSamplerParameterIiv()
        {
            try
            {
                glSamplerParameterIivPtr = (glSamplerParameterIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterIiv"), typeof(glSamplerParameterIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterIiv'.");
            }
        }
        public static void glSamplerParameterIiv(GLuint @sampler, GLenum @pname, const GLint * @param) => glSamplerParameterIivPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterIuivFunc(GLuint @sampler, GLenum @pname, const GLuint * @param);
        internal static glSamplerParameterIuivFunc glSamplerParameterIuivPtr;
        internal static void loadSamplerParameterIuiv()
        {
            try
            {
                glSamplerParameterIuivPtr = (glSamplerParameterIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterIuiv"), typeof(glSamplerParameterIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterIuiv'.");
            }
        }
        public static void glSamplerParameterIuiv(GLuint @sampler, GLenum @pname, const GLuint * @param) => glSamplerParameterIuivPtr(@sampler, @pname, @param);

        internal delegate void glGetSamplerParameterIivFunc(GLuint @sampler, GLenum @pname, GLint * @params);
        internal static glGetSamplerParameterIivFunc glGetSamplerParameterIivPtr;
        internal static void loadGetSamplerParameterIiv()
        {
            try
            {
                glGetSamplerParameterIivPtr = (glGetSamplerParameterIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterIiv"), typeof(glGetSamplerParameterIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterIiv'.");
            }
        }
        public static void glGetSamplerParameterIiv(GLuint @sampler, GLenum @pname, GLint * @params) => glGetSamplerParameterIivPtr(@sampler, @pname, @params);

        internal delegate void glGetSamplerParameterIuivFunc(GLuint @sampler, GLenum @pname, GLuint * @params);
        internal static glGetSamplerParameterIuivFunc glGetSamplerParameterIuivPtr;
        internal static void loadGetSamplerParameterIuiv()
        {
            try
            {
                glGetSamplerParameterIuivPtr = (glGetSamplerParameterIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterIuiv"), typeof(glGetSamplerParameterIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterIuiv'.");
            }
        }
        public static void glGetSamplerParameterIuiv(GLuint @sampler, GLenum @pname, GLuint * @params) => glGetSamplerParameterIuivPtr(@sampler, @pname, @params);

        internal delegate void glTexBufferFunc(GLenum @target, GLenum @internalformat, GLuint @buffer);
        internal static glTexBufferFunc glTexBufferPtr;
        internal static void loadTexBuffer()
        {
            try
            {
                glTexBufferPtr = (glTexBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBuffer"), typeof(glTexBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBuffer'.");
            }
        }
        public static void glTexBuffer(GLenum @target, GLenum @internalformat, GLuint @buffer) => glTexBufferPtr(@target, @internalformat, @buffer);

        internal delegate void glTexBufferRangeFunc(GLenum @target, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glTexBufferRangeFunc glTexBufferRangePtr;
        internal static void loadTexBufferRange()
        {
            try
            {
                glTexBufferRangePtr = (glTexBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBufferRange"), typeof(glTexBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBufferRange'.");
            }
        }
        public static void glTexBufferRange(GLenum @target, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glTexBufferRangePtr(@target, @internalformat, @buffer, @offset, @size);

        internal delegate void glTexStorage3DMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations);
        internal static glTexStorage3DMultisampleFunc glTexStorage3DMultisamplePtr;
        internal static void loadTexStorage3DMultisample()
        {
            try
            {
                glTexStorage3DMultisamplePtr = (glTexStorage3DMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage3DMultisample"), typeof(glTexStorage3DMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage3DMultisample'.");
            }
        }
        public static void glTexStorage3DMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations) => glTexStorage3DMultisamplePtr(@target, @samples, @internalformat, @width, @height, @depth, @fixedsamplelocations);
        #endregion
    }
}
