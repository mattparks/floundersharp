using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL21
    {
        #region Interop
        static GL21()
        {
            Console.WriteLine("Initalising GL21 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadUniformMatrix2x3fv();
            loadUniformMatrix3x2fv();
            loadUniformMatrix2x4fv();
            loadUniformMatrix4x2fv();
            loadUniformMatrix3x4fv();
            loadUniformMatrix4x3fv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PIXEL_PACK_BUFFER = 0x88EB;
        public static UInt32 GL_PIXEL_UNPACK_BUFFER = 0x88EC;
        public static UInt32 GL_PIXEL_PACK_BUFFER_BINDING = 0x88ED;
        public static UInt32 GL_PIXEL_UNPACK_BUFFER_BINDING = 0x88EF;
        public static UInt32 GL_FLOAT_MAT2x3 = 0x8B65;
        public static UInt32 GL_FLOAT_MAT2x4 = 0x8B66;
        public static UInt32 GL_FLOAT_MAT3x2 = 0x8B67;
        public static UInt32 GL_FLOAT_MAT3x4 = 0x8B68;
        public static UInt32 GL_FLOAT_MAT4x2 = 0x8B69;
        public static UInt32 GL_FLOAT_MAT4x3 = 0x8B6A;
        public static UInt32 GL_SRGB = 0x8C40;
        public static UInt32 GL_SRGB8 = 0x8C41;
        public static UInt32 GL_SRGB_ALPHA = 0x8C42;
        public static UInt32 GL_SRGB8_ALPHA8 = 0x8C43;
        public static UInt32 GL_COMPRESSED_SRGB = 0x8C48;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA = 0x8C49;
        public static UInt32 GL_CURRENT_RASTER_SECONDARY_COLOR = 0x845F;
        public static UInt32 GL_SLUMINANCE_ALPHA = 0x8C44;
        public static UInt32 GL_SLUMINANCE8_ALPHA8 = 0x8C45;
        public static UInt32 GL_SLUMINANCE = 0x8C46;
        public static UInt32 GL_SLUMINANCE8 = 0x8C47;
        public static UInt32 GL_COMPRESSED_SLUMINANCE = 0x8C4A;
        public static UInt32 GL_COMPRESSED_SLUMINANCE_ALPHA = 0x8C4B;
        #endregion

        #region Commands
        internal delegate void glUniformMatrix2x3fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix2x3fvFunc glUniformMatrix2x3fvPtr;
        internal static void loadUniformMatrix2x3fv()
        {
            try
            {
                glUniformMatrix2x3fvPtr = (glUniformMatrix2x3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2x3fv"), typeof(glUniformMatrix2x3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2x3fv'.");
            }
        }
        public static void glUniformMatrix2x3fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix2x3fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3x2fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix3x2fvFunc glUniformMatrix3x2fvPtr;
        internal static void loadUniformMatrix3x2fv()
        {
            try
            {
                glUniformMatrix3x2fvPtr = (glUniformMatrix3x2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3x2fv"), typeof(glUniformMatrix3x2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3x2fv'.");
            }
        }
        public static void glUniformMatrix3x2fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix3x2fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix2x4fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix2x4fvFunc glUniformMatrix2x4fvPtr;
        internal static void loadUniformMatrix2x4fv()
        {
            try
            {
                glUniformMatrix2x4fvPtr = (glUniformMatrix2x4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2x4fv"), typeof(glUniformMatrix2x4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2x4fv'.");
            }
        }
        public static void glUniformMatrix2x4fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix2x4fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4x2fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix4x2fvFunc glUniformMatrix4x2fvPtr;
        internal static void loadUniformMatrix4x2fv()
        {
            try
            {
                glUniformMatrix4x2fvPtr = (glUniformMatrix4x2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4x2fv"), typeof(glUniformMatrix4x2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4x2fv'.");
            }
        }
        public static void glUniformMatrix4x2fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix4x2fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3x4fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix3x4fvFunc glUniformMatrix3x4fvPtr;
        internal static void loadUniformMatrix3x4fv()
        {
            try
            {
                glUniformMatrix3x4fvPtr = (glUniformMatrix3x4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3x4fv"), typeof(glUniformMatrix3x4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3x4fv'.");
            }
        }
        public static void glUniformMatrix3x4fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix3x4fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4x3fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix4x3fvFunc glUniformMatrix4x3fvPtr;
        internal static void loadUniformMatrix4x3fv()
        {
            try
            {
                glUniformMatrix4x3fvPtr = (glUniformMatrix4x3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4x3fv"), typeof(glUniformMatrix4x3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4x3fv'.");
            }
        }
        public static void glUniformMatrix4x3fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix4x3fvPtr(@location, @count, @transpose, @value);
        #endregion
    }
}
