using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_HP_image_transform
    {
        #region Interop
        static GL_HP_image_transform()
        {
            Console.WriteLine("Initalising GL_HP_image_transform interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadImageTransformParameteriHP();
            loadImageTransformParameterfHP();
            loadImageTransformParameterivHP();
            loadImageTransformParameterfvHP();
            loadGetImageTransformParameterivHP();
            loadGetImageTransformParameterfvHP();
        }
        #endregion

        #region Enums
        public static UInt32 GL_IMAGE_SCALE_X_HP = 0x8155;
        public static UInt32 GL_IMAGE_SCALE_Y_HP = 0x8156;
        public static UInt32 GL_IMAGE_TRANSLATE_X_HP = 0x8157;
        public static UInt32 GL_IMAGE_TRANSLATE_Y_HP = 0x8158;
        public static UInt32 GL_IMAGE_ROTATE_ANGLE_HP = 0x8159;
        public static UInt32 GL_IMAGE_ROTATE_ORIGIN_X_HP = 0x815A;
        public static UInt32 GL_IMAGE_ROTATE_ORIGIN_Y_HP = 0x815B;
        public static UInt32 GL_IMAGE_MAG_FILTER_HP = 0x815C;
        public static UInt32 GL_IMAGE_MIN_FILTER_HP = 0x815D;
        public static UInt32 GL_IMAGE_CUBIC_WEIGHT_HP = 0x815E;
        public static UInt32 GL_CUBIC_HP = 0x815F;
        public static UInt32 GL_AVERAGE_HP = 0x8160;
        public static UInt32 GL_IMAGE_TRANSFORM_2D_HP = 0x8161;
        public static UInt32 GL_POST_IMAGE_TRANSFORM_COLOR_TABLE_HP = 0x8162;
        public static UInt32 GL_PROXY_POST_IMAGE_TRANSFORM_COLOR_TABLE_HP = 0x8163;
        #endregion

        #region Commands
        internal delegate void glImageTransformParameteriHPFunc(GLenum @target, GLenum @pname, GLint @param);
        internal static glImageTransformParameteriHPFunc glImageTransformParameteriHPPtr;
        internal static void loadImageTransformParameteriHP()
        {
            try
            {
                glImageTransformParameteriHPPtr = (glImageTransformParameteriHPFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glImageTransformParameteriHP"), typeof(glImageTransformParameteriHPFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glImageTransformParameteriHP'.");
            }
        }
        public static void glImageTransformParameteriHP(GLenum @target, GLenum @pname, GLint @param) => glImageTransformParameteriHPPtr(@target, @pname, @param);

        internal delegate void glImageTransformParameterfHPFunc(GLenum @target, GLenum @pname, GLfloat @param);
        internal static glImageTransformParameterfHPFunc glImageTransformParameterfHPPtr;
        internal static void loadImageTransformParameterfHP()
        {
            try
            {
                glImageTransformParameterfHPPtr = (glImageTransformParameterfHPFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glImageTransformParameterfHP"), typeof(glImageTransformParameterfHPFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glImageTransformParameterfHP'.");
            }
        }
        public static void glImageTransformParameterfHP(GLenum @target, GLenum @pname, GLfloat @param) => glImageTransformParameterfHPPtr(@target, @pname, @param);

        internal delegate void glImageTransformParameterivHPFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glImageTransformParameterivHPFunc glImageTransformParameterivHPPtr;
        internal static void loadImageTransformParameterivHP()
        {
            try
            {
                glImageTransformParameterivHPPtr = (glImageTransformParameterivHPFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glImageTransformParameterivHP"), typeof(glImageTransformParameterivHPFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glImageTransformParameterivHP'.");
            }
        }
        public static void glImageTransformParameterivHP(GLenum @target, GLenum @pname, const GLint * @params) => glImageTransformParameterivHPPtr(@target, @pname, @params);

        internal delegate void glImageTransformParameterfvHPFunc(GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glImageTransformParameterfvHPFunc glImageTransformParameterfvHPPtr;
        internal static void loadImageTransformParameterfvHP()
        {
            try
            {
                glImageTransformParameterfvHPPtr = (glImageTransformParameterfvHPFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glImageTransformParameterfvHP"), typeof(glImageTransformParameterfvHPFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glImageTransformParameterfvHP'.");
            }
        }
        public static void glImageTransformParameterfvHP(GLenum @target, GLenum @pname, const GLfloat * @params) => glImageTransformParameterfvHPPtr(@target, @pname, @params);

        internal delegate void glGetImageTransformParameterivHPFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetImageTransformParameterivHPFunc glGetImageTransformParameterivHPPtr;
        internal static void loadGetImageTransformParameterivHP()
        {
            try
            {
                glGetImageTransformParameterivHPPtr = (glGetImageTransformParameterivHPFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetImageTransformParameterivHP"), typeof(glGetImageTransformParameterivHPFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetImageTransformParameterivHP'.");
            }
        }
        public static void glGetImageTransformParameterivHP(GLenum @target, GLenum @pname, GLint * @params) => glGetImageTransformParameterivHPPtr(@target, @pname, @params);

        internal delegate void glGetImageTransformParameterfvHPFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetImageTransformParameterfvHPFunc glGetImageTransformParameterfvHPPtr;
        internal static void loadGetImageTransformParameterfvHP()
        {
            try
            {
                glGetImageTransformParameterfvHPPtr = (glGetImageTransformParameterfvHPFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetImageTransformParameterfvHP"), typeof(glGetImageTransformParameterfvHPFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetImageTransformParameterfvHP'.");
            }
        }
        public static void glGetImageTransformParameterfvHP(GLenum @target, GLenum @pname, GLfloat * @params) => glGetImageTransformParameterfvHPPtr(@target, @pname, @params);
        #endregion
    }
}
