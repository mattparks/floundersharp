using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_HP_occlusion_test
    {
        #region Interop
        static GL_HP_occlusion_test()
        {
            Console.WriteLine("Initalising GL_HP_occlusion_test interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_OCCLUSION_TEST_HP = 0x8165;
        public static UInt32 GL_OCCLUSION_TEST_RESULT_HP = 0x8166;
        #endregion

        #region Commands
        #endregion
    }
}
