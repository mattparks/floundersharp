using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_HP_convolution_border_modes
    {
        #region Interop
        static GL_HP_convolution_border_modes()
        {
            Console.WriteLine("Initalising GL_HP_convolution_border_modes interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_IGNORE_BORDER_HP = 0x8150;
        public static UInt32 GL_CONSTANT_BORDER_HP = 0x8151;
        public static UInt32 GL_REPLICATE_BORDER_HP = 0x8153;
        public static UInt32 GL_CONVOLUTION_BORDER_COLOR_HP = 0x8154;
        #endregion

        #region Commands
        #endregion
    }
}
