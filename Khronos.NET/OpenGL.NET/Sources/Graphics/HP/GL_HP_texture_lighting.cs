using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_HP_texture_lighting
    {
        #region Interop
        static GL_HP_texture_lighting()
        {
            Console.WriteLine("Initalising GL_HP_texture_lighting interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_LIGHTING_MODE_HP = 0x8167;
        public static UInt32 GL_TEXTURE_POST_SPECULAR_HP = 0x8168;
        public static UInt32 GL_TEXTURE_PRE_SPECULAR_HP = 0x8169;
        #endregion

        #region Commands
        #endregion
    }
}
