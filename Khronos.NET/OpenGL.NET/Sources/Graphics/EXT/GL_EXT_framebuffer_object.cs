using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_framebuffer_object
    {
        #region Interop
        static GL_EXT_framebuffer_object()
        {
            Console.WriteLine("Initalising GL_EXT_framebuffer_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadIsRenderbufferEXT();
            loadBindRenderbufferEXT();
            loadDeleteRenderbuffersEXT();
            loadGenRenderbuffersEXT();
            loadRenderbufferStorageEXT();
            loadGetRenderbufferParameterivEXT();
            loadIsFramebufferEXT();
            loadBindFramebufferEXT();
            loadDeleteFramebuffersEXT();
            loadGenFramebuffersEXT();
            loadCheckFramebufferStatusEXT();
            loadFramebufferTexture1DEXT();
            loadFramebufferTexture2DEXT();
            loadFramebufferTexture3DEXT();
            loadFramebufferRenderbufferEXT();
            loadGetFramebufferAttachmentParameterivEXT();
            loadGenerateMipmapEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_INVALID_FRAMEBUFFER_OPERATION_EXT = 0x0506;
        public static UInt32 GL_MAX_RENDERBUFFER_SIZE_EXT = 0x84E8;
        public static UInt32 GL_FRAMEBUFFER_BINDING_EXT = 0x8CA6;
        public static UInt32 GL_RENDERBUFFER_BINDING_EXT = 0x8CA7;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_EXT = 0x8CD0;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_EXT = 0x8CD1;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL_EXT = 0x8CD2;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE_EXT = 0x8CD3;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_3D_ZOFFSET_EXT = 0x8CD4;
        public static UInt32 GL_FRAMEBUFFER_COMPLETE_EXT = 0x8CD5;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT = 0x8CD6;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT = 0x8CD7;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT = 0x8CD9;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT = 0x8CDA;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT = 0x8CDB;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT = 0x8CDC;
        public static UInt32 GL_FRAMEBUFFER_UNSUPPORTED_EXT = 0x8CDD;
        public static UInt32 GL_MAX_COLOR_ATTACHMENTS_EXT = 0x8CDF;
        public static UInt32 GL_COLOR_ATTACHMENT0_EXT = 0x8CE0;
        public static UInt32 GL_COLOR_ATTACHMENT1_EXT = 0x8CE1;
        public static UInt32 GL_COLOR_ATTACHMENT2_EXT = 0x8CE2;
        public static UInt32 GL_COLOR_ATTACHMENT3_EXT = 0x8CE3;
        public static UInt32 GL_COLOR_ATTACHMENT4_EXT = 0x8CE4;
        public static UInt32 GL_COLOR_ATTACHMENT5_EXT = 0x8CE5;
        public static UInt32 GL_COLOR_ATTACHMENT6_EXT = 0x8CE6;
        public static UInt32 GL_COLOR_ATTACHMENT7_EXT = 0x8CE7;
        public static UInt32 GL_COLOR_ATTACHMENT8_EXT = 0x8CE8;
        public static UInt32 GL_COLOR_ATTACHMENT9_EXT = 0x8CE9;
        public static UInt32 GL_COLOR_ATTACHMENT10_EXT = 0x8CEA;
        public static UInt32 GL_COLOR_ATTACHMENT11_EXT = 0x8CEB;
        public static UInt32 GL_COLOR_ATTACHMENT12_EXT = 0x8CEC;
        public static UInt32 GL_COLOR_ATTACHMENT13_EXT = 0x8CED;
        public static UInt32 GL_COLOR_ATTACHMENT14_EXT = 0x8CEE;
        public static UInt32 GL_COLOR_ATTACHMENT15_EXT = 0x8CEF;
        public static UInt32 GL_DEPTH_ATTACHMENT_EXT = 0x8D00;
        public static UInt32 GL_STENCIL_ATTACHMENT_EXT = 0x8D20;
        public static UInt32 GL_FRAMEBUFFER_EXT = 0x8D40;
        public static UInt32 GL_RENDERBUFFER_EXT = 0x8D41;
        public static UInt32 GL_RENDERBUFFER_WIDTH_EXT = 0x8D42;
        public static UInt32 GL_RENDERBUFFER_HEIGHT_EXT = 0x8D43;
        public static UInt32 GL_RENDERBUFFER_INTERNAL_FORMAT_EXT = 0x8D44;
        public static UInt32 GL_STENCIL_INDEX1_EXT = 0x8D46;
        public static UInt32 GL_STENCIL_INDEX4_EXT = 0x8D47;
        public static UInt32 GL_STENCIL_INDEX8_EXT = 0x8D48;
        public static UInt32 GL_STENCIL_INDEX16_EXT = 0x8D49;
        public static UInt32 GL_RENDERBUFFER_RED_SIZE_EXT = 0x8D50;
        public static UInt32 GL_RENDERBUFFER_GREEN_SIZE_EXT = 0x8D51;
        public static UInt32 GL_RENDERBUFFER_BLUE_SIZE_EXT = 0x8D52;
        public static UInt32 GL_RENDERBUFFER_ALPHA_SIZE_EXT = 0x8D53;
        public static UInt32 GL_RENDERBUFFER_DEPTH_SIZE_EXT = 0x8D54;
        public static UInt32 GL_RENDERBUFFER_STENCIL_SIZE_EXT = 0x8D55;
        #endregion

        #region Commands
        internal delegate GLboolean glIsRenderbufferEXTFunc(GLuint @renderbuffer);
        internal static glIsRenderbufferEXTFunc glIsRenderbufferEXTPtr;
        internal static void loadIsRenderbufferEXT()
        {
            try
            {
                glIsRenderbufferEXTPtr = (glIsRenderbufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsRenderbufferEXT"), typeof(glIsRenderbufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsRenderbufferEXT'.");
            }
        }
        public static GLboolean glIsRenderbufferEXT(GLuint @renderbuffer) => glIsRenderbufferEXTPtr(@renderbuffer);

        internal delegate void glBindRenderbufferEXTFunc(GLenum @target, GLuint @renderbuffer);
        internal static glBindRenderbufferEXTFunc glBindRenderbufferEXTPtr;
        internal static void loadBindRenderbufferEXT()
        {
            try
            {
                glBindRenderbufferEXTPtr = (glBindRenderbufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindRenderbufferEXT"), typeof(glBindRenderbufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindRenderbufferEXT'.");
            }
        }
        public static void glBindRenderbufferEXT(GLenum @target, GLuint @renderbuffer) => glBindRenderbufferEXTPtr(@target, @renderbuffer);

        internal delegate void glDeleteRenderbuffersEXTFunc(GLsizei @n, const GLuint * @renderbuffers);
        internal static glDeleteRenderbuffersEXTFunc glDeleteRenderbuffersEXTPtr;
        internal static void loadDeleteRenderbuffersEXT()
        {
            try
            {
                glDeleteRenderbuffersEXTPtr = (glDeleteRenderbuffersEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteRenderbuffersEXT"), typeof(glDeleteRenderbuffersEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteRenderbuffersEXT'.");
            }
        }
        public static void glDeleteRenderbuffersEXT(GLsizei @n, const GLuint * @renderbuffers) => glDeleteRenderbuffersEXTPtr(@n, @renderbuffers);

        internal delegate void glGenRenderbuffersEXTFunc(GLsizei @n, GLuint * @renderbuffers);
        internal static glGenRenderbuffersEXTFunc glGenRenderbuffersEXTPtr;
        internal static void loadGenRenderbuffersEXT()
        {
            try
            {
                glGenRenderbuffersEXTPtr = (glGenRenderbuffersEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenRenderbuffersEXT"), typeof(glGenRenderbuffersEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenRenderbuffersEXT'.");
            }
        }
        public static void glGenRenderbuffersEXT(GLsizei @n, GLuint * @renderbuffers) => glGenRenderbuffersEXTPtr(@n, @renderbuffers);

        internal delegate void glRenderbufferStorageEXTFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageEXTFunc glRenderbufferStorageEXTPtr;
        internal static void loadRenderbufferStorageEXT()
        {
            try
            {
                glRenderbufferStorageEXTPtr = (glRenderbufferStorageEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorageEXT"), typeof(glRenderbufferStorageEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorageEXT'.");
            }
        }
        public static void glRenderbufferStorageEXT(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStorageEXTPtr(@target, @internalformat, @width, @height);

        internal delegate void glGetRenderbufferParameterivEXTFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetRenderbufferParameterivEXTFunc glGetRenderbufferParameterivEXTPtr;
        internal static void loadGetRenderbufferParameterivEXT()
        {
            try
            {
                glGetRenderbufferParameterivEXTPtr = (glGetRenderbufferParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetRenderbufferParameterivEXT"), typeof(glGetRenderbufferParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetRenderbufferParameterivEXT'.");
            }
        }
        public static void glGetRenderbufferParameterivEXT(GLenum @target, GLenum @pname, GLint * @params) => glGetRenderbufferParameterivEXTPtr(@target, @pname, @params);

        internal delegate GLboolean glIsFramebufferEXTFunc(GLuint @framebuffer);
        internal static glIsFramebufferEXTFunc glIsFramebufferEXTPtr;
        internal static void loadIsFramebufferEXT()
        {
            try
            {
                glIsFramebufferEXTPtr = (glIsFramebufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsFramebufferEXT"), typeof(glIsFramebufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsFramebufferEXT'.");
            }
        }
        public static GLboolean glIsFramebufferEXT(GLuint @framebuffer) => glIsFramebufferEXTPtr(@framebuffer);

        internal delegate void glBindFramebufferEXTFunc(GLenum @target, GLuint @framebuffer);
        internal static glBindFramebufferEXTFunc glBindFramebufferEXTPtr;
        internal static void loadBindFramebufferEXT()
        {
            try
            {
                glBindFramebufferEXTPtr = (glBindFramebufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindFramebufferEXT"), typeof(glBindFramebufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindFramebufferEXT'.");
            }
        }
        public static void glBindFramebufferEXT(GLenum @target, GLuint @framebuffer) => glBindFramebufferEXTPtr(@target, @framebuffer);

        internal delegate void glDeleteFramebuffersEXTFunc(GLsizei @n, const GLuint * @framebuffers);
        internal static glDeleteFramebuffersEXTFunc glDeleteFramebuffersEXTPtr;
        internal static void loadDeleteFramebuffersEXT()
        {
            try
            {
                glDeleteFramebuffersEXTPtr = (glDeleteFramebuffersEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteFramebuffersEXT"), typeof(glDeleteFramebuffersEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteFramebuffersEXT'.");
            }
        }
        public static void glDeleteFramebuffersEXT(GLsizei @n, const GLuint * @framebuffers) => glDeleteFramebuffersEXTPtr(@n, @framebuffers);

        internal delegate void glGenFramebuffersEXTFunc(GLsizei @n, GLuint * @framebuffers);
        internal static glGenFramebuffersEXTFunc glGenFramebuffersEXTPtr;
        internal static void loadGenFramebuffersEXT()
        {
            try
            {
                glGenFramebuffersEXTPtr = (glGenFramebuffersEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenFramebuffersEXT"), typeof(glGenFramebuffersEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenFramebuffersEXT'.");
            }
        }
        public static void glGenFramebuffersEXT(GLsizei @n, GLuint * @framebuffers) => glGenFramebuffersEXTPtr(@n, @framebuffers);

        internal delegate GLenum glCheckFramebufferStatusEXTFunc(GLenum @target);
        internal static glCheckFramebufferStatusEXTFunc glCheckFramebufferStatusEXTPtr;
        internal static void loadCheckFramebufferStatusEXT()
        {
            try
            {
                glCheckFramebufferStatusEXTPtr = (glCheckFramebufferStatusEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCheckFramebufferStatusEXT"), typeof(glCheckFramebufferStatusEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCheckFramebufferStatusEXT'.");
            }
        }
        public static GLenum glCheckFramebufferStatusEXT(GLenum @target) => glCheckFramebufferStatusEXTPtr(@target);

        internal delegate void glFramebufferTexture1DEXTFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level);
        internal static glFramebufferTexture1DEXTFunc glFramebufferTexture1DEXTPtr;
        internal static void loadFramebufferTexture1DEXT()
        {
            try
            {
                glFramebufferTexture1DEXTPtr = (glFramebufferTexture1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture1DEXT"), typeof(glFramebufferTexture1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture1DEXT'.");
            }
        }
        public static void glFramebufferTexture1DEXT(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level) => glFramebufferTexture1DEXTPtr(@target, @attachment, @textarget, @texture, @level);

        internal delegate void glFramebufferTexture2DEXTFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level);
        internal static glFramebufferTexture2DEXTFunc glFramebufferTexture2DEXTPtr;
        internal static void loadFramebufferTexture2DEXT()
        {
            try
            {
                glFramebufferTexture2DEXTPtr = (glFramebufferTexture2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture2DEXT"), typeof(glFramebufferTexture2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture2DEXT'.");
            }
        }
        public static void glFramebufferTexture2DEXT(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level) => glFramebufferTexture2DEXTPtr(@target, @attachment, @textarget, @texture, @level);

        internal delegate void glFramebufferTexture3DEXTFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLint @zoffset);
        internal static glFramebufferTexture3DEXTFunc glFramebufferTexture3DEXTPtr;
        internal static void loadFramebufferTexture3DEXT()
        {
            try
            {
                glFramebufferTexture3DEXTPtr = (glFramebufferTexture3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture3DEXT"), typeof(glFramebufferTexture3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture3DEXT'.");
            }
        }
        public static void glFramebufferTexture3DEXT(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLint @zoffset) => glFramebufferTexture3DEXTPtr(@target, @attachment, @textarget, @texture, @level, @zoffset);

        internal delegate void glFramebufferRenderbufferEXTFunc(GLenum @target, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer);
        internal static glFramebufferRenderbufferEXTFunc glFramebufferRenderbufferEXTPtr;
        internal static void loadFramebufferRenderbufferEXT()
        {
            try
            {
                glFramebufferRenderbufferEXTPtr = (glFramebufferRenderbufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferRenderbufferEXT"), typeof(glFramebufferRenderbufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferRenderbufferEXT'.");
            }
        }
        public static void glFramebufferRenderbufferEXT(GLenum @target, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer) => glFramebufferRenderbufferEXTPtr(@target, @attachment, @renderbuffertarget, @renderbuffer);

        internal delegate void glGetFramebufferAttachmentParameterivEXTFunc(GLenum @target, GLenum @attachment, GLenum @pname, GLint * @params);
        internal static glGetFramebufferAttachmentParameterivEXTFunc glGetFramebufferAttachmentParameterivEXTPtr;
        internal static void loadGetFramebufferAttachmentParameterivEXT()
        {
            try
            {
                glGetFramebufferAttachmentParameterivEXTPtr = (glGetFramebufferAttachmentParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFramebufferAttachmentParameterivEXT"), typeof(glGetFramebufferAttachmentParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFramebufferAttachmentParameterivEXT'.");
            }
        }
        public static void glGetFramebufferAttachmentParameterivEXT(GLenum @target, GLenum @attachment, GLenum @pname, GLint * @params) => glGetFramebufferAttachmentParameterivEXTPtr(@target, @attachment, @pname, @params);

        internal delegate void glGenerateMipmapEXTFunc(GLenum @target);
        internal static glGenerateMipmapEXTFunc glGenerateMipmapEXTPtr;
        internal static void loadGenerateMipmapEXT()
        {
            try
            {
                glGenerateMipmapEXTPtr = (glGenerateMipmapEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenerateMipmapEXT"), typeof(glGenerateMipmapEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenerateMipmapEXT'.");
            }
        }
        public static void glGenerateMipmapEXT(GLenum @target) => glGenerateMipmapEXTPtr(@target);
        #endregion
    }
}
