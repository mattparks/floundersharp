using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_sRGB_decode
    {
        #region Interop
        static GL_EXT_texture_sRGB_decode()
        {
            Console.WriteLine("Initalising GL_EXT_texture_sRGB_decode interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_SRGB_DECODE_EXT = 0x8A48;
        public static UInt32 GL_DECODE_EXT = 0x8A49;
        public static UInt32 GL_SKIP_DECODE_EXT = 0x8A4A;
        #endregion

        #region Commands
        #endregion
    }
}
