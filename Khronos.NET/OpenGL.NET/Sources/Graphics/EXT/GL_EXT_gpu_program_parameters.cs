using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_gpu_program_parameters
    {
        #region Interop
        static GL_EXT_gpu_program_parameters()
        {
            Console.WriteLine("Initalising GL_EXT_gpu_program_parameters interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadProgramEnvParameters4fvEXT();
            loadProgramLocalParameters4fvEXT();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glProgramEnvParameters4fvEXTFunc(GLenum @target, GLuint @index, GLsizei @count, const GLfloat * @params);
        internal static glProgramEnvParameters4fvEXTFunc glProgramEnvParameters4fvEXTPtr;
        internal static void loadProgramEnvParameters4fvEXT()
        {
            try
            {
                glProgramEnvParameters4fvEXTPtr = (glProgramEnvParameters4fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameters4fvEXT"), typeof(glProgramEnvParameters4fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameters4fvEXT'.");
            }
        }
        public static void glProgramEnvParameters4fvEXT(GLenum @target, GLuint @index, GLsizei @count, const GLfloat * @params) => glProgramEnvParameters4fvEXTPtr(@target, @index, @count, @params);

        internal delegate void glProgramLocalParameters4fvEXTFunc(GLenum @target, GLuint @index, GLsizei @count, const GLfloat * @params);
        internal static glProgramLocalParameters4fvEXTFunc glProgramLocalParameters4fvEXTPtr;
        internal static void loadProgramLocalParameters4fvEXT()
        {
            try
            {
                glProgramLocalParameters4fvEXTPtr = (glProgramLocalParameters4fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameters4fvEXT"), typeof(glProgramLocalParameters4fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameters4fvEXT'.");
            }
        }
        public static void glProgramLocalParameters4fvEXT(GLenum @target, GLuint @index, GLsizei @count, const GLfloat * @params) => glProgramLocalParameters4fvEXTPtr(@target, @index, @count, @params);
        #endregion
    }
}
