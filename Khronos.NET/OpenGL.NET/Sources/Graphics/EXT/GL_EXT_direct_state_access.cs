using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_direct_state_access
    {
        #region Interop
        static GL_EXT_direct_state_access()
        {
            Console.WriteLine("Initalising GL_EXT_direct_state_access interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMatrixLoadfEXT();
            loadMatrixLoaddEXT();
            loadMatrixMultfEXT();
            loadMatrixMultdEXT();
            loadMatrixLoadIdentityEXT();
            loadMatrixRotatefEXT();
            loadMatrixRotatedEXT();
            loadMatrixScalefEXT();
            loadMatrixScaledEXT();
            loadMatrixTranslatefEXT();
            loadMatrixTranslatedEXT();
            loadMatrixFrustumEXT();
            loadMatrixOrthoEXT();
            loadMatrixPopEXT();
            loadMatrixPushEXT();
            loadClientAttribDefaultEXT();
            loadPushClientAttribDefaultEXT();
            loadTextureParameterfEXT();
            loadTextureParameterfvEXT();
            loadTextureParameteriEXT();
            loadTextureParameterivEXT();
            loadTextureImage1DEXT();
            loadTextureImage2DEXT();
            loadTextureSubImage1DEXT();
            loadTextureSubImage2DEXT();
            loadCopyTextureImage1DEXT();
            loadCopyTextureImage2DEXT();
            loadCopyTextureSubImage1DEXT();
            loadCopyTextureSubImage2DEXT();
            loadGetTextureImageEXT();
            loadGetTextureParameterfvEXT();
            loadGetTextureParameterivEXT();
            loadGetTextureLevelParameterfvEXT();
            loadGetTextureLevelParameterivEXT();
            loadTextureImage3DEXT();
            loadTextureSubImage3DEXT();
            loadCopyTextureSubImage3DEXT();
            loadBindMultiTextureEXT();
            loadMultiTexCoordPointerEXT();
            loadMultiTexEnvfEXT();
            loadMultiTexEnvfvEXT();
            loadMultiTexEnviEXT();
            loadMultiTexEnvivEXT();
            loadMultiTexGendEXT();
            loadMultiTexGendvEXT();
            loadMultiTexGenfEXT();
            loadMultiTexGenfvEXT();
            loadMultiTexGeniEXT();
            loadMultiTexGenivEXT();
            loadGetMultiTexEnvfvEXT();
            loadGetMultiTexEnvivEXT();
            loadGetMultiTexGendvEXT();
            loadGetMultiTexGenfvEXT();
            loadGetMultiTexGenivEXT();
            loadMultiTexParameteriEXT();
            loadMultiTexParameterivEXT();
            loadMultiTexParameterfEXT();
            loadMultiTexParameterfvEXT();
            loadMultiTexImage1DEXT();
            loadMultiTexImage2DEXT();
            loadMultiTexSubImage1DEXT();
            loadMultiTexSubImage2DEXT();
            loadCopyMultiTexImage1DEXT();
            loadCopyMultiTexImage2DEXT();
            loadCopyMultiTexSubImage1DEXT();
            loadCopyMultiTexSubImage2DEXT();
            loadGetMultiTexImageEXT();
            loadGetMultiTexParameterfvEXT();
            loadGetMultiTexParameterivEXT();
            loadGetMultiTexLevelParameterfvEXT();
            loadGetMultiTexLevelParameterivEXT();
            loadMultiTexImage3DEXT();
            loadMultiTexSubImage3DEXT();
            loadCopyMultiTexSubImage3DEXT();
            loadEnableClientStateIndexedEXT();
            loadDisableClientStateIndexedEXT();
            loadGetFloatIndexedvEXT();
            loadGetDoubleIndexedvEXT();
            loadGetPointerIndexedvEXT();
            loadEnableIndexedEXT();
            loadDisableIndexedEXT();
            loadIsEnabledIndexedEXT();
            loadGetIntegerIndexedvEXT();
            loadGetBooleanIndexedvEXT();
            loadCompressedTextureImage3DEXT();
            loadCompressedTextureImage2DEXT();
            loadCompressedTextureImage1DEXT();
            loadCompressedTextureSubImage3DEXT();
            loadCompressedTextureSubImage2DEXT();
            loadCompressedTextureSubImage1DEXT();
            loadGetCompressedTextureImageEXT();
            loadCompressedMultiTexImage3DEXT();
            loadCompressedMultiTexImage2DEXT();
            loadCompressedMultiTexImage1DEXT();
            loadCompressedMultiTexSubImage3DEXT();
            loadCompressedMultiTexSubImage2DEXT();
            loadCompressedMultiTexSubImage1DEXT();
            loadGetCompressedMultiTexImageEXT();
            loadMatrixLoadTransposefEXT();
            loadMatrixLoadTransposedEXT();
            loadMatrixMultTransposefEXT();
            loadMatrixMultTransposedEXT();
            loadNamedBufferDataEXT();
            loadNamedBufferSubDataEXT();
            loadMapNamedBufferEXT();
            loadUnmapNamedBufferEXT();
            loadGetNamedBufferParameterivEXT();
            loadGetNamedBufferPointervEXT();
            loadGetNamedBufferSubDataEXT();
            loadProgramUniform1fEXT();
            loadProgramUniform2fEXT();
            loadProgramUniform3fEXT();
            loadProgramUniform4fEXT();
            loadProgramUniform1iEXT();
            loadProgramUniform2iEXT();
            loadProgramUniform3iEXT();
            loadProgramUniform4iEXT();
            loadProgramUniform1fvEXT();
            loadProgramUniform2fvEXT();
            loadProgramUniform3fvEXT();
            loadProgramUniform4fvEXT();
            loadProgramUniform1ivEXT();
            loadProgramUniform2ivEXT();
            loadProgramUniform3ivEXT();
            loadProgramUniform4ivEXT();
            loadProgramUniformMatrix2fvEXT();
            loadProgramUniformMatrix3fvEXT();
            loadProgramUniformMatrix4fvEXT();
            loadProgramUniformMatrix2x3fvEXT();
            loadProgramUniformMatrix3x2fvEXT();
            loadProgramUniformMatrix2x4fvEXT();
            loadProgramUniformMatrix4x2fvEXT();
            loadProgramUniformMatrix3x4fvEXT();
            loadProgramUniformMatrix4x3fvEXT();
            loadTextureBufferEXT();
            loadMultiTexBufferEXT();
            loadTextureParameterIivEXT();
            loadTextureParameterIuivEXT();
            loadGetTextureParameterIivEXT();
            loadGetTextureParameterIuivEXT();
            loadMultiTexParameterIivEXT();
            loadMultiTexParameterIuivEXT();
            loadGetMultiTexParameterIivEXT();
            loadGetMultiTexParameterIuivEXT();
            loadProgramUniform1uiEXT();
            loadProgramUniform2uiEXT();
            loadProgramUniform3uiEXT();
            loadProgramUniform4uiEXT();
            loadProgramUniform1uivEXT();
            loadProgramUniform2uivEXT();
            loadProgramUniform3uivEXT();
            loadProgramUniform4uivEXT();
            loadNamedProgramLocalParameters4fvEXT();
            loadNamedProgramLocalParameterI4iEXT();
            loadNamedProgramLocalParameterI4ivEXT();
            loadNamedProgramLocalParametersI4ivEXT();
            loadNamedProgramLocalParameterI4uiEXT();
            loadNamedProgramLocalParameterI4uivEXT();
            loadNamedProgramLocalParametersI4uivEXT();
            loadGetNamedProgramLocalParameterIivEXT();
            loadGetNamedProgramLocalParameterIuivEXT();
            loadEnableClientStateiEXT();
            loadDisableClientStateiEXT();
            loadGetFloati_vEXT();
            loadGetDoublei_vEXT();
            loadGetPointeri_vEXT();
            loadNamedProgramStringEXT();
            loadNamedProgramLocalParameter4dEXT();
            loadNamedProgramLocalParameter4dvEXT();
            loadNamedProgramLocalParameter4fEXT();
            loadNamedProgramLocalParameter4fvEXT();
            loadGetNamedProgramLocalParameterdvEXT();
            loadGetNamedProgramLocalParameterfvEXT();
            loadGetNamedProgramivEXT();
            loadGetNamedProgramStringEXT();
            loadNamedRenderbufferStorageEXT();
            loadGetNamedRenderbufferParameterivEXT();
            loadNamedRenderbufferStorageMultisampleEXT();
            loadNamedRenderbufferStorageMultisampleCoverageEXT();
            loadCheckNamedFramebufferStatusEXT();
            loadNamedFramebufferTexture1DEXT();
            loadNamedFramebufferTexture2DEXT();
            loadNamedFramebufferTexture3DEXT();
            loadNamedFramebufferRenderbufferEXT();
            loadGetNamedFramebufferAttachmentParameterivEXT();
            loadGenerateTextureMipmapEXT();
            loadGenerateMultiTexMipmapEXT();
            loadFramebufferDrawBufferEXT();
            loadFramebufferDrawBuffersEXT();
            loadFramebufferReadBufferEXT();
            loadGetFramebufferParameterivEXT();
            loadNamedCopyBufferSubDataEXT();
            loadNamedFramebufferTextureEXT();
            loadNamedFramebufferTextureLayerEXT();
            loadNamedFramebufferTextureFaceEXT();
            loadTextureRenderbufferEXT();
            loadMultiTexRenderbufferEXT();
            loadVertexArrayVertexOffsetEXT();
            loadVertexArrayColorOffsetEXT();
            loadVertexArrayEdgeFlagOffsetEXT();
            loadVertexArrayIndexOffsetEXT();
            loadVertexArrayNormalOffsetEXT();
            loadVertexArrayTexCoordOffsetEXT();
            loadVertexArrayMultiTexCoordOffsetEXT();
            loadVertexArrayFogCoordOffsetEXT();
            loadVertexArraySecondaryColorOffsetEXT();
            loadVertexArrayVertexAttribOffsetEXT();
            loadVertexArrayVertexAttribIOffsetEXT();
            loadEnableVertexArrayEXT();
            loadDisableVertexArrayEXT();
            loadEnableVertexArrayAttribEXT();
            loadDisableVertexArrayAttribEXT();
            loadGetVertexArrayIntegervEXT();
            loadGetVertexArrayPointervEXT();
            loadGetVertexArrayIntegeri_vEXT();
            loadGetVertexArrayPointeri_vEXT();
            loadMapNamedBufferRangeEXT();
            loadFlushMappedNamedBufferRangeEXT();
            loadNamedBufferStorageEXT();
            loadClearNamedBufferDataEXT();
            loadClearNamedBufferSubDataEXT();
            loadNamedFramebufferParameteriEXT();
            loadGetNamedFramebufferParameterivEXT();
            loadProgramUniform1dEXT();
            loadProgramUniform2dEXT();
            loadProgramUniform3dEXT();
            loadProgramUniform4dEXT();
            loadProgramUniform1dvEXT();
            loadProgramUniform2dvEXT();
            loadProgramUniform3dvEXT();
            loadProgramUniform4dvEXT();
            loadProgramUniformMatrix2dvEXT();
            loadProgramUniformMatrix3dvEXT();
            loadProgramUniformMatrix4dvEXT();
            loadProgramUniformMatrix2x3dvEXT();
            loadProgramUniformMatrix2x4dvEXT();
            loadProgramUniformMatrix3x2dvEXT();
            loadProgramUniformMatrix3x4dvEXT();
            loadProgramUniformMatrix4x2dvEXT();
            loadProgramUniformMatrix4x3dvEXT();
            loadTextureBufferRangeEXT();
            loadTextureStorage1DEXT();
            loadTextureStorage2DEXT();
            loadTextureStorage3DEXT();
            loadTextureStorage2DMultisampleEXT();
            loadTextureStorage3DMultisampleEXT();
            loadVertexArrayBindVertexBufferEXT();
            loadVertexArrayVertexAttribFormatEXT();
            loadVertexArrayVertexAttribIFormatEXT();
            loadVertexArrayVertexAttribLFormatEXT();
            loadVertexArrayVertexAttribBindingEXT();
            loadVertexArrayVertexBindingDivisorEXT();
            loadVertexArrayVertexAttribLOffsetEXT();
            loadTexturePageCommitmentEXT();
            loadVertexArrayVertexAttribDivisorEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PROGRAM_MATRIX_EXT = 0x8E2D;
        public static UInt32 GL_TRANSPOSE_PROGRAM_MATRIX_EXT = 0x8E2E;
        public static UInt32 GL_PROGRAM_MATRIX_STACK_DEPTH_EXT = 0x8E2F;
        #endregion

        #region Commands
        internal delegate void glMatrixLoadfEXTFunc(GLenum @mode, const GLfloat * @m);
        internal static glMatrixLoadfEXTFunc glMatrixLoadfEXTPtr;
        internal static void loadMatrixLoadfEXT()
        {
            try
            {
                glMatrixLoadfEXTPtr = (glMatrixLoadfEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixLoadfEXT"), typeof(glMatrixLoadfEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixLoadfEXT'.");
            }
        }
        public static void glMatrixLoadfEXT(GLenum @mode, const GLfloat * @m) => glMatrixLoadfEXTPtr(@mode, @m);

        internal delegate void glMatrixLoaddEXTFunc(GLenum @mode, const GLdouble * @m);
        internal static glMatrixLoaddEXTFunc glMatrixLoaddEXTPtr;
        internal static void loadMatrixLoaddEXT()
        {
            try
            {
                glMatrixLoaddEXTPtr = (glMatrixLoaddEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixLoaddEXT"), typeof(glMatrixLoaddEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixLoaddEXT'.");
            }
        }
        public static void glMatrixLoaddEXT(GLenum @mode, const GLdouble * @m) => glMatrixLoaddEXTPtr(@mode, @m);

        internal delegate void glMatrixMultfEXTFunc(GLenum @mode, const GLfloat * @m);
        internal static glMatrixMultfEXTFunc glMatrixMultfEXTPtr;
        internal static void loadMatrixMultfEXT()
        {
            try
            {
                glMatrixMultfEXTPtr = (glMatrixMultfEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixMultfEXT"), typeof(glMatrixMultfEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixMultfEXT'.");
            }
        }
        public static void glMatrixMultfEXT(GLenum @mode, const GLfloat * @m) => glMatrixMultfEXTPtr(@mode, @m);

        internal delegate void glMatrixMultdEXTFunc(GLenum @mode, const GLdouble * @m);
        internal static glMatrixMultdEXTFunc glMatrixMultdEXTPtr;
        internal static void loadMatrixMultdEXT()
        {
            try
            {
                glMatrixMultdEXTPtr = (glMatrixMultdEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixMultdEXT"), typeof(glMatrixMultdEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixMultdEXT'.");
            }
        }
        public static void glMatrixMultdEXT(GLenum @mode, const GLdouble * @m) => glMatrixMultdEXTPtr(@mode, @m);

        internal delegate void glMatrixLoadIdentityEXTFunc(GLenum @mode);
        internal static glMatrixLoadIdentityEXTFunc glMatrixLoadIdentityEXTPtr;
        internal static void loadMatrixLoadIdentityEXT()
        {
            try
            {
                glMatrixLoadIdentityEXTPtr = (glMatrixLoadIdentityEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixLoadIdentityEXT"), typeof(glMatrixLoadIdentityEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixLoadIdentityEXT'.");
            }
        }
        public static void glMatrixLoadIdentityEXT(GLenum @mode) => glMatrixLoadIdentityEXTPtr(@mode);

        internal delegate void glMatrixRotatefEXTFunc(GLenum @mode, GLfloat @angle, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glMatrixRotatefEXTFunc glMatrixRotatefEXTPtr;
        internal static void loadMatrixRotatefEXT()
        {
            try
            {
                glMatrixRotatefEXTPtr = (glMatrixRotatefEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixRotatefEXT"), typeof(glMatrixRotatefEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixRotatefEXT'.");
            }
        }
        public static void glMatrixRotatefEXT(GLenum @mode, GLfloat @angle, GLfloat @x, GLfloat @y, GLfloat @z) => glMatrixRotatefEXTPtr(@mode, @angle, @x, @y, @z);

        internal delegate void glMatrixRotatedEXTFunc(GLenum @mode, GLdouble @angle, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glMatrixRotatedEXTFunc glMatrixRotatedEXTPtr;
        internal static void loadMatrixRotatedEXT()
        {
            try
            {
                glMatrixRotatedEXTPtr = (glMatrixRotatedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixRotatedEXT"), typeof(glMatrixRotatedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixRotatedEXT'.");
            }
        }
        public static void glMatrixRotatedEXT(GLenum @mode, GLdouble @angle, GLdouble @x, GLdouble @y, GLdouble @z) => glMatrixRotatedEXTPtr(@mode, @angle, @x, @y, @z);

        internal delegate void glMatrixScalefEXTFunc(GLenum @mode, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glMatrixScalefEXTFunc glMatrixScalefEXTPtr;
        internal static void loadMatrixScalefEXT()
        {
            try
            {
                glMatrixScalefEXTPtr = (glMatrixScalefEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixScalefEXT"), typeof(glMatrixScalefEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixScalefEXT'.");
            }
        }
        public static void glMatrixScalefEXT(GLenum @mode, GLfloat @x, GLfloat @y, GLfloat @z) => glMatrixScalefEXTPtr(@mode, @x, @y, @z);

        internal delegate void glMatrixScaledEXTFunc(GLenum @mode, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glMatrixScaledEXTFunc glMatrixScaledEXTPtr;
        internal static void loadMatrixScaledEXT()
        {
            try
            {
                glMatrixScaledEXTPtr = (glMatrixScaledEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixScaledEXT"), typeof(glMatrixScaledEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixScaledEXT'.");
            }
        }
        public static void glMatrixScaledEXT(GLenum @mode, GLdouble @x, GLdouble @y, GLdouble @z) => glMatrixScaledEXTPtr(@mode, @x, @y, @z);

        internal delegate void glMatrixTranslatefEXTFunc(GLenum @mode, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glMatrixTranslatefEXTFunc glMatrixTranslatefEXTPtr;
        internal static void loadMatrixTranslatefEXT()
        {
            try
            {
                glMatrixTranslatefEXTPtr = (glMatrixTranslatefEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixTranslatefEXT"), typeof(glMatrixTranslatefEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixTranslatefEXT'.");
            }
        }
        public static void glMatrixTranslatefEXT(GLenum @mode, GLfloat @x, GLfloat @y, GLfloat @z) => glMatrixTranslatefEXTPtr(@mode, @x, @y, @z);

        internal delegate void glMatrixTranslatedEXTFunc(GLenum @mode, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glMatrixTranslatedEXTFunc glMatrixTranslatedEXTPtr;
        internal static void loadMatrixTranslatedEXT()
        {
            try
            {
                glMatrixTranslatedEXTPtr = (glMatrixTranslatedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixTranslatedEXT"), typeof(glMatrixTranslatedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixTranslatedEXT'.");
            }
        }
        public static void glMatrixTranslatedEXT(GLenum @mode, GLdouble @x, GLdouble @y, GLdouble @z) => glMatrixTranslatedEXTPtr(@mode, @x, @y, @z);

        internal delegate void glMatrixFrustumEXTFunc(GLenum @mode, GLdouble @left, GLdouble @right, GLdouble @bottom, GLdouble @top, GLdouble @zNear, GLdouble @zFar);
        internal static glMatrixFrustumEXTFunc glMatrixFrustumEXTPtr;
        internal static void loadMatrixFrustumEXT()
        {
            try
            {
                glMatrixFrustumEXTPtr = (glMatrixFrustumEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixFrustumEXT"), typeof(glMatrixFrustumEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixFrustumEXT'.");
            }
        }
        public static void glMatrixFrustumEXT(GLenum @mode, GLdouble @left, GLdouble @right, GLdouble @bottom, GLdouble @top, GLdouble @zNear, GLdouble @zFar) => glMatrixFrustumEXTPtr(@mode, @left, @right, @bottom, @top, @zNear, @zFar);

        internal delegate void glMatrixOrthoEXTFunc(GLenum @mode, GLdouble @left, GLdouble @right, GLdouble @bottom, GLdouble @top, GLdouble @zNear, GLdouble @zFar);
        internal static glMatrixOrthoEXTFunc glMatrixOrthoEXTPtr;
        internal static void loadMatrixOrthoEXT()
        {
            try
            {
                glMatrixOrthoEXTPtr = (glMatrixOrthoEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixOrthoEXT"), typeof(glMatrixOrthoEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixOrthoEXT'.");
            }
        }
        public static void glMatrixOrthoEXT(GLenum @mode, GLdouble @left, GLdouble @right, GLdouble @bottom, GLdouble @top, GLdouble @zNear, GLdouble @zFar) => glMatrixOrthoEXTPtr(@mode, @left, @right, @bottom, @top, @zNear, @zFar);

        internal delegate void glMatrixPopEXTFunc(GLenum @mode);
        internal static glMatrixPopEXTFunc glMatrixPopEXTPtr;
        internal static void loadMatrixPopEXT()
        {
            try
            {
                glMatrixPopEXTPtr = (glMatrixPopEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixPopEXT"), typeof(glMatrixPopEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixPopEXT'.");
            }
        }
        public static void glMatrixPopEXT(GLenum @mode) => glMatrixPopEXTPtr(@mode);

        internal delegate void glMatrixPushEXTFunc(GLenum @mode);
        internal static glMatrixPushEXTFunc glMatrixPushEXTPtr;
        internal static void loadMatrixPushEXT()
        {
            try
            {
                glMatrixPushEXTPtr = (glMatrixPushEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixPushEXT"), typeof(glMatrixPushEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixPushEXT'.");
            }
        }
        public static void glMatrixPushEXT(GLenum @mode) => glMatrixPushEXTPtr(@mode);

        internal delegate void glClientAttribDefaultEXTFunc(GLbitfield @mask);
        internal static glClientAttribDefaultEXTFunc glClientAttribDefaultEXTPtr;
        internal static void loadClientAttribDefaultEXT()
        {
            try
            {
                glClientAttribDefaultEXTPtr = (glClientAttribDefaultEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClientAttribDefaultEXT"), typeof(glClientAttribDefaultEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClientAttribDefaultEXT'.");
            }
        }
        public static void glClientAttribDefaultEXT(GLbitfield @mask) => glClientAttribDefaultEXTPtr(@mask);

        internal delegate void glPushClientAttribDefaultEXTFunc(GLbitfield @mask);
        internal static glPushClientAttribDefaultEXTFunc glPushClientAttribDefaultEXTPtr;
        internal static void loadPushClientAttribDefaultEXT()
        {
            try
            {
                glPushClientAttribDefaultEXTPtr = (glPushClientAttribDefaultEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPushClientAttribDefaultEXT"), typeof(glPushClientAttribDefaultEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPushClientAttribDefaultEXT'.");
            }
        }
        public static void glPushClientAttribDefaultEXT(GLbitfield @mask) => glPushClientAttribDefaultEXTPtr(@mask);

        internal delegate void glTextureParameterfEXTFunc(GLuint @texture, GLenum @target, GLenum @pname, GLfloat @param);
        internal static glTextureParameterfEXTFunc glTextureParameterfEXTPtr;
        internal static void loadTextureParameterfEXT()
        {
            try
            {
                glTextureParameterfEXTPtr = (glTextureParameterfEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureParameterfEXT"), typeof(glTextureParameterfEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureParameterfEXT'.");
            }
        }
        public static void glTextureParameterfEXT(GLuint @texture, GLenum @target, GLenum @pname, GLfloat @param) => glTextureParameterfEXTPtr(@texture, @target, @pname, @param);

        internal delegate void glTextureParameterfvEXTFunc(GLuint @texture, GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glTextureParameterfvEXTFunc glTextureParameterfvEXTPtr;
        internal static void loadTextureParameterfvEXT()
        {
            try
            {
                glTextureParameterfvEXTPtr = (glTextureParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureParameterfvEXT"), typeof(glTextureParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureParameterfvEXT'.");
            }
        }
        public static void glTextureParameterfvEXT(GLuint @texture, GLenum @target, GLenum @pname, const GLfloat * @params) => glTextureParameterfvEXTPtr(@texture, @target, @pname, @params);

        internal delegate void glTextureParameteriEXTFunc(GLuint @texture, GLenum @target, GLenum @pname, GLint @param);
        internal static glTextureParameteriEXTFunc glTextureParameteriEXTPtr;
        internal static void loadTextureParameteriEXT()
        {
            try
            {
                glTextureParameteriEXTPtr = (glTextureParameteriEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureParameteriEXT"), typeof(glTextureParameteriEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureParameteriEXT'.");
            }
        }
        public static void glTextureParameteriEXT(GLuint @texture, GLenum @target, GLenum @pname, GLint @param) => glTextureParameteriEXTPtr(@texture, @target, @pname, @param);

        internal delegate void glTextureParameterivEXTFunc(GLuint @texture, GLenum @target, GLenum @pname, const GLint * @params);
        internal static glTextureParameterivEXTFunc glTextureParameterivEXTPtr;
        internal static void loadTextureParameterivEXT()
        {
            try
            {
                glTextureParameterivEXTPtr = (glTextureParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureParameterivEXT"), typeof(glTextureParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureParameterivEXT'.");
            }
        }
        public static void glTextureParameterivEXT(GLuint @texture, GLenum @target, GLenum @pname, const GLint * @params) => glTextureParameterivEXTPtr(@texture, @target, @pname, @params);

        internal delegate void glTextureImage1DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTextureImage1DEXTFunc glTextureImage1DEXTPtr;
        internal static void loadTextureImage1DEXT()
        {
            try
            {
                glTextureImage1DEXTPtr = (glTextureImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureImage1DEXT"), typeof(glTextureImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureImage1DEXT'.");
            }
        }
        public static void glTextureImage1DEXT(GLuint @texture, GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glTextureImage1DEXTPtr(@texture, @target, @level, @internalformat, @width, @border, @format, @type, @pixels);

        internal delegate void glTextureImage2DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTextureImage2DEXTFunc glTextureImage2DEXTPtr;
        internal static void loadTextureImage2DEXT()
        {
            try
            {
                glTextureImage2DEXTPtr = (glTextureImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureImage2DEXT"), typeof(glTextureImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureImage2DEXT'.");
            }
        }
        public static void glTextureImage2DEXT(GLuint @texture, GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glTextureImage2DEXTPtr(@texture, @target, @level, @internalformat, @width, @height, @border, @format, @type, @pixels);

        internal delegate void glTextureSubImage1DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTextureSubImage1DEXTFunc glTextureSubImage1DEXTPtr;
        internal static void loadTextureSubImage1DEXT()
        {
            try
            {
                glTextureSubImage1DEXTPtr = (glTextureSubImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureSubImage1DEXT"), typeof(glTextureSubImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureSubImage1DEXT'.");
            }
        }
        public static void glTextureSubImage1DEXT(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLenum @type, const void * @pixels) => glTextureSubImage1DEXTPtr(@texture, @target, @level, @xoffset, @width, @format, @type, @pixels);

        internal delegate void glTextureSubImage2DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTextureSubImage2DEXTFunc glTextureSubImage2DEXTPtr;
        internal static void loadTextureSubImage2DEXT()
        {
            try
            {
                glTextureSubImage2DEXTPtr = (glTextureSubImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureSubImage2DEXT"), typeof(glTextureSubImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureSubImage2DEXT'.");
            }
        }
        public static void glTextureSubImage2DEXT(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels) => glTextureSubImage2DEXTPtr(@texture, @target, @level, @xoffset, @yoffset, @width, @height, @format, @type, @pixels);

        internal delegate void glCopyTextureImage1DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLint @border);
        internal static glCopyTextureImage1DEXTFunc glCopyTextureImage1DEXTPtr;
        internal static void loadCopyTextureImage1DEXT()
        {
            try
            {
                glCopyTextureImage1DEXTPtr = (glCopyTextureImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTextureImage1DEXT"), typeof(glCopyTextureImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTextureImage1DEXT'.");
            }
        }
        public static void glCopyTextureImage1DEXT(GLuint @texture, GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLint @border) => glCopyTextureImage1DEXTPtr(@texture, @target, @level, @internalformat, @x, @y, @width, @border);

        internal delegate void glCopyTextureImage2DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLint @border);
        internal static glCopyTextureImage2DEXTFunc glCopyTextureImage2DEXTPtr;
        internal static void loadCopyTextureImage2DEXT()
        {
            try
            {
                glCopyTextureImage2DEXTPtr = (glCopyTextureImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTextureImage2DEXT"), typeof(glCopyTextureImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTextureImage2DEXT'.");
            }
        }
        public static void glCopyTextureImage2DEXT(GLuint @texture, GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLint @border) => glCopyTextureImage2DEXTPtr(@texture, @target, @level, @internalformat, @x, @y, @width, @height, @border);

        internal delegate void glCopyTextureSubImage1DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @x, GLint @y, GLsizei @width);
        internal static glCopyTextureSubImage1DEXTFunc glCopyTextureSubImage1DEXTPtr;
        internal static void loadCopyTextureSubImage1DEXT()
        {
            try
            {
                glCopyTextureSubImage1DEXTPtr = (glCopyTextureSubImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTextureSubImage1DEXT"), typeof(glCopyTextureSubImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTextureSubImage1DEXT'.");
            }
        }
        public static void glCopyTextureSubImage1DEXT(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @x, GLint @y, GLsizei @width) => glCopyTextureSubImage1DEXTPtr(@texture, @target, @level, @xoffset, @x, @y, @width);

        internal delegate void glCopyTextureSubImage2DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyTextureSubImage2DEXTFunc glCopyTextureSubImage2DEXTPtr;
        internal static void loadCopyTextureSubImage2DEXT()
        {
            try
            {
                glCopyTextureSubImage2DEXTPtr = (glCopyTextureSubImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTextureSubImage2DEXT"), typeof(glCopyTextureSubImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTextureSubImage2DEXT'.");
            }
        }
        public static void glCopyTextureSubImage2DEXT(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyTextureSubImage2DEXTPtr(@texture, @target, @level, @xoffset, @yoffset, @x, @y, @width, @height);

        internal delegate void glGetTextureImageEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLenum @format, GLenum @type, void * @pixels);
        internal static glGetTextureImageEXTFunc glGetTextureImageEXTPtr;
        internal static void loadGetTextureImageEXT()
        {
            try
            {
                glGetTextureImageEXTPtr = (glGetTextureImageEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureImageEXT"), typeof(glGetTextureImageEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureImageEXT'.");
            }
        }
        public static void glGetTextureImageEXT(GLuint @texture, GLenum @target, GLint @level, GLenum @format, GLenum @type, void * @pixels) => glGetTextureImageEXTPtr(@texture, @target, @level, @format, @type, @pixels);

        internal delegate void glGetTextureParameterfvEXTFunc(GLuint @texture, GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetTextureParameterfvEXTFunc glGetTextureParameterfvEXTPtr;
        internal static void loadGetTextureParameterfvEXT()
        {
            try
            {
                glGetTextureParameterfvEXTPtr = (glGetTextureParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureParameterfvEXT"), typeof(glGetTextureParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureParameterfvEXT'.");
            }
        }
        public static void glGetTextureParameterfvEXT(GLuint @texture, GLenum @target, GLenum @pname, GLfloat * @params) => glGetTextureParameterfvEXTPtr(@texture, @target, @pname, @params);

        internal delegate void glGetTextureParameterivEXTFunc(GLuint @texture, GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetTextureParameterivEXTFunc glGetTextureParameterivEXTPtr;
        internal static void loadGetTextureParameterivEXT()
        {
            try
            {
                glGetTextureParameterivEXTPtr = (glGetTextureParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureParameterivEXT"), typeof(glGetTextureParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureParameterivEXT'.");
            }
        }
        public static void glGetTextureParameterivEXT(GLuint @texture, GLenum @target, GLenum @pname, GLint * @params) => glGetTextureParameterivEXTPtr(@texture, @target, @pname, @params);

        internal delegate void glGetTextureLevelParameterfvEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLenum @pname, GLfloat * @params);
        internal static glGetTextureLevelParameterfvEXTFunc glGetTextureLevelParameterfvEXTPtr;
        internal static void loadGetTextureLevelParameterfvEXT()
        {
            try
            {
                glGetTextureLevelParameterfvEXTPtr = (glGetTextureLevelParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureLevelParameterfvEXT"), typeof(glGetTextureLevelParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureLevelParameterfvEXT'.");
            }
        }
        public static void glGetTextureLevelParameterfvEXT(GLuint @texture, GLenum @target, GLint @level, GLenum @pname, GLfloat * @params) => glGetTextureLevelParameterfvEXTPtr(@texture, @target, @level, @pname, @params);

        internal delegate void glGetTextureLevelParameterivEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLenum @pname, GLint * @params);
        internal static glGetTextureLevelParameterivEXTFunc glGetTextureLevelParameterivEXTPtr;
        internal static void loadGetTextureLevelParameterivEXT()
        {
            try
            {
                glGetTextureLevelParameterivEXTPtr = (glGetTextureLevelParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureLevelParameterivEXT"), typeof(glGetTextureLevelParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureLevelParameterivEXT'.");
            }
        }
        public static void glGetTextureLevelParameterivEXT(GLuint @texture, GLenum @target, GLint @level, GLenum @pname, GLint * @params) => glGetTextureLevelParameterivEXTPtr(@texture, @target, @level, @pname, @params);

        internal delegate void glTextureImage3DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTextureImage3DEXTFunc glTextureImage3DEXTPtr;
        internal static void loadTextureImage3DEXT()
        {
            try
            {
                glTextureImage3DEXTPtr = (glTextureImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureImage3DEXT"), typeof(glTextureImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureImage3DEXT'.");
            }
        }
        public static void glTextureImage3DEXT(GLuint @texture, GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glTextureImage3DEXTPtr(@texture, @target, @level, @internalformat, @width, @height, @depth, @border, @format, @type, @pixels);

        internal delegate void glTextureSubImage3DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTextureSubImage3DEXTFunc glTextureSubImage3DEXTPtr;
        internal static void loadTextureSubImage3DEXT()
        {
            try
            {
                glTextureSubImage3DEXTPtr = (glTextureSubImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureSubImage3DEXT"), typeof(glTextureSubImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureSubImage3DEXT'.");
            }
        }
        public static void glTextureSubImage3DEXT(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels) => glTextureSubImage3DEXTPtr(@texture, @target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @type, @pixels);

        internal delegate void glCopyTextureSubImage3DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyTextureSubImage3DEXTFunc glCopyTextureSubImage3DEXTPtr;
        internal static void loadCopyTextureSubImage3DEXT()
        {
            try
            {
                glCopyTextureSubImage3DEXTPtr = (glCopyTextureSubImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTextureSubImage3DEXT"), typeof(glCopyTextureSubImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTextureSubImage3DEXT'.");
            }
        }
        public static void glCopyTextureSubImage3DEXT(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyTextureSubImage3DEXTPtr(@texture, @target, @level, @xoffset, @yoffset, @zoffset, @x, @y, @width, @height);

        internal delegate void glBindMultiTextureEXTFunc(GLenum @texunit, GLenum @target, GLuint @texture);
        internal static glBindMultiTextureEXTFunc glBindMultiTextureEXTPtr;
        internal static void loadBindMultiTextureEXT()
        {
            try
            {
                glBindMultiTextureEXTPtr = (glBindMultiTextureEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindMultiTextureEXT"), typeof(glBindMultiTextureEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindMultiTextureEXT'.");
            }
        }
        public static void glBindMultiTextureEXT(GLenum @texunit, GLenum @target, GLuint @texture) => glBindMultiTextureEXTPtr(@texunit, @target, @texture);

        internal delegate void glMultiTexCoordPointerEXTFunc(GLenum @texunit, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glMultiTexCoordPointerEXTFunc glMultiTexCoordPointerEXTPtr;
        internal static void loadMultiTexCoordPointerEXT()
        {
            try
            {
                glMultiTexCoordPointerEXTPtr = (glMultiTexCoordPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordPointerEXT"), typeof(glMultiTexCoordPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordPointerEXT'.");
            }
        }
        public static void glMultiTexCoordPointerEXT(GLenum @texunit, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glMultiTexCoordPointerEXTPtr(@texunit, @size, @type, @stride, @pointer);

        internal delegate void glMultiTexEnvfEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, GLfloat @param);
        internal static glMultiTexEnvfEXTFunc glMultiTexEnvfEXTPtr;
        internal static void loadMultiTexEnvfEXT()
        {
            try
            {
                glMultiTexEnvfEXTPtr = (glMultiTexEnvfEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexEnvfEXT"), typeof(glMultiTexEnvfEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexEnvfEXT'.");
            }
        }
        public static void glMultiTexEnvfEXT(GLenum @texunit, GLenum @target, GLenum @pname, GLfloat @param) => glMultiTexEnvfEXTPtr(@texunit, @target, @pname, @param);

        internal delegate void glMultiTexEnvfvEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glMultiTexEnvfvEXTFunc glMultiTexEnvfvEXTPtr;
        internal static void loadMultiTexEnvfvEXT()
        {
            try
            {
                glMultiTexEnvfvEXTPtr = (glMultiTexEnvfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexEnvfvEXT"), typeof(glMultiTexEnvfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexEnvfvEXT'.");
            }
        }
        public static void glMultiTexEnvfvEXT(GLenum @texunit, GLenum @target, GLenum @pname, const GLfloat * @params) => glMultiTexEnvfvEXTPtr(@texunit, @target, @pname, @params);

        internal delegate void glMultiTexEnviEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, GLint @param);
        internal static glMultiTexEnviEXTFunc glMultiTexEnviEXTPtr;
        internal static void loadMultiTexEnviEXT()
        {
            try
            {
                glMultiTexEnviEXTPtr = (glMultiTexEnviEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexEnviEXT"), typeof(glMultiTexEnviEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexEnviEXT'.");
            }
        }
        public static void glMultiTexEnviEXT(GLenum @texunit, GLenum @target, GLenum @pname, GLint @param) => glMultiTexEnviEXTPtr(@texunit, @target, @pname, @param);

        internal delegate void glMultiTexEnvivEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, const GLint * @params);
        internal static glMultiTexEnvivEXTFunc glMultiTexEnvivEXTPtr;
        internal static void loadMultiTexEnvivEXT()
        {
            try
            {
                glMultiTexEnvivEXTPtr = (glMultiTexEnvivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexEnvivEXT"), typeof(glMultiTexEnvivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexEnvivEXT'.");
            }
        }
        public static void glMultiTexEnvivEXT(GLenum @texunit, GLenum @target, GLenum @pname, const GLint * @params) => glMultiTexEnvivEXTPtr(@texunit, @target, @pname, @params);

        internal delegate void glMultiTexGendEXTFunc(GLenum @texunit, GLenum @coord, GLenum @pname, GLdouble @param);
        internal static glMultiTexGendEXTFunc glMultiTexGendEXTPtr;
        internal static void loadMultiTexGendEXT()
        {
            try
            {
                glMultiTexGendEXTPtr = (glMultiTexGendEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexGendEXT"), typeof(glMultiTexGendEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexGendEXT'.");
            }
        }
        public static void glMultiTexGendEXT(GLenum @texunit, GLenum @coord, GLenum @pname, GLdouble @param) => glMultiTexGendEXTPtr(@texunit, @coord, @pname, @param);

        internal delegate void glMultiTexGendvEXTFunc(GLenum @texunit, GLenum @coord, GLenum @pname, const GLdouble * @params);
        internal static glMultiTexGendvEXTFunc glMultiTexGendvEXTPtr;
        internal static void loadMultiTexGendvEXT()
        {
            try
            {
                glMultiTexGendvEXTPtr = (glMultiTexGendvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexGendvEXT"), typeof(glMultiTexGendvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexGendvEXT'.");
            }
        }
        public static void glMultiTexGendvEXT(GLenum @texunit, GLenum @coord, GLenum @pname, const GLdouble * @params) => glMultiTexGendvEXTPtr(@texunit, @coord, @pname, @params);

        internal delegate void glMultiTexGenfEXTFunc(GLenum @texunit, GLenum @coord, GLenum @pname, GLfloat @param);
        internal static glMultiTexGenfEXTFunc glMultiTexGenfEXTPtr;
        internal static void loadMultiTexGenfEXT()
        {
            try
            {
                glMultiTexGenfEXTPtr = (glMultiTexGenfEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexGenfEXT"), typeof(glMultiTexGenfEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexGenfEXT'.");
            }
        }
        public static void glMultiTexGenfEXT(GLenum @texunit, GLenum @coord, GLenum @pname, GLfloat @param) => glMultiTexGenfEXTPtr(@texunit, @coord, @pname, @param);

        internal delegate void glMultiTexGenfvEXTFunc(GLenum @texunit, GLenum @coord, GLenum @pname, const GLfloat * @params);
        internal static glMultiTexGenfvEXTFunc glMultiTexGenfvEXTPtr;
        internal static void loadMultiTexGenfvEXT()
        {
            try
            {
                glMultiTexGenfvEXTPtr = (glMultiTexGenfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexGenfvEXT"), typeof(glMultiTexGenfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexGenfvEXT'.");
            }
        }
        public static void glMultiTexGenfvEXT(GLenum @texunit, GLenum @coord, GLenum @pname, const GLfloat * @params) => glMultiTexGenfvEXTPtr(@texunit, @coord, @pname, @params);

        internal delegate void glMultiTexGeniEXTFunc(GLenum @texunit, GLenum @coord, GLenum @pname, GLint @param);
        internal static glMultiTexGeniEXTFunc glMultiTexGeniEXTPtr;
        internal static void loadMultiTexGeniEXT()
        {
            try
            {
                glMultiTexGeniEXTPtr = (glMultiTexGeniEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexGeniEXT"), typeof(glMultiTexGeniEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexGeniEXT'.");
            }
        }
        public static void glMultiTexGeniEXT(GLenum @texunit, GLenum @coord, GLenum @pname, GLint @param) => glMultiTexGeniEXTPtr(@texunit, @coord, @pname, @param);

        internal delegate void glMultiTexGenivEXTFunc(GLenum @texunit, GLenum @coord, GLenum @pname, const GLint * @params);
        internal static glMultiTexGenivEXTFunc glMultiTexGenivEXTPtr;
        internal static void loadMultiTexGenivEXT()
        {
            try
            {
                glMultiTexGenivEXTPtr = (glMultiTexGenivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexGenivEXT"), typeof(glMultiTexGenivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexGenivEXT'.");
            }
        }
        public static void glMultiTexGenivEXT(GLenum @texunit, GLenum @coord, GLenum @pname, const GLint * @params) => glMultiTexGenivEXTPtr(@texunit, @coord, @pname, @params);

        internal delegate void glGetMultiTexEnvfvEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetMultiTexEnvfvEXTFunc glGetMultiTexEnvfvEXTPtr;
        internal static void loadGetMultiTexEnvfvEXT()
        {
            try
            {
                glGetMultiTexEnvfvEXTPtr = (glGetMultiTexEnvfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultiTexEnvfvEXT"), typeof(glGetMultiTexEnvfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultiTexEnvfvEXT'.");
            }
        }
        public static void glGetMultiTexEnvfvEXT(GLenum @texunit, GLenum @target, GLenum @pname, GLfloat * @params) => glGetMultiTexEnvfvEXTPtr(@texunit, @target, @pname, @params);

        internal delegate void glGetMultiTexEnvivEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetMultiTexEnvivEXTFunc glGetMultiTexEnvivEXTPtr;
        internal static void loadGetMultiTexEnvivEXT()
        {
            try
            {
                glGetMultiTexEnvivEXTPtr = (glGetMultiTexEnvivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultiTexEnvivEXT"), typeof(glGetMultiTexEnvivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultiTexEnvivEXT'.");
            }
        }
        public static void glGetMultiTexEnvivEXT(GLenum @texunit, GLenum @target, GLenum @pname, GLint * @params) => glGetMultiTexEnvivEXTPtr(@texunit, @target, @pname, @params);

        internal delegate void glGetMultiTexGendvEXTFunc(GLenum @texunit, GLenum @coord, GLenum @pname, GLdouble * @params);
        internal static glGetMultiTexGendvEXTFunc glGetMultiTexGendvEXTPtr;
        internal static void loadGetMultiTexGendvEXT()
        {
            try
            {
                glGetMultiTexGendvEXTPtr = (glGetMultiTexGendvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultiTexGendvEXT"), typeof(glGetMultiTexGendvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultiTexGendvEXT'.");
            }
        }
        public static void glGetMultiTexGendvEXT(GLenum @texunit, GLenum @coord, GLenum @pname, GLdouble * @params) => glGetMultiTexGendvEXTPtr(@texunit, @coord, @pname, @params);

        internal delegate void glGetMultiTexGenfvEXTFunc(GLenum @texunit, GLenum @coord, GLenum @pname, GLfloat * @params);
        internal static glGetMultiTexGenfvEXTFunc glGetMultiTexGenfvEXTPtr;
        internal static void loadGetMultiTexGenfvEXT()
        {
            try
            {
                glGetMultiTexGenfvEXTPtr = (glGetMultiTexGenfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultiTexGenfvEXT"), typeof(glGetMultiTexGenfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultiTexGenfvEXT'.");
            }
        }
        public static void glGetMultiTexGenfvEXT(GLenum @texunit, GLenum @coord, GLenum @pname, GLfloat * @params) => glGetMultiTexGenfvEXTPtr(@texunit, @coord, @pname, @params);

        internal delegate void glGetMultiTexGenivEXTFunc(GLenum @texunit, GLenum @coord, GLenum @pname, GLint * @params);
        internal static glGetMultiTexGenivEXTFunc glGetMultiTexGenivEXTPtr;
        internal static void loadGetMultiTexGenivEXT()
        {
            try
            {
                glGetMultiTexGenivEXTPtr = (glGetMultiTexGenivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultiTexGenivEXT"), typeof(glGetMultiTexGenivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultiTexGenivEXT'.");
            }
        }
        public static void glGetMultiTexGenivEXT(GLenum @texunit, GLenum @coord, GLenum @pname, GLint * @params) => glGetMultiTexGenivEXTPtr(@texunit, @coord, @pname, @params);

        internal delegate void glMultiTexParameteriEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, GLint @param);
        internal static glMultiTexParameteriEXTFunc glMultiTexParameteriEXTPtr;
        internal static void loadMultiTexParameteriEXT()
        {
            try
            {
                glMultiTexParameteriEXTPtr = (glMultiTexParameteriEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexParameteriEXT"), typeof(glMultiTexParameteriEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexParameteriEXT'.");
            }
        }
        public static void glMultiTexParameteriEXT(GLenum @texunit, GLenum @target, GLenum @pname, GLint @param) => glMultiTexParameteriEXTPtr(@texunit, @target, @pname, @param);

        internal delegate void glMultiTexParameterivEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, const GLint * @params);
        internal static glMultiTexParameterivEXTFunc glMultiTexParameterivEXTPtr;
        internal static void loadMultiTexParameterivEXT()
        {
            try
            {
                glMultiTexParameterivEXTPtr = (glMultiTexParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexParameterivEXT"), typeof(glMultiTexParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexParameterivEXT'.");
            }
        }
        public static void glMultiTexParameterivEXT(GLenum @texunit, GLenum @target, GLenum @pname, const GLint * @params) => glMultiTexParameterivEXTPtr(@texunit, @target, @pname, @params);

        internal delegate void glMultiTexParameterfEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, GLfloat @param);
        internal static glMultiTexParameterfEXTFunc glMultiTexParameterfEXTPtr;
        internal static void loadMultiTexParameterfEXT()
        {
            try
            {
                glMultiTexParameterfEXTPtr = (glMultiTexParameterfEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexParameterfEXT"), typeof(glMultiTexParameterfEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexParameterfEXT'.");
            }
        }
        public static void glMultiTexParameterfEXT(GLenum @texunit, GLenum @target, GLenum @pname, GLfloat @param) => glMultiTexParameterfEXTPtr(@texunit, @target, @pname, @param);

        internal delegate void glMultiTexParameterfvEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glMultiTexParameterfvEXTFunc glMultiTexParameterfvEXTPtr;
        internal static void loadMultiTexParameterfvEXT()
        {
            try
            {
                glMultiTexParameterfvEXTPtr = (glMultiTexParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexParameterfvEXT"), typeof(glMultiTexParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexParameterfvEXT'.");
            }
        }
        public static void glMultiTexParameterfvEXT(GLenum @texunit, GLenum @target, GLenum @pname, const GLfloat * @params) => glMultiTexParameterfvEXTPtr(@texunit, @target, @pname, @params);

        internal delegate void glMultiTexImage1DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glMultiTexImage1DEXTFunc glMultiTexImage1DEXTPtr;
        internal static void loadMultiTexImage1DEXT()
        {
            try
            {
                glMultiTexImage1DEXTPtr = (glMultiTexImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexImage1DEXT"), typeof(glMultiTexImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexImage1DEXT'.");
            }
        }
        public static void glMultiTexImage1DEXT(GLenum @texunit, GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glMultiTexImage1DEXTPtr(@texunit, @target, @level, @internalformat, @width, @border, @format, @type, @pixels);

        internal delegate void glMultiTexImage2DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glMultiTexImage2DEXTFunc glMultiTexImage2DEXTPtr;
        internal static void loadMultiTexImage2DEXT()
        {
            try
            {
                glMultiTexImage2DEXTPtr = (glMultiTexImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexImage2DEXT"), typeof(glMultiTexImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexImage2DEXT'.");
            }
        }
        public static void glMultiTexImage2DEXT(GLenum @texunit, GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glMultiTexImage2DEXTPtr(@texunit, @target, @level, @internalformat, @width, @height, @border, @format, @type, @pixels);

        internal delegate void glMultiTexSubImage1DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLenum @type, const void * @pixels);
        internal static glMultiTexSubImage1DEXTFunc glMultiTexSubImage1DEXTPtr;
        internal static void loadMultiTexSubImage1DEXT()
        {
            try
            {
                glMultiTexSubImage1DEXTPtr = (glMultiTexSubImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexSubImage1DEXT"), typeof(glMultiTexSubImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexSubImage1DEXT'.");
            }
        }
        public static void glMultiTexSubImage1DEXT(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLenum @type, const void * @pixels) => glMultiTexSubImage1DEXTPtr(@texunit, @target, @level, @xoffset, @width, @format, @type, @pixels);

        internal delegate void glMultiTexSubImage2DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels);
        internal static glMultiTexSubImage2DEXTFunc glMultiTexSubImage2DEXTPtr;
        internal static void loadMultiTexSubImage2DEXT()
        {
            try
            {
                glMultiTexSubImage2DEXTPtr = (glMultiTexSubImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexSubImage2DEXT"), typeof(glMultiTexSubImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexSubImage2DEXT'.");
            }
        }
        public static void glMultiTexSubImage2DEXT(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels) => glMultiTexSubImage2DEXTPtr(@texunit, @target, @level, @xoffset, @yoffset, @width, @height, @format, @type, @pixels);

        internal delegate void glCopyMultiTexImage1DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLint @border);
        internal static glCopyMultiTexImage1DEXTFunc glCopyMultiTexImage1DEXTPtr;
        internal static void loadCopyMultiTexImage1DEXT()
        {
            try
            {
                glCopyMultiTexImage1DEXTPtr = (glCopyMultiTexImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyMultiTexImage1DEXT"), typeof(glCopyMultiTexImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyMultiTexImage1DEXT'.");
            }
        }
        public static void glCopyMultiTexImage1DEXT(GLenum @texunit, GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLint @border) => glCopyMultiTexImage1DEXTPtr(@texunit, @target, @level, @internalformat, @x, @y, @width, @border);

        internal delegate void glCopyMultiTexImage2DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLint @border);
        internal static glCopyMultiTexImage2DEXTFunc glCopyMultiTexImage2DEXTPtr;
        internal static void loadCopyMultiTexImage2DEXT()
        {
            try
            {
                glCopyMultiTexImage2DEXTPtr = (glCopyMultiTexImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyMultiTexImage2DEXT"), typeof(glCopyMultiTexImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyMultiTexImage2DEXT'.");
            }
        }
        public static void glCopyMultiTexImage2DEXT(GLenum @texunit, GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLint @border) => glCopyMultiTexImage2DEXTPtr(@texunit, @target, @level, @internalformat, @x, @y, @width, @height, @border);

        internal delegate void glCopyMultiTexSubImage1DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @x, GLint @y, GLsizei @width);
        internal static glCopyMultiTexSubImage1DEXTFunc glCopyMultiTexSubImage1DEXTPtr;
        internal static void loadCopyMultiTexSubImage1DEXT()
        {
            try
            {
                glCopyMultiTexSubImage1DEXTPtr = (glCopyMultiTexSubImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyMultiTexSubImage1DEXT"), typeof(glCopyMultiTexSubImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyMultiTexSubImage1DEXT'.");
            }
        }
        public static void glCopyMultiTexSubImage1DEXT(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @x, GLint @y, GLsizei @width) => glCopyMultiTexSubImage1DEXTPtr(@texunit, @target, @level, @xoffset, @x, @y, @width);

        internal delegate void glCopyMultiTexSubImage2DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyMultiTexSubImage2DEXTFunc glCopyMultiTexSubImage2DEXTPtr;
        internal static void loadCopyMultiTexSubImage2DEXT()
        {
            try
            {
                glCopyMultiTexSubImage2DEXTPtr = (glCopyMultiTexSubImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyMultiTexSubImage2DEXT"), typeof(glCopyMultiTexSubImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyMultiTexSubImage2DEXT'.");
            }
        }
        public static void glCopyMultiTexSubImage2DEXT(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyMultiTexSubImage2DEXTPtr(@texunit, @target, @level, @xoffset, @yoffset, @x, @y, @width, @height);

        internal delegate void glGetMultiTexImageEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLenum @format, GLenum @type, void * @pixels);
        internal static glGetMultiTexImageEXTFunc glGetMultiTexImageEXTPtr;
        internal static void loadGetMultiTexImageEXT()
        {
            try
            {
                glGetMultiTexImageEXTPtr = (glGetMultiTexImageEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultiTexImageEXT"), typeof(glGetMultiTexImageEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultiTexImageEXT'.");
            }
        }
        public static void glGetMultiTexImageEXT(GLenum @texunit, GLenum @target, GLint @level, GLenum @format, GLenum @type, void * @pixels) => glGetMultiTexImageEXTPtr(@texunit, @target, @level, @format, @type, @pixels);

        internal delegate void glGetMultiTexParameterfvEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetMultiTexParameterfvEXTFunc glGetMultiTexParameterfvEXTPtr;
        internal static void loadGetMultiTexParameterfvEXT()
        {
            try
            {
                glGetMultiTexParameterfvEXTPtr = (glGetMultiTexParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultiTexParameterfvEXT"), typeof(glGetMultiTexParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultiTexParameterfvEXT'.");
            }
        }
        public static void glGetMultiTexParameterfvEXT(GLenum @texunit, GLenum @target, GLenum @pname, GLfloat * @params) => glGetMultiTexParameterfvEXTPtr(@texunit, @target, @pname, @params);

        internal delegate void glGetMultiTexParameterivEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetMultiTexParameterivEXTFunc glGetMultiTexParameterivEXTPtr;
        internal static void loadGetMultiTexParameterivEXT()
        {
            try
            {
                glGetMultiTexParameterivEXTPtr = (glGetMultiTexParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultiTexParameterivEXT"), typeof(glGetMultiTexParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultiTexParameterivEXT'.");
            }
        }
        public static void glGetMultiTexParameterivEXT(GLenum @texunit, GLenum @target, GLenum @pname, GLint * @params) => glGetMultiTexParameterivEXTPtr(@texunit, @target, @pname, @params);

        internal delegate void glGetMultiTexLevelParameterfvEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLenum @pname, GLfloat * @params);
        internal static glGetMultiTexLevelParameterfvEXTFunc glGetMultiTexLevelParameterfvEXTPtr;
        internal static void loadGetMultiTexLevelParameterfvEXT()
        {
            try
            {
                glGetMultiTexLevelParameterfvEXTPtr = (glGetMultiTexLevelParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultiTexLevelParameterfvEXT"), typeof(glGetMultiTexLevelParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultiTexLevelParameterfvEXT'.");
            }
        }
        public static void glGetMultiTexLevelParameterfvEXT(GLenum @texunit, GLenum @target, GLint @level, GLenum @pname, GLfloat * @params) => glGetMultiTexLevelParameterfvEXTPtr(@texunit, @target, @level, @pname, @params);

        internal delegate void glGetMultiTexLevelParameterivEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLenum @pname, GLint * @params);
        internal static glGetMultiTexLevelParameterivEXTFunc glGetMultiTexLevelParameterivEXTPtr;
        internal static void loadGetMultiTexLevelParameterivEXT()
        {
            try
            {
                glGetMultiTexLevelParameterivEXTPtr = (glGetMultiTexLevelParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultiTexLevelParameterivEXT"), typeof(glGetMultiTexLevelParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultiTexLevelParameterivEXT'.");
            }
        }
        public static void glGetMultiTexLevelParameterivEXT(GLenum @texunit, GLenum @target, GLint @level, GLenum @pname, GLint * @params) => glGetMultiTexLevelParameterivEXTPtr(@texunit, @target, @level, @pname, @params);

        internal delegate void glMultiTexImage3DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glMultiTexImage3DEXTFunc glMultiTexImage3DEXTPtr;
        internal static void loadMultiTexImage3DEXT()
        {
            try
            {
                glMultiTexImage3DEXTPtr = (glMultiTexImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexImage3DEXT"), typeof(glMultiTexImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexImage3DEXT'.");
            }
        }
        public static void glMultiTexImage3DEXT(GLenum @texunit, GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glMultiTexImage3DEXTPtr(@texunit, @target, @level, @internalformat, @width, @height, @depth, @border, @format, @type, @pixels);

        internal delegate void glMultiTexSubImage3DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels);
        internal static glMultiTexSubImage3DEXTFunc glMultiTexSubImage3DEXTPtr;
        internal static void loadMultiTexSubImage3DEXT()
        {
            try
            {
                glMultiTexSubImage3DEXTPtr = (glMultiTexSubImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexSubImage3DEXT"), typeof(glMultiTexSubImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexSubImage3DEXT'.");
            }
        }
        public static void glMultiTexSubImage3DEXT(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels) => glMultiTexSubImage3DEXTPtr(@texunit, @target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @type, @pixels);

        internal delegate void glCopyMultiTexSubImage3DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyMultiTexSubImage3DEXTFunc glCopyMultiTexSubImage3DEXTPtr;
        internal static void loadCopyMultiTexSubImage3DEXT()
        {
            try
            {
                glCopyMultiTexSubImage3DEXTPtr = (glCopyMultiTexSubImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyMultiTexSubImage3DEXT"), typeof(glCopyMultiTexSubImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyMultiTexSubImage3DEXT'.");
            }
        }
        public static void glCopyMultiTexSubImage3DEXT(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyMultiTexSubImage3DEXTPtr(@texunit, @target, @level, @xoffset, @yoffset, @zoffset, @x, @y, @width, @height);

        internal delegate void glEnableClientStateIndexedEXTFunc(GLenum @array, GLuint @index);
        internal static glEnableClientStateIndexedEXTFunc glEnableClientStateIndexedEXTPtr;
        internal static void loadEnableClientStateIndexedEXT()
        {
            try
            {
                glEnableClientStateIndexedEXTPtr = (glEnableClientStateIndexedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableClientStateIndexedEXT"), typeof(glEnableClientStateIndexedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableClientStateIndexedEXT'.");
            }
        }
        public static void glEnableClientStateIndexedEXT(GLenum @array, GLuint @index) => glEnableClientStateIndexedEXTPtr(@array, @index);

        internal delegate void glDisableClientStateIndexedEXTFunc(GLenum @array, GLuint @index);
        internal static glDisableClientStateIndexedEXTFunc glDisableClientStateIndexedEXTPtr;
        internal static void loadDisableClientStateIndexedEXT()
        {
            try
            {
                glDisableClientStateIndexedEXTPtr = (glDisableClientStateIndexedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableClientStateIndexedEXT"), typeof(glDisableClientStateIndexedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableClientStateIndexedEXT'.");
            }
        }
        public static void glDisableClientStateIndexedEXT(GLenum @array, GLuint @index) => glDisableClientStateIndexedEXTPtr(@array, @index);

        internal delegate void glGetFloatIndexedvEXTFunc(GLenum @target, GLuint @index, GLfloat * @data);
        internal static glGetFloatIndexedvEXTFunc glGetFloatIndexedvEXTPtr;
        internal static void loadGetFloatIndexedvEXT()
        {
            try
            {
                glGetFloatIndexedvEXTPtr = (glGetFloatIndexedvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFloatIndexedvEXT"), typeof(glGetFloatIndexedvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFloatIndexedvEXT'.");
            }
        }
        public static void glGetFloatIndexedvEXT(GLenum @target, GLuint @index, GLfloat * @data) => glGetFloatIndexedvEXTPtr(@target, @index, @data);

        internal delegate void glGetDoubleIndexedvEXTFunc(GLenum @target, GLuint @index, GLdouble * @data);
        internal static glGetDoubleIndexedvEXTFunc glGetDoubleIndexedvEXTPtr;
        internal static void loadGetDoubleIndexedvEXT()
        {
            try
            {
                glGetDoubleIndexedvEXTPtr = (glGetDoubleIndexedvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDoubleIndexedvEXT"), typeof(glGetDoubleIndexedvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDoubleIndexedvEXT'.");
            }
        }
        public static void glGetDoubleIndexedvEXT(GLenum @target, GLuint @index, GLdouble * @data) => glGetDoubleIndexedvEXTPtr(@target, @index, @data);

        internal delegate void glGetPointerIndexedvEXTFunc(GLenum @target, GLuint @index, void ** @data);
        internal static glGetPointerIndexedvEXTFunc glGetPointerIndexedvEXTPtr;
        internal static void loadGetPointerIndexedvEXT()
        {
            try
            {
                glGetPointerIndexedvEXTPtr = (glGetPointerIndexedvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPointerIndexedvEXT"), typeof(glGetPointerIndexedvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPointerIndexedvEXT'.");
            }
        }
        public static void glGetPointerIndexedvEXT(GLenum @target, GLuint @index, void ** @data) => glGetPointerIndexedvEXTPtr(@target, @index, @data);

        internal delegate void glEnableIndexedEXTFunc(GLenum @target, GLuint @index);
        internal static glEnableIndexedEXTFunc glEnableIndexedEXTPtr;
        internal static void loadEnableIndexedEXT()
        {
            try
            {
                glEnableIndexedEXTPtr = (glEnableIndexedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableIndexedEXT"), typeof(glEnableIndexedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableIndexedEXT'.");
            }
        }
        public static void glEnableIndexedEXT(GLenum @target, GLuint @index) => glEnableIndexedEXTPtr(@target, @index);

        internal delegate void glDisableIndexedEXTFunc(GLenum @target, GLuint @index);
        internal static glDisableIndexedEXTFunc glDisableIndexedEXTPtr;
        internal static void loadDisableIndexedEXT()
        {
            try
            {
                glDisableIndexedEXTPtr = (glDisableIndexedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableIndexedEXT"), typeof(glDisableIndexedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableIndexedEXT'.");
            }
        }
        public static void glDisableIndexedEXT(GLenum @target, GLuint @index) => glDisableIndexedEXTPtr(@target, @index);

        internal delegate GLboolean glIsEnabledIndexedEXTFunc(GLenum @target, GLuint @index);
        internal static glIsEnabledIndexedEXTFunc glIsEnabledIndexedEXTPtr;
        internal static void loadIsEnabledIndexedEXT()
        {
            try
            {
                glIsEnabledIndexedEXTPtr = (glIsEnabledIndexedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsEnabledIndexedEXT"), typeof(glIsEnabledIndexedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsEnabledIndexedEXT'.");
            }
        }
        public static GLboolean glIsEnabledIndexedEXT(GLenum @target, GLuint @index) => glIsEnabledIndexedEXTPtr(@target, @index);

        internal delegate void glGetIntegerIndexedvEXTFunc(GLenum @target, GLuint @index, GLint * @data);
        internal static glGetIntegerIndexedvEXTFunc glGetIntegerIndexedvEXTPtr;
        internal static void loadGetIntegerIndexedvEXT()
        {
            try
            {
                glGetIntegerIndexedvEXTPtr = (glGetIntegerIndexedvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetIntegerIndexedvEXT"), typeof(glGetIntegerIndexedvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetIntegerIndexedvEXT'.");
            }
        }
        public static void glGetIntegerIndexedvEXT(GLenum @target, GLuint @index, GLint * @data) => glGetIntegerIndexedvEXTPtr(@target, @index, @data);

        internal delegate void glGetBooleanIndexedvEXTFunc(GLenum @target, GLuint @index, GLboolean * @data);
        internal static glGetBooleanIndexedvEXTFunc glGetBooleanIndexedvEXTPtr;
        internal static void loadGetBooleanIndexedvEXT()
        {
            try
            {
                glGetBooleanIndexedvEXTPtr = (glGetBooleanIndexedvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBooleanIndexedvEXT"), typeof(glGetBooleanIndexedvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBooleanIndexedvEXT'.");
            }
        }
        public static void glGetBooleanIndexedvEXT(GLenum @target, GLuint @index, GLboolean * @data) => glGetBooleanIndexedvEXTPtr(@target, @index, @data);

        internal delegate void glCompressedTextureImage3DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLsizei @imageSize, const void * @bits);
        internal static glCompressedTextureImage3DEXTFunc glCompressedTextureImage3DEXTPtr;
        internal static void loadCompressedTextureImage3DEXT()
        {
            try
            {
                glCompressedTextureImage3DEXTPtr = (glCompressedTextureImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTextureImage3DEXT"), typeof(glCompressedTextureImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTextureImage3DEXT'.");
            }
        }
        public static void glCompressedTextureImage3DEXT(GLuint @texture, GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLsizei @imageSize, const void * @bits) => glCompressedTextureImage3DEXTPtr(@texture, @target, @level, @internalformat, @width, @height, @depth, @border, @imageSize, @bits);

        internal delegate void glCompressedTextureImage2DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLsizei @imageSize, const void * @bits);
        internal static glCompressedTextureImage2DEXTFunc glCompressedTextureImage2DEXTPtr;
        internal static void loadCompressedTextureImage2DEXT()
        {
            try
            {
                glCompressedTextureImage2DEXTPtr = (glCompressedTextureImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTextureImage2DEXT"), typeof(glCompressedTextureImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTextureImage2DEXT'.");
            }
        }
        public static void glCompressedTextureImage2DEXT(GLuint @texture, GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLsizei @imageSize, const void * @bits) => glCompressedTextureImage2DEXTPtr(@texture, @target, @level, @internalformat, @width, @height, @border, @imageSize, @bits);

        internal delegate void glCompressedTextureImage1DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLint @border, GLsizei @imageSize, const void * @bits);
        internal static glCompressedTextureImage1DEXTFunc glCompressedTextureImage1DEXTPtr;
        internal static void loadCompressedTextureImage1DEXT()
        {
            try
            {
                glCompressedTextureImage1DEXTPtr = (glCompressedTextureImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTextureImage1DEXT"), typeof(glCompressedTextureImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTextureImage1DEXT'.");
            }
        }
        public static void glCompressedTextureImage1DEXT(GLuint @texture, GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLint @border, GLsizei @imageSize, const void * @bits) => glCompressedTextureImage1DEXTPtr(@texture, @target, @level, @internalformat, @width, @border, @imageSize, @bits);

        internal delegate void glCompressedTextureSubImage3DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @bits);
        internal static glCompressedTextureSubImage3DEXTFunc glCompressedTextureSubImage3DEXTPtr;
        internal static void loadCompressedTextureSubImage3DEXT()
        {
            try
            {
                glCompressedTextureSubImage3DEXTPtr = (glCompressedTextureSubImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTextureSubImage3DEXT"), typeof(glCompressedTextureSubImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTextureSubImage3DEXT'.");
            }
        }
        public static void glCompressedTextureSubImage3DEXT(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @bits) => glCompressedTextureSubImage3DEXTPtr(@texture, @target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @imageSize, @bits);

        internal delegate void glCompressedTextureSubImage2DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @bits);
        internal static glCompressedTextureSubImage2DEXTFunc glCompressedTextureSubImage2DEXTPtr;
        internal static void loadCompressedTextureSubImage2DEXT()
        {
            try
            {
                glCompressedTextureSubImage2DEXTPtr = (glCompressedTextureSubImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTextureSubImage2DEXT"), typeof(glCompressedTextureSubImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTextureSubImage2DEXT'.");
            }
        }
        public static void glCompressedTextureSubImage2DEXT(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @bits) => glCompressedTextureSubImage2DEXTPtr(@texture, @target, @level, @xoffset, @yoffset, @width, @height, @format, @imageSize, @bits);

        internal delegate void glCompressedTextureSubImage1DEXTFunc(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLsizei @imageSize, const void * @bits);
        internal static glCompressedTextureSubImage1DEXTFunc glCompressedTextureSubImage1DEXTPtr;
        internal static void loadCompressedTextureSubImage1DEXT()
        {
            try
            {
                glCompressedTextureSubImage1DEXTPtr = (glCompressedTextureSubImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTextureSubImage1DEXT"), typeof(glCompressedTextureSubImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTextureSubImage1DEXT'.");
            }
        }
        public static void glCompressedTextureSubImage1DEXT(GLuint @texture, GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLsizei @imageSize, const void * @bits) => glCompressedTextureSubImage1DEXTPtr(@texture, @target, @level, @xoffset, @width, @format, @imageSize, @bits);

        internal delegate void glGetCompressedTextureImageEXTFunc(GLuint @texture, GLenum @target, GLint @lod, void * @img);
        internal static glGetCompressedTextureImageEXTFunc glGetCompressedTextureImageEXTPtr;
        internal static void loadGetCompressedTextureImageEXT()
        {
            try
            {
                glGetCompressedTextureImageEXTPtr = (glGetCompressedTextureImageEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCompressedTextureImageEXT"), typeof(glGetCompressedTextureImageEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCompressedTextureImageEXT'.");
            }
        }
        public static void glGetCompressedTextureImageEXT(GLuint @texture, GLenum @target, GLint @lod, void * @img) => glGetCompressedTextureImageEXTPtr(@texture, @target, @lod, @img);

        internal delegate void glCompressedMultiTexImage3DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLsizei @imageSize, const void * @bits);
        internal static glCompressedMultiTexImage3DEXTFunc glCompressedMultiTexImage3DEXTPtr;
        internal static void loadCompressedMultiTexImage3DEXT()
        {
            try
            {
                glCompressedMultiTexImage3DEXTPtr = (glCompressedMultiTexImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedMultiTexImage3DEXT"), typeof(glCompressedMultiTexImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedMultiTexImage3DEXT'.");
            }
        }
        public static void glCompressedMultiTexImage3DEXT(GLenum @texunit, GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLsizei @imageSize, const void * @bits) => glCompressedMultiTexImage3DEXTPtr(@texunit, @target, @level, @internalformat, @width, @height, @depth, @border, @imageSize, @bits);

        internal delegate void glCompressedMultiTexImage2DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLsizei @imageSize, const void * @bits);
        internal static glCompressedMultiTexImage2DEXTFunc glCompressedMultiTexImage2DEXTPtr;
        internal static void loadCompressedMultiTexImage2DEXT()
        {
            try
            {
                glCompressedMultiTexImage2DEXTPtr = (glCompressedMultiTexImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedMultiTexImage2DEXT"), typeof(glCompressedMultiTexImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedMultiTexImage2DEXT'.");
            }
        }
        public static void glCompressedMultiTexImage2DEXT(GLenum @texunit, GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLsizei @imageSize, const void * @bits) => glCompressedMultiTexImage2DEXTPtr(@texunit, @target, @level, @internalformat, @width, @height, @border, @imageSize, @bits);

        internal delegate void glCompressedMultiTexImage1DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLint @border, GLsizei @imageSize, const void * @bits);
        internal static glCompressedMultiTexImage1DEXTFunc glCompressedMultiTexImage1DEXTPtr;
        internal static void loadCompressedMultiTexImage1DEXT()
        {
            try
            {
                glCompressedMultiTexImage1DEXTPtr = (glCompressedMultiTexImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedMultiTexImage1DEXT"), typeof(glCompressedMultiTexImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedMultiTexImage1DEXT'.");
            }
        }
        public static void glCompressedMultiTexImage1DEXT(GLenum @texunit, GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLint @border, GLsizei @imageSize, const void * @bits) => glCompressedMultiTexImage1DEXTPtr(@texunit, @target, @level, @internalformat, @width, @border, @imageSize, @bits);

        internal delegate void glCompressedMultiTexSubImage3DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @bits);
        internal static glCompressedMultiTexSubImage3DEXTFunc glCompressedMultiTexSubImage3DEXTPtr;
        internal static void loadCompressedMultiTexSubImage3DEXT()
        {
            try
            {
                glCompressedMultiTexSubImage3DEXTPtr = (glCompressedMultiTexSubImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedMultiTexSubImage3DEXT"), typeof(glCompressedMultiTexSubImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedMultiTexSubImage3DEXT'.");
            }
        }
        public static void glCompressedMultiTexSubImage3DEXT(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @bits) => glCompressedMultiTexSubImage3DEXTPtr(@texunit, @target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @imageSize, @bits);

        internal delegate void glCompressedMultiTexSubImage2DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @bits);
        internal static glCompressedMultiTexSubImage2DEXTFunc glCompressedMultiTexSubImage2DEXTPtr;
        internal static void loadCompressedMultiTexSubImage2DEXT()
        {
            try
            {
                glCompressedMultiTexSubImage2DEXTPtr = (glCompressedMultiTexSubImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedMultiTexSubImage2DEXT"), typeof(glCompressedMultiTexSubImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedMultiTexSubImage2DEXT'.");
            }
        }
        public static void glCompressedMultiTexSubImage2DEXT(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @bits) => glCompressedMultiTexSubImage2DEXTPtr(@texunit, @target, @level, @xoffset, @yoffset, @width, @height, @format, @imageSize, @bits);

        internal delegate void glCompressedMultiTexSubImage1DEXTFunc(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLsizei @imageSize, const void * @bits);
        internal static glCompressedMultiTexSubImage1DEXTFunc glCompressedMultiTexSubImage1DEXTPtr;
        internal static void loadCompressedMultiTexSubImage1DEXT()
        {
            try
            {
                glCompressedMultiTexSubImage1DEXTPtr = (glCompressedMultiTexSubImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedMultiTexSubImage1DEXT"), typeof(glCompressedMultiTexSubImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedMultiTexSubImage1DEXT'.");
            }
        }
        public static void glCompressedMultiTexSubImage1DEXT(GLenum @texunit, GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLsizei @imageSize, const void * @bits) => glCompressedMultiTexSubImage1DEXTPtr(@texunit, @target, @level, @xoffset, @width, @format, @imageSize, @bits);

        internal delegate void glGetCompressedMultiTexImageEXTFunc(GLenum @texunit, GLenum @target, GLint @lod, void * @img);
        internal static glGetCompressedMultiTexImageEXTFunc glGetCompressedMultiTexImageEXTPtr;
        internal static void loadGetCompressedMultiTexImageEXT()
        {
            try
            {
                glGetCompressedMultiTexImageEXTPtr = (glGetCompressedMultiTexImageEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCompressedMultiTexImageEXT"), typeof(glGetCompressedMultiTexImageEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCompressedMultiTexImageEXT'.");
            }
        }
        public static void glGetCompressedMultiTexImageEXT(GLenum @texunit, GLenum @target, GLint @lod, void * @img) => glGetCompressedMultiTexImageEXTPtr(@texunit, @target, @lod, @img);

        internal delegate void glMatrixLoadTransposefEXTFunc(GLenum @mode, const GLfloat * @m);
        internal static glMatrixLoadTransposefEXTFunc glMatrixLoadTransposefEXTPtr;
        internal static void loadMatrixLoadTransposefEXT()
        {
            try
            {
                glMatrixLoadTransposefEXTPtr = (glMatrixLoadTransposefEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixLoadTransposefEXT"), typeof(glMatrixLoadTransposefEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixLoadTransposefEXT'.");
            }
        }
        public static void glMatrixLoadTransposefEXT(GLenum @mode, const GLfloat * @m) => glMatrixLoadTransposefEXTPtr(@mode, @m);

        internal delegate void glMatrixLoadTransposedEXTFunc(GLenum @mode, const GLdouble * @m);
        internal static glMatrixLoadTransposedEXTFunc glMatrixLoadTransposedEXTPtr;
        internal static void loadMatrixLoadTransposedEXT()
        {
            try
            {
                glMatrixLoadTransposedEXTPtr = (glMatrixLoadTransposedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixLoadTransposedEXT"), typeof(glMatrixLoadTransposedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixLoadTransposedEXT'.");
            }
        }
        public static void glMatrixLoadTransposedEXT(GLenum @mode, const GLdouble * @m) => glMatrixLoadTransposedEXTPtr(@mode, @m);

        internal delegate void glMatrixMultTransposefEXTFunc(GLenum @mode, const GLfloat * @m);
        internal static glMatrixMultTransposefEXTFunc glMatrixMultTransposefEXTPtr;
        internal static void loadMatrixMultTransposefEXT()
        {
            try
            {
                glMatrixMultTransposefEXTPtr = (glMatrixMultTransposefEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixMultTransposefEXT"), typeof(glMatrixMultTransposefEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixMultTransposefEXT'.");
            }
        }
        public static void glMatrixMultTransposefEXT(GLenum @mode, const GLfloat * @m) => glMatrixMultTransposefEXTPtr(@mode, @m);

        internal delegate void glMatrixMultTransposedEXTFunc(GLenum @mode, const GLdouble * @m);
        internal static glMatrixMultTransposedEXTFunc glMatrixMultTransposedEXTPtr;
        internal static void loadMatrixMultTransposedEXT()
        {
            try
            {
                glMatrixMultTransposedEXTPtr = (glMatrixMultTransposedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixMultTransposedEXT"), typeof(glMatrixMultTransposedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixMultTransposedEXT'.");
            }
        }
        public static void glMatrixMultTransposedEXT(GLenum @mode, const GLdouble * @m) => glMatrixMultTransposedEXTPtr(@mode, @m);

        internal delegate void glNamedBufferDataEXTFunc(GLuint @buffer, GLsizeiptr @size, const void * @data, GLenum @usage);
        internal static glNamedBufferDataEXTFunc glNamedBufferDataEXTPtr;
        internal static void loadNamedBufferDataEXT()
        {
            try
            {
                glNamedBufferDataEXTPtr = (glNamedBufferDataEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedBufferDataEXT"), typeof(glNamedBufferDataEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedBufferDataEXT'.");
            }
        }
        public static void glNamedBufferDataEXT(GLuint @buffer, GLsizeiptr @size, const void * @data, GLenum @usage) => glNamedBufferDataEXTPtr(@buffer, @size, @data, @usage);

        internal delegate void glNamedBufferSubDataEXTFunc(GLuint @buffer, GLintptr @offset, GLsizeiptr @size, const void * @data);
        internal static glNamedBufferSubDataEXTFunc glNamedBufferSubDataEXTPtr;
        internal static void loadNamedBufferSubDataEXT()
        {
            try
            {
                glNamedBufferSubDataEXTPtr = (glNamedBufferSubDataEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedBufferSubDataEXT"), typeof(glNamedBufferSubDataEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedBufferSubDataEXT'.");
            }
        }
        public static void glNamedBufferSubDataEXT(GLuint @buffer, GLintptr @offset, GLsizeiptr @size, const void * @data) => glNamedBufferSubDataEXTPtr(@buffer, @offset, @size, @data);

        internal delegate void * glMapNamedBufferEXTFunc(GLuint @buffer, GLenum @access);
        internal static glMapNamedBufferEXTFunc glMapNamedBufferEXTPtr;
        internal static void loadMapNamedBufferEXT()
        {
            try
            {
                glMapNamedBufferEXTPtr = (glMapNamedBufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapNamedBufferEXT"), typeof(glMapNamedBufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapNamedBufferEXT'.");
            }
        }
        public static void * glMapNamedBufferEXT(GLuint @buffer, GLenum @access) => glMapNamedBufferEXTPtr(@buffer, @access);

        internal delegate GLboolean glUnmapNamedBufferEXTFunc(GLuint @buffer);
        internal static glUnmapNamedBufferEXTFunc glUnmapNamedBufferEXTPtr;
        internal static void loadUnmapNamedBufferEXT()
        {
            try
            {
                glUnmapNamedBufferEXTPtr = (glUnmapNamedBufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUnmapNamedBufferEXT"), typeof(glUnmapNamedBufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUnmapNamedBufferEXT'.");
            }
        }
        public static GLboolean glUnmapNamedBufferEXT(GLuint @buffer) => glUnmapNamedBufferEXTPtr(@buffer);

        internal delegate void glGetNamedBufferParameterivEXTFunc(GLuint @buffer, GLenum @pname, GLint * @params);
        internal static glGetNamedBufferParameterivEXTFunc glGetNamedBufferParameterivEXTPtr;
        internal static void loadGetNamedBufferParameterivEXT()
        {
            try
            {
                glGetNamedBufferParameterivEXTPtr = (glGetNamedBufferParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedBufferParameterivEXT"), typeof(glGetNamedBufferParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedBufferParameterivEXT'.");
            }
        }
        public static void glGetNamedBufferParameterivEXT(GLuint @buffer, GLenum @pname, GLint * @params) => glGetNamedBufferParameterivEXTPtr(@buffer, @pname, @params);

        internal delegate void glGetNamedBufferPointervEXTFunc(GLuint @buffer, GLenum @pname, void ** @params);
        internal static glGetNamedBufferPointervEXTFunc glGetNamedBufferPointervEXTPtr;
        internal static void loadGetNamedBufferPointervEXT()
        {
            try
            {
                glGetNamedBufferPointervEXTPtr = (glGetNamedBufferPointervEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedBufferPointervEXT"), typeof(glGetNamedBufferPointervEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedBufferPointervEXT'.");
            }
        }
        public static void glGetNamedBufferPointervEXT(GLuint @buffer, GLenum @pname, void ** @params) => glGetNamedBufferPointervEXTPtr(@buffer, @pname, @params);

        internal delegate void glGetNamedBufferSubDataEXTFunc(GLuint @buffer, GLintptr @offset, GLsizeiptr @size, void * @data);
        internal static glGetNamedBufferSubDataEXTFunc glGetNamedBufferSubDataEXTPtr;
        internal static void loadGetNamedBufferSubDataEXT()
        {
            try
            {
                glGetNamedBufferSubDataEXTPtr = (glGetNamedBufferSubDataEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedBufferSubDataEXT"), typeof(glGetNamedBufferSubDataEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedBufferSubDataEXT'.");
            }
        }
        public static void glGetNamedBufferSubDataEXT(GLuint @buffer, GLintptr @offset, GLsizeiptr @size, void * @data) => glGetNamedBufferSubDataEXTPtr(@buffer, @offset, @size, @data);

        internal delegate void glProgramUniform1fEXTFunc(GLuint @program, GLint @location, GLfloat @v0);
        internal static glProgramUniform1fEXTFunc glProgramUniform1fEXTPtr;
        internal static void loadProgramUniform1fEXT()
        {
            try
            {
                glProgramUniform1fEXTPtr = (glProgramUniform1fEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1fEXT"), typeof(glProgramUniform1fEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1fEXT'.");
            }
        }
        public static void glProgramUniform1fEXT(GLuint @program, GLint @location, GLfloat @v0) => glProgramUniform1fEXTPtr(@program, @location, @v0);

        internal delegate void glProgramUniform2fEXTFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1);
        internal static glProgramUniform2fEXTFunc glProgramUniform2fEXTPtr;
        internal static void loadProgramUniform2fEXT()
        {
            try
            {
                glProgramUniform2fEXTPtr = (glProgramUniform2fEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2fEXT"), typeof(glProgramUniform2fEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2fEXT'.");
            }
        }
        public static void glProgramUniform2fEXT(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1) => glProgramUniform2fEXTPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform3fEXTFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2);
        internal static glProgramUniform3fEXTFunc glProgramUniform3fEXTPtr;
        internal static void loadProgramUniform3fEXT()
        {
            try
            {
                glProgramUniform3fEXTPtr = (glProgramUniform3fEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3fEXT"), typeof(glProgramUniform3fEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3fEXT'.");
            }
        }
        public static void glProgramUniform3fEXT(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2) => glProgramUniform3fEXTPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform4fEXTFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3);
        internal static glProgramUniform4fEXTFunc glProgramUniform4fEXTPtr;
        internal static void loadProgramUniform4fEXT()
        {
            try
            {
                glProgramUniform4fEXTPtr = (glProgramUniform4fEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4fEXT"), typeof(glProgramUniform4fEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4fEXT'.");
            }
        }
        public static void glProgramUniform4fEXT(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3) => glProgramUniform4fEXTPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform1iEXTFunc(GLuint @program, GLint @location, GLint @v0);
        internal static glProgramUniform1iEXTFunc glProgramUniform1iEXTPtr;
        internal static void loadProgramUniform1iEXT()
        {
            try
            {
                glProgramUniform1iEXTPtr = (glProgramUniform1iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1iEXT"), typeof(glProgramUniform1iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1iEXT'.");
            }
        }
        public static void glProgramUniform1iEXT(GLuint @program, GLint @location, GLint @v0) => glProgramUniform1iEXTPtr(@program, @location, @v0);

        internal delegate void glProgramUniform2iEXTFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1);
        internal static glProgramUniform2iEXTFunc glProgramUniform2iEXTPtr;
        internal static void loadProgramUniform2iEXT()
        {
            try
            {
                glProgramUniform2iEXTPtr = (glProgramUniform2iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2iEXT"), typeof(glProgramUniform2iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2iEXT'.");
            }
        }
        public static void glProgramUniform2iEXT(GLuint @program, GLint @location, GLint @v0, GLint @v1) => glProgramUniform2iEXTPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform3iEXTFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2);
        internal static glProgramUniform3iEXTFunc glProgramUniform3iEXTPtr;
        internal static void loadProgramUniform3iEXT()
        {
            try
            {
                glProgramUniform3iEXTPtr = (glProgramUniform3iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3iEXT"), typeof(glProgramUniform3iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3iEXT'.");
            }
        }
        public static void glProgramUniform3iEXT(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2) => glProgramUniform3iEXTPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform4iEXTFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3);
        internal static glProgramUniform4iEXTFunc glProgramUniform4iEXTPtr;
        internal static void loadProgramUniform4iEXT()
        {
            try
            {
                glProgramUniform4iEXTPtr = (glProgramUniform4iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4iEXT"), typeof(glProgramUniform4iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4iEXT'.");
            }
        }
        public static void glProgramUniform4iEXT(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3) => glProgramUniform4iEXTPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform1fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform1fvEXTFunc glProgramUniform1fvEXTPtr;
        internal static void loadProgramUniform1fvEXT()
        {
            try
            {
                glProgramUniform1fvEXTPtr = (glProgramUniform1fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1fvEXT"), typeof(glProgramUniform1fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1fvEXT'.");
            }
        }
        public static void glProgramUniform1fvEXT(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform1fvEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform2fvEXTFunc glProgramUniform2fvEXTPtr;
        internal static void loadProgramUniform2fvEXT()
        {
            try
            {
                glProgramUniform2fvEXTPtr = (glProgramUniform2fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2fvEXT"), typeof(glProgramUniform2fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2fvEXT'.");
            }
        }
        public static void glProgramUniform2fvEXT(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform2fvEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform3fvEXTFunc glProgramUniform3fvEXTPtr;
        internal static void loadProgramUniform3fvEXT()
        {
            try
            {
                glProgramUniform3fvEXTPtr = (glProgramUniform3fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3fvEXT"), typeof(glProgramUniform3fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3fvEXT'.");
            }
        }
        public static void glProgramUniform3fvEXT(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform3fvEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform4fvEXTFunc glProgramUniform4fvEXTPtr;
        internal static void loadProgramUniform4fvEXT()
        {
            try
            {
                glProgramUniform4fvEXTPtr = (glProgramUniform4fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4fvEXT"), typeof(glProgramUniform4fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4fvEXT'.");
            }
        }
        public static void glProgramUniform4fvEXT(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform4fvEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform1ivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform1ivEXTFunc glProgramUniform1ivEXTPtr;
        internal static void loadProgramUniform1ivEXT()
        {
            try
            {
                glProgramUniform1ivEXTPtr = (glProgramUniform1ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1ivEXT"), typeof(glProgramUniform1ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1ivEXT'.");
            }
        }
        public static void glProgramUniform1ivEXT(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform1ivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2ivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform2ivEXTFunc glProgramUniform2ivEXTPtr;
        internal static void loadProgramUniform2ivEXT()
        {
            try
            {
                glProgramUniform2ivEXTPtr = (glProgramUniform2ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2ivEXT"), typeof(glProgramUniform2ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2ivEXT'.");
            }
        }
        public static void glProgramUniform2ivEXT(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform2ivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3ivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform3ivEXTFunc glProgramUniform3ivEXTPtr;
        internal static void loadProgramUniform3ivEXT()
        {
            try
            {
                glProgramUniform3ivEXTPtr = (glProgramUniform3ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3ivEXT"), typeof(glProgramUniform3ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3ivEXT'.");
            }
        }
        public static void glProgramUniform3ivEXT(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform3ivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4ivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform4ivEXTFunc glProgramUniform4ivEXTPtr;
        internal static void loadProgramUniform4ivEXT()
        {
            try
            {
                glProgramUniform4ivEXTPtr = (glProgramUniform4ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4ivEXT"), typeof(glProgramUniform4ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4ivEXT'.");
            }
        }
        public static void glProgramUniform4ivEXT(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform4ivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniformMatrix2fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2fvEXTFunc glProgramUniformMatrix2fvEXTPtr;
        internal static void loadProgramUniformMatrix2fvEXT()
        {
            try
            {
                glProgramUniformMatrix2fvEXTPtr = (glProgramUniformMatrix2fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2fvEXT"), typeof(glProgramUniformMatrix2fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix2fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3fvEXTFunc glProgramUniformMatrix3fvEXTPtr;
        internal static void loadProgramUniformMatrix3fvEXT()
        {
            try
            {
                glProgramUniformMatrix3fvEXTPtr = (glProgramUniformMatrix3fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3fvEXT"), typeof(glProgramUniformMatrix3fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix3fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4fvEXTFunc glProgramUniformMatrix4fvEXTPtr;
        internal static void loadProgramUniformMatrix4fvEXT()
        {
            try
            {
                glProgramUniformMatrix4fvEXTPtr = (glProgramUniformMatrix4fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4fvEXT"), typeof(glProgramUniformMatrix4fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix4fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x3fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2x3fvEXTFunc glProgramUniformMatrix2x3fvEXTPtr;
        internal static void loadProgramUniformMatrix2x3fvEXT()
        {
            try
            {
                glProgramUniformMatrix2x3fvEXTPtr = (glProgramUniformMatrix2x3fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x3fvEXT"), typeof(glProgramUniformMatrix2x3fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x3fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix2x3fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2x3fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x2fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3x2fvEXTFunc glProgramUniformMatrix3x2fvEXTPtr;
        internal static void loadProgramUniformMatrix3x2fvEXT()
        {
            try
            {
                glProgramUniformMatrix3x2fvEXTPtr = (glProgramUniformMatrix3x2fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x2fvEXT"), typeof(glProgramUniformMatrix3x2fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x2fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix3x2fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3x2fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x4fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2x4fvEXTFunc glProgramUniformMatrix2x4fvEXTPtr;
        internal static void loadProgramUniformMatrix2x4fvEXT()
        {
            try
            {
                glProgramUniformMatrix2x4fvEXTPtr = (glProgramUniformMatrix2x4fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x4fvEXT"), typeof(glProgramUniformMatrix2x4fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x4fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix2x4fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2x4fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x2fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4x2fvEXTFunc glProgramUniformMatrix4x2fvEXTPtr;
        internal static void loadProgramUniformMatrix4x2fvEXT()
        {
            try
            {
                glProgramUniformMatrix4x2fvEXTPtr = (glProgramUniformMatrix4x2fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x2fvEXT"), typeof(glProgramUniformMatrix4x2fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x2fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix4x2fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4x2fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x4fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3x4fvEXTFunc glProgramUniformMatrix3x4fvEXTPtr;
        internal static void loadProgramUniformMatrix3x4fvEXT()
        {
            try
            {
                glProgramUniformMatrix3x4fvEXTPtr = (glProgramUniformMatrix3x4fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x4fvEXT"), typeof(glProgramUniformMatrix3x4fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x4fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix3x4fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3x4fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x3fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4x3fvEXTFunc glProgramUniformMatrix4x3fvEXTPtr;
        internal static void loadProgramUniformMatrix4x3fvEXT()
        {
            try
            {
                glProgramUniformMatrix4x3fvEXTPtr = (glProgramUniformMatrix4x3fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x3fvEXT"), typeof(glProgramUniformMatrix4x3fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x3fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix4x3fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4x3fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glTextureBufferEXTFunc(GLuint @texture, GLenum @target, GLenum @internalformat, GLuint @buffer);
        internal static glTextureBufferEXTFunc glTextureBufferEXTPtr;
        internal static void loadTextureBufferEXT()
        {
            try
            {
                glTextureBufferEXTPtr = (glTextureBufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureBufferEXT"), typeof(glTextureBufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureBufferEXT'.");
            }
        }
        public static void glTextureBufferEXT(GLuint @texture, GLenum @target, GLenum @internalformat, GLuint @buffer) => glTextureBufferEXTPtr(@texture, @target, @internalformat, @buffer);

        internal delegate void glMultiTexBufferEXTFunc(GLenum @texunit, GLenum @target, GLenum @internalformat, GLuint @buffer);
        internal static glMultiTexBufferEXTFunc glMultiTexBufferEXTPtr;
        internal static void loadMultiTexBufferEXT()
        {
            try
            {
                glMultiTexBufferEXTPtr = (glMultiTexBufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexBufferEXT"), typeof(glMultiTexBufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexBufferEXT'.");
            }
        }
        public static void glMultiTexBufferEXT(GLenum @texunit, GLenum @target, GLenum @internalformat, GLuint @buffer) => glMultiTexBufferEXTPtr(@texunit, @target, @internalformat, @buffer);

        internal delegate void glTextureParameterIivEXTFunc(GLuint @texture, GLenum @target, GLenum @pname, const GLint * @params);
        internal static glTextureParameterIivEXTFunc glTextureParameterIivEXTPtr;
        internal static void loadTextureParameterIivEXT()
        {
            try
            {
                glTextureParameterIivEXTPtr = (glTextureParameterIivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureParameterIivEXT"), typeof(glTextureParameterIivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureParameterIivEXT'.");
            }
        }
        public static void glTextureParameterIivEXT(GLuint @texture, GLenum @target, GLenum @pname, const GLint * @params) => glTextureParameterIivEXTPtr(@texture, @target, @pname, @params);

        internal delegate void glTextureParameterIuivEXTFunc(GLuint @texture, GLenum @target, GLenum @pname, const GLuint * @params);
        internal static glTextureParameterIuivEXTFunc glTextureParameterIuivEXTPtr;
        internal static void loadTextureParameterIuivEXT()
        {
            try
            {
                glTextureParameterIuivEXTPtr = (glTextureParameterIuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureParameterIuivEXT"), typeof(glTextureParameterIuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureParameterIuivEXT'.");
            }
        }
        public static void glTextureParameterIuivEXT(GLuint @texture, GLenum @target, GLenum @pname, const GLuint * @params) => glTextureParameterIuivEXTPtr(@texture, @target, @pname, @params);

        internal delegate void glGetTextureParameterIivEXTFunc(GLuint @texture, GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetTextureParameterIivEXTFunc glGetTextureParameterIivEXTPtr;
        internal static void loadGetTextureParameterIivEXT()
        {
            try
            {
                glGetTextureParameterIivEXTPtr = (glGetTextureParameterIivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureParameterIivEXT"), typeof(glGetTextureParameterIivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureParameterIivEXT'.");
            }
        }
        public static void glGetTextureParameterIivEXT(GLuint @texture, GLenum @target, GLenum @pname, GLint * @params) => glGetTextureParameterIivEXTPtr(@texture, @target, @pname, @params);

        internal delegate void glGetTextureParameterIuivEXTFunc(GLuint @texture, GLenum @target, GLenum @pname, GLuint * @params);
        internal static glGetTextureParameterIuivEXTFunc glGetTextureParameterIuivEXTPtr;
        internal static void loadGetTextureParameterIuivEXT()
        {
            try
            {
                glGetTextureParameterIuivEXTPtr = (glGetTextureParameterIuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureParameterIuivEXT"), typeof(glGetTextureParameterIuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureParameterIuivEXT'.");
            }
        }
        public static void glGetTextureParameterIuivEXT(GLuint @texture, GLenum @target, GLenum @pname, GLuint * @params) => glGetTextureParameterIuivEXTPtr(@texture, @target, @pname, @params);

        internal delegate void glMultiTexParameterIivEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, const GLint * @params);
        internal static glMultiTexParameterIivEXTFunc glMultiTexParameterIivEXTPtr;
        internal static void loadMultiTexParameterIivEXT()
        {
            try
            {
                glMultiTexParameterIivEXTPtr = (glMultiTexParameterIivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexParameterIivEXT"), typeof(glMultiTexParameterIivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexParameterIivEXT'.");
            }
        }
        public static void glMultiTexParameterIivEXT(GLenum @texunit, GLenum @target, GLenum @pname, const GLint * @params) => glMultiTexParameterIivEXTPtr(@texunit, @target, @pname, @params);

        internal delegate void glMultiTexParameterIuivEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, const GLuint * @params);
        internal static glMultiTexParameterIuivEXTFunc glMultiTexParameterIuivEXTPtr;
        internal static void loadMultiTexParameterIuivEXT()
        {
            try
            {
                glMultiTexParameterIuivEXTPtr = (glMultiTexParameterIuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexParameterIuivEXT"), typeof(glMultiTexParameterIuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexParameterIuivEXT'.");
            }
        }
        public static void glMultiTexParameterIuivEXT(GLenum @texunit, GLenum @target, GLenum @pname, const GLuint * @params) => glMultiTexParameterIuivEXTPtr(@texunit, @target, @pname, @params);

        internal delegate void glGetMultiTexParameterIivEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetMultiTexParameterIivEXTFunc glGetMultiTexParameterIivEXTPtr;
        internal static void loadGetMultiTexParameterIivEXT()
        {
            try
            {
                glGetMultiTexParameterIivEXTPtr = (glGetMultiTexParameterIivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultiTexParameterIivEXT"), typeof(glGetMultiTexParameterIivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultiTexParameterIivEXT'.");
            }
        }
        public static void glGetMultiTexParameterIivEXT(GLenum @texunit, GLenum @target, GLenum @pname, GLint * @params) => glGetMultiTexParameterIivEXTPtr(@texunit, @target, @pname, @params);

        internal delegate void glGetMultiTexParameterIuivEXTFunc(GLenum @texunit, GLenum @target, GLenum @pname, GLuint * @params);
        internal static glGetMultiTexParameterIuivEXTFunc glGetMultiTexParameterIuivEXTPtr;
        internal static void loadGetMultiTexParameterIuivEXT()
        {
            try
            {
                glGetMultiTexParameterIuivEXTPtr = (glGetMultiTexParameterIuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultiTexParameterIuivEXT"), typeof(glGetMultiTexParameterIuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultiTexParameterIuivEXT'.");
            }
        }
        public static void glGetMultiTexParameterIuivEXT(GLenum @texunit, GLenum @target, GLenum @pname, GLuint * @params) => glGetMultiTexParameterIuivEXTPtr(@texunit, @target, @pname, @params);

        internal delegate void glProgramUniform1uiEXTFunc(GLuint @program, GLint @location, GLuint @v0);
        internal static glProgramUniform1uiEXTFunc glProgramUniform1uiEXTPtr;
        internal static void loadProgramUniform1uiEXT()
        {
            try
            {
                glProgramUniform1uiEXTPtr = (glProgramUniform1uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1uiEXT"), typeof(glProgramUniform1uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1uiEXT'.");
            }
        }
        public static void glProgramUniform1uiEXT(GLuint @program, GLint @location, GLuint @v0) => glProgramUniform1uiEXTPtr(@program, @location, @v0);

        internal delegate void glProgramUniform2uiEXTFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1);
        internal static glProgramUniform2uiEXTFunc glProgramUniform2uiEXTPtr;
        internal static void loadProgramUniform2uiEXT()
        {
            try
            {
                glProgramUniform2uiEXTPtr = (glProgramUniform2uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2uiEXT"), typeof(glProgramUniform2uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2uiEXT'.");
            }
        }
        public static void glProgramUniform2uiEXT(GLuint @program, GLint @location, GLuint @v0, GLuint @v1) => glProgramUniform2uiEXTPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform3uiEXTFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2);
        internal static glProgramUniform3uiEXTFunc glProgramUniform3uiEXTPtr;
        internal static void loadProgramUniform3uiEXT()
        {
            try
            {
                glProgramUniform3uiEXTPtr = (glProgramUniform3uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3uiEXT"), typeof(glProgramUniform3uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3uiEXT'.");
            }
        }
        public static void glProgramUniform3uiEXT(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2) => glProgramUniform3uiEXTPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform4uiEXTFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3);
        internal static glProgramUniform4uiEXTFunc glProgramUniform4uiEXTPtr;
        internal static void loadProgramUniform4uiEXT()
        {
            try
            {
                glProgramUniform4uiEXTPtr = (glProgramUniform4uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4uiEXT"), typeof(glProgramUniform4uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4uiEXT'.");
            }
        }
        public static void glProgramUniform4uiEXT(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3) => glProgramUniform4uiEXTPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform1uivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform1uivEXTFunc glProgramUniform1uivEXTPtr;
        internal static void loadProgramUniform1uivEXT()
        {
            try
            {
                glProgramUniform1uivEXTPtr = (glProgramUniform1uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1uivEXT"), typeof(glProgramUniform1uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1uivEXT'.");
            }
        }
        public static void glProgramUniform1uivEXT(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform1uivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2uivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform2uivEXTFunc glProgramUniform2uivEXTPtr;
        internal static void loadProgramUniform2uivEXT()
        {
            try
            {
                glProgramUniform2uivEXTPtr = (glProgramUniform2uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2uivEXT"), typeof(glProgramUniform2uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2uivEXT'.");
            }
        }
        public static void glProgramUniform2uivEXT(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform2uivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3uivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform3uivEXTFunc glProgramUniform3uivEXTPtr;
        internal static void loadProgramUniform3uivEXT()
        {
            try
            {
                glProgramUniform3uivEXTPtr = (glProgramUniform3uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3uivEXT"), typeof(glProgramUniform3uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3uivEXT'.");
            }
        }
        public static void glProgramUniform3uivEXT(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform3uivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4uivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform4uivEXTFunc glProgramUniform4uivEXTPtr;
        internal static void loadProgramUniform4uivEXT()
        {
            try
            {
                glProgramUniform4uivEXTPtr = (glProgramUniform4uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4uivEXT"), typeof(glProgramUniform4uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4uivEXT'.");
            }
        }
        public static void glProgramUniform4uivEXT(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform4uivEXTPtr(@program, @location, @count, @value);

        internal delegate void glNamedProgramLocalParameters4fvEXTFunc(GLuint @program, GLenum @target, GLuint @index, GLsizei @count, const GLfloat * @params);
        internal static glNamedProgramLocalParameters4fvEXTFunc glNamedProgramLocalParameters4fvEXTPtr;
        internal static void loadNamedProgramLocalParameters4fvEXT()
        {
            try
            {
                glNamedProgramLocalParameters4fvEXTPtr = (glNamedProgramLocalParameters4fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedProgramLocalParameters4fvEXT"), typeof(glNamedProgramLocalParameters4fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedProgramLocalParameters4fvEXT'.");
            }
        }
        public static void glNamedProgramLocalParameters4fvEXT(GLuint @program, GLenum @target, GLuint @index, GLsizei @count, const GLfloat * @params) => glNamedProgramLocalParameters4fvEXTPtr(@program, @target, @index, @count, @params);

        internal delegate void glNamedProgramLocalParameterI4iEXTFunc(GLuint @program, GLenum @target, GLuint @index, GLint @x, GLint @y, GLint @z, GLint @w);
        internal static glNamedProgramLocalParameterI4iEXTFunc glNamedProgramLocalParameterI4iEXTPtr;
        internal static void loadNamedProgramLocalParameterI4iEXT()
        {
            try
            {
                glNamedProgramLocalParameterI4iEXTPtr = (glNamedProgramLocalParameterI4iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedProgramLocalParameterI4iEXT"), typeof(glNamedProgramLocalParameterI4iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedProgramLocalParameterI4iEXT'.");
            }
        }
        public static void glNamedProgramLocalParameterI4iEXT(GLuint @program, GLenum @target, GLuint @index, GLint @x, GLint @y, GLint @z, GLint @w) => glNamedProgramLocalParameterI4iEXTPtr(@program, @target, @index, @x, @y, @z, @w);

        internal delegate void glNamedProgramLocalParameterI4ivEXTFunc(GLuint @program, GLenum @target, GLuint @index, const GLint * @params);
        internal static glNamedProgramLocalParameterI4ivEXTFunc glNamedProgramLocalParameterI4ivEXTPtr;
        internal static void loadNamedProgramLocalParameterI4ivEXT()
        {
            try
            {
                glNamedProgramLocalParameterI4ivEXTPtr = (glNamedProgramLocalParameterI4ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedProgramLocalParameterI4ivEXT"), typeof(glNamedProgramLocalParameterI4ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedProgramLocalParameterI4ivEXT'.");
            }
        }
        public static void glNamedProgramLocalParameterI4ivEXT(GLuint @program, GLenum @target, GLuint @index, const GLint * @params) => glNamedProgramLocalParameterI4ivEXTPtr(@program, @target, @index, @params);

        internal delegate void glNamedProgramLocalParametersI4ivEXTFunc(GLuint @program, GLenum @target, GLuint @index, GLsizei @count, const GLint * @params);
        internal static glNamedProgramLocalParametersI4ivEXTFunc glNamedProgramLocalParametersI4ivEXTPtr;
        internal static void loadNamedProgramLocalParametersI4ivEXT()
        {
            try
            {
                glNamedProgramLocalParametersI4ivEXTPtr = (glNamedProgramLocalParametersI4ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedProgramLocalParametersI4ivEXT"), typeof(glNamedProgramLocalParametersI4ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedProgramLocalParametersI4ivEXT'.");
            }
        }
        public static void glNamedProgramLocalParametersI4ivEXT(GLuint @program, GLenum @target, GLuint @index, GLsizei @count, const GLint * @params) => glNamedProgramLocalParametersI4ivEXTPtr(@program, @target, @index, @count, @params);

        internal delegate void glNamedProgramLocalParameterI4uiEXTFunc(GLuint @program, GLenum @target, GLuint @index, GLuint @x, GLuint @y, GLuint @z, GLuint @w);
        internal static glNamedProgramLocalParameterI4uiEXTFunc glNamedProgramLocalParameterI4uiEXTPtr;
        internal static void loadNamedProgramLocalParameterI4uiEXT()
        {
            try
            {
                glNamedProgramLocalParameterI4uiEXTPtr = (glNamedProgramLocalParameterI4uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedProgramLocalParameterI4uiEXT"), typeof(glNamedProgramLocalParameterI4uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedProgramLocalParameterI4uiEXT'.");
            }
        }
        public static void glNamedProgramLocalParameterI4uiEXT(GLuint @program, GLenum @target, GLuint @index, GLuint @x, GLuint @y, GLuint @z, GLuint @w) => glNamedProgramLocalParameterI4uiEXTPtr(@program, @target, @index, @x, @y, @z, @w);

        internal delegate void glNamedProgramLocalParameterI4uivEXTFunc(GLuint @program, GLenum @target, GLuint @index, const GLuint * @params);
        internal static glNamedProgramLocalParameterI4uivEXTFunc glNamedProgramLocalParameterI4uivEXTPtr;
        internal static void loadNamedProgramLocalParameterI4uivEXT()
        {
            try
            {
                glNamedProgramLocalParameterI4uivEXTPtr = (glNamedProgramLocalParameterI4uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedProgramLocalParameterI4uivEXT"), typeof(glNamedProgramLocalParameterI4uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedProgramLocalParameterI4uivEXT'.");
            }
        }
        public static void glNamedProgramLocalParameterI4uivEXT(GLuint @program, GLenum @target, GLuint @index, const GLuint * @params) => glNamedProgramLocalParameterI4uivEXTPtr(@program, @target, @index, @params);

        internal delegate void glNamedProgramLocalParametersI4uivEXTFunc(GLuint @program, GLenum @target, GLuint @index, GLsizei @count, const GLuint * @params);
        internal static glNamedProgramLocalParametersI4uivEXTFunc glNamedProgramLocalParametersI4uivEXTPtr;
        internal static void loadNamedProgramLocalParametersI4uivEXT()
        {
            try
            {
                glNamedProgramLocalParametersI4uivEXTPtr = (glNamedProgramLocalParametersI4uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedProgramLocalParametersI4uivEXT"), typeof(glNamedProgramLocalParametersI4uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedProgramLocalParametersI4uivEXT'.");
            }
        }
        public static void glNamedProgramLocalParametersI4uivEXT(GLuint @program, GLenum @target, GLuint @index, GLsizei @count, const GLuint * @params) => glNamedProgramLocalParametersI4uivEXTPtr(@program, @target, @index, @count, @params);

        internal delegate void glGetNamedProgramLocalParameterIivEXTFunc(GLuint @program, GLenum @target, GLuint @index, GLint * @params);
        internal static glGetNamedProgramLocalParameterIivEXTFunc glGetNamedProgramLocalParameterIivEXTPtr;
        internal static void loadGetNamedProgramLocalParameterIivEXT()
        {
            try
            {
                glGetNamedProgramLocalParameterIivEXTPtr = (glGetNamedProgramLocalParameterIivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedProgramLocalParameterIivEXT"), typeof(glGetNamedProgramLocalParameterIivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedProgramLocalParameterIivEXT'.");
            }
        }
        public static void glGetNamedProgramLocalParameterIivEXT(GLuint @program, GLenum @target, GLuint @index, GLint * @params) => glGetNamedProgramLocalParameterIivEXTPtr(@program, @target, @index, @params);

        internal delegate void glGetNamedProgramLocalParameterIuivEXTFunc(GLuint @program, GLenum @target, GLuint @index, GLuint * @params);
        internal static glGetNamedProgramLocalParameterIuivEXTFunc glGetNamedProgramLocalParameterIuivEXTPtr;
        internal static void loadGetNamedProgramLocalParameterIuivEXT()
        {
            try
            {
                glGetNamedProgramLocalParameterIuivEXTPtr = (glGetNamedProgramLocalParameterIuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedProgramLocalParameterIuivEXT"), typeof(glGetNamedProgramLocalParameterIuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedProgramLocalParameterIuivEXT'.");
            }
        }
        public static void glGetNamedProgramLocalParameterIuivEXT(GLuint @program, GLenum @target, GLuint @index, GLuint * @params) => glGetNamedProgramLocalParameterIuivEXTPtr(@program, @target, @index, @params);

        internal delegate void glEnableClientStateiEXTFunc(GLenum @array, GLuint @index);
        internal static glEnableClientStateiEXTFunc glEnableClientStateiEXTPtr;
        internal static void loadEnableClientStateiEXT()
        {
            try
            {
                glEnableClientStateiEXTPtr = (glEnableClientStateiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableClientStateiEXT"), typeof(glEnableClientStateiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableClientStateiEXT'.");
            }
        }
        public static void glEnableClientStateiEXT(GLenum @array, GLuint @index) => glEnableClientStateiEXTPtr(@array, @index);

        internal delegate void glDisableClientStateiEXTFunc(GLenum @array, GLuint @index);
        internal static glDisableClientStateiEXTFunc glDisableClientStateiEXTPtr;
        internal static void loadDisableClientStateiEXT()
        {
            try
            {
                glDisableClientStateiEXTPtr = (glDisableClientStateiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableClientStateiEXT"), typeof(glDisableClientStateiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableClientStateiEXT'.");
            }
        }
        public static void glDisableClientStateiEXT(GLenum @array, GLuint @index) => glDisableClientStateiEXTPtr(@array, @index);

        internal delegate void glGetFloati_vEXTFunc(GLenum @pname, GLuint @index, GLfloat * @params);
        internal static glGetFloati_vEXTFunc glGetFloati_vEXTPtr;
        internal static void loadGetFloati_vEXT()
        {
            try
            {
                glGetFloati_vEXTPtr = (glGetFloati_vEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFloati_vEXT"), typeof(glGetFloati_vEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFloati_vEXT'.");
            }
        }
        public static void glGetFloati_vEXT(GLenum @pname, GLuint @index, GLfloat * @params) => glGetFloati_vEXTPtr(@pname, @index, @params);

        internal delegate void glGetDoublei_vEXTFunc(GLenum @pname, GLuint @index, GLdouble * @params);
        internal static glGetDoublei_vEXTFunc glGetDoublei_vEXTPtr;
        internal static void loadGetDoublei_vEXT()
        {
            try
            {
                glGetDoublei_vEXTPtr = (glGetDoublei_vEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDoublei_vEXT"), typeof(glGetDoublei_vEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDoublei_vEXT'.");
            }
        }
        public static void glGetDoublei_vEXT(GLenum @pname, GLuint @index, GLdouble * @params) => glGetDoublei_vEXTPtr(@pname, @index, @params);

        internal delegate void glGetPointeri_vEXTFunc(GLenum @pname, GLuint @index, void ** @params);
        internal static glGetPointeri_vEXTFunc glGetPointeri_vEXTPtr;
        internal static void loadGetPointeri_vEXT()
        {
            try
            {
                glGetPointeri_vEXTPtr = (glGetPointeri_vEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPointeri_vEXT"), typeof(glGetPointeri_vEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPointeri_vEXT'.");
            }
        }
        public static void glGetPointeri_vEXT(GLenum @pname, GLuint @index, void ** @params) => glGetPointeri_vEXTPtr(@pname, @index, @params);

        internal delegate void glNamedProgramStringEXTFunc(GLuint @program, GLenum @target, GLenum @format, GLsizei @len, const void * @string);
        internal static glNamedProgramStringEXTFunc glNamedProgramStringEXTPtr;
        internal static void loadNamedProgramStringEXT()
        {
            try
            {
                glNamedProgramStringEXTPtr = (glNamedProgramStringEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedProgramStringEXT"), typeof(glNamedProgramStringEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedProgramStringEXT'.");
            }
        }
        public static void glNamedProgramStringEXT(GLuint @program, GLenum @target, GLenum @format, GLsizei @len, const void * @string) => glNamedProgramStringEXTPtr(@program, @target, @format, @len, @string);

        internal delegate void glNamedProgramLocalParameter4dEXTFunc(GLuint @program, GLenum @target, GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glNamedProgramLocalParameter4dEXTFunc glNamedProgramLocalParameter4dEXTPtr;
        internal static void loadNamedProgramLocalParameter4dEXT()
        {
            try
            {
                glNamedProgramLocalParameter4dEXTPtr = (glNamedProgramLocalParameter4dEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedProgramLocalParameter4dEXT"), typeof(glNamedProgramLocalParameter4dEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedProgramLocalParameter4dEXT'.");
            }
        }
        public static void glNamedProgramLocalParameter4dEXT(GLuint @program, GLenum @target, GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glNamedProgramLocalParameter4dEXTPtr(@program, @target, @index, @x, @y, @z, @w);

        internal delegate void glNamedProgramLocalParameter4dvEXTFunc(GLuint @program, GLenum @target, GLuint @index, const GLdouble * @params);
        internal static glNamedProgramLocalParameter4dvEXTFunc glNamedProgramLocalParameter4dvEXTPtr;
        internal static void loadNamedProgramLocalParameter4dvEXT()
        {
            try
            {
                glNamedProgramLocalParameter4dvEXTPtr = (glNamedProgramLocalParameter4dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedProgramLocalParameter4dvEXT"), typeof(glNamedProgramLocalParameter4dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedProgramLocalParameter4dvEXT'.");
            }
        }
        public static void glNamedProgramLocalParameter4dvEXT(GLuint @program, GLenum @target, GLuint @index, const GLdouble * @params) => glNamedProgramLocalParameter4dvEXTPtr(@program, @target, @index, @params);

        internal delegate void glNamedProgramLocalParameter4fEXTFunc(GLuint @program, GLenum @target, GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glNamedProgramLocalParameter4fEXTFunc glNamedProgramLocalParameter4fEXTPtr;
        internal static void loadNamedProgramLocalParameter4fEXT()
        {
            try
            {
                glNamedProgramLocalParameter4fEXTPtr = (glNamedProgramLocalParameter4fEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedProgramLocalParameter4fEXT"), typeof(glNamedProgramLocalParameter4fEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedProgramLocalParameter4fEXT'.");
            }
        }
        public static void glNamedProgramLocalParameter4fEXT(GLuint @program, GLenum @target, GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glNamedProgramLocalParameter4fEXTPtr(@program, @target, @index, @x, @y, @z, @w);

        internal delegate void glNamedProgramLocalParameter4fvEXTFunc(GLuint @program, GLenum @target, GLuint @index, const GLfloat * @params);
        internal static glNamedProgramLocalParameter4fvEXTFunc glNamedProgramLocalParameter4fvEXTPtr;
        internal static void loadNamedProgramLocalParameter4fvEXT()
        {
            try
            {
                glNamedProgramLocalParameter4fvEXTPtr = (glNamedProgramLocalParameter4fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedProgramLocalParameter4fvEXT"), typeof(glNamedProgramLocalParameter4fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedProgramLocalParameter4fvEXT'.");
            }
        }
        public static void glNamedProgramLocalParameter4fvEXT(GLuint @program, GLenum @target, GLuint @index, const GLfloat * @params) => glNamedProgramLocalParameter4fvEXTPtr(@program, @target, @index, @params);

        internal delegate void glGetNamedProgramLocalParameterdvEXTFunc(GLuint @program, GLenum @target, GLuint @index, GLdouble * @params);
        internal static glGetNamedProgramLocalParameterdvEXTFunc glGetNamedProgramLocalParameterdvEXTPtr;
        internal static void loadGetNamedProgramLocalParameterdvEXT()
        {
            try
            {
                glGetNamedProgramLocalParameterdvEXTPtr = (glGetNamedProgramLocalParameterdvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedProgramLocalParameterdvEXT"), typeof(glGetNamedProgramLocalParameterdvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedProgramLocalParameterdvEXT'.");
            }
        }
        public static void glGetNamedProgramLocalParameterdvEXT(GLuint @program, GLenum @target, GLuint @index, GLdouble * @params) => glGetNamedProgramLocalParameterdvEXTPtr(@program, @target, @index, @params);

        internal delegate void glGetNamedProgramLocalParameterfvEXTFunc(GLuint @program, GLenum @target, GLuint @index, GLfloat * @params);
        internal static glGetNamedProgramLocalParameterfvEXTFunc glGetNamedProgramLocalParameterfvEXTPtr;
        internal static void loadGetNamedProgramLocalParameterfvEXT()
        {
            try
            {
                glGetNamedProgramLocalParameterfvEXTPtr = (glGetNamedProgramLocalParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedProgramLocalParameterfvEXT"), typeof(glGetNamedProgramLocalParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedProgramLocalParameterfvEXT'.");
            }
        }
        public static void glGetNamedProgramLocalParameterfvEXT(GLuint @program, GLenum @target, GLuint @index, GLfloat * @params) => glGetNamedProgramLocalParameterfvEXTPtr(@program, @target, @index, @params);

        internal delegate void glGetNamedProgramivEXTFunc(GLuint @program, GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetNamedProgramivEXTFunc glGetNamedProgramivEXTPtr;
        internal static void loadGetNamedProgramivEXT()
        {
            try
            {
                glGetNamedProgramivEXTPtr = (glGetNamedProgramivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedProgramivEXT"), typeof(glGetNamedProgramivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedProgramivEXT'.");
            }
        }
        public static void glGetNamedProgramivEXT(GLuint @program, GLenum @target, GLenum @pname, GLint * @params) => glGetNamedProgramivEXTPtr(@program, @target, @pname, @params);

        internal delegate void glGetNamedProgramStringEXTFunc(GLuint @program, GLenum @target, GLenum @pname, void * @string);
        internal static glGetNamedProgramStringEXTFunc glGetNamedProgramStringEXTPtr;
        internal static void loadGetNamedProgramStringEXT()
        {
            try
            {
                glGetNamedProgramStringEXTPtr = (glGetNamedProgramStringEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedProgramStringEXT"), typeof(glGetNamedProgramStringEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedProgramStringEXT'.");
            }
        }
        public static void glGetNamedProgramStringEXT(GLuint @program, GLenum @target, GLenum @pname, void * @string) => glGetNamedProgramStringEXTPtr(@program, @target, @pname, @string);

        internal delegate void glNamedRenderbufferStorageEXTFunc(GLuint @renderbuffer, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glNamedRenderbufferStorageEXTFunc glNamedRenderbufferStorageEXTPtr;
        internal static void loadNamedRenderbufferStorageEXT()
        {
            try
            {
                glNamedRenderbufferStorageEXTPtr = (glNamedRenderbufferStorageEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedRenderbufferStorageEXT"), typeof(glNamedRenderbufferStorageEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedRenderbufferStorageEXT'.");
            }
        }
        public static void glNamedRenderbufferStorageEXT(GLuint @renderbuffer, GLenum @internalformat, GLsizei @width, GLsizei @height) => glNamedRenderbufferStorageEXTPtr(@renderbuffer, @internalformat, @width, @height);

        internal delegate void glGetNamedRenderbufferParameterivEXTFunc(GLuint @renderbuffer, GLenum @pname, GLint * @params);
        internal static glGetNamedRenderbufferParameterivEXTFunc glGetNamedRenderbufferParameterivEXTPtr;
        internal static void loadGetNamedRenderbufferParameterivEXT()
        {
            try
            {
                glGetNamedRenderbufferParameterivEXTPtr = (glGetNamedRenderbufferParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedRenderbufferParameterivEXT"), typeof(glGetNamedRenderbufferParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedRenderbufferParameterivEXT'.");
            }
        }
        public static void glGetNamedRenderbufferParameterivEXT(GLuint @renderbuffer, GLenum @pname, GLint * @params) => glGetNamedRenderbufferParameterivEXTPtr(@renderbuffer, @pname, @params);

        internal delegate void glNamedRenderbufferStorageMultisampleEXTFunc(GLuint @renderbuffer, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glNamedRenderbufferStorageMultisampleEXTFunc glNamedRenderbufferStorageMultisampleEXTPtr;
        internal static void loadNamedRenderbufferStorageMultisampleEXT()
        {
            try
            {
                glNamedRenderbufferStorageMultisampleEXTPtr = (glNamedRenderbufferStorageMultisampleEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedRenderbufferStorageMultisampleEXT"), typeof(glNamedRenderbufferStorageMultisampleEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedRenderbufferStorageMultisampleEXT'.");
            }
        }
        public static void glNamedRenderbufferStorageMultisampleEXT(GLuint @renderbuffer, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glNamedRenderbufferStorageMultisampleEXTPtr(@renderbuffer, @samples, @internalformat, @width, @height);

        internal delegate void glNamedRenderbufferStorageMultisampleCoverageEXTFunc(GLuint @renderbuffer, GLsizei @coverageSamples, GLsizei @colorSamples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glNamedRenderbufferStorageMultisampleCoverageEXTFunc glNamedRenderbufferStorageMultisampleCoverageEXTPtr;
        internal static void loadNamedRenderbufferStorageMultisampleCoverageEXT()
        {
            try
            {
                glNamedRenderbufferStorageMultisampleCoverageEXTPtr = (glNamedRenderbufferStorageMultisampleCoverageEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedRenderbufferStorageMultisampleCoverageEXT"), typeof(glNamedRenderbufferStorageMultisampleCoverageEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedRenderbufferStorageMultisampleCoverageEXT'.");
            }
        }
        public static void glNamedRenderbufferStorageMultisampleCoverageEXT(GLuint @renderbuffer, GLsizei @coverageSamples, GLsizei @colorSamples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glNamedRenderbufferStorageMultisampleCoverageEXTPtr(@renderbuffer, @coverageSamples, @colorSamples, @internalformat, @width, @height);

        internal delegate GLenum glCheckNamedFramebufferStatusEXTFunc(GLuint @framebuffer, GLenum @target);
        internal static glCheckNamedFramebufferStatusEXTFunc glCheckNamedFramebufferStatusEXTPtr;
        internal static void loadCheckNamedFramebufferStatusEXT()
        {
            try
            {
                glCheckNamedFramebufferStatusEXTPtr = (glCheckNamedFramebufferStatusEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCheckNamedFramebufferStatusEXT"), typeof(glCheckNamedFramebufferStatusEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCheckNamedFramebufferStatusEXT'.");
            }
        }
        public static GLenum glCheckNamedFramebufferStatusEXT(GLuint @framebuffer, GLenum @target) => glCheckNamedFramebufferStatusEXTPtr(@framebuffer, @target);

        internal delegate void glNamedFramebufferTexture1DEXTFunc(GLuint @framebuffer, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level);
        internal static glNamedFramebufferTexture1DEXTFunc glNamedFramebufferTexture1DEXTPtr;
        internal static void loadNamedFramebufferTexture1DEXT()
        {
            try
            {
                glNamedFramebufferTexture1DEXTPtr = (glNamedFramebufferTexture1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferTexture1DEXT"), typeof(glNamedFramebufferTexture1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferTexture1DEXT'.");
            }
        }
        public static void glNamedFramebufferTexture1DEXT(GLuint @framebuffer, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level) => glNamedFramebufferTexture1DEXTPtr(@framebuffer, @attachment, @textarget, @texture, @level);

        internal delegate void glNamedFramebufferTexture2DEXTFunc(GLuint @framebuffer, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level);
        internal static glNamedFramebufferTexture2DEXTFunc glNamedFramebufferTexture2DEXTPtr;
        internal static void loadNamedFramebufferTexture2DEXT()
        {
            try
            {
                glNamedFramebufferTexture2DEXTPtr = (glNamedFramebufferTexture2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferTexture2DEXT"), typeof(glNamedFramebufferTexture2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferTexture2DEXT'.");
            }
        }
        public static void glNamedFramebufferTexture2DEXT(GLuint @framebuffer, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level) => glNamedFramebufferTexture2DEXTPtr(@framebuffer, @attachment, @textarget, @texture, @level);

        internal delegate void glNamedFramebufferTexture3DEXTFunc(GLuint @framebuffer, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLint @zoffset);
        internal static glNamedFramebufferTexture3DEXTFunc glNamedFramebufferTexture3DEXTPtr;
        internal static void loadNamedFramebufferTexture3DEXT()
        {
            try
            {
                glNamedFramebufferTexture3DEXTPtr = (glNamedFramebufferTexture3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferTexture3DEXT"), typeof(glNamedFramebufferTexture3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferTexture3DEXT'.");
            }
        }
        public static void glNamedFramebufferTexture3DEXT(GLuint @framebuffer, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLint @zoffset) => glNamedFramebufferTexture3DEXTPtr(@framebuffer, @attachment, @textarget, @texture, @level, @zoffset);

        internal delegate void glNamedFramebufferRenderbufferEXTFunc(GLuint @framebuffer, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer);
        internal static glNamedFramebufferRenderbufferEXTFunc glNamedFramebufferRenderbufferEXTPtr;
        internal static void loadNamedFramebufferRenderbufferEXT()
        {
            try
            {
                glNamedFramebufferRenderbufferEXTPtr = (glNamedFramebufferRenderbufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferRenderbufferEXT"), typeof(glNamedFramebufferRenderbufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferRenderbufferEXT'.");
            }
        }
        public static void glNamedFramebufferRenderbufferEXT(GLuint @framebuffer, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer) => glNamedFramebufferRenderbufferEXTPtr(@framebuffer, @attachment, @renderbuffertarget, @renderbuffer);

        internal delegate void glGetNamedFramebufferAttachmentParameterivEXTFunc(GLuint @framebuffer, GLenum @attachment, GLenum @pname, GLint * @params);
        internal static glGetNamedFramebufferAttachmentParameterivEXTFunc glGetNamedFramebufferAttachmentParameterivEXTPtr;
        internal static void loadGetNamedFramebufferAttachmentParameterivEXT()
        {
            try
            {
                glGetNamedFramebufferAttachmentParameterivEXTPtr = (glGetNamedFramebufferAttachmentParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedFramebufferAttachmentParameterivEXT"), typeof(glGetNamedFramebufferAttachmentParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedFramebufferAttachmentParameterivEXT'.");
            }
        }
        public static void glGetNamedFramebufferAttachmentParameterivEXT(GLuint @framebuffer, GLenum @attachment, GLenum @pname, GLint * @params) => glGetNamedFramebufferAttachmentParameterivEXTPtr(@framebuffer, @attachment, @pname, @params);

        internal delegate void glGenerateTextureMipmapEXTFunc(GLuint @texture, GLenum @target);
        internal static glGenerateTextureMipmapEXTFunc glGenerateTextureMipmapEXTPtr;
        internal static void loadGenerateTextureMipmapEXT()
        {
            try
            {
                glGenerateTextureMipmapEXTPtr = (glGenerateTextureMipmapEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenerateTextureMipmapEXT"), typeof(glGenerateTextureMipmapEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenerateTextureMipmapEXT'.");
            }
        }
        public static void glGenerateTextureMipmapEXT(GLuint @texture, GLenum @target) => glGenerateTextureMipmapEXTPtr(@texture, @target);

        internal delegate void glGenerateMultiTexMipmapEXTFunc(GLenum @texunit, GLenum @target);
        internal static glGenerateMultiTexMipmapEXTFunc glGenerateMultiTexMipmapEXTPtr;
        internal static void loadGenerateMultiTexMipmapEXT()
        {
            try
            {
                glGenerateMultiTexMipmapEXTPtr = (glGenerateMultiTexMipmapEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenerateMultiTexMipmapEXT"), typeof(glGenerateMultiTexMipmapEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenerateMultiTexMipmapEXT'.");
            }
        }
        public static void glGenerateMultiTexMipmapEXT(GLenum @texunit, GLenum @target) => glGenerateMultiTexMipmapEXTPtr(@texunit, @target);

        internal delegate void glFramebufferDrawBufferEXTFunc(GLuint @framebuffer, GLenum @mode);
        internal static glFramebufferDrawBufferEXTFunc glFramebufferDrawBufferEXTPtr;
        internal static void loadFramebufferDrawBufferEXT()
        {
            try
            {
                glFramebufferDrawBufferEXTPtr = (glFramebufferDrawBufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferDrawBufferEXT"), typeof(glFramebufferDrawBufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferDrawBufferEXT'.");
            }
        }
        public static void glFramebufferDrawBufferEXT(GLuint @framebuffer, GLenum @mode) => glFramebufferDrawBufferEXTPtr(@framebuffer, @mode);

        internal delegate void glFramebufferDrawBuffersEXTFunc(GLuint @framebuffer, GLsizei @n, const GLenum * @bufs);
        internal static glFramebufferDrawBuffersEXTFunc glFramebufferDrawBuffersEXTPtr;
        internal static void loadFramebufferDrawBuffersEXT()
        {
            try
            {
                glFramebufferDrawBuffersEXTPtr = (glFramebufferDrawBuffersEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferDrawBuffersEXT"), typeof(glFramebufferDrawBuffersEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferDrawBuffersEXT'.");
            }
        }
        public static void glFramebufferDrawBuffersEXT(GLuint @framebuffer, GLsizei @n, const GLenum * @bufs) => glFramebufferDrawBuffersEXTPtr(@framebuffer, @n, @bufs);

        internal delegate void glFramebufferReadBufferEXTFunc(GLuint @framebuffer, GLenum @mode);
        internal static glFramebufferReadBufferEXTFunc glFramebufferReadBufferEXTPtr;
        internal static void loadFramebufferReadBufferEXT()
        {
            try
            {
                glFramebufferReadBufferEXTPtr = (glFramebufferReadBufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferReadBufferEXT"), typeof(glFramebufferReadBufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferReadBufferEXT'.");
            }
        }
        public static void glFramebufferReadBufferEXT(GLuint @framebuffer, GLenum @mode) => glFramebufferReadBufferEXTPtr(@framebuffer, @mode);

        internal delegate void glGetFramebufferParameterivEXTFunc(GLuint @framebuffer, GLenum @pname, GLint * @params);
        internal static glGetFramebufferParameterivEXTFunc glGetFramebufferParameterivEXTPtr;
        internal static void loadGetFramebufferParameterivEXT()
        {
            try
            {
                glGetFramebufferParameterivEXTPtr = (glGetFramebufferParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFramebufferParameterivEXT"), typeof(glGetFramebufferParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFramebufferParameterivEXT'.");
            }
        }
        public static void glGetFramebufferParameterivEXT(GLuint @framebuffer, GLenum @pname, GLint * @params) => glGetFramebufferParameterivEXTPtr(@framebuffer, @pname, @params);

        internal delegate void glNamedCopyBufferSubDataEXTFunc(GLuint @readBuffer, GLuint @writeBuffer, GLintptr @readOffset, GLintptr @writeOffset, GLsizeiptr @size);
        internal static glNamedCopyBufferSubDataEXTFunc glNamedCopyBufferSubDataEXTPtr;
        internal static void loadNamedCopyBufferSubDataEXT()
        {
            try
            {
                glNamedCopyBufferSubDataEXTPtr = (glNamedCopyBufferSubDataEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedCopyBufferSubDataEXT"), typeof(glNamedCopyBufferSubDataEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedCopyBufferSubDataEXT'.");
            }
        }
        public static void glNamedCopyBufferSubDataEXT(GLuint @readBuffer, GLuint @writeBuffer, GLintptr @readOffset, GLintptr @writeOffset, GLsizeiptr @size) => glNamedCopyBufferSubDataEXTPtr(@readBuffer, @writeBuffer, @readOffset, @writeOffset, @size);

        internal delegate void glNamedFramebufferTextureEXTFunc(GLuint @framebuffer, GLenum @attachment, GLuint @texture, GLint @level);
        internal static glNamedFramebufferTextureEXTFunc glNamedFramebufferTextureEXTPtr;
        internal static void loadNamedFramebufferTextureEXT()
        {
            try
            {
                glNamedFramebufferTextureEXTPtr = (glNamedFramebufferTextureEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferTextureEXT"), typeof(glNamedFramebufferTextureEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferTextureEXT'.");
            }
        }
        public static void glNamedFramebufferTextureEXT(GLuint @framebuffer, GLenum @attachment, GLuint @texture, GLint @level) => glNamedFramebufferTextureEXTPtr(@framebuffer, @attachment, @texture, @level);

        internal delegate void glNamedFramebufferTextureLayerEXTFunc(GLuint @framebuffer, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer);
        internal static glNamedFramebufferTextureLayerEXTFunc glNamedFramebufferTextureLayerEXTPtr;
        internal static void loadNamedFramebufferTextureLayerEXT()
        {
            try
            {
                glNamedFramebufferTextureLayerEXTPtr = (glNamedFramebufferTextureLayerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferTextureLayerEXT"), typeof(glNamedFramebufferTextureLayerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferTextureLayerEXT'.");
            }
        }
        public static void glNamedFramebufferTextureLayerEXT(GLuint @framebuffer, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer) => glNamedFramebufferTextureLayerEXTPtr(@framebuffer, @attachment, @texture, @level, @layer);

        internal delegate void glNamedFramebufferTextureFaceEXTFunc(GLuint @framebuffer, GLenum @attachment, GLuint @texture, GLint @level, GLenum @face);
        internal static glNamedFramebufferTextureFaceEXTFunc glNamedFramebufferTextureFaceEXTPtr;
        internal static void loadNamedFramebufferTextureFaceEXT()
        {
            try
            {
                glNamedFramebufferTextureFaceEXTPtr = (glNamedFramebufferTextureFaceEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferTextureFaceEXT"), typeof(glNamedFramebufferTextureFaceEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferTextureFaceEXT'.");
            }
        }
        public static void glNamedFramebufferTextureFaceEXT(GLuint @framebuffer, GLenum @attachment, GLuint @texture, GLint @level, GLenum @face) => glNamedFramebufferTextureFaceEXTPtr(@framebuffer, @attachment, @texture, @level, @face);

        internal delegate void glTextureRenderbufferEXTFunc(GLuint @texture, GLenum @target, GLuint @renderbuffer);
        internal static glTextureRenderbufferEXTFunc glTextureRenderbufferEXTPtr;
        internal static void loadTextureRenderbufferEXT()
        {
            try
            {
                glTextureRenderbufferEXTPtr = (glTextureRenderbufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureRenderbufferEXT"), typeof(glTextureRenderbufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureRenderbufferEXT'.");
            }
        }
        public static void glTextureRenderbufferEXT(GLuint @texture, GLenum @target, GLuint @renderbuffer) => glTextureRenderbufferEXTPtr(@texture, @target, @renderbuffer);

        internal delegate void glMultiTexRenderbufferEXTFunc(GLenum @texunit, GLenum @target, GLuint @renderbuffer);
        internal static glMultiTexRenderbufferEXTFunc glMultiTexRenderbufferEXTPtr;
        internal static void loadMultiTexRenderbufferEXT()
        {
            try
            {
                glMultiTexRenderbufferEXTPtr = (glMultiTexRenderbufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexRenderbufferEXT"), typeof(glMultiTexRenderbufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexRenderbufferEXT'.");
            }
        }
        public static void glMultiTexRenderbufferEXT(GLenum @texunit, GLenum @target, GLuint @renderbuffer) => glMultiTexRenderbufferEXTPtr(@texunit, @target, @renderbuffer);

        internal delegate void glVertexArrayVertexOffsetEXTFunc(GLuint @vaobj, GLuint @buffer, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset);
        internal static glVertexArrayVertexOffsetEXTFunc glVertexArrayVertexOffsetEXTPtr;
        internal static void loadVertexArrayVertexOffsetEXT()
        {
            try
            {
                glVertexArrayVertexOffsetEXTPtr = (glVertexArrayVertexOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayVertexOffsetEXT"), typeof(glVertexArrayVertexOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayVertexOffsetEXT'.");
            }
        }
        public static void glVertexArrayVertexOffsetEXT(GLuint @vaobj, GLuint @buffer, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset) => glVertexArrayVertexOffsetEXTPtr(@vaobj, @buffer, @size, @type, @stride, @offset);

        internal delegate void glVertexArrayColorOffsetEXTFunc(GLuint @vaobj, GLuint @buffer, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset);
        internal static glVertexArrayColorOffsetEXTFunc glVertexArrayColorOffsetEXTPtr;
        internal static void loadVertexArrayColorOffsetEXT()
        {
            try
            {
                glVertexArrayColorOffsetEXTPtr = (glVertexArrayColorOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayColorOffsetEXT"), typeof(glVertexArrayColorOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayColorOffsetEXT'.");
            }
        }
        public static void glVertexArrayColorOffsetEXT(GLuint @vaobj, GLuint @buffer, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset) => glVertexArrayColorOffsetEXTPtr(@vaobj, @buffer, @size, @type, @stride, @offset);

        internal delegate void glVertexArrayEdgeFlagOffsetEXTFunc(GLuint @vaobj, GLuint @buffer, GLsizei @stride, GLintptr @offset);
        internal static glVertexArrayEdgeFlagOffsetEXTFunc glVertexArrayEdgeFlagOffsetEXTPtr;
        internal static void loadVertexArrayEdgeFlagOffsetEXT()
        {
            try
            {
                glVertexArrayEdgeFlagOffsetEXTPtr = (glVertexArrayEdgeFlagOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayEdgeFlagOffsetEXT"), typeof(glVertexArrayEdgeFlagOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayEdgeFlagOffsetEXT'.");
            }
        }
        public static void glVertexArrayEdgeFlagOffsetEXT(GLuint @vaobj, GLuint @buffer, GLsizei @stride, GLintptr @offset) => glVertexArrayEdgeFlagOffsetEXTPtr(@vaobj, @buffer, @stride, @offset);

        internal delegate void glVertexArrayIndexOffsetEXTFunc(GLuint @vaobj, GLuint @buffer, GLenum @type, GLsizei @stride, GLintptr @offset);
        internal static glVertexArrayIndexOffsetEXTFunc glVertexArrayIndexOffsetEXTPtr;
        internal static void loadVertexArrayIndexOffsetEXT()
        {
            try
            {
                glVertexArrayIndexOffsetEXTPtr = (glVertexArrayIndexOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayIndexOffsetEXT"), typeof(glVertexArrayIndexOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayIndexOffsetEXT'.");
            }
        }
        public static void glVertexArrayIndexOffsetEXT(GLuint @vaobj, GLuint @buffer, GLenum @type, GLsizei @stride, GLintptr @offset) => glVertexArrayIndexOffsetEXTPtr(@vaobj, @buffer, @type, @stride, @offset);

        internal delegate void glVertexArrayNormalOffsetEXTFunc(GLuint @vaobj, GLuint @buffer, GLenum @type, GLsizei @stride, GLintptr @offset);
        internal static glVertexArrayNormalOffsetEXTFunc glVertexArrayNormalOffsetEXTPtr;
        internal static void loadVertexArrayNormalOffsetEXT()
        {
            try
            {
                glVertexArrayNormalOffsetEXTPtr = (glVertexArrayNormalOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayNormalOffsetEXT"), typeof(glVertexArrayNormalOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayNormalOffsetEXT'.");
            }
        }
        public static void glVertexArrayNormalOffsetEXT(GLuint @vaobj, GLuint @buffer, GLenum @type, GLsizei @stride, GLintptr @offset) => glVertexArrayNormalOffsetEXTPtr(@vaobj, @buffer, @type, @stride, @offset);

        internal delegate void glVertexArrayTexCoordOffsetEXTFunc(GLuint @vaobj, GLuint @buffer, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset);
        internal static glVertexArrayTexCoordOffsetEXTFunc glVertexArrayTexCoordOffsetEXTPtr;
        internal static void loadVertexArrayTexCoordOffsetEXT()
        {
            try
            {
                glVertexArrayTexCoordOffsetEXTPtr = (glVertexArrayTexCoordOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayTexCoordOffsetEXT"), typeof(glVertexArrayTexCoordOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayTexCoordOffsetEXT'.");
            }
        }
        public static void glVertexArrayTexCoordOffsetEXT(GLuint @vaobj, GLuint @buffer, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset) => glVertexArrayTexCoordOffsetEXTPtr(@vaobj, @buffer, @size, @type, @stride, @offset);

        internal delegate void glVertexArrayMultiTexCoordOffsetEXTFunc(GLuint @vaobj, GLuint @buffer, GLenum @texunit, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset);
        internal static glVertexArrayMultiTexCoordOffsetEXTFunc glVertexArrayMultiTexCoordOffsetEXTPtr;
        internal static void loadVertexArrayMultiTexCoordOffsetEXT()
        {
            try
            {
                glVertexArrayMultiTexCoordOffsetEXTPtr = (glVertexArrayMultiTexCoordOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayMultiTexCoordOffsetEXT"), typeof(glVertexArrayMultiTexCoordOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayMultiTexCoordOffsetEXT'.");
            }
        }
        public static void glVertexArrayMultiTexCoordOffsetEXT(GLuint @vaobj, GLuint @buffer, GLenum @texunit, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset) => glVertexArrayMultiTexCoordOffsetEXTPtr(@vaobj, @buffer, @texunit, @size, @type, @stride, @offset);

        internal delegate void glVertexArrayFogCoordOffsetEXTFunc(GLuint @vaobj, GLuint @buffer, GLenum @type, GLsizei @stride, GLintptr @offset);
        internal static glVertexArrayFogCoordOffsetEXTFunc glVertexArrayFogCoordOffsetEXTPtr;
        internal static void loadVertexArrayFogCoordOffsetEXT()
        {
            try
            {
                glVertexArrayFogCoordOffsetEXTPtr = (glVertexArrayFogCoordOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayFogCoordOffsetEXT"), typeof(glVertexArrayFogCoordOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayFogCoordOffsetEXT'.");
            }
        }
        public static void glVertexArrayFogCoordOffsetEXT(GLuint @vaobj, GLuint @buffer, GLenum @type, GLsizei @stride, GLintptr @offset) => glVertexArrayFogCoordOffsetEXTPtr(@vaobj, @buffer, @type, @stride, @offset);

        internal delegate void glVertexArraySecondaryColorOffsetEXTFunc(GLuint @vaobj, GLuint @buffer, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset);
        internal static glVertexArraySecondaryColorOffsetEXTFunc glVertexArraySecondaryColorOffsetEXTPtr;
        internal static void loadVertexArraySecondaryColorOffsetEXT()
        {
            try
            {
                glVertexArraySecondaryColorOffsetEXTPtr = (glVertexArraySecondaryColorOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArraySecondaryColorOffsetEXT"), typeof(glVertexArraySecondaryColorOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArraySecondaryColorOffsetEXT'.");
            }
        }
        public static void glVertexArraySecondaryColorOffsetEXT(GLuint @vaobj, GLuint @buffer, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset) => glVertexArraySecondaryColorOffsetEXTPtr(@vaobj, @buffer, @size, @type, @stride, @offset);

        internal delegate void glVertexArrayVertexAttribOffsetEXTFunc(GLuint @vaobj, GLuint @buffer, GLuint @index, GLint @size, GLenum @type, GLboolean @normalized, GLsizei @stride, GLintptr @offset);
        internal static glVertexArrayVertexAttribOffsetEXTFunc glVertexArrayVertexAttribOffsetEXTPtr;
        internal static void loadVertexArrayVertexAttribOffsetEXT()
        {
            try
            {
                glVertexArrayVertexAttribOffsetEXTPtr = (glVertexArrayVertexAttribOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayVertexAttribOffsetEXT"), typeof(glVertexArrayVertexAttribOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayVertexAttribOffsetEXT'.");
            }
        }
        public static void glVertexArrayVertexAttribOffsetEXT(GLuint @vaobj, GLuint @buffer, GLuint @index, GLint @size, GLenum @type, GLboolean @normalized, GLsizei @stride, GLintptr @offset) => glVertexArrayVertexAttribOffsetEXTPtr(@vaobj, @buffer, @index, @size, @type, @normalized, @stride, @offset);

        internal delegate void glVertexArrayVertexAttribIOffsetEXTFunc(GLuint @vaobj, GLuint @buffer, GLuint @index, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset);
        internal static glVertexArrayVertexAttribIOffsetEXTFunc glVertexArrayVertexAttribIOffsetEXTPtr;
        internal static void loadVertexArrayVertexAttribIOffsetEXT()
        {
            try
            {
                glVertexArrayVertexAttribIOffsetEXTPtr = (glVertexArrayVertexAttribIOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayVertexAttribIOffsetEXT"), typeof(glVertexArrayVertexAttribIOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayVertexAttribIOffsetEXT'.");
            }
        }
        public static void glVertexArrayVertexAttribIOffsetEXT(GLuint @vaobj, GLuint @buffer, GLuint @index, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset) => glVertexArrayVertexAttribIOffsetEXTPtr(@vaobj, @buffer, @index, @size, @type, @stride, @offset);

        internal delegate void glEnableVertexArrayEXTFunc(GLuint @vaobj, GLenum @array);
        internal static glEnableVertexArrayEXTFunc glEnableVertexArrayEXTPtr;
        internal static void loadEnableVertexArrayEXT()
        {
            try
            {
                glEnableVertexArrayEXTPtr = (glEnableVertexArrayEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableVertexArrayEXT"), typeof(glEnableVertexArrayEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableVertexArrayEXT'.");
            }
        }
        public static void glEnableVertexArrayEXT(GLuint @vaobj, GLenum @array) => glEnableVertexArrayEXTPtr(@vaobj, @array);

        internal delegate void glDisableVertexArrayEXTFunc(GLuint @vaobj, GLenum @array);
        internal static glDisableVertexArrayEXTFunc glDisableVertexArrayEXTPtr;
        internal static void loadDisableVertexArrayEXT()
        {
            try
            {
                glDisableVertexArrayEXTPtr = (glDisableVertexArrayEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableVertexArrayEXT"), typeof(glDisableVertexArrayEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableVertexArrayEXT'.");
            }
        }
        public static void glDisableVertexArrayEXT(GLuint @vaobj, GLenum @array) => glDisableVertexArrayEXTPtr(@vaobj, @array);

        internal delegate void glEnableVertexArrayAttribEXTFunc(GLuint @vaobj, GLuint @index);
        internal static glEnableVertexArrayAttribEXTFunc glEnableVertexArrayAttribEXTPtr;
        internal static void loadEnableVertexArrayAttribEXT()
        {
            try
            {
                glEnableVertexArrayAttribEXTPtr = (glEnableVertexArrayAttribEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableVertexArrayAttribEXT"), typeof(glEnableVertexArrayAttribEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableVertexArrayAttribEXT'.");
            }
        }
        public static void glEnableVertexArrayAttribEXT(GLuint @vaobj, GLuint @index) => glEnableVertexArrayAttribEXTPtr(@vaobj, @index);

        internal delegate void glDisableVertexArrayAttribEXTFunc(GLuint @vaobj, GLuint @index);
        internal static glDisableVertexArrayAttribEXTFunc glDisableVertexArrayAttribEXTPtr;
        internal static void loadDisableVertexArrayAttribEXT()
        {
            try
            {
                glDisableVertexArrayAttribEXTPtr = (glDisableVertexArrayAttribEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableVertexArrayAttribEXT"), typeof(glDisableVertexArrayAttribEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableVertexArrayAttribEXT'.");
            }
        }
        public static void glDisableVertexArrayAttribEXT(GLuint @vaobj, GLuint @index) => glDisableVertexArrayAttribEXTPtr(@vaobj, @index);

        internal delegate void glGetVertexArrayIntegervEXTFunc(GLuint @vaobj, GLenum @pname, GLint * @param);
        internal static glGetVertexArrayIntegervEXTFunc glGetVertexArrayIntegervEXTPtr;
        internal static void loadGetVertexArrayIntegervEXT()
        {
            try
            {
                glGetVertexArrayIntegervEXTPtr = (glGetVertexArrayIntegervEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexArrayIntegervEXT"), typeof(glGetVertexArrayIntegervEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexArrayIntegervEXT'.");
            }
        }
        public static void glGetVertexArrayIntegervEXT(GLuint @vaobj, GLenum @pname, GLint * @param) => glGetVertexArrayIntegervEXTPtr(@vaobj, @pname, @param);

        internal delegate void glGetVertexArrayPointervEXTFunc(GLuint @vaobj, GLenum @pname, void ** @param);
        internal static glGetVertexArrayPointervEXTFunc glGetVertexArrayPointervEXTPtr;
        internal static void loadGetVertexArrayPointervEXT()
        {
            try
            {
                glGetVertexArrayPointervEXTPtr = (glGetVertexArrayPointervEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexArrayPointervEXT"), typeof(glGetVertexArrayPointervEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexArrayPointervEXT'.");
            }
        }
        public static void glGetVertexArrayPointervEXT(GLuint @vaobj, GLenum @pname, void ** @param) => glGetVertexArrayPointervEXTPtr(@vaobj, @pname, @param);

        internal delegate void glGetVertexArrayIntegeri_vEXTFunc(GLuint @vaobj, GLuint @index, GLenum @pname, GLint * @param);
        internal static glGetVertexArrayIntegeri_vEXTFunc glGetVertexArrayIntegeri_vEXTPtr;
        internal static void loadGetVertexArrayIntegeri_vEXT()
        {
            try
            {
                glGetVertexArrayIntegeri_vEXTPtr = (glGetVertexArrayIntegeri_vEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexArrayIntegeri_vEXT"), typeof(glGetVertexArrayIntegeri_vEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexArrayIntegeri_vEXT'.");
            }
        }
        public static void glGetVertexArrayIntegeri_vEXT(GLuint @vaobj, GLuint @index, GLenum @pname, GLint * @param) => glGetVertexArrayIntegeri_vEXTPtr(@vaobj, @index, @pname, @param);

        internal delegate void glGetVertexArrayPointeri_vEXTFunc(GLuint @vaobj, GLuint @index, GLenum @pname, void ** @param);
        internal static glGetVertexArrayPointeri_vEXTFunc glGetVertexArrayPointeri_vEXTPtr;
        internal static void loadGetVertexArrayPointeri_vEXT()
        {
            try
            {
                glGetVertexArrayPointeri_vEXTPtr = (glGetVertexArrayPointeri_vEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexArrayPointeri_vEXT"), typeof(glGetVertexArrayPointeri_vEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexArrayPointeri_vEXT'.");
            }
        }
        public static void glGetVertexArrayPointeri_vEXT(GLuint @vaobj, GLuint @index, GLenum @pname, void ** @param) => glGetVertexArrayPointeri_vEXTPtr(@vaobj, @index, @pname, @param);

        internal delegate void * glMapNamedBufferRangeEXTFunc(GLuint @buffer, GLintptr @offset, GLsizeiptr @length, GLbitfield @access);
        internal static glMapNamedBufferRangeEXTFunc glMapNamedBufferRangeEXTPtr;
        internal static void loadMapNamedBufferRangeEXT()
        {
            try
            {
                glMapNamedBufferRangeEXTPtr = (glMapNamedBufferRangeEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapNamedBufferRangeEXT"), typeof(glMapNamedBufferRangeEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapNamedBufferRangeEXT'.");
            }
        }
        public static void * glMapNamedBufferRangeEXT(GLuint @buffer, GLintptr @offset, GLsizeiptr @length, GLbitfield @access) => glMapNamedBufferRangeEXTPtr(@buffer, @offset, @length, @access);

        internal delegate void glFlushMappedNamedBufferRangeEXTFunc(GLuint @buffer, GLintptr @offset, GLsizeiptr @length);
        internal static glFlushMappedNamedBufferRangeEXTFunc glFlushMappedNamedBufferRangeEXTPtr;
        internal static void loadFlushMappedNamedBufferRangeEXT()
        {
            try
            {
                glFlushMappedNamedBufferRangeEXTPtr = (glFlushMappedNamedBufferRangeEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlushMappedNamedBufferRangeEXT"), typeof(glFlushMappedNamedBufferRangeEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlushMappedNamedBufferRangeEXT'.");
            }
        }
        public static void glFlushMappedNamedBufferRangeEXT(GLuint @buffer, GLintptr @offset, GLsizeiptr @length) => glFlushMappedNamedBufferRangeEXTPtr(@buffer, @offset, @length);

        internal delegate void glNamedBufferStorageEXTFunc(GLuint @buffer, GLsizeiptr @size, const void * @data, GLbitfield @flags);
        internal static glNamedBufferStorageEXTFunc glNamedBufferStorageEXTPtr;
        internal static void loadNamedBufferStorageEXT()
        {
            try
            {
                glNamedBufferStorageEXTPtr = (glNamedBufferStorageEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedBufferStorageEXT"), typeof(glNamedBufferStorageEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedBufferStorageEXT'.");
            }
        }
        public static void glNamedBufferStorageEXT(GLuint @buffer, GLsizeiptr @size, const void * @data, GLbitfield @flags) => glNamedBufferStorageEXTPtr(@buffer, @size, @data, @flags);

        internal delegate void glClearNamedBufferDataEXTFunc(GLuint @buffer, GLenum @internalformat, GLenum @format, GLenum @type, const void * @data);
        internal static glClearNamedBufferDataEXTFunc glClearNamedBufferDataEXTPtr;
        internal static void loadClearNamedBufferDataEXT()
        {
            try
            {
                glClearNamedBufferDataEXTPtr = (glClearNamedBufferDataEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearNamedBufferDataEXT"), typeof(glClearNamedBufferDataEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearNamedBufferDataEXT'.");
            }
        }
        public static void glClearNamedBufferDataEXT(GLuint @buffer, GLenum @internalformat, GLenum @format, GLenum @type, const void * @data) => glClearNamedBufferDataEXTPtr(@buffer, @internalformat, @format, @type, @data);

        internal delegate void glClearNamedBufferSubDataEXTFunc(GLuint @buffer, GLenum @internalformat, GLsizeiptr @offset, GLsizeiptr @size, GLenum @format, GLenum @type, const void * @data);
        internal static glClearNamedBufferSubDataEXTFunc glClearNamedBufferSubDataEXTPtr;
        internal static void loadClearNamedBufferSubDataEXT()
        {
            try
            {
                glClearNamedBufferSubDataEXTPtr = (glClearNamedBufferSubDataEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearNamedBufferSubDataEXT"), typeof(glClearNamedBufferSubDataEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearNamedBufferSubDataEXT'.");
            }
        }
        public static void glClearNamedBufferSubDataEXT(GLuint @buffer, GLenum @internalformat, GLsizeiptr @offset, GLsizeiptr @size, GLenum @format, GLenum @type, const void * @data) => glClearNamedBufferSubDataEXTPtr(@buffer, @internalformat, @offset, @size, @format, @type, @data);

        internal delegate void glNamedFramebufferParameteriEXTFunc(GLuint @framebuffer, GLenum @pname, GLint @param);
        internal static glNamedFramebufferParameteriEXTFunc glNamedFramebufferParameteriEXTPtr;
        internal static void loadNamedFramebufferParameteriEXT()
        {
            try
            {
                glNamedFramebufferParameteriEXTPtr = (glNamedFramebufferParameteriEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferParameteriEXT"), typeof(glNamedFramebufferParameteriEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferParameteriEXT'.");
            }
        }
        public static void glNamedFramebufferParameteriEXT(GLuint @framebuffer, GLenum @pname, GLint @param) => glNamedFramebufferParameteriEXTPtr(@framebuffer, @pname, @param);

        internal delegate void glGetNamedFramebufferParameterivEXTFunc(GLuint @framebuffer, GLenum @pname, GLint * @params);
        internal static glGetNamedFramebufferParameterivEXTFunc glGetNamedFramebufferParameterivEXTPtr;
        internal static void loadGetNamedFramebufferParameterivEXT()
        {
            try
            {
                glGetNamedFramebufferParameterivEXTPtr = (glGetNamedFramebufferParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedFramebufferParameterivEXT"), typeof(glGetNamedFramebufferParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedFramebufferParameterivEXT'.");
            }
        }
        public static void glGetNamedFramebufferParameterivEXT(GLuint @framebuffer, GLenum @pname, GLint * @params) => glGetNamedFramebufferParameterivEXTPtr(@framebuffer, @pname, @params);

        internal delegate void glProgramUniform1dEXTFunc(GLuint @program, GLint @location, GLdouble @x);
        internal static glProgramUniform1dEXTFunc glProgramUniform1dEXTPtr;
        internal static void loadProgramUniform1dEXT()
        {
            try
            {
                glProgramUniform1dEXTPtr = (glProgramUniform1dEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1dEXT"), typeof(glProgramUniform1dEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1dEXT'.");
            }
        }
        public static void glProgramUniform1dEXT(GLuint @program, GLint @location, GLdouble @x) => glProgramUniform1dEXTPtr(@program, @location, @x);

        internal delegate void glProgramUniform2dEXTFunc(GLuint @program, GLint @location, GLdouble @x, GLdouble @y);
        internal static glProgramUniform2dEXTFunc glProgramUniform2dEXTPtr;
        internal static void loadProgramUniform2dEXT()
        {
            try
            {
                glProgramUniform2dEXTPtr = (glProgramUniform2dEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2dEXT"), typeof(glProgramUniform2dEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2dEXT'.");
            }
        }
        public static void glProgramUniform2dEXT(GLuint @program, GLint @location, GLdouble @x, GLdouble @y) => glProgramUniform2dEXTPtr(@program, @location, @x, @y);

        internal delegate void glProgramUniform3dEXTFunc(GLuint @program, GLint @location, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glProgramUniform3dEXTFunc glProgramUniform3dEXTPtr;
        internal static void loadProgramUniform3dEXT()
        {
            try
            {
                glProgramUniform3dEXTPtr = (glProgramUniform3dEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3dEXT"), typeof(glProgramUniform3dEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3dEXT'.");
            }
        }
        public static void glProgramUniform3dEXT(GLuint @program, GLint @location, GLdouble @x, GLdouble @y, GLdouble @z) => glProgramUniform3dEXTPtr(@program, @location, @x, @y, @z);

        internal delegate void glProgramUniform4dEXTFunc(GLuint @program, GLint @location, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glProgramUniform4dEXTFunc glProgramUniform4dEXTPtr;
        internal static void loadProgramUniform4dEXT()
        {
            try
            {
                glProgramUniform4dEXTPtr = (glProgramUniform4dEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4dEXT"), typeof(glProgramUniform4dEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4dEXT'.");
            }
        }
        public static void glProgramUniform4dEXT(GLuint @program, GLint @location, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glProgramUniform4dEXTPtr(@program, @location, @x, @y, @z, @w);

        internal delegate void glProgramUniform1dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glProgramUniform1dvEXTFunc glProgramUniform1dvEXTPtr;
        internal static void loadProgramUniform1dvEXT()
        {
            try
            {
                glProgramUniform1dvEXTPtr = (glProgramUniform1dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1dvEXT"), typeof(glProgramUniform1dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1dvEXT'.");
            }
        }
        public static void glProgramUniform1dvEXT(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value) => glProgramUniform1dvEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glProgramUniform2dvEXTFunc glProgramUniform2dvEXTPtr;
        internal static void loadProgramUniform2dvEXT()
        {
            try
            {
                glProgramUniform2dvEXTPtr = (glProgramUniform2dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2dvEXT"), typeof(glProgramUniform2dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2dvEXT'.");
            }
        }
        public static void glProgramUniform2dvEXT(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value) => glProgramUniform2dvEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glProgramUniform3dvEXTFunc glProgramUniform3dvEXTPtr;
        internal static void loadProgramUniform3dvEXT()
        {
            try
            {
                glProgramUniform3dvEXTPtr = (glProgramUniform3dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3dvEXT"), typeof(glProgramUniform3dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3dvEXT'.");
            }
        }
        public static void glProgramUniform3dvEXT(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value) => glProgramUniform3dvEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glProgramUniform4dvEXTFunc glProgramUniform4dvEXTPtr;
        internal static void loadProgramUniform4dvEXT()
        {
            try
            {
                glProgramUniform4dvEXTPtr = (glProgramUniform4dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4dvEXT"), typeof(glProgramUniform4dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4dvEXT'.");
            }
        }
        public static void glProgramUniform4dvEXT(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value) => glProgramUniform4dvEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniformMatrix2dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix2dvEXTFunc glProgramUniformMatrix2dvEXTPtr;
        internal static void loadProgramUniformMatrix2dvEXT()
        {
            try
            {
                glProgramUniformMatrix2dvEXTPtr = (glProgramUniformMatrix2dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2dvEXT"), typeof(glProgramUniformMatrix2dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2dvEXT'.");
            }
        }
        public static void glProgramUniformMatrix2dvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix2dvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix3dvEXTFunc glProgramUniformMatrix3dvEXTPtr;
        internal static void loadProgramUniformMatrix3dvEXT()
        {
            try
            {
                glProgramUniformMatrix3dvEXTPtr = (glProgramUniformMatrix3dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3dvEXT"), typeof(glProgramUniformMatrix3dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3dvEXT'.");
            }
        }
        public static void glProgramUniformMatrix3dvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix3dvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix4dvEXTFunc glProgramUniformMatrix4dvEXTPtr;
        internal static void loadProgramUniformMatrix4dvEXT()
        {
            try
            {
                glProgramUniformMatrix4dvEXTPtr = (glProgramUniformMatrix4dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4dvEXT"), typeof(glProgramUniformMatrix4dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4dvEXT'.");
            }
        }
        public static void glProgramUniformMatrix4dvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix4dvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x3dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix2x3dvEXTFunc glProgramUniformMatrix2x3dvEXTPtr;
        internal static void loadProgramUniformMatrix2x3dvEXT()
        {
            try
            {
                glProgramUniformMatrix2x3dvEXTPtr = (glProgramUniformMatrix2x3dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x3dvEXT"), typeof(glProgramUniformMatrix2x3dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x3dvEXT'.");
            }
        }
        public static void glProgramUniformMatrix2x3dvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix2x3dvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x4dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix2x4dvEXTFunc glProgramUniformMatrix2x4dvEXTPtr;
        internal static void loadProgramUniformMatrix2x4dvEXT()
        {
            try
            {
                glProgramUniformMatrix2x4dvEXTPtr = (glProgramUniformMatrix2x4dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x4dvEXT"), typeof(glProgramUniformMatrix2x4dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x4dvEXT'.");
            }
        }
        public static void glProgramUniformMatrix2x4dvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix2x4dvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x2dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix3x2dvEXTFunc glProgramUniformMatrix3x2dvEXTPtr;
        internal static void loadProgramUniformMatrix3x2dvEXT()
        {
            try
            {
                glProgramUniformMatrix3x2dvEXTPtr = (glProgramUniformMatrix3x2dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x2dvEXT"), typeof(glProgramUniformMatrix3x2dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x2dvEXT'.");
            }
        }
        public static void glProgramUniformMatrix3x2dvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix3x2dvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x4dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix3x4dvEXTFunc glProgramUniformMatrix3x4dvEXTPtr;
        internal static void loadProgramUniformMatrix3x4dvEXT()
        {
            try
            {
                glProgramUniformMatrix3x4dvEXTPtr = (glProgramUniformMatrix3x4dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x4dvEXT"), typeof(glProgramUniformMatrix3x4dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x4dvEXT'.");
            }
        }
        public static void glProgramUniformMatrix3x4dvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix3x4dvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x2dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix4x2dvEXTFunc glProgramUniformMatrix4x2dvEXTPtr;
        internal static void loadProgramUniformMatrix4x2dvEXT()
        {
            try
            {
                glProgramUniformMatrix4x2dvEXTPtr = (glProgramUniformMatrix4x2dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x2dvEXT"), typeof(glProgramUniformMatrix4x2dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x2dvEXT'.");
            }
        }
        public static void glProgramUniformMatrix4x2dvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix4x2dvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x3dvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix4x3dvEXTFunc glProgramUniformMatrix4x3dvEXTPtr;
        internal static void loadProgramUniformMatrix4x3dvEXT()
        {
            try
            {
                glProgramUniformMatrix4x3dvEXTPtr = (glProgramUniformMatrix4x3dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x3dvEXT"), typeof(glProgramUniformMatrix4x3dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x3dvEXT'.");
            }
        }
        public static void glProgramUniformMatrix4x3dvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix4x3dvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glTextureBufferRangeEXTFunc(GLuint @texture, GLenum @target, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glTextureBufferRangeEXTFunc glTextureBufferRangeEXTPtr;
        internal static void loadTextureBufferRangeEXT()
        {
            try
            {
                glTextureBufferRangeEXTPtr = (glTextureBufferRangeEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureBufferRangeEXT"), typeof(glTextureBufferRangeEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureBufferRangeEXT'.");
            }
        }
        public static void glTextureBufferRangeEXT(GLuint @texture, GLenum @target, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glTextureBufferRangeEXTPtr(@texture, @target, @internalformat, @buffer, @offset, @size);

        internal delegate void glTextureStorage1DEXTFunc(GLuint @texture, GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width);
        internal static glTextureStorage1DEXTFunc glTextureStorage1DEXTPtr;
        internal static void loadTextureStorage1DEXT()
        {
            try
            {
                glTextureStorage1DEXTPtr = (glTextureStorage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage1DEXT"), typeof(glTextureStorage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage1DEXT'.");
            }
        }
        public static void glTextureStorage1DEXT(GLuint @texture, GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width) => glTextureStorage1DEXTPtr(@texture, @target, @levels, @internalformat, @width);

        internal delegate void glTextureStorage2DEXTFunc(GLuint @texture, GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glTextureStorage2DEXTFunc glTextureStorage2DEXTPtr;
        internal static void loadTextureStorage2DEXT()
        {
            try
            {
                glTextureStorage2DEXTPtr = (glTextureStorage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage2DEXT"), typeof(glTextureStorage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage2DEXT'.");
            }
        }
        public static void glTextureStorage2DEXT(GLuint @texture, GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height) => glTextureStorage2DEXTPtr(@texture, @target, @levels, @internalformat, @width, @height);

        internal delegate void glTextureStorage3DEXTFunc(GLuint @texture, GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth);
        internal static glTextureStorage3DEXTFunc glTextureStorage3DEXTPtr;
        internal static void loadTextureStorage3DEXT()
        {
            try
            {
                glTextureStorage3DEXTPtr = (glTextureStorage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage3DEXT"), typeof(glTextureStorage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage3DEXT'.");
            }
        }
        public static void glTextureStorage3DEXT(GLuint @texture, GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth) => glTextureStorage3DEXTPtr(@texture, @target, @levels, @internalformat, @width, @height, @depth);

        internal delegate void glTextureStorage2DMultisampleEXTFunc(GLuint @texture, GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations);
        internal static glTextureStorage2DMultisampleEXTFunc glTextureStorage2DMultisampleEXTPtr;
        internal static void loadTextureStorage2DMultisampleEXT()
        {
            try
            {
                glTextureStorage2DMultisampleEXTPtr = (glTextureStorage2DMultisampleEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage2DMultisampleEXT"), typeof(glTextureStorage2DMultisampleEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage2DMultisampleEXT'.");
            }
        }
        public static void glTextureStorage2DMultisampleEXT(GLuint @texture, GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations) => glTextureStorage2DMultisampleEXTPtr(@texture, @target, @samples, @internalformat, @width, @height, @fixedsamplelocations);

        internal delegate void glTextureStorage3DMultisampleEXTFunc(GLuint @texture, GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations);
        internal static glTextureStorage3DMultisampleEXTFunc glTextureStorage3DMultisampleEXTPtr;
        internal static void loadTextureStorage3DMultisampleEXT()
        {
            try
            {
                glTextureStorage3DMultisampleEXTPtr = (glTextureStorage3DMultisampleEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage3DMultisampleEXT"), typeof(glTextureStorage3DMultisampleEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage3DMultisampleEXT'.");
            }
        }
        public static void glTextureStorage3DMultisampleEXT(GLuint @texture, GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations) => glTextureStorage3DMultisampleEXTPtr(@texture, @target, @samples, @internalformat, @width, @height, @depth, @fixedsamplelocations);

        internal delegate void glVertexArrayBindVertexBufferEXTFunc(GLuint @vaobj, GLuint @bindingindex, GLuint @buffer, GLintptr @offset, GLsizei @stride);
        internal static glVertexArrayBindVertexBufferEXTFunc glVertexArrayBindVertexBufferEXTPtr;
        internal static void loadVertexArrayBindVertexBufferEXT()
        {
            try
            {
                glVertexArrayBindVertexBufferEXTPtr = (glVertexArrayBindVertexBufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayBindVertexBufferEXT"), typeof(glVertexArrayBindVertexBufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayBindVertexBufferEXT'.");
            }
        }
        public static void glVertexArrayBindVertexBufferEXT(GLuint @vaobj, GLuint @bindingindex, GLuint @buffer, GLintptr @offset, GLsizei @stride) => glVertexArrayBindVertexBufferEXTPtr(@vaobj, @bindingindex, @buffer, @offset, @stride);

        internal delegate void glVertexArrayVertexAttribFormatEXTFunc(GLuint @vaobj, GLuint @attribindex, GLint @size, GLenum @type, GLboolean @normalized, GLuint @relativeoffset);
        internal static glVertexArrayVertexAttribFormatEXTFunc glVertexArrayVertexAttribFormatEXTPtr;
        internal static void loadVertexArrayVertexAttribFormatEXT()
        {
            try
            {
                glVertexArrayVertexAttribFormatEXTPtr = (glVertexArrayVertexAttribFormatEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayVertexAttribFormatEXT"), typeof(glVertexArrayVertexAttribFormatEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayVertexAttribFormatEXT'.");
            }
        }
        public static void glVertexArrayVertexAttribFormatEXT(GLuint @vaobj, GLuint @attribindex, GLint @size, GLenum @type, GLboolean @normalized, GLuint @relativeoffset) => glVertexArrayVertexAttribFormatEXTPtr(@vaobj, @attribindex, @size, @type, @normalized, @relativeoffset);

        internal delegate void glVertexArrayVertexAttribIFormatEXTFunc(GLuint @vaobj, GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset);
        internal static glVertexArrayVertexAttribIFormatEXTFunc glVertexArrayVertexAttribIFormatEXTPtr;
        internal static void loadVertexArrayVertexAttribIFormatEXT()
        {
            try
            {
                glVertexArrayVertexAttribIFormatEXTPtr = (glVertexArrayVertexAttribIFormatEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayVertexAttribIFormatEXT"), typeof(glVertexArrayVertexAttribIFormatEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayVertexAttribIFormatEXT'.");
            }
        }
        public static void glVertexArrayVertexAttribIFormatEXT(GLuint @vaobj, GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset) => glVertexArrayVertexAttribIFormatEXTPtr(@vaobj, @attribindex, @size, @type, @relativeoffset);

        internal delegate void glVertexArrayVertexAttribLFormatEXTFunc(GLuint @vaobj, GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset);
        internal static glVertexArrayVertexAttribLFormatEXTFunc glVertexArrayVertexAttribLFormatEXTPtr;
        internal static void loadVertexArrayVertexAttribLFormatEXT()
        {
            try
            {
                glVertexArrayVertexAttribLFormatEXTPtr = (glVertexArrayVertexAttribLFormatEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayVertexAttribLFormatEXT"), typeof(glVertexArrayVertexAttribLFormatEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayVertexAttribLFormatEXT'.");
            }
        }
        public static void glVertexArrayVertexAttribLFormatEXT(GLuint @vaobj, GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset) => glVertexArrayVertexAttribLFormatEXTPtr(@vaobj, @attribindex, @size, @type, @relativeoffset);

        internal delegate void glVertexArrayVertexAttribBindingEXTFunc(GLuint @vaobj, GLuint @attribindex, GLuint @bindingindex);
        internal static glVertexArrayVertexAttribBindingEXTFunc glVertexArrayVertexAttribBindingEXTPtr;
        internal static void loadVertexArrayVertexAttribBindingEXT()
        {
            try
            {
                glVertexArrayVertexAttribBindingEXTPtr = (glVertexArrayVertexAttribBindingEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayVertexAttribBindingEXT"), typeof(glVertexArrayVertexAttribBindingEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayVertexAttribBindingEXT'.");
            }
        }
        public static void glVertexArrayVertexAttribBindingEXT(GLuint @vaobj, GLuint @attribindex, GLuint @bindingindex) => glVertexArrayVertexAttribBindingEXTPtr(@vaobj, @attribindex, @bindingindex);

        internal delegate void glVertexArrayVertexBindingDivisorEXTFunc(GLuint @vaobj, GLuint @bindingindex, GLuint @divisor);
        internal static glVertexArrayVertexBindingDivisorEXTFunc glVertexArrayVertexBindingDivisorEXTPtr;
        internal static void loadVertexArrayVertexBindingDivisorEXT()
        {
            try
            {
                glVertexArrayVertexBindingDivisorEXTPtr = (glVertexArrayVertexBindingDivisorEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayVertexBindingDivisorEXT"), typeof(glVertexArrayVertexBindingDivisorEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayVertexBindingDivisorEXT'.");
            }
        }
        public static void glVertexArrayVertexBindingDivisorEXT(GLuint @vaobj, GLuint @bindingindex, GLuint @divisor) => glVertexArrayVertexBindingDivisorEXTPtr(@vaobj, @bindingindex, @divisor);

        internal delegate void glVertexArrayVertexAttribLOffsetEXTFunc(GLuint @vaobj, GLuint @buffer, GLuint @index, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset);
        internal static glVertexArrayVertexAttribLOffsetEXTFunc glVertexArrayVertexAttribLOffsetEXTPtr;
        internal static void loadVertexArrayVertexAttribLOffsetEXT()
        {
            try
            {
                glVertexArrayVertexAttribLOffsetEXTPtr = (glVertexArrayVertexAttribLOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayVertexAttribLOffsetEXT"), typeof(glVertexArrayVertexAttribLOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayVertexAttribLOffsetEXT'.");
            }
        }
        public static void glVertexArrayVertexAttribLOffsetEXT(GLuint @vaobj, GLuint @buffer, GLuint @index, GLint @size, GLenum @type, GLsizei @stride, GLintptr @offset) => glVertexArrayVertexAttribLOffsetEXTPtr(@vaobj, @buffer, @index, @size, @type, @stride, @offset);

        internal delegate void glTexturePageCommitmentEXTFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @commit);
        internal static glTexturePageCommitmentEXTFunc glTexturePageCommitmentEXTPtr;
        internal static void loadTexturePageCommitmentEXT()
        {
            try
            {
                glTexturePageCommitmentEXTPtr = (glTexturePageCommitmentEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexturePageCommitmentEXT"), typeof(glTexturePageCommitmentEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexturePageCommitmentEXT'.");
            }
        }
        public static void glTexturePageCommitmentEXT(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @commit) => glTexturePageCommitmentEXTPtr(@texture, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @commit);

        internal delegate void glVertexArrayVertexAttribDivisorEXTFunc(GLuint @vaobj, GLuint @index, GLuint @divisor);
        internal static glVertexArrayVertexAttribDivisorEXTFunc glVertexArrayVertexAttribDivisorEXTPtr;
        internal static void loadVertexArrayVertexAttribDivisorEXT()
        {
            try
            {
                glVertexArrayVertexAttribDivisorEXTPtr = (glVertexArrayVertexAttribDivisorEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayVertexAttribDivisorEXT"), typeof(glVertexArrayVertexAttribDivisorEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayVertexAttribDivisorEXT'.");
            }
        }
        public static void glVertexArrayVertexAttribDivisorEXT(GLuint @vaobj, GLuint @index, GLuint @divisor) => glVertexArrayVertexAttribDivisorEXTPtr(@vaobj, @index, @divisor);
        #endregion
    }
}
