using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_instanced_arrays
    {
        #region Interop
        static GL_EXT_instanced_arrays()
        {
            Console.WriteLine("Initalising GL_EXT_instanced_arrays interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawArraysInstancedEXT();
            loadDrawElementsInstancedEXT();
            loadVertexAttribDivisorEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_DIVISOR_EXT = 0x88FE;
        #endregion

        #region Commands
        internal delegate void glDrawArraysInstancedEXTFunc(GLenum @mode, GLint @start, GLsizei @count, GLsizei @primcount);
        internal static glDrawArraysInstancedEXTFunc glDrawArraysInstancedEXTPtr;
        internal static void loadDrawArraysInstancedEXT()
        {
            try
            {
                glDrawArraysInstancedEXTPtr = (glDrawArraysInstancedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysInstancedEXT"), typeof(glDrawArraysInstancedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysInstancedEXT'.");
            }
        }
        public static void glDrawArraysInstancedEXT(GLenum @mode, GLint @start, GLsizei @count, GLsizei @primcount) => glDrawArraysInstancedEXTPtr(@mode, @start, @count, @primcount);

        internal delegate void glDrawElementsInstancedEXTFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @primcount);
        internal static glDrawElementsInstancedEXTFunc glDrawElementsInstancedEXTPtr;
        internal static void loadDrawElementsInstancedEXT()
        {
            try
            {
                glDrawElementsInstancedEXTPtr = (glDrawElementsInstancedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedEXT"), typeof(glDrawElementsInstancedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedEXT'.");
            }
        }
        public static void glDrawElementsInstancedEXT(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @primcount) => glDrawElementsInstancedEXTPtr(@mode, @count, @type, @indices, @primcount);

        internal delegate void glVertexAttribDivisorEXTFunc(GLuint @index, GLuint @divisor);
        internal static glVertexAttribDivisorEXTFunc glVertexAttribDivisorEXTPtr;
        internal static void loadVertexAttribDivisorEXT()
        {
            try
            {
                glVertexAttribDivisorEXTPtr = (glVertexAttribDivisorEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribDivisorEXT"), typeof(glVertexAttribDivisorEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribDivisorEXT'.");
            }
        }
        public static void glVertexAttribDivisorEXT(GLuint @index, GLuint @divisor) => glVertexAttribDivisorEXTPtr(@index, @divisor);
        #endregion
    }
}
