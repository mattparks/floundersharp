using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_debug_label
    {
        #region Interop
        static GL_EXT_debug_label()
        {
            Console.WriteLine("Initalising GL_EXT_debug_label interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadLabelObjectEXT();
            loadGetObjectLabelEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PROGRAM_PIPELINE_OBJECT_EXT = 0x8A4F;
        public static UInt32 GL_PROGRAM_OBJECT_EXT = 0x8B40;
        public static UInt32 GL_SHADER_OBJECT_EXT = 0x8B48;
        public static UInt32 GL_BUFFER_OBJECT_EXT = 0x9151;
        public static UInt32 GL_QUERY_OBJECT_EXT = 0x9153;
        public static UInt32 GL_VERTEX_ARRAY_OBJECT_EXT = 0x9154;
        public static UInt32 GL_SAMPLER = 0x82E6;
        public static UInt32 GL_TRANSFORM_FEEDBACK = 0x8E22;
        #endregion

        #region Commands
        internal delegate void glLabelObjectEXTFunc(GLenum @type, GLuint @object, GLsizei @length, const GLchar * @label);
        internal static glLabelObjectEXTFunc glLabelObjectEXTPtr;
        internal static void loadLabelObjectEXT()
        {
            try
            {
                glLabelObjectEXTPtr = (glLabelObjectEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLabelObjectEXT"), typeof(glLabelObjectEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLabelObjectEXT'.");
            }
        }
        public static void glLabelObjectEXT(GLenum @type, GLuint @object, GLsizei @length, const GLchar * @label) => glLabelObjectEXTPtr(@type, @object, @length, @label);

        internal delegate void glGetObjectLabelEXTFunc(GLenum @type, GLuint @object, GLsizei @bufSize, GLsizei * @length, GLchar * @label);
        internal static glGetObjectLabelEXTFunc glGetObjectLabelEXTPtr;
        internal static void loadGetObjectLabelEXT()
        {
            try
            {
                glGetObjectLabelEXTPtr = (glGetObjectLabelEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectLabelEXT"), typeof(glGetObjectLabelEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectLabelEXT'.");
            }
        }
        public static void glGetObjectLabelEXT(GLenum @type, GLuint @object, GLsizei @bufSize, GLsizei * @length, GLchar * @label) => glGetObjectLabelEXTPtr(@type, @object, @bufSize, @length, @label);
        #endregion
    }
}
