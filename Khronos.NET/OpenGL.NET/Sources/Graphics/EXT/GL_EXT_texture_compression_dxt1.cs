using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_compression_dxt1
    {
        #region Interop
        static GL_EXT_texture_compression_dxt1()
        {
            Console.WriteLine("Initalising GL_EXT_texture_compression_dxt1 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_RGB_S3TC_DXT1_EXT = 0x83F0;
        public static UInt32 GL_COMPRESSED_RGBA_S3TC_DXT1_EXT = 0x83F1;
        #endregion

        #region Commands
        #endregion
    }
}
