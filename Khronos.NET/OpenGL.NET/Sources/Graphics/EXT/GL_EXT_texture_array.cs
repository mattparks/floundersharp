using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_array
    {
        #region Interop
        static GL_EXT_texture_array()
        {
            Console.WriteLine("Initalising GL_EXT_texture_array interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFramebufferTextureLayerEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_1D_ARRAY_EXT = 0x8C18;
        public static UInt32 GL_PROXY_TEXTURE_1D_ARRAY_EXT = 0x8C19;
        public static UInt32 GL_TEXTURE_2D_ARRAY_EXT = 0x8C1A;
        public static UInt32 GL_PROXY_TEXTURE_2D_ARRAY_EXT = 0x8C1B;
        public static UInt32 GL_TEXTURE_BINDING_1D_ARRAY_EXT = 0x8C1C;
        public static UInt32 GL_TEXTURE_BINDING_2D_ARRAY_EXT = 0x8C1D;
        public static UInt32 GL_MAX_ARRAY_TEXTURE_LAYERS_EXT = 0x88FF;
        public static UInt32 GL_COMPARE_REF_DEPTH_TO_TEXTURE_EXT = 0x884E;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER_EXT = 0x8CD4;
        #endregion

        #region Commands
        internal delegate void glFramebufferTextureLayerEXTFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer);
        internal static glFramebufferTextureLayerEXTFunc glFramebufferTextureLayerEXTPtr;
        internal static void loadFramebufferTextureLayerEXT()
        {
            try
            {
                glFramebufferTextureLayerEXTPtr = (glFramebufferTextureLayerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureLayerEXT"), typeof(glFramebufferTextureLayerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureLayerEXT'.");
            }
        }
        public static void glFramebufferTextureLayerEXT(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer) => glFramebufferTextureLayerEXTPtr(@target, @attachment, @texture, @level, @layer);
        #endregion
    }
}
