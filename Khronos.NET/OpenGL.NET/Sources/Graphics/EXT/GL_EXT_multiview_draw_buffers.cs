using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_multiview_draw_buffers
    {
        #region Interop
        static GL_EXT_multiview_draw_buffers()
        {
            Console.WriteLine("Initalising GL_EXT_multiview_draw_buffers interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadReadBufferIndexedEXT();
            loadDrawBuffersIndexedEXT();
            loadGetIntegeri_vEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COLOR_ATTACHMENT_EXT = 0x90F0;
        public static UInt32 GL_MULTIVIEW_EXT = 0x90F1;
        public static UInt32 GL_DRAW_BUFFER_EXT = 0x0C01;
        public static UInt32 GL_READ_BUFFER_EXT = 0x0C02;
        public static UInt32 GL_MAX_MULTIVIEW_BUFFERS_EXT = 0x90F2;
        #endregion

        #region Commands
        internal delegate void glReadBufferIndexedEXTFunc(GLenum @src, GLint @index);
        internal static glReadBufferIndexedEXTFunc glReadBufferIndexedEXTPtr;
        internal static void loadReadBufferIndexedEXT()
        {
            try
            {
                glReadBufferIndexedEXTPtr = (glReadBufferIndexedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadBufferIndexedEXT"), typeof(glReadBufferIndexedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadBufferIndexedEXT'.");
            }
        }
        public static void glReadBufferIndexedEXT(GLenum @src, GLint @index) => glReadBufferIndexedEXTPtr(@src, @index);

        internal delegate void glDrawBuffersIndexedEXTFunc(GLint @n, const GLenum * @location, const GLint * @indices);
        internal static glDrawBuffersIndexedEXTFunc glDrawBuffersIndexedEXTPtr;
        internal static void loadDrawBuffersIndexedEXT()
        {
            try
            {
                glDrawBuffersIndexedEXTPtr = (glDrawBuffersIndexedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawBuffersIndexedEXT"), typeof(glDrawBuffersIndexedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawBuffersIndexedEXT'.");
            }
        }
        public static void glDrawBuffersIndexedEXT(GLint @n, const GLenum * @location, const GLint * @indices) => glDrawBuffersIndexedEXTPtr(@n, @location, @indices);

        internal delegate void glGetIntegeri_vEXTFunc(GLenum @target, GLuint @index, GLint * @data);
        internal static glGetIntegeri_vEXTFunc glGetIntegeri_vEXTPtr;
        internal static void loadGetIntegeri_vEXT()
        {
            try
            {
                glGetIntegeri_vEXTPtr = (glGetIntegeri_vEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetIntegeri_vEXT"), typeof(glGetIntegeri_vEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetIntegeri_vEXT'.");
            }
        }
        public static void glGetIntegeri_vEXT(GLenum @target, GLuint @index, GLint * @data) => glGetIntegeri_vEXTPtr(@target, @index, @data);
        #endregion
    }
}
