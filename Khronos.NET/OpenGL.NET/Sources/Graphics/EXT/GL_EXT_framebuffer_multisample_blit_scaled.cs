using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_framebuffer_multisample_blit_scaled
    {
        #region Interop
        static GL_EXT_framebuffer_multisample_blit_scaled()
        {
            Console.WriteLine("Initalising GL_EXT_framebuffer_multisample_blit_scaled interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SCALED_RESOLVE_FASTEST_EXT = 0x90BA;
        public static UInt32 GL_SCALED_RESOLVE_NICEST_EXT = 0x90BB;
        #endregion

        #region Commands
        #endregion
    }
}
