using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_multi_draw_arrays
    {
        #region Interop
        static GL_EXT_multi_draw_arrays()
        {
            Console.WriteLine("Initalising GL_EXT_multi_draw_arrays interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMultiDrawArraysEXT();
            loadMultiDrawElementsEXT();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glMultiDrawArraysEXTFunc(GLenum @mode, const GLint * @first, const GLsizei * @count, GLsizei @primcount);
        internal static glMultiDrawArraysEXTFunc glMultiDrawArraysEXTPtr;
        internal static void loadMultiDrawArraysEXT()
        {
            try
            {
                glMultiDrawArraysEXTPtr = (glMultiDrawArraysEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawArraysEXT"), typeof(glMultiDrawArraysEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawArraysEXT'.");
            }
        }
        public static void glMultiDrawArraysEXT(GLenum @mode, const GLint * @first, const GLsizei * @count, GLsizei @primcount) => glMultiDrawArraysEXTPtr(@mode, @first, @count, @primcount);

        internal delegate void glMultiDrawElementsEXTFunc(GLenum @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @primcount);
        internal static glMultiDrawElementsEXTFunc glMultiDrawElementsEXTPtr;
        internal static void loadMultiDrawElementsEXT()
        {
            try
            {
                glMultiDrawElementsEXTPtr = (glMultiDrawElementsEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementsEXT"), typeof(glMultiDrawElementsEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementsEXT'.");
            }
        }
        public static void glMultiDrawElementsEXT(GLenum @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @primcount) => glMultiDrawElementsEXTPtr(@mode, @count, @type, @indices, @primcount);
        #endregion
    }
}
