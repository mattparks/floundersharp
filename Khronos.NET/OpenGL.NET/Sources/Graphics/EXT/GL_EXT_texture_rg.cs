using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_rg
    {
        #region Interop
        static GL_EXT_texture_rg()
        {
            Console.WriteLine("Initalising GL_EXT_texture_rg interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RED_EXT = 0x1903;
        public static UInt32 GL_RG_EXT = 0x8227;
        public static UInt32 GL_R8_EXT = 0x8229;
        public static UInt32 GL_RG8_EXT = 0x822B;
        #endregion

        #region Commands
        #endregion
    }
}
