using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_pixel_transform
    {
        #region Interop
        static GL_EXT_pixel_transform()
        {
            Console.WriteLine("Initalising GL_EXT_pixel_transform interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPixelTransformParameteriEXT();
            loadPixelTransformParameterfEXT();
            loadPixelTransformParameterivEXT();
            loadPixelTransformParameterfvEXT();
            loadGetPixelTransformParameterivEXT();
            loadGetPixelTransformParameterfvEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PIXEL_TRANSFORM_2D_EXT = 0x8330;
        public static UInt32 GL_PIXEL_MAG_FILTER_EXT = 0x8331;
        public static UInt32 GL_PIXEL_MIN_FILTER_EXT = 0x8332;
        public static UInt32 GL_PIXEL_CUBIC_WEIGHT_EXT = 0x8333;
        public static UInt32 GL_CUBIC_EXT = 0x8334;
        public static UInt32 GL_AVERAGE_EXT = 0x8335;
        public static UInt32 GL_PIXEL_TRANSFORM_2D_STACK_DEPTH_EXT = 0x8336;
        public static UInt32 GL_MAX_PIXEL_TRANSFORM_2D_STACK_DEPTH_EXT = 0x8337;
        public static UInt32 GL_PIXEL_TRANSFORM_2D_MATRIX_EXT = 0x8338;
        #endregion

        #region Commands
        internal delegate void glPixelTransformParameteriEXTFunc(GLenum @target, GLenum @pname, GLint @param);
        internal static glPixelTransformParameteriEXTFunc glPixelTransformParameteriEXTPtr;
        internal static void loadPixelTransformParameteriEXT()
        {
            try
            {
                glPixelTransformParameteriEXTPtr = (glPixelTransformParameteriEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelTransformParameteriEXT"), typeof(glPixelTransformParameteriEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelTransformParameteriEXT'.");
            }
        }
        public static void glPixelTransformParameteriEXT(GLenum @target, GLenum @pname, GLint @param) => glPixelTransformParameteriEXTPtr(@target, @pname, @param);

        internal delegate void glPixelTransformParameterfEXTFunc(GLenum @target, GLenum @pname, GLfloat @param);
        internal static glPixelTransformParameterfEXTFunc glPixelTransformParameterfEXTPtr;
        internal static void loadPixelTransformParameterfEXT()
        {
            try
            {
                glPixelTransformParameterfEXTPtr = (glPixelTransformParameterfEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelTransformParameterfEXT"), typeof(glPixelTransformParameterfEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelTransformParameterfEXT'.");
            }
        }
        public static void glPixelTransformParameterfEXT(GLenum @target, GLenum @pname, GLfloat @param) => glPixelTransformParameterfEXTPtr(@target, @pname, @param);

        internal delegate void glPixelTransformParameterivEXTFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glPixelTransformParameterivEXTFunc glPixelTransformParameterivEXTPtr;
        internal static void loadPixelTransformParameterivEXT()
        {
            try
            {
                glPixelTransformParameterivEXTPtr = (glPixelTransformParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelTransformParameterivEXT"), typeof(glPixelTransformParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelTransformParameterivEXT'.");
            }
        }
        public static void glPixelTransformParameterivEXT(GLenum @target, GLenum @pname, const GLint * @params) => glPixelTransformParameterivEXTPtr(@target, @pname, @params);

        internal delegate void glPixelTransformParameterfvEXTFunc(GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glPixelTransformParameterfvEXTFunc glPixelTransformParameterfvEXTPtr;
        internal static void loadPixelTransformParameterfvEXT()
        {
            try
            {
                glPixelTransformParameterfvEXTPtr = (glPixelTransformParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelTransformParameterfvEXT"), typeof(glPixelTransformParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelTransformParameterfvEXT'.");
            }
        }
        public static void glPixelTransformParameterfvEXT(GLenum @target, GLenum @pname, const GLfloat * @params) => glPixelTransformParameterfvEXTPtr(@target, @pname, @params);

        internal delegate void glGetPixelTransformParameterivEXTFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetPixelTransformParameterivEXTFunc glGetPixelTransformParameterivEXTPtr;
        internal static void loadGetPixelTransformParameterivEXT()
        {
            try
            {
                glGetPixelTransformParameterivEXTPtr = (glGetPixelTransformParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPixelTransformParameterivEXT"), typeof(glGetPixelTransformParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPixelTransformParameterivEXT'.");
            }
        }
        public static void glGetPixelTransformParameterivEXT(GLenum @target, GLenum @pname, GLint * @params) => glGetPixelTransformParameterivEXTPtr(@target, @pname, @params);

        internal delegate void glGetPixelTransformParameterfvEXTFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetPixelTransformParameterfvEXTFunc glGetPixelTransformParameterfvEXTPtr;
        internal static void loadGetPixelTransformParameterfvEXT()
        {
            try
            {
                glGetPixelTransformParameterfvEXTPtr = (glGetPixelTransformParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPixelTransformParameterfvEXT"), typeof(glGetPixelTransformParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPixelTransformParameterfvEXT'.");
            }
        }
        public static void glGetPixelTransformParameterfvEXT(GLenum @target, GLenum @pname, GLfloat * @params) => glGetPixelTransformParameterfvEXTPtr(@target, @pname, @params);
        #endregion
    }
}
