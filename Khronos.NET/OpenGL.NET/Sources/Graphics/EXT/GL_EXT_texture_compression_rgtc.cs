using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_compression_rgtc
    {
        #region Interop
        static GL_EXT_texture_compression_rgtc()
        {
            Console.WriteLine("Initalising GL_EXT_texture_compression_rgtc interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_RED_RGTC1_EXT = 0x8DBB;
        public static UInt32 GL_COMPRESSED_SIGNED_RED_RGTC1_EXT = 0x8DBC;
        public static UInt32 GL_COMPRESSED_RED_GREEN_RGTC2_EXT = 0x8DBD;
        public static UInt32 GL_COMPRESSED_SIGNED_RED_GREEN_RGTC2_EXT = 0x8DBE;
        #endregion

        #region Commands
        #endregion
    }
}
