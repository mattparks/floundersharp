using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_copy_image
    {
        #region Interop
        static GL_EXT_copy_image()
        {
            Console.WriteLine("Initalising GL_EXT_copy_image interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCopyImageSubDataEXT();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glCopyImageSubDataEXTFunc(GLuint @srcName, GLenum @srcTarget, GLint @srcLevel, GLint @srcX, GLint @srcY, GLint @srcZ, GLuint @dstName, GLenum @dstTarget, GLint @dstLevel, GLint @dstX, GLint @dstY, GLint @dstZ, GLsizei @srcWidth, GLsizei @srcHeight, GLsizei @srcDepth);
        internal static glCopyImageSubDataEXTFunc glCopyImageSubDataEXTPtr;
        internal static void loadCopyImageSubDataEXT()
        {
            try
            {
                glCopyImageSubDataEXTPtr = (glCopyImageSubDataEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyImageSubDataEXT"), typeof(glCopyImageSubDataEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyImageSubDataEXT'.");
            }
        }
        public static void glCopyImageSubDataEXT(GLuint @srcName, GLenum @srcTarget, GLint @srcLevel, GLint @srcX, GLint @srcY, GLint @srcZ, GLuint @dstName, GLenum @dstTarget, GLint @dstLevel, GLint @dstX, GLint @dstY, GLint @dstZ, GLsizei @srcWidth, GLsizei @srcHeight, GLsizei @srcDepth) => glCopyImageSubDataEXTPtr(@srcName, @srcTarget, @srcLevel, @srcX, @srcY, @srcZ, @dstName, @dstTarget, @dstLevel, @dstX, @dstY, @dstZ, @srcWidth, @srcHeight, @srcDepth);
        #endregion
    }
}
