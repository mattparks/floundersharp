using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_cmyka
    {
        #region Interop
        static GL_EXT_cmyka()
        {
            Console.WriteLine("Initalising GL_EXT_cmyka interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_CMYK_EXT = 0x800C;
        public static UInt32 GL_CMYKA_EXT = 0x800D;
        public static UInt32 GL_PACK_CMYK_HINT_EXT = 0x800E;
        public static UInt32 GL_UNPACK_CMYK_HINT_EXT = 0x800F;
        #endregion

        #region Commands
        #endregion
    }
}
