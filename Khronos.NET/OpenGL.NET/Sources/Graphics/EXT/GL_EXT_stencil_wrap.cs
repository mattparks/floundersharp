using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_stencil_wrap
    {
        #region Interop
        static GL_EXT_stencil_wrap()
        {
            Console.WriteLine("Initalising GL_EXT_stencil_wrap interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_INCR_WRAP_EXT = 0x8507;
        public static UInt32 GL_DECR_WRAP_EXT = 0x8508;
        #endregion

        #region Commands
        #endregion
    }
}
