using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_coordinate_frame
    {
        #region Interop
        static GL_EXT_coordinate_frame()
        {
            Console.WriteLine("Initalising GL_EXT_coordinate_frame interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTangent3bEXT();
            loadTangent3bvEXT();
            loadTangent3dEXT();
            loadTangent3dvEXT();
            loadTangent3fEXT();
            loadTangent3fvEXT();
            loadTangent3iEXT();
            loadTangent3ivEXT();
            loadTangent3sEXT();
            loadTangent3svEXT();
            loadBinormal3bEXT();
            loadBinormal3bvEXT();
            loadBinormal3dEXT();
            loadBinormal3dvEXT();
            loadBinormal3fEXT();
            loadBinormal3fvEXT();
            loadBinormal3iEXT();
            loadBinormal3ivEXT();
            loadBinormal3sEXT();
            loadBinormal3svEXT();
            loadTangentPointerEXT();
            loadBinormalPointerEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TANGENT_ARRAY_EXT = 0x8439;
        public static UInt32 GL_BINORMAL_ARRAY_EXT = 0x843A;
        public static UInt32 GL_CURRENT_TANGENT_EXT = 0x843B;
        public static UInt32 GL_CURRENT_BINORMAL_EXT = 0x843C;
        public static UInt32 GL_TANGENT_ARRAY_TYPE_EXT = 0x843E;
        public static UInt32 GL_TANGENT_ARRAY_STRIDE_EXT = 0x843F;
        public static UInt32 GL_BINORMAL_ARRAY_TYPE_EXT = 0x8440;
        public static UInt32 GL_BINORMAL_ARRAY_STRIDE_EXT = 0x8441;
        public static UInt32 GL_TANGENT_ARRAY_POINTER_EXT = 0x8442;
        public static UInt32 GL_BINORMAL_ARRAY_POINTER_EXT = 0x8443;
        public static UInt32 GL_MAP1_TANGENT_EXT = 0x8444;
        public static UInt32 GL_MAP2_TANGENT_EXT = 0x8445;
        public static UInt32 GL_MAP1_BINORMAL_EXT = 0x8446;
        public static UInt32 GL_MAP2_BINORMAL_EXT = 0x8447;
        #endregion

        #region Commands
        internal delegate void glTangent3bEXTFunc(GLbyte @tx, GLbyte @ty, GLbyte @tz);
        internal static glTangent3bEXTFunc glTangent3bEXTPtr;
        internal static void loadTangent3bEXT()
        {
            try
            {
                glTangent3bEXTPtr = (glTangent3bEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTangent3bEXT"), typeof(glTangent3bEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTangent3bEXT'.");
            }
        }
        public static void glTangent3bEXT(GLbyte @tx, GLbyte @ty, GLbyte @tz) => glTangent3bEXTPtr(@tx, @ty, @tz);

        internal delegate void glTangent3bvEXTFunc(const GLbyte * @v);
        internal static glTangent3bvEXTFunc glTangent3bvEXTPtr;
        internal static void loadTangent3bvEXT()
        {
            try
            {
                glTangent3bvEXTPtr = (glTangent3bvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTangent3bvEXT"), typeof(glTangent3bvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTangent3bvEXT'.");
            }
        }
        public static void glTangent3bvEXT(const GLbyte * @v) => glTangent3bvEXTPtr(@v);

        internal delegate void glTangent3dEXTFunc(GLdouble @tx, GLdouble @ty, GLdouble @tz);
        internal static glTangent3dEXTFunc glTangent3dEXTPtr;
        internal static void loadTangent3dEXT()
        {
            try
            {
                glTangent3dEXTPtr = (glTangent3dEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTangent3dEXT"), typeof(glTangent3dEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTangent3dEXT'.");
            }
        }
        public static void glTangent3dEXT(GLdouble @tx, GLdouble @ty, GLdouble @tz) => glTangent3dEXTPtr(@tx, @ty, @tz);

        internal delegate void glTangent3dvEXTFunc(const GLdouble * @v);
        internal static glTangent3dvEXTFunc glTangent3dvEXTPtr;
        internal static void loadTangent3dvEXT()
        {
            try
            {
                glTangent3dvEXTPtr = (glTangent3dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTangent3dvEXT"), typeof(glTangent3dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTangent3dvEXT'.");
            }
        }
        public static void glTangent3dvEXT(const GLdouble * @v) => glTangent3dvEXTPtr(@v);

        internal delegate void glTangent3fEXTFunc(GLfloat @tx, GLfloat @ty, GLfloat @tz);
        internal static glTangent3fEXTFunc glTangent3fEXTPtr;
        internal static void loadTangent3fEXT()
        {
            try
            {
                glTangent3fEXTPtr = (glTangent3fEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTangent3fEXT"), typeof(glTangent3fEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTangent3fEXT'.");
            }
        }
        public static void glTangent3fEXT(GLfloat @tx, GLfloat @ty, GLfloat @tz) => glTangent3fEXTPtr(@tx, @ty, @tz);

        internal delegate void glTangent3fvEXTFunc(const GLfloat * @v);
        internal static glTangent3fvEXTFunc glTangent3fvEXTPtr;
        internal static void loadTangent3fvEXT()
        {
            try
            {
                glTangent3fvEXTPtr = (glTangent3fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTangent3fvEXT"), typeof(glTangent3fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTangent3fvEXT'.");
            }
        }
        public static void glTangent3fvEXT(const GLfloat * @v) => glTangent3fvEXTPtr(@v);

        internal delegate void glTangent3iEXTFunc(GLint @tx, GLint @ty, GLint @tz);
        internal static glTangent3iEXTFunc glTangent3iEXTPtr;
        internal static void loadTangent3iEXT()
        {
            try
            {
                glTangent3iEXTPtr = (glTangent3iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTangent3iEXT"), typeof(glTangent3iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTangent3iEXT'.");
            }
        }
        public static void glTangent3iEXT(GLint @tx, GLint @ty, GLint @tz) => glTangent3iEXTPtr(@tx, @ty, @tz);

        internal delegate void glTangent3ivEXTFunc(const GLint * @v);
        internal static glTangent3ivEXTFunc glTangent3ivEXTPtr;
        internal static void loadTangent3ivEXT()
        {
            try
            {
                glTangent3ivEXTPtr = (glTangent3ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTangent3ivEXT"), typeof(glTangent3ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTangent3ivEXT'.");
            }
        }
        public static void glTangent3ivEXT(const GLint * @v) => glTangent3ivEXTPtr(@v);

        internal delegate void glTangent3sEXTFunc(GLshort @tx, GLshort @ty, GLshort @tz);
        internal static glTangent3sEXTFunc glTangent3sEXTPtr;
        internal static void loadTangent3sEXT()
        {
            try
            {
                glTangent3sEXTPtr = (glTangent3sEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTangent3sEXT"), typeof(glTangent3sEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTangent3sEXT'.");
            }
        }
        public static void glTangent3sEXT(GLshort @tx, GLshort @ty, GLshort @tz) => glTangent3sEXTPtr(@tx, @ty, @tz);

        internal delegate void glTangent3svEXTFunc(const GLshort * @v);
        internal static glTangent3svEXTFunc glTangent3svEXTPtr;
        internal static void loadTangent3svEXT()
        {
            try
            {
                glTangent3svEXTPtr = (glTangent3svEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTangent3svEXT"), typeof(glTangent3svEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTangent3svEXT'.");
            }
        }
        public static void glTangent3svEXT(const GLshort * @v) => glTangent3svEXTPtr(@v);

        internal delegate void glBinormal3bEXTFunc(GLbyte @bx, GLbyte @by, GLbyte @bz);
        internal static glBinormal3bEXTFunc glBinormal3bEXTPtr;
        internal static void loadBinormal3bEXT()
        {
            try
            {
                glBinormal3bEXTPtr = (glBinormal3bEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBinormal3bEXT"), typeof(glBinormal3bEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBinormal3bEXT'.");
            }
        }
        public static void glBinormal3bEXT(GLbyte @bx, GLbyte @by, GLbyte @bz) => glBinormal3bEXTPtr(@bx, @by, @bz);

        internal delegate void glBinormal3bvEXTFunc(const GLbyte * @v);
        internal static glBinormal3bvEXTFunc glBinormal3bvEXTPtr;
        internal static void loadBinormal3bvEXT()
        {
            try
            {
                glBinormal3bvEXTPtr = (glBinormal3bvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBinormal3bvEXT"), typeof(glBinormal3bvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBinormal3bvEXT'.");
            }
        }
        public static void glBinormal3bvEXT(const GLbyte * @v) => glBinormal3bvEXTPtr(@v);

        internal delegate void glBinormal3dEXTFunc(GLdouble @bx, GLdouble @by, GLdouble @bz);
        internal static glBinormal3dEXTFunc glBinormal3dEXTPtr;
        internal static void loadBinormal3dEXT()
        {
            try
            {
                glBinormal3dEXTPtr = (glBinormal3dEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBinormal3dEXT"), typeof(glBinormal3dEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBinormal3dEXT'.");
            }
        }
        public static void glBinormal3dEXT(GLdouble @bx, GLdouble @by, GLdouble @bz) => glBinormal3dEXTPtr(@bx, @by, @bz);

        internal delegate void glBinormal3dvEXTFunc(const GLdouble * @v);
        internal static glBinormal3dvEXTFunc glBinormal3dvEXTPtr;
        internal static void loadBinormal3dvEXT()
        {
            try
            {
                glBinormal3dvEXTPtr = (glBinormal3dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBinormal3dvEXT"), typeof(glBinormal3dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBinormal3dvEXT'.");
            }
        }
        public static void glBinormal3dvEXT(const GLdouble * @v) => glBinormal3dvEXTPtr(@v);

        internal delegate void glBinormal3fEXTFunc(GLfloat @bx, GLfloat @by, GLfloat @bz);
        internal static glBinormal3fEXTFunc glBinormal3fEXTPtr;
        internal static void loadBinormal3fEXT()
        {
            try
            {
                glBinormal3fEXTPtr = (glBinormal3fEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBinormal3fEXT"), typeof(glBinormal3fEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBinormal3fEXT'.");
            }
        }
        public static void glBinormal3fEXT(GLfloat @bx, GLfloat @by, GLfloat @bz) => glBinormal3fEXTPtr(@bx, @by, @bz);

        internal delegate void glBinormal3fvEXTFunc(const GLfloat * @v);
        internal static glBinormal3fvEXTFunc glBinormal3fvEXTPtr;
        internal static void loadBinormal3fvEXT()
        {
            try
            {
                glBinormal3fvEXTPtr = (glBinormal3fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBinormal3fvEXT"), typeof(glBinormal3fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBinormal3fvEXT'.");
            }
        }
        public static void glBinormal3fvEXT(const GLfloat * @v) => glBinormal3fvEXTPtr(@v);

        internal delegate void glBinormal3iEXTFunc(GLint @bx, GLint @by, GLint @bz);
        internal static glBinormal3iEXTFunc glBinormal3iEXTPtr;
        internal static void loadBinormal3iEXT()
        {
            try
            {
                glBinormal3iEXTPtr = (glBinormal3iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBinormal3iEXT"), typeof(glBinormal3iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBinormal3iEXT'.");
            }
        }
        public static void glBinormal3iEXT(GLint @bx, GLint @by, GLint @bz) => glBinormal3iEXTPtr(@bx, @by, @bz);

        internal delegate void glBinormal3ivEXTFunc(const GLint * @v);
        internal static glBinormal3ivEXTFunc glBinormal3ivEXTPtr;
        internal static void loadBinormal3ivEXT()
        {
            try
            {
                glBinormal3ivEXTPtr = (glBinormal3ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBinormal3ivEXT"), typeof(glBinormal3ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBinormal3ivEXT'.");
            }
        }
        public static void glBinormal3ivEXT(const GLint * @v) => glBinormal3ivEXTPtr(@v);

        internal delegate void glBinormal3sEXTFunc(GLshort @bx, GLshort @by, GLshort @bz);
        internal static glBinormal3sEXTFunc glBinormal3sEXTPtr;
        internal static void loadBinormal3sEXT()
        {
            try
            {
                glBinormal3sEXTPtr = (glBinormal3sEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBinormal3sEXT"), typeof(glBinormal3sEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBinormal3sEXT'.");
            }
        }
        public static void glBinormal3sEXT(GLshort @bx, GLshort @by, GLshort @bz) => glBinormal3sEXTPtr(@bx, @by, @bz);

        internal delegate void glBinormal3svEXTFunc(const GLshort * @v);
        internal static glBinormal3svEXTFunc glBinormal3svEXTPtr;
        internal static void loadBinormal3svEXT()
        {
            try
            {
                glBinormal3svEXTPtr = (glBinormal3svEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBinormal3svEXT"), typeof(glBinormal3svEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBinormal3svEXT'.");
            }
        }
        public static void glBinormal3svEXT(const GLshort * @v) => glBinormal3svEXTPtr(@v);

        internal delegate void glTangentPointerEXTFunc(GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glTangentPointerEXTFunc glTangentPointerEXTPtr;
        internal static void loadTangentPointerEXT()
        {
            try
            {
                glTangentPointerEXTPtr = (glTangentPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTangentPointerEXT"), typeof(glTangentPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTangentPointerEXT'.");
            }
        }
        public static void glTangentPointerEXT(GLenum @type, GLsizei @stride, const void * @pointer) => glTangentPointerEXTPtr(@type, @stride, @pointer);

        internal delegate void glBinormalPointerEXTFunc(GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glBinormalPointerEXTFunc glBinormalPointerEXTPtr;
        internal static void loadBinormalPointerEXT()
        {
            try
            {
                glBinormalPointerEXTPtr = (glBinormalPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBinormalPointerEXT"), typeof(glBinormalPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBinormalPointerEXT'.");
            }
        }
        public static void glBinormalPointerEXT(GLenum @type, GLsizei @stride, const void * @pointer) => glBinormalPointerEXTPtr(@type, @stride, @pointer);
        #endregion
    }
}
