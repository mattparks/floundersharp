using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_blend_color
    {
        #region Interop
        static GL_EXT_blend_color()
        {
            Console.WriteLine("Initalising GL_EXT_blend_color interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendColorEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_CONSTANT_COLOR_EXT = 0x8001;
        public static UInt32 GL_ONE_MINUS_CONSTANT_COLOR_EXT = 0x8002;
        public static UInt32 GL_CONSTANT_ALPHA_EXT = 0x8003;
        public static UInt32 GL_ONE_MINUS_CONSTANT_ALPHA_EXT = 0x8004;
        public static UInt32 GL_BLEND_COLOR_EXT = 0x8005;
        #endregion

        #region Commands
        internal delegate void glBlendColorEXTFunc(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha);
        internal static glBlendColorEXTFunc glBlendColorEXTPtr;
        internal static void loadBlendColorEXT()
        {
            try
            {
                glBlendColorEXTPtr = (glBlendColorEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendColorEXT"), typeof(glBlendColorEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendColorEXT'.");
            }
        }
        public static void glBlendColorEXT(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha) => glBlendColorEXTPtr(@red, @green, @blue, @alpha);
        #endregion
    }
}
