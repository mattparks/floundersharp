using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_object
    {
        #region Interop
        static GL_EXT_texture_object()
        {
            Console.WriteLine("Initalising GL_EXT_texture_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadAreTexturesResidentEXT();
            loadBindTextureEXT();
            loadDeleteTexturesEXT();
            loadGenTexturesEXT();
            loadIsTextureEXT();
            loadPrioritizeTexturesEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_PRIORITY_EXT = 0x8066;
        public static UInt32 GL_TEXTURE_RESIDENT_EXT = 0x8067;
        public static UInt32 GL_TEXTURE_1D_BINDING_EXT = 0x8068;
        public static UInt32 GL_TEXTURE_2D_BINDING_EXT = 0x8069;
        public static UInt32 GL_TEXTURE_3D_BINDING_EXT = 0x806A;
        #endregion

        #region Commands
        internal delegate GLboolean glAreTexturesResidentEXTFunc(GLsizei @n, const GLuint * @textures, GLboolean * @residences);
        internal static glAreTexturesResidentEXTFunc glAreTexturesResidentEXTPtr;
        internal static void loadAreTexturesResidentEXT()
        {
            try
            {
                glAreTexturesResidentEXTPtr = (glAreTexturesResidentEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAreTexturesResidentEXT"), typeof(glAreTexturesResidentEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAreTexturesResidentEXT'.");
            }
        }
        public static GLboolean glAreTexturesResidentEXT(GLsizei @n, const GLuint * @textures, GLboolean * @residences) => glAreTexturesResidentEXTPtr(@n, @textures, @residences);

        internal delegate void glBindTextureEXTFunc(GLenum @target, GLuint @texture);
        internal static glBindTextureEXTFunc glBindTextureEXTPtr;
        internal static void loadBindTextureEXT()
        {
            try
            {
                glBindTextureEXTPtr = (glBindTextureEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTextureEXT"), typeof(glBindTextureEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTextureEXT'.");
            }
        }
        public static void glBindTextureEXT(GLenum @target, GLuint @texture) => glBindTextureEXTPtr(@target, @texture);

        internal delegate void glDeleteTexturesEXTFunc(GLsizei @n, const GLuint * @textures);
        internal static glDeleteTexturesEXTFunc glDeleteTexturesEXTPtr;
        internal static void loadDeleteTexturesEXT()
        {
            try
            {
                glDeleteTexturesEXTPtr = (glDeleteTexturesEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteTexturesEXT"), typeof(glDeleteTexturesEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteTexturesEXT'.");
            }
        }
        public static void glDeleteTexturesEXT(GLsizei @n, const GLuint * @textures) => glDeleteTexturesEXTPtr(@n, @textures);

        internal delegate void glGenTexturesEXTFunc(GLsizei @n, GLuint * @textures);
        internal static glGenTexturesEXTFunc glGenTexturesEXTPtr;
        internal static void loadGenTexturesEXT()
        {
            try
            {
                glGenTexturesEXTPtr = (glGenTexturesEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenTexturesEXT"), typeof(glGenTexturesEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenTexturesEXT'.");
            }
        }
        public static void glGenTexturesEXT(GLsizei @n, GLuint * @textures) => glGenTexturesEXTPtr(@n, @textures);

        internal delegate GLboolean glIsTextureEXTFunc(GLuint @texture);
        internal static glIsTextureEXTFunc glIsTextureEXTPtr;
        internal static void loadIsTextureEXT()
        {
            try
            {
                glIsTextureEXTPtr = (glIsTextureEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsTextureEXT"), typeof(glIsTextureEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsTextureEXT'.");
            }
        }
        public static GLboolean glIsTextureEXT(GLuint @texture) => glIsTextureEXTPtr(@texture);

        internal delegate void glPrioritizeTexturesEXTFunc(GLsizei @n, const GLuint * @textures, const GLclampf * @priorities);
        internal static glPrioritizeTexturesEXTFunc glPrioritizeTexturesEXTPtr;
        internal static void loadPrioritizeTexturesEXT()
        {
            try
            {
                glPrioritizeTexturesEXTPtr = (glPrioritizeTexturesEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPrioritizeTexturesEXT"), typeof(glPrioritizeTexturesEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPrioritizeTexturesEXT'.");
            }
        }
        public static void glPrioritizeTexturesEXT(GLsizei @n, const GLuint * @textures, const GLclampf * @priorities) => glPrioritizeTexturesEXTPtr(@n, @textures, @priorities);
        #endregion
    }
}
