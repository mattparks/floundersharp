using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_map_buffer_range
    {
        #region Interop
        static GL_EXT_map_buffer_range()
        {
            Console.WriteLine("Initalising GL_EXT_map_buffer_range interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMapBufferRangeEXT();
            loadFlushMappedBufferRangeEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAP_READ_BIT_EXT = 0x0001;
        public static UInt32 GL_MAP_WRITE_BIT_EXT = 0x0002;
        public static UInt32 GL_MAP_INVALIDATE_RANGE_BIT_EXT = 0x0004;
        public static UInt32 GL_MAP_INVALIDATE_BUFFER_BIT_EXT = 0x0008;
        public static UInt32 GL_MAP_FLUSH_EXPLICIT_BIT_EXT = 0x0010;
        public static UInt32 GL_MAP_UNSYNCHRONIZED_BIT_EXT = 0x0020;
        #endregion

        #region Commands
        internal delegate void * glMapBufferRangeEXTFunc(GLenum @target, GLintptr @offset, GLsizeiptr @length, GLbitfield @access);
        internal static glMapBufferRangeEXTFunc glMapBufferRangeEXTPtr;
        internal static void loadMapBufferRangeEXT()
        {
            try
            {
                glMapBufferRangeEXTPtr = (glMapBufferRangeEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapBufferRangeEXT"), typeof(glMapBufferRangeEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapBufferRangeEXT'.");
            }
        }
        public static void * glMapBufferRangeEXT(GLenum @target, GLintptr @offset, GLsizeiptr @length, GLbitfield @access) => glMapBufferRangeEXTPtr(@target, @offset, @length, @access);

        internal delegate void glFlushMappedBufferRangeEXTFunc(GLenum @target, GLintptr @offset, GLsizeiptr @length);
        internal static glFlushMappedBufferRangeEXTFunc glFlushMappedBufferRangeEXTPtr;
        internal static void loadFlushMappedBufferRangeEXT()
        {
            try
            {
                glFlushMappedBufferRangeEXTPtr = (glFlushMappedBufferRangeEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlushMappedBufferRangeEXT"), typeof(glFlushMappedBufferRangeEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlushMappedBufferRangeEXT'.");
            }
        }
        public static void glFlushMappedBufferRangeEXT(GLenum @target, GLintptr @offset, GLsizeiptr @length) => glFlushMappedBufferRangeEXTPtr(@target, @offset, @length);
        #endregion
    }
}
