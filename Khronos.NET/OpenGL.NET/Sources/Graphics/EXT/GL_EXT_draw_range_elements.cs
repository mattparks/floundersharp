using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_draw_range_elements
    {
        #region Interop
        static GL_EXT_draw_range_elements()
        {
            Console.WriteLine("Initalising GL_EXT_draw_range_elements interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawRangeElementsEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_ELEMENTS_VERTICES_EXT = 0x80E8;
        public static UInt32 GL_MAX_ELEMENTS_INDICES_EXT = 0x80E9;
        #endregion

        #region Commands
        internal delegate void glDrawRangeElementsEXTFunc(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices);
        internal static glDrawRangeElementsEXTFunc glDrawRangeElementsEXTPtr;
        internal static void loadDrawRangeElementsEXT()
        {
            try
            {
                glDrawRangeElementsEXTPtr = (glDrawRangeElementsEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawRangeElementsEXT"), typeof(glDrawRangeElementsEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawRangeElementsEXT'.");
            }
        }
        public static void glDrawRangeElementsEXT(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices) => glDrawRangeElementsEXTPtr(@mode, @start, @end, @count, @type, @indices);
        #endregion
    }
}
