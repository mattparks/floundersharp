using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_bgra
    {
        #region Interop
        static GL_EXT_bgra()
        {
            Console.WriteLine("Initalising GL_EXT_bgra interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_BGR_EXT = 0x80E0;
        public static UInt32 GL_BGRA_EXT = 0x80E1;
        #endregion

        #region Commands
        #endregion
    }
}
