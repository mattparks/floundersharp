using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_cube_map
    {
        #region Interop
        static GL_EXT_texture_cube_map()
        {
            Console.WriteLine("Initalising GL_EXT_texture_cube_map interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_NORMAL_MAP_EXT = 0x8511;
        public static UInt32 GL_REFLECTION_MAP_EXT = 0x8512;
        public static UInt32 GL_TEXTURE_CUBE_MAP_EXT = 0x8513;
        public static UInt32 GL_TEXTURE_BINDING_CUBE_MAP_EXT = 0x8514;
        public static UInt32 GL_TEXTURE_CUBE_MAP_POSITIVE_X_EXT = 0x8515;
        public static UInt32 GL_TEXTURE_CUBE_MAP_NEGATIVE_X_EXT = 0x8516;
        public static UInt32 GL_TEXTURE_CUBE_MAP_POSITIVE_Y_EXT = 0x8517;
        public static UInt32 GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_EXT = 0x8518;
        public static UInt32 GL_TEXTURE_CUBE_MAP_POSITIVE_Z_EXT = 0x8519;
        public static UInt32 GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_EXT = 0x851A;
        public static UInt32 GL_PROXY_TEXTURE_CUBE_MAP_EXT = 0x851B;
        public static UInt32 GL_MAX_CUBE_MAP_TEXTURE_SIZE_EXT = 0x851C;
        #endregion

        #region Commands
        #endregion
    }
}
