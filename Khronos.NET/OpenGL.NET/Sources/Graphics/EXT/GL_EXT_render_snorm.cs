using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_render_snorm
    {
        #region Interop
        static GL_EXT_render_snorm()
        {
            Console.WriteLine("Initalising GL_EXT_render_snorm interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_BYTE = 0x1400;
        public static UInt32 GL_SHORT = 0x1402;
        public static UInt32 GL_R8_SNORM = 0x8F94;
        public static UInt32 GL_RG8_SNORM = 0x8F95;
        public static UInt32 GL_RGBA8_SNORM = 0x8F97;
        public static UInt32 GL_R16_SNORM_EXT = 0x8F98;
        public static UInt32 GL_RG16_SNORM_EXT = 0x8F99;
        public static UInt32 GL_RGBA16_SNORM_EXT = 0x8F9B;
        #endregion

        #region Commands
        #endregion
    }
}
