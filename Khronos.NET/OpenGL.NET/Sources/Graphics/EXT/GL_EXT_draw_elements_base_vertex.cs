using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_draw_elements_base_vertex
    {
        #region Interop
        static GL_EXT_draw_elements_base_vertex()
        {
            Console.WriteLine("Initalising GL_EXT_draw_elements_base_vertex interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawElementsBaseVertexEXT();
            loadDrawRangeElementsBaseVertexEXT();
            loadDrawElementsInstancedBaseVertexEXT();
            loadMultiDrawElementsBaseVertexEXT();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glDrawElementsBaseVertexEXTFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex);
        internal static glDrawElementsBaseVertexEXTFunc glDrawElementsBaseVertexEXTPtr;
        internal static void loadDrawElementsBaseVertexEXT()
        {
            try
            {
                glDrawElementsBaseVertexEXTPtr = (glDrawElementsBaseVertexEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsBaseVertexEXT"), typeof(glDrawElementsBaseVertexEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsBaseVertexEXT'.");
            }
        }
        public static void glDrawElementsBaseVertexEXT(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex) => glDrawElementsBaseVertexEXTPtr(@mode, @count, @type, @indices, @basevertex);

        internal delegate void glDrawRangeElementsBaseVertexEXTFunc(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex);
        internal static glDrawRangeElementsBaseVertexEXTFunc glDrawRangeElementsBaseVertexEXTPtr;
        internal static void loadDrawRangeElementsBaseVertexEXT()
        {
            try
            {
                glDrawRangeElementsBaseVertexEXTPtr = (glDrawRangeElementsBaseVertexEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawRangeElementsBaseVertexEXT"), typeof(glDrawRangeElementsBaseVertexEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawRangeElementsBaseVertexEXT'.");
            }
        }
        public static void glDrawRangeElementsBaseVertexEXT(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex) => glDrawRangeElementsBaseVertexEXTPtr(@mode, @start, @end, @count, @type, @indices, @basevertex);

        internal delegate void glDrawElementsInstancedBaseVertexEXTFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex);
        internal static glDrawElementsInstancedBaseVertexEXTFunc glDrawElementsInstancedBaseVertexEXTPtr;
        internal static void loadDrawElementsInstancedBaseVertexEXT()
        {
            try
            {
                glDrawElementsInstancedBaseVertexEXTPtr = (glDrawElementsInstancedBaseVertexEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedBaseVertexEXT"), typeof(glDrawElementsInstancedBaseVertexEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedBaseVertexEXT'.");
            }
        }
        public static void glDrawElementsInstancedBaseVertexEXT(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex) => glDrawElementsInstancedBaseVertexEXTPtr(@mode, @count, @type, @indices, @instancecount, @basevertex);

        internal delegate void glMultiDrawElementsBaseVertexEXTFunc(GLenum @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @primcount, const GLint * @basevertex);
        internal static glMultiDrawElementsBaseVertexEXTFunc glMultiDrawElementsBaseVertexEXTPtr;
        internal static void loadMultiDrawElementsBaseVertexEXT()
        {
            try
            {
                glMultiDrawElementsBaseVertexEXTPtr = (glMultiDrawElementsBaseVertexEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementsBaseVertexEXT"), typeof(glMultiDrawElementsBaseVertexEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementsBaseVertexEXT'.");
            }
        }
        public static void glMultiDrawElementsBaseVertexEXT(GLenum @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @primcount, const GLint * @basevertex) => glMultiDrawElementsBaseVertexEXTPtr(@mode, @count, @type, @indices, @primcount, @basevertex);
        #endregion
    }
}
