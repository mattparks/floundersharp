using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_provoking_vertex
    {
        #region Interop
        static GL_EXT_provoking_vertex()
        {
            Console.WriteLine("Initalising GL_EXT_provoking_vertex interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadProvokingVertexEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_QUADS_FOLLOW_PROVOKING_VERTEX_CONVENTION_EXT = 0x8E4C;
        public static UInt32 GL_FIRST_VERTEX_CONVENTION_EXT = 0x8E4D;
        public static UInt32 GL_LAST_VERTEX_CONVENTION_EXT = 0x8E4E;
        public static UInt32 GL_PROVOKING_VERTEX_EXT = 0x8E4F;
        #endregion

        #region Commands
        internal delegate void glProvokingVertexEXTFunc(GLenum @mode);
        internal static glProvokingVertexEXTFunc glProvokingVertexEXTPtr;
        internal static void loadProvokingVertexEXT()
        {
            try
            {
                glProvokingVertexEXTPtr = (glProvokingVertexEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProvokingVertexEXT"), typeof(glProvokingVertexEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProvokingVertexEXT'.");
            }
        }
        public static void glProvokingVertexEXT(GLenum @mode) => glProvokingVertexEXTPtr(@mode);
        #endregion
    }
}
