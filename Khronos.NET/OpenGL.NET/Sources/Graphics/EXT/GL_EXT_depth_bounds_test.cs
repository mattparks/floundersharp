using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_depth_bounds_test
    {
        #region Interop
        static GL_EXT_depth_bounds_test()
        {
            Console.WriteLine("Initalising GL_EXT_depth_bounds_test interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDepthBoundsEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_BOUNDS_TEST_EXT = 0x8890;
        public static UInt32 GL_DEPTH_BOUNDS_EXT = 0x8891;
        #endregion

        #region Commands
        internal delegate void glDepthBoundsEXTFunc(GLclampd @zmin, GLclampd @zmax);
        internal static glDepthBoundsEXTFunc glDepthBoundsEXTPtr;
        internal static void loadDepthBoundsEXT()
        {
            try
            {
                glDepthBoundsEXTPtr = (glDepthBoundsEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthBoundsEXT"), typeof(glDepthBoundsEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthBoundsEXT'.");
            }
        }
        public static void glDepthBoundsEXT(GLclampd @zmin, GLclampd @zmax) => glDepthBoundsEXTPtr(@zmin, @zmax);
        #endregion
    }
}
