using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_framebuffer_multisample
    {
        #region Interop
        static GL_EXT_framebuffer_multisample()
        {
            Console.WriteLine("Initalising GL_EXT_framebuffer_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadRenderbufferStorageMultisampleEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RENDERBUFFER_SAMPLES_EXT = 0x8CAB;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_EXT = 0x8D56;
        public static UInt32 GL_MAX_SAMPLES_EXT = 0x8D57;
        #endregion

        #region Commands
        internal delegate void glRenderbufferStorageMultisampleEXTFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageMultisampleEXTFunc glRenderbufferStorageMultisampleEXTPtr;
        internal static void loadRenderbufferStorageMultisampleEXT()
        {
            try
            {
                glRenderbufferStorageMultisampleEXTPtr = (glRenderbufferStorageMultisampleEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorageMultisampleEXT"), typeof(glRenderbufferStorageMultisampleEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorageMultisampleEXT'.");
            }
        }
        public static void glRenderbufferStorageMultisampleEXT(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStorageMultisampleEXTPtr(@target, @samples, @internalformat, @width, @height);
        #endregion
    }
}
