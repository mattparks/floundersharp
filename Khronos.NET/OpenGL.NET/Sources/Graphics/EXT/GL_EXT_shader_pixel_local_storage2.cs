using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_shader_pixel_local_storage2
    {
        #region Interop
        static GL_EXT_shader_pixel_local_storage2()
        {
            Console.WriteLine("Initalising GL_EXT_shader_pixel_local_storage2 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFramebufferPixelLocalStorageSizeEXT();
            loadGetFramebufferPixelLocalStorageSizeEXT();
            loadClearPixelLocalStorageuiEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_SHADER_COMBINED_LOCAL_STORAGE_FAST_SIZE_EXT = 0x9650;
        public static UInt32 GL_MAX_SHADER_COMBINED_LOCAL_STORAGE_SIZE_EXT = 0x9651;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_INSUFFICIENT_SHADER_COMBINED_LOCAL_STORAGE_EXT = 0x9652;
        #endregion

        #region Commands
        internal delegate void glFramebufferPixelLocalStorageSizeEXTFunc(GLuint @target, GLsizei @size);
        internal static glFramebufferPixelLocalStorageSizeEXTFunc glFramebufferPixelLocalStorageSizeEXTPtr;
        internal static void loadFramebufferPixelLocalStorageSizeEXT()
        {
            try
            {
                glFramebufferPixelLocalStorageSizeEXTPtr = (glFramebufferPixelLocalStorageSizeEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferPixelLocalStorageSizeEXT"), typeof(glFramebufferPixelLocalStorageSizeEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferPixelLocalStorageSizeEXT'.");
            }
        }
        public static void glFramebufferPixelLocalStorageSizeEXT(GLuint @target, GLsizei @size) => glFramebufferPixelLocalStorageSizeEXTPtr(@target, @size);

        internal delegate GLsizei glGetFramebufferPixelLocalStorageSizeEXTFunc(GLuint @target);
        internal static glGetFramebufferPixelLocalStorageSizeEXTFunc glGetFramebufferPixelLocalStorageSizeEXTPtr;
        internal static void loadGetFramebufferPixelLocalStorageSizeEXT()
        {
            try
            {
                glGetFramebufferPixelLocalStorageSizeEXTPtr = (glGetFramebufferPixelLocalStorageSizeEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFramebufferPixelLocalStorageSizeEXT"), typeof(glGetFramebufferPixelLocalStorageSizeEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFramebufferPixelLocalStorageSizeEXT'.");
            }
        }
        public static GLsizei glGetFramebufferPixelLocalStorageSizeEXT(GLuint @target) => glGetFramebufferPixelLocalStorageSizeEXTPtr(@target);

        internal delegate void glClearPixelLocalStorageuiEXTFunc(GLsizei @offset, GLsizei @n, const GLuint * @values);
        internal static glClearPixelLocalStorageuiEXTFunc glClearPixelLocalStorageuiEXTPtr;
        internal static void loadClearPixelLocalStorageuiEXT()
        {
            try
            {
                glClearPixelLocalStorageuiEXTPtr = (glClearPixelLocalStorageuiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearPixelLocalStorageuiEXT"), typeof(glClearPixelLocalStorageuiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearPixelLocalStorageuiEXT'.");
            }
        }
        public static void glClearPixelLocalStorageuiEXT(GLsizei @offset, GLsizei @n, const GLuint * @values) => glClearPixelLocalStorageuiEXTPtr(@offset, @n, @values);
        #endregion
    }
}
