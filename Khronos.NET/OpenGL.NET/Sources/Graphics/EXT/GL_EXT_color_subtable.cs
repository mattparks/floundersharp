using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_color_subtable
    {
        #region Interop
        static GL_EXT_color_subtable()
        {
            Console.WriteLine("Initalising GL_EXT_color_subtable interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadColorSubTableEXT();
            loadCopyColorSubTableEXT();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glColorSubTableEXTFunc(GLenum @target, GLsizei @start, GLsizei @count, GLenum @format, GLenum @type, const void * @data);
        internal static glColorSubTableEXTFunc glColorSubTableEXTPtr;
        internal static void loadColorSubTableEXT()
        {
            try
            {
                glColorSubTableEXTPtr = (glColorSubTableEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorSubTableEXT"), typeof(glColorSubTableEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorSubTableEXT'.");
            }
        }
        public static void glColorSubTableEXT(GLenum @target, GLsizei @start, GLsizei @count, GLenum @format, GLenum @type, const void * @data) => glColorSubTableEXTPtr(@target, @start, @count, @format, @type, @data);

        internal delegate void glCopyColorSubTableEXTFunc(GLenum @target, GLsizei @start, GLint @x, GLint @y, GLsizei @width);
        internal static glCopyColorSubTableEXTFunc glCopyColorSubTableEXTPtr;
        internal static void loadCopyColorSubTableEXT()
        {
            try
            {
                glCopyColorSubTableEXTPtr = (glCopyColorSubTableEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyColorSubTableEXT"), typeof(glCopyColorSubTableEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyColorSubTableEXT'.");
            }
        }
        public static void glCopyColorSubTableEXT(GLenum @target, GLsizei @start, GLint @x, GLint @y, GLsizei @width) => glCopyColorSubTableEXTPtr(@target, @start, @x, @y, @width);
        #endregion
    }
}
