using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_multisampled_render_to_texture
    {
        #region Interop
        static GL_EXT_multisampled_render_to_texture()
        {
            Console.WriteLine("Initalising GL_EXT_multisampled_render_to_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadRenderbufferStorageMultisampleEXT();
            loadFramebufferTexture2DMultisampleEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_SAMPLES_EXT = 0x8D6C;
        public static UInt32 GL_RENDERBUFFER_SAMPLES_EXT = 0x8CAB;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_EXT = 0x8D56;
        public static UInt32 GL_MAX_SAMPLES_EXT = 0x8D57;
        #endregion

        #region Commands
        internal delegate void glRenderbufferStorageMultisampleEXTFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageMultisampleEXTFunc glRenderbufferStorageMultisampleEXTPtr;
        internal static void loadRenderbufferStorageMultisampleEXT()
        {
            try
            {
                glRenderbufferStorageMultisampleEXTPtr = (glRenderbufferStorageMultisampleEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorageMultisampleEXT"), typeof(glRenderbufferStorageMultisampleEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorageMultisampleEXT'.");
            }
        }
        public static void glRenderbufferStorageMultisampleEXT(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStorageMultisampleEXTPtr(@target, @samples, @internalformat, @width, @height);

        internal delegate void glFramebufferTexture2DMultisampleEXTFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLsizei @samples);
        internal static glFramebufferTexture2DMultisampleEXTFunc glFramebufferTexture2DMultisampleEXTPtr;
        internal static void loadFramebufferTexture2DMultisampleEXT()
        {
            try
            {
                glFramebufferTexture2DMultisampleEXTPtr = (glFramebufferTexture2DMultisampleEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture2DMultisampleEXT"), typeof(glFramebufferTexture2DMultisampleEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture2DMultisampleEXT'.");
            }
        }
        public static void glFramebufferTexture2DMultisampleEXT(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLsizei @samples) => glFramebufferTexture2DMultisampleEXTPtr(@target, @attachment, @textarget, @texture, @level, @samples);
        #endregion
    }
}
