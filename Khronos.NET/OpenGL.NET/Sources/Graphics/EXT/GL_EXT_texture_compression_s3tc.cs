using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_compression_s3tc
    {
        #region Interop
        static GL_EXT_texture_compression_s3tc()
        {
            Console.WriteLine("Initalising GL_EXT_texture_compression_s3tc interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_RGB_S3TC_DXT1_EXT = 0x83F0;
        public static UInt32 GL_COMPRESSED_RGBA_S3TC_DXT1_EXT = 0x83F1;
        public static UInt32 GL_COMPRESSED_RGBA_S3TC_DXT3_EXT = 0x83F2;
        public static UInt32 GL_COMPRESSED_RGBA_S3TC_DXT5_EXT = 0x83F3;
        #endregion

        #region Commands
        #endregion
    }
}
