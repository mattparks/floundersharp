using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_shared_exponent
    {
        #region Interop
        static GL_EXT_texture_shared_exponent()
        {
            Console.WriteLine("Initalising GL_EXT_texture_shared_exponent interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RGB9_E5_EXT = 0x8C3D;
        public static UInt32 GL_UNSIGNED_INT_5_9_9_9_REV_EXT = 0x8C3E;
        public static UInt32 GL_TEXTURE_SHARED_SIZE_EXT = 0x8C3F;
        #endregion

        #region Commands
        #endregion
    }
}
