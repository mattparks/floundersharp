using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_vertex_array
    {
        #region Interop
        static GL_EXT_vertex_array()
        {
            Console.WriteLine("Initalising GL_EXT_vertex_array interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadArrayElementEXT();
            loadColorPointerEXT();
            loadDrawArraysEXT();
            loadEdgeFlagPointerEXT();
            loadGetPointervEXT();
            loadIndexPointerEXT();
            loadNormalPointerEXT();
            loadTexCoordPointerEXT();
            loadVertexPointerEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ARRAY_EXT = 0x8074;
        public static UInt32 GL_NORMAL_ARRAY_EXT = 0x8075;
        public static UInt32 GL_COLOR_ARRAY_EXT = 0x8076;
        public static UInt32 GL_INDEX_ARRAY_EXT = 0x8077;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_EXT = 0x8078;
        public static UInt32 GL_EDGE_FLAG_ARRAY_EXT = 0x8079;
        public static UInt32 GL_VERTEX_ARRAY_SIZE_EXT = 0x807A;
        public static UInt32 GL_VERTEX_ARRAY_TYPE_EXT = 0x807B;
        public static UInt32 GL_VERTEX_ARRAY_STRIDE_EXT = 0x807C;
        public static UInt32 GL_VERTEX_ARRAY_COUNT_EXT = 0x807D;
        public static UInt32 GL_NORMAL_ARRAY_TYPE_EXT = 0x807E;
        public static UInt32 GL_NORMAL_ARRAY_STRIDE_EXT = 0x807F;
        public static UInt32 GL_NORMAL_ARRAY_COUNT_EXT = 0x8080;
        public static UInt32 GL_COLOR_ARRAY_SIZE_EXT = 0x8081;
        public static UInt32 GL_COLOR_ARRAY_TYPE_EXT = 0x8082;
        public static UInt32 GL_COLOR_ARRAY_STRIDE_EXT = 0x8083;
        public static UInt32 GL_COLOR_ARRAY_COUNT_EXT = 0x8084;
        public static UInt32 GL_INDEX_ARRAY_TYPE_EXT = 0x8085;
        public static UInt32 GL_INDEX_ARRAY_STRIDE_EXT = 0x8086;
        public static UInt32 GL_INDEX_ARRAY_COUNT_EXT = 0x8087;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_SIZE_EXT = 0x8088;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_TYPE_EXT = 0x8089;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_STRIDE_EXT = 0x808A;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_COUNT_EXT = 0x808B;
        public static UInt32 GL_EDGE_FLAG_ARRAY_STRIDE_EXT = 0x808C;
        public static UInt32 GL_EDGE_FLAG_ARRAY_COUNT_EXT = 0x808D;
        public static UInt32 GL_VERTEX_ARRAY_POINTER_EXT = 0x808E;
        public static UInt32 GL_NORMAL_ARRAY_POINTER_EXT = 0x808F;
        public static UInt32 GL_COLOR_ARRAY_POINTER_EXT = 0x8090;
        public static UInt32 GL_INDEX_ARRAY_POINTER_EXT = 0x8091;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_POINTER_EXT = 0x8092;
        public static UInt32 GL_EDGE_FLAG_ARRAY_POINTER_EXT = 0x8093;
        #endregion

        #region Commands
        internal delegate void glArrayElementEXTFunc(GLint @i);
        internal static glArrayElementEXTFunc glArrayElementEXTPtr;
        internal static void loadArrayElementEXT()
        {
            try
            {
                glArrayElementEXTPtr = (glArrayElementEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glArrayElementEXT"), typeof(glArrayElementEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glArrayElementEXT'.");
            }
        }
        public static void glArrayElementEXT(GLint @i) => glArrayElementEXTPtr(@i);

        internal delegate void glColorPointerEXTFunc(GLint @size, GLenum @type, GLsizei @stride, GLsizei @count, const void * @pointer);
        internal static glColorPointerEXTFunc glColorPointerEXTPtr;
        internal static void loadColorPointerEXT()
        {
            try
            {
                glColorPointerEXTPtr = (glColorPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorPointerEXT"), typeof(glColorPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorPointerEXT'.");
            }
        }
        public static void glColorPointerEXT(GLint @size, GLenum @type, GLsizei @stride, GLsizei @count, const void * @pointer) => glColorPointerEXTPtr(@size, @type, @stride, @count, @pointer);

        internal delegate void glDrawArraysEXTFunc(GLenum @mode, GLint @first, GLsizei @count);
        internal static glDrawArraysEXTFunc glDrawArraysEXTPtr;
        internal static void loadDrawArraysEXT()
        {
            try
            {
                glDrawArraysEXTPtr = (glDrawArraysEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysEXT"), typeof(glDrawArraysEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysEXT'.");
            }
        }
        public static void glDrawArraysEXT(GLenum @mode, GLint @first, GLsizei @count) => glDrawArraysEXTPtr(@mode, @first, @count);

        internal delegate void glEdgeFlagPointerEXTFunc(GLsizei @stride, GLsizei @count, const GLboolean * @pointer);
        internal static glEdgeFlagPointerEXTFunc glEdgeFlagPointerEXTPtr;
        internal static void loadEdgeFlagPointerEXT()
        {
            try
            {
                glEdgeFlagPointerEXTPtr = (glEdgeFlagPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEdgeFlagPointerEXT"), typeof(glEdgeFlagPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEdgeFlagPointerEXT'.");
            }
        }
        public static void glEdgeFlagPointerEXT(GLsizei @stride, GLsizei @count, const GLboolean * @pointer) => glEdgeFlagPointerEXTPtr(@stride, @count, @pointer);

        internal delegate void glGetPointervEXTFunc(GLenum @pname, void ** @params);
        internal static glGetPointervEXTFunc glGetPointervEXTPtr;
        internal static void loadGetPointervEXT()
        {
            try
            {
                glGetPointervEXTPtr = (glGetPointervEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPointervEXT"), typeof(glGetPointervEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPointervEXT'.");
            }
        }
        public static void glGetPointervEXT(GLenum @pname, void ** @params) => glGetPointervEXTPtr(@pname, @params);

        internal delegate void glIndexPointerEXTFunc(GLenum @type, GLsizei @stride, GLsizei @count, const void * @pointer);
        internal static glIndexPointerEXTFunc glIndexPointerEXTPtr;
        internal static void loadIndexPointerEXT()
        {
            try
            {
                glIndexPointerEXTPtr = (glIndexPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexPointerEXT"), typeof(glIndexPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexPointerEXT'.");
            }
        }
        public static void glIndexPointerEXT(GLenum @type, GLsizei @stride, GLsizei @count, const void * @pointer) => glIndexPointerEXTPtr(@type, @stride, @count, @pointer);

        internal delegate void glNormalPointerEXTFunc(GLenum @type, GLsizei @stride, GLsizei @count, const void * @pointer);
        internal static glNormalPointerEXTFunc glNormalPointerEXTPtr;
        internal static void loadNormalPointerEXT()
        {
            try
            {
                glNormalPointerEXTPtr = (glNormalPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalPointerEXT"), typeof(glNormalPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalPointerEXT'.");
            }
        }
        public static void glNormalPointerEXT(GLenum @type, GLsizei @stride, GLsizei @count, const void * @pointer) => glNormalPointerEXTPtr(@type, @stride, @count, @pointer);

        internal delegate void glTexCoordPointerEXTFunc(GLint @size, GLenum @type, GLsizei @stride, GLsizei @count, const void * @pointer);
        internal static glTexCoordPointerEXTFunc glTexCoordPointerEXTPtr;
        internal static void loadTexCoordPointerEXT()
        {
            try
            {
                glTexCoordPointerEXTPtr = (glTexCoordPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordPointerEXT"), typeof(glTexCoordPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordPointerEXT'.");
            }
        }
        public static void glTexCoordPointerEXT(GLint @size, GLenum @type, GLsizei @stride, GLsizei @count, const void * @pointer) => glTexCoordPointerEXTPtr(@size, @type, @stride, @count, @pointer);

        internal delegate void glVertexPointerEXTFunc(GLint @size, GLenum @type, GLsizei @stride, GLsizei @count, const void * @pointer);
        internal static glVertexPointerEXTFunc glVertexPointerEXTPtr;
        internal static void loadVertexPointerEXT()
        {
            try
            {
                glVertexPointerEXTPtr = (glVertexPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexPointerEXT"), typeof(glVertexPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexPointerEXT'.");
            }
        }
        public static void glVertexPointerEXT(GLint @size, GLenum @type, GLsizei @stride, GLsizei @count, const void * @pointer) => glVertexPointerEXTPtr(@size, @type, @stride, @count, @pointer);
        #endregion
    }
}
