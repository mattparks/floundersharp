using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_buffer_object
    {
        #region Interop
        static GL_EXT_texture_buffer_object()
        {
            Console.WriteLine("Initalising GL_EXT_texture_buffer_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexBufferEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_BUFFER_EXT = 0x8C2A;
        public static UInt32 GL_MAX_TEXTURE_BUFFER_SIZE_EXT = 0x8C2B;
        public static UInt32 GL_TEXTURE_BINDING_BUFFER_EXT = 0x8C2C;
        public static UInt32 GL_TEXTURE_BUFFER_DATA_STORE_BINDING_EXT = 0x8C2D;
        public static UInt32 GL_TEXTURE_BUFFER_FORMAT_EXT = 0x8C2E;
        #endregion

        #region Commands
        internal delegate void glTexBufferEXTFunc(GLenum @target, GLenum @internalformat, GLuint @buffer);
        internal static glTexBufferEXTFunc glTexBufferEXTPtr;
        internal static void loadTexBufferEXT()
        {
            try
            {
                glTexBufferEXTPtr = (glTexBufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBufferEXT"), typeof(glTexBufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBufferEXT'.");
            }
        }
        public static void glTexBufferEXT(GLenum @target, GLenum @internalformat, GLuint @buffer) => glTexBufferEXTPtr(@target, @internalformat, @buffer);
        #endregion
    }
}
