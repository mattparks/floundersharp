using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_separate_specular_color
    {
        #region Interop
        static GL_EXT_separate_specular_color()
        {
            Console.WriteLine("Initalising GL_EXT_separate_specular_color interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_LIGHT_MODEL_COLOR_CONTROL_EXT = 0x81F8;
        public static UInt32 GL_SINGLE_COLOR_EXT = 0x81F9;
        public static UInt32 GL_SEPARATE_SPECULAR_COLOR_EXT = 0x81FA;
        #endregion

        #region Commands
        #endregion
    }
}
