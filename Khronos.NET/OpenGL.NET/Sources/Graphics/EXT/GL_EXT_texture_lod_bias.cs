using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_lod_bias
    {
        #region Interop
        static GL_EXT_texture_lod_bias()
        {
            Console.WriteLine("Initalising GL_EXT_texture_lod_bias interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_TEXTURE_LOD_BIAS_EXT = 0x84FD;
        public static UInt32 GL_TEXTURE_FILTER_CONTROL_EXT = 0x8500;
        public static UInt32 GL_TEXTURE_LOD_BIAS_EXT = 0x8501;
        #endregion

        #region Commands
        #endregion
    }
}
