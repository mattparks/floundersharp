using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_fog_coord
    {
        #region Interop
        static GL_EXT_fog_coord()
        {
            Console.WriteLine("Initalising GL_EXT_fog_coord interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFogCoordfEXT();
            loadFogCoordfvEXT();
            loadFogCoorddEXT();
            loadFogCoorddvEXT();
            loadFogCoordPointerEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FOG_COORDINATE_SOURCE_EXT = 0x8450;
        public static UInt32 GL_FOG_COORDINATE_EXT = 0x8451;
        public static UInt32 GL_FRAGMENT_DEPTH_EXT = 0x8452;
        public static UInt32 GL_CURRENT_FOG_COORDINATE_EXT = 0x8453;
        public static UInt32 GL_FOG_COORDINATE_ARRAY_TYPE_EXT = 0x8454;
        public static UInt32 GL_FOG_COORDINATE_ARRAY_STRIDE_EXT = 0x8455;
        public static UInt32 GL_FOG_COORDINATE_ARRAY_POINTER_EXT = 0x8456;
        public static UInt32 GL_FOG_COORDINATE_ARRAY_EXT = 0x8457;
        #endregion

        #region Commands
        internal delegate void glFogCoordfEXTFunc(GLfloat @coord);
        internal static glFogCoordfEXTFunc glFogCoordfEXTPtr;
        internal static void loadFogCoordfEXT()
        {
            try
            {
                glFogCoordfEXTPtr = (glFogCoordfEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoordfEXT"), typeof(glFogCoordfEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoordfEXT'.");
            }
        }
        public static void glFogCoordfEXT(GLfloat @coord) => glFogCoordfEXTPtr(@coord);

        internal delegate void glFogCoordfvEXTFunc(const GLfloat * @coord);
        internal static glFogCoordfvEXTFunc glFogCoordfvEXTPtr;
        internal static void loadFogCoordfvEXT()
        {
            try
            {
                glFogCoordfvEXTPtr = (glFogCoordfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoordfvEXT"), typeof(glFogCoordfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoordfvEXT'.");
            }
        }
        public static void glFogCoordfvEXT(const GLfloat * @coord) => glFogCoordfvEXTPtr(@coord);

        internal delegate void glFogCoorddEXTFunc(GLdouble @coord);
        internal static glFogCoorddEXTFunc glFogCoorddEXTPtr;
        internal static void loadFogCoorddEXT()
        {
            try
            {
                glFogCoorddEXTPtr = (glFogCoorddEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoorddEXT"), typeof(glFogCoorddEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoorddEXT'.");
            }
        }
        public static void glFogCoorddEXT(GLdouble @coord) => glFogCoorddEXTPtr(@coord);

        internal delegate void glFogCoorddvEXTFunc(const GLdouble * @coord);
        internal static glFogCoorddvEXTFunc glFogCoorddvEXTPtr;
        internal static void loadFogCoorddvEXT()
        {
            try
            {
                glFogCoorddvEXTPtr = (glFogCoorddvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoorddvEXT"), typeof(glFogCoorddvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoorddvEXT'.");
            }
        }
        public static void glFogCoorddvEXT(const GLdouble * @coord) => glFogCoorddvEXTPtr(@coord);

        internal delegate void glFogCoordPointerEXTFunc(GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glFogCoordPointerEXTFunc glFogCoordPointerEXTPtr;
        internal static void loadFogCoordPointerEXT()
        {
            try
            {
                glFogCoordPointerEXTPtr = (glFogCoordPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoordPointerEXT"), typeof(glFogCoordPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoordPointerEXT'.");
            }
        }
        public static void glFogCoordPointerEXT(GLenum @type, GLsizei @stride, const void * @pointer) => glFogCoordPointerEXTPtr(@type, @stride, @pointer);
        #endregion
    }
}
