using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_debug_marker
    {
        #region Interop
        static GL_EXT_debug_marker()
        {
            Console.WriteLine("Initalising GL_EXT_debug_marker interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadInsertEventMarkerEXT();
            loadPushGroupMarkerEXT();
            loadPopGroupMarkerEXT();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glInsertEventMarkerEXTFunc(GLsizei @length, const GLchar * @marker);
        internal static glInsertEventMarkerEXTFunc glInsertEventMarkerEXTPtr;
        internal static void loadInsertEventMarkerEXT()
        {
            try
            {
                glInsertEventMarkerEXTPtr = (glInsertEventMarkerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInsertEventMarkerEXT"), typeof(glInsertEventMarkerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInsertEventMarkerEXT'.");
            }
        }
        public static void glInsertEventMarkerEXT(GLsizei @length, const GLchar * @marker) => glInsertEventMarkerEXTPtr(@length, @marker);

        internal delegate void glPushGroupMarkerEXTFunc(GLsizei @length, const GLchar * @marker);
        internal static glPushGroupMarkerEXTFunc glPushGroupMarkerEXTPtr;
        internal static void loadPushGroupMarkerEXT()
        {
            try
            {
                glPushGroupMarkerEXTPtr = (glPushGroupMarkerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPushGroupMarkerEXT"), typeof(glPushGroupMarkerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPushGroupMarkerEXT'.");
            }
        }
        public static void glPushGroupMarkerEXT(GLsizei @length, const GLchar * @marker) => glPushGroupMarkerEXTPtr(@length, @marker);

        internal delegate void glPopGroupMarkerEXTFunc();
        internal static glPopGroupMarkerEXTFunc glPopGroupMarkerEXTPtr;
        internal static void loadPopGroupMarkerEXT()
        {
            try
            {
                glPopGroupMarkerEXTPtr = (glPopGroupMarkerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPopGroupMarkerEXT"), typeof(glPopGroupMarkerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPopGroupMarkerEXT'.");
            }
        }
        public static void glPopGroupMarkerEXT() => glPopGroupMarkerEXTPtr();
        #endregion
    }
}
