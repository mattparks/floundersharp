using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_index_func
    {
        #region Interop
        static GL_EXT_index_func()
        {
            Console.WriteLine("Initalising GL_EXT_index_func interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadIndexFuncEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_INDEX_TEST_EXT = 0x81B5;
        public static UInt32 GL_INDEX_TEST_FUNC_EXT = 0x81B6;
        public static UInt32 GL_INDEX_TEST_REF_EXT = 0x81B7;
        #endregion

        #region Commands
        internal delegate void glIndexFuncEXTFunc(GLenum @func, GLclampf @ref);
        internal static glIndexFuncEXTFunc glIndexFuncEXTPtr;
        internal static void loadIndexFuncEXT()
        {
            try
            {
                glIndexFuncEXTPtr = (glIndexFuncEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexFuncEXT"), typeof(glIndexFuncEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexFuncEXT'.");
            }
        }
        public static void glIndexFuncEXT(GLenum @func, GLclampf @ref) => glIndexFuncEXTPtr(@func, @ref);
        #endregion
    }
}
