using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_transform_feedback
    {
        #region Interop
        static GL_EXT_transform_feedback()
        {
            Console.WriteLine("Initalising GL_EXT_transform_feedback interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBeginTransformFeedbackEXT();
            loadEndTransformFeedbackEXT();
            loadBindBufferRangeEXT();
            loadBindBufferOffsetEXT();
            loadBindBufferBaseEXT();
            loadTransformFeedbackVaryingsEXT();
            loadGetTransformFeedbackVaryingEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_EXT = 0x8C8E;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_START_EXT = 0x8C84;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_SIZE_EXT = 0x8C85;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_BINDING_EXT = 0x8C8F;
        public static UInt32 GL_INTERLEAVED_ATTRIBS_EXT = 0x8C8C;
        public static UInt32 GL_SEPARATE_ATTRIBS_EXT = 0x8C8D;
        public static UInt32 GL_PRIMITIVES_GENERATED_EXT = 0x8C87;
        public static UInt32 GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN_EXT = 0x8C88;
        public static UInt32 GL_RASTERIZER_DISCARD_EXT = 0x8C89;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS_EXT = 0x8C8A;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS_EXT = 0x8C8B;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS_EXT = 0x8C80;
        public static UInt32 GL_TRANSFORM_FEEDBACK_VARYINGS_EXT = 0x8C83;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_MODE_EXT = 0x8C7F;
        public static UInt32 GL_TRANSFORM_FEEDBACK_VARYING_MAX_LENGTH_EXT = 0x8C76;
        #endregion

        #region Commands
        internal delegate void glBeginTransformFeedbackEXTFunc(GLenum @primitiveMode);
        internal static glBeginTransformFeedbackEXTFunc glBeginTransformFeedbackEXTPtr;
        internal static void loadBeginTransformFeedbackEXT()
        {
            try
            {
                glBeginTransformFeedbackEXTPtr = (glBeginTransformFeedbackEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginTransformFeedbackEXT"), typeof(glBeginTransformFeedbackEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginTransformFeedbackEXT'.");
            }
        }
        public static void glBeginTransformFeedbackEXT(GLenum @primitiveMode) => glBeginTransformFeedbackEXTPtr(@primitiveMode);

        internal delegate void glEndTransformFeedbackEXTFunc();
        internal static glEndTransformFeedbackEXTFunc glEndTransformFeedbackEXTPtr;
        internal static void loadEndTransformFeedbackEXT()
        {
            try
            {
                glEndTransformFeedbackEXTPtr = (glEndTransformFeedbackEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndTransformFeedbackEXT"), typeof(glEndTransformFeedbackEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndTransformFeedbackEXT'.");
            }
        }
        public static void glEndTransformFeedbackEXT() => glEndTransformFeedbackEXTPtr();

        internal delegate void glBindBufferRangeEXTFunc(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glBindBufferRangeEXTFunc glBindBufferRangeEXTPtr;
        internal static void loadBindBufferRangeEXT()
        {
            try
            {
                glBindBufferRangeEXTPtr = (glBindBufferRangeEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferRangeEXT"), typeof(glBindBufferRangeEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferRangeEXT'.");
            }
        }
        public static void glBindBufferRangeEXT(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glBindBufferRangeEXTPtr(@target, @index, @buffer, @offset, @size);

        internal delegate void glBindBufferOffsetEXTFunc(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset);
        internal static glBindBufferOffsetEXTFunc glBindBufferOffsetEXTPtr;
        internal static void loadBindBufferOffsetEXT()
        {
            try
            {
                glBindBufferOffsetEXTPtr = (glBindBufferOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferOffsetEXT"), typeof(glBindBufferOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferOffsetEXT'.");
            }
        }
        public static void glBindBufferOffsetEXT(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset) => glBindBufferOffsetEXTPtr(@target, @index, @buffer, @offset);

        internal delegate void glBindBufferBaseEXTFunc(GLenum @target, GLuint @index, GLuint @buffer);
        internal static glBindBufferBaseEXTFunc glBindBufferBaseEXTPtr;
        internal static void loadBindBufferBaseEXT()
        {
            try
            {
                glBindBufferBaseEXTPtr = (glBindBufferBaseEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferBaseEXT"), typeof(glBindBufferBaseEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferBaseEXT'.");
            }
        }
        public static void glBindBufferBaseEXT(GLenum @target, GLuint @index, GLuint @buffer) => glBindBufferBaseEXTPtr(@target, @index, @buffer);

        internal delegate void glTransformFeedbackVaryingsEXTFunc(GLuint @program, GLsizei @count, const GLchar *const* @varyings, GLenum @bufferMode);
        internal static glTransformFeedbackVaryingsEXTFunc glTransformFeedbackVaryingsEXTPtr;
        internal static void loadTransformFeedbackVaryingsEXT()
        {
            try
            {
                glTransformFeedbackVaryingsEXTPtr = (glTransformFeedbackVaryingsEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTransformFeedbackVaryingsEXT"), typeof(glTransformFeedbackVaryingsEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTransformFeedbackVaryingsEXT'.");
            }
        }
        public static void glTransformFeedbackVaryingsEXT(GLuint @program, GLsizei @count, const GLchar *const* @varyings, GLenum @bufferMode) => glTransformFeedbackVaryingsEXTPtr(@program, @count, @varyings, @bufferMode);

        internal delegate void glGetTransformFeedbackVaryingEXTFunc(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLsizei * @size, GLenum * @type, GLchar * @name);
        internal static glGetTransformFeedbackVaryingEXTFunc glGetTransformFeedbackVaryingEXTPtr;
        internal static void loadGetTransformFeedbackVaryingEXT()
        {
            try
            {
                glGetTransformFeedbackVaryingEXTPtr = (glGetTransformFeedbackVaryingEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTransformFeedbackVaryingEXT"), typeof(glGetTransformFeedbackVaryingEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTransformFeedbackVaryingEXT'.");
            }
        }
        public static void glGetTransformFeedbackVaryingEXT(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLsizei * @size, GLenum * @type, GLchar * @name) => glGetTransformFeedbackVaryingEXTPtr(@program, @index, @bufSize, @length, @size, @type, @name);
        #endregion
    }
}
