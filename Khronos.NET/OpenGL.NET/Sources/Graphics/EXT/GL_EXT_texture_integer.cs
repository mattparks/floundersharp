using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_integer
    {
        #region Interop
        static GL_EXT_texture_integer()
        {
            Console.WriteLine("Initalising GL_EXT_texture_integer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexParameterIivEXT();
            loadTexParameterIuivEXT();
            loadGetTexParameterIivEXT();
            loadGetTexParameterIuivEXT();
            loadClearColorIiEXT();
            loadClearColorIuiEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RGBA32UI_EXT = 0x8D70;
        public static UInt32 GL_RGB32UI_EXT = 0x8D71;
        public static UInt32 GL_ALPHA32UI_EXT = 0x8D72;
        public static UInt32 GL_INTENSITY32UI_EXT = 0x8D73;
        public static UInt32 GL_LUMINANCE32UI_EXT = 0x8D74;
        public static UInt32 GL_LUMINANCE_ALPHA32UI_EXT = 0x8D75;
        public static UInt32 GL_RGBA16UI_EXT = 0x8D76;
        public static UInt32 GL_RGB16UI_EXT = 0x8D77;
        public static UInt32 GL_ALPHA16UI_EXT = 0x8D78;
        public static UInt32 GL_INTENSITY16UI_EXT = 0x8D79;
        public static UInt32 GL_LUMINANCE16UI_EXT = 0x8D7A;
        public static UInt32 GL_LUMINANCE_ALPHA16UI_EXT = 0x8D7B;
        public static UInt32 GL_RGBA8UI_EXT = 0x8D7C;
        public static UInt32 GL_RGB8UI_EXT = 0x8D7D;
        public static UInt32 GL_ALPHA8UI_EXT = 0x8D7E;
        public static UInt32 GL_INTENSITY8UI_EXT = 0x8D7F;
        public static UInt32 GL_LUMINANCE8UI_EXT = 0x8D80;
        public static UInt32 GL_LUMINANCE_ALPHA8UI_EXT = 0x8D81;
        public static UInt32 GL_RGBA32I_EXT = 0x8D82;
        public static UInt32 GL_RGB32I_EXT = 0x8D83;
        public static UInt32 GL_ALPHA32I_EXT = 0x8D84;
        public static UInt32 GL_INTENSITY32I_EXT = 0x8D85;
        public static UInt32 GL_LUMINANCE32I_EXT = 0x8D86;
        public static UInt32 GL_LUMINANCE_ALPHA32I_EXT = 0x8D87;
        public static UInt32 GL_RGBA16I_EXT = 0x8D88;
        public static UInt32 GL_RGB16I_EXT = 0x8D89;
        public static UInt32 GL_ALPHA16I_EXT = 0x8D8A;
        public static UInt32 GL_INTENSITY16I_EXT = 0x8D8B;
        public static UInt32 GL_LUMINANCE16I_EXT = 0x8D8C;
        public static UInt32 GL_LUMINANCE_ALPHA16I_EXT = 0x8D8D;
        public static UInt32 GL_RGBA8I_EXT = 0x8D8E;
        public static UInt32 GL_RGB8I_EXT = 0x8D8F;
        public static UInt32 GL_ALPHA8I_EXT = 0x8D90;
        public static UInt32 GL_INTENSITY8I_EXT = 0x8D91;
        public static UInt32 GL_LUMINANCE8I_EXT = 0x8D92;
        public static UInt32 GL_LUMINANCE_ALPHA8I_EXT = 0x8D93;
        public static UInt32 GL_RED_INTEGER_EXT = 0x8D94;
        public static UInt32 GL_GREEN_INTEGER_EXT = 0x8D95;
        public static UInt32 GL_BLUE_INTEGER_EXT = 0x8D96;
        public static UInt32 GL_ALPHA_INTEGER_EXT = 0x8D97;
        public static UInt32 GL_RGB_INTEGER_EXT = 0x8D98;
        public static UInt32 GL_RGBA_INTEGER_EXT = 0x8D99;
        public static UInt32 GL_BGR_INTEGER_EXT = 0x8D9A;
        public static UInt32 GL_BGRA_INTEGER_EXT = 0x8D9B;
        public static UInt32 GL_LUMINANCE_INTEGER_EXT = 0x8D9C;
        public static UInt32 GL_LUMINANCE_ALPHA_INTEGER_EXT = 0x8D9D;
        public static UInt32 GL_RGBA_INTEGER_MODE_EXT = 0x8D9E;
        #endregion

        #region Commands
        internal delegate void glTexParameterIivEXTFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glTexParameterIivEXTFunc glTexParameterIivEXTPtr;
        internal static void loadTexParameterIivEXT()
        {
            try
            {
                glTexParameterIivEXTPtr = (glTexParameterIivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterIivEXT"), typeof(glTexParameterIivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterIivEXT'.");
            }
        }
        public static void glTexParameterIivEXT(GLenum @target, GLenum @pname, const GLint * @params) => glTexParameterIivEXTPtr(@target, @pname, @params);

        internal delegate void glTexParameterIuivEXTFunc(GLenum @target, GLenum @pname, const GLuint * @params);
        internal static glTexParameterIuivEXTFunc glTexParameterIuivEXTPtr;
        internal static void loadTexParameterIuivEXT()
        {
            try
            {
                glTexParameterIuivEXTPtr = (glTexParameterIuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterIuivEXT"), typeof(glTexParameterIuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterIuivEXT'.");
            }
        }
        public static void glTexParameterIuivEXT(GLenum @target, GLenum @pname, const GLuint * @params) => glTexParameterIuivEXTPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterIivEXTFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetTexParameterIivEXTFunc glGetTexParameterIivEXTPtr;
        internal static void loadGetTexParameterIivEXT()
        {
            try
            {
                glGetTexParameterIivEXTPtr = (glGetTexParameterIivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterIivEXT"), typeof(glGetTexParameterIivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterIivEXT'.");
            }
        }
        public static void glGetTexParameterIivEXT(GLenum @target, GLenum @pname, GLint * @params) => glGetTexParameterIivEXTPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterIuivEXTFunc(GLenum @target, GLenum @pname, GLuint * @params);
        internal static glGetTexParameterIuivEXTFunc glGetTexParameterIuivEXTPtr;
        internal static void loadGetTexParameterIuivEXT()
        {
            try
            {
                glGetTexParameterIuivEXTPtr = (glGetTexParameterIuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterIuivEXT"), typeof(glGetTexParameterIuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterIuivEXT'.");
            }
        }
        public static void glGetTexParameterIuivEXT(GLenum @target, GLenum @pname, GLuint * @params) => glGetTexParameterIuivEXTPtr(@target, @pname, @params);

        internal delegate void glClearColorIiEXTFunc(GLint @red, GLint @green, GLint @blue, GLint @alpha);
        internal static glClearColorIiEXTFunc glClearColorIiEXTPtr;
        internal static void loadClearColorIiEXT()
        {
            try
            {
                glClearColorIiEXTPtr = (glClearColorIiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearColorIiEXT"), typeof(glClearColorIiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearColorIiEXT'.");
            }
        }
        public static void glClearColorIiEXT(GLint @red, GLint @green, GLint @blue, GLint @alpha) => glClearColorIiEXTPtr(@red, @green, @blue, @alpha);

        internal delegate void glClearColorIuiEXTFunc(GLuint @red, GLuint @green, GLuint @blue, GLuint @alpha);
        internal static glClearColorIuiEXTFunc glClearColorIuiEXTPtr;
        internal static void loadClearColorIuiEXT()
        {
            try
            {
                glClearColorIuiEXTPtr = (glClearColorIuiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearColorIuiEXT"), typeof(glClearColorIuiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearColorIuiEXT'.");
            }
        }
        public static void glClearColorIuiEXT(GLuint @red, GLuint @green, GLuint @blue, GLuint @alpha) => glClearColorIuiEXTPtr(@red, @green, @blue, @alpha);
        #endregion
    }
}
