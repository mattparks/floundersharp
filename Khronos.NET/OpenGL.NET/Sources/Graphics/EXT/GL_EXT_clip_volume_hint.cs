using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_clip_volume_hint
    {
        #region Interop
        static GL_EXT_clip_volume_hint()
        {
            Console.WriteLine("Initalising GL_EXT_clip_volume_hint interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_CLIP_VOLUME_CLIPPING_HINT_EXT = 0x80F0;
        #endregion

        #region Commands
        #endregion
    }
}
