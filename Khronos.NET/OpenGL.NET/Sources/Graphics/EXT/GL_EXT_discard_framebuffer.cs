using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_discard_framebuffer
    {
        #region Interop
        static GL_EXT_discard_framebuffer()
        {
            Console.WriteLine("Initalising GL_EXT_discard_framebuffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDiscardFramebufferEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COLOR_EXT = 0x1800;
        public static UInt32 GL_DEPTH_EXT = 0x1801;
        public static UInt32 GL_STENCIL_EXT = 0x1802;
        #endregion

        #region Commands
        internal delegate void glDiscardFramebufferEXTFunc(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments);
        internal static glDiscardFramebufferEXTFunc glDiscardFramebufferEXTPtr;
        internal static void loadDiscardFramebufferEXT()
        {
            try
            {
                glDiscardFramebufferEXTPtr = (glDiscardFramebufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDiscardFramebufferEXT"), typeof(glDiscardFramebufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDiscardFramebufferEXT'.");
            }
        }
        public static void glDiscardFramebufferEXT(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments) => glDiscardFramebufferEXTPtr(@target, @numAttachments, @attachments);
        #endregion
    }
}
