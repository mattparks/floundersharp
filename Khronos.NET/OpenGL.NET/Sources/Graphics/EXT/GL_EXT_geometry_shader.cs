using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_geometry_shader
    {
        #region Interop
        static GL_EXT_geometry_shader()
        {
            Console.WriteLine("Initalising GL_EXT_geometry_shader interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFramebufferTextureEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_GEOMETRY_SHADER_EXT = 0x8DD9;
        public static UInt32 GL_GEOMETRY_SHADER_BIT_EXT = 0x00000004;
        public static UInt32 GL_GEOMETRY_LINKED_VERTICES_OUT_EXT = 0x8916;
        public static UInt32 GL_GEOMETRY_LINKED_INPUT_TYPE_EXT = 0x8917;
        public static UInt32 GL_GEOMETRY_LINKED_OUTPUT_TYPE_EXT = 0x8918;
        public static UInt32 GL_GEOMETRY_SHADER_INVOCATIONS_EXT = 0x887F;
        public static UInt32 GL_LAYER_PROVOKING_VERTEX_EXT = 0x825E;
        public static UInt32 GL_LINES_ADJACENCY_EXT = 0x000A;
        public static UInt32 GL_LINE_STRIP_ADJACENCY_EXT = 0x000B;
        public static UInt32 GL_TRIANGLES_ADJACENCY_EXT = 0x000C;
        public static UInt32 GL_TRIANGLE_STRIP_ADJACENCY_EXT = 0x000D;
        public static UInt32 GL_MAX_GEOMETRY_UNIFORM_COMPONENTS_EXT = 0x8DDF;
        public static UInt32 GL_MAX_GEOMETRY_UNIFORM_BLOCKS_EXT = 0x8A2C;
        public static UInt32 GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS_EXT = 0x8A32;
        public static UInt32 GL_MAX_GEOMETRY_INPUT_COMPONENTS_EXT = 0x9123;
        public static UInt32 GL_MAX_GEOMETRY_OUTPUT_COMPONENTS_EXT = 0x9124;
        public static UInt32 GL_MAX_GEOMETRY_OUTPUT_VERTICES_EXT = 0x8DE0;
        public static UInt32 GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS_EXT = 0x8DE1;
        public static UInt32 GL_MAX_GEOMETRY_SHADER_INVOCATIONS_EXT = 0x8E5A;
        public static UInt32 GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS_EXT = 0x8C29;
        public static UInt32 GL_MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS_EXT = 0x92CF;
        public static UInt32 GL_MAX_GEOMETRY_ATOMIC_COUNTERS_EXT = 0x92D5;
        public static UInt32 GL_MAX_GEOMETRY_IMAGE_UNIFORMS_EXT = 0x90CD;
        public static UInt32 GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS_EXT = 0x90D7;
        public static UInt32 GL_FIRST_VERTEX_CONVENTION_EXT = 0x8E4D;
        public static UInt32 GL_LAST_VERTEX_CONVENTION_EXT = 0x8E4E;
        public static UInt32 GL_UNDEFINED_VERTEX_EXT = 0x8260;
        public static UInt32 GL_PRIMITIVES_GENERATED_EXT = 0x8C87;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_LAYERS_EXT = 0x9312;
        public static UInt32 GL_MAX_FRAMEBUFFER_LAYERS_EXT = 0x9317;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS_EXT = 0x8DA8;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_LAYERED_EXT = 0x8DA7;
        public static UInt32 GL_REFERENCED_BY_GEOMETRY_SHADER_EXT = 0x9309;
        #endregion

        #region Commands
        internal delegate void glFramebufferTextureEXTFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level);
        internal static glFramebufferTextureEXTFunc glFramebufferTextureEXTPtr;
        internal static void loadFramebufferTextureEXT()
        {
            try
            {
                glFramebufferTextureEXTPtr = (glFramebufferTextureEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureEXT"), typeof(glFramebufferTextureEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureEXT'.");
            }
        }
        public static void glFramebufferTextureEXT(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level) => glFramebufferTextureEXTPtr(@target, @attachment, @texture, @level);
        #endregion
    }
}
