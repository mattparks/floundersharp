using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_point_parameters
    {
        #region Interop
        static GL_EXT_point_parameters()
        {
            Console.WriteLine("Initalising GL_EXT_point_parameters interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPointParameterfEXT();
            loadPointParameterfvEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_POINT_SIZE_MIN_EXT = 0x8126;
        public static UInt32 GL_POINT_SIZE_MAX_EXT = 0x8127;
        public static UInt32 GL_POINT_FADE_THRESHOLD_SIZE_EXT = 0x8128;
        public static UInt32 GL_DISTANCE_ATTENUATION_EXT = 0x8129;
        #endregion

        #region Commands
        internal delegate void glPointParameterfEXTFunc(GLenum @pname, GLfloat @param);
        internal static glPointParameterfEXTFunc glPointParameterfEXTPtr;
        internal static void loadPointParameterfEXT()
        {
            try
            {
                glPointParameterfEXTPtr = (glPointParameterfEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterfEXT"), typeof(glPointParameterfEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterfEXT'.");
            }
        }
        public static void glPointParameterfEXT(GLenum @pname, GLfloat @param) => glPointParameterfEXTPtr(@pname, @param);

        internal delegate void glPointParameterfvEXTFunc(GLenum @pname, const GLfloat * @params);
        internal static glPointParameterfvEXTFunc glPointParameterfvEXTPtr;
        internal static void loadPointParameterfvEXT()
        {
            try
            {
                glPointParameterfvEXTPtr = (glPointParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterfvEXT"), typeof(glPointParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterfvEXT'.");
            }
        }
        public static void glPointParameterfvEXT(GLenum @pname, const GLfloat * @params) => glPointParameterfvEXTPtr(@pname, @params);
        #endregion
    }
}
