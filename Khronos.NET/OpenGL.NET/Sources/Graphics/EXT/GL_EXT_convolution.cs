using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_convolution
    {
        #region Interop
        static GL_EXT_convolution()
        {
            Console.WriteLine("Initalising GL_EXT_convolution interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadConvolutionFilter1DEXT();
            loadConvolutionFilter2DEXT();
            loadConvolutionParameterfEXT();
            loadConvolutionParameterfvEXT();
            loadConvolutionParameteriEXT();
            loadConvolutionParameterivEXT();
            loadCopyConvolutionFilter1DEXT();
            loadCopyConvolutionFilter2DEXT();
            loadGetConvolutionFilterEXT();
            loadGetConvolutionParameterfvEXT();
            loadGetConvolutionParameterivEXT();
            loadGetSeparableFilterEXT();
            loadSeparableFilter2DEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_CONVOLUTION_1D_EXT = 0x8010;
        public static UInt32 GL_CONVOLUTION_2D_EXT = 0x8011;
        public static UInt32 GL_SEPARABLE_2D_EXT = 0x8012;
        public static UInt32 GL_CONVOLUTION_BORDER_MODE_EXT = 0x8013;
        public static UInt32 GL_CONVOLUTION_FILTER_SCALE_EXT = 0x8014;
        public static UInt32 GL_CONVOLUTION_FILTER_BIAS_EXT = 0x8015;
        public static UInt32 GL_REDUCE_EXT = 0x8016;
        public static UInt32 GL_CONVOLUTION_FORMAT_EXT = 0x8017;
        public static UInt32 GL_CONVOLUTION_WIDTH_EXT = 0x8018;
        public static UInt32 GL_CONVOLUTION_HEIGHT_EXT = 0x8019;
        public static UInt32 GL_MAX_CONVOLUTION_WIDTH_EXT = 0x801A;
        public static UInt32 GL_MAX_CONVOLUTION_HEIGHT_EXT = 0x801B;
        public static UInt32 GL_POST_CONVOLUTION_RED_SCALE_EXT = 0x801C;
        public static UInt32 GL_POST_CONVOLUTION_GREEN_SCALE_EXT = 0x801D;
        public static UInt32 GL_POST_CONVOLUTION_BLUE_SCALE_EXT = 0x801E;
        public static UInt32 GL_POST_CONVOLUTION_ALPHA_SCALE_EXT = 0x801F;
        public static UInt32 GL_POST_CONVOLUTION_RED_BIAS_EXT = 0x8020;
        public static UInt32 GL_POST_CONVOLUTION_GREEN_BIAS_EXT = 0x8021;
        public static UInt32 GL_POST_CONVOLUTION_BLUE_BIAS_EXT = 0x8022;
        public static UInt32 GL_POST_CONVOLUTION_ALPHA_BIAS_EXT = 0x8023;
        #endregion

        #region Commands
        internal delegate void glConvolutionFilter1DEXTFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLenum @format, GLenum @type, const void * @image);
        internal static glConvolutionFilter1DEXTFunc glConvolutionFilter1DEXTPtr;
        internal static void loadConvolutionFilter1DEXT()
        {
            try
            {
                glConvolutionFilter1DEXTPtr = (glConvolutionFilter1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionFilter1DEXT"), typeof(glConvolutionFilter1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionFilter1DEXT'.");
            }
        }
        public static void glConvolutionFilter1DEXT(GLenum @target, GLenum @internalformat, GLsizei @width, GLenum @format, GLenum @type, const void * @image) => glConvolutionFilter1DEXTPtr(@target, @internalformat, @width, @format, @type, @image);

        internal delegate void glConvolutionFilter2DEXTFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @image);
        internal static glConvolutionFilter2DEXTFunc glConvolutionFilter2DEXTPtr;
        internal static void loadConvolutionFilter2DEXT()
        {
            try
            {
                glConvolutionFilter2DEXTPtr = (glConvolutionFilter2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionFilter2DEXT"), typeof(glConvolutionFilter2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionFilter2DEXT'.");
            }
        }
        public static void glConvolutionFilter2DEXT(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @image) => glConvolutionFilter2DEXTPtr(@target, @internalformat, @width, @height, @format, @type, @image);

        internal delegate void glConvolutionParameterfEXTFunc(GLenum @target, GLenum @pname, GLfloat @params);
        internal static glConvolutionParameterfEXTFunc glConvolutionParameterfEXTPtr;
        internal static void loadConvolutionParameterfEXT()
        {
            try
            {
                glConvolutionParameterfEXTPtr = (glConvolutionParameterfEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionParameterfEXT"), typeof(glConvolutionParameterfEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionParameterfEXT'.");
            }
        }
        public static void glConvolutionParameterfEXT(GLenum @target, GLenum @pname, GLfloat @params) => glConvolutionParameterfEXTPtr(@target, @pname, @params);

        internal delegate void glConvolutionParameterfvEXTFunc(GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glConvolutionParameterfvEXTFunc glConvolutionParameterfvEXTPtr;
        internal static void loadConvolutionParameterfvEXT()
        {
            try
            {
                glConvolutionParameterfvEXTPtr = (glConvolutionParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionParameterfvEXT"), typeof(glConvolutionParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionParameterfvEXT'.");
            }
        }
        public static void glConvolutionParameterfvEXT(GLenum @target, GLenum @pname, const GLfloat * @params) => glConvolutionParameterfvEXTPtr(@target, @pname, @params);

        internal delegate void glConvolutionParameteriEXTFunc(GLenum @target, GLenum @pname, GLint @params);
        internal static glConvolutionParameteriEXTFunc glConvolutionParameteriEXTPtr;
        internal static void loadConvolutionParameteriEXT()
        {
            try
            {
                glConvolutionParameteriEXTPtr = (glConvolutionParameteriEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionParameteriEXT"), typeof(glConvolutionParameteriEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionParameteriEXT'.");
            }
        }
        public static void glConvolutionParameteriEXT(GLenum @target, GLenum @pname, GLint @params) => glConvolutionParameteriEXTPtr(@target, @pname, @params);

        internal delegate void glConvolutionParameterivEXTFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glConvolutionParameterivEXTFunc glConvolutionParameterivEXTPtr;
        internal static void loadConvolutionParameterivEXT()
        {
            try
            {
                glConvolutionParameterivEXTPtr = (glConvolutionParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionParameterivEXT"), typeof(glConvolutionParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionParameterivEXT'.");
            }
        }
        public static void glConvolutionParameterivEXT(GLenum @target, GLenum @pname, const GLint * @params) => glConvolutionParameterivEXTPtr(@target, @pname, @params);

        internal delegate void glCopyConvolutionFilter1DEXTFunc(GLenum @target, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width);
        internal static glCopyConvolutionFilter1DEXTFunc glCopyConvolutionFilter1DEXTPtr;
        internal static void loadCopyConvolutionFilter1DEXT()
        {
            try
            {
                glCopyConvolutionFilter1DEXTPtr = (glCopyConvolutionFilter1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyConvolutionFilter1DEXT"), typeof(glCopyConvolutionFilter1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyConvolutionFilter1DEXT'.");
            }
        }
        public static void glCopyConvolutionFilter1DEXT(GLenum @target, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width) => glCopyConvolutionFilter1DEXTPtr(@target, @internalformat, @x, @y, @width);

        internal delegate void glCopyConvolutionFilter2DEXTFunc(GLenum @target, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyConvolutionFilter2DEXTFunc glCopyConvolutionFilter2DEXTPtr;
        internal static void loadCopyConvolutionFilter2DEXT()
        {
            try
            {
                glCopyConvolutionFilter2DEXTPtr = (glCopyConvolutionFilter2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyConvolutionFilter2DEXT"), typeof(glCopyConvolutionFilter2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyConvolutionFilter2DEXT'.");
            }
        }
        public static void glCopyConvolutionFilter2DEXT(GLenum @target, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyConvolutionFilter2DEXTPtr(@target, @internalformat, @x, @y, @width, @height);

        internal delegate void glGetConvolutionFilterEXTFunc(GLenum @target, GLenum @format, GLenum @type, void * @image);
        internal static glGetConvolutionFilterEXTFunc glGetConvolutionFilterEXTPtr;
        internal static void loadGetConvolutionFilterEXT()
        {
            try
            {
                glGetConvolutionFilterEXTPtr = (glGetConvolutionFilterEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetConvolutionFilterEXT"), typeof(glGetConvolutionFilterEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetConvolutionFilterEXT'.");
            }
        }
        public static void glGetConvolutionFilterEXT(GLenum @target, GLenum @format, GLenum @type, void * @image) => glGetConvolutionFilterEXTPtr(@target, @format, @type, @image);

        internal delegate void glGetConvolutionParameterfvEXTFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetConvolutionParameterfvEXTFunc glGetConvolutionParameterfvEXTPtr;
        internal static void loadGetConvolutionParameterfvEXT()
        {
            try
            {
                glGetConvolutionParameterfvEXTPtr = (glGetConvolutionParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetConvolutionParameterfvEXT"), typeof(glGetConvolutionParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetConvolutionParameterfvEXT'.");
            }
        }
        public static void glGetConvolutionParameterfvEXT(GLenum @target, GLenum @pname, GLfloat * @params) => glGetConvolutionParameterfvEXTPtr(@target, @pname, @params);

        internal delegate void glGetConvolutionParameterivEXTFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetConvolutionParameterivEXTFunc glGetConvolutionParameterivEXTPtr;
        internal static void loadGetConvolutionParameterivEXT()
        {
            try
            {
                glGetConvolutionParameterivEXTPtr = (glGetConvolutionParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetConvolutionParameterivEXT"), typeof(glGetConvolutionParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetConvolutionParameterivEXT'.");
            }
        }
        public static void glGetConvolutionParameterivEXT(GLenum @target, GLenum @pname, GLint * @params) => glGetConvolutionParameterivEXTPtr(@target, @pname, @params);

        internal delegate void glGetSeparableFilterEXTFunc(GLenum @target, GLenum @format, GLenum @type, void * @row, void * @column, void * @span);
        internal static glGetSeparableFilterEXTFunc glGetSeparableFilterEXTPtr;
        internal static void loadGetSeparableFilterEXT()
        {
            try
            {
                glGetSeparableFilterEXTPtr = (glGetSeparableFilterEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSeparableFilterEXT"), typeof(glGetSeparableFilterEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSeparableFilterEXT'.");
            }
        }
        public static void glGetSeparableFilterEXT(GLenum @target, GLenum @format, GLenum @type, void * @row, void * @column, void * @span) => glGetSeparableFilterEXTPtr(@target, @format, @type, @row, @column, @span);

        internal delegate void glSeparableFilter2DEXTFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @row, const void * @column);
        internal static glSeparableFilter2DEXTFunc glSeparableFilter2DEXTPtr;
        internal static void loadSeparableFilter2DEXT()
        {
            try
            {
                glSeparableFilter2DEXTPtr = (glSeparableFilter2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSeparableFilter2DEXT"), typeof(glSeparableFilter2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSeparableFilter2DEXT'.");
            }
        }
        public static void glSeparableFilter2DEXT(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @row, const void * @column) => glSeparableFilter2DEXTPtr(@target, @internalformat, @width, @height, @format, @type, @row, @column);
        #endregion
    }
}
