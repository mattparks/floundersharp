using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_primitive_bounding_box
    {
        #region Interop
        static GL_EXT_primitive_bounding_box()
        {
            Console.WriteLine("Initalising GL_EXT_primitive_bounding_box interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPrimitiveBoundingBoxEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PRIMITIVE_BOUNDING_BOX_EXT = 0x92BE;
        #endregion

        #region Commands
        internal delegate void glPrimitiveBoundingBoxEXTFunc(GLfloat @minX, GLfloat @minY, GLfloat @minZ, GLfloat @minW, GLfloat @maxX, GLfloat @maxY, GLfloat @maxZ, GLfloat @maxW);
        internal static glPrimitiveBoundingBoxEXTFunc glPrimitiveBoundingBoxEXTPtr;
        internal static void loadPrimitiveBoundingBoxEXT()
        {
            try
            {
                glPrimitiveBoundingBoxEXTPtr = (glPrimitiveBoundingBoxEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPrimitiveBoundingBoxEXT"), typeof(glPrimitiveBoundingBoxEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPrimitiveBoundingBoxEXT'.");
            }
        }
        public static void glPrimitiveBoundingBoxEXT(GLfloat @minX, GLfloat @minY, GLfloat @minZ, GLfloat @minW, GLfloat @maxX, GLfloat @maxY, GLfloat @maxZ, GLfloat @maxW) => glPrimitiveBoundingBoxEXTPtr(@minX, @minY, @minZ, @minW, @maxX, @maxY, @maxZ, @maxW);
        #endregion
    }
}
