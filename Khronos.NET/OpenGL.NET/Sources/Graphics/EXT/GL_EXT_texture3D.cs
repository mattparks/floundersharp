using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture3D
    {
        #region Interop
        static GL_EXT_texture3D()
        {
            Console.WriteLine("Initalising GL_EXT_texture3D interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexImage3DEXT();
            loadTexSubImage3DEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PACK_SKIP_IMAGES_EXT = 0x806B;
        public static UInt32 GL_PACK_IMAGE_HEIGHT_EXT = 0x806C;
        public static UInt32 GL_UNPACK_SKIP_IMAGES_EXT = 0x806D;
        public static UInt32 GL_UNPACK_IMAGE_HEIGHT_EXT = 0x806E;
        public static UInt32 GL_TEXTURE_3D_EXT = 0x806F;
        public static UInt32 GL_PROXY_TEXTURE_3D_EXT = 0x8070;
        public static UInt32 GL_TEXTURE_DEPTH_EXT = 0x8071;
        public static UInt32 GL_TEXTURE_WRAP_R_EXT = 0x8072;
        public static UInt32 GL_MAX_3D_TEXTURE_SIZE_EXT = 0x8073;
        #endregion

        #region Commands
        internal delegate void glTexImage3DEXTFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexImage3DEXTFunc glTexImage3DEXTPtr;
        internal static void loadTexImage3DEXT()
        {
            try
            {
                glTexImage3DEXTPtr = (glTexImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage3DEXT"), typeof(glTexImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage3DEXT'.");
            }
        }
        public static void glTexImage3DEXT(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glTexImage3DEXTPtr(@target, @level, @internalformat, @width, @height, @depth, @border, @format, @type, @pixels);

        internal delegate void glTexSubImage3DEXTFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexSubImage3DEXTFunc glTexSubImage3DEXTPtr;
        internal static void loadTexSubImage3DEXT()
        {
            try
            {
                glTexSubImage3DEXTPtr = (glTexSubImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexSubImage3DEXT"), typeof(glTexSubImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexSubImage3DEXT'.");
            }
        }
        public static void glTexSubImage3DEXT(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels) => glTexSubImage3DEXTPtr(@target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @type, @pixels);
        #endregion
    }
}
