using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_timer_query
    {
        #region Interop
        static GL_EXT_timer_query()
        {
            Console.WriteLine("Initalising GL_EXT_timer_query interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetQueryObjecti64vEXT();
            loadGetQueryObjectui64vEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TIME_ELAPSED_EXT = 0x88BF;
        #endregion

        #region Commands
        internal delegate void glGetQueryObjecti64vEXTFunc(GLuint @id, GLenum @pname, GLint64 * @params);
        internal static glGetQueryObjecti64vEXTFunc glGetQueryObjecti64vEXTPtr;
        internal static void loadGetQueryObjecti64vEXT()
        {
            try
            {
                glGetQueryObjecti64vEXTPtr = (glGetQueryObjecti64vEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjecti64vEXT"), typeof(glGetQueryObjecti64vEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjecti64vEXT'.");
            }
        }
        public static void glGetQueryObjecti64vEXT(GLuint @id, GLenum @pname, GLint64 * @params) => glGetQueryObjecti64vEXTPtr(@id, @pname, @params);

        internal delegate void glGetQueryObjectui64vEXTFunc(GLuint @id, GLenum @pname, GLuint64 * @params);
        internal static glGetQueryObjectui64vEXTFunc glGetQueryObjectui64vEXTPtr;
        internal static void loadGetQueryObjectui64vEXT()
        {
            try
            {
                glGetQueryObjectui64vEXTPtr = (glGetQueryObjectui64vEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjectui64vEXT"), typeof(glGetQueryObjectui64vEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjectui64vEXT'.");
            }
        }
        public static void glGetQueryObjectui64vEXT(GLuint @id, GLenum @pname, GLuint64 * @params) => glGetQueryObjectui64vEXTPtr(@id, @pname, @params);
        #endregion
    }
}
