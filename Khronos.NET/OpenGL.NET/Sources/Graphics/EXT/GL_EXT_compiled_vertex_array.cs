using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_compiled_vertex_array
    {
        #region Interop
        static GL_EXT_compiled_vertex_array()
        {
            Console.WriteLine("Initalising GL_EXT_compiled_vertex_array interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadLockArraysEXT();
            loadUnlockArraysEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_ARRAY_ELEMENT_LOCK_FIRST_EXT = 0x81A8;
        public static UInt32 GL_ARRAY_ELEMENT_LOCK_COUNT_EXT = 0x81A9;
        #endregion

        #region Commands
        internal delegate void glLockArraysEXTFunc(GLint @first, GLsizei @count);
        internal static glLockArraysEXTFunc glLockArraysEXTPtr;
        internal static void loadLockArraysEXT()
        {
            try
            {
                glLockArraysEXTPtr = (glLockArraysEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLockArraysEXT"), typeof(glLockArraysEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLockArraysEXT'.");
            }
        }
        public static void glLockArraysEXT(GLint @first, GLsizei @count) => glLockArraysEXTPtr(@first, @count);

        internal delegate void glUnlockArraysEXTFunc();
        internal static glUnlockArraysEXTFunc glUnlockArraysEXTPtr;
        internal static void loadUnlockArraysEXT()
        {
            try
            {
                glUnlockArraysEXTPtr = (glUnlockArraysEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUnlockArraysEXT"), typeof(glUnlockArraysEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUnlockArraysEXT'.");
            }
        }
        public static void glUnlockArraysEXT() => glUnlockArraysEXTPtr();
        #endregion
    }
}
