using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_snorm
    {
        #region Interop
        static GL_EXT_texture_snorm()
        {
            Console.WriteLine("Initalising GL_EXT_texture_snorm interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_ALPHA_SNORM = 0x9010;
        public static UInt32 GL_LUMINANCE_SNORM = 0x9011;
        public static UInt32 GL_LUMINANCE_ALPHA_SNORM = 0x9012;
        public static UInt32 GL_INTENSITY_SNORM = 0x9013;
        public static UInt32 GL_ALPHA8_SNORM = 0x9014;
        public static UInt32 GL_LUMINANCE8_SNORM = 0x9015;
        public static UInt32 GL_LUMINANCE8_ALPHA8_SNORM = 0x9016;
        public static UInt32 GL_INTENSITY8_SNORM = 0x9017;
        public static UInt32 GL_ALPHA16_SNORM = 0x9018;
        public static UInt32 GL_LUMINANCE16_SNORM = 0x9019;
        public static UInt32 GL_LUMINANCE16_ALPHA16_SNORM = 0x901A;
        public static UInt32 GL_INTENSITY16_SNORM = 0x901B;
        public static UInt32 GL_RED_SNORM = 0x8F90;
        public static UInt32 GL_RG_SNORM = 0x8F91;
        public static UInt32 GL_RGB_SNORM = 0x8F92;
        public static UInt32 GL_RGBA_SNORM = 0x8F93;
        public static UInt32 GL_R8_SNORM = 0x8F94;
        public static UInt32 GL_RG8_SNORM = 0x8F95;
        public static UInt32 GL_RGB8_SNORM = 0x8F96;
        public static UInt32 GL_RGBA8_SNORM = 0x8F97;
        public static UInt32 GL_R16_SNORM = 0x8F98;
        public static UInt32 GL_RG16_SNORM = 0x8F99;
        public static UInt32 GL_RGB16_SNORM = 0x8F9A;
        public static UInt32 GL_RGBA16_SNORM = 0x8F9B;
        public static UInt32 GL_SIGNED_NORMALIZED = 0x8F9C;
        #endregion

        #region Commands
        #endregion
    }
}
