using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_border_clamp
    {
        #region Interop
        static GL_EXT_texture_border_clamp()
        {
            Console.WriteLine("Initalising GL_EXT_texture_border_clamp interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexParameterIivEXT();
            loadTexParameterIuivEXT();
            loadGetTexParameterIivEXT();
            loadGetTexParameterIuivEXT();
            loadSamplerParameterIivEXT();
            loadSamplerParameterIuivEXT();
            loadGetSamplerParameterIivEXT();
            loadGetSamplerParameterIuivEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_BORDER_COLOR_EXT = 0x1004;
        public static UInt32 GL_CLAMP_TO_BORDER_EXT = 0x812D;
        #endregion

        #region Commands
        internal delegate void glTexParameterIivEXTFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glTexParameterIivEXTFunc glTexParameterIivEXTPtr;
        internal static void loadTexParameterIivEXT()
        {
            try
            {
                glTexParameterIivEXTPtr = (glTexParameterIivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterIivEXT"), typeof(glTexParameterIivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterIivEXT'.");
            }
        }
        public static void glTexParameterIivEXT(GLenum @target, GLenum @pname, const GLint * @params) => glTexParameterIivEXTPtr(@target, @pname, @params);

        internal delegate void glTexParameterIuivEXTFunc(GLenum @target, GLenum @pname, const GLuint * @params);
        internal static glTexParameterIuivEXTFunc glTexParameterIuivEXTPtr;
        internal static void loadTexParameterIuivEXT()
        {
            try
            {
                glTexParameterIuivEXTPtr = (glTexParameterIuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterIuivEXT"), typeof(glTexParameterIuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterIuivEXT'.");
            }
        }
        public static void glTexParameterIuivEXT(GLenum @target, GLenum @pname, const GLuint * @params) => glTexParameterIuivEXTPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterIivEXTFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetTexParameterIivEXTFunc glGetTexParameterIivEXTPtr;
        internal static void loadGetTexParameterIivEXT()
        {
            try
            {
                glGetTexParameterIivEXTPtr = (glGetTexParameterIivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterIivEXT"), typeof(glGetTexParameterIivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterIivEXT'.");
            }
        }
        public static void glGetTexParameterIivEXT(GLenum @target, GLenum @pname, GLint * @params) => glGetTexParameterIivEXTPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterIuivEXTFunc(GLenum @target, GLenum @pname, GLuint * @params);
        internal static glGetTexParameterIuivEXTFunc glGetTexParameterIuivEXTPtr;
        internal static void loadGetTexParameterIuivEXT()
        {
            try
            {
                glGetTexParameterIuivEXTPtr = (glGetTexParameterIuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterIuivEXT"), typeof(glGetTexParameterIuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterIuivEXT'.");
            }
        }
        public static void glGetTexParameterIuivEXT(GLenum @target, GLenum @pname, GLuint * @params) => glGetTexParameterIuivEXTPtr(@target, @pname, @params);

        internal delegate void glSamplerParameterIivEXTFunc(GLuint @sampler, GLenum @pname, const GLint * @param);
        internal static glSamplerParameterIivEXTFunc glSamplerParameterIivEXTPtr;
        internal static void loadSamplerParameterIivEXT()
        {
            try
            {
                glSamplerParameterIivEXTPtr = (glSamplerParameterIivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterIivEXT"), typeof(glSamplerParameterIivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterIivEXT'.");
            }
        }
        public static void glSamplerParameterIivEXT(GLuint @sampler, GLenum @pname, const GLint * @param) => glSamplerParameterIivEXTPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterIuivEXTFunc(GLuint @sampler, GLenum @pname, const GLuint * @param);
        internal static glSamplerParameterIuivEXTFunc glSamplerParameterIuivEXTPtr;
        internal static void loadSamplerParameterIuivEXT()
        {
            try
            {
                glSamplerParameterIuivEXTPtr = (glSamplerParameterIuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterIuivEXT"), typeof(glSamplerParameterIuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterIuivEXT'.");
            }
        }
        public static void glSamplerParameterIuivEXT(GLuint @sampler, GLenum @pname, const GLuint * @param) => glSamplerParameterIuivEXTPtr(@sampler, @pname, @param);

        internal delegate void glGetSamplerParameterIivEXTFunc(GLuint @sampler, GLenum @pname, GLint * @params);
        internal static glGetSamplerParameterIivEXTFunc glGetSamplerParameterIivEXTPtr;
        internal static void loadGetSamplerParameterIivEXT()
        {
            try
            {
                glGetSamplerParameterIivEXTPtr = (glGetSamplerParameterIivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterIivEXT"), typeof(glGetSamplerParameterIivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterIivEXT'.");
            }
        }
        public static void glGetSamplerParameterIivEXT(GLuint @sampler, GLenum @pname, GLint * @params) => glGetSamplerParameterIivEXTPtr(@sampler, @pname, @params);

        internal delegate void glGetSamplerParameterIuivEXTFunc(GLuint @sampler, GLenum @pname, GLuint * @params);
        internal static glGetSamplerParameterIuivEXTFunc glGetSamplerParameterIuivEXTPtr;
        internal static void loadGetSamplerParameterIuivEXT()
        {
            try
            {
                glGetSamplerParameterIuivEXTPtr = (glGetSamplerParameterIuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterIuivEXT"), typeof(glGetSamplerParameterIuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterIuivEXT'.");
            }
        }
        public static void glGetSamplerParameterIuivEXT(GLuint @sampler, GLenum @pname, GLuint * @params) => glGetSamplerParameterIuivEXTPtr(@sampler, @pname, @params);
        #endregion
    }
}
