using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_blend_equation_separate
    {
        #region Interop
        static GL_EXT_blend_equation_separate()
        {
            Console.WriteLine("Initalising GL_EXT_blend_equation_separate interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendEquationSeparateEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BLEND_EQUATION_RGB_EXT = 0x8009;
        public static UInt32 GL_BLEND_EQUATION_ALPHA_EXT = 0x883D;
        #endregion

        #region Commands
        internal delegate void glBlendEquationSeparateEXTFunc(GLenum @modeRGB, GLenum @modeAlpha);
        internal static glBlendEquationSeparateEXTFunc glBlendEquationSeparateEXTPtr;
        internal static void loadBlendEquationSeparateEXT()
        {
            try
            {
                glBlendEquationSeparateEXTPtr = (glBlendEquationSeparateEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationSeparateEXT"), typeof(glBlendEquationSeparateEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationSeparateEXT'.");
            }
        }
        public static void glBlendEquationSeparateEXT(GLenum @modeRGB, GLenum @modeAlpha) => glBlendEquationSeparateEXTPtr(@modeRGB, @modeAlpha);
        #endregion
    }
}
