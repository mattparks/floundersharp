using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_shadow_samplers
    {
        #region Interop
        static GL_EXT_shadow_samplers()
        {
            Console.WriteLine("Initalising GL_EXT_shadow_samplers interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_COMPARE_MODE_EXT = 0x884C;
        public static UInt32 GL_TEXTURE_COMPARE_FUNC_EXT = 0x884D;
        public static UInt32 GL_COMPARE_REF_TO_TEXTURE_EXT = 0x884E;
        public static UInt32 GL_SAMPLER_2D_SHADOW_EXT = 0x8B62;
        #endregion

        #region Commands
        #endregion
    }
}
