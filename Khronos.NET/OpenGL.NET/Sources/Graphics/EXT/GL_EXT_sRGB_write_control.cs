using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_sRGB_write_control
    {
        #region Interop
        static GL_EXT_sRGB_write_control()
        {
            Console.WriteLine("Initalising GL_EXT_sRGB_write_control interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAMEBUFFER_SRGB_EXT = 0x8DB9;
        #endregion

        #region Commands
        #endregion
    }
}
