using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_view
    {
        #region Interop
        static GL_EXT_texture_view()
        {
            Console.WriteLine("Initalising GL_EXT_texture_view interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTextureViewEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_VIEW_MIN_LEVEL_EXT = 0x82DB;
        public static UInt32 GL_TEXTURE_VIEW_NUM_LEVELS_EXT = 0x82DC;
        public static UInt32 GL_TEXTURE_VIEW_MIN_LAYER_EXT = 0x82DD;
        public static UInt32 GL_TEXTURE_VIEW_NUM_LAYERS_EXT = 0x82DE;
        public static UInt32 GL_TEXTURE_IMMUTABLE_LEVELS = 0x82DF;
        #endregion

        #region Commands
        internal delegate void glTextureViewEXTFunc(GLuint @texture, GLenum @target, GLuint @origtexture, GLenum @internalformat, GLuint @minlevel, GLuint @numlevels, GLuint @minlayer, GLuint @numlayers);
        internal static glTextureViewEXTFunc glTextureViewEXTPtr;
        internal static void loadTextureViewEXT()
        {
            try
            {
                glTextureViewEXTPtr = (glTextureViewEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureViewEXT"), typeof(glTextureViewEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureViewEXT'.");
            }
        }
        public static void glTextureViewEXT(GLuint @texture, GLenum @target, GLuint @origtexture, GLenum @internalformat, GLuint @minlevel, GLuint @numlevels, GLuint @minlayer, GLuint @numlayers) => glTextureViewEXTPtr(@texture, @target, @origtexture, @internalformat, @minlevel, @numlevels, @minlayer, @numlayers);
        #endregion
    }
}
