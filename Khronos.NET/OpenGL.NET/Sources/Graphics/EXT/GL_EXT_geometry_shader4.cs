using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_geometry_shader4
    {
        #region Interop
        static GL_EXT_geometry_shader4()
        {
            Console.WriteLine("Initalising GL_EXT_geometry_shader4 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadProgramParameteriEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_GEOMETRY_SHADER_EXT = 0x8DD9;
        public static UInt32 GL_GEOMETRY_VERTICES_OUT_EXT = 0x8DDA;
        public static UInt32 GL_GEOMETRY_INPUT_TYPE_EXT = 0x8DDB;
        public static UInt32 GL_GEOMETRY_OUTPUT_TYPE_EXT = 0x8DDC;
        public static UInt32 GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS_EXT = 0x8C29;
        public static UInt32 GL_MAX_GEOMETRY_VARYING_COMPONENTS_EXT = 0x8DDD;
        public static UInt32 GL_MAX_VERTEX_VARYING_COMPONENTS_EXT = 0x8DDE;
        public static UInt32 GL_MAX_VARYING_COMPONENTS_EXT = 0x8B4B;
        public static UInt32 GL_MAX_GEOMETRY_UNIFORM_COMPONENTS_EXT = 0x8DDF;
        public static UInt32 GL_MAX_GEOMETRY_OUTPUT_VERTICES_EXT = 0x8DE0;
        public static UInt32 GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS_EXT = 0x8DE1;
        public static UInt32 GL_LINES_ADJACENCY_EXT = 0x000A;
        public static UInt32 GL_LINE_STRIP_ADJACENCY_EXT = 0x000B;
        public static UInt32 GL_TRIANGLES_ADJACENCY_EXT = 0x000C;
        public static UInt32 GL_TRIANGLE_STRIP_ADJACENCY_EXT = 0x000D;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS_EXT = 0x8DA8;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_LAYER_COUNT_EXT = 0x8DA9;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_LAYERED_EXT = 0x8DA7;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER_EXT = 0x8CD4;
        public static UInt32 GL_PROGRAM_POINT_SIZE_EXT = 0x8642;
        #endregion

        #region Commands
        internal delegate void glProgramParameteriEXTFunc(GLuint @program, GLenum @pname, GLint @value);
        internal static glProgramParameteriEXTFunc glProgramParameteriEXTPtr;
        internal static void loadProgramParameteriEXT()
        {
            try
            {
                glProgramParameteriEXTPtr = (glProgramParameteriEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramParameteriEXT"), typeof(glProgramParameteriEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramParameteriEXT'.");
            }
        }
        public static void glProgramParameteriEXT(GLuint @program, GLenum @pname, GLint @value) => glProgramParameteriEXTPtr(@program, @pname, @value);
        #endregion
    }
}
