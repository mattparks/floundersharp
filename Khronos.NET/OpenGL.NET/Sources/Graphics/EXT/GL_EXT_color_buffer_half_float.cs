using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_color_buffer_half_float
    {
        #region Interop
        static GL_EXT_color_buffer_half_float()
        {
            Console.WriteLine("Initalising GL_EXT_color_buffer_half_float interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RGBA16F_EXT = 0x881A;
        public static UInt32 GL_RGB16F_EXT = 0x881B;
        public static UInt32 GL_RG16F_EXT = 0x822F;
        public static UInt32 GL_R16F_EXT = 0x822D;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE_EXT = 0x8211;
        public static UInt32 GL_UNSIGNED_NORMALIZED_EXT = 0x8C17;
        #endregion

        #region Commands
        #endregion
    }
}
