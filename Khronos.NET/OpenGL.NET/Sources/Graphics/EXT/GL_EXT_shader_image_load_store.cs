using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_shader_image_load_store
    {
        #region Interop
        static GL_EXT_shader_image_load_store()
        {
            Console.WriteLine("Initalising GL_EXT_shader_image_load_store interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindImageTextureEXT();
            loadMemoryBarrierEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_IMAGE_UNITS_EXT = 0x8F38;
        public static UInt32 GL_MAX_COMBINED_IMAGE_UNITS_AND_FRAGMENT_OUTPUTS_EXT = 0x8F39;
        public static UInt32 GL_IMAGE_BINDING_NAME_EXT = 0x8F3A;
        public static UInt32 GL_IMAGE_BINDING_LEVEL_EXT = 0x8F3B;
        public static UInt32 GL_IMAGE_BINDING_LAYERED_EXT = 0x8F3C;
        public static UInt32 GL_IMAGE_BINDING_LAYER_EXT = 0x8F3D;
        public static UInt32 GL_IMAGE_BINDING_ACCESS_EXT = 0x8F3E;
        public static UInt32 GL_IMAGE_1D_EXT = 0x904C;
        public static UInt32 GL_IMAGE_2D_EXT = 0x904D;
        public static UInt32 GL_IMAGE_3D_EXT = 0x904E;
        public static UInt32 GL_IMAGE_2D_RECT_EXT = 0x904F;
        public static UInt32 GL_IMAGE_CUBE_EXT = 0x9050;
        public static UInt32 GL_IMAGE_BUFFER_EXT = 0x9051;
        public static UInt32 GL_IMAGE_1D_ARRAY_EXT = 0x9052;
        public static UInt32 GL_IMAGE_2D_ARRAY_EXT = 0x9053;
        public static UInt32 GL_IMAGE_CUBE_MAP_ARRAY_EXT = 0x9054;
        public static UInt32 GL_IMAGE_2D_MULTISAMPLE_EXT = 0x9055;
        public static UInt32 GL_IMAGE_2D_MULTISAMPLE_ARRAY_EXT = 0x9056;
        public static UInt32 GL_INT_IMAGE_1D_EXT = 0x9057;
        public static UInt32 GL_INT_IMAGE_2D_EXT = 0x9058;
        public static UInt32 GL_INT_IMAGE_3D_EXT = 0x9059;
        public static UInt32 GL_INT_IMAGE_2D_RECT_EXT = 0x905A;
        public static UInt32 GL_INT_IMAGE_CUBE_EXT = 0x905B;
        public static UInt32 GL_INT_IMAGE_BUFFER_EXT = 0x905C;
        public static UInt32 GL_INT_IMAGE_1D_ARRAY_EXT = 0x905D;
        public static UInt32 GL_INT_IMAGE_2D_ARRAY_EXT = 0x905E;
        public static UInt32 GL_INT_IMAGE_CUBE_MAP_ARRAY_EXT = 0x905F;
        public static UInt32 GL_INT_IMAGE_2D_MULTISAMPLE_EXT = 0x9060;
        public static UInt32 GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY_EXT = 0x9061;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_1D_EXT = 0x9062;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_EXT = 0x9063;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_3D_EXT = 0x9064;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_RECT_EXT = 0x9065;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_CUBE_EXT = 0x9066;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_BUFFER_EXT = 0x9067;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_1D_ARRAY_EXT = 0x9068;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_ARRAY_EXT = 0x9069;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_CUBE_MAP_ARRAY_EXT = 0x906A;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_EXT = 0x906B;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY_EXT = 0x906C;
        public static UInt32 GL_MAX_IMAGE_SAMPLES_EXT = 0x906D;
        public static UInt32 GL_IMAGE_BINDING_FORMAT_EXT = 0x906E;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT_EXT = 0x00000001;
        public static UInt32 GL_ELEMENT_ARRAY_BARRIER_BIT_EXT = 0x00000002;
        public static UInt32 GL_UNIFORM_BARRIER_BIT_EXT = 0x00000004;
        public static UInt32 GL_TEXTURE_FETCH_BARRIER_BIT_EXT = 0x00000008;
        public static UInt32 GL_SHADER_IMAGE_ACCESS_BARRIER_BIT_EXT = 0x00000020;
        public static UInt32 GL_COMMAND_BARRIER_BIT_EXT = 0x00000040;
        public static UInt32 GL_PIXEL_BUFFER_BARRIER_BIT_EXT = 0x00000080;
        public static UInt32 GL_TEXTURE_UPDATE_BARRIER_BIT_EXT = 0x00000100;
        public static UInt32 GL_BUFFER_UPDATE_BARRIER_BIT_EXT = 0x00000200;
        public static UInt32 GL_FRAMEBUFFER_BARRIER_BIT_EXT = 0x00000400;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BARRIER_BIT_EXT = 0x00000800;
        public static UInt32 GL_ATOMIC_COUNTER_BARRIER_BIT_EXT = 0x00001000;
        public static UInt32 GL_ALL_BARRIER_BITS_EXT = 0xFFFFFFFF;
        #endregion

        #region Commands
        internal delegate void glBindImageTextureEXTFunc(GLuint @index, GLuint @texture, GLint @level, GLboolean @layered, GLint @layer, GLenum @access, GLint @format);
        internal static glBindImageTextureEXTFunc glBindImageTextureEXTPtr;
        internal static void loadBindImageTextureEXT()
        {
            try
            {
                glBindImageTextureEXTPtr = (glBindImageTextureEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindImageTextureEXT"), typeof(glBindImageTextureEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindImageTextureEXT'.");
            }
        }
        public static void glBindImageTextureEXT(GLuint @index, GLuint @texture, GLint @level, GLboolean @layered, GLint @layer, GLenum @access, GLint @format) => glBindImageTextureEXTPtr(@index, @texture, @level, @layered, @layer, @access, @format);

        internal delegate void glMemoryBarrierEXTFunc(GLbitfield @barriers);
        internal static glMemoryBarrierEXTFunc glMemoryBarrierEXTPtr;
        internal static void loadMemoryBarrierEXT()
        {
            try
            {
                glMemoryBarrierEXTPtr = (glMemoryBarrierEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMemoryBarrierEXT"), typeof(glMemoryBarrierEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMemoryBarrierEXT'.");
            }
        }
        public static void glMemoryBarrierEXT(GLbitfield @barriers) => glMemoryBarrierEXTPtr(@barriers);
        #endregion
    }
}
