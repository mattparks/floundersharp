using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_blend_minmax
    {
        #region Interop
        static GL_EXT_blend_minmax()
        {
            Console.WriteLine("Initalising GL_EXT_blend_minmax interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendEquationEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MIN_EXT = 0x8007;
        public static UInt32 GL_MAX_EXT = 0x8008;
        public static UInt32 GL_FUNC_ADD_EXT = 0x8006;
        public static UInt32 GL_BLEND_EQUATION_EXT = 0x8009;
        #endregion

        #region Commands
        internal delegate void glBlendEquationEXTFunc(GLenum @mode);
        internal static glBlendEquationEXTFunc glBlendEquationEXTPtr;
        internal static void loadBlendEquationEXT()
        {
            try
            {
                glBlendEquationEXTPtr = (glBlendEquationEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationEXT"), typeof(glBlendEquationEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationEXT'.");
            }
        }
        public static void glBlendEquationEXT(GLenum @mode) => glBlendEquationEXTPtr(@mode);
        #endregion
    }
}
