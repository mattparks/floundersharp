using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_subtexture
    {
        #region Interop
        static GL_EXT_subtexture()
        {
            Console.WriteLine("Initalising GL_EXT_subtexture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexSubImage1DEXT();
            loadTexSubImage2DEXT();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glTexSubImage1DEXTFunc(GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexSubImage1DEXTFunc glTexSubImage1DEXTPtr;
        internal static void loadTexSubImage1DEXT()
        {
            try
            {
                glTexSubImage1DEXTPtr = (glTexSubImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexSubImage1DEXT"), typeof(glTexSubImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexSubImage1DEXT'.");
            }
        }
        public static void glTexSubImage1DEXT(GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLenum @type, const void * @pixels) => glTexSubImage1DEXTPtr(@target, @level, @xoffset, @width, @format, @type, @pixels);

        internal delegate void glTexSubImage2DEXTFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexSubImage2DEXTFunc glTexSubImage2DEXTPtr;
        internal static void loadTexSubImage2DEXT()
        {
            try
            {
                glTexSubImage2DEXTPtr = (glTexSubImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexSubImage2DEXT"), typeof(glTexSubImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexSubImage2DEXT'.");
            }
        }
        public static void glTexSubImage2DEXT(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels) => glTexSubImage2DEXTPtr(@target, @level, @xoffset, @yoffset, @width, @height, @format, @type, @pixels);
        #endregion
    }
}
