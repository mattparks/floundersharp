using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_base_instance
    {
        #region Interop
        static GL_EXT_base_instance()
        {
            Console.WriteLine("Initalising GL_EXT_base_instance interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawArraysInstancedBaseInstanceEXT();
            loadDrawElementsInstancedBaseInstanceEXT();
            loadDrawElementsInstancedBaseVertexBaseInstanceEXT();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glDrawArraysInstancedBaseInstanceEXTFunc(GLenum @mode, GLint @first, GLsizei @count, GLsizei @instancecount, GLuint @baseinstance);
        internal static glDrawArraysInstancedBaseInstanceEXTFunc glDrawArraysInstancedBaseInstanceEXTPtr;
        internal static void loadDrawArraysInstancedBaseInstanceEXT()
        {
            try
            {
                glDrawArraysInstancedBaseInstanceEXTPtr = (glDrawArraysInstancedBaseInstanceEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysInstancedBaseInstanceEXT"), typeof(glDrawArraysInstancedBaseInstanceEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysInstancedBaseInstanceEXT'.");
            }
        }
        public static void glDrawArraysInstancedBaseInstanceEXT(GLenum @mode, GLint @first, GLsizei @count, GLsizei @instancecount, GLuint @baseinstance) => glDrawArraysInstancedBaseInstanceEXTPtr(@mode, @first, @count, @instancecount, @baseinstance);

        internal delegate void glDrawElementsInstancedBaseInstanceEXTFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLuint @baseinstance);
        internal static glDrawElementsInstancedBaseInstanceEXTFunc glDrawElementsInstancedBaseInstanceEXTPtr;
        internal static void loadDrawElementsInstancedBaseInstanceEXT()
        {
            try
            {
                glDrawElementsInstancedBaseInstanceEXTPtr = (glDrawElementsInstancedBaseInstanceEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedBaseInstanceEXT"), typeof(glDrawElementsInstancedBaseInstanceEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedBaseInstanceEXT'.");
            }
        }
        public static void glDrawElementsInstancedBaseInstanceEXT(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLuint @baseinstance) => glDrawElementsInstancedBaseInstanceEXTPtr(@mode, @count, @type, @indices, @instancecount, @baseinstance);

        internal delegate void glDrawElementsInstancedBaseVertexBaseInstanceEXTFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex, GLuint @baseinstance);
        internal static glDrawElementsInstancedBaseVertexBaseInstanceEXTFunc glDrawElementsInstancedBaseVertexBaseInstanceEXTPtr;
        internal static void loadDrawElementsInstancedBaseVertexBaseInstanceEXT()
        {
            try
            {
                glDrawElementsInstancedBaseVertexBaseInstanceEXTPtr = (glDrawElementsInstancedBaseVertexBaseInstanceEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedBaseVertexBaseInstanceEXT"), typeof(glDrawElementsInstancedBaseVertexBaseInstanceEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedBaseVertexBaseInstanceEXT'.");
            }
        }
        public static void glDrawElementsInstancedBaseVertexBaseInstanceEXT(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex, GLuint @baseinstance) => glDrawElementsInstancedBaseVertexBaseInstanceEXTPtr(@mode, @count, @type, @indices, @instancecount, @basevertex, @baseinstance);
        #endregion
    }
}
