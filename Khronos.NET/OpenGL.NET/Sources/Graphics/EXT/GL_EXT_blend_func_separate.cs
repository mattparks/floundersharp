using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_blend_func_separate
    {
        #region Interop
        static GL_EXT_blend_func_separate()
        {
            Console.WriteLine("Initalising GL_EXT_blend_func_separate interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendFuncSeparateEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BLEND_DST_RGB_EXT = 0x80C8;
        public static UInt32 GL_BLEND_SRC_RGB_EXT = 0x80C9;
        public static UInt32 GL_BLEND_DST_ALPHA_EXT = 0x80CA;
        public static UInt32 GL_BLEND_SRC_ALPHA_EXT = 0x80CB;
        #endregion

        #region Commands
        internal delegate void glBlendFuncSeparateEXTFunc(GLenum @sfactorRGB, GLenum @dfactorRGB, GLenum @sfactorAlpha, GLenum @dfactorAlpha);
        internal static glBlendFuncSeparateEXTFunc glBlendFuncSeparateEXTPtr;
        internal static void loadBlendFuncSeparateEXT()
        {
            try
            {
                glBlendFuncSeparateEXTPtr = (glBlendFuncSeparateEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFuncSeparateEXT"), typeof(glBlendFuncSeparateEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFuncSeparateEXT'.");
            }
        }
        public static void glBlendFuncSeparateEXT(GLenum @sfactorRGB, GLenum @dfactorRGB, GLenum @sfactorAlpha, GLenum @dfactorAlpha) => glBlendFuncSeparateEXTPtr(@sfactorRGB, @dfactorRGB, @sfactorAlpha, @dfactorAlpha);
        #endregion
    }
}
