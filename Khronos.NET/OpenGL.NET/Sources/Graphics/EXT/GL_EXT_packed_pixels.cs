using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_packed_pixels
    {
        #region Interop
        static GL_EXT_packed_pixels()
        {
            Console.WriteLine("Initalising GL_EXT_packed_pixels interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_UNSIGNED_BYTE_3_3_2_EXT = 0x8032;
        public static UInt32 GL_UNSIGNED_SHORT_4_4_4_4_EXT = 0x8033;
        public static UInt32 GL_UNSIGNED_SHORT_5_5_5_1_EXT = 0x8034;
        public static UInt32 GL_UNSIGNED_INT_8_8_8_8_EXT = 0x8035;
        public static UInt32 GL_UNSIGNED_INT_10_10_10_2_EXT = 0x8036;
        #endregion

        #region Commands
        #endregion
    }
}
