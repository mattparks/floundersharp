using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_perturb_normal
    {
        #region Interop
        static GL_EXT_texture_perturb_normal()
        {
            Console.WriteLine("Initalising GL_EXT_texture_perturb_normal interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTextureNormalEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PERTURB_EXT = 0x85AE;
        public static UInt32 GL_TEXTURE_NORMAL_EXT = 0x85AF;
        #endregion

        #region Commands
        internal delegate void glTextureNormalEXTFunc(GLenum @mode);
        internal static glTextureNormalEXTFunc glTextureNormalEXTPtr;
        internal static void loadTextureNormalEXT()
        {
            try
            {
                glTextureNormalEXTPtr = (glTextureNormalEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureNormalEXT"), typeof(glTextureNormalEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureNormalEXT'.");
            }
        }
        public static void glTextureNormalEXT(GLenum @mode) => glTextureNormalEXTPtr(@mode);
        #endregion
    }
}
