using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_packed_depth_stencil
    {
        #region Interop
        static GL_EXT_packed_depth_stencil()
        {
            Console.WriteLine("Initalising GL_EXT_packed_depth_stencil interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_STENCIL_EXT = 0x84F9;
        public static UInt32 GL_UNSIGNED_INT_24_8_EXT = 0x84FA;
        public static UInt32 GL_DEPTH24_STENCIL8_EXT = 0x88F0;
        public static UInt32 GL_TEXTURE_STENCIL_SIZE_EXT = 0x88F1;
        #endregion

        #region Commands
        #endregion
    }
}
