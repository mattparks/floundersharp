using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_buffer
    {
        #region Interop
        static GL_EXT_texture_buffer()
        {
            Console.WriteLine("Initalising GL_EXT_texture_buffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexBufferEXT();
            loadTexBufferRangeEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_BUFFER_EXT = 0x8C2A;
        public static UInt32 GL_TEXTURE_BUFFER_BINDING_EXT = 0x8C2A;
        public static UInt32 GL_MAX_TEXTURE_BUFFER_SIZE_EXT = 0x8C2B;
        public static UInt32 GL_TEXTURE_BINDING_BUFFER_EXT = 0x8C2C;
        public static UInt32 GL_TEXTURE_BUFFER_DATA_STORE_BINDING_EXT = 0x8C2D;
        public static UInt32 GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT_EXT = 0x919F;
        public static UInt32 GL_SAMPLER_BUFFER_EXT = 0x8DC2;
        public static UInt32 GL_INT_SAMPLER_BUFFER_EXT = 0x8DD0;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_BUFFER_EXT = 0x8DD8;
        public static UInt32 GL_IMAGE_BUFFER_EXT = 0x9051;
        public static UInt32 GL_INT_IMAGE_BUFFER_EXT = 0x905C;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_BUFFER_EXT = 0x9067;
        public static UInt32 GL_TEXTURE_BUFFER_OFFSET_EXT = 0x919D;
        public static UInt32 GL_TEXTURE_BUFFER_SIZE_EXT = 0x919E;
        #endregion

        #region Commands
        internal delegate void glTexBufferEXTFunc(GLenum @target, GLenum @internalformat, GLuint @buffer);
        internal static glTexBufferEXTFunc glTexBufferEXTPtr;
        internal static void loadTexBufferEXT()
        {
            try
            {
                glTexBufferEXTPtr = (glTexBufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBufferEXT"), typeof(glTexBufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBufferEXT'.");
            }
        }
        public static void glTexBufferEXT(GLenum @target, GLenum @internalformat, GLuint @buffer) => glTexBufferEXTPtr(@target, @internalformat, @buffer);

        internal delegate void glTexBufferRangeEXTFunc(GLenum @target, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glTexBufferRangeEXTFunc glTexBufferRangeEXTPtr;
        internal static void loadTexBufferRangeEXT()
        {
            try
            {
                glTexBufferRangeEXTPtr = (glTexBufferRangeEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBufferRangeEXT"), typeof(glTexBufferRangeEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBufferRangeEXT'.");
            }
        }
        public static void glTexBufferRangeEXT(GLenum @target, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glTexBufferRangeEXTPtr(@target, @internalformat, @buffer, @offset, @size);
        #endregion
    }
}
