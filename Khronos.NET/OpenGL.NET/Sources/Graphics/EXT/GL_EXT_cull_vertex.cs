using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_cull_vertex
    {
        #region Interop
        static GL_EXT_cull_vertex()
        {
            Console.WriteLine("Initalising GL_EXT_cull_vertex interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCullParameterdvEXT();
            loadCullParameterfvEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_CULL_VERTEX_EXT = 0x81AA;
        public static UInt32 GL_CULL_VERTEX_EYE_POSITION_EXT = 0x81AB;
        public static UInt32 GL_CULL_VERTEX_OBJECT_POSITION_EXT = 0x81AC;
        #endregion

        #region Commands
        internal delegate void glCullParameterdvEXTFunc(GLenum @pname, GLdouble * @params);
        internal static glCullParameterdvEXTFunc glCullParameterdvEXTPtr;
        internal static void loadCullParameterdvEXT()
        {
            try
            {
                glCullParameterdvEXTPtr = (glCullParameterdvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCullParameterdvEXT"), typeof(glCullParameterdvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCullParameterdvEXT'.");
            }
        }
        public static void glCullParameterdvEXT(GLenum @pname, GLdouble * @params) => glCullParameterdvEXTPtr(@pname, @params);

        internal delegate void glCullParameterfvEXTFunc(GLenum @pname, GLfloat * @params);
        internal static glCullParameterfvEXTFunc glCullParameterfvEXTPtr;
        internal static void loadCullParameterfvEXT()
        {
            try
            {
                glCullParameterfvEXTPtr = (glCullParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCullParameterfvEXT"), typeof(glCullParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCullParameterfvEXT'.");
            }
        }
        public static void glCullParameterfvEXT(GLenum @pname, GLfloat * @params) => glCullParameterfvEXTPtr(@pname, @params);
        #endregion
    }
}
