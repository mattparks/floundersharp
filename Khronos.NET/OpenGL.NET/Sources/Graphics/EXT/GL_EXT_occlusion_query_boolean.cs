using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_occlusion_query_boolean
    {
        #region Interop
        static GL_EXT_occlusion_query_boolean()
        {
            Console.WriteLine("Initalising GL_EXT_occlusion_query_boolean interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGenQueriesEXT();
            loadDeleteQueriesEXT();
            loadIsQueryEXT();
            loadBeginQueryEXT();
            loadEndQueryEXT();
            loadGetQueryivEXT();
            loadGetQueryObjectuivEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_ANY_SAMPLES_PASSED_EXT = 0x8C2F;
        public static UInt32 GL_ANY_SAMPLES_PASSED_CONSERVATIVE_EXT = 0x8D6A;
        public static UInt32 GL_CURRENT_QUERY_EXT = 0x8865;
        public static UInt32 GL_QUERY_RESULT_EXT = 0x8866;
        public static UInt32 GL_QUERY_RESULT_AVAILABLE_EXT = 0x8867;
        #endregion

        #region Commands
        internal delegate void glGenQueriesEXTFunc(GLsizei @n, GLuint * @ids);
        internal static glGenQueriesEXTFunc glGenQueriesEXTPtr;
        internal static void loadGenQueriesEXT()
        {
            try
            {
                glGenQueriesEXTPtr = (glGenQueriesEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenQueriesEXT"), typeof(glGenQueriesEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenQueriesEXT'.");
            }
        }
        public static void glGenQueriesEXT(GLsizei @n, GLuint * @ids) => glGenQueriesEXTPtr(@n, @ids);

        internal delegate void glDeleteQueriesEXTFunc(GLsizei @n, const GLuint * @ids);
        internal static glDeleteQueriesEXTFunc glDeleteQueriesEXTPtr;
        internal static void loadDeleteQueriesEXT()
        {
            try
            {
                glDeleteQueriesEXTPtr = (glDeleteQueriesEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteQueriesEXT"), typeof(glDeleteQueriesEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteQueriesEXT'.");
            }
        }
        public static void glDeleteQueriesEXT(GLsizei @n, const GLuint * @ids) => glDeleteQueriesEXTPtr(@n, @ids);

        internal delegate GLboolean glIsQueryEXTFunc(GLuint @id);
        internal static glIsQueryEXTFunc glIsQueryEXTPtr;
        internal static void loadIsQueryEXT()
        {
            try
            {
                glIsQueryEXTPtr = (glIsQueryEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsQueryEXT"), typeof(glIsQueryEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsQueryEXT'.");
            }
        }
        public static GLboolean glIsQueryEXT(GLuint @id) => glIsQueryEXTPtr(@id);

        internal delegate void glBeginQueryEXTFunc(GLenum @target, GLuint @id);
        internal static glBeginQueryEXTFunc glBeginQueryEXTPtr;
        internal static void loadBeginQueryEXT()
        {
            try
            {
                glBeginQueryEXTPtr = (glBeginQueryEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginQueryEXT"), typeof(glBeginQueryEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginQueryEXT'.");
            }
        }
        public static void glBeginQueryEXT(GLenum @target, GLuint @id) => glBeginQueryEXTPtr(@target, @id);

        internal delegate void glEndQueryEXTFunc(GLenum @target);
        internal static glEndQueryEXTFunc glEndQueryEXTPtr;
        internal static void loadEndQueryEXT()
        {
            try
            {
                glEndQueryEXTPtr = (glEndQueryEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndQueryEXT"), typeof(glEndQueryEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndQueryEXT'.");
            }
        }
        public static void glEndQueryEXT(GLenum @target) => glEndQueryEXTPtr(@target);

        internal delegate void glGetQueryivEXTFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetQueryivEXTFunc glGetQueryivEXTPtr;
        internal static void loadGetQueryivEXT()
        {
            try
            {
                glGetQueryivEXTPtr = (glGetQueryivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryivEXT"), typeof(glGetQueryivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryivEXT'.");
            }
        }
        public static void glGetQueryivEXT(GLenum @target, GLenum @pname, GLint * @params) => glGetQueryivEXTPtr(@target, @pname, @params);

        internal delegate void glGetQueryObjectuivEXTFunc(GLuint @id, GLenum @pname, GLuint * @params);
        internal static glGetQueryObjectuivEXTFunc glGetQueryObjectuivEXTPtr;
        internal static void loadGetQueryObjectuivEXT()
        {
            try
            {
                glGetQueryObjectuivEXTPtr = (glGetQueryObjectuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjectuivEXT"), typeof(glGetQueryObjectuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjectuivEXT'.");
            }
        }
        public static void glGetQueryObjectuivEXT(GLuint @id, GLenum @pname, GLuint * @params) => glGetQueryObjectuivEXTPtr(@id, @pname, @params);
        #endregion
    }
}
