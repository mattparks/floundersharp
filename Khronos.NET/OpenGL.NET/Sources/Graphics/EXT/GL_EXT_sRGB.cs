using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_sRGB
    {
        #region Interop
        static GL_EXT_sRGB()
        {
            Console.WriteLine("Initalising GL_EXT_sRGB interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SRGB_EXT = 0x8C40;
        public static UInt32 GL_SRGB_ALPHA_EXT = 0x8C42;
        public static UInt32 GL_SRGB8_ALPHA8_EXT = 0x8C43;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING_EXT = 0x8210;
        #endregion

        #region Commands
        #endregion
    }
}
