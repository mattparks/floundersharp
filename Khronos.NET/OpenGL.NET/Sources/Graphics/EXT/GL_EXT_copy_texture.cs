using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_copy_texture
    {
        #region Interop
        static GL_EXT_copy_texture()
        {
            Console.WriteLine("Initalising GL_EXT_copy_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCopyTexImage1DEXT();
            loadCopyTexImage2DEXT();
            loadCopyTexSubImage1DEXT();
            loadCopyTexSubImage2DEXT();
            loadCopyTexSubImage3DEXT();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glCopyTexImage1DEXTFunc(GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLint @border);
        internal static glCopyTexImage1DEXTFunc glCopyTexImage1DEXTPtr;
        internal static void loadCopyTexImage1DEXT()
        {
            try
            {
                glCopyTexImage1DEXTPtr = (glCopyTexImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexImage1DEXT"), typeof(glCopyTexImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexImage1DEXT'.");
            }
        }
        public static void glCopyTexImage1DEXT(GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLint @border) => glCopyTexImage1DEXTPtr(@target, @level, @internalformat, @x, @y, @width, @border);

        internal delegate void glCopyTexImage2DEXTFunc(GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLint @border);
        internal static glCopyTexImage2DEXTFunc glCopyTexImage2DEXTPtr;
        internal static void loadCopyTexImage2DEXT()
        {
            try
            {
                glCopyTexImage2DEXTPtr = (glCopyTexImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexImage2DEXT"), typeof(glCopyTexImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexImage2DEXT'.");
            }
        }
        public static void glCopyTexImage2DEXT(GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLint @border) => glCopyTexImage2DEXTPtr(@target, @level, @internalformat, @x, @y, @width, @height, @border);

        internal delegate void glCopyTexSubImage1DEXTFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @x, GLint @y, GLsizei @width);
        internal static glCopyTexSubImage1DEXTFunc glCopyTexSubImage1DEXTPtr;
        internal static void loadCopyTexSubImage1DEXT()
        {
            try
            {
                glCopyTexSubImage1DEXTPtr = (glCopyTexSubImage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexSubImage1DEXT"), typeof(glCopyTexSubImage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexSubImage1DEXT'.");
            }
        }
        public static void glCopyTexSubImage1DEXT(GLenum @target, GLint @level, GLint @xoffset, GLint @x, GLint @y, GLsizei @width) => glCopyTexSubImage1DEXTPtr(@target, @level, @xoffset, @x, @y, @width);

        internal delegate void glCopyTexSubImage2DEXTFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyTexSubImage2DEXTFunc glCopyTexSubImage2DEXTPtr;
        internal static void loadCopyTexSubImage2DEXT()
        {
            try
            {
                glCopyTexSubImage2DEXTPtr = (glCopyTexSubImage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexSubImage2DEXT"), typeof(glCopyTexSubImage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexSubImage2DEXT'.");
            }
        }
        public static void glCopyTexSubImage2DEXT(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyTexSubImage2DEXTPtr(@target, @level, @xoffset, @yoffset, @x, @y, @width, @height);

        internal delegate void glCopyTexSubImage3DEXTFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyTexSubImage3DEXTFunc glCopyTexSubImage3DEXTPtr;
        internal static void loadCopyTexSubImage3DEXT()
        {
            try
            {
                glCopyTexSubImage3DEXTPtr = (glCopyTexSubImage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexSubImage3DEXT"), typeof(glCopyTexSubImage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexSubImage3DEXT'.");
            }
        }
        public static void glCopyTexSubImage3DEXT(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyTexSubImage3DEXTPtr(@target, @level, @xoffset, @yoffset, @zoffset, @x, @y, @width, @height);
        #endregion
    }
}
