using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_polygon_offset
    {
        #region Interop
        static GL_EXT_polygon_offset()
        {
            Console.WriteLine("Initalising GL_EXT_polygon_offset interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPolygonOffsetEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_POLYGON_OFFSET_EXT = 0x8037;
        public static UInt32 GL_POLYGON_OFFSET_FACTOR_EXT = 0x8038;
        public static UInt32 GL_POLYGON_OFFSET_BIAS_EXT = 0x8039;
        #endregion

        #region Commands
        internal delegate void glPolygonOffsetEXTFunc(GLfloat @factor, GLfloat @bias);
        internal static glPolygonOffsetEXTFunc glPolygonOffsetEXTPtr;
        internal static void loadPolygonOffsetEXT()
        {
            try
            {
                glPolygonOffsetEXTPtr = (glPolygonOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPolygonOffsetEXT"), typeof(glPolygonOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPolygonOffsetEXT'.");
            }
        }
        public static void glPolygonOffsetEXT(GLfloat @factor, GLfloat @bias) => glPolygonOffsetEXTPtr(@factor, @bias);
        #endregion
    }
}
