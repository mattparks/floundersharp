using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_sparse_texture
    {
        #region Interop
        static GL_EXT_sparse_texture()
        {
            Console.WriteLine("Initalising GL_EXT_sparse_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexPageCommitmentEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_SPARSE_EXT = 0x91A6;
        public static UInt32 GL_VIRTUAL_PAGE_SIZE_INDEX_EXT = 0x91A7;
        public static UInt32 GL_NUM_SPARSE_LEVELS_EXT = 0x91AA;
        public static UInt32 GL_NUM_VIRTUAL_PAGE_SIZES_EXT = 0x91A8;
        public static UInt32 GL_VIRTUAL_PAGE_SIZE_X_EXT = 0x9195;
        public static UInt32 GL_VIRTUAL_PAGE_SIZE_Y_EXT = 0x9196;
        public static UInt32 GL_VIRTUAL_PAGE_SIZE_Z_EXT = 0x9197;
        public static UInt32 GL_TEXTURE_2D = 0x0DE1;
        public static UInt32 GL_TEXTURE_2D_ARRAY = 0x8C1A;
        public static UInt32 GL_TEXTURE_CUBE_MAP = 0x8513;
        public static UInt32 GL_TEXTURE_CUBE_MAP_ARRAY_OES = 0x9009;
        public static UInt32 GL_TEXTURE_3D = 0x806F;
        public static UInt32 GL_MAX_SPARSE_TEXTURE_SIZE_EXT = 0x9198;
        public static UInt32 GL_MAX_SPARSE_3D_TEXTURE_SIZE_EXT = 0x9199;
        public static UInt32 GL_MAX_SPARSE_ARRAY_TEXTURE_LAYERS_EXT = 0x919A;
        public static UInt32 GL_SPARSE_TEXTURE_FULL_ARRAY_CUBE_MIPMAPS_EXT = 0x91A9;
        #endregion

        #region Commands
        internal delegate void glTexPageCommitmentEXTFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @commit);
        internal static glTexPageCommitmentEXTFunc glTexPageCommitmentEXTPtr;
        internal static void loadTexPageCommitmentEXT()
        {
            try
            {
                glTexPageCommitmentEXTPtr = (glTexPageCommitmentEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexPageCommitmentEXT"), typeof(glTexPageCommitmentEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexPageCommitmentEXT'.");
            }
        }
        public static void glTexPageCommitmentEXT(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @commit) => glTexPageCommitmentEXTPtr(@target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @commit);
        #endregion
    }
}
