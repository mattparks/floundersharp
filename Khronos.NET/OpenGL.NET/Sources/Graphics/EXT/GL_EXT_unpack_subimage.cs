using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_unpack_subimage
    {
        #region Interop
        static GL_EXT_unpack_subimage()
        {
            Console.WriteLine("Initalising GL_EXT_unpack_subimage interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_UNPACK_ROW_LENGTH_EXT = 0x0CF2;
        public static UInt32 GL_UNPACK_SKIP_ROWS_EXT = 0x0CF3;
        public static UInt32 GL_UNPACK_SKIP_PIXELS_EXT = 0x0CF4;
        #endregion

        #region Commands
        #endregion
    }
}
