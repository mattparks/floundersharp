using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_draw_buffers_indexed
    {
        #region Interop
        static GL_EXT_draw_buffers_indexed()
        {
            Console.WriteLine("Initalising GL_EXT_draw_buffers_indexed interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadEnableiEXT();
            loadDisableiEXT();
            loadBlendEquationiEXT();
            loadBlendEquationSeparateiEXT();
            loadBlendFunciEXT();
            loadBlendFuncSeparateiEXT();
            loadColorMaskiEXT();
            loadIsEnablediEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BLEND_EQUATION_RGB = 0x8009;
        public static UInt32 GL_BLEND_EQUATION_ALPHA = 0x883D;
        public static UInt32 GL_BLEND_SRC_RGB = 0x80C9;
        public static UInt32 GL_BLEND_SRC_ALPHA = 0x80CB;
        public static UInt32 GL_BLEND_DST_RGB = 0x80C8;
        public static UInt32 GL_BLEND_DST_ALPHA = 0x80CA;
        public static UInt32 GL_COLOR_WRITEMASK = 0x0C23;
        public static UInt32 GL_BLEND = 0x0BE2;
        public static UInt32 GL_FUNC_ADD = 0x8006;
        public static UInt32 GL_FUNC_SUBTRACT = 0x800A;
        public static UInt32 GL_FUNC_REVERSE_SUBTRACT = 0x800B;
        public static UInt32 GL_MIN = 0x8007;
        public static UInt32 GL_MAX = 0x8008;
        public static UInt32 GL_ZERO = 0;
        public static UInt32 GL_ONE = 1;
        public static UInt32 GL_SRC_COLOR = 0x0300;
        public static UInt32 GL_ONE_MINUS_SRC_COLOR = 0x0301;
        public static UInt32 GL_DST_COLOR = 0x0306;
        public static UInt32 GL_ONE_MINUS_DST_COLOR = 0x0307;
        public static UInt32 GL_SRC_ALPHA = 0x0302;
        public static UInt32 GL_ONE_MINUS_SRC_ALPHA = 0x0303;
        public static UInt32 GL_DST_ALPHA = 0x0304;
        public static UInt32 GL_ONE_MINUS_DST_ALPHA = 0x0305;
        public static UInt32 GL_CONSTANT_COLOR = 0x8001;
        public static UInt32 GL_ONE_MINUS_CONSTANT_COLOR = 0x8002;
        public static UInt32 GL_CONSTANT_ALPHA = 0x8003;
        public static UInt32 GL_ONE_MINUS_CONSTANT_ALPHA = 0x8004;
        public static UInt32 GL_SRC_ALPHA_SATURATE = 0x0308;
        #endregion

        #region Commands
        internal delegate void glEnableiEXTFunc(GLenum @target, GLuint @index);
        internal static glEnableiEXTFunc glEnableiEXTPtr;
        internal static void loadEnableiEXT()
        {
            try
            {
                glEnableiEXTPtr = (glEnableiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableiEXT"), typeof(glEnableiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableiEXT'.");
            }
        }
        public static void glEnableiEXT(GLenum @target, GLuint @index) => glEnableiEXTPtr(@target, @index);

        internal delegate void glDisableiEXTFunc(GLenum @target, GLuint @index);
        internal static glDisableiEXTFunc glDisableiEXTPtr;
        internal static void loadDisableiEXT()
        {
            try
            {
                glDisableiEXTPtr = (glDisableiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableiEXT"), typeof(glDisableiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableiEXT'.");
            }
        }
        public static void glDisableiEXT(GLenum @target, GLuint @index) => glDisableiEXTPtr(@target, @index);

        internal delegate void glBlendEquationiEXTFunc(GLuint @buf, GLenum @mode);
        internal static glBlendEquationiEXTFunc glBlendEquationiEXTPtr;
        internal static void loadBlendEquationiEXT()
        {
            try
            {
                glBlendEquationiEXTPtr = (glBlendEquationiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationiEXT"), typeof(glBlendEquationiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationiEXT'.");
            }
        }
        public static void glBlendEquationiEXT(GLuint @buf, GLenum @mode) => glBlendEquationiEXTPtr(@buf, @mode);

        internal delegate void glBlendEquationSeparateiEXTFunc(GLuint @buf, GLenum @modeRGB, GLenum @modeAlpha);
        internal static glBlendEquationSeparateiEXTFunc glBlendEquationSeparateiEXTPtr;
        internal static void loadBlendEquationSeparateiEXT()
        {
            try
            {
                glBlendEquationSeparateiEXTPtr = (glBlendEquationSeparateiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationSeparateiEXT"), typeof(glBlendEquationSeparateiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationSeparateiEXT'.");
            }
        }
        public static void glBlendEquationSeparateiEXT(GLuint @buf, GLenum @modeRGB, GLenum @modeAlpha) => glBlendEquationSeparateiEXTPtr(@buf, @modeRGB, @modeAlpha);

        internal delegate void glBlendFunciEXTFunc(GLuint @buf, GLenum @src, GLenum @dst);
        internal static glBlendFunciEXTFunc glBlendFunciEXTPtr;
        internal static void loadBlendFunciEXT()
        {
            try
            {
                glBlendFunciEXTPtr = (glBlendFunciEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFunciEXT"), typeof(glBlendFunciEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFunciEXT'.");
            }
        }
        public static void glBlendFunciEXT(GLuint @buf, GLenum @src, GLenum @dst) => glBlendFunciEXTPtr(@buf, @src, @dst);

        internal delegate void glBlendFuncSeparateiEXTFunc(GLuint @buf, GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha);
        internal static glBlendFuncSeparateiEXTFunc glBlendFuncSeparateiEXTPtr;
        internal static void loadBlendFuncSeparateiEXT()
        {
            try
            {
                glBlendFuncSeparateiEXTPtr = (glBlendFuncSeparateiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFuncSeparateiEXT"), typeof(glBlendFuncSeparateiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFuncSeparateiEXT'.");
            }
        }
        public static void glBlendFuncSeparateiEXT(GLuint @buf, GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha) => glBlendFuncSeparateiEXTPtr(@buf, @srcRGB, @dstRGB, @srcAlpha, @dstAlpha);

        internal delegate void glColorMaskiEXTFunc(GLuint @index, GLboolean @r, GLboolean @g, GLboolean @b, GLboolean @a);
        internal static glColorMaskiEXTFunc glColorMaskiEXTPtr;
        internal static void loadColorMaskiEXT()
        {
            try
            {
                glColorMaskiEXTPtr = (glColorMaskiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorMaskiEXT"), typeof(glColorMaskiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorMaskiEXT'.");
            }
        }
        public static void glColorMaskiEXT(GLuint @index, GLboolean @r, GLboolean @g, GLboolean @b, GLboolean @a) => glColorMaskiEXTPtr(@index, @r, @g, @b, @a);

        internal delegate GLboolean glIsEnablediEXTFunc(GLenum @target, GLuint @index);
        internal static glIsEnablediEXTFunc glIsEnablediEXTPtr;
        internal static void loadIsEnablediEXT()
        {
            try
            {
                glIsEnablediEXTPtr = (glIsEnablediEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsEnablediEXT"), typeof(glIsEnablediEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsEnablediEXT'.");
            }
        }
        public static GLboolean glIsEnablediEXT(GLenum @target, GLuint @index) => glIsEnablediEXTPtr(@target, @index);
        #endregion
    }
}
