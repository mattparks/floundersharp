using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_swizzle
    {
        #region Interop
        static GL_EXT_texture_swizzle()
        {
            Console.WriteLine("Initalising GL_EXT_texture_swizzle interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_SWIZZLE_R_EXT = 0x8E42;
        public static UInt32 GL_TEXTURE_SWIZZLE_G_EXT = 0x8E43;
        public static UInt32 GL_TEXTURE_SWIZZLE_B_EXT = 0x8E44;
        public static UInt32 GL_TEXTURE_SWIZZLE_A_EXT = 0x8E45;
        public static UInt32 GL_TEXTURE_SWIZZLE_RGBA_EXT = 0x8E46;
        #endregion

        #region Commands
        #endregion
    }
}
