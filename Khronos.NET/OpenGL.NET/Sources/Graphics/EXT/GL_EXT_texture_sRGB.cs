using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_sRGB
    {
        #region Interop
        static GL_EXT_texture_sRGB()
        {
            Console.WriteLine("Initalising GL_EXT_texture_sRGB interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SRGB_EXT = 0x8C40;
        public static UInt32 GL_SRGB8_EXT = 0x8C41;
        public static UInt32 GL_SRGB_ALPHA_EXT = 0x8C42;
        public static UInt32 GL_SRGB8_ALPHA8_EXT = 0x8C43;
        public static UInt32 GL_SLUMINANCE_ALPHA_EXT = 0x8C44;
        public static UInt32 GL_SLUMINANCE8_ALPHA8_EXT = 0x8C45;
        public static UInt32 GL_SLUMINANCE_EXT = 0x8C46;
        public static UInt32 GL_SLUMINANCE8_EXT = 0x8C47;
        public static UInt32 GL_COMPRESSED_SRGB_EXT = 0x8C48;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA_EXT = 0x8C49;
        public static UInt32 GL_COMPRESSED_SLUMINANCE_EXT = 0x8C4A;
        public static UInt32 GL_COMPRESSED_SLUMINANCE_ALPHA_EXT = 0x8C4B;
        public static UInt32 GL_COMPRESSED_SRGB_S3TC_DXT1_EXT = 0x8C4C;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT = 0x8C4D;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT = 0x8C4E;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT = 0x8C4F;
        #endregion

        #region Commands
        #endregion
    }
}
