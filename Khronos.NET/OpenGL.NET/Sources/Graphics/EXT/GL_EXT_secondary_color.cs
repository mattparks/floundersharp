using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_secondary_color
    {
        #region Interop
        static GL_EXT_secondary_color()
        {
            Console.WriteLine("Initalising GL_EXT_secondary_color interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadSecondaryColor3bEXT();
            loadSecondaryColor3bvEXT();
            loadSecondaryColor3dEXT();
            loadSecondaryColor3dvEXT();
            loadSecondaryColor3fEXT();
            loadSecondaryColor3fvEXT();
            loadSecondaryColor3iEXT();
            loadSecondaryColor3ivEXT();
            loadSecondaryColor3sEXT();
            loadSecondaryColor3svEXT();
            loadSecondaryColor3ubEXT();
            loadSecondaryColor3ubvEXT();
            loadSecondaryColor3uiEXT();
            loadSecondaryColor3uivEXT();
            loadSecondaryColor3usEXT();
            loadSecondaryColor3usvEXT();
            loadSecondaryColorPointerEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COLOR_SUM_EXT = 0x8458;
        public static UInt32 GL_CURRENT_SECONDARY_COLOR_EXT = 0x8459;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_SIZE_EXT = 0x845A;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_TYPE_EXT = 0x845B;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_STRIDE_EXT = 0x845C;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_POINTER_EXT = 0x845D;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_EXT = 0x845E;
        #endregion

        #region Commands
        internal delegate void glSecondaryColor3bEXTFunc(GLbyte @red, GLbyte @green, GLbyte @blue);
        internal static glSecondaryColor3bEXTFunc glSecondaryColor3bEXTPtr;
        internal static void loadSecondaryColor3bEXT()
        {
            try
            {
                glSecondaryColor3bEXTPtr = (glSecondaryColor3bEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3bEXT"), typeof(glSecondaryColor3bEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3bEXT'.");
            }
        }
        public static void glSecondaryColor3bEXT(GLbyte @red, GLbyte @green, GLbyte @blue) => glSecondaryColor3bEXTPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3bvEXTFunc(const GLbyte * @v);
        internal static glSecondaryColor3bvEXTFunc glSecondaryColor3bvEXTPtr;
        internal static void loadSecondaryColor3bvEXT()
        {
            try
            {
                glSecondaryColor3bvEXTPtr = (glSecondaryColor3bvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3bvEXT"), typeof(glSecondaryColor3bvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3bvEXT'.");
            }
        }
        public static void glSecondaryColor3bvEXT(const GLbyte * @v) => glSecondaryColor3bvEXTPtr(@v);

        internal delegate void glSecondaryColor3dEXTFunc(GLdouble @red, GLdouble @green, GLdouble @blue);
        internal static glSecondaryColor3dEXTFunc glSecondaryColor3dEXTPtr;
        internal static void loadSecondaryColor3dEXT()
        {
            try
            {
                glSecondaryColor3dEXTPtr = (glSecondaryColor3dEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3dEXT"), typeof(glSecondaryColor3dEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3dEXT'.");
            }
        }
        public static void glSecondaryColor3dEXT(GLdouble @red, GLdouble @green, GLdouble @blue) => glSecondaryColor3dEXTPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3dvEXTFunc(const GLdouble * @v);
        internal static glSecondaryColor3dvEXTFunc glSecondaryColor3dvEXTPtr;
        internal static void loadSecondaryColor3dvEXT()
        {
            try
            {
                glSecondaryColor3dvEXTPtr = (glSecondaryColor3dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3dvEXT"), typeof(glSecondaryColor3dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3dvEXT'.");
            }
        }
        public static void glSecondaryColor3dvEXT(const GLdouble * @v) => glSecondaryColor3dvEXTPtr(@v);

        internal delegate void glSecondaryColor3fEXTFunc(GLfloat @red, GLfloat @green, GLfloat @blue);
        internal static glSecondaryColor3fEXTFunc glSecondaryColor3fEXTPtr;
        internal static void loadSecondaryColor3fEXT()
        {
            try
            {
                glSecondaryColor3fEXTPtr = (glSecondaryColor3fEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3fEXT"), typeof(glSecondaryColor3fEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3fEXT'.");
            }
        }
        public static void glSecondaryColor3fEXT(GLfloat @red, GLfloat @green, GLfloat @blue) => glSecondaryColor3fEXTPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3fvEXTFunc(const GLfloat * @v);
        internal static glSecondaryColor3fvEXTFunc glSecondaryColor3fvEXTPtr;
        internal static void loadSecondaryColor3fvEXT()
        {
            try
            {
                glSecondaryColor3fvEXTPtr = (glSecondaryColor3fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3fvEXT"), typeof(glSecondaryColor3fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3fvEXT'.");
            }
        }
        public static void glSecondaryColor3fvEXT(const GLfloat * @v) => glSecondaryColor3fvEXTPtr(@v);

        internal delegate void glSecondaryColor3iEXTFunc(GLint @red, GLint @green, GLint @blue);
        internal static glSecondaryColor3iEXTFunc glSecondaryColor3iEXTPtr;
        internal static void loadSecondaryColor3iEXT()
        {
            try
            {
                glSecondaryColor3iEXTPtr = (glSecondaryColor3iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3iEXT"), typeof(glSecondaryColor3iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3iEXT'.");
            }
        }
        public static void glSecondaryColor3iEXT(GLint @red, GLint @green, GLint @blue) => glSecondaryColor3iEXTPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3ivEXTFunc(const GLint * @v);
        internal static glSecondaryColor3ivEXTFunc glSecondaryColor3ivEXTPtr;
        internal static void loadSecondaryColor3ivEXT()
        {
            try
            {
                glSecondaryColor3ivEXTPtr = (glSecondaryColor3ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3ivEXT"), typeof(glSecondaryColor3ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3ivEXT'.");
            }
        }
        public static void glSecondaryColor3ivEXT(const GLint * @v) => glSecondaryColor3ivEXTPtr(@v);

        internal delegate void glSecondaryColor3sEXTFunc(GLshort @red, GLshort @green, GLshort @blue);
        internal static glSecondaryColor3sEXTFunc glSecondaryColor3sEXTPtr;
        internal static void loadSecondaryColor3sEXT()
        {
            try
            {
                glSecondaryColor3sEXTPtr = (glSecondaryColor3sEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3sEXT"), typeof(glSecondaryColor3sEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3sEXT'.");
            }
        }
        public static void glSecondaryColor3sEXT(GLshort @red, GLshort @green, GLshort @blue) => glSecondaryColor3sEXTPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3svEXTFunc(const GLshort * @v);
        internal static glSecondaryColor3svEXTFunc glSecondaryColor3svEXTPtr;
        internal static void loadSecondaryColor3svEXT()
        {
            try
            {
                glSecondaryColor3svEXTPtr = (glSecondaryColor3svEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3svEXT"), typeof(glSecondaryColor3svEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3svEXT'.");
            }
        }
        public static void glSecondaryColor3svEXT(const GLshort * @v) => glSecondaryColor3svEXTPtr(@v);

        internal delegate void glSecondaryColor3ubEXTFunc(GLubyte @red, GLubyte @green, GLubyte @blue);
        internal static glSecondaryColor3ubEXTFunc glSecondaryColor3ubEXTPtr;
        internal static void loadSecondaryColor3ubEXT()
        {
            try
            {
                glSecondaryColor3ubEXTPtr = (glSecondaryColor3ubEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3ubEXT"), typeof(glSecondaryColor3ubEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3ubEXT'.");
            }
        }
        public static void glSecondaryColor3ubEXT(GLubyte @red, GLubyte @green, GLubyte @blue) => glSecondaryColor3ubEXTPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3ubvEXTFunc(const GLubyte * @v);
        internal static glSecondaryColor3ubvEXTFunc glSecondaryColor3ubvEXTPtr;
        internal static void loadSecondaryColor3ubvEXT()
        {
            try
            {
                glSecondaryColor3ubvEXTPtr = (glSecondaryColor3ubvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3ubvEXT"), typeof(glSecondaryColor3ubvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3ubvEXT'.");
            }
        }
        public static void glSecondaryColor3ubvEXT(const GLubyte * @v) => glSecondaryColor3ubvEXTPtr(@v);

        internal delegate void glSecondaryColor3uiEXTFunc(GLuint @red, GLuint @green, GLuint @blue);
        internal static glSecondaryColor3uiEXTFunc glSecondaryColor3uiEXTPtr;
        internal static void loadSecondaryColor3uiEXT()
        {
            try
            {
                glSecondaryColor3uiEXTPtr = (glSecondaryColor3uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3uiEXT"), typeof(glSecondaryColor3uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3uiEXT'.");
            }
        }
        public static void glSecondaryColor3uiEXT(GLuint @red, GLuint @green, GLuint @blue) => glSecondaryColor3uiEXTPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3uivEXTFunc(const GLuint * @v);
        internal static glSecondaryColor3uivEXTFunc glSecondaryColor3uivEXTPtr;
        internal static void loadSecondaryColor3uivEXT()
        {
            try
            {
                glSecondaryColor3uivEXTPtr = (glSecondaryColor3uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3uivEXT"), typeof(glSecondaryColor3uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3uivEXT'.");
            }
        }
        public static void glSecondaryColor3uivEXT(const GLuint * @v) => glSecondaryColor3uivEXTPtr(@v);

        internal delegate void glSecondaryColor3usEXTFunc(GLushort @red, GLushort @green, GLushort @blue);
        internal static glSecondaryColor3usEXTFunc glSecondaryColor3usEXTPtr;
        internal static void loadSecondaryColor3usEXT()
        {
            try
            {
                glSecondaryColor3usEXTPtr = (glSecondaryColor3usEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3usEXT"), typeof(glSecondaryColor3usEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3usEXT'.");
            }
        }
        public static void glSecondaryColor3usEXT(GLushort @red, GLushort @green, GLushort @blue) => glSecondaryColor3usEXTPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3usvEXTFunc(const GLushort * @v);
        internal static glSecondaryColor3usvEXTFunc glSecondaryColor3usvEXTPtr;
        internal static void loadSecondaryColor3usvEXT()
        {
            try
            {
                glSecondaryColor3usvEXTPtr = (glSecondaryColor3usvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3usvEXT"), typeof(glSecondaryColor3usvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3usvEXT'.");
            }
        }
        public static void glSecondaryColor3usvEXT(const GLushort * @v) => glSecondaryColor3usvEXTPtr(@v);

        internal delegate void glSecondaryColorPointerEXTFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glSecondaryColorPointerEXTFunc glSecondaryColorPointerEXTPtr;
        internal static void loadSecondaryColorPointerEXT()
        {
            try
            {
                glSecondaryColorPointerEXTPtr = (glSecondaryColorPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColorPointerEXT"), typeof(glSecondaryColorPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColorPointerEXT'.");
            }
        }
        public static void glSecondaryColorPointerEXT(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glSecondaryColorPointerEXTPtr(@size, @type, @stride, @pointer);
        #endregion
    }
}
