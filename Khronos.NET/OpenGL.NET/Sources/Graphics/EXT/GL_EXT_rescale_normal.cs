using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_rescale_normal
    {
        #region Interop
        static GL_EXT_rescale_normal()
        {
            Console.WriteLine("Initalising GL_EXT_rescale_normal interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RESCALE_NORMAL_EXT = 0x803A;
        #endregion

        #region Commands
        #endregion
    }
}
