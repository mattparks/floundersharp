using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_multisample
    {
        #region Interop
        static GL_EXT_multisample()
        {
            Console.WriteLine("Initalising GL_EXT_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadSampleMaskEXT();
            loadSamplePatternEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MULTISAMPLE_EXT = 0x809D;
        public static UInt32 GL_SAMPLE_ALPHA_TO_MASK_EXT = 0x809E;
        public static UInt32 GL_SAMPLE_ALPHA_TO_ONE_EXT = 0x809F;
        public static UInt32 GL_SAMPLE_MASK_EXT = 0x80A0;
        public static UInt32 GL_1PASS_EXT = 0x80A1;
        public static UInt32 GL_2PASS_0_EXT = 0x80A2;
        public static UInt32 GL_2PASS_1_EXT = 0x80A3;
        public static UInt32 GL_4PASS_0_EXT = 0x80A4;
        public static UInt32 GL_4PASS_1_EXT = 0x80A5;
        public static UInt32 GL_4PASS_2_EXT = 0x80A6;
        public static UInt32 GL_4PASS_3_EXT = 0x80A7;
        public static UInt32 GL_SAMPLE_BUFFERS_EXT = 0x80A8;
        public static UInt32 GL_SAMPLES_EXT = 0x80A9;
        public static UInt32 GL_SAMPLE_MASK_VALUE_EXT = 0x80AA;
        public static UInt32 GL_SAMPLE_MASK_INVERT_EXT = 0x80AB;
        public static UInt32 GL_SAMPLE_PATTERN_EXT = 0x80AC;
        public static UInt32 GL_MULTISAMPLE_BIT_EXT = 0x20000000;
        #endregion

        #region Commands
        internal delegate void glSampleMaskEXTFunc(GLclampf @value, GLboolean @invert);
        internal static glSampleMaskEXTFunc glSampleMaskEXTPtr;
        internal static void loadSampleMaskEXT()
        {
            try
            {
                glSampleMaskEXTPtr = (glSampleMaskEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleMaskEXT"), typeof(glSampleMaskEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleMaskEXT'.");
            }
        }
        public static void glSampleMaskEXT(GLclampf @value, GLboolean @invert) => glSampleMaskEXTPtr(@value, @invert);

        internal delegate void glSamplePatternEXTFunc(GLenum @pattern);
        internal static glSamplePatternEXTFunc glSamplePatternEXTPtr;
        internal static void loadSamplePatternEXT()
        {
            try
            {
                glSamplePatternEXTPtr = (glSamplePatternEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplePatternEXT"), typeof(glSamplePatternEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplePatternEXT'.");
            }
        }
        public static void glSamplePatternEXT(GLenum @pattern) => glSamplePatternEXTPtr(@pattern);
        #endregion
    }
}
