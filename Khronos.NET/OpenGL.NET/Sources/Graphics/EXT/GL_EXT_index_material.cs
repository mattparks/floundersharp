using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_index_material
    {
        #region Interop
        static GL_EXT_index_material()
        {
            Console.WriteLine("Initalising GL_EXT_index_material interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadIndexMaterialEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_INDEX_MATERIAL_EXT = 0x81B8;
        public static UInt32 GL_INDEX_MATERIAL_PARAMETER_EXT = 0x81B9;
        public static UInt32 GL_INDEX_MATERIAL_FACE_EXT = 0x81BA;
        #endregion

        #region Commands
        internal delegate void glIndexMaterialEXTFunc(GLenum @face, GLenum @mode);
        internal static glIndexMaterialEXTFunc glIndexMaterialEXTPtr;
        internal static void loadIndexMaterialEXT()
        {
            try
            {
                glIndexMaterialEXTPtr = (glIndexMaterialEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexMaterialEXT"), typeof(glIndexMaterialEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexMaterialEXT'.");
            }
        }
        public static void glIndexMaterialEXT(GLenum @face, GLenum @mode) => glIndexMaterialEXTPtr(@face, @mode);
        #endregion
    }
}
