using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_filter_anisotropic
    {
        #region Interop
        static GL_EXT_texture_filter_anisotropic()
        {
            Console.WriteLine("Initalising GL_EXT_texture_filter_anisotropic interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_MAX_ANISOTROPY_EXT = 0x84FE;
        public static UInt32 GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT = 0x84FF;
        #endregion

        #region Commands
        #endregion
    }
}
