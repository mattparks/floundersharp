using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_blend_subtract
    {
        #region Interop
        static GL_EXT_blend_subtract()
        {
            Console.WriteLine("Initalising GL_EXT_blend_subtract interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FUNC_SUBTRACT_EXT = 0x800A;
        public static UInt32 GL_FUNC_REVERSE_SUBTRACT_EXT = 0x800B;
        #endregion

        #region Commands
        #endregion
    }
}
