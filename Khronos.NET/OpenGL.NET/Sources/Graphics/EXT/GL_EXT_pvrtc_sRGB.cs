using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_pvrtc_sRGB
    {
        #region Interop
        static GL_EXT_pvrtc_sRGB()
        {
            Console.WriteLine("Initalising GL_EXT_pvrtc_sRGB interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_SRGB_PVRTC_2BPPV1_EXT = 0x8A54;
        public static UInt32 GL_COMPRESSED_SRGB_PVRTC_4BPPV1_EXT = 0x8A55;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA_PVRTC_2BPPV1_EXT = 0x8A56;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA_PVRTC_4BPPV1_EXT = 0x8A57;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA_PVRTC_2BPPV2_IMG = 0x93F0;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA_PVRTC_4BPPV2_IMG = 0x93F1;
        #endregion

        #region Commands
        #endregion
    }
}
