using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_pixel_buffer_object
    {
        #region Interop
        static GL_EXT_pixel_buffer_object()
        {
            Console.WriteLine("Initalising GL_EXT_pixel_buffer_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_PIXEL_PACK_BUFFER_EXT = 0x88EB;
        public static UInt32 GL_PIXEL_UNPACK_BUFFER_EXT = 0x88EC;
        public static UInt32 GL_PIXEL_PACK_BUFFER_BINDING_EXT = 0x88ED;
        public static UInt32 GL_PIXEL_UNPACK_BUFFER_BINDING_EXT = 0x88EF;
        #endregion

        #region Commands
        #endregion
    }
}
