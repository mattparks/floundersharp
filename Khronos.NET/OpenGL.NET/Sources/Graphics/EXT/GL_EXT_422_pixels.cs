using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_422_pixels
    {
        #region Interop
        static GL_EXT_422_pixels()
        {
            Console.WriteLine("Initalising GL_EXT_422_pixels interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_422_EXT = 0x80CC;
        public static UInt32 GL_422_REV_EXT = 0x80CD;
        public static UInt32 GL_422_AVERAGE_EXT = 0x80CE;
        public static UInt32 GL_422_REV_AVERAGE_EXT = 0x80CF;
        #endregion

        #region Commands
        #endregion
    }
}
