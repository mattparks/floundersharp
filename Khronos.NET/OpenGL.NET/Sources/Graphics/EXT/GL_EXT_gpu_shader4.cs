using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_gpu_shader4
    {
        #region Interop
        static GL_EXT_gpu_shader4()
        {
            Console.WriteLine("Initalising GL_EXT_gpu_shader4 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetUniformuivEXT();
            loadBindFragDataLocationEXT();
            loadGetFragDataLocationEXT();
            loadUniform1uiEXT();
            loadUniform2uiEXT();
            loadUniform3uiEXT();
            loadUniform4uiEXT();
            loadUniform1uivEXT();
            loadUniform2uivEXT();
            loadUniform3uivEXT();
            loadUniform4uivEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_INTEGER_EXT = 0x88FD;
        public static UInt32 GL_SAMPLER_1D_ARRAY_EXT = 0x8DC0;
        public static UInt32 GL_SAMPLER_2D_ARRAY_EXT = 0x8DC1;
        public static UInt32 GL_SAMPLER_BUFFER_EXT = 0x8DC2;
        public static UInt32 GL_SAMPLER_1D_ARRAY_SHADOW_EXT = 0x8DC3;
        public static UInt32 GL_SAMPLER_2D_ARRAY_SHADOW_EXT = 0x8DC4;
        public static UInt32 GL_SAMPLER_CUBE_SHADOW_EXT = 0x8DC5;
        public static UInt32 GL_UNSIGNED_INT_VEC2_EXT = 0x8DC6;
        public static UInt32 GL_UNSIGNED_INT_VEC3_EXT = 0x8DC7;
        public static UInt32 GL_UNSIGNED_INT_VEC4_EXT = 0x8DC8;
        public static UInt32 GL_INT_SAMPLER_1D_EXT = 0x8DC9;
        public static UInt32 GL_INT_SAMPLER_2D_EXT = 0x8DCA;
        public static UInt32 GL_INT_SAMPLER_3D_EXT = 0x8DCB;
        public static UInt32 GL_INT_SAMPLER_CUBE_EXT = 0x8DCC;
        public static UInt32 GL_INT_SAMPLER_2D_RECT_EXT = 0x8DCD;
        public static UInt32 GL_INT_SAMPLER_1D_ARRAY_EXT = 0x8DCE;
        public static UInt32 GL_INT_SAMPLER_2D_ARRAY_EXT = 0x8DCF;
        public static UInt32 GL_INT_SAMPLER_BUFFER_EXT = 0x8DD0;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_1D_EXT = 0x8DD1;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_EXT = 0x8DD2;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_3D_EXT = 0x8DD3;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_CUBE_EXT = 0x8DD4;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_RECT_EXT = 0x8DD5;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_1D_ARRAY_EXT = 0x8DD6;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_ARRAY_EXT = 0x8DD7;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_BUFFER_EXT = 0x8DD8;
        public static UInt32 GL_MIN_PROGRAM_TEXEL_OFFSET_EXT = 0x8904;
        public static UInt32 GL_MAX_PROGRAM_TEXEL_OFFSET_EXT = 0x8905;
        #endregion

        #region Commands
        internal delegate void glGetUniformuivEXTFunc(GLuint @program, GLint @location, GLuint * @params);
        internal static glGetUniformuivEXTFunc glGetUniformuivEXTPtr;
        internal static void loadGetUniformuivEXT()
        {
            try
            {
                glGetUniformuivEXTPtr = (glGetUniformuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformuivEXT"), typeof(glGetUniformuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformuivEXT'.");
            }
        }
        public static void glGetUniformuivEXT(GLuint @program, GLint @location, GLuint * @params) => glGetUniformuivEXTPtr(@program, @location, @params);

        internal delegate void glBindFragDataLocationEXTFunc(GLuint @program, GLuint @color, const GLchar * @name);
        internal static glBindFragDataLocationEXTFunc glBindFragDataLocationEXTPtr;
        internal static void loadBindFragDataLocationEXT()
        {
            try
            {
                glBindFragDataLocationEXTPtr = (glBindFragDataLocationEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindFragDataLocationEXT"), typeof(glBindFragDataLocationEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindFragDataLocationEXT'.");
            }
        }
        public static void glBindFragDataLocationEXT(GLuint @program, GLuint @color, const GLchar * @name) => glBindFragDataLocationEXTPtr(@program, @color, @name);

        internal delegate GLint glGetFragDataLocationEXTFunc(GLuint @program, const GLchar * @name);
        internal static glGetFragDataLocationEXTFunc glGetFragDataLocationEXTPtr;
        internal static void loadGetFragDataLocationEXT()
        {
            try
            {
                glGetFragDataLocationEXTPtr = (glGetFragDataLocationEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFragDataLocationEXT"), typeof(glGetFragDataLocationEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFragDataLocationEXT'.");
            }
        }
        public static GLint glGetFragDataLocationEXT(GLuint @program, const GLchar * @name) => glGetFragDataLocationEXTPtr(@program, @name);

        internal delegate void glUniform1uiEXTFunc(GLint @location, GLuint @v0);
        internal static glUniform1uiEXTFunc glUniform1uiEXTPtr;
        internal static void loadUniform1uiEXT()
        {
            try
            {
                glUniform1uiEXTPtr = (glUniform1uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1uiEXT"), typeof(glUniform1uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1uiEXT'.");
            }
        }
        public static void glUniform1uiEXT(GLint @location, GLuint @v0) => glUniform1uiEXTPtr(@location, @v0);

        internal delegate void glUniform2uiEXTFunc(GLint @location, GLuint @v0, GLuint @v1);
        internal static glUniform2uiEXTFunc glUniform2uiEXTPtr;
        internal static void loadUniform2uiEXT()
        {
            try
            {
                glUniform2uiEXTPtr = (glUniform2uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2uiEXT"), typeof(glUniform2uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2uiEXT'.");
            }
        }
        public static void glUniform2uiEXT(GLint @location, GLuint @v0, GLuint @v1) => glUniform2uiEXTPtr(@location, @v0, @v1);

        internal delegate void glUniform3uiEXTFunc(GLint @location, GLuint @v0, GLuint @v1, GLuint @v2);
        internal static glUniform3uiEXTFunc glUniform3uiEXTPtr;
        internal static void loadUniform3uiEXT()
        {
            try
            {
                glUniform3uiEXTPtr = (glUniform3uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3uiEXT"), typeof(glUniform3uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3uiEXT'.");
            }
        }
        public static void glUniform3uiEXT(GLint @location, GLuint @v0, GLuint @v1, GLuint @v2) => glUniform3uiEXTPtr(@location, @v0, @v1, @v2);

        internal delegate void glUniform4uiEXTFunc(GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3);
        internal static glUniform4uiEXTFunc glUniform4uiEXTPtr;
        internal static void loadUniform4uiEXT()
        {
            try
            {
                glUniform4uiEXTPtr = (glUniform4uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4uiEXT"), typeof(glUniform4uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4uiEXT'.");
            }
        }
        public static void glUniform4uiEXT(GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3) => glUniform4uiEXTPtr(@location, @v0, @v1, @v2, @v3);

        internal delegate void glUniform1uivEXTFunc(GLint @location, GLsizei @count, const GLuint * @value);
        internal static glUniform1uivEXTFunc glUniform1uivEXTPtr;
        internal static void loadUniform1uivEXT()
        {
            try
            {
                glUniform1uivEXTPtr = (glUniform1uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1uivEXT"), typeof(glUniform1uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1uivEXT'.");
            }
        }
        public static void glUniform1uivEXT(GLint @location, GLsizei @count, const GLuint * @value) => glUniform1uivEXTPtr(@location, @count, @value);

        internal delegate void glUniform2uivEXTFunc(GLint @location, GLsizei @count, const GLuint * @value);
        internal static glUniform2uivEXTFunc glUniform2uivEXTPtr;
        internal static void loadUniform2uivEXT()
        {
            try
            {
                glUniform2uivEXTPtr = (glUniform2uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2uivEXT"), typeof(glUniform2uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2uivEXT'.");
            }
        }
        public static void glUniform2uivEXT(GLint @location, GLsizei @count, const GLuint * @value) => glUniform2uivEXTPtr(@location, @count, @value);

        internal delegate void glUniform3uivEXTFunc(GLint @location, GLsizei @count, const GLuint * @value);
        internal static glUniform3uivEXTFunc glUniform3uivEXTPtr;
        internal static void loadUniform3uivEXT()
        {
            try
            {
                glUniform3uivEXTPtr = (glUniform3uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3uivEXT"), typeof(glUniform3uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3uivEXT'.");
            }
        }
        public static void glUniform3uivEXT(GLint @location, GLsizei @count, const GLuint * @value) => glUniform3uivEXTPtr(@location, @count, @value);

        internal delegate void glUniform4uivEXTFunc(GLint @location, GLsizei @count, const GLuint * @value);
        internal static glUniform4uivEXTFunc glUniform4uivEXTPtr;
        internal static void loadUniform4uivEXT()
        {
            try
            {
                glUniform4uivEXTPtr = (glUniform4uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4uivEXT"), typeof(glUniform4uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4uivEXT'.");
            }
        }
        public static void glUniform4uivEXT(GLint @location, GLsizei @count, const GLuint * @value) => glUniform4uivEXTPtr(@location, @count, @value);
        #endregion
    }
}
