using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture
    {
        #region Interop
        static GL_EXT_texture()
        {
            Console.WriteLine("Initalising GL_EXT_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_ALPHA4_EXT = 0x803B;
        public static UInt32 GL_ALPHA8_EXT = 0x803C;
        public static UInt32 GL_ALPHA12_EXT = 0x803D;
        public static UInt32 GL_ALPHA16_EXT = 0x803E;
        public static UInt32 GL_LUMINANCE4_EXT = 0x803F;
        public static UInt32 GL_LUMINANCE8_EXT = 0x8040;
        public static UInt32 GL_LUMINANCE12_EXT = 0x8041;
        public static UInt32 GL_LUMINANCE16_EXT = 0x8042;
        public static UInt32 GL_LUMINANCE4_ALPHA4_EXT = 0x8043;
        public static UInt32 GL_LUMINANCE6_ALPHA2_EXT = 0x8044;
        public static UInt32 GL_LUMINANCE8_ALPHA8_EXT = 0x8045;
        public static UInt32 GL_LUMINANCE12_ALPHA4_EXT = 0x8046;
        public static UInt32 GL_LUMINANCE12_ALPHA12_EXT = 0x8047;
        public static UInt32 GL_LUMINANCE16_ALPHA16_EXT = 0x8048;
        public static UInt32 GL_INTENSITY_EXT = 0x8049;
        public static UInt32 GL_INTENSITY4_EXT = 0x804A;
        public static UInt32 GL_INTENSITY8_EXT = 0x804B;
        public static UInt32 GL_INTENSITY12_EXT = 0x804C;
        public static UInt32 GL_INTENSITY16_EXT = 0x804D;
        public static UInt32 GL_RGB2_EXT = 0x804E;
        public static UInt32 GL_RGB4_EXT = 0x804F;
        public static UInt32 GL_RGB5_EXT = 0x8050;
        public static UInt32 GL_RGB8_EXT = 0x8051;
        public static UInt32 GL_RGB10_EXT = 0x8052;
        public static UInt32 GL_RGB12_EXT = 0x8053;
        public static UInt32 GL_RGB16_EXT = 0x8054;
        public static UInt32 GL_RGBA2_EXT = 0x8055;
        public static UInt32 GL_RGBA4_EXT = 0x8056;
        public static UInt32 GL_RGB5_A1_EXT = 0x8057;
        public static UInt32 GL_RGBA8_EXT = 0x8058;
        public static UInt32 GL_RGB10_A2_EXT = 0x8059;
        public static UInt32 GL_RGBA12_EXT = 0x805A;
        public static UInt32 GL_RGBA16_EXT = 0x805B;
        public static UInt32 GL_TEXTURE_RED_SIZE_EXT = 0x805C;
        public static UInt32 GL_TEXTURE_GREEN_SIZE_EXT = 0x805D;
        public static UInt32 GL_TEXTURE_BLUE_SIZE_EXT = 0x805E;
        public static UInt32 GL_TEXTURE_ALPHA_SIZE_EXT = 0x805F;
        public static UInt32 GL_TEXTURE_LUMINANCE_SIZE_EXT = 0x8060;
        public static UInt32 GL_TEXTURE_INTENSITY_SIZE_EXT = 0x8061;
        public static UInt32 GL_REPLACE_EXT = 0x8062;
        public static UInt32 GL_PROXY_TEXTURE_1D_EXT = 0x8063;
        public static UInt32 GL_PROXY_TEXTURE_2D_EXT = 0x8064;
        public static UInt32 GL_TEXTURE_TOO_LARGE_EXT = 0x8065;
        #endregion

        #region Commands
        #endregion
    }
}
