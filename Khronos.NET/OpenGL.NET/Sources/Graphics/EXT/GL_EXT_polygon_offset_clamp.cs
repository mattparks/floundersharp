using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_polygon_offset_clamp
    {
        #region Interop
        static GL_EXT_polygon_offset_clamp()
        {
            Console.WriteLine("Initalising GL_EXT_polygon_offset_clamp interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPolygonOffsetClampEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_POLYGON_OFFSET_CLAMP_EXT = 0x8E1B;
        #endregion

        #region Commands
        internal delegate void glPolygonOffsetClampEXTFunc(GLfloat @factor, GLfloat @units, GLfloat @clamp);
        internal static glPolygonOffsetClampEXTFunc glPolygonOffsetClampEXTPtr;
        internal static void loadPolygonOffsetClampEXT()
        {
            try
            {
                glPolygonOffsetClampEXTPtr = (glPolygonOffsetClampEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPolygonOffsetClampEXT"), typeof(glPolygonOffsetClampEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPolygonOffsetClampEXT'.");
            }
        }
        public static void glPolygonOffsetClampEXT(GLfloat @factor, GLfloat @units, GLfloat @clamp) => glPolygonOffsetClampEXTPtr(@factor, @units, @clamp);
        #endregion
    }
}
