using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_raster_multisample
    {
        #region Interop
        static GL_EXT_raster_multisample()
        {
            Console.WriteLine("Initalising GL_EXT_raster_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadRasterSamplesEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RASTER_MULTISAMPLE_EXT = 0x9327;
        public static UInt32 GL_RASTER_SAMPLES_EXT = 0x9328;
        public static UInt32 GL_MAX_RASTER_SAMPLES_EXT = 0x9329;
        public static UInt32 GL_RASTER_FIXED_SAMPLE_LOCATIONS_EXT = 0x932A;
        public static UInt32 GL_MULTISAMPLE_RASTERIZATION_ALLOWED_EXT = 0x932B;
        public static UInt32 GL_EFFECTIVE_RASTER_SAMPLES_EXT = 0x932C;
        #endregion

        #region Commands
        internal delegate void glRasterSamplesEXTFunc(GLuint @samples, GLboolean @fixedsamplelocations);
        internal static glRasterSamplesEXTFunc glRasterSamplesEXTPtr;
        internal static void loadRasterSamplesEXT()
        {
            try
            {
                glRasterSamplesEXTPtr = (glRasterSamplesEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterSamplesEXT"), typeof(glRasterSamplesEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterSamplesEXT'.");
            }
        }
        public static void glRasterSamplesEXT(GLuint @samples, GLboolean @fixedsamplelocations) => glRasterSamplesEXTPtr(@samples, @fixedsamplelocations);
        #endregion
    }
}
