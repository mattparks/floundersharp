using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_vertex_weighting
    {
        #region Interop
        static GL_EXT_vertex_weighting()
        {
            Console.WriteLine("Initalising GL_EXT_vertex_weighting interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexWeightfEXT();
            loadVertexWeightfvEXT();
            loadVertexWeightPointerEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MODELVIEW0_STACK_DEPTH_EXT = 0x0BA3;
        public static UInt32 GL_MODELVIEW1_STACK_DEPTH_EXT = 0x8502;
        public static UInt32 GL_MODELVIEW0_MATRIX_EXT = 0x0BA6;
        public static UInt32 GL_MODELVIEW1_MATRIX_EXT = 0x8506;
        public static UInt32 GL_VERTEX_WEIGHTING_EXT = 0x8509;
        public static UInt32 GL_MODELVIEW0_EXT = 0x1700;
        public static UInt32 GL_MODELVIEW1_EXT = 0x850A;
        public static UInt32 GL_CURRENT_VERTEX_WEIGHT_EXT = 0x850B;
        public static UInt32 GL_VERTEX_WEIGHT_ARRAY_EXT = 0x850C;
        public static UInt32 GL_VERTEX_WEIGHT_ARRAY_SIZE_EXT = 0x850D;
        public static UInt32 GL_VERTEX_WEIGHT_ARRAY_TYPE_EXT = 0x850E;
        public static UInt32 GL_VERTEX_WEIGHT_ARRAY_STRIDE_EXT = 0x850F;
        public static UInt32 GL_VERTEX_WEIGHT_ARRAY_POINTER_EXT = 0x8510;
        #endregion

        #region Commands
        internal delegate void glVertexWeightfEXTFunc(GLfloat @weight);
        internal static glVertexWeightfEXTFunc glVertexWeightfEXTPtr;
        internal static void loadVertexWeightfEXT()
        {
            try
            {
                glVertexWeightfEXTPtr = (glVertexWeightfEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexWeightfEXT"), typeof(glVertexWeightfEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexWeightfEXT'.");
            }
        }
        public static void glVertexWeightfEXT(GLfloat @weight) => glVertexWeightfEXTPtr(@weight);

        internal delegate void glVertexWeightfvEXTFunc(const GLfloat * @weight);
        internal static glVertexWeightfvEXTFunc glVertexWeightfvEXTPtr;
        internal static void loadVertexWeightfvEXT()
        {
            try
            {
                glVertexWeightfvEXTPtr = (glVertexWeightfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexWeightfvEXT"), typeof(glVertexWeightfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexWeightfvEXT'.");
            }
        }
        public static void glVertexWeightfvEXT(const GLfloat * @weight) => glVertexWeightfvEXTPtr(@weight);

        internal delegate void glVertexWeightPointerEXTFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glVertexWeightPointerEXTFunc glVertexWeightPointerEXTPtr;
        internal static void loadVertexWeightPointerEXT()
        {
            try
            {
                glVertexWeightPointerEXTPtr = (glVertexWeightPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexWeightPointerEXT"), typeof(glVertexWeightPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexWeightPointerEXT'.");
            }
        }
        public static void glVertexWeightPointerEXT(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glVertexWeightPointerEXTPtr(@size, @type, @stride, @pointer);
        #endregion
    }
}
