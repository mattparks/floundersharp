using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_sRGB_R8
    {
        #region Interop
        static GL_EXT_texture_sRGB_R8()
        {
            Console.WriteLine("Initalising GL_EXT_texture_sRGB_R8 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SR8_EXT = 0x8FBD;
        #endregion

        #region Commands
        #endregion
    }
}
