using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_light_texture
    {
        #region Interop
        static GL_EXT_light_texture()
        {
            Console.WriteLine("Initalising GL_EXT_light_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadApplyTextureEXT();
            loadTextureLightEXT();
            loadTextureMaterialEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAGMENT_MATERIAL_EXT = 0x8349;
        public static UInt32 GL_FRAGMENT_NORMAL_EXT = 0x834A;
        public static UInt32 GL_FRAGMENT_COLOR_EXT = 0x834C;
        public static UInt32 GL_ATTENUATION_EXT = 0x834D;
        public static UInt32 GL_SHADOW_ATTENUATION_EXT = 0x834E;
        public static UInt32 GL_TEXTURE_APPLICATION_MODE_EXT = 0x834F;
        public static UInt32 GL_TEXTURE_LIGHT_EXT = 0x8350;
        public static UInt32 GL_TEXTURE_MATERIAL_FACE_EXT = 0x8351;
        public static UInt32 GL_TEXTURE_MATERIAL_PARAMETER_EXT = 0x8352;
        public static UInt32 GL_FRAGMENT_DEPTH_EXT = 0x8452;
        #endregion

        #region Commands
        internal delegate void glApplyTextureEXTFunc(GLenum @mode);
        internal static glApplyTextureEXTFunc glApplyTextureEXTPtr;
        internal static void loadApplyTextureEXT()
        {
            try
            {
                glApplyTextureEXTPtr = (glApplyTextureEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glApplyTextureEXT"), typeof(glApplyTextureEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glApplyTextureEXT'.");
            }
        }
        public static void glApplyTextureEXT(GLenum @mode) => glApplyTextureEXTPtr(@mode);

        internal delegate void glTextureLightEXTFunc(GLenum @pname);
        internal static glTextureLightEXTFunc glTextureLightEXTPtr;
        internal static void loadTextureLightEXT()
        {
            try
            {
                glTextureLightEXTPtr = (glTextureLightEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureLightEXT"), typeof(glTextureLightEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureLightEXT'.");
            }
        }
        public static void glTextureLightEXT(GLenum @pname) => glTextureLightEXTPtr(@pname);

        internal delegate void glTextureMaterialEXTFunc(GLenum @face, GLenum @mode);
        internal static glTextureMaterialEXTFunc glTextureMaterialEXTPtr;
        internal static void loadTextureMaterialEXT()
        {
            try
            {
                glTextureMaterialEXTPtr = (glTextureMaterialEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureMaterialEXT"), typeof(glTextureMaterialEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureMaterialEXT'.");
            }
        }
        public static void glTextureMaterialEXT(GLenum @face, GLenum @mode) => glTextureMaterialEXTPtr(@face, @mode);
        #endregion
    }
}
