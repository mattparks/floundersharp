using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_shader_pixel_local_storage
    {
        #region Interop
        static GL_EXT_shader_pixel_local_storage()
        {
            Console.WriteLine("Initalising GL_EXT_shader_pixel_local_storage interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_SHADER_PIXEL_LOCAL_STORAGE_FAST_SIZE_EXT = 0x8F63;
        public static UInt32 GL_MAX_SHADER_PIXEL_LOCAL_STORAGE_SIZE_EXT = 0x8F67;
        public static UInt32 GL_SHADER_PIXEL_LOCAL_STORAGE_EXT = 0x8F64;
        #endregion

        #region Commands
        #endregion
    }
}
