using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_compression_latc
    {
        #region Interop
        static GL_EXT_texture_compression_latc()
        {
            Console.WriteLine("Initalising GL_EXT_texture_compression_latc interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_LUMINANCE_LATC1_EXT = 0x8C70;
        public static UInt32 GL_COMPRESSED_SIGNED_LUMINANCE_LATC1_EXT = 0x8C71;
        public static UInt32 GL_COMPRESSED_LUMINANCE_ALPHA_LATC2_EXT = 0x8C72;
        public static UInt32 GL_COMPRESSED_SIGNED_LUMINANCE_ALPHA_LATC2_EXT = 0x8C73;
        #endregion

        #region Commands
        #endregion
    }
}
