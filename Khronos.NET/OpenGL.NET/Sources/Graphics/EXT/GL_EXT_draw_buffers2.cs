using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_draw_buffers2
    {
        #region Interop
        static GL_EXT_draw_buffers2()
        {
            Console.WriteLine("Initalising GL_EXT_draw_buffers2 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadColorMaskIndexedEXT();
            loadGetBooleanIndexedvEXT();
            loadGetIntegerIndexedvEXT();
            loadEnableIndexedEXT();
            loadDisableIndexedEXT();
            loadIsEnabledIndexedEXT();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glColorMaskIndexedEXTFunc(GLuint @index, GLboolean @r, GLboolean @g, GLboolean @b, GLboolean @a);
        internal static glColorMaskIndexedEXTFunc glColorMaskIndexedEXTPtr;
        internal static void loadColorMaskIndexedEXT()
        {
            try
            {
                glColorMaskIndexedEXTPtr = (glColorMaskIndexedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorMaskIndexedEXT"), typeof(glColorMaskIndexedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorMaskIndexedEXT'.");
            }
        }
        public static void glColorMaskIndexedEXT(GLuint @index, GLboolean @r, GLboolean @g, GLboolean @b, GLboolean @a) => glColorMaskIndexedEXTPtr(@index, @r, @g, @b, @a);

        internal delegate void glGetBooleanIndexedvEXTFunc(GLenum @target, GLuint @index, GLboolean * @data);
        internal static glGetBooleanIndexedvEXTFunc glGetBooleanIndexedvEXTPtr;
        internal static void loadGetBooleanIndexedvEXT()
        {
            try
            {
                glGetBooleanIndexedvEXTPtr = (glGetBooleanIndexedvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBooleanIndexedvEXT"), typeof(glGetBooleanIndexedvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBooleanIndexedvEXT'.");
            }
        }
        public static void glGetBooleanIndexedvEXT(GLenum @target, GLuint @index, GLboolean * @data) => glGetBooleanIndexedvEXTPtr(@target, @index, @data);

        internal delegate void glGetIntegerIndexedvEXTFunc(GLenum @target, GLuint @index, GLint * @data);
        internal static glGetIntegerIndexedvEXTFunc glGetIntegerIndexedvEXTPtr;
        internal static void loadGetIntegerIndexedvEXT()
        {
            try
            {
                glGetIntegerIndexedvEXTPtr = (glGetIntegerIndexedvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetIntegerIndexedvEXT"), typeof(glGetIntegerIndexedvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetIntegerIndexedvEXT'.");
            }
        }
        public static void glGetIntegerIndexedvEXT(GLenum @target, GLuint @index, GLint * @data) => glGetIntegerIndexedvEXTPtr(@target, @index, @data);

        internal delegate void glEnableIndexedEXTFunc(GLenum @target, GLuint @index);
        internal static glEnableIndexedEXTFunc glEnableIndexedEXTPtr;
        internal static void loadEnableIndexedEXT()
        {
            try
            {
                glEnableIndexedEXTPtr = (glEnableIndexedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableIndexedEXT"), typeof(glEnableIndexedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableIndexedEXT'.");
            }
        }
        public static void glEnableIndexedEXT(GLenum @target, GLuint @index) => glEnableIndexedEXTPtr(@target, @index);

        internal delegate void glDisableIndexedEXTFunc(GLenum @target, GLuint @index);
        internal static glDisableIndexedEXTFunc glDisableIndexedEXTPtr;
        internal static void loadDisableIndexedEXT()
        {
            try
            {
                glDisableIndexedEXTPtr = (glDisableIndexedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableIndexedEXT"), typeof(glDisableIndexedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableIndexedEXT'.");
            }
        }
        public static void glDisableIndexedEXT(GLenum @target, GLuint @index) => glDisableIndexedEXTPtr(@target, @index);

        internal delegate GLboolean glIsEnabledIndexedEXTFunc(GLenum @target, GLuint @index);
        internal static glIsEnabledIndexedEXTFunc glIsEnabledIndexedEXTPtr;
        internal static void loadIsEnabledIndexedEXT()
        {
            try
            {
                glIsEnabledIndexedEXTPtr = (glIsEnabledIndexedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsEnabledIndexedEXT"), typeof(glIsEnabledIndexedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsEnabledIndexedEXT'.");
            }
        }
        public static GLboolean glIsEnabledIndexedEXT(GLenum @target, GLuint @index) => glIsEnabledIndexedEXTPtr(@target, @index);
        #endregion
    }
}
