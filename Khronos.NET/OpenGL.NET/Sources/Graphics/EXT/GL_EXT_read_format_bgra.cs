using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_read_format_bgra
    {
        #region Interop
        static GL_EXT_read_format_bgra()
        {
            Console.WriteLine("Initalising GL_EXT_read_format_bgra interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_BGRA_EXT = 0x80E1;
        public static UInt32 GL_UNSIGNED_SHORT_4_4_4_4_REV_EXT = 0x8365;
        public static UInt32 GL_UNSIGNED_SHORT_1_5_5_5_REV_EXT = 0x8366;
        #endregion

        #region Commands
        #endregion
    }
}
