using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_multi_draw_indirect
    {
        #region Interop
        static GL_EXT_multi_draw_indirect()
        {
            Console.WriteLine("Initalising GL_EXT_multi_draw_indirect interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMultiDrawArraysIndirectEXT();
            loadMultiDrawElementsIndirectEXT();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glMultiDrawArraysIndirectEXTFunc(GLenum @mode, const void * @indirect, GLsizei @drawcount, GLsizei @stride);
        internal static glMultiDrawArraysIndirectEXTFunc glMultiDrawArraysIndirectEXTPtr;
        internal static void loadMultiDrawArraysIndirectEXT()
        {
            try
            {
                glMultiDrawArraysIndirectEXTPtr = (glMultiDrawArraysIndirectEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawArraysIndirectEXT"), typeof(glMultiDrawArraysIndirectEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawArraysIndirectEXT'.");
            }
        }
        public static void glMultiDrawArraysIndirectEXT(GLenum @mode, const void * @indirect, GLsizei @drawcount, GLsizei @stride) => glMultiDrawArraysIndirectEXTPtr(@mode, @indirect, @drawcount, @stride);

        internal delegate void glMultiDrawElementsIndirectEXTFunc(GLenum @mode, GLenum @type, const void * @indirect, GLsizei @drawcount, GLsizei @stride);
        internal static glMultiDrawElementsIndirectEXTFunc glMultiDrawElementsIndirectEXTPtr;
        internal static void loadMultiDrawElementsIndirectEXT()
        {
            try
            {
                glMultiDrawElementsIndirectEXTPtr = (glMultiDrawElementsIndirectEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementsIndirectEXT"), typeof(glMultiDrawElementsIndirectEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementsIndirectEXT'.");
            }
        }
        public static void glMultiDrawElementsIndirectEXT(GLenum @mode, GLenum @type, const void * @indirect, GLsizei @drawcount, GLsizei @stride) => glMultiDrawElementsIndirectEXTPtr(@mode, @type, @indirect, @drawcount, @stride);
        #endregion
    }
}
