using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_norm16
    {
        #region Interop
        static GL_EXT_texture_norm16()
        {
            Console.WriteLine("Initalising GL_EXT_texture_norm16 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_R16_EXT = 0x822A;
        public static UInt32 GL_RG16_EXT = 0x822C;
        public static UInt32 GL_RGBA16_EXT = 0x805B;
        public static UInt32 GL_RGB16_EXT = 0x8054;
        public static UInt32 GL_R16_SNORM_EXT = 0x8F98;
        public static UInt32 GL_RG16_SNORM_EXT = 0x8F99;
        public static UInt32 GL_RGB16_SNORM_EXT = 0x8F9A;
        public static UInt32 GL_RGBA16_SNORM_EXT = 0x8F9B;
        #endregion

        #region Commands
        #endregion
    }
}
