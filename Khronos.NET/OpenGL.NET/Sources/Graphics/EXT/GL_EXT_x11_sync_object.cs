using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_x11_sync_object
    {
        #region Interop
        static GL_EXT_x11_sync_object()
        {
            Console.WriteLine("Initalising GL_EXT_x11_sync_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadImportSyncEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SYNC_X11_FENCE_EXT = 0x90E1;
        #endregion

        #region Commands
        internal delegate GLsync glImportSyncEXTFunc(GLenum @external_sync_type, GLintptr @external_sync, GLbitfield @flags);
        internal static glImportSyncEXTFunc glImportSyncEXTPtr;
        internal static void loadImportSyncEXT()
        {
            try
            {
                glImportSyncEXTPtr = (glImportSyncEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glImportSyncEXT"), typeof(glImportSyncEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glImportSyncEXT'.");
            }
        }
        public static GLsync glImportSyncEXT(GLenum @external_sync_type, GLintptr @external_sync, GLbitfield @flags) => glImportSyncEXTPtr(@external_sync_type, @external_sync, @flags);
        #endregion
    }
}
