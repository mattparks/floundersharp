using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_packed_float
    {
        #region Interop
        static GL_EXT_packed_float()
        {
            Console.WriteLine("Initalising GL_EXT_packed_float interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_R11F_G11F_B10F_EXT = 0x8C3A;
        public static UInt32 GL_UNSIGNED_INT_10F_11F_11F_REV_EXT = 0x8C3B;
        public static UInt32 GL_RGBA_SIGNED_COMPONENTS_EXT = 0x8C3C;
        #endregion

        #region Commands
        #endregion
    }
}
