using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_stencil_two_side
    {
        #region Interop
        static GL_EXT_stencil_two_side()
        {
            Console.WriteLine("Initalising GL_EXT_stencil_two_side interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadActiveStencilFaceEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_STENCIL_TEST_TWO_SIDE_EXT = 0x8910;
        public static UInt32 GL_ACTIVE_STENCIL_FACE_EXT = 0x8911;
        #endregion

        #region Commands
        internal delegate void glActiveStencilFaceEXTFunc(GLenum @face);
        internal static glActiveStencilFaceEXTFunc glActiveStencilFaceEXTPtr;
        internal static void loadActiveStencilFaceEXT()
        {
            try
            {
                glActiveStencilFaceEXTPtr = (glActiveStencilFaceEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glActiveStencilFaceEXT"), typeof(glActiveStencilFaceEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glActiveStencilFaceEXT'.");
            }
        }
        public static void glActiveStencilFaceEXT(GLenum @face) => glActiveStencilFaceEXTPtr(@face);
        #endregion
    }
}
