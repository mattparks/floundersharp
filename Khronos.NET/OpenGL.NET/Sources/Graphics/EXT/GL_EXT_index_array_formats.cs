using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_index_array_formats
    {
        #region Interop
        static GL_EXT_index_array_formats()
        {
            Console.WriteLine("Initalising GL_EXT_index_array_formats interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_IUI_V2F_EXT = 0x81AD;
        public static UInt32 GL_IUI_V3F_EXT = 0x81AE;
        public static UInt32 GL_IUI_N3F_V2F_EXT = 0x81AF;
        public static UInt32 GL_IUI_N3F_V3F_EXT = 0x81B0;
        public static UInt32 GL_T2F_IUI_V2F_EXT = 0x81B1;
        public static UInt32 GL_T2F_IUI_V3F_EXT = 0x81B2;
        public static UInt32 GL_T2F_IUI_N3F_V2F_EXT = 0x81B3;
        public static UInt32 GL_T2F_IUI_N3F_V3F_EXT = 0x81B4;
        #endregion

        #region Commands
        #endregion
    }
}
