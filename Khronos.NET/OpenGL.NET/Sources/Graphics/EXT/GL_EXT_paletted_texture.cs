using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_paletted_texture
    {
        #region Interop
        static GL_EXT_paletted_texture()
        {
            Console.WriteLine("Initalising GL_EXT_paletted_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadColorTableEXT();
            loadGetColorTableEXT();
            loadGetColorTableParameterivEXT();
            loadGetColorTableParameterfvEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COLOR_INDEX1_EXT = 0x80E2;
        public static UInt32 GL_COLOR_INDEX2_EXT = 0x80E3;
        public static UInt32 GL_COLOR_INDEX4_EXT = 0x80E4;
        public static UInt32 GL_COLOR_INDEX8_EXT = 0x80E5;
        public static UInt32 GL_COLOR_INDEX12_EXT = 0x80E6;
        public static UInt32 GL_COLOR_INDEX16_EXT = 0x80E7;
        public static UInt32 GL_TEXTURE_INDEX_SIZE_EXT = 0x80ED;
        #endregion

        #region Commands
        internal delegate void glColorTableEXTFunc(GLenum @target, GLenum @internalFormat, GLsizei @width, GLenum @format, GLenum @type, const void * @table);
        internal static glColorTableEXTFunc glColorTableEXTPtr;
        internal static void loadColorTableEXT()
        {
            try
            {
                glColorTableEXTPtr = (glColorTableEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorTableEXT"), typeof(glColorTableEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorTableEXT'.");
            }
        }
        public static void glColorTableEXT(GLenum @target, GLenum @internalFormat, GLsizei @width, GLenum @format, GLenum @type, const void * @table) => glColorTableEXTPtr(@target, @internalFormat, @width, @format, @type, @table);

        internal delegate void glGetColorTableEXTFunc(GLenum @target, GLenum @format, GLenum @type, void * @data);
        internal static glGetColorTableEXTFunc glGetColorTableEXTPtr;
        internal static void loadGetColorTableEXT()
        {
            try
            {
                glGetColorTableEXTPtr = (glGetColorTableEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetColorTableEXT"), typeof(glGetColorTableEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetColorTableEXT'.");
            }
        }
        public static void glGetColorTableEXT(GLenum @target, GLenum @format, GLenum @type, void * @data) => glGetColorTableEXTPtr(@target, @format, @type, @data);

        internal delegate void glGetColorTableParameterivEXTFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetColorTableParameterivEXTFunc glGetColorTableParameterivEXTPtr;
        internal static void loadGetColorTableParameterivEXT()
        {
            try
            {
                glGetColorTableParameterivEXTPtr = (glGetColorTableParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetColorTableParameterivEXT"), typeof(glGetColorTableParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetColorTableParameterivEXT'.");
            }
        }
        public static void glGetColorTableParameterivEXT(GLenum @target, GLenum @pname, GLint * @params) => glGetColorTableParameterivEXTPtr(@target, @pname, @params);

        internal delegate void glGetColorTableParameterfvEXTFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetColorTableParameterfvEXTFunc glGetColorTableParameterfvEXTPtr;
        internal static void loadGetColorTableParameterfvEXT()
        {
            try
            {
                glGetColorTableParameterfvEXTPtr = (glGetColorTableParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetColorTableParameterfvEXT"), typeof(glGetColorTableParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetColorTableParameterfvEXT'.");
            }
        }
        public static void glGetColorTableParameterfvEXT(GLenum @target, GLenum @pname, GLfloat * @params) => glGetColorTableParameterfvEXTPtr(@target, @pname, @params);
        #endregion
    }
}
