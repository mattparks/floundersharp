using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_type_2_10_10_10_REV
    {
        #region Interop
        static GL_EXT_texture_type_2_10_10_10_REV()
        {
            Console.WriteLine("Initalising GL_EXT_texture_type_2_10_10_10_REV interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_UNSIGNED_INT_2_10_10_10_REV_EXT = 0x8368;
        #endregion

        #region Commands
        #endregion
    }
}
