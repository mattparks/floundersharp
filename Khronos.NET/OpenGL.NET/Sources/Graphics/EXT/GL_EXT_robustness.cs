using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_robustness
    {
        #region Interop
        static GL_EXT_robustness()
        {
            Console.WriteLine("Initalising GL_EXT_robustness interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetGraphicsResetStatusEXT();
            loadReadnPixelsEXT();
            loadGetnUniformfvEXT();
            loadGetnUniformivEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_NO_ERROR = 0;
        public static UInt32 GL_GUILTY_CONTEXT_RESET_EXT = 0x8253;
        public static UInt32 GL_INNOCENT_CONTEXT_RESET_EXT = 0x8254;
        public static UInt32 GL_UNKNOWN_CONTEXT_RESET_EXT = 0x8255;
        public static UInt32 GL_CONTEXT_ROBUST_ACCESS_EXT = 0x90F3;
        public static UInt32 GL_RESET_NOTIFICATION_STRATEGY_EXT = 0x8256;
        public static UInt32 GL_LOSE_CONTEXT_ON_RESET_EXT = 0x8252;
        public static UInt32 GL_NO_RESET_NOTIFICATION_EXT = 0x8261;
        #endregion

        #region Commands
        internal delegate GLenum glGetGraphicsResetStatusEXTFunc();
        internal static glGetGraphicsResetStatusEXTFunc glGetGraphicsResetStatusEXTPtr;
        internal static void loadGetGraphicsResetStatusEXT()
        {
            try
            {
                glGetGraphicsResetStatusEXTPtr = (glGetGraphicsResetStatusEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetGraphicsResetStatusEXT"), typeof(glGetGraphicsResetStatusEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetGraphicsResetStatusEXT'.");
            }
        }
        public static GLenum glGetGraphicsResetStatusEXT() => glGetGraphicsResetStatusEXTPtr();

        internal delegate void glReadnPixelsEXTFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, GLsizei @bufSize, void * @data);
        internal static glReadnPixelsEXTFunc glReadnPixelsEXTPtr;
        internal static void loadReadnPixelsEXT()
        {
            try
            {
                glReadnPixelsEXTPtr = (glReadnPixelsEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadnPixelsEXT"), typeof(glReadnPixelsEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadnPixelsEXT'.");
            }
        }
        public static void glReadnPixelsEXT(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, GLsizei @bufSize, void * @data) => glReadnPixelsEXTPtr(@x, @y, @width, @height, @format, @type, @bufSize, @data);

        internal delegate void glGetnUniformfvEXTFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLfloat * @params);
        internal static glGetnUniformfvEXTFunc glGetnUniformfvEXTPtr;
        internal static void loadGetnUniformfvEXT()
        {
            try
            {
                glGetnUniformfvEXTPtr = (glGetnUniformfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformfvEXT"), typeof(glGetnUniformfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformfvEXT'.");
            }
        }
        public static void glGetnUniformfvEXT(GLuint @program, GLint @location, GLsizei @bufSize, GLfloat * @params) => glGetnUniformfvEXTPtr(@program, @location, @bufSize, @params);

        internal delegate void glGetnUniformivEXTFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLint * @params);
        internal static glGetnUniformivEXTFunc glGetnUniformivEXTPtr;
        internal static void loadGetnUniformivEXT()
        {
            try
            {
                glGetnUniformivEXTPtr = (glGetnUniformivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformivEXT"), typeof(glGetnUniformivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformivEXT'.");
            }
        }
        public static void glGetnUniformivEXT(GLuint @program, GLint @location, GLsizei @bufSize, GLint * @params) => glGetnUniformivEXTPtr(@program, @location, @bufSize, @params);
        #endregion
    }
}
