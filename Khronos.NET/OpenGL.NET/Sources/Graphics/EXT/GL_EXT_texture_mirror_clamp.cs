using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_mirror_clamp
    {
        #region Interop
        static GL_EXT_texture_mirror_clamp()
        {
            Console.WriteLine("Initalising GL_EXT_texture_mirror_clamp interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MIRROR_CLAMP_EXT = 0x8742;
        public static UInt32 GL_MIRROR_CLAMP_TO_EDGE_EXT = 0x8743;
        public static UInt32 GL_MIRROR_CLAMP_TO_BORDER_EXT = 0x8912;
        #endregion

        #region Commands
        #endregion
    }
}
