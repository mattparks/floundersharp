using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_separate_shader_objects
    {
        #region Interop
        static GL_EXT_separate_shader_objects()
        {
            Console.WriteLine("Initalising GL_EXT_separate_shader_objects interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadUseShaderProgramEXT();
            loadActiveProgramEXT();
            loadCreateShaderProgramEXT();
            loadActiveShaderProgramEXT();
            loadBindProgramPipelineEXT();
            loadCreateShaderProgramvEXT();
            loadDeleteProgramPipelinesEXT();
            loadGenProgramPipelinesEXT();
            loadGetProgramPipelineInfoLogEXT();
            loadGetProgramPipelineivEXT();
            loadIsProgramPipelineEXT();
            loadProgramParameteriEXT();
            loadProgramUniform1fEXT();
            loadProgramUniform1fvEXT();
            loadProgramUniform1iEXT();
            loadProgramUniform1ivEXT();
            loadProgramUniform2fEXT();
            loadProgramUniform2fvEXT();
            loadProgramUniform2iEXT();
            loadProgramUniform2ivEXT();
            loadProgramUniform3fEXT();
            loadProgramUniform3fvEXT();
            loadProgramUniform3iEXT();
            loadProgramUniform3ivEXT();
            loadProgramUniform4fEXT();
            loadProgramUniform4fvEXT();
            loadProgramUniform4iEXT();
            loadProgramUniform4ivEXT();
            loadProgramUniformMatrix2fvEXT();
            loadProgramUniformMatrix3fvEXT();
            loadProgramUniformMatrix4fvEXT();
            loadUseProgramStagesEXT();
            loadValidateProgramPipelineEXT();
            loadProgramUniform1uiEXT();
            loadProgramUniform2uiEXT();
            loadProgramUniform3uiEXT();
            loadProgramUniform4uiEXT();
            loadProgramUniform1uivEXT();
            loadProgramUniform2uivEXT();
            loadProgramUniform3uivEXT();
            loadProgramUniform4uivEXT();
            loadProgramUniformMatrix4fvEXT();
            loadProgramUniformMatrix2x3fvEXT();
            loadProgramUniformMatrix3x2fvEXT();
            loadProgramUniformMatrix2x4fvEXT();
            loadProgramUniformMatrix4x2fvEXT();
            loadProgramUniformMatrix3x4fvEXT();
            loadProgramUniformMatrix4x3fvEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_ACTIVE_PROGRAM_EXT = 0x8259;
        public static UInt32 GL_VERTEX_SHADER_BIT_EXT = 0x00000001;
        public static UInt32 GL_FRAGMENT_SHADER_BIT_EXT = 0x00000002;
        public static UInt32 GL_ALL_SHADER_BITS_EXT = 0xFFFFFFFF;
        public static UInt32 GL_PROGRAM_SEPARABLE_EXT = 0x8258;
        public static UInt32 GL_PROGRAM_PIPELINE_BINDING_EXT = 0x825A;
        #endregion

        #region Commands
        internal delegate void glUseShaderProgramEXTFunc(GLenum @type, GLuint @program);
        internal static glUseShaderProgramEXTFunc glUseShaderProgramEXTPtr;
        internal static void loadUseShaderProgramEXT()
        {
            try
            {
                glUseShaderProgramEXTPtr = (glUseShaderProgramEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUseShaderProgramEXT"), typeof(glUseShaderProgramEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUseShaderProgramEXT'.");
            }
        }
        public static void glUseShaderProgramEXT(GLenum @type, GLuint @program) => glUseShaderProgramEXTPtr(@type, @program);

        internal delegate void glActiveProgramEXTFunc(GLuint @program);
        internal static glActiveProgramEXTFunc glActiveProgramEXTPtr;
        internal static void loadActiveProgramEXT()
        {
            try
            {
                glActiveProgramEXTPtr = (glActiveProgramEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glActiveProgramEXT"), typeof(glActiveProgramEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glActiveProgramEXT'.");
            }
        }
        public static void glActiveProgramEXT(GLuint @program) => glActiveProgramEXTPtr(@program);

        internal delegate GLuint glCreateShaderProgramEXTFunc(GLenum @type, const GLchar * @string);
        internal static glCreateShaderProgramEXTFunc glCreateShaderProgramEXTPtr;
        internal static void loadCreateShaderProgramEXT()
        {
            try
            {
                glCreateShaderProgramEXTPtr = (glCreateShaderProgramEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateShaderProgramEXT"), typeof(glCreateShaderProgramEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateShaderProgramEXT'.");
            }
        }
        public static GLuint glCreateShaderProgramEXT(GLenum @type, const GLchar * @string) => glCreateShaderProgramEXTPtr(@type, @string);

        internal delegate void glActiveShaderProgramEXTFunc(GLuint @pipeline, GLuint @program);
        internal static glActiveShaderProgramEXTFunc glActiveShaderProgramEXTPtr;
        internal static void loadActiveShaderProgramEXT()
        {
            try
            {
                glActiveShaderProgramEXTPtr = (glActiveShaderProgramEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glActiveShaderProgramEXT"), typeof(glActiveShaderProgramEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glActiveShaderProgramEXT'.");
            }
        }
        public static void glActiveShaderProgramEXT(GLuint @pipeline, GLuint @program) => glActiveShaderProgramEXTPtr(@pipeline, @program);

        internal delegate void glBindProgramPipelineEXTFunc(GLuint @pipeline);
        internal static glBindProgramPipelineEXTFunc glBindProgramPipelineEXTPtr;
        internal static void loadBindProgramPipelineEXT()
        {
            try
            {
                glBindProgramPipelineEXTPtr = (glBindProgramPipelineEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindProgramPipelineEXT"), typeof(glBindProgramPipelineEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindProgramPipelineEXT'.");
            }
        }
        public static void glBindProgramPipelineEXT(GLuint @pipeline) => glBindProgramPipelineEXTPtr(@pipeline);

        internal delegate GLuint glCreateShaderProgramvEXTFunc(GLenum @type, GLsizei @count, const GLchar ** @strings);
        internal static glCreateShaderProgramvEXTFunc glCreateShaderProgramvEXTPtr;
        internal static void loadCreateShaderProgramvEXT()
        {
            try
            {
                glCreateShaderProgramvEXTPtr = (glCreateShaderProgramvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateShaderProgramvEXT"), typeof(glCreateShaderProgramvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateShaderProgramvEXT'.");
            }
        }
        public static GLuint glCreateShaderProgramvEXT(GLenum @type, GLsizei @count, const GLchar ** @strings) => glCreateShaderProgramvEXTPtr(@type, @count, @strings);

        internal delegate void glDeleteProgramPipelinesEXTFunc(GLsizei @n, const GLuint * @pipelines);
        internal static glDeleteProgramPipelinesEXTFunc glDeleteProgramPipelinesEXTPtr;
        internal static void loadDeleteProgramPipelinesEXT()
        {
            try
            {
                glDeleteProgramPipelinesEXTPtr = (glDeleteProgramPipelinesEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteProgramPipelinesEXT"), typeof(glDeleteProgramPipelinesEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteProgramPipelinesEXT'.");
            }
        }
        public static void glDeleteProgramPipelinesEXT(GLsizei @n, const GLuint * @pipelines) => glDeleteProgramPipelinesEXTPtr(@n, @pipelines);

        internal delegate void glGenProgramPipelinesEXTFunc(GLsizei @n, GLuint * @pipelines);
        internal static glGenProgramPipelinesEXTFunc glGenProgramPipelinesEXTPtr;
        internal static void loadGenProgramPipelinesEXT()
        {
            try
            {
                glGenProgramPipelinesEXTPtr = (glGenProgramPipelinesEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenProgramPipelinesEXT"), typeof(glGenProgramPipelinesEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenProgramPipelinesEXT'.");
            }
        }
        public static void glGenProgramPipelinesEXT(GLsizei @n, GLuint * @pipelines) => glGenProgramPipelinesEXTPtr(@n, @pipelines);

        internal delegate void glGetProgramPipelineInfoLogEXTFunc(GLuint @pipeline, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog);
        internal static glGetProgramPipelineInfoLogEXTFunc glGetProgramPipelineInfoLogEXTPtr;
        internal static void loadGetProgramPipelineInfoLogEXT()
        {
            try
            {
                glGetProgramPipelineInfoLogEXTPtr = (glGetProgramPipelineInfoLogEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramPipelineInfoLogEXT"), typeof(glGetProgramPipelineInfoLogEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramPipelineInfoLogEXT'.");
            }
        }
        public static void glGetProgramPipelineInfoLogEXT(GLuint @pipeline, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog) => glGetProgramPipelineInfoLogEXTPtr(@pipeline, @bufSize, @length, @infoLog);

        internal delegate void glGetProgramPipelineivEXTFunc(GLuint @pipeline, GLenum @pname, GLint * @params);
        internal static glGetProgramPipelineivEXTFunc glGetProgramPipelineivEXTPtr;
        internal static void loadGetProgramPipelineivEXT()
        {
            try
            {
                glGetProgramPipelineivEXTPtr = (glGetProgramPipelineivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramPipelineivEXT"), typeof(glGetProgramPipelineivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramPipelineivEXT'.");
            }
        }
        public static void glGetProgramPipelineivEXT(GLuint @pipeline, GLenum @pname, GLint * @params) => glGetProgramPipelineivEXTPtr(@pipeline, @pname, @params);

        internal delegate GLboolean glIsProgramPipelineEXTFunc(GLuint @pipeline);
        internal static glIsProgramPipelineEXTFunc glIsProgramPipelineEXTPtr;
        internal static void loadIsProgramPipelineEXT()
        {
            try
            {
                glIsProgramPipelineEXTPtr = (glIsProgramPipelineEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsProgramPipelineEXT"), typeof(glIsProgramPipelineEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsProgramPipelineEXT'.");
            }
        }
        public static GLboolean glIsProgramPipelineEXT(GLuint @pipeline) => glIsProgramPipelineEXTPtr(@pipeline);

        internal delegate void glProgramParameteriEXTFunc(GLuint @program, GLenum @pname, GLint @value);
        internal static glProgramParameteriEXTFunc glProgramParameteriEXTPtr;
        internal static void loadProgramParameteriEXT()
        {
            try
            {
                glProgramParameteriEXTPtr = (glProgramParameteriEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramParameteriEXT"), typeof(glProgramParameteriEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramParameteriEXT'.");
            }
        }
        public static void glProgramParameteriEXT(GLuint @program, GLenum @pname, GLint @value) => glProgramParameteriEXTPtr(@program, @pname, @value);

        internal delegate void glProgramUniform1fEXTFunc(GLuint @program, GLint @location, GLfloat @v0);
        internal static glProgramUniform1fEXTFunc glProgramUniform1fEXTPtr;
        internal static void loadProgramUniform1fEXT()
        {
            try
            {
                glProgramUniform1fEXTPtr = (glProgramUniform1fEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1fEXT"), typeof(glProgramUniform1fEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1fEXT'.");
            }
        }
        public static void glProgramUniform1fEXT(GLuint @program, GLint @location, GLfloat @v0) => glProgramUniform1fEXTPtr(@program, @location, @v0);

        internal delegate void glProgramUniform1fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform1fvEXTFunc glProgramUniform1fvEXTPtr;
        internal static void loadProgramUniform1fvEXT()
        {
            try
            {
                glProgramUniform1fvEXTPtr = (glProgramUniform1fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1fvEXT"), typeof(glProgramUniform1fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1fvEXT'.");
            }
        }
        public static void glProgramUniform1fvEXT(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform1fvEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform1iEXTFunc(GLuint @program, GLint @location, GLint @v0);
        internal static glProgramUniform1iEXTFunc glProgramUniform1iEXTPtr;
        internal static void loadProgramUniform1iEXT()
        {
            try
            {
                glProgramUniform1iEXTPtr = (glProgramUniform1iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1iEXT"), typeof(glProgramUniform1iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1iEXT'.");
            }
        }
        public static void glProgramUniform1iEXT(GLuint @program, GLint @location, GLint @v0) => glProgramUniform1iEXTPtr(@program, @location, @v0);

        internal delegate void glProgramUniform1ivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform1ivEXTFunc glProgramUniform1ivEXTPtr;
        internal static void loadProgramUniform1ivEXT()
        {
            try
            {
                glProgramUniform1ivEXTPtr = (glProgramUniform1ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1ivEXT"), typeof(glProgramUniform1ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1ivEXT'.");
            }
        }
        public static void glProgramUniform1ivEXT(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform1ivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2fEXTFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1);
        internal static glProgramUniform2fEXTFunc glProgramUniform2fEXTPtr;
        internal static void loadProgramUniform2fEXT()
        {
            try
            {
                glProgramUniform2fEXTPtr = (glProgramUniform2fEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2fEXT"), typeof(glProgramUniform2fEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2fEXT'.");
            }
        }
        public static void glProgramUniform2fEXT(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1) => glProgramUniform2fEXTPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform2fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform2fvEXTFunc glProgramUniform2fvEXTPtr;
        internal static void loadProgramUniform2fvEXT()
        {
            try
            {
                glProgramUniform2fvEXTPtr = (glProgramUniform2fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2fvEXT"), typeof(glProgramUniform2fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2fvEXT'.");
            }
        }
        public static void glProgramUniform2fvEXT(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform2fvEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2iEXTFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1);
        internal static glProgramUniform2iEXTFunc glProgramUniform2iEXTPtr;
        internal static void loadProgramUniform2iEXT()
        {
            try
            {
                glProgramUniform2iEXTPtr = (glProgramUniform2iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2iEXT"), typeof(glProgramUniform2iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2iEXT'.");
            }
        }
        public static void glProgramUniform2iEXT(GLuint @program, GLint @location, GLint @v0, GLint @v1) => glProgramUniform2iEXTPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform2ivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform2ivEXTFunc glProgramUniform2ivEXTPtr;
        internal static void loadProgramUniform2ivEXT()
        {
            try
            {
                glProgramUniform2ivEXTPtr = (glProgramUniform2ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2ivEXT"), typeof(glProgramUniform2ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2ivEXT'.");
            }
        }
        public static void glProgramUniform2ivEXT(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform2ivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3fEXTFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2);
        internal static glProgramUniform3fEXTFunc glProgramUniform3fEXTPtr;
        internal static void loadProgramUniform3fEXT()
        {
            try
            {
                glProgramUniform3fEXTPtr = (glProgramUniform3fEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3fEXT"), typeof(glProgramUniform3fEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3fEXT'.");
            }
        }
        public static void glProgramUniform3fEXT(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2) => glProgramUniform3fEXTPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform3fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform3fvEXTFunc glProgramUniform3fvEXTPtr;
        internal static void loadProgramUniform3fvEXT()
        {
            try
            {
                glProgramUniform3fvEXTPtr = (glProgramUniform3fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3fvEXT"), typeof(glProgramUniform3fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3fvEXT'.");
            }
        }
        public static void glProgramUniform3fvEXT(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform3fvEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3iEXTFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2);
        internal static glProgramUniform3iEXTFunc glProgramUniform3iEXTPtr;
        internal static void loadProgramUniform3iEXT()
        {
            try
            {
                glProgramUniform3iEXTPtr = (glProgramUniform3iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3iEXT"), typeof(glProgramUniform3iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3iEXT'.");
            }
        }
        public static void glProgramUniform3iEXT(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2) => glProgramUniform3iEXTPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform3ivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform3ivEXTFunc glProgramUniform3ivEXTPtr;
        internal static void loadProgramUniform3ivEXT()
        {
            try
            {
                glProgramUniform3ivEXTPtr = (glProgramUniform3ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3ivEXT"), typeof(glProgramUniform3ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3ivEXT'.");
            }
        }
        public static void glProgramUniform3ivEXT(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform3ivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4fEXTFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3);
        internal static glProgramUniform4fEXTFunc glProgramUniform4fEXTPtr;
        internal static void loadProgramUniform4fEXT()
        {
            try
            {
                glProgramUniform4fEXTPtr = (glProgramUniform4fEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4fEXT"), typeof(glProgramUniform4fEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4fEXT'.");
            }
        }
        public static void glProgramUniform4fEXT(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3) => glProgramUniform4fEXTPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform4fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform4fvEXTFunc glProgramUniform4fvEXTPtr;
        internal static void loadProgramUniform4fvEXT()
        {
            try
            {
                glProgramUniform4fvEXTPtr = (glProgramUniform4fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4fvEXT"), typeof(glProgramUniform4fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4fvEXT'.");
            }
        }
        public static void glProgramUniform4fvEXT(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform4fvEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4iEXTFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3);
        internal static glProgramUniform4iEXTFunc glProgramUniform4iEXTPtr;
        internal static void loadProgramUniform4iEXT()
        {
            try
            {
                glProgramUniform4iEXTPtr = (glProgramUniform4iEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4iEXT"), typeof(glProgramUniform4iEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4iEXT'.");
            }
        }
        public static void glProgramUniform4iEXT(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3) => glProgramUniform4iEXTPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform4ivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform4ivEXTFunc glProgramUniform4ivEXTPtr;
        internal static void loadProgramUniform4ivEXT()
        {
            try
            {
                glProgramUniform4ivEXTPtr = (glProgramUniform4ivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4ivEXT"), typeof(glProgramUniform4ivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4ivEXT'.");
            }
        }
        public static void glProgramUniform4ivEXT(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform4ivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniformMatrix2fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2fvEXTFunc glProgramUniformMatrix2fvEXTPtr;
        internal static void loadProgramUniformMatrix2fvEXT()
        {
            try
            {
                glProgramUniformMatrix2fvEXTPtr = (glProgramUniformMatrix2fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2fvEXT"), typeof(glProgramUniformMatrix2fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix2fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3fvEXTFunc glProgramUniformMatrix3fvEXTPtr;
        internal static void loadProgramUniformMatrix3fvEXT()
        {
            try
            {
                glProgramUniformMatrix3fvEXTPtr = (glProgramUniformMatrix3fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3fvEXT"), typeof(glProgramUniformMatrix3fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix3fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4fvEXTFunc glProgramUniformMatrix4fvEXTPtr;
        internal static void loadProgramUniformMatrix4fvEXT()
        {
            try
            {
                glProgramUniformMatrix4fvEXTPtr = (glProgramUniformMatrix4fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4fvEXT"), typeof(glProgramUniformMatrix4fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix4fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glUseProgramStagesEXTFunc(GLuint @pipeline, GLbitfield @stages, GLuint @program);
        internal static glUseProgramStagesEXTFunc glUseProgramStagesEXTPtr;
        internal static void loadUseProgramStagesEXT()
        {
            try
            {
                glUseProgramStagesEXTPtr = (glUseProgramStagesEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUseProgramStagesEXT"), typeof(glUseProgramStagesEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUseProgramStagesEXT'.");
            }
        }
        public static void glUseProgramStagesEXT(GLuint @pipeline, GLbitfield @stages, GLuint @program) => glUseProgramStagesEXTPtr(@pipeline, @stages, @program);

        internal delegate void glValidateProgramPipelineEXTFunc(GLuint @pipeline);
        internal static glValidateProgramPipelineEXTFunc glValidateProgramPipelineEXTPtr;
        internal static void loadValidateProgramPipelineEXT()
        {
            try
            {
                glValidateProgramPipelineEXTPtr = (glValidateProgramPipelineEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glValidateProgramPipelineEXT"), typeof(glValidateProgramPipelineEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glValidateProgramPipelineEXT'.");
            }
        }
        public static void glValidateProgramPipelineEXT(GLuint @pipeline) => glValidateProgramPipelineEXTPtr(@pipeline);

        internal delegate void glProgramUniform1uiEXTFunc(GLuint @program, GLint @location, GLuint @v0);
        internal static glProgramUniform1uiEXTFunc glProgramUniform1uiEXTPtr;
        internal static void loadProgramUniform1uiEXT()
        {
            try
            {
                glProgramUniform1uiEXTPtr = (glProgramUniform1uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1uiEXT"), typeof(glProgramUniform1uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1uiEXT'.");
            }
        }
        public static void glProgramUniform1uiEXT(GLuint @program, GLint @location, GLuint @v0) => glProgramUniform1uiEXTPtr(@program, @location, @v0);

        internal delegate void glProgramUniform2uiEXTFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1);
        internal static glProgramUniform2uiEXTFunc glProgramUniform2uiEXTPtr;
        internal static void loadProgramUniform2uiEXT()
        {
            try
            {
                glProgramUniform2uiEXTPtr = (glProgramUniform2uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2uiEXT"), typeof(glProgramUniform2uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2uiEXT'.");
            }
        }
        public static void glProgramUniform2uiEXT(GLuint @program, GLint @location, GLuint @v0, GLuint @v1) => glProgramUniform2uiEXTPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform3uiEXTFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2);
        internal static glProgramUniform3uiEXTFunc glProgramUniform3uiEXTPtr;
        internal static void loadProgramUniform3uiEXT()
        {
            try
            {
                glProgramUniform3uiEXTPtr = (glProgramUniform3uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3uiEXT"), typeof(glProgramUniform3uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3uiEXT'.");
            }
        }
        public static void glProgramUniform3uiEXT(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2) => glProgramUniform3uiEXTPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform4uiEXTFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3);
        internal static glProgramUniform4uiEXTFunc glProgramUniform4uiEXTPtr;
        internal static void loadProgramUniform4uiEXT()
        {
            try
            {
                glProgramUniform4uiEXTPtr = (glProgramUniform4uiEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4uiEXT"), typeof(glProgramUniform4uiEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4uiEXT'.");
            }
        }
        public static void glProgramUniform4uiEXT(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3) => glProgramUniform4uiEXTPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform1uivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform1uivEXTFunc glProgramUniform1uivEXTPtr;
        internal static void loadProgramUniform1uivEXT()
        {
            try
            {
                glProgramUniform1uivEXTPtr = (glProgramUniform1uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1uivEXT"), typeof(glProgramUniform1uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1uivEXT'.");
            }
        }
        public static void glProgramUniform1uivEXT(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform1uivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2uivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform2uivEXTFunc glProgramUniform2uivEXTPtr;
        internal static void loadProgramUniform2uivEXT()
        {
            try
            {
                glProgramUniform2uivEXTPtr = (glProgramUniform2uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2uivEXT"), typeof(glProgramUniform2uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2uivEXT'.");
            }
        }
        public static void glProgramUniform2uivEXT(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform2uivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3uivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform3uivEXTFunc glProgramUniform3uivEXTPtr;
        internal static void loadProgramUniform3uivEXT()
        {
            try
            {
                glProgramUniform3uivEXTPtr = (glProgramUniform3uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3uivEXT"), typeof(glProgramUniform3uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3uivEXT'.");
            }
        }
        public static void glProgramUniform3uivEXT(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform3uivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4uivEXTFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform4uivEXTFunc glProgramUniform4uivEXTPtr;
        internal static void loadProgramUniform4uivEXT()
        {
            try
            {
                glProgramUniform4uivEXTPtr = (glProgramUniform4uivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4uivEXT"), typeof(glProgramUniform4uivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4uivEXT'.");
            }
        }
        public static void glProgramUniform4uivEXT(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform4uivEXTPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniformMatrix2x3fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2x3fvEXTFunc glProgramUniformMatrix2x3fvEXTPtr;
        internal static void loadProgramUniformMatrix2x3fvEXT()
        {
            try
            {
                glProgramUniformMatrix2x3fvEXTPtr = (glProgramUniformMatrix2x3fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x3fvEXT"), typeof(glProgramUniformMatrix2x3fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x3fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix2x3fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2x3fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x2fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3x2fvEXTFunc glProgramUniformMatrix3x2fvEXTPtr;
        internal static void loadProgramUniformMatrix3x2fvEXT()
        {
            try
            {
                glProgramUniformMatrix3x2fvEXTPtr = (glProgramUniformMatrix3x2fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x2fvEXT"), typeof(glProgramUniformMatrix3x2fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x2fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix3x2fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3x2fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x4fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2x4fvEXTFunc glProgramUniformMatrix2x4fvEXTPtr;
        internal static void loadProgramUniformMatrix2x4fvEXT()
        {
            try
            {
                glProgramUniformMatrix2x4fvEXTPtr = (glProgramUniformMatrix2x4fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x4fvEXT"), typeof(glProgramUniformMatrix2x4fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x4fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix2x4fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2x4fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x2fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4x2fvEXTFunc glProgramUniformMatrix4x2fvEXTPtr;
        internal static void loadProgramUniformMatrix4x2fvEXT()
        {
            try
            {
                glProgramUniformMatrix4x2fvEXTPtr = (glProgramUniformMatrix4x2fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x2fvEXT"), typeof(glProgramUniformMatrix4x2fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x2fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix4x2fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4x2fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x4fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3x4fvEXTFunc glProgramUniformMatrix3x4fvEXTPtr;
        internal static void loadProgramUniformMatrix3x4fvEXT()
        {
            try
            {
                glProgramUniformMatrix3x4fvEXTPtr = (glProgramUniformMatrix3x4fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x4fvEXT"), typeof(glProgramUniformMatrix3x4fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x4fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix3x4fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3x4fvEXTPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x3fvEXTFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4x3fvEXTFunc glProgramUniformMatrix4x3fvEXTPtr;
        internal static void loadProgramUniformMatrix4x3fvEXT()
        {
            try
            {
                glProgramUniformMatrix4x3fvEXTPtr = (glProgramUniformMatrix4x3fvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x3fvEXT"), typeof(glProgramUniformMatrix4x3fvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x3fvEXT'.");
            }
        }
        public static void glProgramUniformMatrix4x3fvEXT(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4x3fvEXTPtr(@program, @location, @count, @transpose, @value);

        #endregion
    }
}
