using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_blend_func_extended
    {
        #region Interop
        static GL_EXT_blend_func_extended()
        {
            Console.WriteLine("Initalising GL_EXT_blend_func_extended interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindFragDataLocationIndexedEXT();
            loadBindFragDataLocationEXT();
            loadGetProgramResourceLocationIndexEXT();
            loadGetFragDataIndexEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SRC1_COLOR_EXT = 0x88F9;
        public static UInt32 GL_SRC1_ALPHA_EXT = 0x8589;
        public static UInt32 GL_ONE_MINUS_SRC1_COLOR_EXT = 0x88FA;
        public static UInt32 GL_ONE_MINUS_SRC1_ALPHA_EXT = 0x88FB;
        public static UInt32 GL_SRC_ALPHA_SATURATE_EXT = 0x0308;
        public static UInt32 GL_LOCATION_INDEX_EXT = 0x930F;
        public static UInt32 GL_MAX_DUAL_SOURCE_DRAW_BUFFERS_EXT = 0x88FC;
        #endregion

        #region Commands
        internal delegate void glBindFragDataLocationIndexedEXTFunc(GLuint @program, GLuint @colorNumber, GLuint @index, const GLchar * @name);
        internal static glBindFragDataLocationIndexedEXTFunc glBindFragDataLocationIndexedEXTPtr;
        internal static void loadBindFragDataLocationIndexedEXT()
        {
            try
            {
                glBindFragDataLocationIndexedEXTPtr = (glBindFragDataLocationIndexedEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindFragDataLocationIndexedEXT"), typeof(glBindFragDataLocationIndexedEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindFragDataLocationIndexedEXT'.");
            }
        }
        public static void glBindFragDataLocationIndexedEXT(GLuint @program, GLuint @colorNumber, GLuint @index, const GLchar * @name) => glBindFragDataLocationIndexedEXTPtr(@program, @colorNumber, @index, @name);

        internal delegate void glBindFragDataLocationEXTFunc(GLuint @program, GLuint @color, const GLchar * @name);
        internal static glBindFragDataLocationEXTFunc glBindFragDataLocationEXTPtr;
        internal static void loadBindFragDataLocationEXT()
        {
            try
            {
                glBindFragDataLocationEXTPtr = (glBindFragDataLocationEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindFragDataLocationEXT"), typeof(glBindFragDataLocationEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindFragDataLocationEXT'.");
            }
        }
        public static void glBindFragDataLocationEXT(GLuint @program, GLuint @color, const GLchar * @name) => glBindFragDataLocationEXTPtr(@program, @color, @name);

        internal delegate GLint glGetProgramResourceLocationIndexEXTFunc(GLuint @program, GLenum @programInterface, const GLchar * @name);
        internal static glGetProgramResourceLocationIndexEXTFunc glGetProgramResourceLocationIndexEXTPtr;
        internal static void loadGetProgramResourceLocationIndexEXT()
        {
            try
            {
                glGetProgramResourceLocationIndexEXTPtr = (glGetProgramResourceLocationIndexEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceLocationIndexEXT"), typeof(glGetProgramResourceLocationIndexEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceLocationIndexEXT'.");
            }
        }
        public static GLint glGetProgramResourceLocationIndexEXT(GLuint @program, GLenum @programInterface, const GLchar * @name) => glGetProgramResourceLocationIndexEXTPtr(@program, @programInterface, @name);

        internal delegate GLint glGetFragDataIndexEXTFunc(GLuint @program, const GLchar * @name);
        internal static glGetFragDataIndexEXTFunc glGetFragDataIndexEXTPtr;
        internal static void loadGetFragDataIndexEXT()
        {
            try
            {
                glGetFragDataIndexEXTPtr = (glGetFragDataIndexEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFragDataIndexEXT"), typeof(glGetFragDataIndexEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFragDataIndexEXT'.");
            }
        }
        public static GLint glGetFragDataIndexEXT(GLuint @program, const GLchar * @name) => glGetFragDataIndexEXTPtr(@program, @name);
        #endregion
    }
}
