using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_vertex_attrib_64bit
    {
        #region Interop
        static GL_EXT_vertex_attrib_64bit()
        {
            Console.WriteLine("Initalising GL_EXT_vertex_attrib_64bit interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexAttribL1dEXT();
            loadVertexAttribL2dEXT();
            loadVertexAttribL3dEXT();
            loadVertexAttribL4dEXT();
            loadVertexAttribL1dvEXT();
            loadVertexAttribL2dvEXT();
            loadVertexAttribL3dvEXT();
            loadVertexAttribL4dvEXT();
            loadVertexAttribLPointerEXT();
            loadGetVertexAttribLdvEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_DOUBLE = 0x140A;
        public static UInt32 GL_DOUBLE_VEC2_EXT = 0x8FFC;
        public static UInt32 GL_DOUBLE_VEC3_EXT = 0x8FFD;
        public static UInt32 GL_DOUBLE_VEC4_EXT = 0x8FFE;
        public static UInt32 GL_DOUBLE_MAT2_EXT = 0x8F46;
        public static UInt32 GL_DOUBLE_MAT3_EXT = 0x8F47;
        public static UInt32 GL_DOUBLE_MAT4_EXT = 0x8F48;
        public static UInt32 GL_DOUBLE_MAT2x3_EXT = 0x8F49;
        public static UInt32 GL_DOUBLE_MAT2x4_EXT = 0x8F4A;
        public static UInt32 GL_DOUBLE_MAT3x2_EXT = 0x8F4B;
        public static UInt32 GL_DOUBLE_MAT3x4_EXT = 0x8F4C;
        public static UInt32 GL_DOUBLE_MAT4x2_EXT = 0x8F4D;
        public static UInt32 GL_DOUBLE_MAT4x3_EXT = 0x8F4E;
        #endregion

        #region Commands
        internal delegate void glVertexAttribL1dEXTFunc(GLuint @index, GLdouble @x);
        internal static glVertexAttribL1dEXTFunc glVertexAttribL1dEXTPtr;
        internal static void loadVertexAttribL1dEXT()
        {
            try
            {
                glVertexAttribL1dEXTPtr = (glVertexAttribL1dEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL1dEXT"), typeof(glVertexAttribL1dEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL1dEXT'.");
            }
        }
        public static void glVertexAttribL1dEXT(GLuint @index, GLdouble @x) => glVertexAttribL1dEXTPtr(@index, @x);

        internal delegate void glVertexAttribL2dEXTFunc(GLuint @index, GLdouble @x, GLdouble @y);
        internal static glVertexAttribL2dEXTFunc glVertexAttribL2dEXTPtr;
        internal static void loadVertexAttribL2dEXT()
        {
            try
            {
                glVertexAttribL2dEXTPtr = (glVertexAttribL2dEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL2dEXT"), typeof(glVertexAttribL2dEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL2dEXT'.");
            }
        }
        public static void glVertexAttribL2dEXT(GLuint @index, GLdouble @x, GLdouble @y) => glVertexAttribL2dEXTPtr(@index, @x, @y);

        internal delegate void glVertexAttribL3dEXTFunc(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glVertexAttribL3dEXTFunc glVertexAttribL3dEXTPtr;
        internal static void loadVertexAttribL3dEXT()
        {
            try
            {
                glVertexAttribL3dEXTPtr = (glVertexAttribL3dEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL3dEXT"), typeof(glVertexAttribL3dEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL3dEXT'.");
            }
        }
        public static void glVertexAttribL3dEXT(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z) => glVertexAttribL3dEXTPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttribL4dEXTFunc(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glVertexAttribL4dEXTFunc glVertexAttribL4dEXTPtr;
        internal static void loadVertexAttribL4dEXT()
        {
            try
            {
                glVertexAttribL4dEXTPtr = (glVertexAttribL4dEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL4dEXT"), typeof(glVertexAttribL4dEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL4dEXT'.");
            }
        }
        public static void glVertexAttribL4dEXT(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glVertexAttribL4dEXTPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttribL1dvEXTFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttribL1dvEXTFunc glVertexAttribL1dvEXTPtr;
        internal static void loadVertexAttribL1dvEXT()
        {
            try
            {
                glVertexAttribL1dvEXTPtr = (glVertexAttribL1dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL1dvEXT"), typeof(glVertexAttribL1dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL1dvEXT'.");
            }
        }
        public static void glVertexAttribL1dvEXT(GLuint @index, const GLdouble * @v) => glVertexAttribL1dvEXTPtr(@index, @v);

        internal delegate void glVertexAttribL2dvEXTFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttribL2dvEXTFunc glVertexAttribL2dvEXTPtr;
        internal static void loadVertexAttribL2dvEXT()
        {
            try
            {
                glVertexAttribL2dvEXTPtr = (glVertexAttribL2dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL2dvEXT"), typeof(glVertexAttribL2dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL2dvEXT'.");
            }
        }
        public static void glVertexAttribL2dvEXT(GLuint @index, const GLdouble * @v) => glVertexAttribL2dvEXTPtr(@index, @v);

        internal delegate void glVertexAttribL3dvEXTFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttribL3dvEXTFunc glVertexAttribL3dvEXTPtr;
        internal static void loadVertexAttribL3dvEXT()
        {
            try
            {
                glVertexAttribL3dvEXTPtr = (glVertexAttribL3dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL3dvEXT"), typeof(glVertexAttribL3dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL3dvEXT'.");
            }
        }
        public static void glVertexAttribL3dvEXT(GLuint @index, const GLdouble * @v) => glVertexAttribL3dvEXTPtr(@index, @v);

        internal delegate void glVertexAttribL4dvEXTFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttribL4dvEXTFunc glVertexAttribL4dvEXTPtr;
        internal static void loadVertexAttribL4dvEXT()
        {
            try
            {
                glVertexAttribL4dvEXTPtr = (glVertexAttribL4dvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL4dvEXT"), typeof(glVertexAttribL4dvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL4dvEXT'.");
            }
        }
        public static void glVertexAttribL4dvEXT(GLuint @index, const GLdouble * @v) => glVertexAttribL4dvEXTPtr(@index, @v);

        internal delegate void glVertexAttribLPointerEXTFunc(GLuint @index, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glVertexAttribLPointerEXTFunc glVertexAttribLPointerEXTPtr;
        internal static void loadVertexAttribLPointerEXT()
        {
            try
            {
                glVertexAttribLPointerEXTPtr = (glVertexAttribLPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribLPointerEXT"), typeof(glVertexAttribLPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribLPointerEXT'.");
            }
        }
        public static void glVertexAttribLPointerEXT(GLuint @index, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glVertexAttribLPointerEXTPtr(@index, @size, @type, @stride, @pointer);

        internal delegate void glGetVertexAttribLdvEXTFunc(GLuint @index, GLenum @pname, GLdouble * @params);
        internal static glGetVertexAttribLdvEXTFunc glGetVertexAttribLdvEXTPtr;
        internal static void loadGetVertexAttribLdvEXT()
        {
            try
            {
                glGetVertexAttribLdvEXTPtr = (glGetVertexAttribLdvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribLdvEXT"), typeof(glGetVertexAttribLdvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribLdvEXT'.");
            }
        }
        public static void glGetVertexAttribLdvEXT(GLuint @index, GLenum @pname, GLdouble * @params) => glGetVertexAttribLdvEXTPtr(@index, @pname, @params);
        #endregion
    }
}
