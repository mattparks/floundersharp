using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_stencil_clear_tag
    {
        #region Interop
        static GL_EXT_stencil_clear_tag()
        {
            Console.WriteLine("Initalising GL_EXT_stencil_clear_tag interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadStencilClearTagEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_STENCIL_TAG_BITS_EXT = 0x88F2;
        public static UInt32 GL_STENCIL_CLEAR_TAG_VALUE_EXT = 0x88F3;
        #endregion

        #region Commands
        internal delegate void glStencilClearTagEXTFunc(GLsizei @stencilTagBits, GLuint @stencilClearTag);
        internal static glStencilClearTagEXTFunc glStencilClearTagEXTPtr;
        internal static void loadStencilClearTagEXT()
        {
            try
            {
                glStencilClearTagEXTPtr = (glStencilClearTagEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilClearTagEXT"), typeof(glStencilClearTagEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilClearTagEXT'.");
            }
        }
        public static void glStencilClearTagEXT(GLsizei @stencilTagBits, GLuint @stencilClearTag) => glStencilClearTagEXTPtr(@stencilTagBits, @stencilClearTag);
        #endregion
    }
}
