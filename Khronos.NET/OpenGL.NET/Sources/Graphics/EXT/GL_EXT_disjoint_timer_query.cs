using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_disjoint_timer_query
    {
        #region Interop
        static GL_EXT_disjoint_timer_query()
        {
            Console.WriteLine("Initalising GL_EXT_disjoint_timer_query interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGenQueriesEXT();
            loadDeleteQueriesEXT();
            loadIsQueryEXT();
            loadBeginQueryEXT();
            loadEndQueryEXT();
            loadQueryCounterEXT();
            loadGetQueryivEXT();
            loadGetQueryObjectivEXT();
            loadGetQueryObjectuivEXT();
            loadGetQueryObjecti64vEXT();
            loadGetQueryObjectui64vEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_QUERY_COUNTER_BITS_EXT = 0x8864;
        public static UInt32 GL_CURRENT_QUERY_EXT = 0x8865;
        public static UInt32 GL_QUERY_RESULT_EXT = 0x8866;
        public static UInt32 GL_QUERY_RESULT_AVAILABLE_EXT = 0x8867;
        public static UInt32 GL_TIME_ELAPSED_EXT = 0x88BF;
        public static UInt32 GL_TIMESTAMP_EXT = 0x8E28;
        public static UInt32 GL_GPU_DISJOINT_EXT = 0x8FBB;
        #endregion

        #region Commands
        internal delegate void glGenQueriesEXTFunc(GLsizei @n, GLuint * @ids);
        internal static glGenQueriesEXTFunc glGenQueriesEXTPtr;
        internal static void loadGenQueriesEXT()
        {
            try
            {
                glGenQueriesEXTPtr = (glGenQueriesEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenQueriesEXT"), typeof(glGenQueriesEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenQueriesEXT'.");
            }
        }
        public static void glGenQueriesEXT(GLsizei @n, GLuint * @ids) => glGenQueriesEXTPtr(@n, @ids);

        internal delegate void glDeleteQueriesEXTFunc(GLsizei @n, const GLuint * @ids);
        internal static glDeleteQueriesEXTFunc glDeleteQueriesEXTPtr;
        internal static void loadDeleteQueriesEXT()
        {
            try
            {
                glDeleteQueriesEXTPtr = (glDeleteQueriesEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteQueriesEXT"), typeof(glDeleteQueriesEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteQueriesEXT'.");
            }
        }
        public static void glDeleteQueriesEXT(GLsizei @n, const GLuint * @ids) => glDeleteQueriesEXTPtr(@n, @ids);

        internal delegate GLboolean glIsQueryEXTFunc(GLuint @id);
        internal static glIsQueryEXTFunc glIsQueryEXTPtr;
        internal static void loadIsQueryEXT()
        {
            try
            {
                glIsQueryEXTPtr = (glIsQueryEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsQueryEXT"), typeof(glIsQueryEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsQueryEXT'.");
            }
        }
        public static GLboolean glIsQueryEXT(GLuint @id) => glIsQueryEXTPtr(@id);

        internal delegate void glBeginQueryEXTFunc(GLenum @target, GLuint @id);
        internal static glBeginQueryEXTFunc glBeginQueryEXTPtr;
        internal static void loadBeginQueryEXT()
        {
            try
            {
                glBeginQueryEXTPtr = (glBeginQueryEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginQueryEXT"), typeof(glBeginQueryEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginQueryEXT'.");
            }
        }
        public static void glBeginQueryEXT(GLenum @target, GLuint @id) => glBeginQueryEXTPtr(@target, @id);

        internal delegate void glEndQueryEXTFunc(GLenum @target);
        internal static glEndQueryEXTFunc glEndQueryEXTPtr;
        internal static void loadEndQueryEXT()
        {
            try
            {
                glEndQueryEXTPtr = (glEndQueryEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndQueryEXT"), typeof(glEndQueryEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndQueryEXT'.");
            }
        }
        public static void glEndQueryEXT(GLenum @target) => glEndQueryEXTPtr(@target);

        internal delegate void glQueryCounterEXTFunc(GLuint @id, GLenum @target);
        internal static glQueryCounterEXTFunc glQueryCounterEXTPtr;
        internal static void loadQueryCounterEXT()
        {
            try
            {
                glQueryCounterEXTPtr = (glQueryCounterEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glQueryCounterEXT"), typeof(glQueryCounterEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glQueryCounterEXT'.");
            }
        }
        public static void glQueryCounterEXT(GLuint @id, GLenum @target) => glQueryCounterEXTPtr(@id, @target);

        internal delegate void glGetQueryivEXTFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetQueryivEXTFunc glGetQueryivEXTPtr;
        internal static void loadGetQueryivEXT()
        {
            try
            {
                glGetQueryivEXTPtr = (glGetQueryivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryivEXT"), typeof(glGetQueryivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryivEXT'.");
            }
        }
        public static void glGetQueryivEXT(GLenum @target, GLenum @pname, GLint * @params) => glGetQueryivEXTPtr(@target, @pname, @params);

        internal delegate void glGetQueryObjectivEXTFunc(GLuint @id, GLenum @pname, GLint * @params);
        internal static glGetQueryObjectivEXTFunc glGetQueryObjectivEXTPtr;
        internal static void loadGetQueryObjectivEXT()
        {
            try
            {
                glGetQueryObjectivEXTPtr = (glGetQueryObjectivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjectivEXT"), typeof(glGetQueryObjectivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjectivEXT'.");
            }
        }
        public static void glGetQueryObjectivEXT(GLuint @id, GLenum @pname, GLint * @params) => glGetQueryObjectivEXTPtr(@id, @pname, @params);

        internal delegate void glGetQueryObjectuivEXTFunc(GLuint @id, GLenum @pname, GLuint * @params);
        internal static glGetQueryObjectuivEXTFunc glGetQueryObjectuivEXTPtr;
        internal static void loadGetQueryObjectuivEXT()
        {
            try
            {
                glGetQueryObjectuivEXTPtr = (glGetQueryObjectuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjectuivEXT"), typeof(glGetQueryObjectuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjectuivEXT'.");
            }
        }
        public static void glGetQueryObjectuivEXT(GLuint @id, GLenum @pname, GLuint * @params) => glGetQueryObjectuivEXTPtr(@id, @pname, @params);

        internal delegate void glGetQueryObjecti64vEXTFunc(GLuint @id, GLenum @pname, GLint64 * @params);
        internal static glGetQueryObjecti64vEXTFunc glGetQueryObjecti64vEXTPtr;
        internal static void loadGetQueryObjecti64vEXT()
        {
            try
            {
                glGetQueryObjecti64vEXTPtr = (glGetQueryObjecti64vEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjecti64vEXT"), typeof(glGetQueryObjecti64vEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjecti64vEXT'.");
            }
        }
        public static void glGetQueryObjecti64vEXT(GLuint @id, GLenum @pname, GLint64 * @params) => glGetQueryObjecti64vEXTPtr(@id, @pname, @params);

        internal delegate void glGetQueryObjectui64vEXTFunc(GLuint @id, GLenum @pname, GLuint64 * @params);
        internal static glGetQueryObjectui64vEXTFunc glGetQueryObjectui64vEXTPtr;
        internal static void loadGetQueryObjectui64vEXT()
        {
            try
            {
                glGetQueryObjectui64vEXTPtr = (glGetQueryObjectui64vEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjectui64vEXT"), typeof(glGetQueryObjectui64vEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjectui64vEXT'.");
            }
        }
        public static void glGetQueryObjectui64vEXT(GLuint @id, GLenum @pname, GLuint64 * @params) => glGetQueryObjectui64vEXTPtr(@id, @pname, @params);
        #endregion
    }
}
