using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_vertex_shader
    {
        #region Interop
        static GL_EXT_vertex_shader()
        {
            Console.WriteLine("Initalising GL_EXT_vertex_shader interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBeginVertexShaderEXT();
            loadEndVertexShaderEXT();
            loadBindVertexShaderEXT();
            loadGenVertexShadersEXT();
            loadDeleteVertexShaderEXT();
            loadShaderOp1EXT();
            loadShaderOp2EXT();
            loadShaderOp3EXT();
            loadSwizzleEXT();
            loadWriteMaskEXT();
            loadInsertComponentEXT();
            loadExtractComponentEXT();
            loadGenSymbolsEXT();
            loadSetInvariantEXT();
            loadSetLocalConstantEXT();
            loadVariantbvEXT();
            loadVariantsvEXT();
            loadVariantivEXT();
            loadVariantfvEXT();
            loadVariantdvEXT();
            loadVariantubvEXT();
            loadVariantusvEXT();
            loadVariantuivEXT();
            loadVariantPointerEXT();
            loadEnableVariantClientStateEXT();
            loadDisableVariantClientStateEXT();
            loadBindLightParameterEXT();
            loadBindMaterialParameterEXT();
            loadBindTexGenParameterEXT();
            loadBindTextureUnitParameterEXT();
            loadBindParameterEXT();
            loadIsVariantEnabledEXT();
            loadGetVariantBooleanvEXT();
            loadGetVariantIntegervEXT();
            loadGetVariantFloatvEXT();
            loadGetVariantPointervEXT();
            loadGetInvariantBooleanvEXT();
            loadGetInvariantIntegervEXT();
            loadGetInvariantFloatvEXT();
            loadGetLocalConstantBooleanvEXT();
            loadGetLocalConstantIntegervEXT();
            loadGetLocalConstantFloatvEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_SHADER_EXT = 0x8780;
        public static UInt32 GL_VERTEX_SHADER_BINDING_EXT = 0x8781;
        public static UInt32 GL_OP_INDEX_EXT = 0x8782;
        public static UInt32 GL_OP_NEGATE_EXT = 0x8783;
        public static UInt32 GL_OP_DOT3_EXT = 0x8784;
        public static UInt32 GL_OP_DOT4_EXT = 0x8785;
        public static UInt32 GL_OP_MUL_EXT = 0x8786;
        public static UInt32 GL_OP_ADD_EXT = 0x8787;
        public static UInt32 GL_OP_MADD_EXT = 0x8788;
        public static UInt32 GL_OP_FRAC_EXT = 0x8789;
        public static UInt32 GL_OP_MAX_EXT = 0x878A;
        public static UInt32 GL_OP_MIN_EXT = 0x878B;
        public static UInt32 GL_OP_SET_GE_EXT = 0x878C;
        public static UInt32 GL_OP_SET_LT_EXT = 0x878D;
        public static UInt32 GL_OP_CLAMP_EXT = 0x878E;
        public static UInt32 GL_OP_FLOOR_EXT = 0x878F;
        public static UInt32 GL_OP_ROUND_EXT = 0x8790;
        public static UInt32 GL_OP_EXP_BASE_2_EXT = 0x8791;
        public static UInt32 GL_OP_LOG_BASE_2_EXT = 0x8792;
        public static UInt32 GL_OP_POWER_EXT = 0x8793;
        public static UInt32 GL_OP_RECIP_EXT = 0x8794;
        public static UInt32 GL_OP_RECIP_SQRT_EXT = 0x8795;
        public static UInt32 GL_OP_SUB_EXT = 0x8796;
        public static UInt32 GL_OP_CROSS_PRODUCT_EXT = 0x8797;
        public static UInt32 GL_OP_MULTIPLY_MATRIX_EXT = 0x8798;
        public static UInt32 GL_OP_MOV_EXT = 0x8799;
        public static UInt32 GL_OUTPUT_VERTEX_EXT = 0x879A;
        public static UInt32 GL_OUTPUT_COLOR0_EXT = 0x879B;
        public static UInt32 GL_OUTPUT_COLOR1_EXT = 0x879C;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD0_EXT = 0x879D;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD1_EXT = 0x879E;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD2_EXT = 0x879F;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD3_EXT = 0x87A0;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD4_EXT = 0x87A1;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD5_EXT = 0x87A2;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD6_EXT = 0x87A3;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD7_EXT = 0x87A4;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD8_EXT = 0x87A5;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD9_EXT = 0x87A6;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD10_EXT = 0x87A7;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD11_EXT = 0x87A8;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD12_EXT = 0x87A9;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD13_EXT = 0x87AA;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD14_EXT = 0x87AB;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD15_EXT = 0x87AC;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD16_EXT = 0x87AD;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD17_EXT = 0x87AE;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD18_EXT = 0x87AF;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD19_EXT = 0x87B0;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD20_EXT = 0x87B1;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD21_EXT = 0x87B2;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD22_EXT = 0x87B3;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD23_EXT = 0x87B4;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD24_EXT = 0x87B5;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD25_EXT = 0x87B6;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD26_EXT = 0x87B7;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD27_EXT = 0x87B8;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD28_EXT = 0x87B9;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD29_EXT = 0x87BA;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD30_EXT = 0x87BB;
        public static UInt32 GL_OUTPUT_TEXTURE_COORD31_EXT = 0x87BC;
        public static UInt32 GL_OUTPUT_FOG_EXT = 0x87BD;
        public static UInt32 GL_SCALAR_EXT = 0x87BE;
        public static UInt32 GL_VECTOR_EXT = 0x87BF;
        public static UInt32 GL_MATRIX_EXT = 0x87C0;
        public static UInt32 GL_VARIANT_EXT = 0x87C1;
        public static UInt32 GL_INVARIANT_EXT = 0x87C2;
        public static UInt32 GL_LOCAL_CONSTANT_EXT = 0x87C3;
        public static UInt32 GL_LOCAL_EXT = 0x87C4;
        public static UInt32 GL_MAX_VERTEX_SHADER_INSTRUCTIONS_EXT = 0x87C5;
        public static UInt32 GL_MAX_VERTEX_SHADER_VARIANTS_EXT = 0x87C6;
        public static UInt32 GL_MAX_VERTEX_SHADER_INVARIANTS_EXT = 0x87C7;
        public static UInt32 GL_MAX_VERTEX_SHADER_LOCAL_CONSTANTS_EXT = 0x87C8;
        public static UInt32 GL_MAX_VERTEX_SHADER_LOCALS_EXT = 0x87C9;
        public static UInt32 GL_MAX_OPTIMIZED_VERTEX_SHADER_INSTRUCTIONS_EXT = 0x87CA;
        public static UInt32 GL_MAX_OPTIMIZED_VERTEX_SHADER_VARIANTS_EXT = 0x87CB;
        public static UInt32 GL_MAX_OPTIMIZED_VERTEX_SHADER_LOCAL_CONSTANTS_EXT = 0x87CC;
        public static UInt32 GL_MAX_OPTIMIZED_VERTEX_SHADER_INVARIANTS_EXT = 0x87CD;
        public static UInt32 GL_MAX_OPTIMIZED_VERTEX_SHADER_LOCALS_EXT = 0x87CE;
        public static UInt32 GL_VERTEX_SHADER_INSTRUCTIONS_EXT = 0x87CF;
        public static UInt32 GL_VERTEX_SHADER_VARIANTS_EXT = 0x87D0;
        public static UInt32 GL_VERTEX_SHADER_INVARIANTS_EXT = 0x87D1;
        public static UInt32 GL_VERTEX_SHADER_LOCAL_CONSTANTS_EXT = 0x87D2;
        public static UInt32 GL_VERTEX_SHADER_LOCALS_EXT = 0x87D3;
        public static UInt32 GL_VERTEX_SHADER_OPTIMIZED_EXT = 0x87D4;
        public static UInt32 GL_X_EXT = 0x87D5;
        public static UInt32 GL_Y_EXT = 0x87D6;
        public static UInt32 GL_Z_EXT = 0x87D7;
        public static UInt32 GL_W_EXT = 0x87D8;
        public static UInt32 GL_NEGATIVE_X_EXT = 0x87D9;
        public static UInt32 GL_NEGATIVE_Y_EXT = 0x87DA;
        public static UInt32 GL_NEGATIVE_Z_EXT = 0x87DB;
        public static UInt32 GL_NEGATIVE_W_EXT = 0x87DC;
        public static UInt32 GL_ZERO_EXT = 0x87DD;
        public static UInt32 GL_ONE_EXT = 0x87DE;
        public static UInt32 GL_NEGATIVE_ONE_EXT = 0x87DF;
        public static UInt32 GL_NORMALIZED_RANGE_EXT = 0x87E0;
        public static UInt32 GL_FULL_RANGE_EXT = 0x87E1;
        public static UInt32 GL_CURRENT_VERTEX_EXT = 0x87E2;
        public static UInt32 GL_MVP_MATRIX_EXT = 0x87E3;
        public static UInt32 GL_VARIANT_VALUE_EXT = 0x87E4;
        public static UInt32 GL_VARIANT_DATATYPE_EXT = 0x87E5;
        public static UInt32 GL_VARIANT_ARRAY_STRIDE_EXT = 0x87E6;
        public static UInt32 GL_VARIANT_ARRAY_TYPE_EXT = 0x87E7;
        public static UInt32 GL_VARIANT_ARRAY_EXT = 0x87E8;
        public static UInt32 GL_VARIANT_ARRAY_POINTER_EXT = 0x87E9;
        public static UInt32 GL_INVARIANT_VALUE_EXT = 0x87EA;
        public static UInt32 GL_INVARIANT_DATATYPE_EXT = 0x87EB;
        public static UInt32 GL_LOCAL_CONSTANT_VALUE_EXT = 0x87EC;
        public static UInt32 GL_LOCAL_CONSTANT_DATATYPE_EXT = 0x87ED;
        #endregion

        #region Commands
        internal delegate void glBeginVertexShaderEXTFunc();
        internal static glBeginVertexShaderEXTFunc glBeginVertexShaderEXTPtr;
        internal static void loadBeginVertexShaderEXT()
        {
            try
            {
                glBeginVertexShaderEXTPtr = (glBeginVertexShaderEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginVertexShaderEXT"), typeof(glBeginVertexShaderEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginVertexShaderEXT'.");
            }
        }
        public static void glBeginVertexShaderEXT() => glBeginVertexShaderEXTPtr();

        internal delegate void glEndVertexShaderEXTFunc();
        internal static glEndVertexShaderEXTFunc glEndVertexShaderEXTPtr;
        internal static void loadEndVertexShaderEXT()
        {
            try
            {
                glEndVertexShaderEXTPtr = (glEndVertexShaderEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndVertexShaderEXT"), typeof(glEndVertexShaderEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndVertexShaderEXT'.");
            }
        }
        public static void glEndVertexShaderEXT() => glEndVertexShaderEXTPtr();

        internal delegate void glBindVertexShaderEXTFunc(GLuint @id);
        internal static glBindVertexShaderEXTFunc glBindVertexShaderEXTPtr;
        internal static void loadBindVertexShaderEXT()
        {
            try
            {
                glBindVertexShaderEXTPtr = (glBindVertexShaderEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVertexShaderEXT"), typeof(glBindVertexShaderEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVertexShaderEXT'.");
            }
        }
        public static void glBindVertexShaderEXT(GLuint @id) => glBindVertexShaderEXTPtr(@id);

        internal delegate GLuint glGenVertexShadersEXTFunc(GLuint @range);
        internal static glGenVertexShadersEXTFunc glGenVertexShadersEXTPtr;
        internal static void loadGenVertexShadersEXT()
        {
            try
            {
                glGenVertexShadersEXTPtr = (glGenVertexShadersEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenVertexShadersEXT"), typeof(glGenVertexShadersEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenVertexShadersEXT'.");
            }
        }
        public static GLuint glGenVertexShadersEXT(GLuint @range) => glGenVertexShadersEXTPtr(@range);

        internal delegate void glDeleteVertexShaderEXTFunc(GLuint @id);
        internal static glDeleteVertexShaderEXTFunc glDeleteVertexShaderEXTPtr;
        internal static void loadDeleteVertexShaderEXT()
        {
            try
            {
                glDeleteVertexShaderEXTPtr = (glDeleteVertexShaderEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteVertexShaderEXT"), typeof(glDeleteVertexShaderEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteVertexShaderEXT'.");
            }
        }
        public static void glDeleteVertexShaderEXT(GLuint @id) => glDeleteVertexShaderEXTPtr(@id);

        internal delegate void glShaderOp1EXTFunc(GLenum @op, GLuint @res, GLuint @arg1);
        internal static glShaderOp1EXTFunc glShaderOp1EXTPtr;
        internal static void loadShaderOp1EXT()
        {
            try
            {
                glShaderOp1EXTPtr = (glShaderOp1EXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShaderOp1EXT"), typeof(glShaderOp1EXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShaderOp1EXT'.");
            }
        }
        public static void glShaderOp1EXT(GLenum @op, GLuint @res, GLuint @arg1) => glShaderOp1EXTPtr(@op, @res, @arg1);

        internal delegate void glShaderOp2EXTFunc(GLenum @op, GLuint @res, GLuint @arg1, GLuint @arg2);
        internal static glShaderOp2EXTFunc glShaderOp2EXTPtr;
        internal static void loadShaderOp2EXT()
        {
            try
            {
                glShaderOp2EXTPtr = (glShaderOp2EXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShaderOp2EXT"), typeof(glShaderOp2EXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShaderOp2EXT'.");
            }
        }
        public static void glShaderOp2EXT(GLenum @op, GLuint @res, GLuint @arg1, GLuint @arg2) => glShaderOp2EXTPtr(@op, @res, @arg1, @arg2);

        internal delegate void glShaderOp3EXTFunc(GLenum @op, GLuint @res, GLuint @arg1, GLuint @arg2, GLuint @arg3);
        internal static glShaderOp3EXTFunc glShaderOp3EXTPtr;
        internal static void loadShaderOp3EXT()
        {
            try
            {
                glShaderOp3EXTPtr = (glShaderOp3EXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShaderOp3EXT"), typeof(glShaderOp3EXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShaderOp3EXT'.");
            }
        }
        public static void glShaderOp3EXT(GLenum @op, GLuint @res, GLuint @arg1, GLuint @arg2, GLuint @arg3) => glShaderOp3EXTPtr(@op, @res, @arg1, @arg2, @arg3);

        internal delegate void glSwizzleEXTFunc(GLuint @res, GLuint @in, GLenum @outX, GLenum @outY, GLenum @outZ, GLenum @outW);
        internal static glSwizzleEXTFunc glSwizzleEXTPtr;
        internal static void loadSwizzleEXT()
        {
            try
            {
                glSwizzleEXTPtr = (glSwizzleEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSwizzleEXT"), typeof(glSwizzleEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSwizzleEXT'.");
            }
        }
        public static void glSwizzleEXT(GLuint @res, GLuint @in, GLenum @outX, GLenum @outY, GLenum @outZ, GLenum @outW) => glSwizzleEXTPtr(@res, @in, @outX, @outY, @outZ, @outW);

        internal delegate void glWriteMaskEXTFunc(GLuint @res, GLuint @in, GLenum @outX, GLenum @outY, GLenum @outZ, GLenum @outW);
        internal static glWriteMaskEXTFunc glWriteMaskEXTPtr;
        internal static void loadWriteMaskEXT()
        {
            try
            {
                glWriteMaskEXTPtr = (glWriteMaskEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWriteMaskEXT"), typeof(glWriteMaskEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWriteMaskEXT'.");
            }
        }
        public static void glWriteMaskEXT(GLuint @res, GLuint @in, GLenum @outX, GLenum @outY, GLenum @outZ, GLenum @outW) => glWriteMaskEXTPtr(@res, @in, @outX, @outY, @outZ, @outW);

        internal delegate void glInsertComponentEXTFunc(GLuint @res, GLuint @src, GLuint @num);
        internal static glInsertComponentEXTFunc glInsertComponentEXTPtr;
        internal static void loadInsertComponentEXT()
        {
            try
            {
                glInsertComponentEXTPtr = (glInsertComponentEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInsertComponentEXT"), typeof(glInsertComponentEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInsertComponentEXT'.");
            }
        }
        public static void glInsertComponentEXT(GLuint @res, GLuint @src, GLuint @num) => glInsertComponentEXTPtr(@res, @src, @num);

        internal delegate void glExtractComponentEXTFunc(GLuint @res, GLuint @src, GLuint @num);
        internal static glExtractComponentEXTFunc glExtractComponentEXTPtr;
        internal static void loadExtractComponentEXT()
        {
            try
            {
                glExtractComponentEXTPtr = (glExtractComponentEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtractComponentEXT"), typeof(glExtractComponentEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtractComponentEXT'.");
            }
        }
        public static void glExtractComponentEXT(GLuint @res, GLuint @src, GLuint @num) => glExtractComponentEXTPtr(@res, @src, @num);

        internal delegate GLuint glGenSymbolsEXTFunc(GLenum @datatype, GLenum @storagetype, GLenum @range, GLuint @components);
        internal static glGenSymbolsEXTFunc glGenSymbolsEXTPtr;
        internal static void loadGenSymbolsEXT()
        {
            try
            {
                glGenSymbolsEXTPtr = (glGenSymbolsEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenSymbolsEXT"), typeof(glGenSymbolsEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenSymbolsEXT'.");
            }
        }
        public static GLuint glGenSymbolsEXT(GLenum @datatype, GLenum @storagetype, GLenum @range, GLuint @components) => glGenSymbolsEXTPtr(@datatype, @storagetype, @range, @components);

        internal delegate void glSetInvariantEXTFunc(GLuint @id, GLenum @type, const void * @addr);
        internal static glSetInvariantEXTFunc glSetInvariantEXTPtr;
        internal static void loadSetInvariantEXT()
        {
            try
            {
                glSetInvariantEXTPtr = (glSetInvariantEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSetInvariantEXT"), typeof(glSetInvariantEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSetInvariantEXT'.");
            }
        }
        public static void glSetInvariantEXT(GLuint @id, GLenum @type, const void * @addr) => glSetInvariantEXTPtr(@id, @type, @addr);

        internal delegate void glSetLocalConstantEXTFunc(GLuint @id, GLenum @type, const void * @addr);
        internal static glSetLocalConstantEXTFunc glSetLocalConstantEXTPtr;
        internal static void loadSetLocalConstantEXT()
        {
            try
            {
                glSetLocalConstantEXTPtr = (glSetLocalConstantEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSetLocalConstantEXT"), typeof(glSetLocalConstantEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSetLocalConstantEXT'.");
            }
        }
        public static void glSetLocalConstantEXT(GLuint @id, GLenum @type, const void * @addr) => glSetLocalConstantEXTPtr(@id, @type, @addr);

        internal delegate void glVariantbvEXTFunc(GLuint @id, const GLbyte * @addr);
        internal static glVariantbvEXTFunc glVariantbvEXTPtr;
        internal static void loadVariantbvEXT()
        {
            try
            {
                glVariantbvEXTPtr = (glVariantbvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVariantbvEXT"), typeof(glVariantbvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVariantbvEXT'.");
            }
        }
        public static void glVariantbvEXT(GLuint @id, const GLbyte * @addr) => glVariantbvEXTPtr(@id, @addr);

        internal delegate void glVariantsvEXTFunc(GLuint @id, const GLshort * @addr);
        internal static glVariantsvEXTFunc glVariantsvEXTPtr;
        internal static void loadVariantsvEXT()
        {
            try
            {
                glVariantsvEXTPtr = (glVariantsvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVariantsvEXT"), typeof(glVariantsvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVariantsvEXT'.");
            }
        }
        public static void glVariantsvEXT(GLuint @id, const GLshort * @addr) => glVariantsvEXTPtr(@id, @addr);

        internal delegate void glVariantivEXTFunc(GLuint @id, const GLint * @addr);
        internal static glVariantivEXTFunc glVariantivEXTPtr;
        internal static void loadVariantivEXT()
        {
            try
            {
                glVariantivEXTPtr = (glVariantivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVariantivEXT"), typeof(glVariantivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVariantivEXT'.");
            }
        }
        public static void glVariantivEXT(GLuint @id, const GLint * @addr) => glVariantivEXTPtr(@id, @addr);

        internal delegate void glVariantfvEXTFunc(GLuint @id, const GLfloat * @addr);
        internal static glVariantfvEXTFunc glVariantfvEXTPtr;
        internal static void loadVariantfvEXT()
        {
            try
            {
                glVariantfvEXTPtr = (glVariantfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVariantfvEXT"), typeof(glVariantfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVariantfvEXT'.");
            }
        }
        public static void glVariantfvEXT(GLuint @id, const GLfloat * @addr) => glVariantfvEXTPtr(@id, @addr);

        internal delegate void glVariantdvEXTFunc(GLuint @id, const GLdouble * @addr);
        internal static glVariantdvEXTFunc glVariantdvEXTPtr;
        internal static void loadVariantdvEXT()
        {
            try
            {
                glVariantdvEXTPtr = (glVariantdvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVariantdvEXT"), typeof(glVariantdvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVariantdvEXT'.");
            }
        }
        public static void glVariantdvEXT(GLuint @id, const GLdouble * @addr) => glVariantdvEXTPtr(@id, @addr);

        internal delegate void glVariantubvEXTFunc(GLuint @id, const GLubyte * @addr);
        internal static glVariantubvEXTFunc glVariantubvEXTPtr;
        internal static void loadVariantubvEXT()
        {
            try
            {
                glVariantubvEXTPtr = (glVariantubvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVariantubvEXT"), typeof(glVariantubvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVariantubvEXT'.");
            }
        }
        public static void glVariantubvEXT(GLuint @id, const GLubyte * @addr) => glVariantubvEXTPtr(@id, @addr);

        internal delegate void glVariantusvEXTFunc(GLuint @id, const GLushort * @addr);
        internal static glVariantusvEXTFunc glVariantusvEXTPtr;
        internal static void loadVariantusvEXT()
        {
            try
            {
                glVariantusvEXTPtr = (glVariantusvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVariantusvEXT"), typeof(glVariantusvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVariantusvEXT'.");
            }
        }
        public static void glVariantusvEXT(GLuint @id, const GLushort * @addr) => glVariantusvEXTPtr(@id, @addr);

        internal delegate void glVariantuivEXTFunc(GLuint @id, const GLuint * @addr);
        internal static glVariantuivEXTFunc glVariantuivEXTPtr;
        internal static void loadVariantuivEXT()
        {
            try
            {
                glVariantuivEXTPtr = (glVariantuivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVariantuivEXT"), typeof(glVariantuivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVariantuivEXT'.");
            }
        }
        public static void glVariantuivEXT(GLuint @id, const GLuint * @addr) => glVariantuivEXTPtr(@id, @addr);

        internal delegate void glVariantPointerEXTFunc(GLuint @id, GLenum @type, GLuint @stride, const void * @addr);
        internal static glVariantPointerEXTFunc glVariantPointerEXTPtr;
        internal static void loadVariantPointerEXT()
        {
            try
            {
                glVariantPointerEXTPtr = (glVariantPointerEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVariantPointerEXT"), typeof(glVariantPointerEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVariantPointerEXT'.");
            }
        }
        public static void glVariantPointerEXT(GLuint @id, GLenum @type, GLuint @stride, const void * @addr) => glVariantPointerEXTPtr(@id, @type, @stride, @addr);

        internal delegate void glEnableVariantClientStateEXTFunc(GLuint @id);
        internal static glEnableVariantClientStateEXTFunc glEnableVariantClientStateEXTPtr;
        internal static void loadEnableVariantClientStateEXT()
        {
            try
            {
                glEnableVariantClientStateEXTPtr = (glEnableVariantClientStateEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableVariantClientStateEXT"), typeof(glEnableVariantClientStateEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableVariantClientStateEXT'.");
            }
        }
        public static void glEnableVariantClientStateEXT(GLuint @id) => glEnableVariantClientStateEXTPtr(@id);

        internal delegate void glDisableVariantClientStateEXTFunc(GLuint @id);
        internal static glDisableVariantClientStateEXTFunc glDisableVariantClientStateEXTPtr;
        internal static void loadDisableVariantClientStateEXT()
        {
            try
            {
                glDisableVariantClientStateEXTPtr = (glDisableVariantClientStateEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableVariantClientStateEXT"), typeof(glDisableVariantClientStateEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableVariantClientStateEXT'.");
            }
        }
        public static void glDisableVariantClientStateEXT(GLuint @id) => glDisableVariantClientStateEXTPtr(@id);

        internal delegate GLuint glBindLightParameterEXTFunc(GLenum @light, GLenum @value);
        internal static glBindLightParameterEXTFunc glBindLightParameterEXTPtr;
        internal static void loadBindLightParameterEXT()
        {
            try
            {
                glBindLightParameterEXTPtr = (glBindLightParameterEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindLightParameterEXT"), typeof(glBindLightParameterEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindLightParameterEXT'.");
            }
        }
        public static GLuint glBindLightParameterEXT(GLenum @light, GLenum @value) => glBindLightParameterEXTPtr(@light, @value);

        internal delegate GLuint glBindMaterialParameterEXTFunc(GLenum @face, GLenum @value);
        internal static glBindMaterialParameterEXTFunc glBindMaterialParameterEXTPtr;
        internal static void loadBindMaterialParameterEXT()
        {
            try
            {
                glBindMaterialParameterEXTPtr = (glBindMaterialParameterEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindMaterialParameterEXT"), typeof(glBindMaterialParameterEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindMaterialParameterEXT'.");
            }
        }
        public static GLuint glBindMaterialParameterEXT(GLenum @face, GLenum @value) => glBindMaterialParameterEXTPtr(@face, @value);

        internal delegate GLuint glBindTexGenParameterEXTFunc(GLenum @unit, GLenum @coord, GLenum @value);
        internal static glBindTexGenParameterEXTFunc glBindTexGenParameterEXTPtr;
        internal static void loadBindTexGenParameterEXT()
        {
            try
            {
                glBindTexGenParameterEXTPtr = (glBindTexGenParameterEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTexGenParameterEXT"), typeof(glBindTexGenParameterEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTexGenParameterEXT'.");
            }
        }
        public static GLuint glBindTexGenParameterEXT(GLenum @unit, GLenum @coord, GLenum @value) => glBindTexGenParameterEXTPtr(@unit, @coord, @value);

        internal delegate GLuint glBindTextureUnitParameterEXTFunc(GLenum @unit, GLenum @value);
        internal static glBindTextureUnitParameterEXTFunc glBindTextureUnitParameterEXTPtr;
        internal static void loadBindTextureUnitParameterEXT()
        {
            try
            {
                glBindTextureUnitParameterEXTPtr = (glBindTextureUnitParameterEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTextureUnitParameterEXT"), typeof(glBindTextureUnitParameterEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTextureUnitParameterEXT'.");
            }
        }
        public static GLuint glBindTextureUnitParameterEXT(GLenum @unit, GLenum @value) => glBindTextureUnitParameterEXTPtr(@unit, @value);

        internal delegate GLuint glBindParameterEXTFunc(GLenum @value);
        internal static glBindParameterEXTFunc glBindParameterEXTPtr;
        internal static void loadBindParameterEXT()
        {
            try
            {
                glBindParameterEXTPtr = (glBindParameterEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindParameterEXT"), typeof(glBindParameterEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindParameterEXT'.");
            }
        }
        public static GLuint glBindParameterEXT(GLenum @value) => glBindParameterEXTPtr(@value);

        internal delegate GLboolean glIsVariantEnabledEXTFunc(GLuint @id, GLenum @cap);
        internal static glIsVariantEnabledEXTFunc glIsVariantEnabledEXTPtr;
        internal static void loadIsVariantEnabledEXT()
        {
            try
            {
                glIsVariantEnabledEXTPtr = (glIsVariantEnabledEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsVariantEnabledEXT"), typeof(glIsVariantEnabledEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsVariantEnabledEXT'.");
            }
        }
        public static GLboolean glIsVariantEnabledEXT(GLuint @id, GLenum @cap) => glIsVariantEnabledEXTPtr(@id, @cap);

        internal delegate void glGetVariantBooleanvEXTFunc(GLuint @id, GLenum @value, GLboolean * @data);
        internal static glGetVariantBooleanvEXTFunc glGetVariantBooleanvEXTPtr;
        internal static void loadGetVariantBooleanvEXT()
        {
            try
            {
                glGetVariantBooleanvEXTPtr = (glGetVariantBooleanvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVariantBooleanvEXT"), typeof(glGetVariantBooleanvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVariantBooleanvEXT'.");
            }
        }
        public static void glGetVariantBooleanvEXT(GLuint @id, GLenum @value, GLboolean * @data) => glGetVariantBooleanvEXTPtr(@id, @value, @data);

        internal delegate void glGetVariantIntegervEXTFunc(GLuint @id, GLenum @value, GLint * @data);
        internal static glGetVariantIntegervEXTFunc glGetVariantIntegervEXTPtr;
        internal static void loadGetVariantIntegervEXT()
        {
            try
            {
                glGetVariantIntegervEXTPtr = (glGetVariantIntegervEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVariantIntegervEXT"), typeof(glGetVariantIntegervEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVariantIntegervEXT'.");
            }
        }
        public static void glGetVariantIntegervEXT(GLuint @id, GLenum @value, GLint * @data) => glGetVariantIntegervEXTPtr(@id, @value, @data);

        internal delegate void glGetVariantFloatvEXTFunc(GLuint @id, GLenum @value, GLfloat * @data);
        internal static glGetVariantFloatvEXTFunc glGetVariantFloatvEXTPtr;
        internal static void loadGetVariantFloatvEXT()
        {
            try
            {
                glGetVariantFloatvEXTPtr = (glGetVariantFloatvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVariantFloatvEXT"), typeof(glGetVariantFloatvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVariantFloatvEXT'.");
            }
        }
        public static void glGetVariantFloatvEXT(GLuint @id, GLenum @value, GLfloat * @data) => glGetVariantFloatvEXTPtr(@id, @value, @data);

        internal delegate void glGetVariantPointervEXTFunc(GLuint @id, GLenum @value, void ** @data);
        internal static glGetVariantPointervEXTFunc glGetVariantPointervEXTPtr;
        internal static void loadGetVariantPointervEXT()
        {
            try
            {
                glGetVariantPointervEXTPtr = (glGetVariantPointervEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVariantPointervEXT"), typeof(glGetVariantPointervEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVariantPointervEXT'.");
            }
        }
        public static void glGetVariantPointervEXT(GLuint @id, GLenum @value, void ** @data) => glGetVariantPointervEXTPtr(@id, @value, @data);

        internal delegate void glGetInvariantBooleanvEXTFunc(GLuint @id, GLenum @value, GLboolean * @data);
        internal static glGetInvariantBooleanvEXTFunc glGetInvariantBooleanvEXTPtr;
        internal static void loadGetInvariantBooleanvEXT()
        {
            try
            {
                glGetInvariantBooleanvEXTPtr = (glGetInvariantBooleanvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInvariantBooleanvEXT"), typeof(glGetInvariantBooleanvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInvariantBooleanvEXT'.");
            }
        }
        public static void glGetInvariantBooleanvEXT(GLuint @id, GLenum @value, GLboolean * @data) => glGetInvariantBooleanvEXTPtr(@id, @value, @data);

        internal delegate void glGetInvariantIntegervEXTFunc(GLuint @id, GLenum @value, GLint * @data);
        internal static glGetInvariantIntegervEXTFunc glGetInvariantIntegervEXTPtr;
        internal static void loadGetInvariantIntegervEXT()
        {
            try
            {
                glGetInvariantIntegervEXTPtr = (glGetInvariantIntegervEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInvariantIntegervEXT"), typeof(glGetInvariantIntegervEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInvariantIntegervEXT'.");
            }
        }
        public static void glGetInvariantIntegervEXT(GLuint @id, GLenum @value, GLint * @data) => glGetInvariantIntegervEXTPtr(@id, @value, @data);

        internal delegate void glGetInvariantFloatvEXTFunc(GLuint @id, GLenum @value, GLfloat * @data);
        internal static glGetInvariantFloatvEXTFunc glGetInvariantFloatvEXTPtr;
        internal static void loadGetInvariantFloatvEXT()
        {
            try
            {
                glGetInvariantFloatvEXTPtr = (glGetInvariantFloatvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInvariantFloatvEXT"), typeof(glGetInvariantFloatvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInvariantFloatvEXT'.");
            }
        }
        public static void glGetInvariantFloatvEXT(GLuint @id, GLenum @value, GLfloat * @data) => glGetInvariantFloatvEXTPtr(@id, @value, @data);

        internal delegate void glGetLocalConstantBooleanvEXTFunc(GLuint @id, GLenum @value, GLboolean * @data);
        internal static glGetLocalConstantBooleanvEXTFunc glGetLocalConstantBooleanvEXTPtr;
        internal static void loadGetLocalConstantBooleanvEXT()
        {
            try
            {
                glGetLocalConstantBooleanvEXTPtr = (glGetLocalConstantBooleanvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetLocalConstantBooleanvEXT"), typeof(glGetLocalConstantBooleanvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetLocalConstantBooleanvEXT'.");
            }
        }
        public static void glGetLocalConstantBooleanvEXT(GLuint @id, GLenum @value, GLboolean * @data) => glGetLocalConstantBooleanvEXTPtr(@id, @value, @data);

        internal delegate void glGetLocalConstantIntegervEXTFunc(GLuint @id, GLenum @value, GLint * @data);
        internal static glGetLocalConstantIntegervEXTFunc glGetLocalConstantIntegervEXTPtr;
        internal static void loadGetLocalConstantIntegervEXT()
        {
            try
            {
                glGetLocalConstantIntegervEXTPtr = (glGetLocalConstantIntegervEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetLocalConstantIntegervEXT"), typeof(glGetLocalConstantIntegervEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetLocalConstantIntegervEXT'.");
            }
        }
        public static void glGetLocalConstantIntegervEXT(GLuint @id, GLenum @value, GLint * @data) => glGetLocalConstantIntegervEXTPtr(@id, @value, @data);

        internal delegate void glGetLocalConstantFloatvEXTFunc(GLuint @id, GLenum @value, GLfloat * @data);
        internal static glGetLocalConstantFloatvEXTFunc glGetLocalConstantFloatvEXTPtr;
        internal static void loadGetLocalConstantFloatvEXT()
        {
            try
            {
                glGetLocalConstantFloatvEXTPtr = (glGetLocalConstantFloatvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetLocalConstantFloatvEXT"), typeof(glGetLocalConstantFloatvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetLocalConstantFloatvEXT'.");
            }
        }
        public static void glGetLocalConstantFloatvEXT(GLuint @id, GLenum @value, GLfloat * @data) => glGetLocalConstantFloatvEXTPtr(@id, @value, @data);
        #endregion
    }
}
