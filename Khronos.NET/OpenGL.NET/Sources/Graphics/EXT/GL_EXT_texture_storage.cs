using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_texture_storage
    {
        #region Interop
        static GL_EXT_texture_storage()
        {
            Console.WriteLine("Initalising GL_EXT_texture_storage interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexStorage1DEXT();
            loadTexStorage2DEXT();
            loadTexStorage3DEXT();
            loadTextureStorage1DEXT();
            loadTextureStorage2DEXT();
            loadTextureStorage3DEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_IMMUTABLE_FORMAT_EXT = 0x912F;
        public static UInt32 GL_ALPHA8_EXT = 0x803C;
        public static UInt32 GL_LUMINANCE8_EXT = 0x8040;
        public static UInt32 GL_LUMINANCE8_ALPHA8_EXT = 0x8045;
        public static UInt32 GL_RGBA32F_EXT = 0x8814;
        public static UInt32 GL_RGB32F_EXT = 0x8815;
        public static UInt32 GL_ALPHA32F_EXT = 0x8816;
        public static UInt32 GL_LUMINANCE32F_EXT = 0x8818;
        public static UInt32 GL_LUMINANCE_ALPHA32F_EXT = 0x8819;
        public static UInt32 GL_RGBA16F_EXT = 0x881A;
        public static UInt32 GL_RGB16F_EXT = 0x881B;
        public static UInt32 GL_ALPHA16F_EXT = 0x881C;
        public static UInt32 GL_LUMINANCE16F_EXT = 0x881E;
        public static UInt32 GL_LUMINANCE_ALPHA16F_EXT = 0x881F;
        public static UInt32 GL_RGB10_A2_EXT = 0x8059;
        public static UInt32 GL_RGB10_EXT = 0x8052;
        public static UInt32 GL_BGRA8_EXT = 0x93A1;
        public static UInt32 GL_R8_EXT = 0x8229;
        public static UInt32 GL_RG8_EXT = 0x822B;
        public static UInt32 GL_R32F_EXT = 0x822E;
        public static UInt32 GL_RG32F_EXT = 0x8230;
        public static UInt32 GL_R16F_EXT = 0x822D;
        public static UInt32 GL_RG16F_EXT = 0x822F;
        #endregion

        #region Commands
        internal delegate void glTexStorage1DEXTFunc(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width);
        internal static glTexStorage1DEXTFunc glTexStorage1DEXTPtr;
        internal static void loadTexStorage1DEXT()
        {
            try
            {
                glTexStorage1DEXTPtr = (glTexStorage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage1DEXT"), typeof(glTexStorage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage1DEXT'.");
            }
        }
        public static void glTexStorage1DEXT(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width) => glTexStorage1DEXTPtr(@target, @levels, @internalformat, @width);

        internal delegate void glTexStorage2DEXTFunc(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glTexStorage2DEXTFunc glTexStorage2DEXTPtr;
        internal static void loadTexStorage2DEXT()
        {
            try
            {
                glTexStorage2DEXTPtr = (glTexStorage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage2DEXT"), typeof(glTexStorage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage2DEXT'.");
            }
        }
        public static void glTexStorage2DEXT(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height) => glTexStorage2DEXTPtr(@target, @levels, @internalformat, @width, @height);

        internal delegate void glTexStorage3DEXTFunc(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth);
        internal static glTexStorage3DEXTFunc glTexStorage3DEXTPtr;
        internal static void loadTexStorage3DEXT()
        {
            try
            {
                glTexStorage3DEXTPtr = (glTexStorage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage3DEXT"), typeof(glTexStorage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage3DEXT'.");
            }
        }
        public static void glTexStorage3DEXT(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth) => glTexStorage3DEXTPtr(@target, @levels, @internalformat, @width, @height, @depth);

        internal delegate void glTextureStorage1DEXTFunc(GLuint @texture, GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width);
        internal static glTextureStorage1DEXTFunc glTextureStorage1DEXTPtr;
        internal static void loadTextureStorage1DEXT()
        {
            try
            {
                glTextureStorage1DEXTPtr = (glTextureStorage1DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage1DEXT"), typeof(glTextureStorage1DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage1DEXT'.");
            }
        }
        public static void glTextureStorage1DEXT(GLuint @texture, GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width) => glTextureStorage1DEXTPtr(@texture, @target, @levels, @internalformat, @width);

        internal delegate void glTextureStorage2DEXTFunc(GLuint @texture, GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glTextureStorage2DEXTFunc glTextureStorage2DEXTPtr;
        internal static void loadTextureStorage2DEXT()
        {
            try
            {
                glTextureStorage2DEXTPtr = (glTextureStorage2DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage2DEXT"), typeof(glTextureStorage2DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage2DEXT'.");
            }
        }
        public static void glTextureStorage2DEXT(GLuint @texture, GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height) => glTextureStorage2DEXTPtr(@texture, @target, @levels, @internalformat, @width, @height);

        internal delegate void glTextureStorage3DEXTFunc(GLuint @texture, GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth);
        internal static glTextureStorage3DEXTFunc glTextureStorage3DEXTPtr;
        internal static void loadTextureStorage3DEXT()
        {
            try
            {
                glTextureStorage3DEXTPtr = (glTextureStorage3DEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage3DEXT"), typeof(glTextureStorage3DEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage3DEXT'.");
            }
        }
        public static void glTextureStorage3DEXT(GLuint @texture, GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth) => glTextureStorage3DEXTPtr(@texture, @target, @levels, @internalformat, @width, @height, @depth);
        #endregion
    }
}
