using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_bindable_uniform
    {
        #region Interop
        static GL_EXT_bindable_uniform()
        {
            Console.WriteLine("Initalising GL_EXT_bindable_uniform interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadUniformBufferEXT();
            loadGetUniformBufferSizeEXT();
            loadGetUniformOffsetEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_VERTEX_BINDABLE_UNIFORMS_EXT = 0x8DE2;
        public static UInt32 GL_MAX_FRAGMENT_BINDABLE_UNIFORMS_EXT = 0x8DE3;
        public static UInt32 GL_MAX_GEOMETRY_BINDABLE_UNIFORMS_EXT = 0x8DE4;
        public static UInt32 GL_MAX_BINDABLE_UNIFORM_SIZE_EXT = 0x8DED;
        public static UInt32 GL_UNIFORM_BUFFER_EXT = 0x8DEE;
        public static UInt32 GL_UNIFORM_BUFFER_BINDING_EXT = 0x8DEF;
        #endregion

        #region Commands
        internal delegate void glUniformBufferEXTFunc(GLuint @program, GLint @location, GLuint @buffer);
        internal static glUniformBufferEXTFunc glUniformBufferEXTPtr;
        internal static void loadUniformBufferEXT()
        {
            try
            {
                glUniformBufferEXTPtr = (glUniformBufferEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformBufferEXT"), typeof(glUniformBufferEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformBufferEXT'.");
            }
        }
        public static void glUniformBufferEXT(GLuint @program, GLint @location, GLuint @buffer) => glUniformBufferEXTPtr(@program, @location, @buffer);

        internal delegate GLint glGetUniformBufferSizeEXTFunc(GLuint @program, GLint @location);
        internal static glGetUniformBufferSizeEXTFunc glGetUniformBufferSizeEXTPtr;
        internal static void loadGetUniformBufferSizeEXT()
        {
            try
            {
                glGetUniformBufferSizeEXTPtr = (glGetUniformBufferSizeEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformBufferSizeEXT"), typeof(glGetUniformBufferSizeEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformBufferSizeEXT'.");
            }
        }
        public static GLint glGetUniformBufferSizeEXT(GLuint @program, GLint @location) => glGetUniformBufferSizeEXTPtr(@program, @location);

        internal delegate GLintptr glGetUniformOffsetEXTFunc(GLuint @program, GLint @location);
        internal static glGetUniformOffsetEXTFunc glGetUniformOffsetEXTPtr;
        internal static void loadGetUniformOffsetEXT()
        {
            try
            {
                glGetUniformOffsetEXTPtr = (glGetUniformOffsetEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformOffsetEXT"), typeof(glGetUniformOffsetEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformOffsetEXT'.");
            }
        }
        public static GLintptr glGetUniformOffsetEXT(GLuint @program, GLint @location) => glGetUniformOffsetEXTPtr(@program, @location);
        #endregion
    }
}
