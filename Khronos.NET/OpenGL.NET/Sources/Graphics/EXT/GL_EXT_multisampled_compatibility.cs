using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_multisampled_compatibility
    {
        #region Interop
        static GL_EXT_multisampled_compatibility()
        {
            Console.WriteLine("Initalising GL_EXT_multisampled_compatibility interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MULTISAMPLE_EXT = 0x809D;
        public static UInt32 GL_SAMPLE_ALPHA_TO_ONE_EXT = 0x809F;
        #endregion

        #region Commands
        #endregion
    }
}
