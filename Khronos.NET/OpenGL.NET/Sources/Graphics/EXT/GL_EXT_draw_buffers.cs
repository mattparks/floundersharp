using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_draw_buffers
    {
        #region Interop
        static GL_EXT_draw_buffers()
        {
            Console.WriteLine("Initalising GL_EXT_draw_buffers interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawBuffersEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_COLOR_ATTACHMENTS_EXT = 0x8CDF;
        public static UInt32 GL_MAX_DRAW_BUFFERS_EXT = 0x8824;
        public static UInt32 GL_DRAW_BUFFER0_EXT = 0x8825;
        public static UInt32 GL_DRAW_BUFFER1_EXT = 0x8826;
        public static UInt32 GL_DRAW_BUFFER2_EXT = 0x8827;
        public static UInt32 GL_DRAW_BUFFER3_EXT = 0x8828;
        public static UInt32 GL_DRAW_BUFFER4_EXT = 0x8829;
        public static UInt32 GL_DRAW_BUFFER5_EXT = 0x882A;
        public static UInt32 GL_DRAW_BUFFER6_EXT = 0x882B;
        public static UInt32 GL_DRAW_BUFFER7_EXT = 0x882C;
        public static UInt32 GL_DRAW_BUFFER8_EXT = 0x882D;
        public static UInt32 GL_DRAW_BUFFER9_EXT = 0x882E;
        public static UInt32 GL_DRAW_BUFFER10_EXT = 0x882F;
        public static UInt32 GL_DRAW_BUFFER11_EXT = 0x8830;
        public static UInt32 GL_DRAW_BUFFER12_EXT = 0x8831;
        public static UInt32 GL_DRAW_BUFFER13_EXT = 0x8832;
        public static UInt32 GL_DRAW_BUFFER14_EXT = 0x8833;
        public static UInt32 GL_DRAW_BUFFER15_EXT = 0x8834;
        public static UInt32 GL_COLOR_ATTACHMENT0_EXT = 0x8CE0;
        public static UInt32 GL_COLOR_ATTACHMENT1_EXT = 0x8CE1;
        public static UInt32 GL_COLOR_ATTACHMENT2_EXT = 0x8CE2;
        public static UInt32 GL_COLOR_ATTACHMENT3_EXT = 0x8CE3;
        public static UInt32 GL_COLOR_ATTACHMENT4_EXT = 0x8CE4;
        public static UInt32 GL_COLOR_ATTACHMENT5_EXT = 0x8CE5;
        public static UInt32 GL_COLOR_ATTACHMENT6_EXT = 0x8CE6;
        public static UInt32 GL_COLOR_ATTACHMENT7_EXT = 0x8CE7;
        public static UInt32 GL_COLOR_ATTACHMENT8_EXT = 0x8CE8;
        public static UInt32 GL_COLOR_ATTACHMENT9_EXT = 0x8CE9;
        public static UInt32 GL_COLOR_ATTACHMENT10_EXT = 0x8CEA;
        public static UInt32 GL_COLOR_ATTACHMENT11_EXT = 0x8CEB;
        public static UInt32 GL_COLOR_ATTACHMENT12_EXT = 0x8CEC;
        public static UInt32 GL_COLOR_ATTACHMENT13_EXT = 0x8CED;
        public static UInt32 GL_COLOR_ATTACHMENT14_EXT = 0x8CEE;
        public static UInt32 GL_COLOR_ATTACHMENT15_EXT = 0x8CEF;
        #endregion

        #region Commands
        internal delegate void glDrawBuffersEXTFunc(GLsizei @n, const GLenum * @bufs);
        internal static glDrawBuffersEXTFunc glDrawBuffersEXTPtr;
        internal static void loadDrawBuffersEXT()
        {
            try
            {
                glDrawBuffersEXTPtr = (glDrawBuffersEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawBuffersEXT"), typeof(glDrawBuffersEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawBuffersEXT'.");
            }
        }
        public static void glDrawBuffersEXT(GLsizei @n, const GLenum * @bufs) => glDrawBuffersEXTPtr(@n, @bufs);
        #endregion
    }
}
