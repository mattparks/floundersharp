using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_EXT_histogram
    {
        #region Interop
        static GL_EXT_histogram()
        {
            Console.WriteLine("Initalising GL_EXT_histogram interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetHistogramEXT();
            loadGetHistogramParameterfvEXT();
            loadGetHistogramParameterivEXT();
            loadGetMinmaxEXT();
            loadGetMinmaxParameterfvEXT();
            loadGetMinmaxParameterivEXT();
            loadHistogramEXT();
            loadMinmaxEXT();
            loadResetHistogramEXT();
            loadResetMinmaxEXT();
        }
        #endregion

        #region Enums
        public static UInt32 GL_HISTOGRAM_EXT = 0x8024;
        public static UInt32 GL_PROXY_HISTOGRAM_EXT = 0x8025;
        public static UInt32 GL_HISTOGRAM_WIDTH_EXT = 0x8026;
        public static UInt32 GL_HISTOGRAM_FORMAT_EXT = 0x8027;
        public static UInt32 GL_HISTOGRAM_RED_SIZE_EXT = 0x8028;
        public static UInt32 GL_HISTOGRAM_GREEN_SIZE_EXT = 0x8029;
        public static UInt32 GL_HISTOGRAM_BLUE_SIZE_EXT = 0x802A;
        public static UInt32 GL_HISTOGRAM_ALPHA_SIZE_EXT = 0x802B;
        public static UInt32 GL_HISTOGRAM_LUMINANCE_SIZE_EXT = 0x802C;
        public static UInt32 GL_HISTOGRAM_SINK_EXT = 0x802D;
        public static UInt32 GL_MINMAX_EXT = 0x802E;
        public static UInt32 GL_MINMAX_FORMAT_EXT = 0x802F;
        public static UInt32 GL_MINMAX_SINK_EXT = 0x8030;
        public static UInt32 GL_TABLE_TOO_LARGE_EXT = 0x8031;
        #endregion

        #region Commands
        internal delegate void glGetHistogramEXTFunc(GLenum @target, GLboolean @reset, GLenum @format, GLenum @type, void * @values);
        internal static glGetHistogramEXTFunc glGetHistogramEXTPtr;
        internal static void loadGetHistogramEXT()
        {
            try
            {
                glGetHistogramEXTPtr = (glGetHistogramEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetHistogramEXT"), typeof(glGetHistogramEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetHistogramEXT'.");
            }
        }
        public static void glGetHistogramEXT(GLenum @target, GLboolean @reset, GLenum @format, GLenum @type, void * @values) => glGetHistogramEXTPtr(@target, @reset, @format, @type, @values);

        internal delegate void glGetHistogramParameterfvEXTFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetHistogramParameterfvEXTFunc glGetHistogramParameterfvEXTPtr;
        internal static void loadGetHistogramParameterfvEXT()
        {
            try
            {
                glGetHistogramParameterfvEXTPtr = (glGetHistogramParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetHistogramParameterfvEXT"), typeof(glGetHistogramParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetHistogramParameterfvEXT'.");
            }
        }
        public static void glGetHistogramParameterfvEXT(GLenum @target, GLenum @pname, GLfloat * @params) => glGetHistogramParameterfvEXTPtr(@target, @pname, @params);

        internal delegate void glGetHistogramParameterivEXTFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetHistogramParameterivEXTFunc glGetHistogramParameterivEXTPtr;
        internal static void loadGetHistogramParameterivEXT()
        {
            try
            {
                glGetHistogramParameterivEXTPtr = (glGetHistogramParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetHistogramParameterivEXT"), typeof(glGetHistogramParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetHistogramParameterivEXT'.");
            }
        }
        public static void glGetHistogramParameterivEXT(GLenum @target, GLenum @pname, GLint * @params) => glGetHistogramParameterivEXTPtr(@target, @pname, @params);

        internal delegate void glGetMinmaxEXTFunc(GLenum @target, GLboolean @reset, GLenum @format, GLenum @type, void * @values);
        internal static glGetMinmaxEXTFunc glGetMinmaxEXTPtr;
        internal static void loadGetMinmaxEXT()
        {
            try
            {
                glGetMinmaxEXTPtr = (glGetMinmaxEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMinmaxEXT"), typeof(glGetMinmaxEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMinmaxEXT'.");
            }
        }
        public static void glGetMinmaxEXT(GLenum @target, GLboolean @reset, GLenum @format, GLenum @type, void * @values) => glGetMinmaxEXTPtr(@target, @reset, @format, @type, @values);

        internal delegate void glGetMinmaxParameterfvEXTFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetMinmaxParameterfvEXTFunc glGetMinmaxParameterfvEXTPtr;
        internal static void loadGetMinmaxParameterfvEXT()
        {
            try
            {
                glGetMinmaxParameterfvEXTPtr = (glGetMinmaxParameterfvEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMinmaxParameterfvEXT"), typeof(glGetMinmaxParameterfvEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMinmaxParameterfvEXT'.");
            }
        }
        public static void glGetMinmaxParameterfvEXT(GLenum @target, GLenum @pname, GLfloat * @params) => glGetMinmaxParameterfvEXTPtr(@target, @pname, @params);

        internal delegate void glGetMinmaxParameterivEXTFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetMinmaxParameterivEXTFunc glGetMinmaxParameterivEXTPtr;
        internal static void loadGetMinmaxParameterivEXT()
        {
            try
            {
                glGetMinmaxParameterivEXTPtr = (glGetMinmaxParameterivEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMinmaxParameterivEXT"), typeof(glGetMinmaxParameterivEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMinmaxParameterivEXT'.");
            }
        }
        public static void glGetMinmaxParameterivEXT(GLenum @target, GLenum @pname, GLint * @params) => glGetMinmaxParameterivEXTPtr(@target, @pname, @params);

        internal delegate void glHistogramEXTFunc(GLenum @target, GLsizei @width, GLenum @internalformat, GLboolean @sink);
        internal static glHistogramEXTFunc glHistogramEXTPtr;
        internal static void loadHistogramEXT()
        {
            try
            {
                glHistogramEXTPtr = (glHistogramEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glHistogramEXT"), typeof(glHistogramEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glHistogramEXT'.");
            }
        }
        public static void glHistogramEXT(GLenum @target, GLsizei @width, GLenum @internalformat, GLboolean @sink) => glHistogramEXTPtr(@target, @width, @internalformat, @sink);

        internal delegate void glMinmaxEXTFunc(GLenum @target, GLenum @internalformat, GLboolean @sink);
        internal static glMinmaxEXTFunc glMinmaxEXTPtr;
        internal static void loadMinmaxEXT()
        {
            try
            {
                glMinmaxEXTPtr = (glMinmaxEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMinmaxEXT"), typeof(glMinmaxEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMinmaxEXT'.");
            }
        }
        public static void glMinmaxEXT(GLenum @target, GLenum @internalformat, GLboolean @sink) => glMinmaxEXTPtr(@target, @internalformat, @sink);

        internal delegate void glResetHistogramEXTFunc(GLenum @target);
        internal static glResetHistogramEXTFunc glResetHistogramEXTPtr;
        internal static void loadResetHistogramEXT()
        {
            try
            {
                glResetHistogramEXTPtr = (glResetHistogramEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glResetHistogramEXT"), typeof(glResetHistogramEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glResetHistogramEXT'.");
            }
        }
        public static void glResetHistogramEXT(GLenum @target) => glResetHistogramEXTPtr(@target);

        internal delegate void glResetMinmaxEXTFunc(GLenum @target);
        internal static glResetMinmaxEXTFunc glResetMinmaxEXTPtr;
        internal static void loadResetMinmaxEXT()
        {
            try
            {
                glResetMinmaxEXTPtr = (glResetMinmaxEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glResetMinmaxEXT"), typeof(glResetMinmaxEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glResetMinmaxEXT'.");
            }
        }
        public static void glResetMinmaxEXT(GLenum @target) => glResetMinmaxEXTPtr(@target);
        #endregion
    }
}
