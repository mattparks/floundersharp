using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL43
    {
        #region Interop
        static GL43()
        {
            Console.WriteLine("Initalising GL43 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadClearBufferData();
            loadClearBufferSubData();
            loadDispatchCompute();
            loadDispatchComputeIndirect();
            loadCopyImageSubData();
            loadFramebufferParameteri();
            loadGetFramebufferParameteriv();
            loadGetInternalformati64v();
            loadInvalidateTexSubImage();
            loadInvalidateTexImage();
            loadInvalidateBufferSubData();
            loadInvalidateBufferData();
            loadInvalidateFramebuffer();
            loadInvalidateSubFramebuffer();
            loadMultiDrawArraysIndirect();
            loadMultiDrawElementsIndirect();
            loadGetProgramInterfaceiv();
            loadGetProgramResourceIndex();
            loadGetProgramResourceName();
            loadGetProgramResourceiv();
            loadGetProgramResourceLocation();
            loadGetProgramResourceLocationIndex();
            loadShaderStorageBlockBinding();
            loadTexBufferRange();
            loadTexStorage2DMultisample();
            loadTexStorage3DMultisample();
            loadTextureView();
            loadBindVertexBuffer();
            loadVertexAttribFormat();
            loadVertexAttribIFormat();
            loadVertexAttribLFormat();
            loadVertexAttribBinding();
            loadVertexBindingDivisor();
            loadDebugMessageControl();
            loadDebugMessageInsert();
            loadDebugMessageCallback();
            loadGetDebugMessageLog();
            loadPushDebugGroup();
            loadPopDebugGroup();
            loadObjectLabel();
            loadGetObjectLabel();
            loadObjectPtrLabel();
            loadGetObjectPtrLabel();
            loadGetPointerv();
            loadGetPointerv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_NUM_SHADING_LANGUAGE_VERSIONS = 0x82E9;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_LONG = 0x874E;
        public static UInt32 GL_COMPRESSED_RGB8_ETC2 = 0x9274;
        public static UInt32 GL_COMPRESSED_SRGB8_ETC2 = 0x9275;
        public static UInt32 GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2 = 0x9276;
        public static UInt32 GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2 = 0x9277;
        public static UInt32 GL_COMPRESSED_RGBA8_ETC2_EAC = 0x9278;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC = 0x9279;
        public static UInt32 GL_COMPRESSED_R11_EAC = 0x9270;
        public static UInt32 GL_COMPRESSED_SIGNED_R11_EAC = 0x9271;
        public static UInt32 GL_COMPRESSED_RG11_EAC = 0x9272;
        public static UInt32 GL_COMPRESSED_SIGNED_RG11_EAC = 0x9273;
        public static UInt32 GL_PRIMITIVE_RESTART_FIXED_INDEX = 0x8D69;
        public static UInt32 GL_ANY_SAMPLES_PASSED_CONSERVATIVE = 0x8D6A;
        public static UInt32 GL_MAX_ELEMENT_INDEX = 0x8D6B;
        public static UInt32 GL_COMPUTE_SHADER = 0x91B9;
        public static UInt32 GL_MAX_COMPUTE_UNIFORM_BLOCKS = 0x91BB;
        public static UInt32 GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS = 0x91BC;
        public static UInt32 GL_MAX_COMPUTE_IMAGE_UNIFORMS = 0x91BD;
        public static UInt32 GL_MAX_COMPUTE_SHARED_MEMORY_SIZE = 0x8262;
        public static UInt32 GL_MAX_COMPUTE_UNIFORM_COMPONENTS = 0x8263;
        public static UInt32 GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS = 0x8264;
        public static UInt32 GL_MAX_COMPUTE_ATOMIC_COUNTERS = 0x8265;
        public static UInt32 GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS = 0x8266;
        public static UInt32 GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS = 0x90EB;
        public static UInt32 GL_MAX_COMPUTE_WORK_GROUP_COUNT = 0x91BE;
        public static UInt32 GL_MAX_COMPUTE_WORK_GROUP_SIZE = 0x91BF;
        public static UInt32 GL_COMPUTE_WORK_GROUP_SIZE = 0x8267;
        public static UInt32 GL_UNIFORM_BLOCK_REFERENCED_BY_COMPUTE_SHADER = 0x90EC;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_COMPUTE_SHADER = 0x90ED;
        public static UInt32 GL_DISPATCH_INDIRECT_BUFFER = 0x90EE;
        public static UInt32 GL_DISPATCH_INDIRECT_BUFFER_BINDING = 0x90EF;
        public static UInt32 GL_COMPUTE_SHADER_BIT = 0x00000020;
        public static UInt32 GL_DEBUG_OUTPUT_SYNCHRONOUS = 0x8242;
        public static UInt32 GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH = 0x8243;
        public static UInt32 GL_DEBUG_CALLBACK_FUNCTION = 0x8244;
        public static UInt32 GL_DEBUG_CALLBACK_USER_PARAM = 0x8245;
        public static UInt32 GL_DEBUG_SOURCE_API = 0x8246;
        public static UInt32 GL_DEBUG_SOURCE_WINDOW_SYSTEM = 0x8247;
        public static UInt32 GL_DEBUG_SOURCE_SHADER_COMPILER = 0x8248;
        public static UInt32 GL_DEBUG_SOURCE_THIRD_PARTY = 0x8249;
        public static UInt32 GL_DEBUG_SOURCE_APPLICATION = 0x824A;
        public static UInt32 GL_DEBUG_SOURCE_OTHER = 0x824B;
        public static UInt32 GL_DEBUG_TYPE_ERROR = 0x824C;
        public static UInt32 GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR = 0x824D;
        public static UInt32 GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR = 0x824E;
        public static UInt32 GL_DEBUG_TYPE_PORTABILITY = 0x824F;
        public static UInt32 GL_DEBUG_TYPE_PERFORMANCE = 0x8250;
        public static UInt32 GL_DEBUG_TYPE_OTHER = 0x8251;
        public static UInt32 GL_MAX_DEBUG_MESSAGE_LENGTH = 0x9143;
        public static UInt32 GL_MAX_DEBUG_LOGGED_MESSAGES = 0x9144;
        public static UInt32 GL_DEBUG_LOGGED_MESSAGES = 0x9145;
        public static UInt32 GL_DEBUG_SEVERITY_HIGH = 0x9146;
        public static UInt32 GL_DEBUG_SEVERITY_MEDIUM = 0x9147;
        public static UInt32 GL_DEBUG_SEVERITY_LOW = 0x9148;
        public static UInt32 GL_DEBUG_TYPE_MARKER = 0x8268;
        public static UInt32 GL_DEBUG_TYPE_PUSH_GROUP = 0x8269;
        public static UInt32 GL_DEBUG_TYPE_POP_GROUP = 0x826A;
        public static UInt32 GL_DEBUG_SEVERITY_NOTIFICATION = 0x826B;
        public static UInt32 GL_MAX_DEBUG_GROUP_STACK_DEPTH = 0x826C;
        public static UInt32 GL_DEBUG_GROUP_STACK_DEPTH = 0x826D;
        public static UInt32 GL_BUFFER = 0x82E0;
        public static UInt32 GL_SHADER = 0x82E1;
        public static UInt32 GL_PROGRAM = 0x82E2;
        public static UInt32 GL_VERTEX_ARRAY = 0x8074;
        public static UInt32 GL_QUERY = 0x82E3;
        public static UInt32 GL_PROGRAM_PIPELINE = 0x82E4;
        public static UInt32 GL_SAMPLER = 0x82E6;
        public static UInt32 GL_MAX_LABEL_LENGTH = 0x82E8;
        public static UInt32 GL_DEBUG_OUTPUT = 0x92E0;
        public static UInt32 GL_CONTEXT_FLAG_DEBUG_BIT = 0x00000002;
        public static UInt32 GL_MAX_UNIFORM_LOCATIONS = 0x826E;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_WIDTH = 0x9310;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_HEIGHT = 0x9311;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_LAYERS = 0x9312;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_SAMPLES = 0x9313;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS = 0x9314;
        public static UInt32 GL_MAX_FRAMEBUFFER_WIDTH = 0x9315;
        public static UInt32 GL_MAX_FRAMEBUFFER_HEIGHT = 0x9316;
        public static UInt32 GL_MAX_FRAMEBUFFER_LAYERS = 0x9317;
        public static UInt32 GL_MAX_FRAMEBUFFER_SAMPLES = 0x9318;
        public static UInt32 GL_INTERNALFORMAT_SUPPORTED = 0x826F;
        public static UInt32 GL_INTERNALFORMAT_PREFERRED = 0x8270;
        public static UInt32 GL_INTERNALFORMAT_RED_SIZE = 0x8271;
        public static UInt32 GL_INTERNALFORMAT_GREEN_SIZE = 0x8272;
        public static UInt32 GL_INTERNALFORMAT_BLUE_SIZE = 0x8273;
        public static UInt32 GL_INTERNALFORMAT_ALPHA_SIZE = 0x8274;
        public static UInt32 GL_INTERNALFORMAT_DEPTH_SIZE = 0x8275;
        public static UInt32 GL_INTERNALFORMAT_STENCIL_SIZE = 0x8276;
        public static UInt32 GL_INTERNALFORMAT_SHARED_SIZE = 0x8277;
        public static UInt32 GL_INTERNALFORMAT_RED_TYPE = 0x8278;
        public static UInt32 GL_INTERNALFORMAT_GREEN_TYPE = 0x8279;
        public static UInt32 GL_INTERNALFORMAT_BLUE_TYPE = 0x827A;
        public static UInt32 GL_INTERNALFORMAT_ALPHA_TYPE = 0x827B;
        public static UInt32 GL_INTERNALFORMAT_DEPTH_TYPE = 0x827C;
        public static UInt32 GL_INTERNALFORMAT_STENCIL_TYPE = 0x827D;
        public static UInt32 GL_MAX_WIDTH = 0x827E;
        public static UInt32 GL_MAX_HEIGHT = 0x827F;
        public static UInt32 GL_MAX_DEPTH = 0x8280;
        public static UInt32 GL_MAX_LAYERS = 0x8281;
        public static UInt32 GL_MAX_COMBINED_DIMENSIONS = 0x8282;
        public static UInt32 GL_COLOR_COMPONENTS = 0x8283;
        public static UInt32 GL_DEPTH_COMPONENTS = 0x8284;
        public static UInt32 GL_STENCIL_COMPONENTS = 0x8285;
        public static UInt32 GL_COLOR_RENDERABLE = 0x8286;
        public static UInt32 GL_DEPTH_RENDERABLE = 0x8287;
        public static UInt32 GL_STENCIL_RENDERABLE = 0x8288;
        public static UInt32 GL_FRAMEBUFFER_RENDERABLE = 0x8289;
        public static UInt32 GL_FRAMEBUFFER_RENDERABLE_LAYERED = 0x828A;
        public static UInt32 GL_FRAMEBUFFER_BLEND = 0x828B;
        public static UInt32 GL_READ_PIXELS = 0x828C;
        public static UInt32 GL_READ_PIXELS_FORMAT = 0x828D;
        public static UInt32 GL_READ_PIXELS_TYPE = 0x828E;
        public static UInt32 GL_TEXTURE_IMAGE_FORMAT = 0x828F;
        public static UInt32 GL_TEXTURE_IMAGE_TYPE = 0x8290;
        public static UInt32 GL_GET_TEXTURE_IMAGE_FORMAT = 0x8291;
        public static UInt32 GL_GET_TEXTURE_IMAGE_TYPE = 0x8292;
        public static UInt32 GL_MIPMAP = 0x8293;
        public static UInt32 GL_MANUAL_GENERATE_MIPMAP = 0x8294;
        public static UInt32 GL_AUTO_GENERATE_MIPMAP = 0x8295;
        public static UInt32 GL_COLOR_ENCODING = 0x8296;
        public static UInt32 GL_SRGB_READ = 0x8297;
        public static UInt32 GL_SRGB_WRITE = 0x8298;
        public static UInt32 GL_FILTER = 0x829A;
        public static UInt32 GL_VERTEX_TEXTURE = 0x829B;
        public static UInt32 GL_TESS_CONTROL_TEXTURE = 0x829C;
        public static UInt32 GL_TESS_EVALUATION_TEXTURE = 0x829D;
        public static UInt32 GL_GEOMETRY_TEXTURE = 0x829E;
        public static UInt32 GL_FRAGMENT_TEXTURE = 0x829F;
        public static UInt32 GL_COMPUTE_TEXTURE = 0x82A0;
        public static UInt32 GL_TEXTURE_SHADOW = 0x82A1;
        public static UInt32 GL_TEXTURE_GATHER = 0x82A2;
        public static UInt32 GL_TEXTURE_GATHER_SHADOW = 0x82A3;
        public static UInt32 GL_SHADER_IMAGE_LOAD = 0x82A4;
        public static UInt32 GL_SHADER_IMAGE_STORE = 0x82A5;
        public static UInt32 GL_SHADER_IMAGE_ATOMIC = 0x82A6;
        public static UInt32 GL_IMAGE_TEXEL_SIZE = 0x82A7;
        public static UInt32 GL_IMAGE_COMPATIBILITY_CLASS = 0x82A8;
        public static UInt32 GL_IMAGE_PIXEL_FORMAT = 0x82A9;
        public static UInt32 GL_IMAGE_PIXEL_TYPE = 0x82AA;
        public static UInt32 GL_SIMULTANEOUS_TEXTURE_AND_DEPTH_TEST = 0x82AC;
        public static UInt32 GL_SIMULTANEOUS_TEXTURE_AND_STENCIL_TEST = 0x82AD;
        public static UInt32 GL_SIMULTANEOUS_TEXTURE_AND_DEPTH_WRITE = 0x82AE;
        public static UInt32 GL_SIMULTANEOUS_TEXTURE_AND_STENCIL_WRITE = 0x82AF;
        public static UInt32 GL_TEXTURE_COMPRESSED_BLOCK_WIDTH = 0x82B1;
        public static UInt32 GL_TEXTURE_COMPRESSED_BLOCK_HEIGHT = 0x82B2;
        public static UInt32 GL_TEXTURE_COMPRESSED_BLOCK_SIZE = 0x82B3;
        public static UInt32 GL_CLEAR_BUFFER = 0x82B4;
        public static UInt32 GL_TEXTURE_VIEW = 0x82B5;
        public static UInt32 GL_VIEW_COMPATIBILITY_CLASS = 0x82B6;
        public static UInt32 GL_FULL_SUPPORT = 0x82B7;
        public static UInt32 GL_CAVEAT_SUPPORT = 0x82B8;
        public static UInt32 GL_IMAGE_CLASS_4_X_32 = 0x82B9;
        public static UInt32 GL_IMAGE_CLASS_2_X_32 = 0x82BA;
        public static UInt32 GL_IMAGE_CLASS_1_X_32 = 0x82BB;
        public static UInt32 GL_IMAGE_CLASS_4_X_16 = 0x82BC;
        public static UInt32 GL_IMAGE_CLASS_2_X_16 = 0x82BD;
        public static UInt32 GL_IMAGE_CLASS_1_X_16 = 0x82BE;
        public static UInt32 GL_IMAGE_CLASS_4_X_8 = 0x82BF;
        public static UInt32 GL_IMAGE_CLASS_2_X_8 = 0x82C0;
        public static UInt32 GL_IMAGE_CLASS_1_X_8 = 0x82C1;
        public static UInt32 GL_IMAGE_CLASS_11_11_10 = 0x82C2;
        public static UInt32 GL_IMAGE_CLASS_10_10_10_2 = 0x82C3;
        public static UInt32 GL_VIEW_CLASS_128_BITS = 0x82C4;
        public static UInt32 GL_VIEW_CLASS_96_BITS = 0x82C5;
        public static UInt32 GL_VIEW_CLASS_64_BITS = 0x82C6;
        public static UInt32 GL_VIEW_CLASS_48_BITS = 0x82C7;
        public static UInt32 GL_VIEW_CLASS_32_BITS = 0x82C8;
        public static UInt32 GL_VIEW_CLASS_24_BITS = 0x82C9;
        public static UInt32 GL_VIEW_CLASS_16_BITS = 0x82CA;
        public static UInt32 GL_VIEW_CLASS_8_BITS = 0x82CB;
        public static UInt32 GL_VIEW_CLASS_S3TC_DXT1_RGB = 0x82CC;
        public static UInt32 GL_VIEW_CLASS_S3TC_DXT1_RGBA = 0x82CD;
        public static UInt32 GL_VIEW_CLASS_S3TC_DXT3_RGBA = 0x82CE;
        public static UInt32 GL_VIEW_CLASS_S3TC_DXT5_RGBA = 0x82CF;
        public static UInt32 GL_VIEW_CLASS_RGTC1_RED = 0x82D0;
        public static UInt32 GL_VIEW_CLASS_RGTC2_RG = 0x82D1;
        public static UInt32 GL_VIEW_CLASS_BPTC_UNORM = 0x82D2;
        public static UInt32 GL_VIEW_CLASS_BPTC_FLOAT = 0x82D3;
        public static UInt32 GL_UNIFORM = 0x92E1;
        public static UInt32 GL_UNIFORM_BLOCK = 0x92E2;
        public static UInt32 GL_PROGRAM_INPUT = 0x92E3;
        public static UInt32 GL_PROGRAM_OUTPUT = 0x92E4;
        public static UInt32 GL_BUFFER_VARIABLE = 0x92E5;
        public static UInt32 GL_SHADER_STORAGE_BLOCK = 0x92E6;
        public static UInt32 GL_VERTEX_SUBROUTINE = 0x92E8;
        public static UInt32 GL_TESS_CONTROL_SUBROUTINE = 0x92E9;
        public static UInt32 GL_TESS_EVALUATION_SUBROUTINE = 0x92EA;
        public static UInt32 GL_GEOMETRY_SUBROUTINE = 0x92EB;
        public static UInt32 GL_FRAGMENT_SUBROUTINE = 0x92EC;
        public static UInt32 GL_COMPUTE_SUBROUTINE = 0x92ED;
        public static UInt32 GL_VERTEX_SUBROUTINE_UNIFORM = 0x92EE;
        public static UInt32 GL_TESS_CONTROL_SUBROUTINE_UNIFORM = 0x92EF;
        public static UInt32 GL_TESS_EVALUATION_SUBROUTINE_UNIFORM = 0x92F0;
        public static UInt32 GL_GEOMETRY_SUBROUTINE_UNIFORM = 0x92F1;
        public static UInt32 GL_FRAGMENT_SUBROUTINE_UNIFORM = 0x92F2;
        public static UInt32 GL_COMPUTE_SUBROUTINE_UNIFORM = 0x92F3;
        public static UInt32 GL_TRANSFORM_FEEDBACK_VARYING = 0x92F4;
        public static UInt32 GL_ACTIVE_RESOURCES = 0x92F5;
        public static UInt32 GL_MAX_NAME_LENGTH = 0x92F6;
        public static UInt32 GL_MAX_NUM_ACTIVE_VARIABLES = 0x92F7;
        public static UInt32 GL_MAX_NUM_COMPATIBLE_SUBROUTINES = 0x92F8;
        public static UInt32 GL_NAME_LENGTH = 0x92F9;
        public static UInt32 GL_TYPE = 0x92FA;
        public static UInt32 GL_ARRAY_SIZE = 0x92FB;
        public static UInt32 GL_OFFSET = 0x92FC;
        public static UInt32 GL_BLOCK_INDEX = 0x92FD;
        public static UInt32 GL_ARRAY_STRIDE = 0x92FE;
        public static UInt32 GL_MATRIX_STRIDE = 0x92FF;
        public static UInt32 GL_IS_ROW_MAJOR = 0x9300;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_INDEX = 0x9301;
        public static UInt32 GL_BUFFER_BINDING = 0x9302;
        public static UInt32 GL_BUFFER_DATA_SIZE = 0x9303;
        public static UInt32 GL_NUM_ACTIVE_VARIABLES = 0x9304;
        public static UInt32 GL_ACTIVE_VARIABLES = 0x9305;
        public static UInt32 GL_REFERENCED_BY_VERTEX_SHADER = 0x9306;
        public static UInt32 GL_REFERENCED_BY_TESS_CONTROL_SHADER = 0x9307;
        public static UInt32 GL_REFERENCED_BY_TESS_EVALUATION_SHADER = 0x9308;
        public static UInt32 GL_REFERENCED_BY_GEOMETRY_SHADER = 0x9309;
        public static UInt32 GL_REFERENCED_BY_FRAGMENT_SHADER = 0x930A;
        public static UInt32 GL_REFERENCED_BY_COMPUTE_SHADER = 0x930B;
        public static UInt32 GL_TOP_LEVEL_ARRAY_SIZE = 0x930C;
        public static UInt32 GL_TOP_LEVEL_ARRAY_STRIDE = 0x930D;
        public static UInt32 GL_LOCATION = 0x930E;
        public static UInt32 GL_LOCATION_INDEX = 0x930F;
        public static UInt32 GL_IS_PER_PATCH = 0x92E7;
        public static UInt32 GL_SHADER_STORAGE_BUFFER = 0x90D2;
        public static UInt32 GL_SHADER_STORAGE_BUFFER_BINDING = 0x90D3;
        public static UInt32 GL_SHADER_STORAGE_BUFFER_START = 0x90D4;
        public static UInt32 GL_SHADER_STORAGE_BUFFER_SIZE = 0x90D5;
        public static UInt32 GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS = 0x90D6;
        public static UInt32 GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS = 0x90D7;
        public static UInt32 GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS = 0x90D8;
        public static UInt32 GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS = 0x90D9;
        public static UInt32 GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS = 0x90DA;
        public static UInt32 GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS = 0x90DB;
        public static UInt32 GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS = 0x90DC;
        public static UInt32 GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS = 0x90DD;
        public static UInt32 GL_MAX_SHADER_STORAGE_BLOCK_SIZE = 0x90DE;
        public static UInt32 GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT = 0x90DF;
        public static UInt32 GL_SHADER_STORAGE_BARRIER_BIT = 0x00002000;
        public static UInt32 GL_MAX_COMBINED_SHADER_OUTPUT_RESOURCES = 0x8F39;
        public static UInt32 GL_DEPTH_STENCIL_TEXTURE_MODE = 0x90EA;
        public static UInt32 GL_TEXTURE_BUFFER_OFFSET = 0x919D;
        public static UInt32 GL_TEXTURE_BUFFER_SIZE = 0x919E;
        public static UInt32 GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT = 0x919F;
        public static UInt32 GL_TEXTURE_VIEW_MIN_LEVEL = 0x82DB;
        public static UInt32 GL_TEXTURE_VIEW_NUM_LEVELS = 0x82DC;
        public static UInt32 GL_TEXTURE_VIEW_MIN_LAYER = 0x82DD;
        public static UInt32 GL_TEXTURE_VIEW_NUM_LAYERS = 0x82DE;
        public static UInt32 GL_TEXTURE_IMMUTABLE_LEVELS = 0x82DF;
        public static UInt32 GL_VERTEX_ATTRIB_BINDING = 0x82D4;
        public static UInt32 GL_VERTEX_ATTRIB_RELATIVE_OFFSET = 0x82D5;
        public static UInt32 GL_VERTEX_BINDING_DIVISOR = 0x82D6;
        public static UInt32 GL_VERTEX_BINDING_OFFSET = 0x82D7;
        public static UInt32 GL_VERTEX_BINDING_STRIDE = 0x82D8;
        public static UInt32 GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET = 0x82D9;
        public static UInt32 GL_MAX_VERTEX_ATTRIB_BINDINGS = 0x82DA;
        public static UInt32 GL_VERTEX_BINDING_BUFFER = 0x8F4F;
        public static UInt32 GL_DISPLAY_LIST = 0x82E7;
        public static UInt32 GL_STACK_UNDERFLOW = 0x0504;
        public static UInt32 GL_STACK_OVERFLOW = 0x0503;
        #endregion

        #region Commands
        internal delegate void glClearBufferDataFunc(GLenum @target, GLenum @internalformat, GLenum @format, GLenum @type, const void * @data);
        internal static glClearBufferDataFunc glClearBufferDataPtr;
        internal static void loadClearBufferData()
        {
            try
            {
                glClearBufferDataPtr = (glClearBufferDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearBufferData"), typeof(glClearBufferDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearBufferData'.");
            }
        }
        public static void glClearBufferData(GLenum @target, GLenum @internalformat, GLenum @format, GLenum @type, const void * @data) => glClearBufferDataPtr(@target, @internalformat, @format, @type, @data);

        internal delegate void glClearBufferSubDataFunc(GLenum @target, GLenum @internalformat, GLintptr @offset, GLsizeiptr @size, GLenum @format, GLenum @type, const void * @data);
        internal static glClearBufferSubDataFunc glClearBufferSubDataPtr;
        internal static void loadClearBufferSubData()
        {
            try
            {
                glClearBufferSubDataPtr = (glClearBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearBufferSubData"), typeof(glClearBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearBufferSubData'.");
            }
        }
        public static void glClearBufferSubData(GLenum @target, GLenum @internalformat, GLintptr @offset, GLsizeiptr @size, GLenum @format, GLenum @type, const void * @data) => glClearBufferSubDataPtr(@target, @internalformat, @offset, @size, @format, @type, @data);

        internal delegate void glDispatchComputeFunc(GLuint @num_groups_x, GLuint @num_groups_y, GLuint @num_groups_z);
        internal static glDispatchComputeFunc glDispatchComputePtr;
        internal static void loadDispatchCompute()
        {
            try
            {
                glDispatchComputePtr = (glDispatchComputeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDispatchCompute"), typeof(glDispatchComputeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDispatchCompute'.");
            }
        }
        public static void glDispatchCompute(GLuint @num_groups_x, GLuint @num_groups_y, GLuint @num_groups_z) => glDispatchComputePtr(@num_groups_x, @num_groups_y, @num_groups_z);

        internal delegate void glDispatchComputeIndirectFunc(GLintptr @indirect);
        internal static glDispatchComputeIndirectFunc glDispatchComputeIndirectPtr;
        internal static void loadDispatchComputeIndirect()
        {
            try
            {
                glDispatchComputeIndirectPtr = (glDispatchComputeIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDispatchComputeIndirect"), typeof(glDispatchComputeIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDispatchComputeIndirect'.");
            }
        }
        public static void glDispatchComputeIndirect(GLintptr @indirect) => glDispatchComputeIndirectPtr(@indirect);

        internal delegate void glCopyImageSubDataFunc(GLuint @srcName, GLenum @srcTarget, GLint @srcLevel, GLint @srcX, GLint @srcY, GLint @srcZ, GLuint @dstName, GLenum @dstTarget, GLint @dstLevel, GLint @dstX, GLint @dstY, GLint @dstZ, GLsizei @srcWidth, GLsizei @srcHeight, GLsizei @srcDepth);
        internal static glCopyImageSubDataFunc glCopyImageSubDataPtr;
        internal static void loadCopyImageSubData()
        {
            try
            {
                glCopyImageSubDataPtr = (glCopyImageSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyImageSubData"), typeof(glCopyImageSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyImageSubData'.");
            }
        }
        public static void glCopyImageSubData(GLuint @srcName, GLenum @srcTarget, GLint @srcLevel, GLint @srcX, GLint @srcY, GLint @srcZ, GLuint @dstName, GLenum @dstTarget, GLint @dstLevel, GLint @dstX, GLint @dstY, GLint @dstZ, GLsizei @srcWidth, GLsizei @srcHeight, GLsizei @srcDepth) => glCopyImageSubDataPtr(@srcName, @srcTarget, @srcLevel, @srcX, @srcY, @srcZ, @dstName, @dstTarget, @dstLevel, @dstX, @dstY, @dstZ, @srcWidth, @srcHeight, @srcDepth);

        internal delegate void glFramebufferParameteriFunc(GLenum @target, GLenum @pname, GLint @param);
        internal static glFramebufferParameteriFunc glFramebufferParameteriPtr;
        internal static void loadFramebufferParameteri()
        {
            try
            {
                glFramebufferParameteriPtr = (glFramebufferParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferParameteri"), typeof(glFramebufferParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferParameteri'.");
            }
        }
        public static void glFramebufferParameteri(GLenum @target, GLenum @pname, GLint @param) => glFramebufferParameteriPtr(@target, @pname, @param);

        internal delegate void glGetFramebufferParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetFramebufferParameterivFunc glGetFramebufferParameterivPtr;
        internal static void loadGetFramebufferParameteriv()
        {
            try
            {
                glGetFramebufferParameterivPtr = (glGetFramebufferParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFramebufferParameteriv"), typeof(glGetFramebufferParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFramebufferParameteriv'.");
            }
        }
        public static void glGetFramebufferParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetFramebufferParameterivPtr(@target, @pname, @params);

        internal delegate void glGetInternalformati64vFunc(GLenum @target, GLenum @internalformat, GLenum @pname, GLsizei @bufSize, GLint64 * @params);
        internal static glGetInternalformati64vFunc glGetInternalformati64vPtr;
        internal static void loadGetInternalformati64v()
        {
            try
            {
                glGetInternalformati64vPtr = (glGetInternalformati64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInternalformati64v"), typeof(glGetInternalformati64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInternalformati64v'.");
            }
        }
        public static void glGetInternalformati64v(GLenum @target, GLenum @internalformat, GLenum @pname, GLsizei @bufSize, GLint64 * @params) => glGetInternalformati64vPtr(@target, @internalformat, @pname, @bufSize, @params);

        internal delegate void glInvalidateTexSubImageFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth);
        internal static glInvalidateTexSubImageFunc glInvalidateTexSubImagePtr;
        internal static void loadInvalidateTexSubImage()
        {
            try
            {
                glInvalidateTexSubImagePtr = (glInvalidateTexSubImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateTexSubImage"), typeof(glInvalidateTexSubImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateTexSubImage'.");
            }
        }
        public static void glInvalidateTexSubImage(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth) => glInvalidateTexSubImagePtr(@texture, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth);

        internal delegate void glInvalidateTexImageFunc(GLuint @texture, GLint @level);
        internal static glInvalidateTexImageFunc glInvalidateTexImagePtr;
        internal static void loadInvalidateTexImage()
        {
            try
            {
                glInvalidateTexImagePtr = (glInvalidateTexImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateTexImage"), typeof(glInvalidateTexImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateTexImage'.");
            }
        }
        public static void glInvalidateTexImage(GLuint @texture, GLint @level) => glInvalidateTexImagePtr(@texture, @level);

        internal delegate void glInvalidateBufferSubDataFunc(GLuint @buffer, GLintptr @offset, GLsizeiptr @length);
        internal static glInvalidateBufferSubDataFunc glInvalidateBufferSubDataPtr;
        internal static void loadInvalidateBufferSubData()
        {
            try
            {
                glInvalidateBufferSubDataPtr = (glInvalidateBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateBufferSubData"), typeof(glInvalidateBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateBufferSubData'.");
            }
        }
        public static void glInvalidateBufferSubData(GLuint @buffer, GLintptr @offset, GLsizeiptr @length) => glInvalidateBufferSubDataPtr(@buffer, @offset, @length);

        internal delegate void glInvalidateBufferDataFunc(GLuint @buffer);
        internal static glInvalidateBufferDataFunc glInvalidateBufferDataPtr;
        internal static void loadInvalidateBufferData()
        {
            try
            {
                glInvalidateBufferDataPtr = (glInvalidateBufferDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateBufferData"), typeof(glInvalidateBufferDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateBufferData'.");
            }
        }
        public static void glInvalidateBufferData(GLuint @buffer) => glInvalidateBufferDataPtr(@buffer);

        internal delegate void glInvalidateFramebufferFunc(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments);
        internal static glInvalidateFramebufferFunc glInvalidateFramebufferPtr;
        internal static void loadInvalidateFramebuffer()
        {
            try
            {
                glInvalidateFramebufferPtr = (glInvalidateFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateFramebuffer"), typeof(glInvalidateFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateFramebuffer'.");
            }
        }
        public static void glInvalidateFramebuffer(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments) => glInvalidateFramebufferPtr(@target, @numAttachments, @attachments);

        internal delegate void glInvalidateSubFramebufferFunc(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glInvalidateSubFramebufferFunc glInvalidateSubFramebufferPtr;
        internal static void loadInvalidateSubFramebuffer()
        {
            try
            {
                glInvalidateSubFramebufferPtr = (glInvalidateSubFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateSubFramebuffer"), typeof(glInvalidateSubFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateSubFramebuffer'.");
            }
        }
        public static void glInvalidateSubFramebuffer(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glInvalidateSubFramebufferPtr(@target, @numAttachments, @attachments, @x, @y, @width, @height);

        internal delegate void glMultiDrawArraysIndirectFunc(GLenum @mode, const void * @indirect, GLsizei @drawcount, GLsizei @stride);
        internal static glMultiDrawArraysIndirectFunc glMultiDrawArraysIndirectPtr;
        internal static void loadMultiDrawArraysIndirect()
        {
            try
            {
                glMultiDrawArraysIndirectPtr = (glMultiDrawArraysIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawArraysIndirect"), typeof(glMultiDrawArraysIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawArraysIndirect'.");
            }
        }
        public static void glMultiDrawArraysIndirect(GLenum @mode, const void * @indirect, GLsizei @drawcount, GLsizei @stride) => glMultiDrawArraysIndirectPtr(@mode, @indirect, @drawcount, @stride);

        internal delegate void glMultiDrawElementsIndirectFunc(GLenum @mode, GLenum @type, const void * @indirect, GLsizei @drawcount, GLsizei @stride);
        internal static glMultiDrawElementsIndirectFunc glMultiDrawElementsIndirectPtr;
        internal static void loadMultiDrawElementsIndirect()
        {
            try
            {
                glMultiDrawElementsIndirectPtr = (glMultiDrawElementsIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementsIndirect"), typeof(glMultiDrawElementsIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementsIndirect'.");
            }
        }
        public static void glMultiDrawElementsIndirect(GLenum @mode, GLenum @type, const void * @indirect, GLsizei @drawcount, GLsizei @stride) => glMultiDrawElementsIndirectPtr(@mode, @type, @indirect, @drawcount, @stride);

        internal delegate void glGetProgramInterfaceivFunc(GLuint @program, GLenum @programInterface, GLenum @pname, GLint * @params);
        internal static glGetProgramInterfaceivFunc glGetProgramInterfaceivPtr;
        internal static void loadGetProgramInterfaceiv()
        {
            try
            {
                glGetProgramInterfaceivPtr = (glGetProgramInterfaceivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramInterfaceiv"), typeof(glGetProgramInterfaceivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramInterfaceiv'.");
            }
        }
        public static void glGetProgramInterfaceiv(GLuint @program, GLenum @programInterface, GLenum @pname, GLint * @params) => glGetProgramInterfaceivPtr(@program, @programInterface, @pname, @params);

        internal delegate GLuint glGetProgramResourceIndexFunc(GLuint @program, GLenum @programInterface, const GLchar * @name);
        internal static glGetProgramResourceIndexFunc glGetProgramResourceIndexPtr;
        internal static void loadGetProgramResourceIndex()
        {
            try
            {
                glGetProgramResourceIndexPtr = (glGetProgramResourceIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceIndex"), typeof(glGetProgramResourceIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceIndex'.");
            }
        }
        public static GLuint glGetProgramResourceIndex(GLuint @program, GLenum @programInterface, const GLchar * @name) => glGetProgramResourceIndexPtr(@program, @programInterface, @name);

        internal delegate void glGetProgramResourceNameFunc(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLchar * @name);
        internal static glGetProgramResourceNameFunc glGetProgramResourceNamePtr;
        internal static void loadGetProgramResourceName()
        {
            try
            {
                glGetProgramResourceNamePtr = (glGetProgramResourceNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceName"), typeof(glGetProgramResourceNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceName'.");
            }
        }
        public static void glGetProgramResourceName(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLchar * @name) => glGetProgramResourceNamePtr(@program, @programInterface, @index, @bufSize, @length, @name);

        internal delegate void glGetProgramResourceivFunc(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @propCount, const GLenum * @props, GLsizei @bufSize, GLsizei * @length, GLint * @params);
        internal static glGetProgramResourceivFunc glGetProgramResourceivPtr;
        internal static void loadGetProgramResourceiv()
        {
            try
            {
                glGetProgramResourceivPtr = (glGetProgramResourceivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceiv"), typeof(glGetProgramResourceivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceiv'.");
            }
        }
        public static void glGetProgramResourceiv(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @propCount, const GLenum * @props, GLsizei @bufSize, GLsizei * @length, GLint * @params) => glGetProgramResourceivPtr(@program, @programInterface, @index, @propCount, @props, @bufSize, @length, @params);

        internal delegate GLint glGetProgramResourceLocationFunc(GLuint @program, GLenum @programInterface, const GLchar * @name);
        internal static glGetProgramResourceLocationFunc glGetProgramResourceLocationPtr;
        internal static void loadGetProgramResourceLocation()
        {
            try
            {
                glGetProgramResourceLocationPtr = (glGetProgramResourceLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceLocation"), typeof(glGetProgramResourceLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceLocation'.");
            }
        }
        public static GLint glGetProgramResourceLocation(GLuint @program, GLenum @programInterface, const GLchar * @name) => glGetProgramResourceLocationPtr(@program, @programInterface, @name);

        internal delegate GLint glGetProgramResourceLocationIndexFunc(GLuint @program, GLenum @programInterface, const GLchar * @name);
        internal static glGetProgramResourceLocationIndexFunc glGetProgramResourceLocationIndexPtr;
        internal static void loadGetProgramResourceLocationIndex()
        {
            try
            {
                glGetProgramResourceLocationIndexPtr = (glGetProgramResourceLocationIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceLocationIndex"), typeof(glGetProgramResourceLocationIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceLocationIndex'.");
            }
        }
        public static GLint glGetProgramResourceLocationIndex(GLuint @program, GLenum @programInterface, const GLchar * @name) => glGetProgramResourceLocationIndexPtr(@program, @programInterface, @name);

        internal delegate void glShaderStorageBlockBindingFunc(GLuint @program, GLuint @storageBlockIndex, GLuint @storageBlockBinding);
        internal static glShaderStorageBlockBindingFunc glShaderStorageBlockBindingPtr;
        internal static void loadShaderStorageBlockBinding()
        {
            try
            {
                glShaderStorageBlockBindingPtr = (glShaderStorageBlockBindingFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShaderStorageBlockBinding"), typeof(glShaderStorageBlockBindingFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShaderStorageBlockBinding'.");
            }
        }
        public static void glShaderStorageBlockBinding(GLuint @program, GLuint @storageBlockIndex, GLuint @storageBlockBinding) => glShaderStorageBlockBindingPtr(@program, @storageBlockIndex, @storageBlockBinding);

        internal delegate void glTexBufferRangeFunc(GLenum @target, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glTexBufferRangeFunc glTexBufferRangePtr;
        internal static void loadTexBufferRange()
        {
            try
            {
                glTexBufferRangePtr = (glTexBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBufferRange"), typeof(glTexBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBufferRange'.");
            }
        }
        public static void glTexBufferRange(GLenum @target, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glTexBufferRangePtr(@target, @internalformat, @buffer, @offset, @size);

        internal delegate void glTexStorage2DMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations);
        internal static glTexStorage2DMultisampleFunc glTexStorage2DMultisamplePtr;
        internal static void loadTexStorage2DMultisample()
        {
            try
            {
                glTexStorage2DMultisamplePtr = (glTexStorage2DMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage2DMultisample"), typeof(glTexStorage2DMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage2DMultisample'.");
            }
        }
        public static void glTexStorage2DMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations) => glTexStorage2DMultisamplePtr(@target, @samples, @internalformat, @width, @height, @fixedsamplelocations);

        internal delegate void glTexStorage3DMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations);
        internal static glTexStorage3DMultisampleFunc glTexStorage3DMultisamplePtr;
        internal static void loadTexStorage3DMultisample()
        {
            try
            {
                glTexStorage3DMultisamplePtr = (glTexStorage3DMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage3DMultisample"), typeof(glTexStorage3DMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage3DMultisample'.");
            }
        }
        public static void glTexStorage3DMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations) => glTexStorage3DMultisamplePtr(@target, @samples, @internalformat, @width, @height, @depth, @fixedsamplelocations);

        internal delegate void glTextureViewFunc(GLuint @texture, GLenum @target, GLuint @origtexture, GLenum @internalformat, GLuint @minlevel, GLuint @numlevels, GLuint @minlayer, GLuint @numlayers);
        internal static glTextureViewFunc glTextureViewPtr;
        internal static void loadTextureView()
        {
            try
            {
                glTextureViewPtr = (glTextureViewFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureView"), typeof(glTextureViewFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureView'.");
            }
        }
        public static void glTextureView(GLuint @texture, GLenum @target, GLuint @origtexture, GLenum @internalformat, GLuint @minlevel, GLuint @numlevels, GLuint @minlayer, GLuint @numlayers) => glTextureViewPtr(@texture, @target, @origtexture, @internalformat, @minlevel, @numlevels, @minlayer, @numlayers);

        internal delegate void glBindVertexBufferFunc(GLuint @bindingindex, GLuint @buffer, GLintptr @offset, GLsizei @stride);
        internal static glBindVertexBufferFunc glBindVertexBufferPtr;
        internal static void loadBindVertexBuffer()
        {
            try
            {
                glBindVertexBufferPtr = (glBindVertexBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVertexBuffer"), typeof(glBindVertexBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVertexBuffer'.");
            }
        }
        public static void glBindVertexBuffer(GLuint @bindingindex, GLuint @buffer, GLintptr @offset, GLsizei @stride) => glBindVertexBufferPtr(@bindingindex, @buffer, @offset, @stride);

        internal delegate void glVertexAttribFormatFunc(GLuint @attribindex, GLint @size, GLenum @type, GLboolean @normalized, GLuint @relativeoffset);
        internal static glVertexAttribFormatFunc glVertexAttribFormatPtr;
        internal static void loadVertexAttribFormat()
        {
            try
            {
                glVertexAttribFormatPtr = (glVertexAttribFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribFormat"), typeof(glVertexAttribFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribFormat'.");
            }
        }
        public static void glVertexAttribFormat(GLuint @attribindex, GLint @size, GLenum @type, GLboolean @normalized, GLuint @relativeoffset) => glVertexAttribFormatPtr(@attribindex, @size, @type, @normalized, @relativeoffset);

        internal delegate void glVertexAttribIFormatFunc(GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset);
        internal static glVertexAttribIFormatFunc glVertexAttribIFormatPtr;
        internal static void loadVertexAttribIFormat()
        {
            try
            {
                glVertexAttribIFormatPtr = (glVertexAttribIFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribIFormat"), typeof(glVertexAttribIFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribIFormat'.");
            }
        }
        public static void glVertexAttribIFormat(GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset) => glVertexAttribIFormatPtr(@attribindex, @size, @type, @relativeoffset);

        internal delegate void glVertexAttribLFormatFunc(GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset);
        internal static glVertexAttribLFormatFunc glVertexAttribLFormatPtr;
        internal static void loadVertexAttribLFormat()
        {
            try
            {
                glVertexAttribLFormatPtr = (glVertexAttribLFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribLFormat"), typeof(glVertexAttribLFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribLFormat'.");
            }
        }
        public static void glVertexAttribLFormat(GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset) => glVertexAttribLFormatPtr(@attribindex, @size, @type, @relativeoffset);

        internal delegate void glVertexAttribBindingFunc(GLuint @attribindex, GLuint @bindingindex);
        internal static glVertexAttribBindingFunc glVertexAttribBindingPtr;
        internal static void loadVertexAttribBinding()
        {
            try
            {
                glVertexAttribBindingPtr = (glVertexAttribBindingFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribBinding"), typeof(glVertexAttribBindingFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribBinding'.");
            }
        }
        public static void glVertexAttribBinding(GLuint @attribindex, GLuint @bindingindex) => glVertexAttribBindingPtr(@attribindex, @bindingindex);

        internal delegate void glVertexBindingDivisorFunc(GLuint @bindingindex, GLuint @divisor);
        internal static glVertexBindingDivisorFunc glVertexBindingDivisorPtr;
        internal static void loadVertexBindingDivisor()
        {
            try
            {
                glVertexBindingDivisorPtr = (glVertexBindingDivisorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexBindingDivisor"), typeof(glVertexBindingDivisorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexBindingDivisor'.");
            }
        }
        public static void glVertexBindingDivisor(GLuint @bindingindex, GLuint @divisor) => glVertexBindingDivisorPtr(@bindingindex, @divisor);

        internal delegate void glDebugMessageControlFunc(GLenum @source, GLenum @type, GLenum @severity, GLsizei @count, const GLuint * @ids, GLboolean @enabled);
        internal static glDebugMessageControlFunc glDebugMessageControlPtr;
        internal static void loadDebugMessageControl()
        {
            try
            {
                glDebugMessageControlPtr = (glDebugMessageControlFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageControl"), typeof(glDebugMessageControlFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageControl'.");
            }
        }
        public static void glDebugMessageControl(GLenum @source, GLenum @type, GLenum @severity, GLsizei @count, const GLuint * @ids, GLboolean @enabled) => glDebugMessageControlPtr(@source, @type, @severity, @count, @ids, @enabled);

        internal delegate void glDebugMessageInsertFunc(GLenum @source, GLenum @type, GLuint @id, GLenum @severity, GLsizei @length, const GLchar * @buf);
        internal static glDebugMessageInsertFunc glDebugMessageInsertPtr;
        internal static void loadDebugMessageInsert()
        {
            try
            {
                glDebugMessageInsertPtr = (glDebugMessageInsertFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageInsert"), typeof(glDebugMessageInsertFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageInsert'.");
            }
        }
        public static void glDebugMessageInsert(GLenum @source, GLenum @type, GLuint @id, GLenum @severity, GLsizei @length, const GLchar * @buf) => glDebugMessageInsertPtr(@source, @type, @id, @severity, @length, @buf);

        internal delegate void glDebugMessageCallbackFunc(GLDEBUGPROC @callback, const void * @userParam);
        internal static glDebugMessageCallbackFunc glDebugMessageCallbackPtr;
        internal static void loadDebugMessageCallback()
        {
            try
            {
                glDebugMessageCallbackPtr = (glDebugMessageCallbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageCallback"), typeof(glDebugMessageCallbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageCallback'.");
            }
        }
        public static void glDebugMessageCallback(GLDEBUGPROC @callback, const void * @userParam) => glDebugMessageCallbackPtr(@callback, @userParam);

        internal delegate GLuint glGetDebugMessageLogFunc(GLuint @count, GLsizei @bufSize, GLenum * @sources, GLenum * @types, GLuint * @ids, GLenum * @severities, GLsizei * @lengths, GLchar * @messageLog);
        internal static glGetDebugMessageLogFunc glGetDebugMessageLogPtr;
        internal static void loadGetDebugMessageLog()
        {
            try
            {
                glGetDebugMessageLogPtr = (glGetDebugMessageLogFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDebugMessageLog"), typeof(glGetDebugMessageLogFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDebugMessageLog'.");
            }
        }
        public static GLuint glGetDebugMessageLog(GLuint @count, GLsizei @bufSize, GLenum * @sources, GLenum * @types, GLuint * @ids, GLenum * @severities, GLsizei * @lengths, GLchar * @messageLog) => glGetDebugMessageLogPtr(@count, @bufSize, @sources, @types, @ids, @severities, @lengths, @messageLog);

        internal delegate void glPushDebugGroupFunc(GLenum @source, GLuint @id, GLsizei @length, const GLchar * @message);
        internal static glPushDebugGroupFunc glPushDebugGroupPtr;
        internal static void loadPushDebugGroup()
        {
            try
            {
                glPushDebugGroupPtr = (glPushDebugGroupFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPushDebugGroup"), typeof(glPushDebugGroupFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPushDebugGroup'.");
            }
        }
        public static void glPushDebugGroup(GLenum @source, GLuint @id, GLsizei @length, const GLchar * @message) => glPushDebugGroupPtr(@source, @id, @length, @message);

        internal delegate void glPopDebugGroupFunc();
        internal static glPopDebugGroupFunc glPopDebugGroupPtr;
        internal static void loadPopDebugGroup()
        {
            try
            {
                glPopDebugGroupPtr = (glPopDebugGroupFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPopDebugGroup"), typeof(glPopDebugGroupFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPopDebugGroup'.");
            }
        }
        public static void glPopDebugGroup() => glPopDebugGroupPtr();

        internal delegate void glObjectLabelFunc(GLenum @identifier, GLuint @name, GLsizei @length, const GLchar * @label);
        internal static glObjectLabelFunc glObjectLabelPtr;
        internal static void loadObjectLabel()
        {
            try
            {
                glObjectLabelPtr = (glObjectLabelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glObjectLabel"), typeof(glObjectLabelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glObjectLabel'.");
            }
        }
        public static void glObjectLabel(GLenum @identifier, GLuint @name, GLsizei @length, const GLchar * @label) => glObjectLabelPtr(@identifier, @name, @length, @label);

        internal delegate void glGetObjectLabelFunc(GLenum @identifier, GLuint @name, GLsizei @bufSize, GLsizei * @length, GLchar * @label);
        internal static glGetObjectLabelFunc glGetObjectLabelPtr;
        internal static void loadGetObjectLabel()
        {
            try
            {
                glGetObjectLabelPtr = (glGetObjectLabelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectLabel"), typeof(glGetObjectLabelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectLabel'.");
            }
        }
        public static void glGetObjectLabel(GLenum @identifier, GLuint @name, GLsizei @bufSize, GLsizei * @length, GLchar * @label) => glGetObjectLabelPtr(@identifier, @name, @bufSize, @length, @label);

        internal delegate void glObjectPtrLabelFunc(const void * @ptr, GLsizei @length, const GLchar * @label);
        internal static glObjectPtrLabelFunc glObjectPtrLabelPtr;
        internal static void loadObjectPtrLabel()
        {
            try
            {
                glObjectPtrLabelPtr = (glObjectPtrLabelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glObjectPtrLabel"), typeof(glObjectPtrLabelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glObjectPtrLabel'.");
            }
        }
        public static void glObjectPtrLabel(const void * @ptr, GLsizei @length, const GLchar * @label) => glObjectPtrLabelPtr(@ptr, @length, @label);

        internal delegate void glGetObjectPtrLabelFunc(const void * @ptr, GLsizei @bufSize, GLsizei * @length, GLchar * @label);
        internal static glGetObjectPtrLabelFunc glGetObjectPtrLabelPtr;
        internal static void loadGetObjectPtrLabel()
        {
            try
            {
                glGetObjectPtrLabelPtr = (glGetObjectPtrLabelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectPtrLabel"), typeof(glGetObjectPtrLabelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectPtrLabel'.");
            }
        }
        public static void glGetObjectPtrLabel(const void * @ptr, GLsizei @bufSize, GLsizei * @length, GLchar * @label) => glGetObjectPtrLabelPtr(@ptr, @bufSize, @length, @label);

        internal delegate void glGetPointervFunc(GLenum @pname, void ** @params);
        internal static glGetPointervFunc glGetPointervPtr;
        internal static void loadGetPointerv()
        {
            try
            {
                glGetPointervPtr = (glGetPointervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPointerv"), typeof(glGetPointervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPointerv'.");
            }
        }
        public static void glGetPointerv(GLenum @pname, void ** @params) => glGetPointervPtr(@pname, @params);

        #endregion
    }
}
