using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GLES30
    {
        #region Interop
        static GLES30()
        {
            Console.WriteLine("Initalising GLES30 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadReadBuffer();
            loadDrawRangeElements();
            loadTexImage3D();
            loadTexSubImage3D();
            loadCopyTexSubImage3D();
            loadCompressedTexImage3D();
            loadCompressedTexSubImage3D();
            loadGenQueries();
            loadDeleteQueries();
            loadIsQuery();
            loadBeginQuery();
            loadEndQuery();
            loadGetQueryiv();
            loadGetQueryObjectuiv();
            loadUnmapBuffer();
            loadGetBufferPointerv();
            loadDrawBuffers();
            loadUniformMatrix2x3fv();
            loadUniformMatrix3x2fv();
            loadUniformMatrix2x4fv();
            loadUniformMatrix4x2fv();
            loadUniformMatrix3x4fv();
            loadUniformMatrix4x3fv();
            loadBlitFramebuffer();
            loadRenderbufferStorageMultisample();
            loadFramebufferTextureLayer();
            loadMapBufferRange();
            loadFlushMappedBufferRange();
            loadBindVertexArray();
            loadDeleteVertexArrays();
            loadGenVertexArrays();
            loadIsVertexArray();
            loadGetIntegeri_v();
            loadBeginTransformFeedback();
            loadEndTransformFeedback();
            loadBindBufferRange();
            loadBindBufferBase();
            loadTransformFeedbackVaryings();
            loadGetTransformFeedbackVarying();
            loadVertexAttribIPointer();
            loadGetVertexAttribIiv();
            loadGetVertexAttribIuiv();
            loadVertexAttribI4i();
            loadVertexAttribI4ui();
            loadVertexAttribI4iv();
            loadVertexAttribI4uiv();
            loadGetUniformuiv();
            loadGetFragDataLocation();
            loadUniform1ui();
            loadUniform2ui();
            loadUniform3ui();
            loadUniform4ui();
            loadUniform1uiv();
            loadUniform2uiv();
            loadUniform3uiv();
            loadUniform4uiv();
            loadClearBufferiv();
            loadClearBufferuiv();
            loadClearBufferfv();
            loadClearBufferfi();
            loadGetStringi();
            loadCopyBufferSubData();
            loadGetUniformIndices();
            loadGetActiveUniformsiv();
            loadGetUniformBlockIndex();
            loadGetActiveUniformBlockiv();
            loadGetActiveUniformBlockName();
            loadUniformBlockBinding();
            loadDrawArraysInstanced();
            loadDrawElementsInstanced();
            loadFenceSync();
            loadIsSync();
            loadDeleteSync();
            loadClientWaitSync();
            loadWaitSync();
            loadGetInteger64v();
            loadGetSynciv();
            loadGetInteger64i_v();
            loadGetBufferParameteri64v();
            loadGenSamplers();
            loadDeleteSamplers();
            loadIsSampler();
            loadBindSampler();
            loadSamplerParameteri();
            loadSamplerParameteriv();
            loadSamplerParameterf();
            loadSamplerParameterfv();
            loadGetSamplerParameteriv();
            loadGetSamplerParameterfv();
            loadVertexAttribDivisor();
            loadBindTransformFeedback();
            loadDeleteTransformFeedbacks();
            loadGenTransformFeedbacks();
            loadIsTransformFeedback();
            loadPauseTransformFeedback();
            loadResumeTransformFeedback();
            loadGetProgramBinary();
            loadProgramBinary();
            loadProgramParameteri();
            loadInvalidateFramebuffer();
            loadInvalidateSubFramebuffer();
            loadTexStorage2D();
            loadTexStorage3D();
            loadGetInternalformativ();
        }
        #endregion

        #region Enums
        public static UInt32 GL_READ_BUFFER = 0x0C02;
        public static UInt32 GL_UNPACK_ROW_LENGTH = 0x0CF2;
        public static UInt32 GL_UNPACK_SKIP_ROWS = 0x0CF3;
        public static UInt32 GL_UNPACK_SKIP_PIXELS = 0x0CF4;
        public static UInt32 GL_PACK_ROW_LENGTH = 0x0D02;
        public static UInt32 GL_PACK_SKIP_ROWS = 0x0D03;
        public static UInt32 GL_PACK_SKIP_PIXELS = 0x0D04;
        public static UInt32 GL_COLOR = 0x1800;
        public static UInt32 GL_DEPTH = 0x1801;
        public static UInt32 GL_STENCIL = 0x1802;
        public static UInt32 GL_RED = 0x1903;
        public static UInt32 GL_RGB8 = 0x8051;
        public static UInt32 GL_RGBA8 = 0x8058;
        public static UInt32 GL_RGB10_A2 = 0x8059;
        public static UInt32 GL_TEXTURE_BINDING_3D = 0x806A;
        public static UInt32 GL_UNPACK_SKIP_IMAGES = 0x806D;
        public static UInt32 GL_UNPACK_IMAGE_HEIGHT = 0x806E;
        public static UInt32 GL_TEXTURE_3D = 0x806F;
        public static UInt32 GL_TEXTURE_WRAP_R = 0x8072;
        public static UInt32 GL_MAX_3D_TEXTURE_SIZE = 0x8073;
        public static UInt32 GL_UNSIGNED_INT_2_10_10_10_REV = 0x8368;
        public static UInt32 GL_MAX_ELEMENTS_VERTICES = 0x80E8;
        public static UInt32 GL_MAX_ELEMENTS_INDICES = 0x80E9;
        public static UInt32 GL_TEXTURE_MIN_LOD = 0x813A;
        public static UInt32 GL_TEXTURE_MAX_LOD = 0x813B;
        public static UInt32 GL_TEXTURE_BASE_LEVEL = 0x813C;
        public static UInt32 GL_TEXTURE_MAX_LEVEL = 0x813D;
        public static UInt32 GL_MIN = 0x8007;
        public static UInt32 GL_MAX = 0x8008;
        public static UInt32 GL_DEPTH_COMPONENT24 = 0x81A6;
        public static UInt32 GL_MAX_TEXTURE_LOD_BIAS = 0x84FD;
        public static UInt32 GL_TEXTURE_COMPARE_MODE = 0x884C;
        public static UInt32 GL_TEXTURE_COMPARE_FUNC = 0x884D;
        public static UInt32 GL_CURRENT_QUERY = 0x8865;
        public static UInt32 GL_QUERY_RESULT = 0x8866;
        public static UInt32 GL_QUERY_RESULT_AVAILABLE = 0x8867;
        public static UInt32 GL_BUFFER_MAPPED = 0x88BC;
        public static UInt32 GL_BUFFER_MAP_POINTER = 0x88BD;
        public static UInt32 GL_STREAM_READ = 0x88E1;
        public static UInt32 GL_STREAM_COPY = 0x88E2;
        public static UInt32 GL_STATIC_READ = 0x88E5;
        public static UInt32 GL_STATIC_COPY = 0x88E6;
        public static UInt32 GL_DYNAMIC_READ = 0x88E9;
        public static UInt32 GL_DYNAMIC_COPY = 0x88EA;
        public static UInt32 GL_MAX_DRAW_BUFFERS = 0x8824;
        public static UInt32 GL_DRAW_BUFFER0 = 0x8825;
        public static UInt32 GL_DRAW_BUFFER1 = 0x8826;
        public static UInt32 GL_DRAW_BUFFER2 = 0x8827;
        public static UInt32 GL_DRAW_BUFFER3 = 0x8828;
        public static UInt32 GL_DRAW_BUFFER4 = 0x8829;
        public static UInt32 GL_DRAW_BUFFER5 = 0x882A;
        public static UInt32 GL_DRAW_BUFFER6 = 0x882B;
        public static UInt32 GL_DRAW_BUFFER7 = 0x882C;
        public static UInt32 GL_DRAW_BUFFER8 = 0x882D;
        public static UInt32 GL_DRAW_BUFFER9 = 0x882E;
        public static UInt32 GL_DRAW_BUFFER10 = 0x882F;
        public static UInt32 GL_DRAW_BUFFER11 = 0x8830;
        public static UInt32 GL_DRAW_BUFFER12 = 0x8831;
        public static UInt32 GL_DRAW_BUFFER13 = 0x8832;
        public static UInt32 GL_DRAW_BUFFER14 = 0x8833;
        public static UInt32 GL_DRAW_BUFFER15 = 0x8834;
        public static UInt32 GL_MAX_FRAGMENT_UNIFORM_COMPONENTS = 0x8B49;
        public static UInt32 GL_MAX_VERTEX_UNIFORM_COMPONENTS = 0x8B4A;
        public static UInt32 GL_SAMPLER_3D = 0x8B5F;
        public static UInt32 GL_SAMPLER_2D_SHADOW = 0x8B62;
        public static UInt32 GL_FRAGMENT_SHADER_DERIVATIVE_HINT = 0x8B8B;
        public static UInt32 GL_PIXEL_PACK_BUFFER = 0x88EB;
        public static UInt32 GL_PIXEL_UNPACK_BUFFER = 0x88EC;
        public static UInt32 GL_PIXEL_PACK_BUFFER_BINDING = 0x88ED;
        public static UInt32 GL_PIXEL_UNPACK_BUFFER_BINDING = 0x88EF;
        public static UInt32 GL_FLOAT_MAT2x3 = 0x8B65;
        public static UInt32 GL_FLOAT_MAT2x4 = 0x8B66;
        public static UInt32 GL_FLOAT_MAT3x2 = 0x8B67;
        public static UInt32 GL_FLOAT_MAT3x4 = 0x8B68;
        public static UInt32 GL_FLOAT_MAT4x2 = 0x8B69;
        public static UInt32 GL_FLOAT_MAT4x3 = 0x8B6A;
        public static UInt32 GL_SRGB = 0x8C40;
        public static UInt32 GL_SRGB8 = 0x8C41;
        public static UInt32 GL_SRGB8_ALPHA8 = 0x8C43;
        public static UInt32 GL_COMPARE_REF_TO_TEXTURE = 0x884E;
        public static UInt32 GL_MAJOR_VERSION = 0x821B;
        public static UInt32 GL_MINOR_VERSION = 0x821C;
        public static UInt32 GL_NUM_EXTENSIONS = 0x821D;
        public static UInt32 GL_RGBA32F = 0x8814;
        public static UInt32 GL_RGB32F = 0x8815;
        public static UInt32 GL_RGBA16F = 0x881A;
        public static UInt32 GL_RGB16F = 0x881B;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_INTEGER = 0x88FD;
        public static UInt32 GL_MAX_ARRAY_TEXTURE_LAYERS = 0x88FF;
        public static UInt32 GL_MIN_PROGRAM_TEXEL_OFFSET = 0x8904;
        public static UInt32 GL_MAX_PROGRAM_TEXEL_OFFSET = 0x8905;
        public static UInt32 GL_MAX_VARYING_COMPONENTS = 0x8B4B;
        public static UInt32 GL_TEXTURE_2D_ARRAY = 0x8C1A;
        public static UInt32 GL_TEXTURE_BINDING_2D_ARRAY = 0x8C1D;
        public static UInt32 GL_R11F_G11F_B10F = 0x8C3A;
        public static UInt32 GL_UNSIGNED_INT_10F_11F_11F_REV = 0x8C3B;
        public static UInt32 GL_RGB9_E5 = 0x8C3D;
        public static UInt32 GL_UNSIGNED_INT_5_9_9_9_REV = 0x8C3E;
        public static UInt32 GL_TRANSFORM_FEEDBACK_VARYING_MAX_LENGTH = 0x8C76;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_MODE = 0x8C7F;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS = 0x8C80;
        public static UInt32 GL_TRANSFORM_FEEDBACK_VARYINGS = 0x8C83;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_START = 0x8C84;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_SIZE = 0x8C85;
        public static UInt32 GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN = 0x8C88;
        public static UInt32 GL_RASTERIZER_DISCARD = 0x8C89;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS = 0x8C8A;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS = 0x8C8B;
        public static UInt32 GL_INTERLEAVED_ATTRIBS = 0x8C8C;
        public static UInt32 GL_SEPARATE_ATTRIBS = 0x8C8D;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER = 0x8C8E;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_BINDING = 0x8C8F;
        public static UInt32 GL_RGBA32UI = 0x8D70;
        public static UInt32 GL_RGB32UI = 0x8D71;
        public static UInt32 GL_RGBA16UI = 0x8D76;
        public static UInt32 GL_RGB16UI = 0x8D77;
        public static UInt32 GL_RGBA8UI = 0x8D7C;
        public static UInt32 GL_RGB8UI = 0x8D7D;
        public static UInt32 GL_RGBA32I = 0x8D82;
        public static UInt32 GL_RGB32I = 0x8D83;
        public static UInt32 GL_RGBA16I = 0x8D88;
        public static UInt32 GL_RGB16I = 0x8D89;
        public static UInt32 GL_RGBA8I = 0x8D8E;
        public static UInt32 GL_RGB8I = 0x8D8F;
        public static UInt32 GL_RED_INTEGER = 0x8D94;
        public static UInt32 GL_RGB_INTEGER = 0x8D98;
        public static UInt32 GL_RGBA_INTEGER = 0x8D99;
        public static UInt32 GL_SAMPLER_2D_ARRAY = 0x8DC1;
        public static UInt32 GL_SAMPLER_2D_ARRAY_SHADOW = 0x8DC4;
        public static UInt32 GL_SAMPLER_CUBE_SHADOW = 0x8DC5;
        public static UInt32 GL_UNSIGNED_INT_VEC2 = 0x8DC6;
        public static UInt32 GL_UNSIGNED_INT_VEC3 = 0x8DC7;
        public static UInt32 GL_UNSIGNED_INT_VEC4 = 0x8DC8;
        public static UInt32 GL_INT_SAMPLER_2D = 0x8DCA;
        public static UInt32 GL_INT_SAMPLER_3D = 0x8DCB;
        public static UInt32 GL_INT_SAMPLER_CUBE = 0x8DCC;
        public static UInt32 GL_INT_SAMPLER_2D_ARRAY = 0x8DCF;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D = 0x8DD2;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_3D = 0x8DD3;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_CUBE = 0x8DD4;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_ARRAY = 0x8DD7;
        public static UInt32 GL_BUFFER_ACCESS_FLAGS = 0x911F;
        public static UInt32 GL_BUFFER_MAP_LENGTH = 0x9120;
        public static UInt32 GL_BUFFER_MAP_OFFSET = 0x9121;
        public static UInt32 GL_DEPTH_COMPONENT32F = 0x8CAC;
        public static UInt32 GL_DEPTH32F_STENCIL8 = 0x8CAD;
        public static UInt32 GL_FLOAT_32_UNSIGNED_INT_24_8_REV = 0x8DAD;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING = 0x8210;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE = 0x8211;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_RED_SIZE = 0x8212;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_GREEN_SIZE = 0x8213;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_BLUE_SIZE = 0x8214;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE = 0x8215;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE = 0x8216;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE = 0x8217;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT = 0x8218;
        public static UInt32 GL_FRAMEBUFFER_UNDEFINED = 0x8219;
        public static UInt32 GL_DEPTH_STENCIL_ATTACHMENT = 0x821A;
        public static UInt32 GL_DEPTH_STENCIL = 0x84F9;
        public static UInt32 GL_UNSIGNED_INT_24_8 = 0x84FA;
        public static UInt32 GL_DEPTH24_STENCIL8 = 0x88F0;
        public static UInt32 GL_UNSIGNED_NORMALIZED = 0x8C17;
        public static UInt32 GL_DRAW_FRAMEBUFFER_BINDING = 0x8CA6;
        public static UInt32 GL_READ_FRAMEBUFFER = 0x8CA8;
        public static UInt32 GL_DRAW_FRAMEBUFFER = 0x8CA9;
        public static UInt32 GL_READ_FRAMEBUFFER_BINDING = 0x8CAA;
        public static UInt32 GL_RENDERBUFFER_SAMPLES = 0x8CAB;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER = 0x8CD4;
        public static UInt32 GL_MAX_COLOR_ATTACHMENTS = 0x8CDF;
        public static UInt32 GL_COLOR_ATTACHMENT1 = 0x8CE1;
        public static UInt32 GL_COLOR_ATTACHMENT2 = 0x8CE2;
        public static UInt32 GL_COLOR_ATTACHMENT3 = 0x8CE3;
        public static UInt32 GL_COLOR_ATTACHMENT4 = 0x8CE4;
        public static UInt32 GL_COLOR_ATTACHMENT5 = 0x8CE5;
        public static UInt32 GL_COLOR_ATTACHMENT6 = 0x8CE6;
        public static UInt32 GL_COLOR_ATTACHMENT7 = 0x8CE7;
        public static UInt32 GL_COLOR_ATTACHMENT8 = 0x8CE8;
        public static UInt32 GL_COLOR_ATTACHMENT9 = 0x8CE9;
        public static UInt32 GL_COLOR_ATTACHMENT10 = 0x8CEA;
        public static UInt32 GL_COLOR_ATTACHMENT11 = 0x8CEB;
        public static UInt32 GL_COLOR_ATTACHMENT12 = 0x8CEC;
        public static UInt32 GL_COLOR_ATTACHMENT13 = 0x8CED;
        public static UInt32 GL_COLOR_ATTACHMENT14 = 0x8CEE;
        public static UInt32 GL_COLOR_ATTACHMENT15 = 0x8CEF;
        public static UInt32 GL_COLOR_ATTACHMENT16 = 0x8CF0;
        public static UInt32 GL_COLOR_ATTACHMENT17 = 0x8CF1;
        public static UInt32 GL_COLOR_ATTACHMENT18 = 0x8CF2;
        public static UInt32 GL_COLOR_ATTACHMENT19 = 0x8CF3;
        public static UInt32 GL_COLOR_ATTACHMENT20 = 0x8CF4;
        public static UInt32 GL_COLOR_ATTACHMENT21 = 0x8CF5;
        public static UInt32 GL_COLOR_ATTACHMENT22 = 0x8CF6;
        public static UInt32 GL_COLOR_ATTACHMENT23 = 0x8CF7;
        public static UInt32 GL_COLOR_ATTACHMENT24 = 0x8CF8;
        public static UInt32 GL_COLOR_ATTACHMENT25 = 0x8CF9;
        public static UInt32 GL_COLOR_ATTACHMENT26 = 0x8CFA;
        public static UInt32 GL_COLOR_ATTACHMENT27 = 0x8CFB;
        public static UInt32 GL_COLOR_ATTACHMENT28 = 0x8CFC;
        public static UInt32 GL_COLOR_ATTACHMENT29 = 0x8CFD;
        public static UInt32 GL_COLOR_ATTACHMENT30 = 0x8CFE;
        public static UInt32 GL_COLOR_ATTACHMENT31 = 0x8CFF;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE = 0x8D56;
        public static UInt32 GL_MAX_SAMPLES = 0x8D57;
        public static UInt32 GL_HALF_FLOAT = 0x140B;
        public static UInt32 GL_MAP_READ_BIT = 0x0001;
        public static UInt32 GL_MAP_WRITE_BIT = 0x0002;
        public static UInt32 GL_MAP_INVALIDATE_RANGE_BIT = 0x0004;
        public static UInt32 GL_MAP_INVALIDATE_BUFFER_BIT = 0x0008;
        public static UInt32 GL_MAP_FLUSH_EXPLICIT_BIT = 0x0010;
        public static UInt32 GL_MAP_UNSYNCHRONIZED_BIT = 0x0020;
        public static UInt32 GL_RG = 0x8227;
        public static UInt32 GL_RG_INTEGER = 0x8228;
        public static UInt32 GL_R8 = 0x8229;
        public static UInt32 GL_RG8 = 0x822B;
        public static UInt32 GL_R16F = 0x822D;
        public static UInt32 GL_R32F = 0x822E;
        public static UInt32 GL_RG16F = 0x822F;
        public static UInt32 GL_RG32F = 0x8230;
        public static UInt32 GL_R8I = 0x8231;
        public static UInt32 GL_R8UI = 0x8232;
        public static UInt32 GL_R16I = 0x8233;
        public static UInt32 GL_R16UI = 0x8234;
        public static UInt32 GL_R32I = 0x8235;
        public static UInt32 GL_R32UI = 0x8236;
        public static UInt32 GL_RG8I = 0x8237;
        public static UInt32 GL_RG8UI = 0x8238;
        public static UInt32 GL_RG16I = 0x8239;
        public static UInt32 GL_RG16UI = 0x823A;
        public static UInt32 GL_RG32I = 0x823B;
        public static UInt32 GL_RG32UI = 0x823C;
        public static UInt32 GL_VERTEX_ARRAY_BINDING = 0x85B5;
        public static UInt32 GL_R8_SNORM = 0x8F94;
        public static UInt32 GL_RG8_SNORM = 0x8F95;
        public static UInt32 GL_RGB8_SNORM = 0x8F96;
        public static UInt32 GL_RGBA8_SNORM = 0x8F97;
        public static UInt32 GL_SIGNED_NORMALIZED = 0x8F9C;
        public static UInt32 GL_PRIMITIVE_RESTART_FIXED_INDEX = 0x8D69;
        public static UInt32 GL_COPY_READ_BUFFER = 0x8F36;
        public static UInt32 GL_COPY_WRITE_BUFFER = 0x8F37;
        public static UInt32 GL_COPY_READ_BUFFER_BINDING = 0x8F36;
        public static UInt32 GL_COPY_WRITE_BUFFER_BINDING = 0x8F37;
        public static UInt32 GL_UNIFORM_BUFFER = 0x8A11;
        public static UInt32 GL_UNIFORM_BUFFER_BINDING = 0x8A28;
        public static UInt32 GL_UNIFORM_BUFFER_START = 0x8A29;
        public static UInt32 GL_UNIFORM_BUFFER_SIZE = 0x8A2A;
        public static UInt32 GL_MAX_VERTEX_UNIFORM_BLOCKS = 0x8A2B;
        public static UInt32 GL_MAX_FRAGMENT_UNIFORM_BLOCKS = 0x8A2D;
        public static UInt32 GL_MAX_COMBINED_UNIFORM_BLOCKS = 0x8A2E;
        public static UInt32 GL_MAX_UNIFORM_BUFFER_BINDINGS = 0x8A2F;
        public static UInt32 GL_MAX_UNIFORM_BLOCK_SIZE = 0x8A30;
        public static UInt32 GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS = 0x8A31;
        public static UInt32 GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS = 0x8A33;
        public static UInt32 GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT = 0x8A34;
        public static UInt32 GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH = 0x8A35;
        public static UInt32 GL_ACTIVE_UNIFORM_BLOCKS = 0x8A36;
        public static UInt32 GL_UNIFORM_TYPE = 0x8A37;
        public static UInt32 GL_UNIFORM_SIZE = 0x8A38;
        public static UInt32 GL_UNIFORM_NAME_LENGTH = 0x8A39;
        public static UInt32 GL_UNIFORM_BLOCK_INDEX = 0x8A3A;
        public static UInt32 GL_UNIFORM_OFFSET = 0x8A3B;
        public static UInt32 GL_UNIFORM_ARRAY_STRIDE = 0x8A3C;
        public static UInt32 GL_UNIFORM_MATRIX_STRIDE = 0x8A3D;
        public static UInt32 GL_UNIFORM_IS_ROW_MAJOR = 0x8A3E;
        public static UInt32 GL_UNIFORM_BLOCK_BINDING = 0x8A3F;
        public static UInt32 GL_UNIFORM_BLOCK_DATA_SIZE = 0x8A40;
        public static UInt32 GL_UNIFORM_BLOCK_NAME_LENGTH = 0x8A41;
        public static UInt32 GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS = 0x8A42;
        public static UInt32 GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES = 0x8A43;
        public static UInt32 GL_UNIFORM_BLOCK_REFERENCED_BY_VERTEX_SHADER = 0x8A44;
        public static UInt32 GL_UNIFORM_BLOCK_REFERENCED_BY_FRAGMENT_SHADER = 0x8A46;
        public static UInt32 GL_INVALID_INDEX = 0xFFFFFFFF;
        public static UInt32 GL_MAX_VERTEX_OUTPUT_COMPONENTS = 0x9122;
        public static UInt32 GL_MAX_FRAGMENT_INPUT_COMPONENTS = 0x9125;
        public static UInt32 GL_MAX_SERVER_WAIT_TIMEOUT = 0x9111;
        public static UInt32 GL_OBJECT_TYPE = 0x9112;
        public static UInt32 GL_SYNC_CONDITION = 0x9113;
        public static UInt32 GL_SYNC_STATUS = 0x9114;
        public static UInt32 GL_SYNC_FLAGS = 0x9115;
        public static UInt32 GL_SYNC_FENCE = 0x9116;
        public static UInt32 GL_SYNC_GPU_COMMANDS_COMPLETE = 0x9117;
        public static UInt32 GL_UNSIGNALED = 0x9118;
        public static UInt32 GL_SIGNALED = 0x9119;
        public static UInt32 GL_ALREADY_SIGNALED = 0x911A;
        public static UInt32 GL_TIMEOUT_EXPIRED = 0x911B;
        public static UInt32 GL_CONDITION_SATISFIED = 0x911C;
        public static UInt32 GL_WAIT_FAILED = 0x911D;
        public static UInt32 GL_SYNC_FLUSH_COMMANDS_BIT = 0x00000001;
        public static UInt64 GL_TIMEOUT_IGNORED = 0xFFFFFFFFFFFFFFFF;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_DIVISOR = 0x88FE;
        public static UInt32 GL_ANY_SAMPLES_PASSED = 0x8C2F;
        public static UInt32 GL_ANY_SAMPLES_PASSED_CONSERVATIVE = 0x8D6A;
        public static UInt32 GL_SAMPLER_BINDING = 0x8919;
        public static UInt32 GL_RGB10_A2UI = 0x906F;
        public static UInt32 GL_TEXTURE_SWIZZLE_R = 0x8E42;
        public static UInt32 GL_TEXTURE_SWIZZLE_G = 0x8E43;
        public static UInt32 GL_TEXTURE_SWIZZLE_B = 0x8E44;
        public static UInt32 GL_TEXTURE_SWIZZLE_A = 0x8E45;
        public static UInt32 GL_GREEN = 0x1904;
        public static UInt32 GL_BLUE = 0x1905;
        public static UInt32 GL_INT_2_10_10_10_REV = 0x8D9F;
        public static UInt32 GL_TRANSFORM_FEEDBACK = 0x8E22;
        public static UInt32 GL_TRANSFORM_FEEDBACK_PAUSED = 0x8E23;
        public static UInt32 GL_TRANSFORM_FEEDBACK_ACTIVE = 0x8E24;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BINDING = 0x8E25;
        public static UInt32 GL_PROGRAM_BINARY_RETRIEVABLE_HINT = 0x8257;
        public static UInt32 GL_PROGRAM_BINARY_LENGTH = 0x8741;
        public static UInt32 GL_NUM_PROGRAM_BINARY_FORMATS = 0x87FE;
        public static UInt32 GL_PROGRAM_BINARY_FORMATS = 0x87FF;
        public static UInt32 GL_COMPRESSED_R11_EAC = 0x9270;
        public static UInt32 GL_COMPRESSED_SIGNED_R11_EAC = 0x9271;
        public static UInt32 GL_COMPRESSED_RG11_EAC = 0x9272;
        public static UInt32 GL_COMPRESSED_SIGNED_RG11_EAC = 0x9273;
        public static UInt32 GL_COMPRESSED_RGB8_ETC2 = 0x9274;
        public static UInt32 GL_COMPRESSED_SRGB8_ETC2 = 0x9275;
        public static UInt32 GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2 = 0x9276;
        public static UInt32 GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2 = 0x9277;
        public static UInt32 GL_COMPRESSED_RGBA8_ETC2_EAC = 0x9278;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC = 0x9279;
        public static UInt32 GL_TEXTURE_IMMUTABLE_FORMAT = 0x912F;
        public static UInt32 GL_MAX_ELEMENT_INDEX = 0x8D6B;
        public static UInt32 GL_NUM_SAMPLE_COUNTS = 0x9380;
        public static UInt32 GL_TEXTURE_IMMUTABLE_LEVELS = 0x82DF;
        #endregion

        #region Commands
        internal delegate void glReadBufferFunc(GLenum @src);
        internal static glReadBufferFunc glReadBufferPtr;
        internal static void loadReadBuffer()
        {
            try
            {
                glReadBufferPtr = (glReadBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadBuffer"), typeof(glReadBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadBuffer'.");
            }
        }
        public static void glReadBuffer(GLenum @src) => glReadBufferPtr(@src);

        internal delegate void glDrawRangeElementsFunc(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices);
        internal static glDrawRangeElementsFunc glDrawRangeElementsPtr;
        internal static void loadDrawRangeElements()
        {
            try
            {
                glDrawRangeElementsPtr = (glDrawRangeElementsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawRangeElements"), typeof(glDrawRangeElementsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawRangeElements'.");
            }
        }
        public static void glDrawRangeElements(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices) => glDrawRangeElementsPtr(@mode, @start, @end, @count, @type, @indices);

        internal delegate void glTexImage3DFunc(GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexImage3DFunc glTexImage3DPtr;
        internal static void loadTexImage3D()
        {
            try
            {
                glTexImage3DPtr = (glTexImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage3D"), typeof(glTexImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage3D'.");
            }
        }
        public static void glTexImage3D(GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glTexImage3DPtr(@target, @level, @internalformat, @width, @height, @depth, @border, @format, @type, @pixels);

        internal delegate void glTexSubImage3DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexSubImage3DFunc glTexSubImage3DPtr;
        internal static void loadTexSubImage3D()
        {
            try
            {
                glTexSubImage3DPtr = (glTexSubImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexSubImage3D"), typeof(glTexSubImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexSubImage3D'.");
            }
        }
        public static void glTexSubImage3D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels) => glTexSubImage3DPtr(@target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @type, @pixels);

        internal delegate void glCopyTexSubImage3DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyTexSubImage3DFunc glCopyTexSubImage3DPtr;
        internal static void loadCopyTexSubImage3D()
        {
            try
            {
                glCopyTexSubImage3DPtr = (glCopyTexSubImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexSubImage3D"), typeof(glCopyTexSubImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexSubImage3D'.");
            }
        }
        public static void glCopyTexSubImage3D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyTexSubImage3DPtr(@target, @level, @xoffset, @yoffset, @zoffset, @x, @y, @width, @height);

        internal delegate void glCompressedTexImage3DFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexImage3DFunc glCompressedTexImage3DPtr;
        internal static void loadCompressedTexImage3D()
        {
            try
            {
                glCompressedTexImage3DPtr = (glCompressedTexImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexImage3D"), typeof(glCompressedTexImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexImage3D'.");
            }
        }
        public static void glCompressedTexImage3D(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLsizei @imageSize, const void * @data) => glCompressedTexImage3DPtr(@target, @level, @internalformat, @width, @height, @depth, @border, @imageSize, @data);

        internal delegate void glCompressedTexSubImage3DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexSubImage3DFunc glCompressedTexSubImage3DPtr;
        internal static void loadCompressedTexSubImage3D()
        {
            try
            {
                glCompressedTexSubImage3DPtr = (glCompressedTexSubImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexSubImage3D"), typeof(glCompressedTexSubImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexSubImage3D'.");
            }
        }
        public static void glCompressedTexSubImage3D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTexSubImage3DPtr(@target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @imageSize, @data);

        internal delegate void glGenQueriesFunc(GLsizei @n, GLuint * @ids);
        internal static glGenQueriesFunc glGenQueriesPtr;
        internal static void loadGenQueries()
        {
            try
            {
                glGenQueriesPtr = (glGenQueriesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenQueries"), typeof(glGenQueriesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenQueries'.");
            }
        }
        public static void glGenQueries(GLsizei @n, GLuint * @ids) => glGenQueriesPtr(@n, @ids);

        internal delegate void glDeleteQueriesFunc(GLsizei @n, const GLuint * @ids);
        internal static glDeleteQueriesFunc glDeleteQueriesPtr;
        internal static void loadDeleteQueries()
        {
            try
            {
                glDeleteQueriesPtr = (glDeleteQueriesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteQueries"), typeof(glDeleteQueriesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteQueries'.");
            }
        }
        public static void glDeleteQueries(GLsizei @n, const GLuint * @ids) => glDeleteQueriesPtr(@n, @ids);

        internal delegate GLboolean glIsQueryFunc(GLuint @id);
        internal static glIsQueryFunc glIsQueryPtr;
        internal static void loadIsQuery()
        {
            try
            {
                glIsQueryPtr = (glIsQueryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsQuery"), typeof(glIsQueryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsQuery'.");
            }
        }
        public static GLboolean glIsQuery(GLuint @id) => glIsQueryPtr(@id);

        internal delegate void glBeginQueryFunc(GLenum @target, GLuint @id);
        internal static glBeginQueryFunc glBeginQueryPtr;
        internal static void loadBeginQuery()
        {
            try
            {
                glBeginQueryPtr = (glBeginQueryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginQuery"), typeof(glBeginQueryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginQuery'.");
            }
        }
        public static void glBeginQuery(GLenum @target, GLuint @id) => glBeginQueryPtr(@target, @id);

        internal delegate void glEndQueryFunc(GLenum @target);
        internal static glEndQueryFunc glEndQueryPtr;
        internal static void loadEndQuery()
        {
            try
            {
                glEndQueryPtr = (glEndQueryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndQuery"), typeof(glEndQueryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndQuery'.");
            }
        }
        public static void glEndQuery(GLenum @target) => glEndQueryPtr(@target);

        internal delegate void glGetQueryivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetQueryivFunc glGetQueryivPtr;
        internal static void loadGetQueryiv()
        {
            try
            {
                glGetQueryivPtr = (glGetQueryivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryiv"), typeof(glGetQueryivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryiv'.");
            }
        }
        public static void glGetQueryiv(GLenum @target, GLenum @pname, GLint * @params) => glGetQueryivPtr(@target, @pname, @params);

        internal delegate void glGetQueryObjectuivFunc(GLuint @id, GLenum @pname, GLuint * @params);
        internal static glGetQueryObjectuivFunc glGetQueryObjectuivPtr;
        internal static void loadGetQueryObjectuiv()
        {
            try
            {
                glGetQueryObjectuivPtr = (glGetQueryObjectuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjectuiv"), typeof(glGetQueryObjectuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjectuiv'.");
            }
        }
        public static void glGetQueryObjectuiv(GLuint @id, GLenum @pname, GLuint * @params) => glGetQueryObjectuivPtr(@id, @pname, @params);

        internal delegate GLboolean glUnmapBufferFunc(GLenum @target);
        internal static glUnmapBufferFunc glUnmapBufferPtr;
        internal static void loadUnmapBuffer()
        {
            try
            {
                glUnmapBufferPtr = (glUnmapBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUnmapBuffer"), typeof(glUnmapBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUnmapBuffer'.");
            }
        }
        public static GLboolean glUnmapBuffer(GLenum @target) => glUnmapBufferPtr(@target);

        internal delegate void glGetBufferPointervFunc(GLenum @target, GLenum @pname, void ** @params);
        internal static glGetBufferPointervFunc glGetBufferPointervPtr;
        internal static void loadGetBufferPointerv()
        {
            try
            {
                glGetBufferPointervPtr = (glGetBufferPointervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferPointerv"), typeof(glGetBufferPointervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferPointerv'.");
            }
        }
        public static void glGetBufferPointerv(GLenum @target, GLenum @pname, void ** @params) => glGetBufferPointervPtr(@target, @pname, @params);

        internal delegate void glDrawBuffersFunc(GLsizei @n, const GLenum * @bufs);
        internal static glDrawBuffersFunc glDrawBuffersPtr;
        internal static void loadDrawBuffers()
        {
            try
            {
                glDrawBuffersPtr = (glDrawBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawBuffers"), typeof(glDrawBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawBuffers'.");
            }
        }
        public static void glDrawBuffers(GLsizei @n, const GLenum * @bufs) => glDrawBuffersPtr(@n, @bufs);

        internal delegate void glUniformMatrix2x3fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix2x3fvFunc glUniformMatrix2x3fvPtr;
        internal static void loadUniformMatrix2x3fv()
        {
            try
            {
                glUniformMatrix2x3fvPtr = (glUniformMatrix2x3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2x3fv"), typeof(glUniformMatrix2x3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2x3fv'.");
            }
        }
        public static void glUniformMatrix2x3fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix2x3fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3x2fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix3x2fvFunc glUniformMatrix3x2fvPtr;
        internal static void loadUniformMatrix3x2fv()
        {
            try
            {
                glUniformMatrix3x2fvPtr = (glUniformMatrix3x2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3x2fv"), typeof(glUniformMatrix3x2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3x2fv'.");
            }
        }
        public static void glUniformMatrix3x2fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix3x2fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix2x4fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix2x4fvFunc glUniformMatrix2x4fvPtr;
        internal static void loadUniformMatrix2x4fv()
        {
            try
            {
                glUniformMatrix2x4fvPtr = (glUniformMatrix2x4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2x4fv"), typeof(glUniformMatrix2x4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2x4fv'.");
            }
        }
        public static void glUniformMatrix2x4fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix2x4fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4x2fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix4x2fvFunc glUniformMatrix4x2fvPtr;
        internal static void loadUniformMatrix4x2fv()
        {
            try
            {
                glUniformMatrix4x2fvPtr = (glUniformMatrix4x2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4x2fv"), typeof(glUniformMatrix4x2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4x2fv'.");
            }
        }
        public static void glUniformMatrix4x2fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix4x2fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3x4fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix3x4fvFunc glUniformMatrix3x4fvPtr;
        internal static void loadUniformMatrix3x4fv()
        {
            try
            {
                glUniformMatrix3x4fvPtr = (glUniformMatrix3x4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3x4fv"), typeof(glUniformMatrix3x4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3x4fv'.");
            }
        }
        public static void glUniformMatrix3x4fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix3x4fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4x3fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix4x3fvFunc glUniformMatrix4x3fvPtr;
        internal static void loadUniformMatrix4x3fv()
        {
            try
            {
                glUniformMatrix4x3fvPtr = (glUniformMatrix4x3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4x3fv"), typeof(glUniformMatrix4x3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4x3fv'.");
            }
        }
        public static void glUniformMatrix4x3fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix4x3fvPtr(@location, @count, @transpose, @value);

        internal delegate void glBlitFramebufferFunc(GLint @srcX0, GLint @srcY0, GLint @srcX1, GLint @srcY1, GLint @dstX0, GLint @dstY0, GLint @dstX1, GLint @dstY1, GLbitfield @mask, GLenum @filter);
        internal static glBlitFramebufferFunc glBlitFramebufferPtr;
        internal static void loadBlitFramebuffer()
        {
            try
            {
                glBlitFramebufferPtr = (glBlitFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlitFramebuffer"), typeof(glBlitFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlitFramebuffer'.");
            }
        }
        public static void glBlitFramebuffer(GLint @srcX0, GLint @srcY0, GLint @srcX1, GLint @srcY1, GLint @dstX0, GLint @dstY0, GLint @dstX1, GLint @dstY1, GLbitfield @mask, GLenum @filter) => glBlitFramebufferPtr(@srcX0, @srcY0, @srcX1, @srcY1, @dstX0, @dstY0, @dstX1, @dstY1, @mask, @filter);

        internal delegate void glRenderbufferStorageMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageMultisampleFunc glRenderbufferStorageMultisamplePtr;
        internal static void loadRenderbufferStorageMultisample()
        {
            try
            {
                glRenderbufferStorageMultisamplePtr = (glRenderbufferStorageMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorageMultisample"), typeof(glRenderbufferStorageMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorageMultisample'.");
            }
        }
        public static void glRenderbufferStorageMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStorageMultisamplePtr(@target, @samples, @internalformat, @width, @height);

        internal delegate void glFramebufferTextureLayerFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer);
        internal static glFramebufferTextureLayerFunc glFramebufferTextureLayerPtr;
        internal static void loadFramebufferTextureLayer()
        {
            try
            {
                glFramebufferTextureLayerPtr = (glFramebufferTextureLayerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureLayer"), typeof(glFramebufferTextureLayerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureLayer'.");
            }
        }
        public static void glFramebufferTextureLayer(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer) => glFramebufferTextureLayerPtr(@target, @attachment, @texture, @level, @layer);

        internal delegate void * glMapBufferRangeFunc(GLenum @target, GLintptr @offset, GLsizeiptr @length, GLbitfield @access);
        internal static glMapBufferRangeFunc glMapBufferRangePtr;
        internal static void loadMapBufferRange()
        {
            try
            {
                glMapBufferRangePtr = (glMapBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapBufferRange"), typeof(glMapBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapBufferRange'.");
            }
        }
        public static void * glMapBufferRange(GLenum @target, GLintptr @offset, GLsizeiptr @length, GLbitfield @access) => glMapBufferRangePtr(@target, @offset, @length, @access);

        internal delegate void glFlushMappedBufferRangeFunc(GLenum @target, GLintptr @offset, GLsizeiptr @length);
        internal static glFlushMappedBufferRangeFunc glFlushMappedBufferRangePtr;
        internal static void loadFlushMappedBufferRange()
        {
            try
            {
                glFlushMappedBufferRangePtr = (glFlushMappedBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlushMappedBufferRange"), typeof(glFlushMappedBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlushMappedBufferRange'.");
            }
        }
        public static void glFlushMappedBufferRange(GLenum @target, GLintptr @offset, GLsizeiptr @length) => glFlushMappedBufferRangePtr(@target, @offset, @length);

        internal delegate void glBindVertexArrayFunc(GLuint @array);
        internal static glBindVertexArrayFunc glBindVertexArrayPtr;
        internal static void loadBindVertexArray()
        {
            try
            {
                glBindVertexArrayPtr = (glBindVertexArrayFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVertexArray"), typeof(glBindVertexArrayFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVertexArray'.");
            }
        }
        public static void glBindVertexArray(GLuint @array) => glBindVertexArrayPtr(@array);

        internal delegate void glDeleteVertexArraysFunc(GLsizei @n, const GLuint * @arrays);
        internal static glDeleteVertexArraysFunc glDeleteVertexArraysPtr;
        internal static void loadDeleteVertexArrays()
        {
            try
            {
                glDeleteVertexArraysPtr = (glDeleteVertexArraysFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteVertexArrays"), typeof(glDeleteVertexArraysFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteVertexArrays'.");
            }
        }
        public static void glDeleteVertexArrays(GLsizei @n, const GLuint * @arrays) => glDeleteVertexArraysPtr(@n, @arrays);

        internal delegate void glGenVertexArraysFunc(GLsizei @n, GLuint * @arrays);
        internal static glGenVertexArraysFunc glGenVertexArraysPtr;
        internal static void loadGenVertexArrays()
        {
            try
            {
                glGenVertexArraysPtr = (glGenVertexArraysFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenVertexArrays"), typeof(glGenVertexArraysFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenVertexArrays'.");
            }
        }
        public static void glGenVertexArrays(GLsizei @n, GLuint * @arrays) => glGenVertexArraysPtr(@n, @arrays);

        internal delegate GLboolean glIsVertexArrayFunc(GLuint @array);
        internal static glIsVertexArrayFunc glIsVertexArrayPtr;
        internal static void loadIsVertexArray()
        {
            try
            {
                glIsVertexArrayPtr = (glIsVertexArrayFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsVertexArray"), typeof(glIsVertexArrayFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsVertexArray'.");
            }
        }
        public static GLboolean glIsVertexArray(GLuint @array) => glIsVertexArrayPtr(@array);

        internal delegate void glGetIntegeri_vFunc(GLenum @target, GLuint @index, GLint * @data);
        internal static glGetIntegeri_vFunc glGetIntegeri_vPtr;
        internal static void loadGetIntegeri_v()
        {
            try
            {
                glGetIntegeri_vPtr = (glGetIntegeri_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetIntegeri_v"), typeof(glGetIntegeri_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetIntegeri_v'.");
            }
        }
        public static void glGetIntegeri_v(GLenum @target, GLuint @index, GLint * @data) => glGetIntegeri_vPtr(@target, @index, @data);

        internal delegate void glBeginTransformFeedbackFunc(GLenum @primitiveMode);
        internal static glBeginTransformFeedbackFunc glBeginTransformFeedbackPtr;
        internal static void loadBeginTransformFeedback()
        {
            try
            {
                glBeginTransformFeedbackPtr = (glBeginTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginTransformFeedback"), typeof(glBeginTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginTransformFeedback'.");
            }
        }
        public static void glBeginTransformFeedback(GLenum @primitiveMode) => glBeginTransformFeedbackPtr(@primitiveMode);

        internal delegate void glEndTransformFeedbackFunc();
        internal static glEndTransformFeedbackFunc glEndTransformFeedbackPtr;
        internal static void loadEndTransformFeedback()
        {
            try
            {
                glEndTransformFeedbackPtr = (glEndTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndTransformFeedback"), typeof(glEndTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndTransformFeedback'.");
            }
        }
        public static void glEndTransformFeedback() => glEndTransformFeedbackPtr();

        internal delegate void glBindBufferRangeFunc(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glBindBufferRangeFunc glBindBufferRangePtr;
        internal static void loadBindBufferRange()
        {
            try
            {
                glBindBufferRangePtr = (glBindBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferRange"), typeof(glBindBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferRange'.");
            }
        }
        public static void glBindBufferRange(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glBindBufferRangePtr(@target, @index, @buffer, @offset, @size);

        internal delegate void glBindBufferBaseFunc(GLenum @target, GLuint @index, GLuint @buffer);
        internal static glBindBufferBaseFunc glBindBufferBasePtr;
        internal static void loadBindBufferBase()
        {
            try
            {
                glBindBufferBasePtr = (glBindBufferBaseFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferBase"), typeof(glBindBufferBaseFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferBase'.");
            }
        }
        public static void glBindBufferBase(GLenum @target, GLuint @index, GLuint @buffer) => glBindBufferBasePtr(@target, @index, @buffer);

        internal delegate void glTransformFeedbackVaryingsFunc(GLuint @program, GLsizei @count, const GLchar *const* @varyings, GLenum @bufferMode);
        internal static glTransformFeedbackVaryingsFunc glTransformFeedbackVaryingsPtr;
        internal static void loadTransformFeedbackVaryings()
        {
            try
            {
                glTransformFeedbackVaryingsPtr = (glTransformFeedbackVaryingsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTransformFeedbackVaryings"), typeof(glTransformFeedbackVaryingsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTransformFeedbackVaryings'.");
            }
        }
        public static void glTransformFeedbackVaryings(GLuint @program, GLsizei @count, const GLchar *const* @varyings, GLenum @bufferMode) => glTransformFeedbackVaryingsPtr(@program, @count, @varyings, @bufferMode);

        internal delegate void glGetTransformFeedbackVaryingFunc(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLsizei * @size, GLenum * @type, GLchar * @name);
        internal static glGetTransformFeedbackVaryingFunc glGetTransformFeedbackVaryingPtr;
        internal static void loadGetTransformFeedbackVarying()
        {
            try
            {
                glGetTransformFeedbackVaryingPtr = (glGetTransformFeedbackVaryingFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTransformFeedbackVarying"), typeof(glGetTransformFeedbackVaryingFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTransformFeedbackVarying'.");
            }
        }
        public static void glGetTransformFeedbackVarying(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLsizei * @size, GLenum * @type, GLchar * @name) => glGetTransformFeedbackVaryingPtr(@program, @index, @bufSize, @length, @size, @type, @name);

        internal delegate void glVertexAttribIPointerFunc(GLuint @index, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glVertexAttribIPointerFunc glVertexAttribIPointerPtr;
        internal static void loadVertexAttribIPointer()
        {
            try
            {
                glVertexAttribIPointerPtr = (glVertexAttribIPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribIPointer"), typeof(glVertexAttribIPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribIPointer'.");
            }
        }
        public static void glVertexAttribIPointer(GLuint @index, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glVertexAttribIPointerPtr(@index, @size, @type, @stride, @pointer);

        internal delegate void glGetVertexAttribIivFunc(GLuint @index, GLenum @pname, GLint * @params);
        internal static glGetVertexAttribIivFunc glGetVertexAttribIivPtr;
        internal static void loadGetVertexAttribIiv()
        {
            try
            {
                glGetVertexAttribIivPtr = (glGetVertexAttribIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribIiv"), typeof(glGetVertexAttribIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribIiv'.");
            }
        }
        public static void glGetVertexAttribIiv(GLuint @index, GLenum @pname, GLint * @params) => glGetVertexAttribIivPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribIuivFunc(GLuint @index, GLenum @pname, GLuint * @params);
        internal static glGetVertexAttribIuivFunc glGetVertexAttribIuivPtr;
        internal static void loadGetVertexAttribIuiv()
        {
            try
            {
                glGetVertexAttribIuivPtr = (glGetVertexAttribIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribIuiv"), typeof(glGetVertexAttribIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribIuiv'.");
            }
        }
        public static void glGetVertexAttribIuiv(GLuint @index, GLenum @pname, GLuint * @params) => glGetVertexAttribIuivPtr(@index, @pname, @params);

        internal delegate void glVertexAttribI4iFunc(GLuint @index, GLint @x, GLint @y, GLint @z, GLint @w);
        internal static glVertexAttribI4iFunc glVertexAttribI4iPtr;
        internal static void loadVertexAttribI4i()
        {
            try
            {
                glVertexAttribI4iPtr = (glVertexAttribI4iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4i"), typeof(glVertexAttribI4iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4i'.");
            }
        }
        public static void glVertexAttribI4i(GLuint @index, GLint @x, GLint @y, GLint @z, GLint @w) => glVertexAttribI4iPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttribI4uiFunc(GLuint @index, GLuint @x, GLuint @y, GLuint @z, GLuint @w);
        internal static glVertexAttribI4uiFunc glVertexAttribI4uiPtr;
        internal static void loadVertexAttribI4ui()
        {
            try
            {
                glVertexAttribI4uiPtr = (glVertexAttribI4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4ui"), typeof(glVertexAttribI4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4ui'.");
            }
        }
        public static void glVertexAttribI4ui(GLuint @index, GLuint @x, GLuint @y, GLuint @z, GLuint @w) => glVertexAttribI4uiPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttribI4ivFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttribI4ivFunc glVertexAttribI4ivPtr;
        internal static void loadVertexAttribI4iv()
        {
            try
            {
                glVertexAttribI4ivPtr = (glVertexAttribI4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4iv"), typeof(glVertexAttribI4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4iv'.");
            }
        }
        public static void glVertexAttribI4iv(GLuint @index, const GLint * @v) => glVertexAttribI4ivPtr(@index, @v);

        internal delegate void glVertexAttribI4uivFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttribI4uivFunc glVertexAttribI4uivPtr;
        internal static void loadVertexAttribI4uiv()
        {
            try
            {
                glVertexAttribI4uivPtr = (glVertexAttribI4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4uiv"), typeof(glVertexAttribI4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4uiv'.");
            }
        }
        public static void glVertexAttribI4uiv(GLuint @index, const GLuint * @v) => glVertexAttribI4uivPtr(@index, @v);

        internal delegate void glGetUniformuivFunc(GLuint @program, GLint @location, GLuint * @params);
        internal static glGetUniformuivFunc glGetUniformuivPtr;
        internal static void loadGetUniformuiv()
        {
            try
            {
                glGetUniformuivPtr = (glGetUniformuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformuiv"), typeof(glGetUniformuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformuiv'.");
            }
        }
        public static void glGetUniformuiv(GLuint @program, GLint @location, GLuint * @params) => glGetUniformuivPtr(@program, @location, @params);

        internal delegate GLint glGetFragDataLocationFunc(GLuint @program, const GLchar * @name);
        internal static glGetFragDataLocationFunc glGetFragDataLocationPtr;
        internal static void loadGetFragDataLocation()
        {
            try
            {
                glGetFragDataLocationPtr = (glGetFragDataLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFragDataLocation"), typeof(glGetFragDataLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFragDataLocation'.");
            }
        }
        public static GLint glGetFragDataLocation(GLuint @program, const GLchar * @name) => glGetFragDataLocationPtr(@program, @name);

        internal delegate void glUniform1uiFunc(GLint @location, GLuint @v0);
        internal static glUniform1uiFunc glUniform1uiPtr;
        internal static void loadUniform1ui()
        {
            try
            {
                glUniform1uiPtr = (glUniform1uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1ui"), typeof(glUniform1uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1ui'.");
            }
        }
        public static void glUniform1ui(GLint @location, GLuint @v0) => glUniform1uiPtr(@location, @v0);

        internal delegate void glUniform2uiFunc(GLint @location, GLuint @v0, GLuint @v1);
        internal static glUniform2uiFunc glUniform2uiPtr;
        internal static void loadUniform2ui()
        {
            try
            {
                glUniform2uiPtr = (glUniform2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2ui"), typeof(glUniform2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2ui'.");
            }
        }
        public static void glUniform2ui(GLint @location, GLuint @v0, GLuint @v1) => glUniform2uiPtr(@location, @v0, @v1);

        internal delegate void glUniform3uiFunc(GLint @location, GLuint @v0, GLuint @v1, GLuint @v2);
        internal static glUniform3uiFunc glUniform3uiPtr;
        internal static void loadUniform3ui()
        {
            try
            {
                glUniform3uiPtr = (glUniform3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3ui"), typeof(glUniform3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3ui'.");
            }
        }
        public static void glUniform3ui(GLint @location, GLuint @v0, GLuint @v1, GLuint @v2) => glUniform3uiPtr(@location, @v0, @v1, @v2);

        internal delegate void glUniform4uiFunc(GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3);
        internal static glUniform4uiFunc glUniform4uiPtr;
        internal static void loadUniform4ui()
        {
            try
            {
                glUniform4uiPtr = (glUniform4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4ui"), typeof(glUniform4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4ui'.");
            }
        }
        public static void glUniform4ui(GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3) => glUniform4uiPtr(@location, @v0, @v1, @v2, @v3);

        internal delegate void glUniform1uivFunc(GLint @location, GLsizei @count, const GLuint * @value);
        internal static glUniform1uivFunc glUniform1uivPtr;
        internal static void loadUniform1uiv()
        {
            try
            {
                glUniform1uivPtr = (glUniform1uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1uiv"), typeof(glUniform1uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1uiv'.");
            }
        }
        public static void glUniform1uiv(GLint @location, GLsizei @count, const GLuint * @value) => glUniform1uivPtr(@location, @count, @value);

        internal delegate void glUniform2uivFunc(GLint @location, GLsizei @count, const GLuint * @value);
        internal static glUniform2uivFunc glUniform2uivPtr;
        internal static void loadUniform2uiv()
        {
            try
            {
                glUniform2uivPtr = (glUniform2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2uiv"), typeof(glUniform2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2uiv'.");
            }
        }
        public static void glUniform2uiv(GLint @location, GLsizei @count, const GLuint * @value) => glUniform2uivPtr(@location, @count, @value);

        internal delegate void glUniform3uivFunc(GLint @location, GLsizei @count, const GLuint * @value);
        internal static glUniform3uivFunc glUniform3uivPtr;
        internal static void loadUniform3uiv()
        {
            try
            {
                glUniform3uivPtr = (glUniform3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3uiv"), typeof(glUniform3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3uiv'.");
            }
        }
        public static void glUniform3uiv(GLint @location, GLsizei @count, const GLuint * @value) => glUniform3uivPtr(@location, @count, @value);

        internal delegate void glUniform4uivFunc(GLint @location, GLsizei @count, const GLuint * @value);
        internal static glUniform4uivFunc glUniform4uivPtr;
        internal static void loadUniform4uiv()
        {
            try
            {
                glUniform4uivPtr = (glUniform4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4uiv"), typeof(glUniform4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4uiv'.");
            }
        }
        public static void glUniform4uiv(GLint @location, GLsizei @count, const GLuint * @value) => glUniform4uivPtr(@location, @count, @value);

        internal delegate void glClearBufferivFunc(GLenum @buffer, GLint @drawbuffer, const GLint * @value);
        internal static glClearBufferivFunc glClearBufferivPtr;
        internal static void loadClearBufferiv()
        {
            try
            {
                glClearBufferivPtr = (glClearBufferivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearBufferiv"), typeof(glClearBufferivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearBufferiv'.");
            }
        }
        public static void glClearBufferiv(GLenum @buffer, GLint @drawbuffer, const GLint * @value) => glClearBufferivPtr(@buffer, @drawbuffer, @value);

        internal delegate void glClearBufferuivFunc(GLenum @buffer, GLint @drawbuffer, const GLuint * @value);
        internal static glClearBufferuivFunc glClearBufferuivPtr;
        internal static void loadClearBufferuiv()
        {
            try
            {
                glClearBufferuivPtr = (glClearBufferuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearBufferuiv"), typeof(glClearBufferuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearBufferuiv'.");
            }
        }
        public static void glClearBufferuiv(GLenum @buffer, GLint @drawbuffer, const GLuint * @value) => glClearBufferuivPtr(@buffer, @drawbuffer, @value);

        internal delegate void glClearBufferfvFunc(GLenum @buffer, GLint @drawbuffer, const GLfloat * @value);
        internal static glClearBufferfvFunc glClearBufferfvPtr;
        internal static void loadClearBufferfv()
        {
            try
            {
                glClearBufferfvPtr = (glClearBufferfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearBufferfv"), typeof(glClearBufferfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearBufferfv'.");
            }
        }
        public static void glClearBufferfv(GLenum @buffer, GLint @drawbuffer, const GLfloat * @value) => glClearBufferfvPtr(@buffer, @drawbuffer, @value);

        internal delegate void glClearBufferfiFunc(GLenum @buffer, GLint @drawbuffer, GLfloat @depth, GLint @stencil);
        internal static glClearBufferfiFunc glClearBufferfiPtr;
        internal static void loadClearBufferfi()
        {
            try
            {
                glClearBufferfiPtr = (glClearBufferfiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearBufferfi"), typeof(glClearBufferfiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearBufferfi'.");
            }
        }
        public static void glClearBufferfi(GLenum @buffer, GLint @drawbuffer, GLfloat @depth, GLint @stencil) => glClearBufferfiPtr(@buffer, @drawbuffer, @depth, @stencil);

        internal delegate constGLubyte* glGetStringiFunc(GLenum @name, GLuint @index);
        internal static glGetStringiFunc glGetStringiPtr;
        internal static void loadGetStringi()
        {
            try
            {
                glGetStringiPtr = (glGetStringiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetStringi"), typeof(glGetStringiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetStringi'.");
            }
        }
        public static constGLubyte* glGetStringi(GLenum @name, GLuint @index) => glGetStringiPtr(@name, @index);

        internal delegate void glCopyBufferSubDataFunc(GLenum @readTarget, GLenum @writeTarget, GLintptr @readOffset, GLintptr @writeOffset, GLsizeiptr @size);
        internal static glCopyBufferSubDataFunc glCopyBufferSubDataPtr;
        internal static void loadCopyBufferSubData()
        {
            try
            {
                glCopyBufferSubDataPtr = (glCopyBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyBufferSubData"), typeof(glCopyBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyBufferSubData'.");
            }
        }
        public static void glCopyBufferSubData(GLenum @readTarget, GLenum @writeTarget, GLintptr @readOffset, GLintptr @writeOffset, GLsizeiptr @size) => glCopyBufferSubDataPtr(@readTarget, @writeTarget, @readOffset, @writeOffset, @size);

        internal delegate void glGetUniformIndicesFunc(GLuint @program, GLsizei @uniformCount, const GLchar *const* @uniformNames, GLuint * @uniformIndices);
        internal static glGetUniformIndicesFunc glGetUniformIndicesPtr;
        internal static void loadGetUniformIndices()
        {
            try
            {
                glGetUniformIndicesPtr = (glGetUniformIndicesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformIndices"), typeof(glGetUniformIndicesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformIndices'.");
            }
        }
        public static void glGetUniformIndices(GLuint @program, GLsizei @uniformCount, const GLchar *const* @uniformNames, GLuint * @uniformIndices) => glGetUniformIndicesPtr(@program, @uniformCount, @uniformNames, @uniformIndices);

        internal delegate void glGetActiveUniformsivFunc(GLuint @program, GLsizei @uniformCount, const GLuint * @uniformIndices, GLenum @pname, GLint * @params);
        internal static glGetActiveUniformsivFunc glGetActiveUniformsivPtr;
        internal static void loadGetActiveUniformsiv()
        {
            try
            {
                glGetActiveUniformsivPtr = (glGetActiveUniformsivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveUniformsiv"), typeof(glGetActiveUniformsivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveUniformsiv'.");
            }
        }
        public static void glGetActiveUniformsiv(GLuint @program, GLsizei @uniformCount, const GLuint * @uniformIndices, GLenum @pname, GLint * @params) => glGetActiveUniformsivPtr(@program, @uniformCount, @uniformIndices, @pname, @params);

        internal delegate GLuint glGetUniformBlockIndexFunc(GLuint @program, const GLchar * @uniformBlockName);
        internal static glGetUniformBlockIndexFunc glGetUniformBlockIndexPtr;
        internal static void loadGetUniformBlockIndex()
        {
            try
            {
                glGetUniformBlockIndexPtr = (glGetUniformBlockIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformBlockIndex"), typeof(glGetUniformBlockIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformBlockIndex'.");
            }
        }
        public static GLuint glGetUniformBlockIndex(GLuint @program, const GLchar * @uniformBlockName) => glGetUniformBlockIndexPtr(@program, @uniformBlockName);

        internal delegate void glGetActiveUniformBlockivFunc(GLuint @program, GLuint @uniformBlockIndex, GLenum @pname, GLint * @params);
        internal static glGetActiveUniformBlockivFunc glGetActiveUniformBlockivPtr;
        internal static void loadGetActiveUniformBlockiv()
        {
            try
            {
                glGetActiveUniformBlockivPtr = (glGetActiveUniformBlockivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveUniformBlockiv"), typeof(glGetActiveUniformBlockivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveUniformBlockiv'.");
            }
        }
        public static void glGetActiveUniformBlockiv(GLuint @program, GLuint @uniformBlockIndex, GLenum @pname, GLint * @params) => glGetActiveUniformBlockivPtr(@program, @uniformBlockIndex, @pname, @params);

        internal delegate void glGetActiveUniformBlockNameFunc(GLuint @program, GLuint @uniformBlockIndex, GLsizei @bufSize, GLsizei * @length, GLchar * @uniformBlockName);
        internal static glGetActiveUniformBlockNameFunc glGetActiveUniformBlockNamePtr;
        internal static void loadGetActiveUniformBlockName()
        {
            try
            {
                glGetActiveUniformBlockNamePtr = (glGetActiveUniformBlockNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveUniformBlockName"), typeof(glGetActiveUniformBlockNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveUniformBlockName'.");
            }
        }
        public static void glGetActiveUniformBlockName(GLuint @program, GLuint @uniformBlockIndex, GLsizei @bufSize, GLsizei * @length, GLchar * @uniformBlockName) => glGetActiveUniformBlockNamePtr(@program, @uniformBlockIndex, @bufSize, @length, @uniformBlockName);

        internal delegate void glUniformBlockBindingFunc(GLuint @program, GLuint @uniformBlockIndex, GLuint @uniformBlockBinding);
        internal static glUniformBlockBindingFunc glUniformBlockBindingPtr;
        internal static void loadUniformBlockBinding()
        {
            try
            {
                glUniformBlockBindingPtr = (glUniformBlockBindingFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformBlockBinding"), typeof(glUniformBlockBindingFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformBlockBinding'.");
            }
        }
        public static void glUniformBlockBinding(GLuint @program, GLuint @uniformBlockIndex, GLuint @uniformBlockBinding) => glUniformBlockBindingPtr(@program, @uniformBlockIndex, @uniformBlockBinding);

        internal delegate void glDrawArraysInstancedFunc(GLenum @mode, GLint @first, GLsizei @count, GLsizei @instancecount);
        internal static glDrawArraysInstancedFunc glDrawArraysInstancedPtr;
        internal static void loadDrawArraysInstanced()
        {
            try
            {
                glDrawArraysInstancedPtr = (glDrawArraysInstancedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysInstanced"), typeof(glDrawArraysInstancedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysInstanced'.");
            }
        }
        public static void glDrawArraysInstanced(GLenum @mode, GLint @first, GLsizei @count, GLsizei @instancecount) => glDrawArraysInstancedPtr(@mode, @first, @count, @instancecount);

        internal delegate void glDrawElementsInstancedFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount);
        internal static glDrawElementsInstancedFunc glDrawElementsInstancedPtr;
        internal static void loadDrawElementsInstanced()
        {
            try
            {
                glDrawElementsInstancedPtr = (glDrawElementsInstancedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstanced"), typeof(glDrawElementsInstancedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstanced'.");
            }
        }
        public static void glDrawElementsInstanced(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount) => glDrawElementsInstancedPtr(@mode, @count, @type, @indices, @instancecount);

        internal delegate GLsync glFenceSyncFunc(GLenum @condition, GLbitfield @flags);
        internal static glFenceSyncFunc glFenceSyncPtr;
        internal static void loadFenceSync()
        {
            try
            {
                glFenceSyncPtr = (glFenceSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFenceSync"), typeof(glFenceSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFenceSync'.");
            }
        }
        public static GLsync glFenceSync(GLenum @condition, GLbitfield @flags) => glFenceSyncPtr(@condition, @flags);

        internal delegate GLboolean glIsSyncFunc(GLsync @sync);
        internal static glIsSyncFunc glIsSyncPtr;
        internal static void loadIsSync()
        {
            try
            {
                glIsSyncPtr = (glIsSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsSync"), typeof(glIsSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsSync'.");
            }
        }
        public static GLboolean glIsSync(GLsync @sync) => glIsSyncPtr(@sync);

        internal delegate void glDeleteSyncFunc(GLsync @sync);
        internal static glDeleteSyncFunc glDeleteSyncPtr;
        internal static void loadDeleteSync()
        {
            try
            {
                glDeleteSyncPtr = (glDeleteSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteSync"), typeof(glDeleteSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteSync'.");
            }
        }
        public static void glDeleteSync(GLsync @sync) => glDeleteSyncPtr(@sync);

        internal delegate GLenum glClientWaitSyncFunc(GLsync @sync, GLbitfield @flags, GLuint64 @timeout);
        internal static glClientWaitSyncFunc glClientWaitSyncPtr;
        internal static void loadClientWaitSync()
        {
            try
            {
                glClientWaitSyncPtr = (glClientWaitSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClientWaitSync"), typeof(glClientWaitSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClientWaitSync'.");
            }
        }
        public static GLenum glClientWaitSync(GLsync @sync, GLbitfield @flags, GLuint64 @timeout) => glClientWaitSyncPtr(@sync, @flags, @timeout);

        internal delegate void glWaitSyncFunc(GLsync @sync, GLbitfield @flags, GLuint64 @timeout);
        internal static glWaitSyncFunc glWaitSyncPtr;
        internal static void loadWaitSync()
        {
            try
            {
                glWaitSyncPtr = (glWaitSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWaitSync"), typeof(glWaitSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWaitSync'.");
            }
        }
        public static void glWaitSync(GLsync @sync, GLbitfield @flags, GLuint64 @timeout) => glWaitSyncPtr(@sync, @flags, @timeout);

        internal delegate void glGetInteger64vFunc(GLenum @pname, GLint64 * @data);
        internal static glGetInteger64vFunc glGetInteger64vPtr;
        internal static void loadGetInteger64v()
        {
            try
            {
                glGetInteger64vPtr = (glGetInteger64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInteger64v"), typeof(glGetInteger64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInteger64v'.");
            }
        }
        public static void glGetInteger64v(GLenum @pname, GLint64 * @data) => glGetInteger64vPtr(@pname, @data);

        internal delegate void glGetSyncivFunc(GLsync @sync, GLenum @pname, GLsizei @bufSize, GLsizei * @length, GLint * @values);
        internal static glGetSyncivFunc glGetSyncivPtr;
        internal static void loadGetSynciv()
        {
            try
            {
                glGetSyncivPtr = (glGetSyncivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSynciv"), typeof(glGetSyncivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSynciv'.");
            }
        }
        public static void glGetSynciv(GLsync @sync, GLenum @pname, GLsizei @bufSize, GLsizei * @length, GLint * @values) => glGetSyncivPtr(@sync, @pname, @bufSize, @length, @values);

        internal delegate void glGetInteger64i_vFunc(GLenum @target, GLuint @index, GLint64 * @data);
        internal static glGetInteger64i_vFunc glGetInteger64i_vPtr;
        internal static void loadGetInteger64i_v()
        {
            try
            {
                glGetInteger64i_vPtr = (glGetInteger64i_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInteger64i_v"), typeof(glGetInteger64i_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInteger64i_v'.");
            }
        }
        public static void glGetInteger64i_v(GLenum @target, GLuint @index, GLint64 * @data) => glGetInteger64i_vPtr(@target, @index, @data);

        internal delegate void glGetBufferParameteri64vFunc(GLenum @target, GLenum @pname, GLint64 * @params);
        internal static glGetBufferParameteri64vFunc glGetBufferParameteri64vPtr;
        internal static void loadGetBufferParameteri64v()
        {
            try
            {
                glGetBufferParameteri64vPtr = (glGetBufferParameteri64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferParameteri64v"), typeof(glGetBufferParameteri64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferParameteri64v'.");
            }
        }
        public static void glGetBufferParameteri64v(GLenum @target, GLenum @pname, GLint64 * @params) => glGetBufferParameteri64vPtr(@target, @pname, @params);

        internal delegate void glGenSamplersFunc(GLsizei @count, GLuint * @samplers);
        internal static glGenSamplersFunc glGenSamplersPtr;
        internal static void loadGenSamplers()
        {
            try
            {
                glGenSamplersPtr = (glGenSamplersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenSamplers"), typeof(glGenSamplersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenSamplers'.");
            }
        }
        public static void glGenSamplers(GLsizei @count, GLuint * @samplers) => glGenSamplersPtr(@count, @samplers);

        internal delegate void glDeleteSamplersFunc(GLsizei @count, const GLuint * @samplers);
        internal static glDeleteSamplersFunc glDeleteSamplersPtr;
        internal static void loadDeleteSamplers()
        {
            try
            {
                glDeleteSamplersPtr = (glDeleteSamplersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteSamplers"), typeof(glDeleteSamplersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteSamplers'.");
            }
        }
        public static void glDeleteSamplers(GLsizei @count, const GLuint * @samplers) => glDeleteSamplersPtr(@count, @samplers);

        internal delegate GLboolean glIsSamplerFunc(GLuint @sampler);
        internal static glIsSamplerFunc glIsSamplerPtr;
        internal static void loadIsSampler()
        {
            try
            {
                glIsSamplerPtr = (glIsSamplerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsSampler"), typeof(glIsSamplerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsSampler'.");
            }
        }
        public static GLboolean glIsSampler(GLuint @sampler) => glIsSamplerPtr(@sampler);

        internal delegate void glBindSamplerFunc(GLuint @unit, GLuint @sampler);
        internal static glBindSamplerFunc glBindSamplerPtr;
        internal static void loadBindSampler()
        {
            try
            {
                glBindSamplerPtr = (glBindSamplerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindSampler"), typeof(glBindSamplerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindSampler'.");
            }
        }
        public static void glBindSampler(GLuint @unit, GLuint @sampler) => glBindSamplerPtr(@unit, @sampler);

        internal delegate void glSamplerParameteriFunc(GLuint @sampler, GLenum @pname, GLint @param);
        internal static glSamplerParameteriFunc glSamplerParameteriPtr;
        internal static void loadSamplerParameteri()
        {
            try
            {
                glSamplerParameteriPtr = (glSamplerParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameteri"), typeof(glSamplerParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameteri'.");
            }
        }
        public static void glSamplerParameteri(GLuint @sampler, GLenum @pname, GLint @param) => glSamplerParameteriPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterivFunc(GLuint @sampler, GLenum @pname, const GLint * @param);
        internal static glSamplerParameterivFunc glSamplerParameterivPtr;
        internal static void loadSamplerParameteriv()
        {
            try
            {
                glSamplerParameterivPtr = (glSamplerParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameteriv"), typeof(glSamplerParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameteriv'.");
            }
        }
        public static void glSamplerParameteriv(GLuint @sampler, GLenum @pname, const GLint * @param) => glSamplerParameterivPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterfFunc(GLuint @sampler, GLenum @pname, GLfloat @param);
        internal static glSamplerParameterfFunc glSamplerParameterfPtr;
        internal static void loadSamplerParameterf()
        {
            try
            {
                glSamplerParameterfPtr = (glSamplerParameterfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterf"), typeof(glSamplerParameterfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterf'.");
            }
        }
        public static void glSamplerParameterf(GLuint @sampler, GLenum @pname, GLfloat @param) => glSamplerParameterfPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterfvFunc(GLuint @sampler, GLenum @pname, const GLfloat * @param);
        internal static glSamplerParameterfvFunc glSamplerParameterfvPtr;
        internal static void loadSamplerParameterfv()
        {
            try
            {
                glSamplerParameterfvPtr = (glSamplerParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterfv"), typeof(glSamplerParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterfv'.");
            }
        }
        public static void glSamplerParameterfv(GLuint @sampler, GLenum @pname, const GLfloat * @param) => glSamplerParameterfvPtr(@sampler, @pname, @param);

        internal delegate void glGetSamplerParameterivFunc(GLuint @sampler, GLenum @pname, GLint * @params);
        internal static glGetSamplerParameterivFunc glGetSamplerParameterivPtr;
        internal static void loadGetSamplerParameteriv()
        {
            try
            {
                glGetSamplerParameterivPtr = (glGetSamplerParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameteriv"), typeof(glGetSamplerParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameteriv'.");
            }
        }
        public static void glGetSamplerParameteriv(GLuint @sampler, GLenum @pname, GLint * @params) => glGetSamplerParameterivPtr(@sampler, @pname, @params);

        internal delegate void glGetSamplerParameterfvFunc(GLuint @sampler, GLenum @pname, GLfloat * @params);
        internal static glGetSamplerParameterfvFunc glGetSamplerParameterfvPtr;
        internal static void loadGetSamplerParameterfv()
        {
            try
            {
                glGetSamplerParameterfvPtr = (glGetSamplerParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterfv"), typeof(glGetSamplerParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterfv'.");
            }
        }
        public static void glGetSamplerParameterfv(GLuint @sampler, GLenum @pname, GLfloat * @params) => glGetSamplerParameterfvPtr(@sampler, @pname, @params);

        internal delegate void glVertexAttribDivisorFunc(GLuint @index, GLuint @divisor);
        internal static glVertexAttribDivisorFunc glVertexAttribDivisorPtr;
        internal static void loadVertexAttribDivisor()
        {
            try
            {
                glVertexAttribDivisorPtr = (glVertexAttribDivisorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribDivisor"), typeof(glVertexAttribDivisorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribDivisor'.");
            }
        }
        public static void glVertexAttribDivisor(GLuint @index, GLuint @divisor) => glVertexAttribDivisorPtr(@index, @divisor);

        internal delegate void glBindTransformFeedbackFunc(GLenum @target, GLuint @id);
        internal static glBindTransformFeedbackFunc glBindTransformFeedbackPtr;
        internal static void loadBindTransformFeedback()
        {
            try
            {
                glBindTransformFeedbackPtr = (glBindTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTransformFeedback"), typeof(glBindTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTransformFeedback'.");
            }
        }
        public static void glBindTransformFeedback(GLenum @target, GLuint @id) => glBindTransformFeedbackPtr(@target, @id);

        internal delegate void glDeleteTransformFeedbacksFunc(GLsizei @n, const GLuint * @ids);
        internal static glDeleteTransformFeedbacksFunc glDeleteTransformFeedbacksPtr;
        internal static void loadDeleteTransformFeedbacks()
        {
            try
            {
                glDeleteTransformFeedbacksPtr = (glDeleteTransformFeedbacksFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteTransformFeedbacks"), typeof(glDeleteTransformFeedbacksFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteTransformFeedbacks'.");
            }
        }
        public static void glDeleteTransformFeedbacks(GLsizei @n, const GLuint * @ids) => glDeleteTransformFeedbacksPtr(@n, @ids);

        internal delegate void glGenTransformFeedbacksFunc(GLsizei @n, GLuint * @ids);
        internal static glGenTransformFeedbacksFunc glGenTransformFeedbacksPtr;
        internal static void loadGenTransformFeedbacks()
        {
            try
            {
                glGenTransformFeedbacksPtr = (glGenTransformFeedbacksFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenTransformFeedbacks"), typeof(glGenTransformFeedbacksFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenTransformFeedbacks'.");
            }
        }
        public static void glGenTransformFeedbacks(GLsizei @n, GLuint * @ids) => glGenTransformFeedbacksPtr(@n, @ids);

        internal delegate GLboolean glIsTransformFeedbackFunc(GLuint @id);
        internal static glIsTransformFeedbackFunc glIsTransformFeedbackPtr;
        internal static void loadIsTransformFeedback()
        {
            try
            {
                glIsTransformFeedbackPtr = (glIsTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsTransformFeedback"), typeof(glIsTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsTransformFeedback'.");
            }
        }
        public static GLboolean glIsTransformFeedback(GLuint @id) => glIsTransformFeedbackPtr(@id);

        internal delegate void glPauseTransformFeedbackFunc();
        internal static glPauseTransformFeedbackFunc glPauseTransformFeedbackPtr;
        internal static void loadPauseTransformFeedback()
        {
            try
            {
                glPauseTransformFeedbackPtr = (glPauseTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPauseTransformFeedback"), typeof(glPauseTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPauseTransformFeedback'.");
            }
        }
        public static void glPauseTransformFeedback() => glPauseTransformFeedbackPtr();

        internal delegate void glResumeTransformFeedbackFunc();
        internal static glResumeTransformFeedbackFunc glResumeTransformFeedbackPtr;
        internal static void loadResumeTransformFeedback()
        {
            try
            {
                glResumeTransformFeedbackPtr = (glResumeTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glResumeTransformFeedback"), typeof(glResumeTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glResumeTransformFeedback'.");
            }
        }
        public static void glResumeTransformFeedback() => glResumeTransformFeedbackPtr();

        internal delegate void glGetProgramBinaryFunc(GLuint @program, GLsizei @bufSize, GLsizei * @length, GLenum * @binaryFormat, void * @binary);
        internal static glGetProgramBinaryFunc glGetProgramBinaryPtr;
        internal static void loadGetProgramBinary()
        {
            try
            {
                glGetProgramBinaryPtr = (glGetProgramBinaryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramBinary"), typeof(glGetProgramBinaryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramBinary'.");
            }
        }
        public static void glGetProgramBinary(GLuint @program, GLsizei @bufSize, GLsizei * @length, GLenum * @binaryFormat, void * @binary) => glGetProgramBinaryPtr(@program, @bufSize, @length, @binaryFormat, @binary);

        internal delegate void glProgramBinaryFunc(GLuint @program, GLenum @binaryFormat, const void * @binary, GLsizei @length);
        internal static glProgramBinaryFunc glProgramBinaryPtr;
        internal static void loadProgramBinary()
        {
            try
            {
                glProgramBinaryPtr = (glProgramBinaryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramBinary"), typeof(glProgramBinaryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramBinary'.");
            }
        }
        public static void glProgramBinary(GLuint @program, GLenum @binaryFormat, const void * @binary, GLsizei @length) => glProgramBinaryPtr(@program, @binaryFormat, @binary, @length);

        internal delegate void glProgramParameteriFunc(GLuint @program, GLenum @pname, GLint @value);
        internal static glProgramParameteriFunc glProgramParameteriPtr;
        internal static void loadProgramParameteri()
        {
            try
            {
                glProgramParameteriPtr = (glProgramParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramParameteri"), typeof(glProgramParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramParameteri'.");
            }
        }
        public static void glProgramParameteri(GLuint @program, GLenum @pname, GLint @value) => glProgramParameteriPtr(@program, @pname, @value);

        internal delegate void glInvalidateFramebufferFunc(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments);
        internal static glInvalidateFramebufferFunc glInvalidateFramebufferPtr;
        internal static void loadInvalidateFramebuffer()
        {
            try
            {
                glInvalidateFramebufferPtr = (glInvalidateFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateFramebuffer"), typeof(glInvalidateFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateFramebuffer'.");
            }
        }
        public static void glInvalidateFramebuffer(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments) => glInvalidateFramebufferPtr(@target, @numAttachments, @attachments);

        internal delegate void glInvalidateSubFramebufferFunc(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glInvalidateSubFramebufferFunc glInvalidateSubFramebufferPtr;
        internal static void loadInvalidateSubFramebuffer()
        {
            try
            {
                glInvalidateSubFramebufferPtr = (glInvalidateSubFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateSubFramebuffer"), typeof(glInvalidateSubFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateSubFramebuffer'.");
            }
        }
        public static void glInvalidateSubFramebuffer(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glInvalidateSubFramebufferPtr(@target, @numAttachments, @attachments, @x, @y, @width, @height);

        internal delegate void glTexStorage2DFunc(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glTexStorage2DFunc glTexStorage2DPtr;
        internal static void loadTexStorage2D()
        {
            try
            {
                glTexStorage2DPtr = (glTexStorage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage2D"), typeof(glTexStorage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage2D'.");
            }
        }
        public static void glTexStorage2D(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height) => glTexStorage2DPtr(@target, @levels, @internalformat, @width, @height);

        internal delegate void glTexStorage3DFunc(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth);
        internal static glTexStorage3DFunc glTexStorage3DPtr;
        internal static void loadTexStorage3D()
        {
            try
            {
                glTexStorage3DPtr = (glTexStorage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage3D"), typeof(glTexStorage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage3D'.");
            }
        }
        public static void glTexStorage3D(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth) => glTexStorage3DPtr(@target, @levels, @internalformat, @width, @height, @depth);

        internal delegate void glGetInternalformativFunc(GLenum @target, GLenum @internalformat, GLenum @pname, GLsizei @bufSize, GLint * @params);
        internal static glGetInternalformativFunc glGetInternalformativPtr;
        internal static void loadGetInternalformativ()
        {
            try
            {
                glGetInternalformativPtr = (glGetInternalformativFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInternalformativ"), typeof(glGetInternalformativFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInternalformativ'.");
            }
        }
        public static void glGetInternalformativ(GLenum @target, GLenum @internalformat, GLenum @pname, GLsizei @bufSize, GLint * @params) => glGetInternalformativPtr(@target, @internalformat, @pname, @bufSize, @params);
        #endregion
    }
}
