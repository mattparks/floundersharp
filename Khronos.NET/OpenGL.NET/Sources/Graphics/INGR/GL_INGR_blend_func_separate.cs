using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_INGR_blend_func_separate
    {
        #region Interop
        static GL_INGR_blend_func_separate()
        {
            Console.WriteLine("Initalising GL_INGR_blend_func_separate interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendFuncSeparateINGR();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glBlendFuncSeparateINGRFunc(GLenum @sfactorRGB, GLenum @dfactorRGB, GLenum @sfactorAlpha, GLenum @dfactorAlpha);
        internal static glBlendFuncSeparateINGRFunc glBlendFuncSeparateINGRPtr;
        internal static void loadBlendFuncSeparateINGR()
        {
            try
            {
                glBlendFuncSeparateINGRPtr = (glBlendFuncSeparateINGRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFuncSeparateINGR"), typeof(glBlendFuncSeparateINGRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFuncSeparateINGR'.");
            }
        }
        public static void glBlendFuncSeparateINGR(GLenum @sfactorRGB, GLenum @dfactorRGB, GLenum @sfactorAlpha, GLenum @dfactorAlpha) => glBlendFuncSeparateINGRPtr(@sfactorRGB, @dfactorRGB, @sfactorAlpha, @dfactorAlpha);
        #endregion
    }
}
