using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_INGR_color_clamp
    {
        #region Interop
        static GL_INGR_color_clamp()
        {
            Console.WriteLine("Initalising GL_INGR_color_clamp interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RED_MIN_CLAMP_INGR = 0x8560;
        public static UInt32 GL_GREEN_MIN_CLAMP_INGR = 0x8561;
        public static UInt32 GL_BLUE_MIN_CLAMP_INGR = 0x8562;
        public static UInt32 GL_ALPHA_MIN_CLAMP_INGR = 0x8563;
        public static UInt32 GL_RED_MAX_CLAMP_INGR = 0x8564;
        public static UInt32 GL_GREEN_MAX_CLAMP_INGR = 0x8565;
        public static UInt32 GL_BLUE_MAX_CLAMP_INGR = 0x8566;
        public static UInt32 GL_ALPHA_MAX_CLAMP_INGR = 0x8567;
        #endregion

        #region Commands
        #endregion
    }
}
