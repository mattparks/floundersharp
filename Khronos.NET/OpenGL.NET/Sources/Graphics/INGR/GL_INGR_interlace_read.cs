using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_INGR_interlace_read
    {
        #region Interop
        static GL_INGR_interlace_read()
        {
            Console.WriteLine("Initalising GL_INGR_interlace_read interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_INTERLACE_READ_INGR = 0x8568;
        #endregion

        #region Commands
        #endregion
    }
}
