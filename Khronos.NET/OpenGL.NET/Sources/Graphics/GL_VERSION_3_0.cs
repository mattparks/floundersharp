using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL30
    {
        #region Interop
        static GL30()
        {
            Console.WriteLine("Initalising GL30 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadColorMaski();
            loadGetBooleani_v();
            loadGetIntegeri_v();
            loadEnablei();
            loadDisablei();
            loadIsEnabledi();
            loadBeginTransformFeedback();
            loadEndTransformFeedback();
            loadBindBufferRange();
            loadBindBufferBase();
            loadTransformFeedbackVaryings();
            loadGetTransformFeedbackVarying();
            loadClampColor();
            loadBeginConditionalRender();
            loadEndConditionalRender();
            loadVertexAttribIPointer();
            loadGetVertexAttribIiv();
            loadGetVertexAttribIuiv();
            loadVertexAttribI1i();
            loadVertexAttribI2i();
            loadVertexAttribI3i();
            loadVertexAttribI4i();
            loadVertexAttribI1ui();
            loadVertexAttribI2ui();
            loadVertexAttribI3ui();
            loadVertexAttribI4ui();
            loadVertexAttribI1iv();
            loadVertexAttribI2iv();
            loadVertexAttribI3iv();
            loadVertexAttribI4iv();
            loadVertexAttribI1uiv();
            loadVertexAttribI2uiv();
            loadVertexAttribI3uiv();
            loadVertexAttribI4uiv();
            loadVertexAttribI4bv();
            loadVertexAttribI4sv();
            loadVertexAttribI4ubv();
            loadVertexAttribI4usv();
            loadGetUniformuiv();
            loadBindFragDataLocation();
            loadGetFragDataLocation();
            loadUniform1ui();
            loadUniform2ui();
            loadUniform3ui();
            loadUniform4ui();
            loadUniform1uiv();
            loadUniform2uiv();
            loadUniform3uiv();
            loadUniform4uiv();
            loadTexParameterIiv();
            loadTexParameterIuiv();
            loadGetTexParameterIiv();
            loadGetTexParameterIuiv();
            loadClearBufferiv();
            loadClearBufferuiv();
            loadClearBufferfv();
            loadClearBufferfi();
            loadGetStringi();
            loadIsRenderbuffer();
            loadBindRenderbuffer();
            loadDeleteRenderbuffers();
            loadGenRenderbuffers();
            loadRenderbufferStorage();
            loadGetRenderbufferParameteriv();
            loadIsFramebuffer();
            loadBindFramebuffer();
            loadDeleteFramebuffers();
            loadGenFramebuffers();
            loadCheckFramebufferStatus();
            loadFramebufferTexture1D();
            loadFramebufferTexture2D();
            loadFramebufferTexture3D();
            loadFramebufferRenderbuffer();
            loadGetFramebufferAttachmentParameteriv();
            loadGenerateMipmap();
            loadBlitFramebuffer();
            loadRenderbufferStorageMultisample();
            loadFramebufferTextureLayer();
            loadMapBufferRange();
            loadFlushMappedBufferRange();
            loadBindVertexArray();
            loadDeleteVertexArrays();
            loadGenVertexArrays();
            loadIsVertexArray();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPARE_REF_TO_TEXTURE = 0x884E;
        public static UInt32 GL_CLIP_DISTANCE0 = 0x3000;
        public static UInt32 GL_CLIP_DISTANCE1 = 0x3001;
        public static UInt32 GL_CLIP_DISTANCE2 = 0x3002;
        public static UInt32 GL_CLIP_DISTANCE3 = 0x3003;
        public static UInt32 GL_CLIP_DISTANCE4 = 0x3004;
        public static UInt32 GL_CLIP_DISTANCE5 = 0x3005;
        public static UInt32 GL_CLIP_DISTANCE6 = 0x3006;
        public static UInt32 GL_CLIP_DISTANCE7 = 0x3007;
        public static UInt32 GL_MAX_CLIP_DISTANCES = 0x0D32;
        public static UInt32 GL_MAJOR_VERSION = 0x821B;
        public static UInt32 GL_MINOR_VERSION = 0x821C;
        public static UInt32 GL_NUM_EXTENSIONS = 0x821D;
        public static UInt32 GL_CONTEXT_FLAGS = 0x821E;
        public static UInt32 GL_COMPRESSED_RED = 0x8225;
        public static UInt32 GL_COMPRESSED_RG = 0x8226;
        public static UInt32 GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT = 0x00000001;
        public static UInt32 GL_RGBA32F = 0x8814;
        public static UInt32 GL_RGB32F = 0x8815;
        public static UInt32 GL_RGBA16F = 0x881A;
        public static UInt32 GL_RGB16F = 0x881B;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_INTEGER = 0x88FD;
        public static UInt32 GL_MAX_ARRAY_TEXTURE_LAYERS = 0x88FF;
        public static UInt32 GL_MIN_PROGRAM_TEXEL_OFFSET = 0x8904;
        public static UInt32 GL_MAX_PROGRAM_TEXEL_OFFSET = 0x8905;
        public static UInt32 GL_CLAMP_READ_COLOR = 0x891C;
        public static UInt32 GL_FIXED_ONLY = 0x891D;
        public static UInt32 GL_MAX_VARYING_COMPONENTS = 0x8B4B;
        public static UInt32 GL_TEXTURE_1D_ARRAY = 0x8C18;
        public static UInt32 GL_PROXY_TEXTURE_1D_ARRAY = 0x8C19;
        public static UInt32 GL_TEXTURE_2D_ARRAY = 0x8C1A;
        public static UInt32 GL_PROXY_TEXTURE_2D_ARRAY = 0x8C1B;
        public static UInt32 GL_TEXTURE_BINDING_1D_ARRAY = 0x8C1C;
        public static UInt32 GL_TEXTURE_BINDING_2D_ARRAY = 0x8C1D;
        public static UInt32 GL_R11F_G11F_B10F = 0x8C3A;
        public static UInt32 GL_UNSIGNED_INT_10F_11F_11F_REV = 0x8C3B;
        public static UInt32 GL_RGB9_E5 = 0x8C3D;
        public static UInt32 GL_UNSIGNED_INT_5_9_9_9_REV = 0x8C3E;
        public static UInt32 GL_TEXTURE_SHARED_SIZE = 0x8C3F;
        public static UInt32 GL_TRANSFORM_FEEDBACK_VARYING_MAX_LENGTH = 0x8C76;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_MODE = 0x8C7F;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS = 0x8C80;
        public static UInt32 GL_TRANSFORM_FEEDBACK_VARYINGS = 0x8C83;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_START = 0x8C84;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_SIZE = 0x8C85;
        public static UInt32 GL_PRIMITIVES_GENERATED = 0x8C87;
        public static UInt32 GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN = 0x8C88;
        public static UInt32 GL_RASTERIZER_DISCARD = 0x8C89;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS = 0x8C8A;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS = 0x8C8B;
        public static UInt32 GL_INTERLEAVED_ATTRIBS = 0x8C8C;
        public static UInt32 GL_SEPARATE_ATTRIBS = 0x8C8D;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER = 0x8C8E;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_BINDING = 0x8C8F;
        public static UInt32 GL_RGBA32UI = 0x8D70;
        public static UInt32 GL_RGB32UI = 0x8D71;
        public static UInt32 GL_RGBA16UI = 0x8D76;
        public static UInt32 GL_RGB16UI = 0x8D77;
        public static UInt32 GL_RGBA8UI = 0x8D7C;
        public static UInt32 GL_RGB8UI = 0x8D7D;
        public static UInt32 GL_RGBA32I = 0x8D82;
        public static UInt32 GL_RGB32I = 0x8D83;
        public static UInt32 GL_RGBA16I = 0x8D88;
        public static UInt32 GL_RGB16I = 0x8D89;
        public static UInt32 GL_RGBA8I = 0x8D8E;
        public static UInt32 GL_RGB8I = 0x8D8F;
        public static UInt32 GL_RED_INTEGER = 0x8D94;
        public static UInt32 GL_GREEN_INTEGER = 0x8D95;
        public static UInt32 GL_BLUE_INTEGER = 0x8D96;
        public static UInt32 GL_RGB_INTEGER = 0x8D98;
        public static UInt32 GL_RGBA_INTEGER = 0x8D99;
        public static UInt32 GL_BGR_INTEGER = 0x8D9A;
        public static UInt32 GL_BGRA_INTEGER = 0x8D9B;
        public static UInt32 GL_SAMPLER_1D_ARRAY = 0x8DC0;
        public static UInt32 GL_SAMPLER_2D_ARRAY = 0x8DC1;
        public static UInt32 GL_SAMPLER_1D_ARRAY_SHADOW = 0x8DC3;
        public static UInt32 GL_SAMPLER_2D_ARRAY_SHADOW = 0x8DC4;
        public static UInt32 GL_SAMPLER_CUBE_SHADOW = 0x8DC5;
        public static UInt32 GL_UNSIGNED_INT_VEC2 = 0x8DC6;
        public static UInt32 GL_UNSIGNED_INT_VEC3 = 0x8DC7;
        public static UInt32 GL_UNSIGNED_INT_VEC4 = 0x8DC8;
        public static UInt32 GL_INT_SAMPLER_1D = 0x8DC9;
        public static UInt32 GL_INT_SAMPLER_2D = 0x8DCA;
        public static UInt32 GL_INT_SAMPLER_3D = 0x8DCB;
        public static UInt32 GL_INT_SAMPLER_CUBE = 0x8DCC;
        public static UInt32 GL_INT_SAMPLER_1D_ARRAY = 0x8DCE;
        public static UInt32 GL_INT_SAMPLER_2D_ARRAY = 0x8DCF;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_1D = 0x8DD1;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D = 0x8DD2;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_3D = 0x8DD3;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_CUBE = 0x8DD4;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_1D_ARRAY = 0x8DD6;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_ARRAY = 0x8DD7;
        public static UInt32 GL_QUERY_WAIT = 0x8E13;
        public static UInt32 GL_QUERY_NO_WAIT = 0x8E14;
        public static UInt32 GL_QUERY_BY_REGION_WAIT = 0x8E15;
        public static UInt32 GL_QUERY_BY_REGION_NO_WAIT = 0x8E16;
        public static UInt32 GL_BUFFER_ACCESS_FLAGS = 0x911F;
        public static UInt32 GL_BUFFER_MAP_LENGTH = 0x9120;
        public static UInt32 GL_BUFFER_MAP_OFFSET = 0x9121;
        public static UInt32 GL_DEPTH_COMPONENT32F = 0x8CAC;
        public static UInt32 GL_DEPTH32F_STENCIL8 = 0x8CAD;
        public static UInt32 GL_FLOAT_32_UNSIGNED_INT_24_8_REV = 0x8DAD;
        public static UInt32 GL_INVALID_FRAMEBUFFER_OPERATION = 0x0506;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING = 0x8210;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE = 0x8211;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_RED_SIZE = 0x8212;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_GREEN_SIZE = 0x8213;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_BLUE_SIZE = 0x8214;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE = 0x8215;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE = 0x8216;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE = 0x8217;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT = 0x8218;
        public static UInt32 GL_FRAMEBUFFER_UNDEFINED = 0x8219;
        public static UInt32 GL_DEPTH_STENCIL_ATTACHMENT = 0x821A;
        public static UInt32 GL_MAX_RENDERBUFFER_SIZE = 0x84E8;
        public static UInt32 GL_DEPTH_STENCIL = 0x84F9;
        public static UInt32 GL_UNSIGNED_INT_24_8 = 0x84FA;
        public static UInt32 GL_DEPTH24_STENCIL8 = 0x88F0;
        public static UInt32 GL_TEXTURE_STENCIL_SIZE = 0x88F1;
        public static UInt32 GL_TEXTURE_RED_TYPE = 0x8C10;
        public static UInt32 GL_TEXTURE_GREEN_TYPE = 0x8C11;
        public static UInt32 GL_TEXTURE_BLUE_TYPE = 0x8C12;
        public static UInt32 GL_TEXTURE_ALPHA_TYPE = 0x8C13;
        public static UInt32 GL_TEXTURE_DEPTH_TYPE = 0x8C16;
        public static UInt32 GL_UNSIGNED_NORMALIZED = 0x8C17;
        public static UInt32 GL_FRAMEBUFFER_BINDING = 0x8CA6;
        public static UInt32 GL_DRAW_FRAMEBUFFER_BINDING = 0x8CA6;
        public static UInt32 GL_RENDERBUFFER_BINDING = 0x8CA7;
        public static UInt32 GL_READ_FRAMEBUFFER = 0x8CA8;
        public static UInt32 GL_DRAW_FRAMEBUFFER = 0x8CA9;
        public static UInt32 GL_READ_FRAMEBUFFER_BINDING = 0x8CAA;
        public static UInt32 GL_RENDERBUFFER_SAMPLES = 0x8CAB;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE = 0x8CD0;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME = 0x8CD1;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL = 0x8CD2;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE = 0x8CD3;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER = 0x8CD4;
        public static UInt32 GL_FRAMEBUFFER_COMPLETE = 0x8CD5;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT = 0x8CD6;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT = 0x8CD7;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER = 0x8CDB;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER = 0x8CDC;
        public static UInt32 GL_FRAMEBUFFER_UNSUPPORTED = 0x8CDD;
        public static UInt32 GL_MAX_COLOR_ATTACHMENTS = 0x8CDF;
        public static UInt32 GL_COLOR_ATTACHMENT0 = 0x8CE0;
        public static UInt32 GL_COLOR_ATTACHMENT1 = 0x8CE1;
        public static UInt32 GL_COLOR_ATTACHMENT2 = 0x8CE2;
        public static UInt32 GL_COLOR_ATTACHMENT3 = 0x8CE3;
        public static UInt32 GL_COLOR_ATTACHMENT4 = 0x8CE4;
        public static UInt32 GL_COLOR_ATTACHMENT5 = 0x8CE5;
        public static UInt32 GL_COLOR_ATTACHMENT6 = 0x8CE6;
        public static UInt32 GL_COLOR_ATTACHMENT7 = 0x8CE7;
        public static UInt32 GL_COLOR_ATTACHMENT8 = 0x8CE8;
        public static UInt32 GL_COLOR_ATTACHMENT9 = 0x8CE9;
        public static UInt32 GL_COLOR_ATTACHMENT10 = 0x8CEA;
        public static UInt32 GL_COLOR_ATTACHMENT11 = 0x8CEB;
        public static UInt32 GL_COLOR_ATTACHMENT12 = 0x8CEC;
        public static UInt32 GL_COLOR_ATTACHMENT13 = 0x8CED;
        public static UInt32 GL_COLOR_ATTACHMENT14 = 0x8CEE;
        public static UInt32 GL_COLOR_ATTACHMENT15 = 0x8CEF;
        public static UInt32 GL_COLOR_ATTACHMENT16 = 0x8CF0;
        public static UInt32 GL_COLOR_ATTACHMENT17 = 0x8CF1;
        public static UInt32 GL_COLOR_ATTACHMENT18 = 0x8CF2;
        public static UInt32 GL_COLOR_ATTACHMENT19 = 0x8CF3;
        public static UInt32 GL_COLOR_ATTACHMENT20 = 0x8CF4;
        public static UInt32 GL_COLOR_ATTACHMENT21 = 0x8CF5;
        public static UInt32 GL_COLOR_ATTACHMENT22 = 0x8CF6;
        public static UInt32 GL_COLOR_ATTACHMENT23 = 0x8CF7;
        public static UInt32 GL_COLOR_ATTACHMENT24 = 0x8CF8;
        public static UInt32 GL_COLOR_ATTACHMENT25 = 0x8CF9;
        public static UInt32 GL_COLOR_ATTACHMENT26 = 0x8CFA;
        public static UInt32 GL_COLOR_ATTACHMENT27 = 0x8CFB;
        public static UInt32 GL_COLOR_ATTACHMENT28 = 0x8CFC;
        public static UInt32 GL_COLOR_ATTACHMENT29 = 0x8CFD;
        public static UInt32 GL_COLOR_ATTACHMENT30 = 0x8CFE;
        public static UInt32 GL_COLOR_ATTACHMENT31 = 0x8CFF;
        public static UInt32 GL_DEPTH_ATTACHMENT = 0x8D00;
        public static UInt32 GL_STENCIL_ATTACHMENT = 0x8D20;
        public static UInt32 GL_FRAMEBUFFER = 0x8D40;
        public static UInt32 GL_RENDERBUFFER = 0x8D41;
        public static UInt32 GL_RENDERBUFFER_WIDTH = 0x8D42;
        public static UInt32 GL_RENDERBUFFER_HEIGHT = 0x8D43;
        public static UInt32 GL_RENDERBUFFER_INTERNAL_FORMAT = 0x8D44;
        public static UInt32 GL_STENCIL_INDEX1 = 0x8D46;
        public static UInt32 GL_STENCIL_INDEX4 = 0x8D47;
        public static UInt32 GL_STENCIL_INDEX8 = 0x8D48;
        public static UInt32 GL_STENCIL_INDEX16 = 0x8D49;
        public static UInt32 GL_RENDERBUFFER_RED_SIZE = 0x8D50;
        public static UInt32 GL_RENDERBUFFER_GREEN_SIZE = 0x8D51;
        public static UInt32 GL_RENDERBUFFER_BLUE_SIZE = 0x8D52;
        public static UInt32 GL_RENDERBUFFER_ALPHA_SIZE = 0x8D53;
        public static UInt32 GL_RENDERBUFFER_DEPTH_SIZE = 0x8D54;
        public static UInt32 GL_RENDERBUFFER_STENCIL_SIZE = 0x8D55;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE = 0x8D56;
        public static UInt32 GL_MAX_SAMPLES = 0x8D57;
        public static UInt32 GL_INDEX = 0x8222;
        public static UInt32 GL_TEXTURE_LUMINANCE_TYPE = 0x8C14;
        public static UInt32 GL_TEXTURE_INTENSITY_TYPE = 0x8C15;
        public static UInt32 GL_FRAMEBUFFER_SRGB = 0x8DB9;
        public static UInt32 GL_HALF_FLOAT = 0x140B;
        public static UInt32 GL_MAP_READ_BIT = 0x0001;
        public static UInt32 GL_MAP_WRITE_BIT = 0x0002;
        public static UInt32 GL_MAP_INVALIDATE_RANGE_BIT = 0x0004;
        public static UInt32 GL_MAP_INVALIDATE_BUFFER_BIT = 0x0008;
        public static UInt32 GL_MAP_FLUSH_EXPLICIT_BIT = 0x0010;
        public static UInt32 GL_MAP_UNSYNCHRONIZED_BIT = 0x0020;
        public static UInt32 GL_COMPRESSED_RED_RGTC1 = 0x8DBB;
        public static UInt32 GL_COMPRESSED_SIGNED_RED_RGTC1 = 0x8DBC;
        public static UInt32 GL_COMPRESSED_RG_RGTC2 = 0x8DBD;
        public static UInt32 GL_COMPRESSED_SIGNED_RG_RGTC2 = 0x8DBE;
        public static UInt32 GL_RG = 0x8227;
        public static UInt32 GL_RG_INTEGER = 0x8228;
        public static UInt32 GL_R8 = 0x8229;
        public static UInt32 GL_R16 = 0x822A;
        public static UInt32 GL_RG8 = 0x822B;
        public static UInt32 GL_RG16 = 0x822C;
        public static UInt32 GL_R16F = 0x822D;
        public static UInt32 GL_R32F = 0x822E;
        public static UInt32 GL_RG16F = 0x822F;
        public static UInt32 GL_RG32F = 0x8230;
        public static UInt32 GL_R8I = 0x8231;
        public static UInt32 GL_R8UI = 0x8232;
        public static UInt32 GL_R16I = 0x8233;
        public static UInt32 GL_R16UI = 0x8234;
        public static UInt32 GL_R32I = 0x8235;
        public static UInt32 GL_R32UI = 0x8236;
        public static UInt32 GL_RG8I = 0x8237;
        public static UInt32 GL_RG8UI = 0x8238;
        public static UInt32 GL_RG16I = 0x8239;
        public static UInt32 GL_RG16UI = 0x823A;
        public static UInt32 GL_RG32I = 0x823B;
        public static UInt32 GL_RG32UI = 0x823C;
        public static UInt32 GL_VERTEX_ARRAY_BINDING = 0x85B5;
        public static UInt32 GL_CLAMP_VERTEX_COLOR = 0x891A;
        public static UInt32 GL_CLAMP_FRAGMENT_COLOR = 0x891B;
        public static UInt32 GL_ALPHA_INTEGER = 0x8D97;
        #endregion

        #region Commands
        internal delegate void glColorMaskiFunc(GLuint @index, GLboolean @r, GLboolean @g, GLboolean @b, GLboolean @a);
        internal static glColorMaskiFunc glColorMaskiPtr;
        internal static void loadColorMaski()
        {
            try
            {
                glColorMaskiPtr = (glColorMaskiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorMaski"), typeof(glColorMaskiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorMaski'.");
            }
        }
        public static void glColorMaski(GLuint @index, GLboolean @r, GLboolean @g, GLboolean @b, GLboolean @a) => glColorMaskiPtr(@index, @r, @g, @b, @a);

        internal delegate void glGetBooleani_vFunc(GLenum @target, GLuint @index, GLboolean * @data);
        internal static glGetBooleani_vFunc glGetBooleani_vPtr;
        internal static void loadGetBooleani_v()
        {
            try
            {
                glGetBooleani_vPtr = (glGetBooleani_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBooleani_v"), typeof(glGetBooleani_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBooleani_v'.");
            }
        }
        public static void glGetBooleani_v(GLenum @target, GLuint @index, GLboolean * @data) => glGetBooleani_vPtr(@target, @index, @data);

        internal delegate void glGetIntegeri_vFunc(GLenum @target, GLuint @index, GLint * @data);
        internal static glGetIntegeri_vFunc glGetIntegeri_vPtr;
        internal static void loadGetIntegeri_v()
        {
            try
            {
                glGetIntegeri_vPtr = (glGetIntegeri_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetIntegeri_v"), typeof(glGetIntegeri_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetIntegeri_v'.");
            }
        }
        public static void glGetIntegeri_v(GLenum @target, GLuint @index, GLint * @data) => glGetIntegeri_vPtr(@target, @index, @data);

        internal delegate void glEnableiFunc(GLenum @target, GLuint @index);
        internal static glEnableiFunc glEnableiPtr;
        internal static void loadEnablei()
        {
            try
            {
                glEnableiPtr = (glEnableiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnablei"), typeof(glEnableiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnablei'.");
            }
        }
        public static void glEnablei(GLenum @target, GLuint @index) => glEnableiPtr(@target, @index);

        internal delegate void glDisableiFunc(GLenum @target, GLuint @index);
        internal static glDisableiFunc glDisableiPtr;
        internal static void loadDisablei()
        {
            try
            {
                glDisableiPtr = (glDisableiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisablei"), typeof(glDisableiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisablei'.");
            }
        }
        public static void glDisablei(GLenum @target, GLuint @index) => glDisableiPtr(@target, @index);

        internal delegate GLboolean glIsEnablediFunc(GLenum @target, GLuint @index);
        internal static glIsEnablediFunc glIsEnablediPtr;
        internal static void loadIsEnabledi()
        {
            try
            {
                glIsEnablediPtr = (glIsEnablediFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsEnabledi"), typeof(glIsEnablediFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsEnabledi'.");
            }
        }
        public static GLboolean glIsEnabledi(GLenum @target, GLuint @index) => glIsEnablediPtr(@target, @index);

        internal delegate void glBeginTransformFeedbackFunc(GLenum @primitiveMode);
        internal static glBeginTransformFeedbackFunc glBeginTransformFeedbackPtr;
        internal static void loadBeginTransformFeedback()
        {
            try
            {
                glBeginTransformFeedbackPtr = (glBeginTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginTransformFeedback"), typeof(glBeginTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginTransformFeedback'.");
            }
        }
        public static void glBeginTransformFeedback(GLenum @primitiveMode) => glBeginTransformFeedbackPtr(@primitiveMode);

        internal delegate void glEndTransformFeedbackFunc();
        internal static glEndTransformFeedbackFunc glEndTransformFeedbackPtr;
        internal static void loadEndTransformFeedback()
        {
            try
            {
                glEndTransformFeedbackPtr = (glEndTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndTransformFeedback"), typeof(glEndTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndTransformFeedback'.");
            }
        }
        public static void glEndTransformFeedback() => glEndTransformFeedbackPtr();

        internal delegate void glBindBufferRangeFunc(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glBindBufferRangeFunc glBindBufferRangePtr;
        internal static void loadBindBufferRange()
        {
            try
            {
                glBindBufferRangePtr = (glBindBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferRange"), typeof(glBindBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferRange'.");
            }
        }
        public static void glBindBufferRange(GLenum @target, GLuint @index, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glBindBufferRangePtr(@target, @index, @buffer, @offset, @size);

        internal delegate void glBindBufferBaseFunc(GLenum @target, GLuint @index, GLuint @buffer);
        internal static glBindBufferBaseFunc glBindBufferBasePtr;
        internal static void loadBindBufferBase()
        {
            try
            {
                glBindBufferBasePtr = (glBindBufferBaseFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferBase"), typeof(glBindBufferBaseFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferBase'.");
            }
        }
        public static void glBindBufferBase(GLenum @target, GLuint @index, GLuint @buffer) => glBindBufferBasePtr(@target, @index, @buffer);

        internal delegate void glTransformFeedbackVaryingsFunc(GLuint @program, GLsizei @count, const GLchar *const* @varyings, GLenum @bufferMode);
        internal static glTransformFeedbackVaryingsFunc glTransformFeedbackVaryingsPtr;
        internal static void loadTransformFeedbackVaryings()
        {
            try
            {
                glTransformFeedbackVaryingsPtr = (glTransformFeedbackVaryingsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTransformFeedbackVaryings"), typeof(glTransformFeedbackVaryingsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTransformFeedbackVaryings'.");
            }
        }
        public static void glTransformFeedbackVaryings(GLuint @program, GLsizei @count, const GLchar *const* @varyings, GLenum @bufferMode) => glTransformFeedbackVaryingsPtr(@program, @count, @varyings, @bufferMode);

        internal delegate void glGetTransformFeedbackVaryingFunc(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLsizei * @size, GLenum * @type, GLchar * @name);
        internal static glGetTransformFeedbackVaryingFunc glGetTransformFeedbackVaryingPtr;
        internal static void loadGetTransformFeedbackVarying()
        {
            try
            {
                glGetTransformFeedbackVaryingPtr = (glGetTransformFeedbackVaryingFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTransformFeedbackVarying"), typeof(glGetTransformFeedbackVaryingFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTransformFeedbackVarying'.");
            }
        }
        public static void glGetTransformFeedbackVarying(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLsizei * @size, GLenum * @type, GLchar * @name) => glGetTransformFeedbackVaryingPtr(@program, @index, @bufSize, @length, @size, @type, @name);

        internal delegate void glClampColorFunc(GLenum @target, GLenum @clamp);
        internal static glClampColorFunc glClampColorPtr;
        internal static void loadClampColor()
        {
            try
            {
                glClampColorPtr = (glClampColorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClampColor"), typeof(glClampColorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClampColor'.");
            }
        }
        public static void glClampColor(GLenum @target, GLenum @clamp) => glClampColorPtr(@target, @clamp);

        internal delegate void glBeginConditionalRenderFunc(GLuint @id, GLenum @mode);
        internal static glBeginConditionalRenderFunc glBeginConditionalRenderPtr;
        internal static void loadBeginConditionalRender()
        {
            try
            {
                glBeginConditionalRenderPtr = (glBeginConditionalRenderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginConditionalRender"), typeof(glBeginConditionalRenderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginConditionalRender'.");
            }
        }
        public static void glBeginConditionalRender(GLuint @id, GLenum @mode) => glBeginConditionalRenderPtr(@id, @mode);

        internal delegate void glEndConditionalRenderFunc();
        internal static glEndConditionalRenderFunc glEndConditionalRenderPtr;
        internal static void loadEndConditionalRender()
        {
            try
            {
                glEndConditionalRenderPtr = (glEndConditionalRenderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndConditionalRender"), typeof(glEndConditionalRenderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndConditionalRender'.");
            }
        }
        public static void glEndConditionalRender() => glEndConditionalRenderPtr();

        internal delegate void glVertexAttribIPointerFunc(GLuint @index, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glVertexAttribIPointerFunc glVertexAttribIPointerPtr;
        internal static void loadVertexAttribIPointer()
        {
            try
            {
                glVertexAttribIPointerPtr = (glVertexAttribIPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribIPointer"), typeof(glVertexAttribIPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribIPointer'.");
            }
        }
        public static void glVertexAttribIPointer(GLuint @index, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glVertexAttribIPointerPtr(@index, @size, @type, @stride, @pointer);

        internal delegate void glGetVertexAttribIivFunc(GLuint @index, GLenum @pname, GLint * @params);
        internal static glGetVertexAttribIivFunc glGetVertexAttribIivPtr;
        internal static void loadGetVertexAttribIiv()
        {
            try
            {
                glGetVertexAttribIivPtr = (glGetVertexAttribIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribIiv"), typeof(glGetVertexAttribIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribIiv'.");
            }
        }
        public static void glGetVertexAttribIiv(GLuint @index, GLenum @pname, GLint * @params) => glGetVertexAttribIivPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribIuivFunc(GLuint @index, GLenum @pname, GLuint * @params);
        internal static glGetVertexAttribIuivFunc glGetVertexAttribIuivPtr;
        internal static void loadGetVertexAttribIuiv()
        {
            try
            {
                glGetVertexAttribIuivPtr = (glGetVertexAttribIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribIuiv"), typeof(glGetVertexAttribIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribIuiv'.");
            }
        }
        public static void glGetVertexAttribIuiv(GLuint @index, GLenum @pname, GLuint * @params) => glGetVertexAttribIuivPtr(@index, @pname, @params);

        internal delegate void glVertexAttribI1iFunc(GLuint @index, GLint @x);
        internal static glVertexAttribI1iFunc glVertexAttribI1iPtr;
        internal static void loadVertexAttribI1i()
        {
            try
            {
                glVertexAttribI1iPtr = (glVertexAttribI1iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI1i"), typeof(glVertexAttribI1iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI1i'.");
            }
        }
        public static void glVertexAttribI1i(GLuint @index, GLint @x) => glVertexAttribI1iPtr(@index, @x);

        internal delegate void glVertexAttribI2iFunc(GLuint @index, GLint @x, GLint @y);
        internal static glVertexAttribI2iFunc glVertexAttribI2iPtr;
        internal static void loadVertexAttribI2i()
        {
            try
            {
                glVertexAttribI2iPtr = (glVertexAttribI2iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI2i"), typeof(glVertexAttribI2iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI2i'.");
            }
        }
        public static void glVertexAttribI2i(GLuint @index, GLint @x, GLint @y) => glVertexAttribI2iPtr(@index, @x, @y);

        internal delegate void glVertexAttribI3iFunc(GLuint @index, GLint @x, GLint @y, GLint @z);
        internal static glVertexAttribI3iFunc glVertexAttribI3iPtr;
        internal static void loadVertexAttribI3i()
        {
            try
            {
                glVertexAttribI3iPtr = (glVertexAttribI3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI3i"), typeof(glVertexAttribI3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI3i'.");
            }
        }
        public static void glVertexAttribI3i(GLuint @index, GLint @x, GLint @y, GLint @z) => glVertexAttribI3iPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttribI4iFunc(GLuint @index, GLint @x, GLint @y, GLint @z, GLint @w);
        internal static glVertexAttribI4iFunc glVertexAttribI4iPtr;
        internal static void loadVertexAttribI4i()
        {
            try
            {
                glVertexAttribI4iPtr = (glVertexAttribI4iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4i"), typeof(glVertexAttribI4iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4i'.");
            }
        }
        public static void glVertexAttribI4i(GLuint @index, GLint @x, GLint @y, GLint @z, GLint @w) => glVertexAttribI4iPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttribI1uiFunc(GLuint @index, GLuint @x);
        internal static glVertexAttribI1uiFunc glVertexAttribI1uiPtr;
        internal static void loadVertexAttribI1ui()
        {
            try
            {
                glVertexAttribI1uiPtr = (glVertexAttribI1uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI1ui"), typeof(glVertexAttribI1uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI1ui'.");
            }
        }
        public static void glVertexAttribI1ui(GLuint @index, GLuint @x) => glVertexAttribI1uiPtr(@index, @x);

        internal delegate void glVertexAttribI2uiFunc(GLuint @index, GLuint @x, GLuint @y);
        internal static glVertexAttribI2uiFunc glVertexAttribI2uiPtr;
        internal static void loadVertexAttribI2ui()
        {
            try
            {
                glVertexAttribI2uiPtr = (glVertexAttribI2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI2ui"), typeof(glVertexAttribI2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI2ui'.");
            }
        }
        public static void glVertexAttribI2ui(GLuint @index, GLuint @x, GLuint @y) => glVertexAttribI2uiPtr(@index, @x, @y);

        internal delegate void glVertexAttribI3uiFunc(GLuint @index, GLuint @x, GLuint @y, GLuint @z);
        internal static glVertexAttribI3uiFunc glVertexAttribI3uiPtr;
        internal static void loadVertexAttribI3ui()
        {
            try
            {
                glVertexAttribI3uiPtr = (glVertexAttribI3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI3ui"), typeof(glVertexAttribI3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI3ui'.");
            }
        }
        public static void glVertexAttribI3ui(GLuint @index, GLuint @x, GLuint @y, GLuint @z) => glVertexAttribI3uiPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttribI4uiFunc(GLuint @index, GLuint @x, GLuint @y, GLuint @z, GLuint @w);
        internal static glVertexAttribI4uiFunc glVertexAttribI4uiPtr;
        internal static void loadVertexAttribI4ui()
        {
            try
            {
                glVertexAttribI4uiPtr = (glVertexAttribI4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4ui"), typeof(glVertexAttribI4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4ui'.");
            }
        }
        public static void glVertexAttribI4ui(GLuint @index, GLuint @x, GLuint @y, GLuint @z, GLuint @w) => glVertexAttribI4uiPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttribI1ivFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttribI1ivFunc glVertexAttribI1ivPtr;
        internal static void loadVertexAttribI1iv()
        {
            try
            {
                glVertexAttribI1ivPtr = (glVertexAttribI1ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI1iv"), typeof(glVertexAttribI1ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI1iv'.");
            }
        }
        public static void glVertexAttribI1iv(GLuint @index, const GLint * @v) => glVertexAttribI1ivPtr(@index, @v);

        internal delegate void glVertexAttribI2ivFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttribI2ivFunc glVertexAttribI2ivPtr;
        internal static void loadVertexAttribI2iv()
        {
            try
            {
                glVertexAttribI2ivPtr = (glVertexAttribI2ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI2iv"), typeof(glVertexAttribI2ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI2iv'.");
            }
        }
        public static void glVertexAttribI2iv(GLuint @index, const GLint * @v) => glVertexAttribI2ivPtr(@index, @v);

        internal delegate void glVertexAttribI3ivFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttribI3ivFunc glVertexAttribI3ivPtr;
        internal static void loadVertexAttribI3iv()
        {
            try
            {
                glVertexAttribI3ivPtr = (glVertexAttribI3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI3iv"), typeof(glVertexAttribI3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI3iv'.");
            }
        }
        public static void glVertexAttribI3iv(GLuint @index, const GLint * @v) => glVertexAttribI3ivPtr(@index, @v);

        internal delegate void glVertexAttribI4ivFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttribI4ivFunc glVertexAttribI4ivPtr;
        internal static void loadVertexAttribI4iv()
        {
            try
            {
                glVertexAttribI4ivPtr = (glVertexAttribI4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4iv"), typeof(glVertexAttribI4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4iv'.");
            }
        }
        public static void glVertexAttribI4iv(GLuint @index, const GLint * @v) => glVertexAttribI4ivPtr(@index, @v);

        internal delegate void glVertexAttribI1uivFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttribI1uivFunc glVertexAttribI1uivPtr;
        internal static void loadVertexAttribI1uiv()
        {
            try
            {
                glVertexAttribI1uivPtr = (glVertexAttribI1uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI1uiv"), typeof(glVertexAttribI1uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI1uiv'.");
            }
        }
        public static void glVertexAttribI1uiv(GLuint @index, const GLuint * @v) => glVertexAttribI1uivPtr(@index, @v);

        internal delegate void glVertexAttribI2uivFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttribI2uivFunc glVertexAttribI2uivPtr;
        internal static void loadVertexAttribI2uiv()
        {
            try
            {
                glVertexAttribI2uivPtr = (glVertexAttribI2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI2uiv"), typeof(glVertexAttribI2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI2uiv'.");
            }
        }
        public static void glVertexAttribI2uiv(GLuint @index, const GLuint * @v) => glVertexAttribI2uivPtr(@index, @v);

        internal delegate void glVertexAttribI3uivFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttribI3uivFunc glVertexAttribI3uivPtr;
        internal static void loadVertexAttribI3uiv()
        {
            try
            {
                glVertexAttribI3uivPtr = (glVertexAttribI3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI3uiv"), typeof(glVertexAttribI3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI3uiv'.");
            }
        }
        public static void glVertexAttribI3uiv(GLuint @index, const GLuint * @v) => glVertexAttribI3uivPtr(@index, @v);

        internal delegate void glVertexAttribI4uivFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttribI4uivFunc glVertexAttribI4uivPtr;
        internal static void loadVertexAttribI4uiv()
        {
            try
            {
                glVertexAttribI4uivPtr = (glVertexAttribI4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4uiv"), typeof(glVertexAttribI4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4uiv'.");
            }
        }
        public static void glVertexAttribI4uiv(GLuint @index, const GLuint * @v) => glVertexAttribI4uivPtr(@index, @v);

        internal delegate void glVertexAttribI4bvFunc(GLuint @index, const GLbyte * @v);
        internal static glVertexAttribI4bvFunc glVertexAttribI4bvPtr;
        internal static void loadVertexAttribI4bv()
        {
            try
            {
                glVertexAttribI4bvPtr = (glVertexAttribI4bvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4bv"), typeof(glVertexAttribI4bvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4bv'.");
            }
        }
        public static void glVertexAttribI4bv(GLuint @index, const GLbyte * @v) => glVertexAttribI4bvPtr(@index, @v);

        internal delegate void glVertexAttribI4svFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttribI4svFunc glVertexAttribI4svPtr;
        internal static void loadVertexAttribI4sv()
        {
            try
            {
                glVertexAttribI4svPtr = (glVertexAttribI4svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4sv"), typeof(glVertexAttribI4svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4sv'.");
            }
        }
        public static void glVertexAttribI4sv(GLuint @index, const GLshort * @v) => glVertexAttribI4svPtr(@index, @v);

        internal delegate void glVertexAttribI4ubvFunc(GLuint @index, const GLubyte * @v);
        internal static glVertexAttribI4ubvFunc glVertexAttribI4ubvPtr;
        internal static void loadVertexAttribI4ubv()
        {
            try
            {
                glVertexAttribI4ubvPtr = (glVertexAttribI4ubvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4ubv"), typeof(glVertexAttribI4ubvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4ubv'.");
            }
        }
        public static void glVertexAttribI4ubv(GLuint @index, const GLubyte * @v) => glVertexAttribI4ubvPtr(@index, @v);

        internal delegate void glVertexAttribI4usvFunc(GLuint @index, const GLushort * @v);
        internal static glVertexAttribI4usvFunc glVertexAttribI4usvPtr;
        internal static void loadVertexAttribI4usv()
        {
            try
            {
                glVertexAttribI4usvPtr = (glVertexAttribI4usvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribI4usv"), typeof(glVertexAttribI4usvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribI4usv'.");
            }
        }
        public static void glVertexAttribI4usv(GLuint @index, const GLushort * @v) => glVertexAttribI4usvPtr(@index, @v);

        internal delegate void glGetUniformuivFunc(GLuint @program, GLint @location, GLuint * @params);
        internal static glGetUniformuivFunc glGetUniformuivPtr;
        internal static void loadGetUniformuiv()
        {
            try
            {
                glGetUniformuivPtr = (glGetUniformuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformuiv"), typeof(glGetUniformuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformuiv'.");
            }
        }
        public static void glGetUniformuiv(GLuint @program, GLint @location, GLuint * @params) => glGetUniformuivPtr(@program, @location, @params);

        internal delegate void glBindFragDataLocationFunc(GLuint @program, GLuint @color, const GLchar * @name);
        internal static glBindFragDataLocationFunc glBindFragDataLocationPtr;
        internal static void loadBindFragDataLocation()
        {
            try
            {
                glBindFragDataLocationPtr = (glBindFragDataLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindFragDataLocation"), typeof(glBindFragDataLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindFragDataLocation'.");
            }
        }
        public static void glBindFragDataLocation(GLuint @program, GLuint @color, const GLchar * @name) => glBindFragDataLocationPtr(@program, @color, @name);

        internal delegate GLint glGetFragDataLocationFunc(GLuint @program, const GLchar * @name);
        internal static glGetFragDataLocationFunc glGetFragDataLocationPtr;
        internal static void loadGetFragDataLocation()
        {
            try
            {
                glGetFragDataLocationPtr = (glGetFragDataLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFragDataLocation"), typeof(glGetFragDataLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFragDataLocation'.");
            }
        }
        public static GLint glGetFragDataLocation(GLuint @program, const GLchar * @name) => glGetFragDataLocationPtr(@program, @name);

        internal delegate void glUniform1uiFunc(GLint @location, GLuint @v0);
        internal static glUniform1uiFunc glUniform1uiPtr;
        internal static void loadUniform1ui()
        {
            try
            {
                glUniform1uiPtr = (glUniform1uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1ui"), typeof(glUniform1uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1ui'.");
            }
        }
        public static void glUniform1ui(GLint @location, GLuint @v0) => glUniform1uiPtr(@location, @v0);

        internal delegate void glUniform2uiFunc(GLint @location, GLuint @v0, GLuint @v1);
        internal static glUniform2uiFunc glUniform2uiPtr;
        internal static void loadUniform2ui()
        {
            try
            {
                glUniform2uiPtr = (glUniform2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2ui"), typeof(glUniform2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2ui'.");
            }
        }
        public static void glUniform2ui(GLint @location, GLuint @v0, GLuint @v1) => glUniform2uiPtr(@location, @v0, @v1);

        internal delegate void glUniform3uiFunc(GLint @location, GLuint @v0, GLuint @v1, GLuint @v2);
        internal static glUniform3uiFunc glUniform3uiPtr;
        internal static void loadUniform3ui()
        {
            try
            {
                glUniform3uiPtr = (glUniform3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3ui"), typeof(glUniform3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3ui'.");
            }
        }
        public static void glUniform3ui(GLint @location, GLuint @v0, GLuint @v1, GLuint @v2) => glUniform3uiPtr(@location, @v0, @v1, @v2);

        internal delegate void glUniform4uiFunc(GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3);
        internal static glUniform4uiFunc glUniform4uiPtr;
        internal static void loadUniform4ui()
        {
            try
            {
                glUniform4uiPtr = (glUniform4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4ui"), typeof(glUniform4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4ui'.");
            }
        }
        public static void glUniform4ui(GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3) => glUniform4uiPtr(@location, @v0, @v1, @v2, @v3);

        internal delegate void glUniform1uivFunc(GLint @location, GLsizei @count, const GLuint * @value);
        internal static glUniform1uivFunc glUniform1uivPtr;
        internal static void loadUniform1uiv()
        {
            try
            {
                glUniform1uivPtr = (glUniform1uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1uiv"), typeof(glUniform1uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1uiv'.");
            }
        }
        public static void glUniform1uiv(GLint @location, GLsizei @count, const GLuint * @value) => glUniform1uivPtr(@location, @count, @value);

        internal delegate void glUniform2uivFunc(GLint @location, GLsizei @count, const GLuint * @value);
        internal static glUniform2uivFunc glUniform2uivPtr;
        internal static void loadUniform2uiv()
        {
            try
            {
                glUniform2uivPtr = (glUniform2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2uiv"), typeof(glUniform2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2uiv'.");
            }
        }
        public static void glUniform2uiv(GLint @location, GLsizei @count, const GLuint * @value) => glUniform2uivPtr(@location, @count, @value);

        internal delegate void glUniform3uivFunc(GLint @location, GLsizei @count, const GLuint * @value);
        internal static glUniform3uivFunc glUniform3uivPtr;
        internal static void loadUniform3uiv()
        {
            try
            {
                glUniform3uivPtr = (glUniform3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3uiv"), typeof(glUniform3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3uiv'.");
            }
        }
        public static void glUniform3uiv(GLint @location, GLsizei @count, const GLuint * @value) => glUniform3uivPtr(@location, @count, @value);

        internal delegate void glUniform4uivFunc(GLint @location, GLsizei @count, const GLuint * @value);
        internal static glUniform4uivFunc glUniform4uivPtr;
        internal static void loadUniform4uiv()
        {
            try
            {
                glUniform4uivPtr = (glUniform4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4uiv"), typeof(glUniform4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4uiv'.");
            }
        }
        public static void glUniform4uiv(GLint @location, GLsizei @count, const GLuint * @value) => glUniform4uivPtr(@location, @count, @value);

        internal delegate void glTexParameterIivFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glTexParameterIivFunc glTexParameterIivPtr;
        internal static void loadTexParameterIiv()
        {
            try
            {
                glTexParameterIivPtr = (glTexParameterIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterIiv"), typeof(glTexParameterIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterIiv'.");
            }
        }
        public static void glTexParameterIiv(GLenum @target, GLenum @pname, const GLint * @params) => glTexParameterIivPtr(@target, @pname, @params);

        internal delegate void glTexParameterIuivFunc(GLenum @target, GLenum @pname, const GLuint * @params);
        internal static glTexParameterIuivFunc glTexParameterIuivPtr;
        internal static void loadTexParameterIuiv()
        {
            try
            {
                glTexParameterIuivPtr = (glTexParameterIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterIuiv"), typeof(glTexParameterIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterIuiv'.");
            }
        }
        public static void glTexParameterIuiv(GLenum @target, GLenum @pname, const GLuint * @params) => glTexParameterIuivPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterIivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetTexParameterIivFunc glGetTexParameterIivPtr;
        internal static void loadGetTexParameterIiv()
        {
            try
            {
                glGetTexParameterIivPtr = (glGetTexParameterIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterIiv"), typeof(glGetTexParameterIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterIiv'.");
            }
        }
        public static void glGetTexParameterIiv(GLenum @target, GLenum @pname, GLint * @params) => glGetTexParameterIivPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterIuivFunc(GLenum @target, GLenum @pname, GLuint * @params);
        internal static glGetTexParameterIuivFunc glGetTexParameterIuivPtr;
        internal static void loadGetTexParameterIuiv()
        {
            try
            {
                glGetTexParameterIuivPtr = (glGetTexParameterIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterIuiv"), typeof(glGetTexParameterIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterIuiv'.");
            }
        }
        public static void glGetTexParameterIuiv(GLenum @target, GLenum @pname, GLuint * @params) => glGetTexParameterIuivPtr(@target, @pname, @params);

        internal delegate void glClearBufferivFunc(GLenum @buffer, GLint @drawbuffer, const GLint * @value);
        internal static glClearBufferivFunc glClearBufferivPtr;
        internal static void loadClearBufferiv()
        {
            try
            {
                glClearBufferivPtr = (glClearBufferivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearBufferiv"), typeof(glClearBufferivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearBufferiv'.");
            }
        }
        public static void glClearBufferiv(GLenum @buffer, GLint @drawbuffer, const GLint * @value) => glClearBufferivPtr(@buffer, @drawbuffer, @value);

        internal delegate void glClearBufferuivFunc(GLenum @buffer, GLint @drawbuffer, const GLuint * @value);
        internal static glClearBufferuivFunc glClearBufferuivPtr;
        internal static void loadClearBufferuiv()
        {
            try
            {
                glClearBufferuivPtr = (glClearBufferuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearBufferuiv"), typeof(glClearBufferuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearBufferuiv'.");
            }
        }
        public static void glClearBufferuiv(GLenum @buffer, GLint @drawbuffer, const GLuint * @value) => glClearBufferuivPtr(@buffer, @drawbuffer, @value);

        internal delegate void glClearBufferfvFunc(GLenum @buffer, GLint @drawbuffer, const GLfloat * @value);
        internal static glClearBufferfvFunc glClearBufferfvPtr;
        internal static void loadClearBufferfv()
        {
            try
            {
                glClearBufferfvPtr = (glClearBufferfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearBufferfv"), typeof(glClearBufferfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearBufferfv'.");
            }
        }
        public static void glClearBufferfv(GLenum @buffer, GLint @drawbuffer, const GLfloat * @value) => glClearBufferfvPtr(@buffer, @drawbuffer, @value);

        internal delegate void glClearBufferfiFunc(GLenum @buffer, GLint @drawbuffer, GLfloat @depth, GLint @stencil);
        internal static glClearBufferfiFunc glClearBufferfiPtr;
        internal static void loadClearBufferfi()
        {
            try
            {
                glClearBufferfiPtr = (glClearBufferfiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearBufferfi"), typeof(glClearBufferfiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearBufferfi'.");
            }
        }
        public static void glClearBufferfi(GLenum @buffer, GLint @drawbuffer, GLfloat @depth, GLint @stencil) => glClearBufferfiPtr(@buffer, @drawbuffer, @depth, @stencil);

        internal delegate constGLubyte* glGetStringiFunc(GLenum @name, GLuint @index);
        internal static glGetStringiFunc glGetStringiPtr;
        internal static void loadGetStringi()
        {
            try
            {
                glGetStringiPtr = (glGetStringiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetStringi"), typeof(glGetStringiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetStringi'.");
            }
        }
        public static constGLubyte* glGetStringi(GLenum @name, GLuint @index) => glGetStringiPtr(@name, @index);

        internal delegate GLboolean glIsRenderbufferFunc(GLuint @renderbuffer);
        internal static glIsRenderbufferFunc glIsRenderbufferPtr;
        internal static void loadIsRenderbuffer()
        {
            try
            {
                glIsRenderbufferPtr = (glIsRenderbufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsRenderbuffer"), typeof(glIsRenderbufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsRenderbuffer'.");
            }
        }
        public static GLboolean glIsRenderbuffer(GLuint @renderbuffer) => glIsRenderbufferPtr(@renderbuffer);

        internal delegate void glBindRenderbufferFunc(GLenum @target, GLuint @renderbuffer);
        internal static glBindRenderbufferFunc glBindRenderbufferPtr;
        internal static void loadBindRenderbuffer()
        {
            try
            {
                glBindRenderbufferPtr = (glBindRenderbufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindRenderbuffer"), typeof(glBindRenderbufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindRenderbuffer'.");
            }
        }
        public static void glBindRenderbuffer(GLenum @target, GLuint @renderbuffer) => glBindRenderbufferPtr(@target, @renderbuffer);

        internal delegate void glDeleteRenderbuffersFunc(GLsizei @n, const GLuint * @renderbuffers);
        internal static glDeleteRenderbuffersFunc glDeleteRenderbuffersPtr;
        internal static void loadDeleteRenderbuffers()
        {
            try
            {
                glDeleteRenderbuffersPtr = (glDeleteRenderbuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteRenderbuffers"), typeof(glDeleteRenderbuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteRenderbuffers'.");
            }
        }
        public static void glDeleteRenderbuffers(GLsizei @n, const GLuint * @renderbuffers) => glDeleteRenderbuffersPtr(@n, @renderbuffers);

        internal delegate void glGenRenderbuffersFunc(GLsizei @n, GLuint * @renderbuffers);
        internal static glGenRenderbuffersFunc glGenRenderbuffersPtr;
        internal static void loadGenRenderbuffers()
        {
            try
            {
                glGenRenderbuffersPtr = (glGenRenderbuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenRenderbuffers"), typeof(glGenRenderbuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenRenderbuffers'.");
            }
        }
        public static void glGenRenderbuffers(GLsizei @n, GLuint * @renderbuffers) => glGenRenderbuffersPtr(@n, @renderbuffers);

        internal delegate void glRenderbufferStorageFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageFunc glRenderbufferStoragePtr;
        internal static void loadRenderbufferStorage()
        {
            try
            {
                glRenderbufferStoragePtr = (glRenderbufferStorageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorage"), typeof(glRenderbufferStorageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorage'.");
            }
        }
        public static void glRenderbufferStorage(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStoragePtr(@target, @internalformat, @width, @height);

        internal delegate void glGetRenderbufferParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetRenderbufferParameterivFunc glGetRenderbufferParameterivPtr;
        internal static void loadGetRenderbufferParameteriv()
        {
            try
            {
                glGetRenderbufferParameterivPtr = (glGetRenderbufferParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetRenderbufferParameteriv"), typeof(glGetRenderbufferParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetRenderbufferParameteriv'.");
            }
        }
        public static void glGetRenderbufferParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetRenderbufferParameterivPtr(@target, @pname, @params);

        internal delegate GLboolean glIsFramebufferFunc(GLuint @framebuffer);
        internal static glIsFramebufferFunc glIsFramebufferPtr;
        internal static void loadIsFramebuffer()
        {
            try
            {
                glIsFramebufferPtr = (glIsFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsFramebuffer"), typeof(glIsFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsFramebuffer'.");
            }
        }
        public static GLboolean glIsFramebuffer(GLuint @framebuffer) => glIsFramebufferPtr(@framebuffer);

        internal delegate void glBindFramebufferFunc(GLenum @target, GLuint @framebuffer);
        internal static glBindFramebufferFunc glBindFramebufferPtr;
        internal static void loadBindFramebuffer()
        {
            try
            {
                glBindFramebufferPtr = (glBindFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindFramebuffer"), typeof(glBindFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindFramebuffer'.");
            }
        }
        public static void glBindFramebuffer(GLenum @target, GLuint @framebuffer) => glBindFramebufferPtr(@target, @framebuffer);

        internal delegate void glDeleteFramebuffersFunc(GLsizei @n, const GLuint * @framebuffers);
        internal static glDeleteFramebuffersFunc glDeleteFramebuffersPtr;
        internal static void loadDeleteFramebuffers()
        {
            try
            {
                glDeleteFramebuffersPtr = (glDeleteFramebuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteFramebuffers"), typeof(glDeleteFramebuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteFramebuffers'.");
            }
        }
        public static void glDeleteFramebuffers(GLsizei @n, const GLuint * @framebuffers) => glDeleteFramebuffersPtr(@n, @framebuffers);

        internal delegate void glGenFramebuffersFunc(GLsizei @n, GLuint * @framebuffers);
        internal static glGenFramebuffersFunc glGenFramebuffersPtr;
        internal static void loadGenFramebuffers()
        {
            try
            {
                glGenFramebuffersPtr = (glGenFramebuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenFramebuffers"), typeof(glGenFramebuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenFramebuffers'.");
            }
        }
        public static void glGenFramebuffers(GLsizei @n, GLuint * @framebuffers) => glGenFramebuffersPtr(@n, @framebuffers);

        internal delegate GLenum glCheckFramebufferStatusFunc(GLenum @target);
        internal static glCheckFramebufferStatusFunc glCheckFramebufferStatusPtr;
        internal static void loadCheckFramebufferStatus()
        {
            try
            {
                glCheckFramebufferStatusPtr = (glCheckFramebufferStatusFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCheckFramebufferStatus"), typeof(glCheckFramebufferStatusFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCheckFramebufferStatus'.");
            }
        }
        public static GLenum glCheckFramebufferStatus(GLenum @target) => glCheckFramebufferStatusPtr(@target);

        internal delegate void glFramebufferTexture1DFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level);
        internal static glFramebufferTexture1DFunc glFramebufferTexture1DPtr;
        internal static void loadFramebufferTexture1D()
        {
            try
            {
                glFramebufferTexture1DPtr = (glFramebufferTexture1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture1D"), typeof(glFramebufferTexture1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture1D'.");
            }
        }
        public static void glFramebufferTexture1D(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level) => glFramebufferTexture1DPtr(@target, @attachment, @textarget, @texture, @level);

        internal delegate void glFramebufferTexture2DFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level);
        internal static glFramebufferTexture2DFunc glFramebufferTexture2DPtr;
        internal static void loadFramebufferTexture2D()
        {
            try
            {
                glFramebufferTexture2DPtr = (glFramebufferTexture2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture2D"), typeof(glFramebufferTexture2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture2D'.");
            }
        }
        public static void glFramebufferTexture2D(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level) => glFramebufferTexture2DPtr(@target, @attachment, @textarget, @texture, @level);

        internal delegate void glFramebufferTexture3DFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLint @zoffset);
        internal static glFramebufferTexture3DFunc glFramebufferTexture3DPtr;
        internal static void loadFramebufferTexture3D()
        {
            try
            {
                glFramebufferTexture3DPtr = (glFramebufferTexture3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture3D"), typeof(glFramebufferTexture3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture3D'.");
            }
        }
        public static void glFramebufferTexture3D(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLint @zoffset) => glFramebufferTexture3DPtr(@target, @attachment, @textarget, @texture, @level, @zoffset);

        internal delegate void glFramebufferRenderbufferFunc(GLenum @target, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer);
        internal static glFramebufferRenderbufferFunc glFramebufferRenderbufferPtr;
        internal static void loadFramebufferRenderbuffer()
        {
            try
            {
                glFramebufferRenderbufferPtr = (glFramebufferRenderbufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferRenderbuffer"), typeof(glFramebufferRenderbufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferRenderbuffer'.");
            }
        }
        public static void glFramebufferRenderbuffer(GLenum @target, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer) => glFramebufferRenderbufferPtr(@target, @attachment, @renderbuffertarget, @renderbuffer);

        internal delegate void glGetFramebufferAttachmentParameterivFunc(GLenum @target, GLenum @attachment, GLenum @pname, GLint * @params);
        internal static glGetFramebufferAttachmentParameterivFunc glGetFramebufferAttachmentParameterivPtr;
        internal static void loadGetFramebufferAttachmentParameteriv()
        {
            try
            {
                glGetFramebufferAttachmentParameterivPtr = (glGetFramebufferAttachmentParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFramebufferAttachmentParameteriv"), typeof(glGetFramebufferAttachmentParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFramebufferAttachmentParameteriv'.");
            }
        }
        public static void glGetFramebufferAttachmentParameteriv(GLenum @target, GLenum @attachment, GLenum @pname, GLint * @params) => glGetFramebufferAttachmentParameterivPtr(@target, @attachment, @pname, @params);

        internal delegate void glGenerateMipmapFunc(GLenum @target);
        internal static glGenerateMipmapFunc glGenerateMipmapPtr;
        internal static void loadGenerateMipmap()
        {
            try
            {
                glGenerateMipmapPtr = (glGenerateMipmapFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenerateMipmap"), typeof(glGenerateMipmapFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenerateMipmap'.");
            }
        }
        public static void glGenerateMipmap(GLenum @target) => glGenerateMipmapPtr(@target);

        internal delegate void glBlitFramebufferFunc(GLint @srcX0, GLint @srcY0, GLint @srcX1, GLint @srcY1, GLint @dstX0, GLint @dstY0, GLint @dstX1, GLint @dstY1, GLbitfield @mask, GLenum @filter);
        internal static glBlitFramebufferFunc glBlitFramebufferPtr;
        internal static void loadBlitFramebuffer()
        {
            try
            {
                glBlitFramebufferPtr = (glBlitFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlitFramebuffer"), typeof(glBlitFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlitFramebuffer'.");
            }
        }
        public static void glBlitFramebuffer(GLint @srcX0, GLint @srcY0, GLint @srcX1, GLint @srcY1, GLint @dstX0, GLint @dstY0, GLint @dstX1, GLint @dstY1, GLbitfield @mask, GLenum @filter) => glBlitFramebufferPtr(@srcX0, @srcY0, @srcX1, @srcY1, @dstX0, @dstY0, @dstX1, @dstY1, @mask, @filter);

        internal delegate void glRenderbufferStorageMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageMultisampleFunc glRenderbufferStorageMultisamplePtr;
        internal static void loadRenderbufferStorageMultisample()
        {
            try
            {
                glRenderbufferStorageMultisamplePtr = (glRenderbufferStorageMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorageMultisample"), typeof(glRenderbufferStorageMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorageMultisample'.");
            }
        }
        public static void glRenderbufferStorageMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStorageMultisamplePtr(@target, @samples, @internalformat, @width, @height);

        internal delegate void glFramebufferTextureLayerFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer);
        internal static glFramebufferTextureLayerFunc glFramebufferTextureLayerPtr;
        internal static void loadFramebufferTextureLayer()
        {
            try
            {
                glFramebufferTextureLayerPtr = (glFramebufferTextureLayerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureLayer"), typeof(glFramebufferTextureLayerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureLayer'.");
            }
        }
        public static void glFramebufferTextureLayer(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer) => glFramebufferTextureLayerPtr(@target, @attachment, @texture, @level, @layer);

        internal delegate void * glMapBufferRangeFunc(GLenum @target, GLintptr @offset, GLsizeiptr @length, GLbitfield @access);
        internal static glMapBufferRangeFunc glMapBufferRangePtr;
        internal static void loadMapBufferRange()
        {
            try
            {
                glMapBufferRangePtr = (glMapBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapBufferRange"), typeof(glMapBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapBufferRange'.");
            }
        }
        public static void * glMapBufferRange(GLenum @target, GLintptr @offset, GLsizeiptr @length, GLbitfield @access) => glMapBufferRangePtr(@target, @offset, @length, @access);

        internal delegate void glFlushMappedBufferRangeFunc(GLenum @target, GLintptr @offset, GLsizeiptr @length);
        internal static glFlushMappedBufferRangeFunc glFlushMappedBufferRangePtr;
        internal static void loadFlushMappedBufferRange()
        {
            try
            {
                glFlushMappedBufferRangePtr = (glFlushMappedBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlushMappedBufferRange"), typeof(glFlushMappedBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlushMappedBufferRange'.");
            }
        }
        public static void glFlushMappedBufferRange(GLenum @target, GLintptr @offset, GLsizeiptr @length) => glFlushMappedBufferRangePtr(@target, @offset, @length);

        internal delegate void glBindVertexArrayFunc(GLuint @array);
        internal static glBindVertexArrayFunc glBindVertexArrayPtr;
        internal static void loadBindVertexArray()
        {
            try
            {
                glBindVertexArrayPtr = (glBindVertexArrayFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVertexArray"), typeof(glBindVertexArrayFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVertexArray'.");
            }
        }
        public static void glBindVertexArray(GLuint @array) => glBindVertexArrayPtr(@array);

        internal delegate void glDeleteVertexArraysFunc(GLsizei @n, const GLuint * @arrays);
        internal static glDeleteVertexArraysFunc glDeleteVertexArraysPtr;
        internal static void loadDeleteVertexArrays()
        {
            try
            {
                glDeleteVertexArraysPtr = (glDeleteVertexArraysFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteVertexArrays"), typeof(glDeleteVertexArraysFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteVertexArrays'.");
            }
        }
        public static void glDeleteVertexArrays(GLsizei @n, const GLuint * @arrays) => glDeleteVertexArraysPtr(@n, @arrays);

        internal delegate void glGenVertexArraysFunc(GLsizei @n, GLuint * @arrays);
        internal static glGenVertexArraysFunc glGenVertexArraysPtr;
        internal static void loadGenVertexArrays()
        {
            try
            {
                glGenVertexArraysPtr = (glGenVertexArraysFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenVertexArrays"), typeof(glGenVertexArraysFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenVertexArrays'.");
            }
        }
        public static void glGenVertexArrays(GLsizei @n, GLuint * @arrays) => glGenVertexArraysPtr(@n, @arrays);

        internal delegate GLboolean glIsVertexArrayFunc(GLuint @array);
        internal static glIsVertexArrayFunc glIsVertexArrayPtr;
        internal static void loadIsVertexArray()
        {
            try
            {
                glIsVertexArrayPtr = (glIsVertexArrayFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsVertexArray"), typeof(glIsVertexArrayFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsVertexArray'.");
            }
        }
        public static GLboolean glIsVertexArray(GLuint @array) => glIsVertexArrayPtr(@array);
        #endregion
    }
}
