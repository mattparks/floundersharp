using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_PGI_vertex_hints
    {
        #region Interop
        static GL_PGI_vertex_hints()
        {
            Console.WriteLine("Initalising GL_PGI_vertex_hints interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_DATA_HINT_PGI = 0x1A22A;
        public static UInt32 GL_VERTEX_CONSISTENT_HINT_PGI = 0x1A22B;
        public static UInt32 GL_MATERIAL_SIDE_HINT_PGI = 0x1A22C;
        public static UInt32 GL_MAX_VERTEX_HINT_PGI = 0x1A22D;
        public static UInt32 GL_COLOR3_BIT_PGI = 0x00010000;
        public static UInt32 GL_COLOR4_BIT_PGI = 0x00020000;
        public static UInt32 GL_EDGEFLAG_BIT_PGI = 0x00040000;
        public static UInt32 GL_INDEX_BIT_PGI = 0x00080000;
        public static UInt32 GL_MAT_AMBIENT_BIT_PGI = 0x00100000;
        public static UInt32 GL_MAT_AMBIENT_AND_DIFFUSE_BIT_PGI = 0x00200000;
        public static UInt32 GL_MAT_DIFFUSE_BIT_PGI = 0x00400000;
        public static UInt32 GL_MAT_EMISSION_BIT_PGI = 0x00800000;
        public static UInt32 GL_MAT_COLOR_INDEXES_BIT_PGI = 0x01000000;
        public static UInt32 GL_MAT_SHININESS_BIT_PGI = 0x02000000;
        public static UInt32 GL_MAT_SPECULAR_BIT_PGI = 0x04000000;
        public static UInt32 GL_NORMAL_BIT_PGI = 0x08000000;
        public static UInt32 GL_TEXCOORD1_BIT_PGI = 0x10000000;
        public static UInt32 GL_TEXCOORD2_BIT_PGI = 0x20000000;
        public static UInt32 GL_TEXCOORD3_BIT_PGI = 0x40000000;
        public static UInt32 GL_TEXCOORD4_BIT_PGI = 0x80000000;
        public static UInt32 GL_VERTEX23_BIT_PGI = 0x00000004;
        public static UInt32 GL_VERTEX4_BIT_PGI = 0x00000008;
        #endregion

        #region Commands
        #endregion
    }
}
