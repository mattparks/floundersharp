using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_PGI_misc_hints
    {
        #region Interop
        static GL_PGI_misc_hints()
        {
            Console.WriteLine("Initalising GL_PGI_misc_hints interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadHintPGI();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PREFER_DOUBLEBUFFER_HINT_PGI = 0x1A1F8;
        public static UInt32 GL_CONSERVE_MEMORY_HINT_PGI = 0x1A1FD;
        public static UInt32 GL_RECLAIM_MEMORY_HINT_PGI = 0x1A1FE;
        public static UInt32 GL_NATIVE_GRAPHICS_HANDLE_PGI = 0x1A202;
        public static UInt32 GL_NATIVE_GRAPHICS_BEGIN_HINT_PGI = 0x1A203;
        public static UInt32 GL_NATIVE_GRAPHICS_END_HINT_PGI = 0x1A204;
        public static UInt32 GL_ALWAYS_FAST_HINT_PGI = 0x1A20C;
        public static UInt32 GL_ALWAYS_SOFT_HINT_PGI = 0x1A20D;
        public static UInt32 GL_ALLOW_DRAW_OBJ_HINT_PGI = 0x1A20E;
        public static UInt32 GL_ALLOW_DRAW_WIN_HINT_PGI = 0x1A20F;
        public static UInt32 GL_ALLOW_DRAW_FRG_HINT_PGI = 0x1A210;
        public static UInt32 GL_ALLOW_DRAW_MEM_HINT_PGI = 0x1A211;
        public static UInt32 GL_STRICT_DEPTHFUNC_HINT_PGI = 0x1A216;
        public static UInt32 GL_STRICT_LIGHTING_HINT_PGI = 0x1A217;
        public static UInt32 GL_STRICT_SCISSOR_HINT_PGI = 0x1A218;
        public static UInt32 GL_FULL_STIPPLE_HINT_PGI = 0x1A219;
        public static UInt32 GL_CLIP_NEAR_HINT_PGI = 0x1A220;
        public static UInt32 GL_CLIP_FAR_HINT_PGI = 0x1A221;
        public static UInt32 GL_WIDE_LINE_HINT_PGI = 0x1A222;
        public static UInt32 GL_BACK_NORMALS_HINT_PGI = 0x1A223;
        #endregion

        #region Commands
        internal delegate void glHintPGIFunc(GLenum @target, GLint @mode);
        internal static glHintPGIFunc glHintPGIPtr;
        internal static void loadHintPGI()
        {
            try
            {
                glHintPGIPtr = (glHintPGIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glHintPGI"), typeof(glHintPGIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glHintPGI'.");
            }
        }
        public static void glHintPGI(GLenum @target, GLint @mode) => glHintPGIPtr(@target, @mode);
        #endregion
    }
}
