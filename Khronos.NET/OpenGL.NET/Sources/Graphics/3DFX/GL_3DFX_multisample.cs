using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_3DFX_multisample
    {
        #region Interop
        static GL_3DFX_multisample()
        {
            Console.WriteLine("Initalising GL_3DFX_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MULTISAMPLE_3DFX = 0x86B2;
        public static UInt32 GL_SAMPLE_BUFFERS_3DFX = 0x86B3;
        public static UInt32 GL_SAMPLES_3DFX = 0x86B4;
        public static UInt32 GL_MULTISAMPLE_BIT_3DFX = 0x20000000;
        #endregion

        #region Commands
        #endregion
    }
}
