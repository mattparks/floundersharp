using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_3DFX_tbuffer
    {
        #region Interop
        static GL_3DFX_tbuffer()
        {
            Console.WriteLine("Initalising GL_3DFX_tbuffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTbufferMask3DFX();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glTbufferMask3DFXFunc(GLuint @mask);
        internal static glTbufferMask3DFXFunc glTbufferMask3DFXPtr;
        internal static void loadTbufferMask3DFX()
        {
            try
            {
                glTbufferMask3DFXPtr = (glTbufferMask3DFXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTbufferMask3DFX"), typeof(glTbufferMask3DFXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTbufferMask3DFX'.");
            }
        }
        public static void glTbufferMask3DFX(GLuint @mask) => glTbufferMask3DFXPtr(@mask);
        #endregion
    }
}
