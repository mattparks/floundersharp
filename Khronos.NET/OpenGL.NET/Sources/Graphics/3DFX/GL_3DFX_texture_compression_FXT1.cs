using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_3DFX_texture_compression_FXT1
    {
        #region Interop
        static GL_3DFX_texture_compression_FXT1()
        {
            Console.WriteLine("Initalising GL_3DFX_texture_compression_FXT1 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_RGB_FXT1_3DFX = 0x86B0;
        public static UInt32 GL_COMPRESSED_RGBA_FXT1_3DFX = 0x86B1;
        #endregion

        #region Commands
        #endregion
    }
}
