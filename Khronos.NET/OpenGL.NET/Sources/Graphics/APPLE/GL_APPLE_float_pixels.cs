using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_float_pixels
    {
        #region Interop
        static GL_APPLE_float_pixels()
        {
            Console.WriteLine("Initalising GL_APPLE_float_pixels interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_HALF_APPLE = 0x140B;
        public static UInt32 GL_RGBA_FLOAT32_APPLE = 0x8814;
        public static UInt32 GL_RGB_FLOAT32_APPLE = 0x8815;
        public static UInt32 GL_ALPHA_FLOAT32_APPLE = 0x8816;
        public static UInt32 GL_INTENSITY_FLOAT32_APPLE = 0x8817;
        public static UInt32 GL_LUMINANCE_FLOAT32_APPLE = 0x8818;
        public static UInt32 GL_LUMINANCE_ALPHA_FLOAT32_APPLE = 0x8819;
        public static UInt32 GL_RGBA_FLOAT16_APPLE = 0x881A;
        public static UInt32 GL_RGB_FLOAT16_APPLE = 0x881B;
        public static UInt32 GL_ALPHA_FLOAT16_APPLE = 0x881C;
        public static UInt32 GL_INTENSITY_FLOAT16_APPLE = 0x881D;
        public static UInt32 GL_LUMINANCE_FLOAT16_APPLE = 0x881E;
        public static UInt32 GL_LUMINANCE_ALPHA_FLOAT16_APPLE = 0x881F;
        public static UInt32 GL_COLOR_FLOAT_APPLE = 0x8A0F;
        #endregion

        #region Commands
        #endregion
    }
}
