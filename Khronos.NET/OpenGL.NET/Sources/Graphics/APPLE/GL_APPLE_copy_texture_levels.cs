using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_copy_texture_levels
    {
        #region Interop
        static GL_APPLE_copy_texture_levels()
        {
            Console.WriteLine("Initalising GL_APPLE_copy_texture_levels interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCopyTextureLevelsAPPLE();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glCopyTextureLevelsAPPLEFunc(GLuint @destinationTexture, GLuint @sourceTexture, GLint @sourceBaseLevel, GLsizei @sourceLevelCount);
        internal static glCopyTextureLevelsAPPLEFunc glCopyTextureLevelsAPPLEPtr;
        internal static void loadCopyTextureLevelsAPPLE()
        {
            try
            {
                glCopyTextureLevelsAPPLEPtr = (glCopyTextureLevelsAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTextureLevelsAPPLE"), typeof(glCopyTextureLevelsAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTextureLevelsAPPLE'.");
            }
        }
        public static void glCopyTextureLevelsAPPLE(GLuint @destinationTexture, GLuint @sourceTexture, GLint @sourceBaseLevel, GLsizei @sourceLevelCount) => glCopyTextureLevelsAPPLEPtr(@destinationTexture, @sourceTexture, @sourceBaseLevel, @sourceLevelCount);
        #endregion
    }
}
