using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_framebuffer_multisample
    {
        #region Interop
        static GL_APPLE_framebuffer_multisample()
        {
            Console.WriteLine("Initalising GL_APPLE_framebuffer_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadRenderbufferStorageMultisampleAPPLE();
            loadResolveMultisampleFramebufferAPPLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RENDERBUFFER_SAMPLES_APPLE = 0x8CAB;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_APPLE = 0x8D56;
        public static UInt32 GL_MAX_SAMPLES_APPLE = 0x8D57;
        public static UInt32 GL_READ_FRAMEBUFFER_APPLE = 0x8CA8;
        public static UInt32 GL_DRAW_FRAMEBUFFER_APPLE = 0x8CA9;
        public static UInt32 GL_DRAW_FRAMEBUFFER_BINDING_APPLE = 0x8CA6;
        public static UInt32 GL_READ_FRAMEBUFFER_BINDING_APPLE = 0x8CAA;
        #endregion

        #region Commands
        internal delegate void glRenderbufferStorageMultisampleAPPLEFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageMultisampleAPPLEFunc glRenderbufferStorageMultisampleAPPLEPtr;
        internal static void loadRenderbufferStorageMultisampleAPPLE()
        {
            try
            {
                glRenderbufferStorageMultisampleAPPLEPtr = (glRenderbufferStorageMultisampleAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorageMultisampleAPPLE"), typeof(glRenderbufferStorageMultisampleAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorageMultisampleAPPLE'.");
            }
        }
        public static void glRenderbufferStorageMultisampleAPPLE(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStorageMultisampleAPPLEPtr(@target, @samples, @internalformat, @width, @height);

        internal delegate void glResolveMultisampleFramebufferAPPLEFunc();
        internal static glResolveMultisampleFramebufferAPPLEFunc glResolveMultisampleFramebufferAPPLEPtr;
        internal static void loadResolveMultisampleFramebufferAPPLE()
        {
            try
            {
                glResolveMultisampleFramebufferAPPLEPtr = (glResolveMultisampleFramebufferAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glResolveMultisampleFramebufferAPPLE"), typeof(glResolveMultisampleFramebufferAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glResolveMultisampleFramebufferAPPLE'.");
            }
        }
        public static void glResolveMultisampleFramebufferAPPLE() => glResolveMultisampleFramebufferAPPLEPtr();
        #endregion
    }
}
