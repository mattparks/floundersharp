using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_row_bytes
    {
        #region Interop
        static GL_APPLE_row_bytes()
        {
            Console.WriteLine("Initalising GL_APPLE_row_bytes interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_PACK_ROW_BYTES_APPLE = 0x8A15;
        public static UInt32 GL_UNPACK_ROW_BYTES_APPLE = 0x8A16;
        #endregion

        #region Commands
        #endregion
    }
}
