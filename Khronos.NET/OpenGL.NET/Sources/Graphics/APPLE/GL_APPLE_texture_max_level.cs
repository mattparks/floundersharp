using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_texture_max_level
    {
        #region Interop
        static GL_APPLE_texture_max_level()
        {
            Console.WriteLine("Initalising GL_APPLE_texture_max_level interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_MAX_LEVEL_APPLE = 0x813D;
        #endregion

        #region Commands
        #endregion
    }
}
