using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_fence
    {
        #region Interop
        static GL_APPLE_fence()
        {
            Console.WriteLine("Initalising GL_APPLE_fence interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGenFencesAPPLE();
            loadDeleteFencesAPPLE();
            loadSetFenceAPPLE();
            loadIsFenceAPPLE();
            loadTestFenceAPPLE();
            loadFinishFenceAPPLE();
            loadTestObjectAPPLE();
            loadFinishObjectAPPLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_DRAW_PIXELS_APPLE = 0x8A0A;
        public static UInt32 GL_FENCE_APPLE = 0x8A0B;
        #endregion

        #region Commands
        internal delegate void glGenFencesAPPLEFunc(GLsizei @n, GLuint * @fences);
        internal static glGenFencesAPPLEFunc glGenFencesAPPLEPtr;
        internal static void loadGenFencesAPPLE()
        {
            try
            {
                glGenFencesAPPLEPtr = (glGenFencesAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenFencesAPPLE"), typeof(glGenFencesAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenFencesAPPLE'.");
            }
        }
        public static void glGenFencesAPPLE(GLsizei @n, GLuint * @fences) => glGenFencesAPPLEPtr(@n, @fences);

        internal delegate void glDeleteFencesAPPLEFunc(GLsizei @n, const GLuint * @fences);
        internal static glDeleteFencesAPPLEFunc glDeleteFencesAPPLEPtr;
        internal static void loadDeleteFencesAPPLE()
        {
            try
            {
                glDeleteFencesAPPLEPtr = (glDeleteFencesAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteFencesAPPLE"), typeof(glDeleteFencesAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteFencesAPPLE'.");
            }
        }
        public static void glDeleteFencesAPPLE(GLsizei @n, const GLuint * @fences) => glDeleteFencesAPPLEPtr(@n, @fences);

        internal delegate void glSetFenceAPPLEFunc(GLuint @fence);
        internal static glSetFenceAPPLEFunc glSetFenceAPPLEPtr;
        internal static void loadSetFenceAPPLE()
        {
            try
            {
                glSetFenceAPPLEPtr = (glSetFenceAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSetFenceAPPLE"), typeof(glSetFenceAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSetFenceAPPLE'.");
            }
        }
        public static void glSetFenceAPPLE(GLuint @fence) => glSetFenceAPPLEPtr(@fence);

        internal delegate GLboolean glIsFenceAPPLEFunc(GLuint @fence);
        internal static glIsFenceAPPLEFunc glIsFenceAPPLEPtr;
        internal static void loadIsFenceAPPLE()
        {
            try
            {
                glIsFenceAPPLEPtr = (glIsFenceAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsFenceAPPLE"), typeof(glIsFenceAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsFenceAPPLE'.");
            }
        }
        public static GLboolean glIsFenceAPPLE(GLuint @fence) => glIsFenceAPPLEPtr(@fence);

        internal delegate GLboolean glTestFenceAPPLEFunc(GLuint @fence);
        internal static glTestFenceAPPLEFunc glTestFenceAPPLEPtr;
        internal static void loadTestFenceAPPLE()
        {
            try
            {
                glTestFenceAPPLEPtr = (glTestFenceAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTestFenceAPPLE"), typeof(glTestFenceAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTestFenceAPPLE'.");
            }
        }
        public static GLboolean glTestFenceAPPLE(GLuint @fence) => glTestFenceAPPLEPtr(@fence);

        internal delegate void glFinishFenceAPPLEFunc(GLuint @fence);
        internal static glFinishFenceAPPLEFunc glFinishFenceAPPLEPtr;
        internal static void loadFinishFenceAPPLE()
        {
            try
            {
                glFinishFenceAPPLEPtr = (glFinishFenceAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFinishFenceAPPLE"), typeof(glFinishFenceAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFinishFenceAPPLE'.");
            }
        }
        public static void glFinishFenceAPPLE(GLuint @fence) => glFinishFenceAPPLEPtr(@fence);

        internal delegate GLboolean glTestObjectAPPLEFunc(GLenum @object, GLuint @name);
        internal static glTestObjectAPPLEFunc glTestObjectAPPLEPtr;
        internal static void loadTestObjectAPPLE()
        {
            try
            {
                glTestObjectAPPLEPtr = (glTestObjectAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTestObjectAPPLE"), typeof(glTestObjectAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTestObjectAPPLE'.");
            }
        }
        public static GLboolean glTestObjectAPPLE(GLenum @object, GLuint @name) => glTestObjectAPPLEPtr(@object, @name);

        internal delegate void glFinishObjectAPPLEFunc(GLenum @object, GLint @name);
        internal static glFinishObjectAPPLEFunc glFinishObjectAPPLEPtr;
        internal static void loadFinishObjectAPPLE()
        {
            try
            {
                glFinishObjectAPPLEPtr = (glFinishObjectAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFinishObjectAPPLE"), typeof(glFinishObjectAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFinishObjectAPPLE'.");
            }
        }
        public static void glFinishObjectAPPLE(GLenum @object, GLint @name) => glFinishObjectAPPLEPtr(@object, @name);
        #endregion
    }
}
