using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_client_storage
    {
        #region Interop
        static GL_APPLE_client_storage()
        {
            Console.WriteLine("Initalising GL_APPLE_client_storage interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_UNPACK_CLIENT_STORAGE_APPLE = 0x85B2;
        #endregion

        #region Commands
        #endregion
    }
}
