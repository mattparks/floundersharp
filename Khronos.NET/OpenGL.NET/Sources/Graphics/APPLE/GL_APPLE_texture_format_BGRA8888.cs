using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_texture_format_BGRA8888
    {
        #region Interop
        static GL_APPLE_texture_format_BGRA8888()
        {
            Console.WriteLine("Initalising GL_APPLE_texture_format_BGRA8888 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_BGRA_EXT = 0x80E1;
        public static UInt32 GL_BGRA8_EXT = 0x93A1;
        #endregion

        #region Commands
        #endregion
    }
}
