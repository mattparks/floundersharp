using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_texture_packed_float
    {
        #region Interop
        static GL_APPLE_texture_packed_float()
        {
            Console.WriteLine("Initalising GL_APPLE_texture_packed_float interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_UNSIGNED_INT_10F_11F_11F_REV_APPLE = 0x8C3B;
        public static UInt32 GL_UNSIGNED_INT_5_9_9_9_REV_APPLE = 0x8C3E;
        public static UInt32 GL_R11F_G11F_B10F_APPLE = 0x8C3A;
        public static UInt32 GL_RGB9_E5_APPLE = 0x8C3D;
        #endregion

        #region Commands
        #endregion
    }
}
