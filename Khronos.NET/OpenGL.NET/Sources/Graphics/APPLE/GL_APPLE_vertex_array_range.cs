using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_vertex_array_range
    {
        #region Interop
        static GL_APPLE_vertex_array_range()
        {
            Console.WriteLine("Initalising GL_APPLE_vertex_array_range interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexArrayRangeAPPLE();
            loadFlushVertexArrayRangeAPPLE();
            loadVertexArrayParameteriAPPLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ARRAY_RANGE_APPLE = 0x851D;
        public static UInt32 GL_VERTEX_ARRAY_RANGE_LENGTH_APPLE = 0x851E;
        public static UInt32 GL_VERTEX_ARRAY_STORAGE_HINT_APPLE = 0x851F;
        public static UInt32 GL_VERTEX_ARRAY_RANGE_POINTER_APPLE = 0x8521;
        public static UInt32 GL_STORAGE_CLIENT_APPLE = 0x85B4;
        public static UInt32 GL_STORAGE_CACHED_APPLE = 0x85BE;
        public static UInt32 GL_STORAGE_SHARED_APPLE = 0x85BF;
        #endregion

        #region Commands
        internal delegate void glVertexArrayRangeAPPLEFunc(GLsizei @length, void * @pointer);
        internal static glVertexArrayRangeAPPLEFunc glVertexArrayRangeAPPLEPtr;
        internal static void loadVertexArrayRangeAPPLE()
        {
            try
            {
                glVertexArrayRangeAPPLEPtr = (glVertexArrayRangeAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayRangeAPPLE"), typeof(glVertexArrayRangeAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayRangeAPPLE'.");
            }
        }
        public static void glVertexArrayRangeAPPLE(GLsizei @length, void * @pointer) => glVertexArrayRangeAPPLEPtr(@length, @pointer);

        internal delegate void glFlushVertexArrayRangeAPPLEFunc(GLsizei @length, void * @pointer);
        internal static glFlushVertexArrayRangeAPPLEFunc glFlushVertexArrayRangeAPPLEPtr;
        internal static void loadFlushVertexArrayRangeAPPLE()
        {
            try
            {
                glFlushVertexArrayRangeAPPLEPtr = (glFlushVertexArrayRangeAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlushVertexArrayRangeAPPLE"), typeof(glFlushVertexArrayRangeAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlushVertexArrayRangeAPPLE'.");
            }
        }
        public static void glFlushVertexArrayRangeAPPLE(GLsizei @length, void * @pointer) => glFlushVertexArrayRangeAPPLEPtr(@length, @pointer);

        internal delegate void glVertexArrayParameteriAPPLEFunc(GLenum @pname, GLint @param);
        internal static glVertexArrayParameteriAPPLEFunc glVertexArrayParameteriAPPLEPtr;
        internal static void loadVertexArrayParameteriAPPLE()
        {
            try
            {
                glVertexArrayParameteriAPPLEPtr = (glVertexArrayParameteriAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayParameteriAPPLE"), typeof(glVertexArrayParameteriAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayParameteriAPPLE'.");
            }
        }
        public static void glVertexArrayParameteriAPPLE(GLenum @pname, GLint @param) => glVertexArrayParameteriAPPLEPtr(@pname, @param);
        #endregion
    }
}
