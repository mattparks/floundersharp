using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_vertex_program_evaluators
    {
        #region Interop
        static GL_APPLE_vertex_program_evaluators()
        {
            Console.WriteLine("Initalising GL_APPLE_vertex_program_evaluators interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadEnableVertexAttribAPPLE();
            loadDisableVertexAttribAPPLE();
            loadIsVertexAttribEnabledAPPLE();
            loadMapVertexAttrib1dAPPLE();
            loadMapVertexAttrib1fAPPLE();
            loadMapVertexAttrib2dAPPLE();
            loadMapVertexAttrib2fAPPLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ATTRIB_MAP1_APPLE = 0x8A00;
        public static UInt32 GL_VERTEX_ATTRIB_MAP2_APPLE = 0x8A01;
        public static UInt32 GL_VERTEX_ATTRIB_MAP1_SIZE_APPLE = 0x8A02;
        public static UInt32 GL_VERTEX_ATTRIB_MAP1_COEFF_APPLE = 0x8A03;
        public static UInt32 GL_VERTEX_ATTRIB_MAP1_ORDER_APPLE = 0x8A04;
        public static UInt32 GL_VERTEX_ATTRIB_MAP1_DOMAIN_APPLE = 0x8A05;
        public static UInt32 GL_VERTEX_ATTRIB_MAP2_SIZE_APPLE = 0x8A06;
        public static UInt32 GL_VERTEX_ATTRIB_MAP2_COEFF_APPLE = 0x8A07;
        public static UInt32 GL_VERTEX_ATTRIB_MAP2_ORDER_APPLE = 0x8A08;
        public static UInt32 GL_VERTEX_ATTRIB_MAP2_DOMAIN_APPLE = 0x8A09;
        #endregion

        #region Commands
        internal delegate void glEnableVertexAttribAPPLEFunc(GLuint @index, GLenum @pname);
        internal static glEnableVertexAttribAPPLEFunc glEnableVertexAttribAPPLEPtr;
        internal static void loadEnableVertexAttribAPPLE()
        {
            try
            {
                glEnableVertexAttribAPPLEPtr = (glEnableVertexAttribAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableVertexAttribAPPLE"), typeof(glEnableVertexAttribAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableVertexAttribAPPLE'.");
            }
        }
        public static void glEnableVertexAttribAPPLE(GLuint @index, GLenum @pname) => glEnableVertexAttribAPPLEPtr(@index, @pname);

        internal delegate void glDisableVertexAttribAPPLEFunc(GLuint @index, GLenum @pname);
        internal static glDisableVertexAttribAPPLEFunc glDisableVertexAttribAPPLEPtr;
        internal static void loadDisableVertexAttribAPPLE()
        {
            try
            {
                glDisableVertexAttribAPPLEPtr = (glDisableVertexAttribAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableVertexAttribAPPLE"), typeof(glDisableVertexAttribAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableVertexAttribAPPLE'.");
            }
        }
        public static void glDisableVertexAttribAPPLE(GLuint @index, GLenum @pname) => glDisableVertexAttribAPPLEPtr(@index, @pname);

        internal delegate GLboolean glIsVertexAttribEnabledAPPLEFunc(GLuint @index, GLenum @pname);
        internal static glIsVertexAttribEnabledAPPLEFunc glIsVertexAttribEnabledAPPLEPtr;
        internal static void loadIsVertexAttribEnabledAPPLE()
        {
            try
            {
                glIsVertexAttribEnabledAPPLEPtr = (glIsVertexAttribEnabledAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsVertexAttribEnabledAPPLE"), typeof(glIsVertexAttribEnabledAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsVertexAttribEnabledAPPLE'.");
            }
        }
        public static GLboolean glIsVertexAttribEnabledAPPLE(GLuint @index, GLenum @pname) => glIsVertexAttribEnabledAPPLEPtr(@index, @pname);

        internal delegate void glMapVertexAttrib1dAPPLEFunc(GLuint @index, GLuint @size, GLdouble @u1, GLdouble @u2, GLint @stride, GLint @order, const GLdouble * @points);
        internal static glMapVertexAttrib1dAPPLEFunc glMapVertexAttrib1dAPPLEPtr;
        internal static void loadMapVertexAttrib1dAPPLE()
        {
            try
            {
                glMapVertexAttrib1dAPPLEPtr = (glMapVertexAttrib1dAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapVertexAttrib1dAPPLE"), typeof(glMapVertexAttrib1dAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapVertexAttrib1dAPPLE'.");
            }
        }
        public static void glMapVertexAttrib1dAPPLE(GLuint @index, GLuint @size, GLdouble @u1, GLdouble @u2, GLint @stride, GLint @order, const GLdouble * @points) => glMapVertexAttrib1dAPPLEPtr(@index, @size, @u1, @u2, @stride, @order, @points);

        internal delegate void glMapVertexAttrib1fAPPLEFunc(GLuint @index, GLuint @size, GLfloat @u1, GLfloat @u2, GLint @stride, GLint @order, const GLfloat * @points);
        internal static glMapVertexAttrib1fAPPLEFunc glMapVertexAttrib1fAPPLEPtr;
        internal static void loadMapVertexAttrib1fAPPLE()
        {
            try
            {
                glMapVertexAttrib1fAPPLEPtr = (glMapVertexAttrib1fAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapVertexAttrib1fAPPLE"), typeof(glMapVertexAttrib1fAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapVertexAttrib1fAPPLE'.");
            }
        }
        public static void glMapVertexAttrib1fAPPLE(GLuint @index, GLuint @size, GLfloat @u1, GLfloat @u2, GLint @stride, GLint @order, const GLfloat * @points) => glMapVertexAttrib1fAPPLEPtr(@index, @size, @u1, @u2, @stride, @order, @points);

        internal delegate void glMapVertexAttrib2dAPPLEFunc(GLuint @index, GLuint @size, GLdouble @u1, GLdouble @u2, GLint @ustride, GLint @uorder, GLdouble @v1, GLdouble @v2, GLint @vstride, GLint @vorder, const GLdouble * @points);
        internal static glMapVertexAttrib2dAPPLEFunc glMapVertexAttrib2dAPPLEPtr;
        internal static void loadMapVertexAttrib2dAPPLE()
        {
            try
            {
                glMapVertexAttrib2dAPPLEPtr = (glMapVertexAttrib2dAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapVertexAttrib2dAPPLE"), typeof(glMapVertexAttrib2dAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapVertexAttrib2dAPPLE'.");
            }
        }
        public static void glMapVertexAttrib2dAPPLE(GLuint @index, GLuint @size, GLdouble @u1, GLdouble @u2, GLint @ustride, GLint @uorder, GLdouble @v1, GLdouble @v2, GLint @vstride, GLint @vorder, const GLdouble * @points) => glMapVertexAttrib2dAPPLEPtr(@index, @size, @u1, @u2, @ustride, @uorder, @v1, @v2, @vstride, @vorder, @points);

        internal delegate void glMapVertexAttrib2fAPPLEFunc(GLuint @index, GLuint @size, GLfloat @u1, GLfloat @u2, GLint @ustride, GLint @uorder, GLfloat @v1, GLfloat @v2, GLint @vstride, GLint @vorder, const GLfloat * @points);
        internal static glMapVertexAttrib2fAPPLEFunc glMapVertexAttrib2fAPPLEPtr;
        internal static void loadMapVertexAttrib2fAPPLE()
        {
            try
            {
                glMapVertexAttrib2fAPPLEPtr = (glMapVertexAttrib2fAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapVertexAttrib2fAPPLE"), typeof(glMapVertexAttrib2fAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapVertexAttrib2fAPPLE'.");
            }
        }
        public static void glMapVertexAttrib2fAPPLE(GLuint @index, GLuint @size, GLfloat @u1, GLfloat @u2, GLint @ustride, GLint @uorder, GLfloat @v1, GLfloat @v2, GLint @vstride, GLint @vorder, const GLfloat * @points) => glMapVertexAttrib2fAPPLEPtr(@index, @size, @u1, @u2, @ustride, @uorder, @v1, @v2, @vstride, @vorder, @points);
        #endregion
    }
}
