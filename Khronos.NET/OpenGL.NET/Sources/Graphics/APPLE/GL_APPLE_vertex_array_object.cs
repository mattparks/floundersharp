using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_vertex_array_object
    {
        #region Interop
        static GL_APPLE_vertex_array_object()
        {
            Console.WriteLine("Initalising GL_APPLE_vertex_array_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindVertexArrayAPPLE();
            loadDeleteVertexArraysAPPLE();
            loadGenVertexArraysAPPLE();
            loadIsVertexArrayAPPLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ARRAY_BINDING_APPLE = 0x85B5;
        #endregion

        #region Commands
        internal delegate void glBindVertexArrayAPPLEFunc(GLuint @array);
        internal static glBindVertexArrayAPPLEFunc glBindVertexArrayAPPLEPtr;
        internal static void loadBindVertexArrayAPPLE()
        {
            try
            {
                glBindVertexArrayAPPLEPtr = (glBindVertexArrayAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVertexArrayAPPLE"), typeof(glBindVertexArrayAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVertexArrayAPPLE'.");
            }
        }
        public static void glBindVertexArrayAPPLE(GLuint @array) => glBindVertexArrayAPPLEPtr(@array);

        internal delegate void glDeleteVertexArraysAPPLEFunc(GLsizei @n, const GLuint * @arrays);
        internal static glDeleteVertexArraysAPPLEFunc glDeleteVertexArraysAPPLEPtr;
        internal static void loadDeleteVertexArraysAPPLE()
        {
            try
            {
                glDeleteVertexArraysAPPLEPtr = (glDeleteVertexArraysAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteVertexArraysAPPLE"), typeof(glDeleteVertexArraysAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteVertexArraysAPPLE'.");
            }
        }
        public static void glDeleteVertexArraysAPPLE(GLsizei @n, const GLuint * @arrays) => glDeleteVertexArraysAPPLEPtr(@n, @arrays);

        internal delegate void glGenVertexArraysAPPLEFunc(GLsizei @n, GLuint * @arrays);
        internal static glGenVertexArraysAPPLEFunc glGenVertexArraysAPPLEPtr;
        internal static void loadGenVertexArraysAPPLE()
        {
            try
            {
                glGenVertexArraysAPPLEPtr = (glGenVertexArraysAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenVertexArraysAPPLE"), typeof(glGenVertexArraysAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenVertexArraysAPPLE'.");
            }
        }
        public static void glGenVertexArraysAPPLE(GLsizei @n, GLuint * @arrays) => glGenVertexArraysAPPLEPtr(@n, @arrays);

        internal delegate GLboolean glIsVertexArrayAPPLEFunc(GLuint @array);
        internal static glIsVertexArrayAPPLEFunc glIsVertexArrayAPPLEPtr;
        internal static void loadIsVertexArrayAPPLE()
        {
            try
            {
                glIsVertexArrayAPPLEPtr = (glIsVertexArrayAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsVertexArrayAPPLE"), typeof(glIsVertexArrayAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsVertexArrayAPPLE'.");
            }
        }
        public static GLboolean glIsVertexArrayAPPLE(GLuint @array) => glIsVertexArrayAPPLEPtr(@array);
        #endregion
    }
}
