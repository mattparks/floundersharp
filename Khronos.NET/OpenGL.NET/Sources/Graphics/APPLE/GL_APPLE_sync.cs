using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_sync
    {
        #region Interop
        static GL_APPLE_sync()
        {
            Console.WriteLine("Initalising GL_APPLE_sync interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFenceSyncAPPLE();
            loadIsSyncAPPLE();
            loadDeleteSyncAPPLE();
            loadClientWaitSyncAPPLE();
            loadWaitSyncAPPLE();
            loadGetInteger64vAPPLE();
            loadGetSyncivAPPLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SYNC_OBJECT_APPLE = 0x8A53;
        public static UInt32 GL_MAX_SERVER_WAIT_TIMEOUT_APPLE = 0x9111;
        public static UInt32 GL_OBJECT_TYPE_APPLE = 0x9112;
        public static UInt32 GL_SYNC_CONDITION_APPLE = 0x9113;
        public static UInt32 GL_SYNC_STATUS_APPLE = 0x9114;
        public static UInt32 GL_SYNC_FLAGS_APPLE = 0x9115;
        public static UInt32 GL_SYNC_FENCE_APPLE = 0x9116;
        public static UInt32 GL_SYNC_GPU_COMMANDS_COMPLETE_APPLE = 0x9117;
        public static UInt32 GL_UNSIGNALED_APPLE = 0x9118;
        public static UInt32 GL_SIGNALED_APPLE = 0x9119;
        public static UInt32 GL_ALREADY_SIGNALED_APPLE = 0x911A;
        public static UInt32 GL_TIMEOUT_EXPIRED_APPLE = 0x911B;
        public static UInt32 GL_CONDITION_SATISFIED_APPLE = 0x911C;
        public static UInt32 GL_WAIT_FAILED_APPLE = 0x911D;
        public static UInt32 GL_SYNC_FLUSH_COMMANDS_BIT_APPLE = 0x00000001;
        public static UInt64 GL_TIMEOUT_IGNORED_APPLE = 0xFFFFFFFFFFFFFFFF;
        #endregion

        #region Commands
        internal delegate GLsync glFenceSyncAPPLEFunc(GLenum @condition, GLbitfield @flags);
        internal static glFenceSyncAPPLEFunc glFenceSyncAPPLEPtr;
        internal static void loadFenceSyncAPPLE()
        {
            try
            {
                glFenceSyncAPPLEPtr = (glFenceSyncAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFenceSyncAPPLE"), typeof(glFenceSyncAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFenceSyncAPPLE'.");
            }
        }
        public static GLsync glFenceSyncAPPLE(GLenum @condition, GLbitfield @flags) => glFenceSyncAPPLEPtr(@condition, @flags);

        internal delegate GLboolean glIsSyncAPPLEFunc(GLsync @sync);
        internal static glIsSyncAPPLEFunc glIsSyncAPPLEPtr;
        internal static void loadIsSyncAPPLE()
        {
            try
            {
                glIsSyncAPPLEPtr = (glIsSyncAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsSyncAPPLE"), typeof(glIsSyncAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsSyncAPPLE'.");
            }
        }
        public static GLboolean glIsSyncAPPLE(GLsync @sync) => glIsSyncAPPLEPtr(@sync);

        internal delegate void glDeleteSyncAPPLEFunc(GLsync @sync);
        internal static glDeleteSyncAPPLEFunc glDeleteSyncAPPLEPtr;
        internal static void loadDeleteSyncAPPLE()
        {
            try
            {
                glDeleteSyncAPPLEPtr = (glDeleteSyncAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteSyncAPPLE"), typeof(glDeleteSyncAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteSyncAPPLE'.");
            }
        }
        public static void glDeleteSyncAPPLE(GLsync @sync) => glDeleteSyncAPPLEPtr(@sync);

        internal delegate GLenum glClientWaitSyncAPPLEFunc(GLsync @sync, GLbitfield @flags, GLuint64 @timeout);
        internal static glClientWaitSyncAPPLEFunc glClientWaitSyncAPPLEPtr;
        internal static void loadClientWaitSyncAPPLE()
        {
            try
            {
                glClientWaitSyncAPPLEPtr = (glClientWaitSyncAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClientWaitSyncAPPLE"), typeof(glClientWaitSyncAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClientWaitSyncAPPLE'.");
            }
        }
        public static GLenum glClientWaitSyncAPPLE(GLsync @sync, GLbitfield @flags, GLuint64 @timeout) => glClientWaitSyncAPPLEPtr(@sync, @flags, @timeout);

        internal delegate void glWaitSyncAPPLEFunc(GLsync @sync, GLbitfield @flags, GLuint64 @timeout);
        internal static glWaitSyncAPPLEFunc glWaitSyncAPPLEPtr;
        internal static void loadWaitSyncAPPLE()
        {
            try
            {
                glWaitSyncAPPLEPtr = (glWaitSyncAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWaitSyncAPPLE"), typeof(glWaitSyncAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWaitSyncAPPLE'.");
            }
        }
        public static void glWaitSyncAPPLE(GLsync @sync, GLbitfield @flags, GLuint64 @timeout) => glWaitSyncAPPLEPtr(@sync, @flags, @timeout);

        internal delegate void glGetInteger64vAPPLEFunc(GLenum @pname, GLint64 * @params);
        internal static glGetInteger64vAPPLEFunc glGetInteger64vAPPLEPtr;
        internal static void loadGetInteger64vAPPLE()
        {
            try
            {
                glGetInteger64vAPPLEPtr = (glGetInteger64vAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInteger64vAPPLE"), typeof(glGetInteger64vAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInteger64vAPPLE'.");
            }
        }
        public static void glGetInteger64vAPPLE(GLenum @pname, GLint64 * @params) => glGetInteger64vAPPLEPtr(@pname, @params);

        internal delegate void glGetSyncivAPPLEFunc(GLsync @sync, GLenum @pname, GLsizei @bufSize, GLsizei * @length, GLint * @values);
        internal static glGetSyncivAPPLEFunc glGetSyncivAPPLEPtr;
        internal static void loadGetSyncivAPPLE()
        {
            try
            {
                glGetSyncivAPPLEPtr = (glGetSyncivAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSyncivAPPLE"), typeof(glGetSyncivAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSyncivAPPLE'.");
            }
        }
        public static void glGetSyncivAPPLE(GLsync @sync, GLenum @pname, GLsizei @bufSize, GLsizei * @length, GLint * @values) => glGetSyncivAPPLEPtr(@sync, @pname, @bufSize, @length, @values);
        #endregion
    }
}
