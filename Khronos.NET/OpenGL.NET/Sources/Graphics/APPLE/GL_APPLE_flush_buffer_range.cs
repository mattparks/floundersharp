using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_flush_buffer_range
    {
        #region Interop
        static GL_APPLE_flush_buffer_range()
        {
            Console.WriteLine("Initalising GL_APPLE_flush_buffer_range interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBufferParameteriAPPLE();
            loadFlushMappedBufferRangeAPPLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BUFFER_SERIALIZED_MODIFY_APPLE = 0x8A12;
        public static UInt32 GL_BUFFER_FLUSHING_UNMAP_APPLE = 0x8A13;
        #endregion

        #region Commands
        internal delegate void glBufferParameteriAPPLEFunc(GLenum @target, GLenum @pname, GLint @param);
        internal static glBufferParameteriAPPLEFunc glBufferParameteriAPPLEPtr;
        internal static void loadBufferParameteriAPPLE()
        {
            try
            {
                glBufferParameteriAPPLEPtr = (glBufferParameteriAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferParameteriAPPLE"), typeof(glBufferParameteriAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferParameteriAPPLE'.");
            }
        }
        public static void glBufferParameteriAPPLE(GLenum @target, GLenum @pname, GLint @param) => glBufferParameteriAPPLEPtr(@target, @pname, @param);

        internal delegate void glFlushMappedBufferRangeAPPLEFunc(GLenum @target, GLintptr @offset, GLsizeiptr @size);
        internal static glFlushMappedBufferRangeAPPLEFunc glFlushMappedBufferRangeAPPLEPtr;
        internal static void loadFlushMappedBufferRangeAPPLE()
        {
            try
            {
                glFlushMappedBufferRangeAPPLEPtr = (glFlushMappedBufferRangeAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlushMappedBufferRangeAPPLE"), typeof(glFlushMappedBufferRangeAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlushMappedBufferRangeAPPLE'.");
            }
        }
        public static void glFlushMappedBufferRangeAPPLE(GLenum @target, GLintptr @offset, GLsizeiptr @size) => glFlushMappedBufferRangeAPPLEPtr(@target, @offset, @size);
        #endregion
    }
}
