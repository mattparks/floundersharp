using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_ycbcr_422
    {
        #region Interop
        static GL_APPLE_ycbcr_422()
        {
            Console.WriteLine("Initalising GL_APPLE_ycbcr_422 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_YCBCR_422_APPLE = 0x85B9;
        public static UInt32 GL_UNSIGNED_SHORT_8_8_APPLE = 0x85BA;
        public static UInt32 GL_UNSIGNED_SHORT_8_8_REV_APPLE = 0x85BB;
        #endregion

        #region Commands
        #endregion
    }
}
