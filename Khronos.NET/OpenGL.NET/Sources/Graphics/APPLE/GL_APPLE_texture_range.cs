using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_texture_range
    {
        #region Interop
        static GL_APPLE_texture_range()
        {
            Console.WriteLine("Initalising GL_APPLE_texture_range interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTextureRangeAPPLE();
            loadGetTexParameterPointervAPPLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_RANGE_LENGTH_APPLE = 0x85B7;
        public static UInt32 GL_TEXTURE_RANGE_POINTER_APPLE = 0x85B8;
        public static UInt32 GL_TEXTURE_STORAGE_HINT_APPLE = 0x85BC;
        public static UInt32 GL_STORAGE_PRIVATE_APPLE = 0x85BD;
        public static UInt32 GL_STORAGE_CACHED_APPLE = 0x85BE;
        public static UInt32 GL_STORAGE_SHARED_APPLE = 0x85BF;
        #endregion

        #region Commands
        internal delegate void glTextureRangeAPPLEFunc(GLenum @target, GLsizei @length, const void * @pointer);
        internal static glTextureRangeAPPLEFunc glTextureRangeAPPLEPtr;
        internal static void loadTextureRangeAPPLE()
        {
            try
            {
                glTextureRangeAPPLEPtr = (glTextureRangeAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureRangeAPPLE"), typeof(glTextureRangeAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureRangeAPPLE'.");
            }
        }
        public static void glTextureRangeAPPLE(GLenum @target, GLsizei @length, const void * @pointer) => glTextureRangeAPPLEPtr(@target, @length, @pointer);

        internal delegate void glGetTexParameterPointervAPPLEFunc(GLenum @target, GLenum @pname, void ** @params);
        internal static glGetTexParameterPointervAPPLEFunc glGetTexParameterPointervAPPLEPtr;
        internal static void loadGetTexParameterPointervAPPLE()
        {
            try
            {
                glGetTexParameterPointervAPPLEPtr = (glGetTexParameterPointervAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterPointervAPPLE"), typeof(glGetTexParameterPointervAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterPointervAPPLE'.");
            }
        }
        public static void glGetTexParameterPointervAPPLE(GLenum @target, GLenum @pname, void ** @params) => glGetTexParameterPointervAPPLEPtr(@target, @pname, @params);
        #endregion
    }
}
