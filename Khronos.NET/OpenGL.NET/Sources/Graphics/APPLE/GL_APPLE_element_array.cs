using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_element_array
    {
        #region Interop
        static GL_APPLE_element_array()
        {
            Console.WriteLine("Initalising GL_APPLE_element_array interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadElementPointerAPPLE();
            loadDrawElementArrayAPPLE();
            loadDrawRangeElementArrayAPPLE();
            loadMultiDrawElementArrayAPPLE();
            loadMultiDrawRangeElementArrayAPPLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_ELEMENT_ARRAY_APPLE = 0x8A0C;
        public static UInt32 GL_ELEMENT_ARRAY_TYPE_APPLE = 0x8A0D;
        public static UInt32 GL_ELEMENT_ARRAY_POINTER_APPLE = 0x8A0E;
        #endregion

        #region Commands
        internal delegate void glElementPointerAPPLEFunc(GLenum @type, const void * @pointer);
        internal static glElementPointerAPPLEFunc glElementPointerAPPLEPtr;
        internal static void loadElementPointerAPPLE()
        {
            try
            {
                glElementPointerAPPLEPtr = (glElementPointerAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glElementPointerAPPLE"), typeof(glElementPointerAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glElementPointerAPPLE'.");
            }
        }
        public static void glElementPointerAPPLE(GLenum @type, const void * @pointer) => glElementPointerAPPLEPtr(@type, @pointer);

        internal delegate void glDrawElementArrayAPPLEFunc(GLenum @mode, GLint @first, GLsizei @count);
        internal static glDrawElementArrayAPPLEFunc glDrawElementArrayAPPLEPtr;
        internal static void loadDrawElementArrayAPPLE()
        {
            try
            {
                glDrawElementArrayAPPLEPtr = (glDrawElementArrayAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementArrayAPPLE"), typeof(glDrawElementArrayAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementArrayAPPLE'.");
            }
        }
        public static void glDrawElementArrayAPPLE(GLenum @mode, GLint @first, GLsizei @count) => glDrawElementArrayAPPLEPtr(@mode, @first, @count);

        internal delegate void glDrawRangeElementArrayAPPLEFunc(GLenum @mode, GLuint @start, GLuint @end, GLint @first, GLsizei @count);
        internal static glDrawRangeElementArrayAPPLEFunc glDrawRangeElementArrayAPPLEPtr;
        internal static void loadDrawRangeElementArrayAPPLE()
        {
            try
            {
                glDrawRangeElementArrayAPPLEPtr = (glDrawRangeElementArrayAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawRangeElementArrayAPPLE"), typeof(glDrawRangeElementArrayAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawRangeElementArrayAPPLE'.");
            }
        }
        public static void glDrawRangeElementArrayAPPLE(GLenum @mode, GLuint @start, GLuint @end, GLint @first, GLsizei @count) => glDrawRangeElementArrayAPPLEPtr(@mode, @start, @end, @first, @count);

        internal delegate void glMultiDrawElementArrayAPPLEFunc(GLenum @mode, const GLint * @first, const GLsizei * @count, GLsizei @primcount);
        internal static glMultiDrawElementArrayAPPLEFunc glMultiDrawElementArrayAPPLEPtr;
        internal static void loadMultiDrawElementArrayAPPLE()
        {
            try
            {
                glMultiDrawElementArrayAPPLEPtr = (glMultiDrawElementArrayAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementArrayAPPLE"), typeof(glMultiDrawElementArrayAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementArrayAPPLE'.");
            }
        }
        public static void glMultiDrawElementArrayAPPLE(GLenum @mode, const GLint * @first, const GLsizei * @count, GLsizei @primcount) => glMultiDrawElementArrayAPPLEPtr(@mode, @first, @count, @primcount);

        internal delegate void glMultiDrawRangeElementArrayAPPLEFunc(GLenum @mode, GLuint @start, GLuint @end, const GLint * @first, const GLsizei * @count, GLsizei @primcount);
        internal static glMultiDrawRangeElementArrayAPPLEFunc glMultiDrawRangeElementArrayAPPLEPtr;
        internal static void loadMultiDrawRangeElementArrayAPPLE()
        {
            try
            {
                glMultiDrawRangeElementArrayAPPLEPtr = (glMultiDrawRangeElementArrayAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawRangeElementArrayAPPLE"), typeof(glMultiDrawRangeElementArrayAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawRangeElementArrayAPPLE'.");
            }
        }
        public static void glMultiDrawRangeElementArrayAPPLE(GLenum @mode, GLuint @start, GLuint @end, const GLint * @first, const GLsizei * @count, GLsizei @primcount) => glMultiDrawRangeElementArrayAPPLEPtr(@mode, @start, @end, @first, @count, @primcount);
        #endregion
    }
}
