using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_rgb_422
    {
        #region Interop
        static GL_APPLE_rgb_422()
        {
            Console.WriteLine("Initalising GL_APPLE_rgb_422 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RGB_422_APPLE = 0x8A1F;
        public static UInt32 GL_UNSIGNED_SHORT_8_8_APPLE = 0x85BA;
        public static UInt32 GL_UNSIGNED_SHORT_8_8_REV_APPLE = 0x85BB;
        public static UInt32 GL_RGB_RAW_422_APPLE = 0x8A51;
        #endregion

        #region Commands
        #endregion
    }
}
