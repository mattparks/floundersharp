using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_object_purgeable
    {
        #region Interop
        static GL_APPLE_object_purgeable()
        {
            Console.WriteLine("Initalising GL_APPLE_object_purgeable interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadObjectPurgeableAPPLE();
            loadObjectUnpurgeableAPPLE();
            loadGetObjectParameterivAPPLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BUFFER_OBJECT_APPLE = 0x85B3;
        public static UInt32 GL_RELEASED_APPLE = 0x8A19;
        public static UInt32 GL_VOLATILE_APPLE = 0x8A1A;
        public static UInt32 GL_RETAINED_APPLE = 0x8A1B;
        public static UInt32 GL_UNDEFINED_APPLE = 0x8A1C;
        public static UInt32 GL_PURGEABLE_APPLE = 0x8A1D;
        #endregion

        #region Commands
        internal delegate GLenum glObjectPurgeableAPPLEFunc(GLenum @objectType, GLuint @name, GLenum @option);
        internal static glObjectPurgeableAPPLEFunc glObjectPurgeableAPPLEPtr;
        internal static void loadObjectPurgeableAPPLE()
        {
            try
            {
                glObjectPurgeableAPPLEPtr = (glObjectPurgeableAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glObjectPurgeableAPPLE"), typeof(glObjectPurgeableAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glObjectPurgeableAPPLE'.");
            }
        }
        public static GLenum glObjectPurgeableAPPLE(GLenum @objectType, GLuint @name, GLenum @option) => glObjectPurgeableAPPLEPtr(@objectType, @name, @option);

        internal delegate GLenum glObjectUnpurgeableAPPLEFunc(GLenum @objectType, GLuint @name, GLenum @option);
        internal static glObjectUnpurgeableAPPLEFunc glObjectUnpurgeableAPPLEPtr;
        internal static void loadObjectUnpurgeableAPPLE()
        {
            try
            {
                glObjectUnpurgeableAPPLEPtr = (glObjectUnpurgeableAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glObjectUnpurgeableAPPLE"), typeof(glObjectUnpurgeableAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glObjectUnpurgeableAPPLE'.");
            }
        }
        public static GLenum glObjectUnpurgeableAPPLE(GLenum @objectType, GLuint @name, GLenum @option) => glObjectUnpurgeableAPPLEPtr(@objectType, @name, @option);

        internal delegate void glGetObjectParameterivAPPLEFunc(GLenum @objectType, GLuint @name, GLenum @pname, GLint * @params);
        internal static glGetObjectParameterivAPPLEFunc glGetObjectParameterivAPPLEPtr;
        internal static void loadGetObjectParameterivAPPLE()
        {
            try
            {
                glGetObjectParameterivAPPLEPtr = (glGetObjectParameterivAPPLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectParameterivAPPLE"), typeof(glGetObjectParameterivAPPLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectParameterivAPPLE'.");
            }
        }
        public static void glGetObjectParameterivAPPLE(GLenum @objectType, GLuint @name, GLenum @pname, GLint * @params) => glGetObjectParameterivAPPLEPtr(@objectType, @name, @pname, @params);
        #endregion
    }
}
