using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_APPLE_clip_distance
    {
        #region Interop
        static GL_APPLE_clip_distance()
        {
            Console.WriteLine("Initalising GL_APPLE_clip_distance interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_CLIP_DISTANCES_APPLE = 0x0D32;
        public static UInt32 GL_CLIP_DISTANCE0_APPLE = 0x3000;
        public static UInt32 GL_CLIP_DISTANCE1_APPLE = 0x3001;
        public static UInt32 GL_CLIP_DISTANCE2_APPLE = 0x3002;
        public static UInt32 GL_CLIP_DISTANCE3_APPLE = 0x3003;
        public static UInt32 GL_CLIP_DISTANCE4_APPLE = 0x3004;
        public static UInt32 GL_CLIP_DISTANCE5_APPLE = 0x3005;
        public static UInt32 GL_CLIP_DISTANCE6_APPLE = 0x3006;
        public static UInt32 GL_CLIP_DISTANCE7_APPLE = 0x3007;
        #endregion

        #region Commands
        #endregion
    }
}
