using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_element_array
    {
        #region Interop
        static GL_ATI_element_array()
        {
            Console.WriteLine("Initalising GL_ATI_element_array interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadElementPointerATI();
            loadDrawElementArrayATI();
            loadDrawRangeElementArrayATI();
        }
        #endregion

        #region Enums
        public static UInt32 GL_ELEMENT_ARRAY_ATI = 0x8768;
        public static UInt32 GL_ELEMENT_ARRAY_TYPE_ATI = 0x8769;
        public static UInt32 GL_ELEMENT_ARRAY_POINTER_ATI = 0x876A;
        #endregion

        #region Commands
        internal delegate void glElementPointerATIFunc(GLenum @type, const void * @pointer);
        internal static glElementPointerATIFunc glElementPointerATIPtr;
        internal static void loadElementPointerATI()
        {
            try
            {
                glElementPointerATIPtr = (glElementPointerATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glElementPointerATI"), typeof(glElementPointerATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glElementPointerATI'.");
            }
        }
        public static void glElementPointerATI(GLenum @type, const void * @pointer) => glElementPointerATIPtr(@type, @pointer);

        internal delegate void glDrawElementArrayATIFunc(GLenum @mode, GLsizei @count);
        internal static glDrawElementArrayATIFunc glDrawElementArrayATIPtr;
        internal static void loadDrawElementArrayATI()
        {
            try
            {
                glDrawElementArrayATIPtr = (glDrawElementArrayATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementArrayATI"), typeof(glDrawElementArrayATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementArrayATI'.");
            }
        }
        public static void glDrawElementArrayATI(GLenum @mode, GLsizei @count) => glDrawElementArrayATIPtr(@mode, @count);

        internal delegate void glDrawRangeElementArrayATIFunc(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count);
        internal static glDrawRangeElementArrayATIFunc glDrawRangeElementArrayATIPtr;
        internal static void loadDrawRangeElementArrayATI()
        {
            try
            {
                glDrawRangeElementArrayATIPtr = (glDrawRangeElementArrayATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawRangeElementArrayATI"), typeof(glDrawRangeElementArrayATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawRangeElementArrayATI'.");
            }
        }
        public static void glDrawRangeElementArrayATI(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count) => glDrawRangeElementArrayATIPtr(@mode, @start, @end, @count);
        #endregion
    }
}
