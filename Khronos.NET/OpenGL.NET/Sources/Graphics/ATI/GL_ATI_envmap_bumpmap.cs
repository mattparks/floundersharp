using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_envmap_bumpmap
    {
        #region Interop
        static GL_ATI_envmap_bumpmap()
        {
            Console.WriteLine("Initalising GL_ATI_envmap_bumpmap interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexBumpParameterivATI();
            loadTexBumpParameterfvATI();
            loadGetTexBumpParameterivATI();
            loadGetTexBumpParameterfvATI();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BUMP_ROT_MATRIX_ATI = 0x8775;
        public static UInt32 GL_BUMP_ROT_MATRIX_SIZE_ATI = 0x8776;
        public static UInt32 GL_BUMP_NUM_TEX_UNITS_ATI = 0x8777;
        public static UInt32 GL_BUMP_TEX_UNITS_ATI = 0x8778;
        public static UInt32 GL_DUDV_ATI = 0x8779;
        public static UInt32 GL_DU8DV8_ATI = 0x877A;
        public static UInt32 GL_BUMP_ENVMAP_ATI = 0x877B;
        public static UInt32 GL_BUMP_TARGET_ATI = 0x877C;
        #endregion

        #region Commands
        internal delegate void glTexBumpParameterivATIFunc(GLenum @pname, const GLint * @param);
        internal static glTexBumpParameterivATIFunc glTexBumpParameterivATIPtr;
        internal static void loadTexBumpParameterivATI()
        {
            try
            {
                glTexBumpParameterivATIPtr = (glTexBumpParameterivATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBumpParameterivATI"), typeof(glTexBumpParameterivATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBumpParameterivATI'.");
            }
        }
        public static void glTexBumpParameterivATI(GLenum @pname, const GLint * @param) => glTexBumpParameterivATIPtr(@pname, @param);

        internal delegate void glTexBumpParameterfvATIFunc(GLenum @pname, const GLfloat * @param);
        internal static glTexBumpParameterfvATIFunc glTexBumpParameterfvATIPtr;
        internal static void loadTexBumpParameterfvATI()
        {
            try
            {
                glTexBumpParameterfvATIPtr = (glTexBumpParameterfvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBumpParameterfvATI"), typeof(glTexBumpParameterfvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBumpParameterfvATI'.");
            }
        }
        public static void glTexBumpParameterfvATI(GLenum @pname, const GLfloat * @param) => glTexBumpParameterfvATIPtr(@pname, @param);

        internal delegate void glGetTexBumpParameterivATIFunc(GLenum @pname, GLint * @param);
        internal static glGetTexBumpParameterivATIFunc glGetTexBumpParameterivATIPtr;
        internal static void loadGetTexBumpParameterivATI()
        {
            try
            {
                glGetTexBumpParameterivATIPtr = (glGetTexBumpParameterivATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexBumpParameterivATI"), typeof(glGetTexBumpParameterivATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexBumpParameterivATI'.");
            }
        }
        public static void glGetTexBumpParameterivATI(GLenum @pname, GLint * @param) => glGetTexBumpParameterivATIPtr(@pname, @param);

        internal delegate void glGetTexBumpParameterfvATIFunc(GLenum @pname, GLfloat * @param);
        internal static glGetTexBumpParameterfvATIFunc glGetTexBumpParameterfvATIPtr;
        internal static void loadGetTexBumpParameterfvATI()
        {
            try
            {
                glGetTexBumpParameterfvATIPtr = (glGetTexBumpParameterfvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexBumpParameterfvATI"), typeof(glGetTexBumpParameterfvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexBumpParameterfvATI'.");
            }
        }
        public static void glGetTexBumpParameterfvATI(GLenum @pname, GLfloat * @param) => glGetTexBumpParameterfvATIPtr(@pname, @param);
        #endregion
    }
}
