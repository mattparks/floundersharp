using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_pixel_format_float
    {
        #region Interop
        static GL_ATI_pixel_format_float()
        {
            Console.WriteLine("Initalising GL_ATI_pixel_format_float interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RGBA_FLOAT_MODE_ATI = 0x8820;
        public static UInt32 GL_COLOR_CLEAR_UNCLAMPED_VALUE_ATI = 0x8835;
        #endregion

        #region Commands
        #endregion
    }
}
