using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_vertex_array_object
    {
        #region Interop
        static GL_ATI_vertex_array_object()
        {
            Console.WriteLine("Initalising GL_ATI_vertex_array_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadNewObjectBufferATI();
            loadIsObjectBufferATI();
            loadUpdateObjectBufferATI();
            loadGetObjectBufferfvATI();
            loadGetObjectBufferivATI();
            loadFreeObjectBufferATI();
            loadArrayObjectATI();
            loadGetArrayObjectfvATI();
            loadGetArrayObjectivATI();
            loadVariantArrayObjectATI();
            loadGetVariantArrayObjectfvATI();
            loadGetVariantArrayObjectivATI();
        }
        #endregion

        #region Enums
        public static UInt32 GL_STATIC_ATI = 0x8760;
        public static UInt32 GL_DYNAMIC_ATI = 0x8761;
        public static UInt32 GL_PRESERVE_ATI = 0x8762;
        public static UInt32 GL_DISCARD_ATI = 0x8763;
        public static UInt32 GL_OBJECT_BUFFER_SIZE_ATI = 0x8764;
        public static UInt32 GL_OBJECT_BUFFER_USAGE_ATI = 0x8765;
        public static UInt32 GL_ARRAY_OBJECT_BUFFER_ATI = 0x8766;
        public static UInt32 GL_ARRAY_OBJECT_OFFSET_ATI = 0x8767;
        #endregion

        #region Commands
        internal delegate GLuint glNewObjectBufferATIFunc(GLsizei @size, const void * @pointer, GLenum @usage);
        internal static glNewObjectBufferATIFunc glNewObjectBufferATIPtr;
        internal static void loadNewObjectBufferATI()
        {
            try
            {
                glNewObjectBufferATIPtr = (glNewObjectBufferATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNewObjectBufferATI"), typeof(glNewObjectBufferATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNewObjectBufferATI'.");
            }
        }
        public static GLuint glNewObjectBufferATI(GLsizei @size, const void * @pointer, GLenum @usage) => glNewObjectBufferATIPtr(@size, @pointer, @usage);

        internal delegate GLboolean glIsObjectBufferATIFunc(GLuint @buffer);
        internal static glIsObjectBufferATIFunc glIsObjectBufferATIPtr;
        internal static void loadIsObjectBufferATI()
        {
            try
            {
                glIsObjectBufferATIPtr = (glIsObjectBufferATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsObjectBufferATI"), typeof(glIsObjectBufferATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsObjectBufferATI'.");
            }
        }
        public static GLboolean glIsObjectBufferATI(GLuint @buffer) => glIsObjectBufferATIPtr(@buffer);

        internal delegate void glUpdateObjectBufferATIFunc(GLuint @buffer, GLuint @offset, GLsizei @size, const void * @pointer, GLenum @preserve);
        internal static glUpdateObjectBufferATIFunc glUpdateObjectBufferATIPtr;
        internal static void loadUpdateObjectBufferATI()
        {
            try
            {
                glUpdateObjectBufferATIPtr = (glUpdateObjectBufferATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUpdateObjectBufferATI"), typeof(glUpdateObjectBufferATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUpdateObjectBufferATI'.");
            }
        }
        public static void glUpdateObjectBufferATI(GLuint @buffer, GLuint @offset, GLsizei @size, const void * @pointer, GLenum @preserve) => glUpdateObjectBufferATIPtr(@buffer, @offset, @size, @pointer, @preserve);

        internal delegate void glGetObjectBufferfvATIFunc(GLuint @buffer, GLenum @pname, GLfloat * @params);
        internal static glGetObjectBufferfvATIFunc glGetObjectBufferfvATIPtr;
        internal static void loadGetObjectBufferfvATI()
        {
            try
            {
                glGetObjectBufferfvATIPtr = (glGetObjectBufferfvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectBufferfvATI"), typeof(glGetObjectBufferfvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectBufferfvATI'.");
            }
        }
        public static void glGetObjectBufferfvATI(GLuint @buffer, GLenum @pname, GLfloat * @params) => glGetObjectBufferfvATIPtr(@buffer, @pname, @params);

        internal delegate void glGetObjectBufferivATIFunc(GLuint @buffer, GLenum @pname, GLint * @params);
        internal static glGetObjectBufferivATIFunc glGetObjectBufferivATIPtr;
        internal static void loadGetObjectBufferivATI()
        {
            try
            {
                glGetObjectBufferivATIPtr = (glGetObjectBufferivATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectBufferivATI"), typeof(glGetObjectBufferivATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectBufferivATI'.");
            }
        }
        public static void glGetObjectBufferivATI(GLuint @buffer, GLenum @pname, GLint * @params) => glGetObjectBufferivATIPtr(@buffer, @pname, @params);

        internal delegate void glFreeObjectBufferATIFunc(GLuint @buffer);
        internal static glFreeObjectBufferATIFunc glFreeObjectBufferATIPtr;
        internal static void loadFreeObjectBufferATI()
        {
            try
            {
                glFreeObjectBufferATIPtr = (glFreeObjectBufferATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFreeObjectBufferATI"), typeof(glFreeObjectBufferATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFreeObjectBufferATI'.");
            }
        }
        public static void glFreeObjectBufferATI(GLuint @buffer) => glFreeObjectBufferATIPtr(@buffer);

        internal delegate void glArrayObjectATIFunc(GLenum @array, GLint @size, GLenum @type, GLsizei @stride, GLuint @buffer, GLuint @offset);
        internal static glArrayObjectATIFunc glArrayObjectATIPtr;
        internal static void loadArrayObjectATI()
        {
            try
            {
                glArrayObjectATIPtr = (glArrayObjectATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glArrayObjectATI"), typeof(glArrayObjectATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glArrayObjectATI'.");
            }
        }
        public static void glArrayObjectATI(GLenum @array, GLint @size, GLenum @type, GLsizei @stride, GLuint @buffer, GLuint @offset) => glArrayObjectATIPtr(@array, @size, @type, @stride, @buffer, @offset);

        internal delegate void glGetArrayObjectfvATIFunc(GLenum @array, GLenum @pname, GLfloat * @params);
        internal static glGetArrayObjectfvATIFunc glGetArrayObjectfvATIPtr;
        internal static void loadGetArrayObjectfvATI()
        {
            try
            {
                glGetArrayObjectfvATIPtr = (glGetArrayObjectfvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetArrayObjectfvATI"), typeof(glGetArrayObjectfvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetArrayObjectfvATI'.");
            }
        }
        public static void glGetArrayObjectfvATI(GLenum @array, GLenum @pname, GLfloat * @params) => glGetArrayObjectfvATIPtr(@array, @pname, @params);

        internal delegate void glGetArrayObjectivATIFunc(GLenum @array, GLenum @pname, GLint * @params);
        internal static glGetArrayObjectivATIFunc glGetArrayObjectivATIPtr;
        internal static void loadGetArrayObjectivATI()
        {
            try
            {
                glGetArrayObjectivATIPtr = (glGetArrayObjectivATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetArrayObjectivATI"), typeof(glGetArrayObjectivATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetArrayObjectivATI'.");
            }
        }
        public static void glGetArrayObjectivATI(GLenum @array, GLenum @pname, GLint * @params) => glGetArrayObjectivATIPtr(@array, @pname, @params);

        internal delegate void glVariantArrayObjectATIFunc(GLuint @id, GLenum @type, GLsizei @stride, GLuint @buffer, GLuint @offset);
        internal static glVariantArrayObjectATIFunc glVariantArrayObjectATIPtr;
        internal static void loadVariantArrayObjectATI()
        {
            try
            {
                glVariantArrayObjectATIPtr = (glVariantArrayObjectATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVariantArrayObjectATI"), typeof(glVariantArrayObjectATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVariantArrayObjectATI'.");
            }
        }
        public static void glVariantArrayObjectATI(GLuint @id, GLenum @type, GLsizei @stride, GLuint @buffer, GLuint @offset) => glVariantArrayObjectATIPtr(@id, @type, @stride, @buffer, @offset);

        internal delegate void glGetVariantArrayObjectfvATIFunc(GLuint @id, GLenum @pname, GLfloat * @params);
        internal static glGetVariantArrayObjectfvATIFunc glGetVariantArrayObjectfvATIPtr;
        internal static void loadGetVariantArrayObjectfvATI()
        {
            try
            {
                glGetVariantArrayObjectfvATIPtr = (glGetVariantArrayObjectfvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVariantArrayObjectfvATI"), typeof(glGetVariantArrayObjectfvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVariantArrayObjectfvATI'.");
            }
        }
        public static void glGetVariantArrayObjectfvATI(GLuint @id, GLenum @pname, GLfloat * @params) => glGetVariantArrayObjectfvATIPtr(@id, @pname, @params);

        internal delegate void glGetVariantArrayObjectivATIFunc(GLuint @id, GLenum @pname, GLint * @params);
        internal static glGetVariantArrayObjectivATIFunc glGetVariantArrayObjectivATIPtr;
        internal static void loadGetVariantArrayObjectivATI()
        {
            try
            {
                glGetVariantArrayObjectivATIPtr = (glGetVariantArrayObjectivATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVariantArrayObjectivATI"), typeof(glGetVariantArrayObjectivATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVariantArrayObjectivATI'.");
            }
        }
        public static void glGetVariantArrayObjectivATI(GLuint @id, GLenum @pname, GLint * @params) => glGetVariantArrayObjectivATIPtr(@id, @pname, @params);
        #endregion
    }
}
