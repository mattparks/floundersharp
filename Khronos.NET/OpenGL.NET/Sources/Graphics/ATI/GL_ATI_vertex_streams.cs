using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_vertex_streams
    {
        #region Interop
        static GL_ATI_vertex_streams()
        {
            Console.WriteLine("Initalising GL_ATI_vertex_streams interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexStream1sATI();
            loadVertexStream1svATI();
            loadVertexStream1iATI();
            loadVertexStream1ivATI();
            loadVertexStream1fATI();
            loadVertexStream1fvATI();
            loadVertexStream1dATI();
            loadVertexStream1dvATI();
            loadVertexStream2sATI();
            loadVertexStream2svATI();
            loadVertexStream2iATI();
            loadVertexStream2ivATI();
            loadVertexStream2fATI();
            loadVertexStream2fvATI();
            loadVertexStream2dATI();
            loadVertexStream2dvATI();
            loadVertexStream3sATI();
            loadVertexStream3svATI();
            loadVertexStream3iATI();
            loadVertexStream3ivATI();
            loadVertexStream3fATI();
            loadVertexStream3fvATI();
            loadVertexStream3dATI();
            loadVertexStream3dvATI();
            loadVertexStream4sATI();
            loadVertexStream4svATI();
            loadVertexStream4iATI();
            loadVertexStream4ivATI();
            loadVertexStream4fATI();
            loadVertexStream4fvATI();
            loadVertexStream4dATI();
            loadVertexStream4dvATI();
            loadNormalStream3bATI();
            loadNormalStream3bvATI();
            loadNormalStream3sATI();
            loadNormalStream3svATI();
            loadNormalStream3iATI();
            loadNormalStream3ivATI();
            loadNormalStream3fATI();
            loadNormalStream3fvATI();
            loadNormalStream3dATI();
            loadNormalStream3dvATI();
            loadClientActiveVertexStreamATI();
            loadVertexBlendEnviATI();
            loadVertexBlendEnvfATI();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_VERTEX_STREAMS_ATI = 0x876B;
        public static UInt32 GL_VERTEX_STREAM0_ATI = 0x876C;
        public static UInt32 GL_VERTEX_STREAM1_ATI = 0x876D;
        public static UInt32 GL_VERTEX_STREAM2_ATI = 0x876E;
        public static UInt32 GL_VERTEX_STREAM3_ATI = 0x876F;
        public static UInt32 GL_VERTEX_STREAM4_ATI = 0x8770;
        public static UInt32 GL_VERTEX_STREAM5_ATI = 0x8771;
        public static UInt32 GL_VERTEX_STREAM6_ATI = 0x8772;
        public static UInt32 GL_VERTEX_STREAM7_ATI = 0x8773;
        public static UInt32 GL_VERTEX_SOURCE_ATI = 0x8774;
        #endregion

        #region Commands
        internal delegate void glVertexStream1sATIFunc(GLenum @stream, GLshort @x);
        internal static glVertexStream1sATIFunc glVertexStream1sATIPtr;
        internal static void loadVertexStream1sATI()
        {
            try
            {
                glVertexStream1sATIPtr = (glVertexStream1sATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream1sATI"), typeof(glVertexStream1sATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream1sATI'.");
            }
        }
        public static void glVertexStream1sATI(GLenum @stream, GLshort @x) => glVertexStream1sATIPtr(@stream, @x);

        internal delegate void glVertexStream1svATIFunc(GLenum @stream, const GLshort * @coords);
        internal static glVertexStream1svATIFunc glVertexStream1svATIPtr;
        internal static void loadVertexStream1svATI()
        {
            try
            {
                glVertexStream1svATIPtr = (glVertexStream1svATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream1svATI"), typeof(glVertexStream1svATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream1svATI'.");
            }
        }
        public static void glVertexStream1svATI(GLenum @stream, const GLshort * @coords) => glVertexStream1svATIPtr(@stream, @coords);

        internal delegate void glVertexStream1iATIFunc(GLenum @stream, GLint @x);
        internal static glVertexStream1iATIFunc glVertexStream1iATIPtr;
        internal static void loadVertexStream1iATI()
        {
            try
            {
                glVertexStream1iATIPtr = (glVertexStream1iATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream1iATI"), typeof(glVertexStream1iATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream1iATI'.");
            }
        }
        public static void glVertexStream1iATI(GLenum @stream, GLint @x) => glVertexStream1iATIPtr(@stream, @x);

        internal delegate void glVertexStream1ivATIFunc(GLenum @stream, const GLint * @coords);
        internal static glVertexStream1ivATIFunc glVertexStream1ivATIPtr;
        internal static void loadVertexStream1ivATI()
        {
            try
            {
                glVertexStream1ivATIPtr = (glVertexStream1ivATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream1ivATI"), typeof(glVertexStream1ivATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream1ivATI'.");
            }
        }
        public static void glVertexStream1ivATI(GLenum @stream, const GLint * @coords) => glVertexStream1ivATIPtr(@stream, @coords);

        internal delegate void glVertexStream1fATIFunc(GLenum @stream, GLfloat @x);
        internal static glVertexStream1fATIFunc glVertexStream1fATIPtr;
        internal static void loadVertexStream1fATI()
        {
            try
            {
                glVertexStream1fATIPtr = (glVertexStream1fATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream1fATI"), typeof(glVertexStream1fATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream1fATI'.");
            }
        }
        public static void glVertexStream1fATI(GLenum @stream, GLfloat @x) => glVertexStream1fATIPtr(@stream, @x);

        internal delegate void glVertexStream1fvATIFunc(GLenum @stream, const GLfloat * @coords);
        internal static glVertexStream1fvATIFunc glVertexStream1fvATIPtr;
        internal static void loadVertexStream1fvATI()
        {
            try
            {
                glVertexStream1fvATIPtr = (glVertexStream1fvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream1fvATI"), typeof(glVertexStream1fvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream1fvATI'.");
            }
        }
        public static void glVertexStream1fvATI(GLenum @stream, const GLfloat * @coords) => glVertexStream1fvATIPtr(@stream, @coords);

        internal delegate void glVertexStream1dATIFunc(GLenum @stream, GLdouble @x);
        internal static glVertexStream1dATIFunc glVertexStream1dATIPtr;
        internal static void loadVertexStream1dATI()
        {
            try
            {
                glVertexStream1dATIPtr = (glVertexStream1dATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream1dATI"), typeof(glVertexStream1dATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream1dATI'.");
            }
        }
        public static void glVertexStream1dATI(GLenum @stream, GLdouble @x) => glVertexStream1dATIPtr(@stream, @x);

        internal delegate void glVertexStream1dvATIFunc(GLenum @stream, const GLdouble * @coords);
        internal static glVertexStream1dvATIFunc glVertexStream1dvATIPtr;
        internal static void loadVertexStream1dvATI()
        {
            try
            {
                glVertexStream1dvATIPtr = (glVertexStream1dvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream1dvATI"), typeof(glVertexStream1dvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream1dvATI'.");
            }
        }
        public static void glVertexStream1dvATI(GLenum @stream, const GLdouble * @coords) => glVertexStream1dvATIPtr(@stream, @coords);

        internal delegate void glVertexStream2sATIFunc(GLenum @stream, GLshort @x, GLshort @y);
        internal static glVertexStream2sATIFunc glVertexStream2sATIPtr;
        internal static void loadVertexStream2sATI()
        {
            try
            {
                glVertexStream2sATIPtr = (glVertexStream2sATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream2sATI"), typeof(glVertexStream2sATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream2sATI'.");
            }
        }
        public static void glVertexStream2sATI(GLenum @stream, GLshort @x, GLshort @y) => glVertexStream2sATIPtr(@stream, @x, @y);

        internal delegate void glVertexStream2svATIFunc(GLenum @stream, const GLshort * @coords);
        internal static glVertexStream2svATIFunc glVertexStream2svATIPtr;
        internal static void loadVertexStream2svATI()
        {
            try
            {
                glVertexStream2svATIPtr = (glVertexStream2svATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream2svATI"), typeof(glVertexStream2svATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream2svATI'.");
            }
        }
        public static void glVertexStream2svATI(GLenum @stream, const GLshort * @coords) => glVertexStream2svATIPtr(@stream, @coords);

        internal delegate void glVertexStream2iATIFunc(GLenum @stream, GLint @x, GLint @y);
        internal static glVertexStream2iATIFunc glVertexStream2iATIPtr;
        internal static void loadVertexStream2iATI()
        {
            try
            {
                glVertexStream2iATIPtr = (glVertexStream2iATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream2iATI"), typeof(glVertexStream2iATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream2iATI'.");
            }
        }
        public static void glVertexStream2iATI(GLenum @stream, GLint @x, GLint @y) => glVertexStream2iATIPtr(@stream, @x, @y);

        internal delegate void glVertexStream2ivATIFunc(GLenum @stream, const GLint * @coords);
        internal static glVertexStream2ivATIFunc glVertexStream2ivATIPtr;
        internal static void loadVertexStream2ivATI()
        {
            try
            {
                glVertexStream2ivATIPtr = (glVertexStream2ivATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream2ivATI"), typeof(glVertexStream2ivATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream2ivATI'.");
            }
        }
        public static void glVertexStream2ivATI(GLenum @stream, const GLint * @coords) => glVertexStream2ivATIPtr(@stream, @coords);

        internal delegate void glVertexStream2fATIFunc(GLenum @stream, GLfloat @x, GLfloat @y);
        internal static glVertexStream2fATIFunc glVertexStream2fATIPtr;
        internal static void loadVertexStream2fATI()
        {
            try
            {
                glVertexStream2fATIPtr = (glVertexStream2fATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream2fATI"), typeof(glVertexStream2fATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream2fATI'.");
            }
        }
        public static void glVertexStream2fATI(GLenum @stream, GLfloat @x, GLfloat @y) => glVertexStream2fATIPtr(@stream, @x, @y);

        internal delegate void glVertexStream2fvATIFunc(GLenum @stream, const GLfloat * @coords);
        internal static glVertexStream2fvATIFunc glVertexStream2fvATIPtr;
        internal static void loadVertexStream2fvATI()
        {
            try
            {
                glVertexStream2fvATIPtr = (glVertexStream2fvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream2fvATI"), typeof(glVertexStream2fvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream2fvATI'.");
            }
        }
        public static void glVertexStream2fvATI(GLenum @stream, const GLfloat * @coords) => glVertexStream2fvATIPtr(@stream, @coords);

        internal delegate void glVertexStream2dATIFunc(GLenum @stream, GLdouble @x, GLdouble @y);
        internal static glVertexStream2dATIFunc glVertexStream2dATIPtr;
        internal static void loadVertexStream2dATI()
        {
            try
            {
                glVertexStream2dATIPtr = (glVertexStream2dATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream2dATI"), typeof(glVertexStream2dATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream2dATI'.");
            }
        }
        public static void glVertexStream2dATI(GLenum @stream, GLdouble @x, GLdouble @y) => glVertexStream2dATIPtr(@stream, @x, @y);

        internal delegate void glVertexStream2dvATIFunc(GLenum @stream, const GLdouble * @coords);
        internal static glVertexStream2dvATIFunc glVertexStream2dvATIPtr;
        internal static void loadVertexStream2dvATI()
        {
            try
            {
                glVertexStream2dvATIPtr = (glVertexStream2dvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream2dvATI"), typeof(glVertexStream2dvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream2dvATI'.");
            }
        }
        public static void glVertexStream2dvATI(GLenum @stream, const GLdouble * @coords) => glVertexStream2dvATIPtr(@stream, @coords);

        internal delegate void glVertexStream3sATIFunc(GLenum @stream, GLshort @x, GLshort @y, GLshort @z);
        internal static glVertexStream3sATIFunc glVertexStream3sATIPtr;
        internal static void loadVertexStream3sATI()
        {
            try
            {
                glVertexStream3sATIPtr = (glVertexStream3sATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream3sATI"), typeof(glVertexStream3sATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream3sATI'.");
            }
        }
        public static void glVertexStream3sATI(GLenum @stream, GLshort @x, GLshort @y, GLshort @z) => glVertexStream3sATIPtr(@stream, @x, @y, @z);

        internal delegate void glVertexStream3svATIFunc(GLenum @stream, const GLshort * @coords);
        internal static glVertexStream3svATIFunc glVertexStream3svATIPtr;
        internal static void loadVertexStream3svATI()
        {
            try
            {
                glVertexStream3svATIPtr = (glVertexStream3svATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream3svATI"), typeof(glVertexStream3svATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream3svATI'.");
            }
        }
        public static void glVertexStream3svATI(GLenum @stream, const GLshort * @coords) => glVertexStream3svATIPtr(@stream, @coords);

        internal delegate void glVertexStream3iATIFunc(GLenum @stream, GLint @x, GLint @y, GLint @z);
        internal static glVertexStream3iATIFunc glVertexStream3iATIPtr;
        internal static void loadVertexStream3iATI()
        {
            try
            {
                glVertexStream3iATIPtr = (glVertexStream3iATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream3iATI"), typeof(glVertexStream3iATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream3iATI'.");
            }
        }
        public static void glVertexStream3iATI(GLenum @stream, GLint @x, GLint @y, GLint @z) => glVertexStream3iATIPtr(@stream, @x, @y, @z);

        internal delegate void glVertexStream3ivATIFunc(GLenum @stream, const GLint * @coords);
        internal static glVertexStream3ivATIFunc glVertexStream3ivATIPtr;
        internal static void loadVertexStream3ivATI()
        {
            try
            {
                glVertexStream3ivATIPtr = (glVertexStream3ivATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream3ivATI"), typeof(glVertexStream3ivATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream3ivATI'.");
            }
        }
        public static void glVertexStream3ivATI(GLenum @stream, const GLint * @coords) => glVertexStream3ivATIPtr(@stream, @coords);

        internal delegate void glVertexStream3fATIFunc(GLenum @stream, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glVertexStream3fATIFunc glVertexStream3fATIPtr;
        internal static void loadVertexStream3fATI()
        {
            try
            {
                glVertexStream3fATIPtr = (glVertexStream3fATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream3fATI"), typeof(glVertexStream3fATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream3fATI'.");
            }
        }
        public static void glVertexStream3fATI(GLenum @stream, GLfloat @x, GLfloat @y, GLfloat @z) => glVertexStream3fATIPtr(@stream, @x, @y, @z);

        internal delegate void glVertexStream3fvATIFunc(GLenum @stream, const GLfloat * @coords);
        internal static glVertexStream3fvATIFunc glVertexStream3fvATIPtr;
        internal static void loadVertexStream3fvATI()
        {
            try
            {
                glVertexStream3fvATIPtr = (glVertexStream3fvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream3fvATI"), typeof(glVertexStream3fvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream3fvATI'.");
            }
        }
        public static void glVertexStream3fvATI(GLenum @stream, const GLfloat * @coords) => glVertexStream3fvATIPtr(@stream, @coords);

        internal delegate void glVertexStream3dATIFunc(GLenum @stream, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glVertexStream3dATIFunc glVertexStream3dATIPtr;
        internal static void loadVertexStream3dATI()
        {
            try
            {
                glVertexStream3dATIPtr = (glVertexStream3dATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream3dATI"), typeof(glVertexStream3dATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream3dATI'.");
            }
        }
        public static void glVertexStream3dATI(GLenum @stream, GLdouble @x, GLdouble @y, GLdouble @z) => glVertexStream3dATIPtr(@stream, @x, @y, @z);

        internal delegate void glVertexStream3dvATIFunc(GLenum @stream, const GLdouble * @coords);
        internal static glVertexStream3dvATIFunc glVertexStream3dvATIPtr;
        internal static void loadVertexStream3dvATI()
        {
            try
            {
                glVertexStream3dvATIPtr = (glVertexStream3dvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream3dvATI"), typeof(glVertexStream3dvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream3dvATI'.");
            }
        }
        public static void glVertexStream3dvATI(GLenum @stream, const GLdouble * @coords) => glVertexStream3dvATIPtr(@stream, @coords);

        internal delegate void glVertexStream4sATIFunc(GLenum @stream, GLshort @x, GLshort @y, GLshort @z, GLshort @w);
        internal static glVertexStream4sATIFunc glVertexStream4sATIPtr;
        internal static void loadVertexStream4sATI()
        {
            try
            {
                glVertexStream4sATIPtr = (glVertexStream4sATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream4sATI"), typeof(glVertexStream4sATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream4sATI'.");
            }
        }
        public static void glVertexStream4sATI(GLenum @stream, GLshort @x, GLshort @y, GLshort @z, GLshort @w) => glVertexStream4sATIPtr(@stream, @x, @y, @z, @w);

        internal delegate void glVertexStream4svATIFunc(GLenum @stream, const GLshort * @coords);
        internal static glVertexStream4svATIFunc glVertexStream4svATIPtr;
        internal static void loadVertexStream4svATI()
        {
            try
            {
                glVertexStream4svATIPtr = (glVertexStream4svATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream4svATI"), typeof(glVertexStream4svATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream4svATI'.");
            }
        }
        public static void glVertexStream4svATI(GLenum @stream, const GLshort * @coords) => glVertexStream4svATIPtr(@stream, @coords);

        internal delegate void glVertexStream4iATIFunc(GLenum @stream, GLint @x, GLint @y, GLint @z, GLint @w);
        internal static glVertexStream4iATIFunc glVertexStream4iATIPtr;
        internal static void loadVertexStream4iATI()
        {
            try
            {
                glVertexStream4iATIPtr = (glVertexStream4iATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream4iATI"), typeof(glVertexStream4iATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream4iATI'.");
            }
        }
        public static void glVertexStream4iATI(GLenum @stream, GLint @x, GLint @y, GLint @z, GLint @w) => glVertexStream4iATIPtr(@stream, @x, @y, @z, @w);

        internal delegate void glVertexStream4ivATIFunc(GLenum @stream, const GLint * @coords);
        internal static glVertexStream4ivATIFunc glVertexStream4ivATIPtr;
        internal static void loadVertexStream4ivATI()
        {
            try
            {
                glVertexStream4ivATIPtr = (glVertexStream4ivATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream4ivATI"), typeof(glVertexStream4ivATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream4ivATI'.");
            }
        }
        public static void glVertexStream4ivATI(GLenum @stream, const GLint * @coords) => glVertexStream4ivATIPtr(@stream, @coords);

        internal delegate void glVertexStream4fATIFunc(GLenum @stream, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glVertexStream4fATIFunc glVertexStream4fATIPtr;
        internal static void loadVertexStream4fATI()
        {
            try
            {
                glVertexStream4fATIPtr = (glVertexStream4fATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream4fATI"), typeof(glVertexStream4fATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream4fATI'.");
            }
        }
        public static void glVertexStream4fATI(GLenum @stream, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glVertexStream4fATIPtr(@stream, @x, @y, @z, @w);

        internal delegate void glVertexStream4fvATIFunc(GLenum @stream, const GLfloat * @coords);
        internal static glVertexStream4fvATIFunc glVertexStream4fvATIPtr;
        internal static void loadVertexStream4fvATI()
        {
            try
            {
                glVertexStream4fvATIPtr = (glVertexStream4fvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream4fvATI"), typeof(glVertexStream4fvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream4fvATI'.");
            }
        }
        public static void glVertexStream4fvATI(GLenum @stream, const GLfloat * @coords) => glVertexStream4fvATIPtr(@stream, @coords);

        internal delegate void glVertexStream4dATIFunc(GLenum @stream, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glVertexStream4dATIFunc glVertexStream4dATIPtr;
        internal static void loadVertexStream4dATI()
        {
            try
            {
                glVertexStream4dATIPtr = (glVertexStream4dATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream4dATI"), typeof(glVertexStream4dATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream4dATI'.");
            }
        }
        public static void glVertexStream4dATI(GLenum @stream, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glVertexStream4dATIPtr(@stream, @x, @y, @z, @w);

        internal delegate void glVertexStream4dvATIFunc(GLenum @stream, const GLdouble * @coords);
        internal static glVertexStream4dvATIFunc glVertexStream4dvATIPtr;
        internal static void loadVertexStream4dvATI()
        {
            try
            {
                glVertexStream4dvATIPtr = (glVertexStream4dvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexStream4dvATI"), typeof(glVertexStream4dvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexStream4dvATI'.");
            }
        }
        public static void glVertexStream4dvATI(GLenum @stream, const GLdouble * @coords) => glVertexStream4dvATIPtr(@stream, @coords);

        internal delegate void glNormalStream3bATIFunc(GLenum @stream, GLbyte @nx, GLbyte @ny, GLbyte @nz);
        internal static glNormalStream3bATIFunc glNormalStream3bATIPtr;
        internal static void loadNormalStream3bATI()
        {
            try
            {
                glNormalStream3bATIPtr = (glNormalStream3bATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalStream3bATI"), typeof(glNormalStream3bATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalStream3bATI'.");
            }
        }
        public static void glNormalStream3bATI(GLenum @stream, GLbyte @nx, GLbyte @ny, GLbyte @nz) => glNormalStream3bATIPtr(@stream, @nx, @ny, @nz);

        internal delegate void glNormalStream3bvATIFunc(GLenum @stream, const GLbyte * @coords);
        internal static glNormalStream3bvATIFunc glNormalStream3bvATIPtr;
        internal static void loadNormalStream3bvATI()
        {
            try
            {
                glNormalStream3bvATIPtr = (glNormalStream3bvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalStream3bvATI"), typeof(glNormalStream3bvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalStream3bvATI'.");
            }
        }
        public static void glNormalStream3bvATI(GLenum @stream, const GLbyte * @coords) => glNormalStream3bvATIPtr(@stream, @coords);

        internal delegate void glNormalStream3sATIFunc(GLenum @stream, GLshort @nx, GLshort @ny, GLshort @nz);
        internal static glNormalStream3sATIFunc glNormalStream3sATIPtr;
        internal static void loadNormalStream3sATI()
        {
            try
            {
                glNormalStream3sATIPtr = (glNormalStream3sATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalStream3sATI"), typeof(glNormalStream3sATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalStream3sATI'.");
            }
        }
        public static void glNormalStream3sATI(GLenum @stream, GLshort @nx, GLshort @ny, GLshort @nz) => glNormalStream3sATIPtr(@stream, @nx, @ny, @nz);

        internal delegate void glNormalStream3svATIFunc(GLenum @stream, const GLshort * @coords);
        internal static glNormalStream3svATIFunc glNormalStream3svATIPtr;
        internal static void loadNormalStream3svATI()
        {
            try
            {
                glNormalStream3svATIPtr = (glNormalStream3svATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalStream3svATI"), typeof(glNormalStream3svATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalStream3svATI'.");
            }
        }
        public static void glNormalStream3svATI(GLenum @stream, const GLshort * @coords) => glNormalStream3svATIPtr(@stream, @coords);

        internal delegate void glNormalStream3iATIFunc(GLenum @stream, GLint @nx, GLint @ny, GLint @nz);
        internal static glNormalStream3iATIFunc glNormalStream3iATIPtr;
        internal static void loadNormalStream3iATI()
        {
            try
            {
                glNormalStream3iATIPtr = (glNormalStream3iATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalStream3iATI"), typeof(glNormalStream3iATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalStream3iATI'.");
            }
        }
        public static void glNormalStream3iATI(GLenum @stream, GLint @nx, GLint @ny, GLint @nz) => glNormalStream3iATIPtr(@stream, @nx, @ny, @nz);

        internal delegate void glNormalStream3ivATIFunc(GLenum @stream, const GLint * @coords);
        internal static glNormalStream3ivATIFunc glNormalStream3ivATIPtr;
        internal static void loadNormalStream3ivATI()
        {
            try
            {
                glNormalStream3ivATIPtr = (glNormalStream3ivATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalStream3ivATI"), typeof(glNormalStream3ivATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalStream3ivATI'.");
            }
        }
        public static void glNormalStream3ivATI(GLenum @stream, const GLint * @coords) => glNormalStream3ivATIPtr(@stream, @coords);

        internal delegate void glNormalStream3fATIFunc(GLenum @stream, GLfloat @nx, GLfloat @ny, GLfloat @nz);
        internal static glNormalStream3fATIFunc glNormalStream3fATIPtr;
        internal static void loadNormalStream3fATI()
        {
            try
            {
                glNormalStream3fATIPtr = (glNormalStream3fATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalStream3fATI"), typeof(glNormalStream3fATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalStream3fATI'.");
            }
        }
        public static void glNormalStream3fATI(GLenum @stream, GLfloat @nx, GLfloat @ny, GLfloat @nz) => glNormalStream3fATIPtr(@stream, @nx, @ny, @nz);

        internal delegate void glNormalStream3fvATIFunc(GLenum @stream, const GLfloat * @coords);
        internal static glNormalStream3fvATIFunc glNormalStream3fvATIPtr;
        internal static void loadNormalStream3fvATI()
        {
            try
            {
                glNormalStream3fvATIPtr = (glNormalStream3fvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalStream3fvATI"), typeof(glNormalStream3fvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalStream3fvATI'.");
            }
        }
        public static void glNormalStream3fvATI(GLenum @stream, const GLfloat * @coords) => glNormalStream3fvATIPtr(@stream, @coords);

        internal delegate void glNormalStream3dATIFunc(GLenum @stream, GLdouble @nx, GLdouble @ny, GLdouble @nz);
        internal static glNormalStream3dATIFunc glNormalStream3dATIPtr;
        internal static void loadNormalStream3dATI()
        {
            try
            {
                glNormalStream3dATIPtr = (glNormalStream3dATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalStream3dATI"), typeof(glNormalStream3dATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalStream3dATI'.");
            }
        }
        public static void glNormalStream3dATI(GLenum @stream, GLdouble @nx, GLdouble @ny, GLdouble @nz) => glNormalStream3dATIPtr(@stream, @nx, @ny, @nz);

        internal delegate void glNormalStream3dvATIFunc(GLenum @stream, const GLdouble * @coords);
        internal static glNormalStream3dvATIFunc glNormalStream3dvATIPtr;
        internal static void loadNormalStream3dvATI()
        {
            try
            {
                glNormalStream3dvATIPtr = (glNormalStream3dvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalStream3dvATI"), typeof(glNormalStream3dvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalStream3dvATI'.");
            }
        }
        public static void glNormalStream3dvATI(GLenum @stream, const GLdouble * @coords) => glNormalStream3dvATIPtr(@stream, @coords);

        internal delegate void glClientActiveVertexStreamATIFunc(GLenum @stream);
        internal static glClientActiveVertexStreamATIFunc glClientActiveVertexStreamATIPtr;
        internal static void loadClientActiveVertexStreamATI()
        {
            try
            {
                glClientActiveVertexStreamATIPtr = (glClientActiveVertexStreamATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClientActiveVertexStreamATI"), typeof(glClientActiveVertexStreamATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClientActiveVertexStreamATI'.");
            }
        }
        public static void glClientActiveVertexStreamATI(GLenum @stream) => glClientActiveVertexStreamATIPtr(@stream);

        internal delegate void glVertexBlendEnviATIFunc(GLenum @pname, GLint @param);
        internal static glVertexBlendEnviATIFunc glVertexBlendEnviATIPtr;
        internal static void loadVertexBlendEnviATI()
        {
            try
            {
                glVertexBlendEnviATIPtr = (glVertexBlendEnviATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexBlendEnviATI"), typeof(glVertexBlendEnviATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexBlendEnviATI'.");
            }
        }
        public static void glVertexBlendEnviATI(GLenum @pname, GLint @param) => glVertexBlendEnviATIPtr(@pname, @param);

        internal delegate void glVertexBlendEnvfATIFunc(GLenum @pname, GLfloat @param);
        internal static glVertexBlendEnvfATIFunc glVertexBlendEnvfATIPtr;
        internal static void loadVertexBlendEnvfATI()
        {
            try
            {
                glVertexBlendEnvfATIPtr = (glVertexBlendEnvfATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexBlendEnvfATI"), typeof(glVertexBlendEnvfATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexBlendEnvfATI'.");
            }
        }
        public static void glVertexBlendEnvfATI(GLenum @pname, GLfloat @param) => glVertexBlendEnvfATIPtr(@pname, @param);
        #endregion
    }
}
