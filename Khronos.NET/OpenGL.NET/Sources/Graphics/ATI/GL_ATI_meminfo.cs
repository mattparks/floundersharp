using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_meminfo
    {
        #region Interop
        static GL_ATI_meminfo()
        {
            Console.WriteLine("Initalising GL_ATI_meminfo interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_VBO_FREE_MEMORY_ATI = 0x87FB;
        public static UInt32 GL_TEXTURE_FREE_MEMORY_ATI = 0x87FC;
        public static UInt32 GL_RENDERBUFFER_FREE_MEMORY_ATI = 0x87FD;
        #endregion

        #region Commands
        #endregion
    }
}
