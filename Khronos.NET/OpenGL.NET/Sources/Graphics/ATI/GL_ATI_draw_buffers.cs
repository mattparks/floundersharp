using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_draw_buffers
    {
        #region Interop
        static GL_ATI_draw_buffers()
        {
            Console.WriteLine("Initalising GL_ATI_draw_buffers interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawBuffersATI();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_DRAW_BUFFERS_ATI = 0x8824;
        public static UInt32 GL_DRAW_BUFFER0_ATI = 0x8825;
        public static UInt32 GL_DRAW_BUFFER1_ATI = 0x8826;
        public static UInt32 GL_DRAW_BUFFER2_ATI = 0x8827;
        public static UInt32 GL_DRAW_BUFFER3_ATI = 0x8828;
        public static UInt32 GL_DRAW_BUFFER4_ATI = 0x8829;
        public static UInt32 GL_DRAW_BUFFER5_ATI = 0x882A;
        public static UInt32 GL_DRAW_BUFFER6_ATI = 0x882B;
        public static UInt32 GL_DRAW_BUFFER7_ATI = 0x882C;
        public static UInt32 GL_DRAW_BUFFER8_ATI = 0x882D;
        public static UInt32 GL_DRAW_BUFFER9_ATI = 0x882E;
        public static UInt32 GL_DRAW_BUFFER10_ATI = 0x882F;
        public static UInt32 GL_DRAW_BUFFER11_ATI = 0x8830;
        public static UInt32 GL_DRAW_BUFFER12_ATI = 0x8831;
        public static UInt32 GL_DRAW_BUFFER13_ATI = 0x8832;
        public static UInt32 GL_DRAW_BUFFER14_ATI = 0x8833;
        public static UInt32 GL_DRAW_BUFFER15_ATI = 0x8834;
        #endregion

        #region Commands
        internal delegate void glDrawBuffersATIFunc(GLsizei @n, const GLenum * @bufs);
        internal static glDrawBuffersATIFunc glDrawBuffersATIPtr;
        internal static void loadDrawBuffersATI()
        {
            try
            {
                glDrawBuffersATIPtr = (glDrawBuffersATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawBuffersATI"), typeof(glDrawBuffersATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawBuffersATI'.");
            }
        }
        public static void glDrawBuffersATI(GLsizei @n, const GLenum * @bufs) => glDrawBuffersATIPtr(@n, @bufs);
        #endregion
    }
}
