using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_vertex_attrib_array_object
    {
        #region Interop
        static GL_ATI_vertex_attrib_array_object()
        {
            Console.WriteLine("Initalising GL_ATI_vertex_attrib_array_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexAttribArrayObjectATI();
            loadGetVertexAttribArrayObjectfvATI();
            loadGetVertexAttribArrayObjectivATI();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glVertexAttribArrayObjectATIFunc(GLuint @index, GLint @size, GLenum @type, GLboolean @normalized, GLsizei @stride, GLuint @buffer, GLuint @offset);
        internal static glVertexAttribArrayObjectATIFunc glVertexAttribArrayObjectATIPtr;
        internal static void loadVertexAttribArrayObjectATI()
        {
            try
            {
                glVertexAttribArrayObjectATIPtr = (glVertexAttribArrayObjectATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribArrayObjectATI"), typeof(glVertexAttribArrayObjectATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribArrayObjectATI'.");
            }
        }
        public static void glVertexAttribArrayObjectATI(GLuint @index, GLint @size, GLenum @type, GLboolean @normalized, GLsizei @stride, GLuint @buffer, GLuint @offset) => glVertexAttribArrayObjectATIPtr(@index, @size, @type, @normalized, @stride, @buffer, @offset);

        internal delegate void glGetVertexAttribArrayObjectfvATIFunc(GLuint @index, GLenum @pname, GLfloat * @params);
        internal static glGetVertexAttribArrayObjectfvATIFunc glGetVertexAttribArrayObjectfvATIPtr;
        internal static void loadGetVertexAttribArrayObjectfvATI()
        {
            try
            {
                glGetVertexAttribArrayObjectfvATIPtr = (glGetVertexAttribArrayObjectfvATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribArrayObjectfvATI"), typeof(glGetVertexAttribArrayObjectfvATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribArrayObjectfvATI'.");
            }
        }
        public static void glGetVertexAttribArrayObjectfvATI(GLuint @index, GLenum @pname, GLfloat * @params) => glGetVertexAttribArrayObjectfvATIPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribArrayObjectivATIFunc(GLuint @index, GLenum @pname, GLint * @params);
        internal static glGetVertexAttribArrayObjectivATIFunc glGetVertexAttribArrayObjectivATIPtr;
        internal static void loadGetVertexAttribArrayObjectivATI()
        {
            try
            {
                glGetVertexAttribArrayObjectivATIPtr = (glGetVertexAttribArrayObjectivATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribArrayObjectivATI"), typeof(glGetVertexAttribArrayObjectivATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribArrayObjectivATI'.");
            }
        }
        public static void glGetVertexAttribArrayObjectivATI(GLuint @index, GLenum @pname, GLint * @params) => glGetVertexAttribArrayObjectivATIPtr(@index, @pname, @params);
        #endregion
    }
}
