using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_texture_env_combine3
    {
        #region Interop
        static GL_ATI_texture_env_combine3()
        {
            Console.WriteLine("Initalising GL_ATI_texture_env_combine3 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MODULATE_ADD_ATI = 0x8744;
        public static UInt32 GL_MODULATE_SIGNED_ADD_ATI = 0x8745;
        public static UInt32 GL_MODULATE_SUBTRACT_ATI = 0x8746;
        #endregion

        #region Commands
        #endregion
    }
}
