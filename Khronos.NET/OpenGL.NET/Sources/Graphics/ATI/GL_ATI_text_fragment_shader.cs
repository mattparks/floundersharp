using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_text_fragment_shader
    {
        #region Interop
        static GL_ATI_text_fragment_shader()
        {
            Console.WriteLine("Initalising GL_ATI_text_fragment_shader interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXT_FRAGMENT_SHADER_ATI = 0x8200;
        #endregion

        #region Commands
        #endregion
    }
}
