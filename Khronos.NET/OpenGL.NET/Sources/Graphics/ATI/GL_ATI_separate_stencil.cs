using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_separate_stencil
    {
        #region Interop
        static GL_ATI_separate_stencil()
        {
            Console.WriteLine("Initalising GL_ATI_separate_stencil interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadStencilOpSeparateATI();
            loadStencilFuncSeparateATI();
        }
        #endregion

        #region Enums
        public static UInt32 GL_STENCIL_BACK_FUNC_ATI = 0x8800;
        public static UInt32 GL_STENCIL_BACK_FAIL_ATI = 0x8801;
        public static UInt32 GL_STENCIL_BACK_PASS_DEPTH_FAIL_ATI = 0x8802;
        public static UInt32 GL_STENCIL_BACK_PASS_DEPTH_PASS_ATI = 0x8803;
        #endregion

        #region Commands
        internal delegate void glStencilOpSeparateATIFunc(GLenum @face, GLenum @sfail, GLenum @dpfail, GLenum @dppass);
        internal static glStencilOpSeparateATIFunc glStencilOpSeparateATIPtr;
        internal static void loadStencilOpSeparateATI()
        {
            try
            {
                glStencilOpSeparateATIPtr = (glStencilOpSeparateATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilOpSeparateATI"), typeof(glStencilOpSeparateATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilOpSeparateATI'.");
            }
        }
        public static void glStencilOpSeparateATI(GLenum @face, GLenum @sfail, GLenum @dpfail, GLenum @dppass) => glStencilOpSeparateATIPtr(@face, @sfail, @dpfail, @dppass);

        internal delegate void glStencilFuncSeparateATIFunc(GLenum @frontfunc, GLenum @backfunc, GLint @ref, GLuint @mask);
        internal static glStencilFuncSeparateATIFunc glStencilFuncSeparateATIPtr;
        internal static void loadStencilFuncSeparateATI()
        {
            try
            {
                glStencilFuncSeparateATIPtr = (glStencilFuncSeparateATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilFuncSeparateATI"), typeof(glStencilFuncSeparateATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilFuncSeparateATI'.");
            }
        }
        public static void glStencilFuncSeparateATI(GLenum @frontfunc, GLenum @backfunc, GLint @ref, GLuint @mask) => glStencilFuncSeparateATIPtr(@frontfunc, @backfunc, @ref, @mask);
        #endregion
    }
}
