using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_pn_triangles
    {
        #region Interop
        static GL_ATI_pn_triangles()
        {
            Console.WriteLine("Initalising GL_ATI_pn_triangles interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPNTrianesiATI();
            loadPNTrianesfATI();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PN_TRIANGLES_ATI = 0x87F0;
        public static UInt32 GL_MAX_PN_TRIANGLES_TESSELATION_LEVEL_ATI = 0x87F1;
        public static UInt32 GL_PN_TRIANGLES_POINT_MODE_ATI = 0x87F2;
        public static UInt32 GL_PN_TRIANGLES_NORMAL_MODE_ATI = 0x87F3;
        public static UInt32 GL_PN_TRIANGLES_TESSELATION_LEVEL_ATI = 0x87F4;
        public static UInt32 GL_PN_TRIANGLES_POINT_MODE_LINEAR_ATI = 0x87F5;
        public static UInt32 GL_PN_TRIANGLES_POINT_MODE_CUBIC_ATI = 0x87F6;
        public static UInt32 GL_PN_TRIANGLES_NORMAL_MODE_LINEAR_ATI = 0x87F7;
        public static UInt32 GL_PN_TRIANGLES_NORMAL_MODE_QUADRATIC_ATI = 0x87F8;
        #endregion

        #region Commands
        internal delegate void glPNTrianglesiATIFunc(GLenum @pname, GLint @param);
        internal static glPNTrianglesiATIFunc glPNTrianglesiATIPtr;
        internal static void loadPNTrianesiATI()
        {
            try
            {
                glPNTrianglesiATIPtr = (glPNTrianglesiATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPNTrianglesiATI"), typeof(glPNTrianglesiATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPNTrianglesiATI'.");
            }
        }
        public static void glPNTrianglesiATI(GLenum @pname, GLint @param) => glPNTrianglesiATIPtr(@pname, @param);

        internal delegate void glPNTrianglesfATIFunc(GLenum @pname, GLfloat @param);
        internal static glPNTrianglesfATIFunc glPNTrianglesfATIPtr;
        internal static void loadPNTrianesfATI()
        {
            try
            {
                glPNTrianglesfATIPtr = (glPNTrianglesfATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPNTrianglesfATI"), typeof(glPNTrianglesfATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPNTrianglesfATI'.");
            }
        }
        public static void glPNTrianglesfATI(GLenum @pname, GLfloat @param) => glPNTrianglesfATIPtr(@pname, @param);
        #endregion
    }
}
