using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_fragment_shader
    {
        #region Interop
        static GL_ATI_fragment_shader()
        {
            Console.WriteLine("Initalising GL_ATI_fragment_shader interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGenFragmentShadersATI();
            loadBindFragmentShaderATI();
            loadDeleteFragmentShaderATI();
            loadBeginFragmentShaderATI();
            loadEndFragmentShaderATI();
            loadPassTexCoordATI();
            loadSampleMapATI();
            loadColorFragmentOp1ATI();
            loadColorFragmentOp2ATI();
            loadColorFragmentOp3ATI();
            loadAlphaFragmentOp1ATI();
            loadAlphaFragmentOp2ATI();
            loadAlphaFragmentOp3ATI();
            loadSetFragmentShaderConstantATI();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAGMENT_SHADER_ATI = 0x8920;
        public static UInt32 GL_REG_0_ATI = 0x8921;
        public static UInt32 GL_REG_1_ATI = 0x8922;
        public static UInt32 GL_REG_2_ATI = 0x8923;
        public static UInt32 GL_REG_3_ATI = 0x8924;
        public static UInt32 GL_REG_4_ATI = 0x8925;
        public static UInt32 GL_REG_5_ATI = 0x8926;
        public static UInt32 GL_REG_6_ATI = 0x8927;
        public static UInt32 GL_REG_7_ATI = 0x8928;
        public static UInt32 GL_REG_8_ATI = 0x8929;
        public static UInt32 GL_REG_9_ATI = 0x892A;
        public static UInt32 GL_REG_10_ATI = 0x892B;
        public static UInt32 GL_REG_11_ATI = 0x892C;
        public static UInt32 GL_REG_12_ATI = 0x892D;
        public static UInt32 GL_REG_13_ATI = 0x892E;
        public static UInt32 GL_REG_14_ATI = 0x892F;
        public static UInt32 GL_REG_15_ATI = 0x8930;
        public static UInt32 GL_REG_16_ATI = 0x8931;
        public static UInt32 GL_REG_17_ATI = 0x8932;
        public static UInt32 GL_REG_18_ATI = 0x8933;
        public static UInt32 GL_REG_19_ATI = 0x8934;
        public static UInt32 GL_REG_20_ATI = 0x8935;
        public static UInt32 GL_REG_21_ATI = 0x8936;
        public static UInt32 GL_REG_22_ATI = 0x8937;
        public static UInt32 GL_REG_23_ATI = 0x8938;
        public static UInt32 GL_REG_24_ATI = 0x8939;
        public static UInt32 GL_REG_25_ATI = 0x893A;
        public static UInt32 GL_REG_26_ATI = 0x893B;
        public static UInt32 GL_REG_27_ATI = 0x893C;
        public static UInt32 GL_REG_28_ATI = 0x893D;
        public static UInt32 GL_REG_29_ATI = 0x893E;
        public static UInt32 GL_REG_30_ATI = 0x893F;
        public static UInt32 GL_REG_31_ATI = 0x8940;
        public static UInt32 GL_CON_0_ATI = 0x8941;
        public static UInt32 GL_CON_1_ATI = 0x8942;
        public static UInt32 GL_CON_2_ATI = 0x8943;
        public static UInt32 GL_CON_3_ATI = 0x8944;
        public static UInt32 GL_CON_4_ATI = 0x8945;
        public static UInt32 GL_CON_5_ATI = 0x8946;
        public static UInt32 GL_CON_6_ATI = 0x8947;
        public static UInt32 GL_CON_7_ATI = 0x8948;
        public static UInt32 GL_CON_8_ATI = 0x8949;
        public static UInt32 GL_CON_9_ATI = 0x894A;
        public static UInt32 GL_CON_10_ATI = 0x894B;
        public static UInt32 GL_CON_11_ATI = 0x894C;
        public static UInt32 GL_CON_12_ATI = 0x894D;
        public static UInt32 GL_CON_13_ATI = 0x894E;
        public static UInt32 GL_CON_14_ATI = 0x894F;
        public static UInt32 GL_CON_15_ATI = 0x8950;
        public static UInt32 GL_CON_16_ATI = 0x8951;
        public static UInt32 GL_CON_17_ATI = 0x8952;
        public static UInt32 GL_CON_18_ATI = 0x8953;
        public static UInt32 GL_CON_19_ATI = 0x8954;
        public static UInt32 GL_CON_20_ATI = 0x8955;
        public static UInt32 GL_CON_21_ATI = 0x8956;
        public static UInt32 GL_CON_22_ATI = 0x8957;
        public static UInt32 GL_CON_23_ATI = 0x8958;
        public static UInt32 GL_CON_24_ATI = 0x8959;
        public static UInt32 GL_CON_25_ATI = 0x895A;
        public static UInt32 GL_CON_26_ATI = 0x895B;
        public static UInt32 GL_CON_27_ATI = 0x895C;
        public static UInt32 GL_CON_28_ATI = 0x895D;
        public static UInt32 GL_CON_29_ATI = 0x895E;
        public static UInt32 GL_CON_30_ATI = 0x895F;
        public static UInt32 GL_CON_31_ATI = 0x8960;
        public static UInt32 GL_MOV_ATI = 0x8961;
        public static UInt32 GL_ADD_ATI = 0x8963;
        public static UInt32 GL_MUL_ATI = 0x8964;
        public static UInt32 GL_SUB_ATI = 0x8965;
        public static UInt32 GL_DOT3_ATI = 0x8966;
        public static UInt32 GL_DOT4_ATI = 0x8967;
        public static UInt32 GL_MAD_ATI = 0x8968;
        public static UInt32 GL_LERP_ATI = 0x8969;
        public static UInt32 GL_CND_ATI = 0x896A;
        public static UInt32 GL_CND0_ATI = 0x896B;
        public static UInt32 GL_DOT2_ADD_ATI = 0x896C;
        public static UInt32 GL_SECONDARY_INTERPOLATOR_ATI = 0x896D;
        public static UInt32 GL_NUM_FRAGMENT_REGISTERS_ATI = 0x896E;
        public static UInt32 GL_NUM_FRAGMENT_CONSTANTS_ATI = 0x896F;
        public static UInt32 GL_NUM_PASSES_ATI = 0x8970;
        public static UInt32 GL_NUM_INSTRUCTIONS_PER_PASS_ATI = 0x8971;
        public static UInt32 GL_NUM_INSTRUCTIONS_TOTAL_ATI = 0x8972;
        public static UInt32 GL_NUM_INPUT_INTERPOLATOR_COMPONENTS_ATI = 0x8973;
        public static UInt32 GL_NUM_LOOPBACK_COMPONENTS_ATI = 0x8974;
        public static UInt32 GL_COLOR_ALPHA_PAIRING_ATI = 0x8975;
        public static UInt32 GL_SWIZZLE_STR_ATI = 0x8976;
        public static UInt32 GL_SWIZZLE_STQ_ATI = 0x8977;
        public static UInt32 GL_SWIZZLE_STR_DR_ATI = 0x8978;
        public static UInt32 GL_SWIZZLE_STQ_DQ_ATI = 0x8979;
        public static UInt32 GL_SWIZZLE_STRQ_ATI = 0x897A;
        public static UInt32 GL_SWIZZLE_STRQ_DQ_ATI = 0x897B;
        public static UInt32 GL_RED_BIT_ATI = 0x00000001;
        public static UInt32 GL_GREEN_BIT_ATI = 0x00000002;
        public static UInt32 GL_BLUE_BIT_ATI = 0x00000004;
        public static UInt32 GL_2X_BIT_ATI = 0x00000001;
        public static UInt32 GL_4X_BIT_ATI = 0x00000002;
        public static UInt32 GL_8X_BIT_ATI = 0x00000004;
        public static UInt32 GL_HALF_BIT_ATI = 0x00000008;
        public static UInt32 GL_QUARTER_BIT_ATI = 0x00000010;
        public static UInt32 GL_EIGHTH_BIT_ATI = 0x00000020;
        public static UInt32 GL_SATURATE_BIT_ATI = 0x00000040;
        public static UInt32 GL_COMP_BIT_ATI = 0x00000002;
        public static UInt32 GL_NEGATE_BIT_ATI = 0x00000004;
        public static UInt32 GL_BIAS_BIT_ATI = 0x00000008;
        #endregion

        #region Commands
        internal delegate GLuint glGenFragmentShadersATIFunc(GLuint @range);
        internal static glGenFragmentShadersATIFunc glGenFragmentShadersATIPtr;
        internal static void loadGenFragmentShadersATI()
        {
            try
            {
                glGenFragmentShadersATIPtr = (glGenFragmentShadersATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenFragmentShadersATI"), typeof(glGenFragmentShadersATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenFragmentShadersATI'.");
            }
        }
        public static GLuint glGenFragmentShadersATI(GLuint @range) => glGenFragmentShadersATIPtr(@range);

        internal delegate void glBindFragmentShaderATIFunc(GLuint @id);
        internal static glBindFragmentShaderATIFunc glBindFragmentShaderATIPtr;
        internal static void loadBindFragmentShaderATI()
        {
            try
            {
                glBindFragmentShaderATIPtr = (glBindFragmentShaderATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindFragmentShaderATI"), typeof(glBindFragmentShaderATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindFragmentShaderATI'.");
            }
        }
        public static void glBindFragmentShaderATI(GLuint @id) => glBindFragmentShaderATIPtr(@id);

        internal delegate void glDeleteFragmentShaderATIFunc(GLuint @id);
        internal static glDeleteFragmentShaderATIFunc glDeleteFragmentShaderATIPtr;
        internal static void loadDeleteFragmentShaderATI()
        {
            try
            {
                glDeleteFragmentShaderATIPtr = (glDeleteFragmentShaderATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteFragmentShaderATI"), typeof(glDeleteFragmentShaderATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteFragmentShaderATI'.");
            }
        }
        public static void glDeleteFragmentShaderATI(GLuint @id) => glDeleteFragmentShaderATIPtr(@id);

        internal delegate void glBeginFragmentShaderATIFunc();
        internal static glBeginFragmentShaderATIFunc glBeginFragmentShaderATIPtr;
        internal static void loadBeginFragmentShaderATI()
        {
            try
            {
                glBeginFragmentShaderATIPtr = (glBeginFragmentShaderATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginFragmentShaderATI"), typeof(glBeginFragmentShaderATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginFragmentShaderATI'.");
            }
        }
        public static void glBeginFragmentShaderATI() => glBeginFragmentShaderATIPtr();

        internal delegate void glEndFragmentShaderATIFunc();
        internal static glEndFragmentShaderATIFunc glEndFragmentShaderATIPtr;
        internal static void loadEndFragmentShaderATI()
        {
            try
            {
                glEndFragmentShaderATIPtr = (glEndFragmentShaderATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndFragmentShaderATI"), typeof(glEndFragmentShaderATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndFragmentShaderATI'.");
            }
        }
        public static void glEndFragmentShaderATI() => glEndFragmentShaderATIPtr();

        internal delegate void glPassTexCoordATIFunc(GLuint @dst, GLuint @coord, GLenum @swizzle);
        internal static glPassTexCoordATIFunc glPassTexCoordATIPtr;
        internal static void loadPassTexCoordATI()
        {
            try
            {
                glPassTexCoordATIPtr = (glPassTexCoordATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPassTexCoordATI"), typeof(glPassTexCoordATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPassTexCoordATI'.");
            }
        }
        public static void glPassTexCoordATI(GLuint @dst, GLuint @coord, GLenum @swizzle) => glPassTexCoordATIPtr(@dst, @coord, @swizzle);

        internal delegate void glSampleMapATIFunc(GLuint @dst, GLuint @interp, GLenum @swizzle);
        internal static glSampleMapATIFunc glSampleMapATIPtr;
        internal static void loadSampleMapATI()
        {
            try
            {
                glSampleMapATIPtr = (glSampleMapATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleMapATI"), typeof(glSampleMapATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleMapATI'.");
            }
        }
        public static void glSampleMapATI(GLuint @dst, GLuint @interp, GLenum @swizzle) => glSampleMapATIPtr(@dst, @interp, @swizzle);

        internal delegate void glColorFragmentOp1ATIFunc(GLenum @op, GLuint @dst, GLuint @dstMask, GLuint @dstMod, GLuint @arg1, GLuint @arg1Rep, GLuint @arg1Mod);
        internal static glColorFragmentOp1ATIFunc glColorFragmentOp1ATIPtr;
        internal static void loadColorFragmentOp1ATI()
        {
            try
            {
                glColorFragmentOp1ATIPtr = (glColorFragmentOp1ATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorFragmentOp1ATI"), typeof(glColorFragmentOp1ATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorFragmentOp1ATI'.");
            }
        }
        public static void glColorFragmentOp1ATI(GLenum @op, GLuint @dst, GLuint @dstMask, GLuint @dstMod, GLuint @arg1, GLuint @arg1Rep, GLuint @arg1Mod) => glColorFragmentOp1ATIPtr(@op, @dst, @dstMask, @dstMod, @arg1, @arg1Rep, @arg1Mod);

        internal delegate void glColorFragmentOp2ATIFunc(GLenum @op, GLuint @dst, GLuint @dstMask, GLuint @dstMod, GLuint @arg1, GLuint @arg1Rep, GLuint @arg1Mod, GLuint @arg2, GLuint @arg2Rep, GLuint @arg2Mod);
        internal static glColorFragmentOp2ATIFunc glColorFragmentOp2ATIPtr;
        internal static void loadColorFragmentOp2ATI()
        {
            try
            {
                glColorFragmentOp2ATIPtr = (glColorFragmentOp2ATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorFragmentOp2ATI"), typeof(glColorFragmentOp2ATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorFragmentOp2ATI'.");
            }
        }
        public static void glColorFragmentOp2ATI(GLenum @op, GLuint @dst, GLuint @dstMask, GLuint @dstMod, GLuint @arg1, GLuint @arg1Rep, GLuint @arg1Mod, GLuint @arg2, GLuint @arg2Rep, GLuint @arg2Mod) => glColorFragmentOp2ATIPtr(@op, @dst, @dstMask, @dstMod, @arg1, @arg1Rep, @arg1Mod, @arg2, @arg2Rep, @arg2Mod);

        internal delegate void glColorFragmentOp3ATIFunc(GLenum @op, GLuint @dst, GLuint @dstMask, GLuint @dstMod, GLuint @arg1, GLuint @arg1Rep, GLuint @arg1Mod, GLuint @arg2, GLuint @arg2Rep, GLuint @arg2Mod, GLuint @arg3, GLuint @arg3Rep, GLuint @arg3Mod);
        internal static glColorFragmentOp3ATIFunc glColorFragmentOp3ATIPtr;
        internal static void loadColorFragmentOp3ATI()
        {
            try
            {
                glColorFragmentOp3ATIPtr = (glColorFragmentOp3ATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorFragmentOp3ATI"), typeof(glColorFragmentOp3ATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorFragmentOp3ATI'.");
            }
        }
        public static void glColorFragmentOp3ATI(GLenum @op, GLuint @dst, GLuint @dstMask, GLuint @dstMod, GLuint @arg1, GLuint @arg1Rep, GLuint @arg1Mod, GLuint @arg2, GLuint @arg2Rep, GLuint @arg2Mod, GLuint @arg3, GLuint @arg3Rep, GLuint @arg3Mod) => glColorFragmentOp3ATIPtr(@op, @dst, @dstMask, @dstMod, @arg1, @arg1Rep, @arg1Mod, @arg2, @arg2Rep, @arg2Mod, @arg3, @arg3Rep, @arg3Mod);

        internal delegate void glAlphaFragmentOp1ATIFunc(GLenum @op, GLuint @dst, GLuint @dstMod, GLuint @arg1, GLuint @arg1Rep, GLuint @arg1Mod);
        internal static glAlphaFragmentOp1ATIFunc glAlphaFragmentOp1ATIPtr;
        internal static void loadAlphaFragmentOp1ATI()
        {
            try
            {
                glAlphaFragmentOp1ATIPtr = (glAlphaFragmentOp1ATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAlphaFragmentOp1ATI"), typeof(glAlphaFragmentOp1ATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAlphaFragmentOp1ATI'.");
            }
        }
        public static void glAlphaFragmentOp1ATI(GLenum @op, GLuint @dst, GLuint @dstMod, GLuint @arg1, GLuint @arg1Rep, GLuint @arg1Mod) => glAlphaFragmentOp1ATIPtr(@op, @dst, @dstMod, @arg1, @arg1Rep, @arg1Mod);

        internal delegate void glAlphaFragmentOp2ATIFunc(GLenum @op, GLuint @dst, GLuint @dstMod, GLuint @arg1, GLuint @arg1Rep, GLuint @arg1Mod, GLuint @arg2, GLuint @arg2Rep, GLuint @arg2Mod);
        internal static glAlphaFragmentOp2ATIFunc glAlphaFragmentOp2ATIPtr;
        internal static void loadAlphaFragmentOp2ATI()
        {
            try
            {
                glAlphaFragmentOp2ATIPtr = (glAlphaFragmentOp2ATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAlphaFragmentOp2ATI"), typeof(glAlphaFragmentOp2ATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAlphaFragmentOp2ATI'.");
            }
        }
        public static void glAlphaFragmentOp2ATI(GLenum @op, GLuint @dst, GLuint @dstMod, GLuint @arg1, GLuint @arg1Rep, GLuint @arg1Mod, GLuint @arg2, GLuint @arg2Rep, GLuint @arg2Mod) => glAlphaFragmentOp2ATIPtr(@op, @dst, @dstMod, @arg1, @arg1Rep, @arg1Mod, @arg2, @arg2Rep, @arg2Mod);

        internal delegate void glAlphaFragmentOp3ATIFunc(GLenum @op, GLuint @dst, GLuint @dstMod, GLuint @arg1, GLuint @arg1Rep, GLuint @arg1Mod, GLuint @arg2, GLuint @arg2Rep, GLuint @arg2Mod, GLuint @arg3, GLuint @arg3Rep, GLuint @arg3Mod);
        internal static glAlphaFragmentOp3ATIFunc glAlphaFragmentOp3ATIPtr;
        internal static void loadAlphaFragmentOp3ATI()
        {
            try
            {
                glAlphaFragmentOp3ATIPtr = (glAlphaFragmentOp3ATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAlphaFragmentOp3ATI"), typeof(glAlphaFragmentOp3ATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAlphaFragmentOp3ATI'.");
            }
        }
        public static void glAlphaFragmentOp3ATI(GLenum @op, GLuint @dst, GLuint @dstMod, GLuint @arg1, GLuint @arg1Rep, GLuint @arg1Mod, GLuint @arg2, GLuint @arg2Rep, GLuint @arg2Mod, GLuint @arg3, GLuint @arg3Rep, GLuint @arg3Mod) => glAlphaFragmentOp3ATIPtr(@op, @dst, @dstMod, @arg1, @arg1Rep, @arg1Mod, @arg2, @arg2Rep, @arg2Mod, @arg3, @arg3Rep, @arg3Mod);

        internal delegate void glSetFragmentShaderConstantATIFunc(GLuint @dst, const GLfloat * @value);
        internal static glSetFragmentShaderConstantATIFunc glSetFragmentShaderConstantATIPtr;
        internal static void loadSetFragmentShaderConstantATI()
        {
            try
            {
                glSetFragmentShaderConstantATIPtr = (glSetFragmentShaderConstantATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSetFragmentShaderConstantATI"), typeof(glSetFragmentShaderConstantATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSetFragmentShaderConstantATI'.");
            }
        }
        public static void glSetFragmentShaderConstantATI(GLuint @dst, const GLfloat * @value) => glSetFragmentShaderConstantATIPtr(@dst, @value);
        #endregion
    }
}
