using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_map_object_buffer
    {
        #region Interop
        static GL_ATI_map_object_buffer()
        {
            Console.WriteLine("Initalising GL_ATI_map_object_buffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMapObjectBufferATI();
            loadUnmapObjectBufferATI();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void * glMapObjectBufferATIFunc(GLuint @buffer);
        internal static glMapObjectBufferATIFunc glMapObjectBufferATIPtr;
        internal static void loadMapObjectBufferATI()
        {
            try
            {
                glMapObjectBufferATIPtr = (glMapObjectBufferATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapObjectBufferATI"), typeof(glMapObjectBufferATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapObjectBufferATI'.");
            }
        }
        public static void * glMapObjectBufferATI(GLuint @buffer) => glMapObjectBufferATIPtr(@buffer);

        internal delegate void glUnmapObjectBufferATIFunc(GLuint @buffer);
        internal static glUnmapObjectBufferATIFunc glUnmapObjectBufferATIPtr;
        internal static void loadUnmapObjectBufferATI()
        {
            try
            {
                glUnmapObjectBufferATIPtr = (glUnmapObjectBufferATIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUnmapObjectBufferATI"), typeof(glUnmapObjectBufferATIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUnmapObjectBufferATI'.");
            }
        }
        public static void glUnmapObjectBufferATI(GLuint @buffer) => glUnmapObjectBufferATIPtr(@buffer);
        #endregion
    }
}
