using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_texture_float
    {
        #region Interop
        static GL_ATI_texture_float()
        {
            Console.WriteLine("Initalising GL_ATI_texture_float interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RGBA_FLOAT32_ATI = 0x8814;
        public static UInt32 GL_RGB_FLOAT32_ATI = 0x8815;
        public static UInt32 GL_ALPHA_FLOAT32_ATI = 0x8816;
        public static UInt32 GL_INTENSITY_FLOAT32_ATI = 0x8817;
        public static UInt32 GL_LUMINANCE_FLOAT32_ATI = 0x8818;
        public static UInt32 GL_LUMINANCE_ALPHA_FLOAT32_ATI = 0x8819;
        public static UInt32 GL_RGBA_FLOAT16_ATI = 0x881A;
        public static UInt32 GL_RGB_FLOAT16_ATI = 0x881B;
        public static UInt32 GL_ALPHA_FLOAT16_ATI = 0x881C;
        public static UInt32 GL_INTENSITY_FLOAT16_ATI = 0x881D;
        public static UInt32 GL_LUMINANCE_FLOAT16_ATI = 0x881E;
        public static UInt32 GL_LUMINANCE_ALPHA_FLOAT16_ATI = 0x881F;
        #endregion

        #region Commands
        #endregion
    }
}
