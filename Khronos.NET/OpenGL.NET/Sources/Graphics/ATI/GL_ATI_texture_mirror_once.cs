using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ATI_texture_mirror_once
    {
        #region Interop
        static GL_ATI_texture_mirror_once()
        {
            Console.WriteLine("Initalising GL_ATI_texture_mirror_once interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MIRROR_CLAMP_ATI = 0x8742;
        public static UInt32 GL_MIRROR_CLAMP_TO_EDGE_ATI = 0x8743;
        #endregion

        #region Commands
        #endregion
    }
}
