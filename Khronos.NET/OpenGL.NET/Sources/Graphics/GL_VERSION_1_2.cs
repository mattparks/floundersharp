using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL12
    {
        #region Interop
        static GL12()
        {
            Console.WriteLine("Initalising GL12 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawRangeElements();
            loadTexImage3D();
            loadTexSubImage3D();
            loadCopyTexSubImage3D();
        }
        #endregion

        #region Enums
        public static UInt32 GL_UNSIGNED_BYTE_3_3_2 = 0x8032;
        public static UInt32 GL_UNSIGNED_SHORT_4_4_4_4 = 0x8033;
        public static UInt32 GL_UNSIGNED_SHORT_5_5_5_1 = 0x8034;
        public static UInt32 GL_UNSIGNED_INT_8_8_8_8 = 0x8035;
        public static UInt32 GL_UNSIGNED_INT_10_10_10_2 = 0x8036;
        public static UInt32 GL_TEXTURE_BINDING_3D = 0x806A;
        public static UInt32 GL_PACK_SKIP_IMAGES = 0x806B;
        public static UInt32 GL_PACK_IMAGE_HEIGHT = 0x806C;
        public static UInt32 GL_UNPACK_SKIP_IMAGES = 0x806D;
        public static UInt32 GL_UNPACK_IMAGE_HEIGHT = 0x806E;
        public static UInt32 GL_TEXTURE_3D = 0x806F;
        public static UInt32 GL_PROXY_TEXTURE_3D = 0x8070;
        public static UInt32 GL_TEXTURE_DEPTH = 0x8071;
        public static UInt32 GL_TEXTURE_WRAP_R = 0x8072;
        public static UInt32 GL_MAX_3D_TEXTURE_SIZE = 0x8073;
        public static UInt32 GL_UNSIGNED_BYTE_2_3_3_REV = 0x8362;
        public static UInt32 GL_UNSIGNED_SHORT_5_6_5 = 0x8363;
        public static UInt32 GL_UNSIGNED_SHORT_5_6_5_REV = 0x8364;
        public static UInt32 GL_UNSIGNED_SHORT_4_4_4_4_REV = 0x8365;
        public static UInt32 GL_UNSIGNED_SHORT_1_5_5_5_REV = 0x8366;
        public static UInt32 GL_UNSIGNED_INT_8_8_8_8_REV = 0x8367;
        public static UInt32 GL_UNSIGNED_INT_2_10_10_10_REV = 0x8368;
        public static UInt32 GL_BGR = 0x80E0;
        public static UInt32 GL_BGRA = 0x80E1;
        public static UInt32 GL_MAX_ELEMENTS_VERTICES = 0x80E8;
        public static UInt32 GL_MAX_ELEMENTS_INDICES = 0x80E9;
        public static UInt32 GL_CLAMP_TO_EDGE = 0x812F;
        public static UInt32 GL_TEXTURE_MIN_LOD = 0x813A;
        public static UInt32 GL_TEXTURE_MAX_LOD = 0x813B;
        public static UInt32 GL_TEXTURE_BASE_LEVEL = 0x813C;
        public static UInt32 GL_TEXTURE_MAX_LEVEL = 0x813D;
        public static UInt32 GL_SMOOTH_POINT_SIZE_RANGE = 0x0B12;
        public static UInt32 GL_SMOOTH_POINT_SIZE_GRANULARITY = 0x0B13;
        public static UInt32 GL_SMOOTH_LINE_WIDTH_RANGE = 0x0B22;
        public static UInt32 GL_SMOOTH_LINE_WIDTH_GRANULARITY = 0x0B23;
        public static UInt32 GL_ALIASED_LINE_WIDTH_RANGE = 0x846E;
        public static UInt32 GL_RESCALE_NORMAL = 0x803A;
        public static UInt32 GL_LIGHT_MODEL_COLOR_CONTROL = 0x81F8;
        public static UInt32 GL_SINGLE_COLOR = 0x81F9;
        public static UInt32 GL_SEPARATE_SPECULAR_COLOR = 0x81FA;
        public static UInt32 GL_ALIASED_POINT_SIZE_RANGE = 0x846D;
        #endregion

        #region Commands
        internal delegate void glDrawRangeElementsFunc(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices);
        internal static glDrawRangeElementsFunc glDrawRangeElementsPtr;
        internal static void loadDrawRangeElements()
        {
            try
            {
                glDrawRangeElementsPtr = (glDrawRangeElementsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawRangeElements"), typeof(glDrawRangeElementsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawRangeElements'.");
            }
        }
        public static void glDrawRangeElements(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices) => glDrawRangeElementsPtr(@mode, @start, @end, @count, @type, @indices);

        internal delegate void glTexImage3DFunc(GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexImage3DFunc glTexImage3DPtr;
        internal static void loadTexImage3D()
        {
            try
            {
                glTexImage3DPtr = (glTexImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage3D"), typeof(glTexImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage3D'.");
            }
        }
        public static void glTexImage3D(GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glTexImage3DPtr(@target, @level, @internalformat, @width, @height, @depth, @border, @format, @type, @pixels);

        internal delegate void glTexSubImage3DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexSubImage3DFunc glTexSubImage3DPtr;
        internal static void loadTexSubImage3D()
        {
            try
            {
                glTexSubImage3DPtr = (glTexSubImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexSubImage3D"), typeof(glTexSubImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexSubImage3D'.");
            }
        }
        public static void glTexSubImage3D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels) => glTexSubImage3DPtr(@target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @type, @pixels);

        internal delegate void glCopyTexSubImage3DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyTexSubImage3DFunc glCopyTexSubImage3DPtr;
        internal static void loadCopyTexSubImage3D()
        {
            try
            {
                glCopyTexSubImage3DPtr = (glCopyTexSubImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexSubImage3D"), typeof(glCopyTexSubImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexSubImage3D'.");
            }
        }
        public static void glCopyTexSubImage3D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyTexSubImage3DPtr(@target, @level, @xoffset, @yoffset, @zoffset, @x, @y, @width, @height);
        #endregion
    }
}
