using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IBM_cull_vertex
    {
        #region Interop
        static GL_IBM_cull_vertex()
        {
            Console.WriteLine("Initalising GL_IBM_cull_vertex interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_CULL_VERTEX_IBM = 103050;
        #endregion

        #region Commands
        #endregion
    }
}
