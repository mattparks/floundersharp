using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IBM_static_data
    {
        #region Interop
        static GL_IBM_static_data()
        {
            Console.WriteLine("Initalising GL_IBM_static_data interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFlushStaticDataIBM();
        }
        #endregion

        #region Enums
        public static UInt32 GL_ALL_STATIC_DATA_IBM = 103060;
        public static UInt32 GL_STATIC_VERTEX_ARRAY_IBM = 103061;
        #endregion

        #region Commands
        internal delegate void glFlushStaticDataIBMFunc(GLenum @target);
        internal static glFlushStaticDataIBMFunc glFlushStaticDataIBMPtr;
        internal static void loadFlushStaticDataIBM()
        {
            try
            {
                glFlushStaticDataIBMPtr = (glFlushStaticDataIBMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlushStaticDataIBM"), typeof(glFlushStaticDataIBMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlushStaticDataIBM'.");
            }
        }
        public static void glFlushStaticDataIBM(GLenum @target) => glFlushStaticDataIBMPtr(@target);
        #endregion
    }
}
