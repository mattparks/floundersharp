using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IBM_multimode_draw_arrays
    {
        #region Interop
        static GL_IBM_multimode_draw_arrays()
        {
            Console.WriteLine("Initalising GL_IBM_multimode_draw_arrays interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMultiModeDrawArraysIBM();
            loadMultiModeDrawElementsIBM();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glMultiModeDrawArraysIBMFunc(const GLenum * @mode, const GLint * @first, const GLsizei * @count, GLsizei @primcount, GLint @modestride);
        internal static glMultiModeDrawArraysIBMFunc glMultiModeDrawArraysIBMPtr;
        internal static void loadMultiModeDrawArraysIBM()
        {
            try
            {
                glMultiModeDrawArraysIBMPtr = (glMultiModeDrawArraysIBMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiModeDrawArraysIBM"), typeof(glMultiModeDrawArraysIBMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiModeDrawArraysIBM'.");
            }
        }
        public static void glMultiModeDrawArraysIBM(const GLenum * @mode, const GLint * @first, const GLsizei * @count, GLsizei @primcount, GLint @modestride) => glMultiModeDrawArraysIBMPtr(@mode, @first, @count, @primcount, @modestride);

        internal delegate void glMultiModeDrawElementsIBMFunc(const GLenum * @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @primcount, GLint @modestride);
        internal static glMultiModeDrawElementsIBMFunc glMultiModeDrawElementsIBMPtr;
        internal static void loadMultiModeDrawElementsIBM()
        {
            try
            {
                glMultiModeDrawElementsIBMPtr = (glMultiModeDrawElementsIBMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiModeDrawElementsIBM"), typeof(glMultiModeDrawElementsIBMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiModeDrawElementsIBM'.");
            }
        }
        public static void glMultiModeDrawElementsIBM(const GLenum * @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @primcount, GLint @modestride) => glMultiModeDrawElementsIBMPtr(@mode, @count, @type, @indices, @primcount, @modestride);
        #endregion
    }
}
