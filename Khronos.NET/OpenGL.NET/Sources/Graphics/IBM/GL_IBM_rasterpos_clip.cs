using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IBM_rasterpos_clip
    {
        #region Interop
        static GL_IBM_rasterpos_clip()
        {
            Console.WriteLine("Initalising GL_IBM_rasterpos_clip interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RASTER_POSITION_UNCLIPPED_IBM = 0x19262;
        #endregion

        #region Commands
        #endregion
    }
}
