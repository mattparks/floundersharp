using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IBM_texture_mirrored_repeat
    {
        #region Interop
        static GL_IBM_texture_mirrored_repeat()
        {
            Console.WriteLine("Initalising GL_IBM_texture_mirrored_repeat interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MIRRORED_REPEAT_IBM = 0x8370;
        #endregion

        #region Commands
        #endregion
    }
}
