using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_IBM_vertex_array_lists
    {
        #region Interop
        static GL_IBM_vertex_array_lists()
        {
            Console.WriteLine("Initalising GL_IBM_vertex_array_lists interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadColorPointerListIBM();
            loadSecondaryColorPointerListIBM();
            loadEdgeFlagPointerListIBM();
            loadFogCoordPointerListIBM();
            loadIndexPointerListIBM();
            loadNormalPointerListIBM();
            loadTexCoordPointerListIBM();
            loadVertexPointerListIBM();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ARRAY_LIST_IBM = 103070;
        public static UInt32 GL_NORMAL_ARRAY_LIST_IBM = 103071;
        public static UInt32 GL_COLOR_ARRAY_LIST_IBM = 103072;
        public static UInt32 GL_INDEX_ARRAY_LIST_IBM = 103073;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_LIST_IBM = 103074;
        public static UInt32 GL_EDGE_FLAG_ARRAY_LIST_IBM = 103075;
        public static UInt32 GL_FOG_COORDINATE_ARRAY_LIST_IBM = 103076;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_LIST_IBM = 103077;
        public static UInt32 GL_VERTEX_ARRAY_LIST_STRIDE_IBM = 103080;
        public static UInt32 GL_NORMAL_ARRAY_LIST_STRIDE_IBM = 103081;
        public static UInt32 GL_COLOR_ARRAY_LIST_STRIDE_IBM = 103082;
        public static UInt32 GL_INDEX_ARRAY_LIST_STRIDE_IBM = 103083;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_LIST_STRIDE_IBM = 103084;
        public static UInt32 GL_EDGE_FLAG_ARRAY_LIST_STRIDE_IBM = 103085;
        public static UInt32 GL_FOG_COORDINATE_ARRAY_LIST_STRIDE_IBM = 103086;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_LIST_STRIDE_IBM = 103087;
        #endregion

        #region Commands
        internal delegate void glColorPointerListIBMFunc(GLint @size, GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride);
        internal static glColorPointerListIBMFunc glColorPointerListIBMPtr;
        internal static void loadColorPointerListIBM()
        {
            try
            {
                glColorPointerListIBMPtr = (glColorPointerListIBMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorPointerListIBM"), typeof(glColorPointerListIBMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorPointerListIBM'.");
            }
        }
        public static void glColorPointerListIBM(GLint @size, GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride) => glColorPointerListIBMPtr(@size, @type, @stride, @pointer, @ptrstride);

        internal delegate void glSecondaryColorPointerListIBMFunc(GLint @size, GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride);
        internal static glSecondaryColorPointerListIBMFunc glSecondaryColorPointerListIBMPtr;
        internal static void loadSecondaryColorPointerListIBM()
        {
            try
            {
                glSecondaryColorPointerListIBMPtr = (glSecondaryColorPointerListIBMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColorPointerListIBM"), typeof(glSecondaryColorPointerListIBMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColorPointerListIBM'.");
            }
        }
        public static void glSecondaryColorPointerListIBM(GLint @size, GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride) => glSecondaryColorPointerListIBMPtr(@size, @type, @stride, @pointer, @ptrstride);

        internal delegate void glEdgeFlagPointerListIBMFunc(GLint @stride, const GLboolean ** @pointer, GLint @ptrstride);
        internal static glEdgeFlagPointerListIBMFunc glEdgeFlagPointerListIBMPtr;
        internal static void loadEdgeFlagPointerListIBM()
        {
            try
            {
                glEdgeFlagPointerListIBMPtr = (glEdgeFlagPointerListIBMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEdgeFlagPointerListIBM"), typeof(glEdgeFlagPointerListIBMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEdgeFlagPointerListIBM'.");
            }
        }
        public static void glEdgeFlagPointerListIBM(GLint @stride, const GLboolean ** @pointer, GLint @ptrstride) => glEdgeFlagPointerListIBMPtr(@stride, @pointer, @ptrstride);

        internal delegate void glFogCoordPointerListIBMFunc(GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride);
        internal static glFogCoordPointerListIBMFunc glFogCoordPointerListIBMPtr;
        internal static void loadFogCoordPointerListIBM()
        {
            try
            {
                glFogCoordPointerListIBMPtr = (glFogCoordPointerListIBMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoordPointerListIBM"), typeof(glFogCoordPointerListIBMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoordPointerListIBM'.");
            }
        }
        public static void glFogCoordPointerListIBM(GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride) => glFogCoordPointerListIBMPtr(@type, @stride, @pointer, @ptrstride);

        internal delegate void glIndexPointerListIBMFunc(GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride);
        internal static glIndexPointerListIBMFunc glIndexPointerListIBMPtr;
        internal static void loadIndexPointerListIBM()
        {
            try
            {
                glIndexPointerListIBMPtr = (glIndexPointerListIBMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexPointerListIBM"), typeof(glIndexPointerListIBMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexPointerListIBM'.");
            }
        }
        public static void glIndexPointerListIBM(GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride) => glIndexPointerListIBMPtr(@type, @stride, @pointer, @ptrstride);

        internal delegate void glNormalPointerListIBMFunc(GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride);
        internal static glNormalPointerListIBMFunc glNormalPointerListIBMPtr;
        internal static void loadNormalPointerListIBM()
        {
            try
            {
                glNormalPointerListIBMPtr = (glNormalPointerListIBMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalPointerListIBM"), typeof(glNormalPointerListIBMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalPointerListIBM'.");
            }
        }
        public static void glNormalPointerListIBM(GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride) => glNormalPointerListIBMPtr(@type, @stride, @pointer, @ptrstride);

        internal delegate void glTexCoordPointerListIBMFunc(GLint @size, GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride);
        internal static glTexCoordPointerListIBMFunc glTexCoordPointerListIBMPtr;
        internal static void loadTexCoordPointerListIBM()
        {
            try
            {
                glTexCoordPointerListIBMPtr = (glTexCoordPointerListIBMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordPointerListIBM"), typeof(glTexCoordPointerListIBMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordPointerListIBM'.");
            }
        }
        public static void glTexCoordPointerListIBM(GLint @size, GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride) => glTexCoordPointerListIBMPtr(@size, @type, @stride, @pointer, @ptrstride);

        internal delegate void glVertexPointerListIBMFunc(GLint @size, GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride);
        internal static glVertexPointerListIBMFunc glVertexPointerListIBMPtr;
        internal static void loadVertexPointerListIBM()
        {
            try
            {
                glVertexPointerListIBMPtr = (glVertexPointerListIBMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexPointerListIBM"), typeof(glVertexPointerListIBMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexPointerListIBM'.");
            }
        }
        public static void glVertexPointerListIBM(GLint @size, GLenum @type, GLint @stride, const void ** @pointer, GLint @ptrstride) => glVertexPointerListIBMPtr(@size, @type, @stride, @pointer, @ptrstride);
        #endregion
    }
}
