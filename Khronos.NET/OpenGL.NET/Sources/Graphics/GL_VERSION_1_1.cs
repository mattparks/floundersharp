using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL11
    {
        #region Interop
        static GL11()
        {
            Console.WriteLine("Initalising GL11 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawArrays();
            loadDrawElements();
            loadGetPointerv();
            loadPolygonOffset();
            loadCopyTexImage1D();
            loadCopyTexImage2D();
            loadCopyTexSubImage1D();
            loadCopyTexSubImage2D();
            loadTexSubImage1D();
            loadTexSubImage2D();
            loadBindTexture();
            loadDeleteTextures();
            loadGenTextures();
            loadIsTexture();
            loadArrayElement();
            loadColorPointer();
            loadDisableClientState();
            loadEdgeFlagPointer();
            loadEnableClientState();
            loadIndexPointer();
            loadInterleavedArrays();
            loadNormalPointer();
            loadTexCoordPointer();
            loadVertexPointer();
            loadAreTexturesResident();
            loadPrioritizeTextures();
            loadIndexub();
            loadIndexubv();
            loadPopClientAttrib();
            loadPushClientAttrib();
        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_BUFFER_BIT = 0x00000100;
        public static UInt32 GL_STENCIL_BUFFER_BIT = 0x00000400;
        public static UInt32 GL_COLOR_BUFFER_BIT = 0x00004000;
        public static UInt32 GL_FALSE = 0;
        public static UInt32 GL_TRUE = 1;
        public static UInt32 GL_POINTS = 0x0000;
        public static UInt32 GL_LINES = 0x0001;
        public static UInt32 GL_LINE_LOOP = 0x0002;
        public static UInt32 GL_LINE_STRIP = 0x0003;
        public static UInt32 GL_TRIANGLES = 0x0004;
        public static UInt32 GL_TRIANGLE_STRIP = 0x0005;
        public static UInt32 GL_TRIANGLE_FAN = 0x0006;
        public static UInt32 GL_QUADS = 0x0007;
        public static UInt32 GL_NEVER = 0x0200;
        public static UInt32 GL_LESS = 0x0201;
        public static UInt32 GL_EQUAL = 0x0202;
        public static UInt32 GL_LEQUAL = 0x0203;
        public static UInt32 GL_GREATER = 0x0204;
        public static UInt32 GL_NOTEQUAL = 0x0205;
        public static UInt32 GL_GEQUAL = 0x0206;
        public static UInt32 GL_ALWAYS = 0x0207;
        public static UInt32 GL_ZERO = 0;
        public static UInt32 GL_ONE = 1;
        public static UInt32 GL_SRC_COLOR = 0x0300;
        public static UInt32 GL_ONE_MINUS_SRC_COLOR = 0x0301;
        public static UInt32 GL_SRC_ALPHA = 0x0302;
        public static UInt32 GL_ONE_MINUS_SRC_ALPHA = 0x0303;
        public static UInt32 GL_DST_ALPHA = 0x0304;
        public static UInt32 GL_ONE_MINUS_DST_ALPHA = 0x0305;
        public static UInt32 GL_DST_COLOR = 0x0306;
        public static UInt32 GL_ONE_MINUS_DST_COLOR = 0x0307;
        public static UInt32 GL_SRC_ALPHA_SATURATE = 0x0308;
        public static UInt32 GL_NONE = 0;
        public static UInt32 GL_FRONT_LEFT = 0x0400;
        public static UInt32 GL_FRONT_RIGHT = 0x0401;
        public static UInt32 GL_BACK_LEFT = 0x0402;
        public static UInt32 GL_BACK_RIGHT = 0x0403;
        public static UInt32 GL_FRONT = 0x0404;
        public static UInt32 GL_BACK = 0x0405;
        public static UInt32 GL_LEFT = 0x0406;
        public static UInt32 GL_RIGHT = 0x0407;
        public static UInt32 GL_FRONT_AND_BACK = 0x0408;
        public static UInt32 GL_NO_ERROR = 0;
        public static UInt32 GL_INVALID_ENUM = 0x0500;
        public static UInt32 GL_INVALID_VALUE = 0x0501;
        public static UInt32 GL_INVALID_OPERATION = 0x0502;
        public static UInt32 GL_OUT_OF_MEMORY = 0x0505;
        public static UInt32 GL_CW = 0x0900;
        public static UInt32 GL_CCW = 0x0901;
        public static UInt32 GL_POINT_SIZE = 0x0B11;
        public static UInt32 GL_POINT_SIZE_RANGE = 0x0B12;
        public static UInt32 GL_POINT_SIZE_GRANULARITY = 0x0B13;
        public static UInt32 GL_LINE_SMOOTH = 0x0B20;
        public static UInt32 GL_LINE_WIDTH = 0x0B21;
        public static UInt32 GL_LINE_WIDTH_RANGE = 0x0B22;
        public static UInt32 GL_LINE_WIDTH_GRANULARITY = 0x0B23;
        public static UInt32 GL_POLYGON_MODE = 0x0B40;
        public static UInt32 GL_POLYGON_SMOOTH = 0x0B41;
        public static UInt32 GL_CULL_FACE = 0x0B44;
        public static UInt32 GL_CULL_FACE_MODE = 0x0B45;
        public static UInt32 GL_FRONT_FACE = 0x0B46;
        public static UInt32 GL_DEPTH_RANGE = 0x0B70;
        public static UInt32 GL_DEPTH_TEST = 0x0B71;
        public static UInt32 GL_DEPTH_WRITEMASK = 0x0B72;
        public static UInt32 GL_DEPTH_CLEAR_VALUE = 0x0B73;
        public static UInt32 GL_DEPTH_FUNC = 0x0B74;
        public static UInt32 GL_STENCIL_TEST = 0x0B90;
        public static UInt32 GL_STENCIL_CLEAR_VALUE = 0x0B91;
        public static UInt32 GL_STENCIL_FUNC = 0x0B92;
        public static UInt32 GL_STENCIL_VALUE_MASK = 0x0B93;
        public static UInt32 GL_STENCIL_FAIL = 0x0B94;
        public static UInt32 GL_STENCIL_PASS_DEPTH_FAIL = 0x0B95;
        public static UInt32 GL_STENCIL_PASS_DEPTH_PASS = 0x0B96;
        public static UInt32 GL_STENCIL_REF = 0x0B97;
        public static UInt32 GL_STENCIL_WRITEMASK = 0x0B98;
        public static UInt32 GL_VIEWPORT = 0x0BA2;
        public static UInt32 GL_DITHER = 0x0BD0;
        public static UInt32 GL_BLEND_DST = 0x0BE0;
        public static UInt32 GL_BLEND_SRC = 0x0BE1;
        public static UInt32 GL_BLEND = 0x0BE2;
        public static UInt32 GL_LOGIC_OP_MODE = 0x0BF0;
        public static UInt32 GL_COLOR_LOGIC_OP = 0x0BF2;
        public static UInt32 GL_DRAW_BUFFER = 0x0C01;
        public static UInt32 GL_READ_BUFFER = 0x0C02;
        public static UInt32 GL_SCISSOR_BOX = 0x0C10;
        public static UInt32 GL_SCISSOR_TEST = 0x0C11;
        public static UInt32 GL_COLOR_CLEAR_VALUE = 0x0C22;
        public static UInt32 GL_COLOR_WRITEMASK = 0x0C23;
        public static UInt32 GL_DOUBLEBUFFER = 0x0C32;
        public static UInt32 GL_STEREO = 0x0C33;
        public static UInt32 GL_LINE_SMOOTH_HINT = 0x0C52;
        public static UInt32 GL_POLYGON_SMOOTH_HINT = 0x0C53;
        public static UInt32 GL_UNPACK_SWAP_BYTES = 0x0CF0;
        public static UInt32 GL_UNPACK_LSB_FIRST = 0x0CF1;
        public static UInt32 GL_UNPACK_ROW_LENGTH = 0x0CF2;
        public static UInt32 GL_UNPACK_SKIP_ROWS = 0x0CF3;
        public static UInt32 GL_UNPACK_SKIP_PIXELS = 0x0CF4;
        public static UInt32 GL_UNPACK_ALIGNMENT = 0x0CF5;
        public static UInt32 GL_PACK_SWAP_BYTES = 0x0D00;
        public static UInt32 GL_PACK_LSB_FIRST = 0x0D01;
        public static UInt32 GL_PACK_ROW_LENGTH = 0x0D02;
        public static UInt32 GL_PACK_SKIP_ROWS = 0x0D03;
        public static UInt32 GL_PACK_SKIP_PIXELS = 0x0D04;
        public static UInt32 GL_PACK_ALIGNMENT = 0x0D05;
        public static UInt32 GL_MAX_TEXTURE_SIZE = 0x0D33;
        public static UInt32 GL_MAX_VIEWPORT_DIMS = 0x0D3A;
        public static UInt32 GL_SUBPIXEL_BITS = 0x0D50;
        public static UInt32 GL_TEXTURE_1D = 0x0DE0;
        public static UInt32 GL_TEXTURE_2D = 0x0DE1;
        public static UInt32 GL_POLYGON_OFFSET_UNITS = 0x2A00;
        public static UInt32 GL_POLYGON_OFFSET_POINT = 0x2A01;
        public static UInt32 GL_POLYGON_OFFSET_LINE = 0x2A02;
        public static UInt32 GL_POLYGON_OFFSET_FILL = 0x8037;
        public static UInt32 GL_POLYGON_OFFSET_FACTOR = 0x8038;
        public static UInt32 GL_TEXTURE_BINDING_1D = 0x8068;
        public static UInt32 GL_TEXTURE_BINDING_2D = 0x8069;
        public static UInt32 GL_TEXTURE_WIDTH = 0x1000;
        public static UInt32 GL_TEXTURE_HEIGHT = 0x1001;
        public static UInt32 GL_TEXTURE_INTERNAL_FORMAT = 0x1003;
        public static UInt32 GL_TEXTURE_BORDER_COLOR = 0x1004;
        public static UInt32 GL_TEXTURE_RED_SIZE = 0x805C;
        public static UInt32 GL_TEXTURE_GREEN_SIZE = 0x805D;
        public static UInt32 GL_TEXTURE_BLUE_SIZE = 0x805E;
        public static UInt32 GL_TEXTURE_ALPHA_SIZE = 0x805F;
        public static UInt32 GL_DONT_CARE = 0x1100;
        public static UInt32 GL_FASTEST = 0x1101;
        public static UInt32 GL_NICEST = 0x1102;
        public static UInt32 GL_BYTE = 0x1400;
        public static UInt32 GL_UNSIGNED_BYTE = 0x1401;
        public static UInt32 GL_SHORT = 0x1402;
        public static UInt32 GL_UNSIGNED_SHORT = 0x1403;
        public static UInt32 GL_INT = 0x1404;
        public static UInt32 GL_UNSIGNED_INT = 0x1405;
        public static UInt32 GL_FLOAT = 0x1406;
        public static UInt32 GL_DOUBLE = 0x140A;
        public static UInt32 GL_STACK_OVERFLOW = 0x0503;
        public static UInt32 GL_STACK_UNDERFLOW = 0x0504;
        public static UInt32 GL_CLEAR = 0x1500;
        public static UInt32 GL_AND = 0x1501;
        public static UInt32 GL_AND_REVERSE = 0x1502;
        public static UInt32 GL_COPY = 0x1503;
        public static UInt32 GL_AND_INVERTED = 0x1504;
        public static UInt32 GL_NOOP = 0x1505;
        public static UInt32 GL_XOR = 0x1506;
        public static UInt32 GL_OR = 0x1507;
        public static UInt32 GL_NOR = 0x1508;
        public static UInt32 GL_EQUIV = 0x1509;
        public static UInt32 GL_INVERT = 0x150A;
        public static UInt32 GL_OR_REVERSE = 0x150B;
        public static UInt32 GL_COPY_INVERTED = 0x150C;
        public static UInt32 GL_OR_INVERTED = 0x150D;
        public static UInt32 GL_NAND = 0x150E;
        public static UInt32 GL_SET = 0x150F;
        public static UInt32 GL_TEXTURE = 0x1702;
        public static UInt32 GL_COLOR = 0x1800;
        public static UInt32 GL_DEPTH = 0x1801;
        public static UInt32 GL_STENCIL = 0x1802;
        public static UInt32 GL_STENCIL_INDEX = 0x1901;
        public static UInt32 GL_DEPTH_COMPONENT = 0x1902;
        public static UInt32 GL_RED = 0x1903;
        public static UInt32 GL_GREEN = 0x1904;
        public static UInt32 GL_BLUE = 0x1905;
        public static UInt32 GL_ALPHA = 0x1906;
        public static UInt32 GL_RGB = 0x1907;
        public static UInt32 GL_RGBA = 0x1908;
        public static UInt32 GL_POINT = 0x1B00;
        public static UInt32 GL_LINE = 0x1B01;
        public static UInt32 GL_FILL = 0x1B02;
        public static UInt32 GL_KEEP = 0x1E00;
        public static UInt32 GL_REPLACE = 0x1E01;
        public static UInt32 GL_INCR = 0x1E02;
        public static UInt32 GL_DECR = 0x1E03;
        public static UInt32 GL_VENDOR = 0x1F00;
        public static UInt32 GL_RENDERER = 0x1F01;
        public static UInt32 GL_VERSION = 0x1F02;
        public static UInt32 GL_EXTENSIONS = 0x1F03;
        public static UInt32 GL_NEAREST = 0x2600;
        public static UInt32 GL_LINEAR = 0x2601;
        public static UInt32 GL_NEAREST_MIPMAP_NEAREST = 0x2700;
        public static UInt32 GL_LINEAR_MIPMAP_NEAREST = 0x2701;
        public static UInt32 GL_NEAREST_MIPMAP_LINEAR = 0x2702;
        public static UInt32 GL_LINEAR_MIPMAP_LINEAR = 0x2703;
        public static UInt32 GL_TEXTURE_MAG_FILTER = 0x2800;
        public static UInt32 GL_TEXTURE_MIN_FILTER = 0x2801;
        public static UInt32 GL_TEXTURE_WRAP_S = 0x2802;
        public static UInt32 GL_TEXTURE_WRAP_T = 0x2803;
        public static UInt32 GL_PROXY_TEXTURE_1D = 0x8063;
        public static UInt32 GL_PROXY_TEXTURE_2D = 0x8064;
        public static UInt32 GL_REPEAT = 0x2901;
        public static UInt32 GL_R3_G3_B2 = 0x2A10;
        public static UInt32 GL_RGB4 = 0x804F;
        public static UInt32 GL_RGB5 = 0x8050;
        public static UInt32 GL_RGB8 = 0x8051;
        public static UInt32 GL_RGB10 = 0x8052;
        public static UInt32 GL_RGB12 = 0x8053;
        public static UInt32 GL_RGB16 = 0x8054;
        public static UInt32 GL_RGBA2 = 0x8055;
        public static UInt32 GL_RGBA4 = 0x8056;
        public static UInt32 GL_RGB5_A1 = 0x8057;
        public static UInt32 GL_RGBA8 = 0x8058;
        public static UInt32 GL_RGB10_A2 = 0x8059;
        public static UInt32 GL_RGBA12 = 0x805A;
        public static UInt32 GL_RGBA16 = 0x805B;
        public static UInt32 GL_CURRENT_BIT = 0x00000001;
        public static UInt32 GL_POINT_BIT = 0x00000002;
        public static UInt32 GL_LINE_BIT = 0x00000004;
        public static UInt32 GL_POLYGON_BIT = 0x00000008;
        public static UInt32 GL_POLYGON_STIPPLE_BIT = 0x00000010;
        public static UInt32 GL_PIXEL_MODE_BIT = 0x00000020;
        public static UInt32 GL_LIGHTING_BIT = 0x00000040;
        public static UInt32 GL_FOG_BIT = 0x00000080;
        public static UInt32 GL_ACCUM_BUFFER_BIT = 0x00000200;
        public static UInt32 GL_VIEWPORT_BIT = 0x00000800;
        public static UInt32 GL_TRANSFORM_BIT = 0x00001000;
        public static UInt32 GL_ENABLE_BIT = 0x00002000;
        public static UInt32 GL_HINT_BIT = 0x00008000;
        public static UInt32 GL_EVAL_BIT = 0x00010000;
        public static UInt32 GL_LIST_BIT = 0x00020000;
        public static UInt32 GL_TEXTURE_BIT = 0x00040000;
        public static UInt32 GL_SCISSOR_BIT = 0x00080000;
        public static UInt32 GL_ALL_ATTRIB_BITS = 0xFFFFFFFF;
        public static UInt32 GL_CLIENT_PIXEL_STORE_BIT = 0x00000001;
        public static UInt32 GL_CLIENT_VERTEX_ARRAY_BIT = 0x00000002;
        public static UInt32 GL_CLIENT_ALL_ATTRIB_BITS = 0xFFFFFFFF;
        public static UInt32 GL_QUAD_STRIP = 0x0008;
        public static UInt32 GL_POLYGON = 0x0009;
        public static UInt32 GL_ACCUM = 0x0100;
        public static UInt32 GL_LOAD = 0x0101;
        public static UInt32 GL_RETURN = 0x0102;
        public static UInt32 GL_MULT = 0x0103;
        public static UInt32 GL_ADD = 0x0104;
        public static UInt32 GL_AUX0 = 0x0409;
        public static UInt32 GL_AUX1 = 0x040A;
        public static UInt32 GL_AUX2 = 0x040B;
        public static UInt32 GL_AUX3 = 0x040C;
        public static UInt32 GL_2D = 0x0600;
        public static UInt32 GL_3D = 0x0601;
        public static UInt32 GL_3D_COLOR = 0x0602;
        public static UInt32 GL_3D_COLOR_TEXTURE = 0x0603;
        public static UInt32 GL_4D_COLOR_TEXTURE = 0x0604;
        public static UInt32 GL_PASS_THROUGH_TOKEN = 0x0700;
        public static UInt32 GL_POINT_TOKEN = 0x0701;
        public static UInt32 GL_LINE_TOKEN = 0x0702;
        public static UInt32 GL_POLYGON_TOKEN = 0x0703;
        public static UInt32 GL_BITMAP_TOKEN = 0x0704;
        public static UInt32 GL_DRAW_PIXEL_TOKEN = 0x0705;
        public static UInt32 GL_COPY_PIXEL_TOKEN = 0x0706;
        public static UInt32 GL_LINE_RESET_TOKEN = 0x0707;
        public static UInt32 GL_EXP = 0x0800;
        public static UInt32 GL_EXP2 = 0x0801;
        public static UInt32 GL_COEFF = 0x0A00;
        public static UInt32 GL_ORDER = 0x0A01;
        public static UInt32 GL_DOMAIN = 0x0A02;
        public static UInt32 GL_PIXEL_MAP_I_TO_I = 0x0C70;
        public static UInt32 GL_PIXEL_MAP_S_TO_S = 0x0C71;
        public static UInt32 GL_PIXEL_MAP_I_TO_R = 0x0C72;
        public static UInt32 GL_PIXEL_MAP_I_TO_G = 0x0C73;
        public static UInt32 GL_PIXEL_MAP_I_TO_B = 0x0C74;
        public static UInt32 GL_PIXEL_MAP_I_TO_A = 0x0C75;
        public static UInt32 GL_PIXEL_MAP_R_TO_R = 0x0C76;
        public static UInt32 GL_PIXEL_MAP_G_TO_G = 0x0C77;
        public static UInt32 GL_PIXEL_MAP_B_TO_B = 0x0C78;
        public static UInt32 GL_PIXEL_MAP_A_TO_A = 0x0C79;
        public static UInt32 GL_VERTEX_ARRAY_POINTER = 0x808E;
        public static UInt32 GL_NORMAL_ARRAY_POINTER = 0x808F;
        public static UInt32 GL_COLOR_ARRAY_POINTER = 0x8090;
        public static UInt32 GL_INDEX_ARRAY_POINTER = 0x8091;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_POINTER = 0x8092;
        public static UInt32 GL_EDGE_FLAG_ARRAY_POINTER = 0x8093;
        public static UInt32 GL_FEEDBACK_BUFFER_POINTER = 0x0DF0;
        public static UInt32 GL_SELECTION_BUFFER_POINTER = 0x0DF3;
        public static UInt32 GL_CURRENT_COLOR = 0x0B00;
        public static UInt32 GL_CURRENT_INDEX = 0x0B01;
        public static UInt32 GL_CURRENT_NORMAL = 0x0B02;
        public static UInt32 GL_CURRENT_TEXTURE_COORDS = 0x0B03;
        public static UInt32 GL_CURRENT_RASTER_COLOR = 0x0B04;
        public static UInt32 GL_CURRENT_RASTER_INDEX = 0x0B05;
        public static UInt32 GL_CURRENT_RASTER_TEXTURE_COORDS = 0x0B06;
        public static UInt32 GL_CURRENT_RASTER_POSITION = 0x0B07;
        public static UInt32 GL_CURRENT_RASTER_POSITION_VALID = 0x0B08;
        public static UInt32 GL_CURRENT_RASTER_DISTANCE = 0x0B09;
        public static UInt32 GL_POINT_SMOOTH = 0x0B10;
        public static UInt32 GL_LINE_STIPPLE = 0x0B24;
        public static UInt32 GL_LINE_STIPPLE_PATTERN = 0x0B25;
        public static UInt32 GL_LINE_STIPPLE_REPEAT = 0x0B26;
        public static UInt32 GL_LIST_MODE = 0x0B30;
        public static UInt32 GL_MAX_LIST_NESTING = 0x0B31;
        public static UInt32 GL_LIST_BASE = 0x0B32;
        public static UInt32 GL_LIST_INDEX = 0x0B33;
        public static UInt32 GL_POLYGON_STIPPLE = 0x0B42;
        public static UInt32 GL_EDGE_FLAG = 0x0B43;
        public static UInt32 GL_LIGHTING = 0x0B50;
        public static UInt32 GL_LIGHT_MODEL_LOCAL_VIEWER = 0x0B51;
        public static UInt32 GL_LIGHT_MODEL_TWO_SIDE = 0x0B52;
        public static UInt32 GL_LIGHT_MODEL_AMBIENT = 0x0B53;
        public static UInt32 GL_SHADE_MODEL = 0x0B54;
        public static UInt32 GL_COLOR_MATERIAL_FACE = 0x0B55;
        public static UInt32 GL_COLOR_MATERIAL_PARAMETER = 0x0B56;
        public static UInt32 GL_COLOR_MATERIAL = 0x0B57;
        public static UInt32 GL_FOG = 0x0B60;
        public static UInt32 GL_FOG_INDEX = 0x0B61;
        public static UInt32 GL_FOG_DENSITY = 0x0B62;
        public static UInt32 GL_FOG_START = 0x0B63;
        public static UInt32 GL_FOG_END = 0x0B64;
        public static UInt32 GL_FOG_MODE = 0x0B65;
        public static UInt32 GL_FOG_COLOR = 0x0B66;
        public static UInt32 GL_ACCUM_CLEAR_VALUE = 0x0B80;
        public static UInt32 GL_MATRIX_MODE = 0x0BA0;
        public static UInt32 GL_NORMALIZE = 0x0BA1;
        public static UInt32 GL_MODELVIEW_STACK_DEPTH = 0x0BA3;
        public static UInt32 GL_PROJECTION_STACK_DEPTH = 0x0BA4;
        public static UInt32 GL_TEXTURE_STACK_DEPTH = 0x0BA5;
        public static UInt32 GL_MODELVIEW_MATRIX = 0x0BA6;
        public static UInt32 GL_PROJECTION_MATRIX = 0x0BA7;
        public static UInt32 GL_TEXTURE_MATRIX = 0x0BA8;
        public static UInt32 GL_ATTRIB_STACK_DEPTH = 0x0BB0;
        public static UInt32 GL_CLIENT_ATTRIB_STACK_DEPTH = 0x0BB1;
        public static UInt32 GL_ALPHA_TEST = 0x0BC0;
        public static UInt32 GL_ALPHA_TEST_FUNC = 0x0BC1;
        public static UInt32 GL_ALPHA_TEST_REF = 0x0BC2;
        public static UInt32 GL_INDEX_LOGIC_OP = 0x0BF1;
        public static UInt32 GL_LOGIC_OP = 0x0BF1;
        public static UInt32 GL_AUX_BUFFERS = 0x0C00;
        public static UInt32 GL_INDEX_CLEAR_VALUE = 0x0C20;
        public static UInt32 GL_INDEX_WRITEMASK = 0x0C21;
        public static UInt32 GL_INDEX_MODE = 0x0C30;
        public static UInt32 GL_RGBA_MODE = 0x0C31;
        public static UInt32 GL_RENDER_MODE = 0x0C40;
        public static UInt32 GL_PERSPECTIVE_CORRECTION_HINT = 0x0C50;
        public static UInt32 GL_POINT_SMOOTH_HINT = 0x0C51;
        public static UInt32 GL_FOG_HINT = 0x0C54;
        public static UInt32 GL_TEXTURE_GEN_S = 0x0C60;
        public static UInt32 GL_TEXTURE_GEN_T = 0x0C61;
        public static UInt32 GL_TEXTURE_GEN_R = 0x0C62;
        public static UInt32 GL_TEXTURE_GEN_Q = 0x0C63;
        public static UInt32 GL_PIXEL_MAP_I_TO_I_SIZE = 0x0CB0;
        public static UInt32 GL_PIXEL_MAP_S_TO_S_SIZE = 0x0CB1;
        public static UInt32 GL_PIXEL_MAP_I_TO_R_SIZE = 0x0CB2;
        public static UInt32 GL_PIXEL_MAP_I_TO_G_SIZE = 0x0CB3;
        public static UInt32 GL_PIXEL_MAP_I_TO_B_SIZE = 0x0CB4;
        public static UInt32 GL_PIXEL_MAP_I_TO_A_SIZE = 0x0CB5;
        public static UInt32 GL_PIXEL_MAP_R_TO_R_SIZE = 0x0CB6;
        public static UInt32 GL_PIXEL_MAP_G_TO_G_SIZE = 0x0CB7;
        public static UInt32 GL_PIXEL_MAP_B_TO_B_SIZE = 0x0CB8;
        public static UInt32 GL_PIXEL_MAP_A_TO_A_SIZE = 0x0CB9;
        public static UInt32 GL_MAP_COLOR = 0x0D10;
        public static UInt32 GL_MAP_STENCIL = 0x0D11;
        public static UInt32 GL_INDEX_SHIFT = 0x0D12;
        public static UInt32 GL_INDEX_OFFSET = 0x0D13;
        public static UInt32 GL_RED_SCALE = 0x0D14;
        public static UInt32 GL_RED_BIAS = 0x0D15;
        public static UInt32 GL_ZOOM_X = 0x0D16;
        public static UInt32 GL_ZOOM_Y = 0x0D17;
        public static UInt32 GL_GREEN_SCALE = 0x0D18;
        public static UInt32 GL_GREEN_BIAS = 0x0D19;
        public static UInt32 GL_BLUE_SCALE = 0x0D1A;
        public static UInt32 GL_BLUE_BIAS = 0x0D1B;
        public static UInt32 GL_ALPHA_SCALE = 0x0D1C;
        public static UInt32 GL_ALPHA_BIAS = 0x0D1D;
        public static UInt32 GL_DEPTH_SCALE = 0x0D1E;
        public static UInt32 GL_DEPTH_BIAS = 0x0D1F;
        public static UInt32 GL_MAX_EVAL_ORDER = 0x0D30;
        public static UInt32 GL_MAX_LIGHTS = 0x0D31;
        public static UInt32 GL_MAX_CLIP_PLANES = 0x0D32;
        public static UInt32 GL_MAX_PIXEL_MAP_TABLE = 0x0D34;
        public static UInt32 GL_MAX_ATTRIB_STACK_DEPTH = 0x0D35;
        public static UInt32 GL_MAX_MODELVIEW_STACK_DEPTH = 0x0D36;
        public static UInt32 GL_MAX_NAME_STACK_DEPTH = 0x0D37;
        public static UInt32 GL_MAX_PROJECTION_STACK_DEPTH = 0x0D38;
        public static UInt32 GL_MAX_TEXTURE_STACK_DEPTH = 0x0D39;
        public static UInt32 GL_MAX_CLIENT_ATTRIB_STACK_DEPTH = 0x0D3B;
        public static UInt32 GL_INDEX_BITS = 0x0D51;
        public static UInt32 GL_RED_BITS = 0x0D52;
        public static UInt32 GL_GREEN_BITS = 0x0D53;
        public static UInt32 GL_BLUE_BITS = 0x0D54;
        public static UInt32 GL_ALPHA_BITS = 0x0D55;
        public static UInt32 GL_DEPTH_BITS = 0x0D56;
        public static UInt32 GL_STENCIL_BITS = 0x0D57;
        public static UInt32 GL_ACCUM_RED_BITS = 0x0D58;
        public static UInt32 GL_ACCUM_GREEN_BITS = 0x0D59;
        public static UInt32 GL_ACCUM_BLUE_BITS = 0x0D5A;
        public static UInt32 GL_ACCUM_ALPHA_BITS = 0x0D5B;
        public static UInt32 GL_NAME_STACK_DEPTH = 0x0D70;
        public static UInt32 GL_AUTO_NORMAL = 0x0D80;
        public static UInt32 GL_MAP1_COLOR_4 = 0x0D90;
        public static UInt32 GL_MAP1_INDEX = 0x0D91;
        public static UInt32 GL_MAP1_NORMAL = 0x0D92;
        public static UInt32 GL_MAP1_TEXTURE_COORD_1 = 0x0D93;
        public static UInt32 GL_MAP1_TEXTURE_COORD_2 = 0x0D94;
        public static UInt32 GL_MAP1_TEXTURE_COORD_3 = 0x0D95;
        public static UInt32 GL_MAP1_TEXTURE_COORD_4 = 0x0D96;
        public static UInt32 GL_MAP1_VERTEX_3 = 0x0D97;
        public static UInt32 GL_MAP1_VERTEX_4 = 0x0D98;
        public static UInt32 GL_MAP2_COLOR_4 = 0x0DB0;
        public static UInt32 GL_MAP2_INDEX = 0x0DB1;
        public static UInt32 GL_MAP2_NORMAL = 0x0DB2;
        public static UInt32 GL_MAP2_TEXTURE_COORD_1 = 0x0DB3;
        public static UInt32 GL_MAP2_TEXTURE_COORD_2 = 0x0DB4;
        public static UInt32 GL_MAP2_TEXTURE_COORD_3 = 0x0DB5;
        public static UInt32 GL_MAP2_TEXTURE_COORD_4 = 0x0DB6;
        public static UInt32 GL_MAP2_VERTEX_3 = 0x0DB7;
        public static UInt32 GL_MAP2_VERTEX_4 = 0x0DB8;
        public static UInt32 GL_MAP1_GRID_DOMAIN = 0x0DD0;
        public static UInt32 GL_MAP1_GRID_SEGMENTS = 0x0DD1;
        public static UInt32 GL_MAP2_GRID_DOMAIN = 0x0DD2;
        public static UInt32 GL_MAP2_GRID_SEGMENTS = 0x0DD3;
        public static UInt32 GL_FEEDBACK_BUFFER_SIZE = 0x0DF1;
        public static UInt32 GL_FEEDBACK_BUFFER_TYPE = 0x0DF2;
        public static UInt32 GL_SELECTION_BUFFER_SIZE = 0x0DF4;
        public static UInt32 GL_VERTEX_ARRAY = 0x8074;
        public static UInt32 GL_NORMAL_ARRAY = 0x8075;
        public static UInt32 GL_COLOR_ARRAY = 0x8076;
        public static UInt32 GL_INDEX_ARRAY = 0x8077;
        public static UInt32 GL_TEXTURE_COORD_ARRAY = 0x8078;
        public static UInt32 GL_EDGE_FLAG_ARRAY = 0x8079;
        public static UInt32 GL_VERTEX_ARRAY_SIZE = 0x807A;
        public static UInt32 GL_VERTEX_ARRAY_TYPE = 0x807B;
        public static UInt32 GL_VERTEX_ARRAY_STRIDE = 0x807C;
        public static UInt32 GL_NORMAL_ARRAY_TYPE = 0x807E;
        public static UInt32 GL_NORMAL_ARRAY_STRIDE = 0x807F;
        public static UInt32 GL_COLOR_ARRAY_SIZE = 0x8081;
        public static UInt32 GL_COLOR_ARRAY_TYPE = 0x8082;
        public static UInt32 GL_COLOR_ARRAY_STRIDE = 0x8083;
        public static UInt32 GL_INDEX_ARRAY_TYPE = 0x8085;
        public static UInt32 GL_INDEX_ARRAY_STRIDE = 0x8086;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_SIZE = 0x8088;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_TYPE = 0x8089;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_STRIDE = 0x808A;
        public static UInt32 GL_EDGE_FLAG_ARRAY_STRIDE = 0x808C;
        public static UInt32 GL_TEXTURE_COMPONENTS = 0x1003;
        public static UInt32 GL_TEXTURE_BORDER = 0x1005;
        public static UInt32 GL_TEXTURE_LUMINANCE_SIZE = 0x8060;
        public static UInt32 GL_TEXTURE_INTENSITY_SIZE = 0x8061;
        public static UInt32 GL_TEXTURE_PRIORITY = 0x8066;
        public static UInt32 GL_TEXTURE_RESIDENT = 0x8067;
        public static UInt32 GL_AMBIENT = 0x1200;
        public static UInt32 GL_DIFFUSE = 0x1201;
        public static UInt32 GL_SPECULAR = 0x1202;
        public static UInt32 GL_POSITION = 0x1203;
        public static UInt32 GL_SPOT_DIRECTION = 0x1204;
        public static UInt32 GL_SPOT_EXPONENT = 0x1205;
        public static UInt32 GL_SPOT_CUTOFF = 0x1206;
        public static UInt32 GL_CONSTANT_ATTENUATION = 0x1207;
        public static UInt32 GL_LINEAR_ATTENUATION = 0x1208;
        public static UInt32 GL_QUADRATIC_ATTENUATION = 0x1209;
        public static UInt32 GL_COMPILE = 0x1300;
        public static UInt32 GL_COMPILE_AND_EXECUTE = 0x1301;
        public static UInt32 GL_2_BYTES = 0x1407;
        public static UInt32 GL_3_BYTES = 0x1408;
        public static UInt32 GL_4_BYTES = 0x1409;
        public static UInt32 GL_EMISSION = 0x1600;
        public static UInt32 GL_SHININESS = 0x1601;
        public static UInt32 GL_AMBIENT_AND_DIFFUSE = 0x1602;
        public static UInt32 GL_COLOR_INDEXES = 0x1603;
        public static UInt32 GL_MODELVIEW = 0x1700;
        public static UInt32 GL_PROJECTION = 0x1701;
        public static UInt32 GL_COLOR_INDEX = 0x1900;
        public static UInt32 GL_LUMINANCE = 0x1909;
        public static UInt32 GL_LUMINANCE_ALPHA = 0x190A;
        public static UInt32 GL_BITMAP = 0x1A00;
        public static UInt32 GL_RENDER = 0x1C00;
        public static UInt32 GL_FEEDBACK = 0x1C01;
        public static UInt32 GL_SELECT = 0x1C02;
        public static UInt32 GL_FLAT = 0x1D00;
        public static UInt32 GL_SMOOTH = 0x1D01;
        public static UInt32 GL_S = 0x2000;
        public static UInt32 GL_T = 0x2001;
        public static UInt32 GL_R = 0x2002;
        public static UInt32 GL_Q = 0x2003;
        public static UInt32 GL_MODULATE = 0x2100;
        public static UInt32 GL_DECAL = 0x2101;
        public static UInt32 GL_TEXTURE_ENV_MODE = 0x2200;
        public static UInt32 GL_TEXTURE_ENV_COLOR = 0x2201;
        public static UInt32 GL_TEXTURE_ENV = 0x2300;
        public static UInt32 GL_EYE_LINEAR = 0x2400;
        public static UInt32 GL_OBJECT_LINEAR = 0x2401;
        public static UInt32 GL_SPHERE_MAP = 0x2402;
        public static UInt32 GL_TEXTURE_GEN_MODE = 0x2500;
        public static UInt32 GL_OBJECT_PLANE = 0x2501;
        public static UInt32 GL_EYE_PLANE = 0x2502;
        public static UInt32 GL_CLAMP = 0x2900;
        public static UInt32 GL_ALPHA4 = 0x803B;
        public static UInt32 GL_ALPHA8 = 0x803C;
        public static UInt32 GL_ALPHA12 = 0x803D;
        public static UInt32 GL_ALPHA16 = 0x803E;
        public static UInt32 GL_LUMINANCE4 = 0x803F;
        public static UInt32 GL_LUMINANCE8 = 0x8040;
        public static UInt32 GL_LUMINANCE12 = 0x8041;
        public static UInt32 GL_LUMINANCE16 = 0x8042;
        public static UInt32 GL_LUMINANCE4_ALPHA4 = 0x8043;
        public static UInt32 GL_LUMINANCE6_ALPHA2 = 0x8044;
        public static UInt32 GL_LUMINANCE8_ALPHA8 = 0x8045;
        public static UInt32 GL_LUMINANCE12_ALPHA4 = 0x8046;
        public static UInt32 GL_LUMINANCE12_ALPHA12 = 0x8047;
        public static UInt32 GL_LUMINANCE16_ALPHA16 = 0x8048;
        public static UInt32 GL_INTENSITY = 0x8049;
        public static UInt32 GL_INTENSITY4 = 0x804A;
        public static UInt32 GL_INTENSITY8 = 0x804B;
        public static UInt32 GL_INTENSITY12 = 0x804C;
        public static UInt32 GL_INTENSITY16 = 0x804D;
        public static UInt32 GL_V2F = 0x2A20;
        public static UInt32 GL_V3F = 0x2A21;
        public static UInt32 GL_C4UB_V2F = 0x2A22;
        public static UInt32 GL_C4UB_V3F = 0x2A23;
        public static UInt32 GL_C3F_V3F = 0x2A24;
        public static UInt32 GL_N3F_V3F = 0x2A25;
        public static UInt32 GL_C4F_N3F_V3F = 0x2A26;
        public static UInt32 GL_T2F_V3F = 0x2A27;
        public static UInt32 GL_T4F_V4F = 0x2A28;
        public static UInt32 GL_T2F_C4UB_V3F = 0x2A29;
        public static UInt32 GL_T2F_C3F_V3F = 0x2A2A;
        public static UInt32 GL_T2F_N3F_V3F = 0x2A2B;
        public static UInt32 GL_T2F_C4F_N3F_V3F = 0x2A2C;
        public static UInt32 GL_T4F_C4F_N3F_V4F = 0x2A2D;
        public static UInt32 GL_CLIP_PLANE0 = 0x3000;
        public static UInt32 GL_CLIP_PLANE1 = 0x3001;
        public static UInt32 GL_CLIP_PLANE2 = 0x3002;
        public static UInt32 GL_CLIP_PLANE3 = 0x3003;
        public static UInt32 GL_CLIP_PLANE4 = 0x3004;
        public static UInt32 GL_CLIP_PLANE5 = 0x3005;
        public static UInt32 GL_LIGHT0 = 0x4000;
        public static UInt32 GL_LIGHT1 = 0x4001;
        public static UInt32 GL_LIGHT2 = 0x4002;
        public static UInt32 GL_LIGHT3 = 0x4003;
        public static UInt32 GL_LIGHT4 = 0x4004;
        public static UInt32 GL_LIGHT5 = 0x4005;
        public static UInt32 GL_LIGHT6 = 0x4006;
        public static UInt32 GL_LIGHT7 = 0x4007;
        #endregion

        #region Commands
        internal delegate void glDrawArraysFunc(GLenum @mode, GLint @first, GLsizei @count);
        internal static glDrawArraysFunc glDrawArraysPtr;
        internal static void loadDrawArrays()
        {
            try
            {
                glDrawArraysPtr = (glDrawArraysFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArrays"), typeof(glDrawArraysFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArrays'.");
            }
        }
        public static void glDrawArrays(GLenum @mode, GLint @first, GLsizei @count) => glDrawArraysPtr(@mode, @first, @count);

        internal delegate void glDrawElementsFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices);
        internal static glDrawElementsFunc glDrawElementsPtr;
        internal static void loadDrawElements()
        {
            try
            {
                glDrawElementsPtr = (glDrawElementsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElements"), typeof(glDrawElementsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElements'.");
            }
        }
        public static void glDrawElements(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices) => glDrawElementsPtr(@mode, @count, @type, @indices);

        internal delegate void glGetPointervFunc(GLenum @pname, void ** @params);
        internal static glGetPointervFunc glGetPointervPtr;
        internal static void loadGetPointerv()
        {
            try
            {
                glGetPointervPtr = (glGetPointervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPointerv"), typeof(glGetPointervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPointerv'.");
            }
        }
        public static void glGetPointerv(GLenum @pname, void ** @params) => glGetPointervPtr(@pname, @params);

        internal delegate void glPolygonOffsetFunc(GLfloat @factor, GLfloat @units);
        internal static glPolygonOffsetFunc glPolygonOffsetPtr;
        internal static void loadPolygonOffset()
        {
            try
            {
                glPolygonOffsetPtr = (glPolygonOffsetFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPolygonOffset"), typeof(glPolygonOffsetFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPolygonOffset'.");
            }
        }
        public static void glPolygonOffset(GLfloat @factor, GLfloat @units) => glPolygonOffsetPtr(@factor, @units);

        internal delegate void glCopyTexImage1DFunc(GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLint @border);
        internal static glCopyTexImage1DFunc glCopyTexImage1DPtr;
        internal static void loadCopyTexImage1D()
        {
            try
            {
                glCopyTexImage1DPtr = (glCopyTexImage1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexImage1D"), typeof(glCopyTexImage1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexImage1D'.");
            }
        }
        public static void glCopyTexImage1D(GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLint @border) => glCopyTexImage1DPtr(@target, @level, @internalformat, @x, @y, @width, @border);

        internal delegate void glCopyTexImage2DFunc(GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLint @border);
        internal static glCopyTexImage2DFunc glCopyTexImage2DPtr;
        internal static void loadCopyTexImage2D()
        {
            try
            {
                glCopyTexImage2DPtr = (glCopyTexImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexImage2D"), typeof(glCopyTexImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexImage2D'.");
            }
        }
        public static void glCopyTexImage2D(GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLint @border) => glCopyTexImage2DPtr(@target, @level, @internalformat, @x, @y, @width, @height, @border);

        internal delegate void glCopyTexSubImage1DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @x, GLint @y, GLsizei @width);
        internal static glCopyTexSubImage1DFunc glCopyTexSubImage1DPtr;
        internal static void loadCopyTexSubImage1D()
        {
            try
            {
                glCopyTexSubImage1DPtr = (glCopyTexSubImage1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexSubImage1D"), typeof(glCopyTexSubImage1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexSubImage1D'.");
            }
        }
        public static void glCopyTexSubImage1D(GLenum @target, GLint @level, GLint @xoffset, GLint @x, GLint @y, GLsizei @width) => glCopyTexSubImage1DPtr(@target, @level, @xoffset, @x, @y, @width);

        internal delegate void glCopyTexSubImage2DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyTexSubImage2DFunc glCopyTexSubImage2DPtr;
        internal static void loadCopyTexSubImage2D()
        {
            try
            {
                glCopyTexSubImage2DPtr = (glCopyTexSubImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexSubImage2D"), typeof(glCopyTexSubImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexSubImage2D'.");
            }
        }
        public static void glCopyTexSubImage2D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyTexSubImage2DPtr(@target, @level, @xoffset, @yoffset, @x, @y, @width, @height);

        internal delegate void glTexSubImage1DFunc(GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexSubImage1DFunc glTexSubImage1DPtr;
        internal static void loadTexSubImage1D()
        {
            try
            {
                glTexSubImage1DPtr = (glTexSubImage1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexSubImage1D"), typeof(glTexSubImage1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexSubImage1D'.");
            }
        }
        public static void glTexSubImage1D(GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLenum @type, const void * @pixels) => glTexSubImage1DPtr(@target, @level, @xoffset, @width, @format, @type, @pixels);

        internal delegate void glTexSubImage2DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexSubImage2DFunc glTexSubImage2DPtr;
        internal static void loadTexSubImage2D()
        {
            try
            {
                glTexSubImage2DPtr = (glTexSubImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexSubImage2D"), typeof(glTexSubImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexSubImage2D'.");
            }
        }
        public static void glTexSubImage2D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels) => glTexSubImage2DPtr(@target, @level, @xoffset, @yoffset, @width, @height, @format, @type, @pixels);

        internal delegate void glBindTextureFunc(GLenum @target, GLuint @texture);
        internal static glBindTextureFunc glBindTexturePtr;
        internal static void loadBindTexture()
        {
            try
            {
                glBindTexturePtr = (glBindTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTexture"), typeof(glBindTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTexture'.");
            }
        }
        public static void glBindTexture(GLenum @target, GLuint @texture) => glBindTexturePtr(@target, @texture);

        internal delegate void glDeleteTexturesFunc(GLsizei @n, const GLuint * @textures);
        internal static glDeleteTexturesFunc glDeleteTexturesPtr;
        internal static void loadDeleteTextures()
        {
            try
            {
                glDeleteTexturesPtr = (glDeleteTexturesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteTextures"), typeof(glDeleteTexturesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteTextures'.");
            }
        }
        public static void glDeleteTextures(GLsizei @n, const GLuint * @textures) => glDeleteTexturesPtr(@n, @textures);

        internal delegate void glGenTexturesFunc(GLsizei @n, GLuint * @textures);
        internal static glGenTexturesFunc glGenTexturesPtr;
        internal static void loadGenTextures()
        {
            try
            {
                glGenTexturesPtr = (glGenTexturesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenTextures"), typeof(glGenTexturesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenTextures'.");
            }
        }
        public static void glGenTextures(GLsizei @n, GLuint * @textures) => glGenTexturesPtr(@n, @textures);

        internal delegate GLboolean glIsTextureFunc(GLuint @texture);
        internal static glIsTextureFunc glIsTexturePtr;
        internal static void loadIsTexture()
        {
            try
            {
                glIsTexturePtr = (glIsTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsTexture"), typeof(glIsTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsTexture'.");
            }
        }
        public static GLboolean glIsTexture(GLuint @texture) => glIsTexturePtr(@texture);

        internal delegate void glArrayElementFunc(GLint @i);
        internal static glArrayElementFunc glArrayElementPtr;
        internal static void loadArrayElement()
        {
            try
            {
                glArrayElementPtr = (glArrayElementFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glArrayElement"), typeof(glArrayElementFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glArrayElement'.");
            }
        }
        public static void glArrayElement(GLint @i) => glArrayElementPtr(@i);

        internal delegate void glColorPointerFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glColorPointerFunc glColorPointerPtr;
        internal static void loadColorPointer()
        {
            try
            {
                glColorPointerPtr = (glColorPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorPointer"), typeof(glColorPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorPointer'.");
            }
        }
        public static void glColorPointer(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glColorPointerPtr(@size, @type, @stride, @pointer);

        internal delegate void glDisableClientStateFunc(GLenum @array);
        internal static glDisableClientStateFunc glDisableClientStatePtr;
        internal static void loadDisableClientState()
        {
            try
            {
                glDisableClientStatePtr = (glDisableClientStateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableClientState"), typeof(glDisableClientStateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableClientState'.");
            }
        }
        public static void glDisableClientState(GLenum @array) => glDisableClientStatePtr(@array);

        internal delegate void glEdgeFlagPointerFunc(GLsizei @stride, const void * @pointer);
        internal static glEdgeFlagPointerFunc glEdgeFlagPointerPtr;
        internal static void loadEdgeFlagPointer()
        {
            try
            {
                glEdgeFlagPointerPtr = (glEdgeFlagPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEdgeFlagPointer"), typeof(glEdgeFlagPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEdgeFlagPointer'.");
            }
        }
        public static void glEdgeFlagPointer(GLsizei @stride, const void * @pointer) => glEdgeFlagPointerPtr(@stride, @pointer);

        internal delegate void glEnableClientStateFunc(GLenum @array);
        internal static glEnableClientStateFunc glEnableClientStatePtr;
        internal static void loadEnableClientState()
        {
            try
            {
                glEnableClientStatePtr = (glEnableClientStateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableClientState"), typeof(glEnableClientStateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableClientState'.");
            }
        }
        public static void glEnableClientState(GLenum @array) => glEnableClientStatePtr(@array);

        internal delegate void glIndexPointerFunc(GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glIndexPointerFunc glIndexPointerPtr;
        internal static void loadIndexPointer()
        {
            try
            {
                glIndexPointerPtr = (glIndexPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexPointer"), typeof(glIndexPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexPointer'.");
            }
        }
        public static void glIndexPointer(GLenum @type, GLsizei @stride, const void * @pointer) => glIndexPointerPtr(@type, @stride, @pointer);

        internal delegate void glInterleavedArraysFunc(GLenum @format, GLsizei @stride, const void * @pointer);
        internal static glInterleavedArraysFunc glInterleavedArraysPtr;
        internal static void loadInterleavedArrays()
        {
            try
            {
                glInterleavedArraysPtr = (glInterleavedArraysFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInterleavedArrays"), typeof(glInterleavedArraysFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInterleavedArrays'.");
            }
        }
        public static void glInterleavedArrays(GLenum @format, GLsizei @stride, const void * @pointer) => glInterleavedArraysPtr(@format, @stride, @pointer);

        internal delegate void glNormalPointerFunc(GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glNormalPointerFunc glNormalPointerPtr;
        internal static void loadNormalPointer()
        {
            try
            {
                glNormalPointerPtr = (glNormalPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalPointer"), typeof(glNormalPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalPointer'.");
            }
        }
        public static void glNormalPointer(GLenum @type, GLsizei @stride, const void * @pointer) => glNormalPointerPtr(@type, @stride, @pointer);

        internal delegate void glTexCoordPointerFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glTexCoordPointerFunc glTexCoordPointerPtr;
        internal static void loadTexCoordPointer()
        {
            try
            {
                glTexCoordPointerPtr = (glTexCoordPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordPointer"), typeof(glTexCoordPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordPointer'.");
            }
        }
        public static void glTexCoordPointer(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glTexCoordPointerPtr(@size, @type, @stride, @pointer);

        internal delegate void glVertexPointerFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glVertexPointerFunc glVertexPointerPtr;
        internal static void loadVertexPointer()
        {
            try
            {
                glVertexPointerPtr = (glVertexPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexPointer"), typeof(glVertexPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexPointer'.");
            }
        }
        public static void glVertexPointer(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glVertexPointerPtr(@size, @type, @stride, @pointer);

        internal delegate GLboolean glAreTexturesResidentFunc(GLsizei @n, const GLuint * @textures, GLboolean * @residences);
        internal static glAreTexturesResidentFunc glAreTexturesResidentPtr;
        internal static void loadAreTexturesResident()
        {
            try
            {
                glAreTexturesResidentPtr = (glAreTexturesResidentFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAreTexturesResident"), typeof(glAreTexturesResidentFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAreTexturesResident'.");
            }
        }
        public static GLboolean glAreTexturesResident(GLsizei @n, const GLuint * @textures, GLboolean * @residences) => glAreTexturesResidentPtr(@n, @textures, @residences);

        internal delegate void glPrioritizeTexturesFunc(GLsizei @n, const GLuint * @textures, const GLfloat * @priorities);
        internal static glPrioritizeTexturesFunc glPrioritizeTexturesPtr;
        internal static void loadPrioritizeTextures()
        {
            try
            {
                glPrioritizeTexturesPtr = (glPrioritizeTexturesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPrioritizeTextures"), typeof(glPrioritizeTexturesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPrioritizeTextures'.");
            }
        }
        public static void glPrioritizeTextures(GLsizei @n, const GLuint * @textures, const GLfloat * @priorities) => glPrioritizeTexturesPtr(@n, @textures, @priorities);

        internal delegate void glIndexubFunc(GLubyte @c);
        internal static glIndexubFunc glIndexubPtr;
        internal static void loadIndexub()
        {
            try
            {
                glIndexubPtr = (glIndexubFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexub"), typeof(glIndexubFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexub'.");
            }
        }
        public static void glIndexub(GLubyte @c) => glIndexubPtr(@c);

        internal delegate void glIndexubvFunc(const GLubyte * @c);
        internal static glIndexubvFunc glIndexubvPtr;
        internal static void loadIndexubv()
        {
            try
            {
                glIndexubvPtr = (glIndexubvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexubv"), typeof(glIndexubvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexubv'.");
            }
        }
        public static void glIndexubv(const GLubyte * @c) => glIndexubvPtr(@c);

        internal delegate void glPopClientAttribFunc();
        internal static glPopClientAttribFunc glPopClientAttribPtr;
        internal static void loadPopClientAttrib()
        {
            try
            {
                glPopClientAttribPtr = (glPopClientAttribFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPopClientAttrib"), typeof(glPopClientAttribFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPopClientAttrib'.");
            }
        }
        public static void glPopClientAttrib() => glPopClientAttribPtr();

        internal delegate void glPushClientAttribFunc(GLbitfield @mask);
        internal static glPushClientAttribFunc glPushClientAttribPtr;
        internal static void loadPushClientAttrib()
        {
            try
            {
                glPushClientAttribPtr = (glPushClientAttribFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPushClientAttrib"), typeof(glPushClientAttribFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPushClientAttrib'.");
            }
        }
        public static void glPushClientAttrib(GLbitfield @mask) => glPushClientAttribPtr(@mask);
        #endregion
    }
}
