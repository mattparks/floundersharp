using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL41
    {
        #region Interop
        static GL41()
        {
            Console.WriteLine("Initalising GL41 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadReleaseShaderCompiler();
            loadShaderBinary();
            loadGetShaderPrecisionFormat();
            loadDepthRangef();
            loadClearDepthf();
            loadGetProgramBinary();
            loadProgramBinary();
            loadProgramParameteri();
            loadUseProgramStages();
            loadActiveShaderProgram();
            loadCreateShaderProgramv();
            loadBindProgramPipeline();
            loadDeleteProgramPipelines();
            loadGenProgramPipelines();
            loadIsProgramPipeline();
            loadGetProgramPipelineiv();
            loadProgramUniform1i();
            loadProgramUniform1iv();
            loadProgramUniform1f();
            loadProgramUniform1fv();
            loadProgramUniform1d();
            loadProgramUniform1dv();
            loadProgramUniform1ui();
            loadProgramUniform1uiv();
            loadProgramUniform2i();
            loadProgramUniform2iv();
            loadProgramUniform2f();
            loadProgramUniform2fv();
            loadProgramUniform2d();
            loadProgramUniform2dv();
            loadProgramUniform2ui();
            loadProgramUniform2uiv();
            loadProgramUniform3i();
            loadProgramUniform3iv();
            loadProgramUniform3f();
            loadProgramUniform3fv();
            loadProgramUniform3d();
            loadProgramUniform3dv();
            loadProgramUniform3ui();
            loadProgramUniform3uiv();
            loadProgramUniform4i();
            loadProgramUniform4iv();
            loadProgramUniform4f();
            loadProgramUniform4fv();
            loadProgramUniform4d();
            loadProgramUniform4dv();
            loadProgramUniform4ui();
            loadProgramUniform4uiv();
            loadProgramUniformMatrix2fv();
            loadProgramUniformMatrix3fv();
            loadProgramUniformMatrix4fv();
            loadProgramUniformMatrix2dv();
            loadProgramUniformMatrix3dv();
            loadProgramUniformMatrix4dv();
            loadProgramUniformMatrix2x3fv();
            loadProgramUniformMatrix3x2fv();
            loadProgramUniformMatrix2x4fv();
            loadProgramUniformMatrix4x2fv();
            loadProgramUniformMatrix3x4fv();
            loadProgramUniformMatrix4x3fv();
            loadProgramUniformMatrix2x3dv();
            loadProgramUniformMatrix3x2dv();
            loadProgramUniformMatrix2x4dv();
            loadProgramUniformMatrix4x2dv();
            loadProgramUniformMatrix3x4dv();
            loadProgramUniformMatrix4x3dv();
            loadValidateProgramPipeline();
            loadGetProgramPipelineInfoLog();
            loadVertexAttribL1d();
            loadVertexAttribL2d();
            loadVertexAttribL3d();
            loadVertexAttribL4d();
            loadVertexAttribL1dv();
            loadVertexAttribL2dv();
            loadVertexAttribL3dv();
            loadVertexAttribL4dv();
            loadVertexAttribLPointer();
            loadGetVertexAttribLdv();
            loadViewportArrayv();
            loadViewportIndexedf();
            loadViewportIndexedfv();
            loadScissorArrayv();
            loadScissorIndexed();
            loadScissorIndexedv();
            loadDepthRangeArrayv();
            loadDepthRangeIndexed();
            loadGetFloati_v();
            loadGetDoublei_v();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FIXED = 0x140C;
        public static UInt32 GL_IMPLEMENTATION_COLOR_READ_TYPE = 0x8B9A;
        public static UInt32 GL_IMPLEMENTATION_COLOR_READ_FORMAT = 0x8B9B;
        public static UInt32 GL_LOW_FLOAT = 0x8DF0;
        public static UInt32 GL_MEDIUM_FLOAT = 0x8DF1;
        public static UInt32 GL_HIGH_FLOAT = 0x8DF2;
        public static UInt32 GL_LOW_INT = 0x8DF3;
        public static UInt32 GL_MEDIUM_INT = 0x8DF4;
        public static UInt32 GL_HIGH_INT = 0x8DF5;
        public static UInt32 GL_SHADER_COMPILER = 0x8DFA;
        public static UInt32 GL_SHADER_BINARY_FORMATS = 0x8DF8;
        public static UInt32 GL_NUM_SHADER_BINARY_FORMATS = 0x8DF9;
        public static UInt32 GL_MAX_VERTEX_UNIFORM_VECTORS = 0x8DFB;
        public static UInt32 GL_MAX_VARYING_VECTORS = 0x8DFC;
        public static UInt32 GL_MAX_FRAGMENT_UNIFORM_VECTORS = 0x8DFD;
        public static UInt32 GL_RGB565 = 0x8D62;
        public static UInt32 GL_PROGRAM_BINARY_RETRIEVABLE_HINT = 0x8257;
        public static UInt32 GL_PROGRAM_BINARY_LENGTH = 0x8741;
        public static UInt32 GL_NUM_PROGRAM_BINARY_FORMATS = 0x87FE;
        public static UInt32 GL_PROGRAM_BINARY_FORMATS = 0x87FF;
        public static UInt32 GL_VERTEX_SHADER_BIT = 0x00000001;
        public static UInt32 GL_FRAGMENT_SHADER_BIT = 0x00000002;
        public static UInt32 GL_GEOMETRY_SHADER_BIT = 0x00000004;
        public static UInt32 GL_TESS_CONTROL_SHADER_BIT = 0x00000008;
        public static UInt32 GL_TESS_EVALUATION_SHADER_BIT = 0x00000010;
        public static UInt32 GL_ALL_SHADER_BITS = 0xFFFFFFFF;
        public static UInt32 GL_PROGRAM_SEPARABLE = 0x8258;
        public static UInt32 GL_ACTIVE_PROGRAM = 0x8259;
        public static UInt32 GL_PROGRAM_PIPELINE_BINDING = 0x825A;
        public static UInt32 GL_MAX_VIEWPORTS = 0x825B;
        public static UInt32 GL_VIEWPORT_SUBPIXEL_BITS = 0x825C;
        public static UInt32 GL_VIEWPORT_BOUNDS_RANGE = 0x825D;
        public static UInt32 GL_LAYER_PROVOKING_VERTEX = 0x825E;
        public static UInt32 GL_VIEWPORT_INDEX_PROVOKING_VERTEX = 0x825F;
        public static UInt32 GL_UNDEFINED_VERTEX = 0x8260;
        #endregion

        #region Commands
        internal delegate void glReleaseShaderCompilerFunc();
        internal static glReleaseShaderCompilerFunc glReleaseShaderCompilerPtr;
        internal static void loadReleaseShaderCompiler()
        {
            try
            {
                glReleaseShaderCompilerPtr = (glReleaseShaderCompilerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReleaseShaderCompiler"), typeof(glReleaseShaderCompilerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReleaseShaderCompiler'.");
            }
        }
        public static void glReleaseShaderCompiler() => glReleaseShaderCompilerPtr();

        internal delegate void glShaderBinaryFunc(GLsizei @count, const GLuint * @shaders, GLenum @binaryformat, const void * @binary, GLsizei @length);
        internal static glShaderBinaryFunc glShaderBinaryPtr;
        internal static void loadShaderBinary()
        {
            try
            {
                glShaderBinaryPtr = (glShaderBinaryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShaderBinary"), typeof(glShaderBinaryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShaderBinary'.");
            }
        }
        public static void glShaderBinary(GLsizei @count, const GLuint * @shaders, GLenum @binaryformat, const void * @binary, GLsizei @length) => glShaderBinaryPtr(@count, @shaders, @binaryformat, @binary, @length);

        internal delegate void glGetShaderPrecisionFormatFunc(GLenum @shadertype, GLenum @precisiontype, GLint * @range, GLint * @precision);
        internal static glGetShaderPrecisionFormatFunc glGetShaderPrecisionFormatPtr;
        internal static void loadGetShaderPrecisionFormat()
        {
            try
            {
                glGetShaderPrecisionFormatPtr = (glGetShaderPrecisionFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetShaderPrecisionFormat"), typeof(glGetShaderPrecisionFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetShaderPrecisionFormat'.");
            }
        }
        public static void glGetShaderPrecisionFormat(GLenum @shadertype, GLenum @precisiontype, GLint * @range, GLint * @precision) => glGetShaderPrecisionFormatPtr(@shadertype, @precisiontype, @range, @precision);

        internal delegate void glDepthRangefFunc(GLfloat @n, GLfloat @f);
        internal static glDepthRangefFunc glDepthRangefPtr;
        internal static void loadDepthRangef()
        {
            try
            {
                glDepthRangefPtr = (glDepthRangefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangef"), typeof(glDepthRangefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangef'.");
            }
        }
        public static void glDepthRangef(GLfloat @n, GLfloat @f) => glDepthRangefPtr(@n, @f);

        internal delegate void glClearDepthfFunc(GLfloat @d);
        internal static glClearDepthfFunc glClearDepthfPtr;
        internal static void loadClearDepthf()
        {
            try
            {
                glClearDepthfPtr = (glClearDepthfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearDepthf"), typeof(glClearDepthfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearDepthf'.");
            }
        }
        public static void glClearDepthf(GLfloat @d) => glClearDepthfPtr(@d);

        internal delegate void glGetProgramBinaryFunc(GLuint @program, GLsizei @bufSize, GLsizei * @length, GLenum * @binaryFormat, void * @binary);
        internal static glGetProgramBinaryFunc glGetProgramBinaryPtr;
        internal static void loadGetProgramBinary()
        {
            try
            {
                glGetProgramBinaryPtr = (glGetProgramBinaryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramBinary"), typeof(glGetProgramBinaryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramBinary'.");
            }
        }
        public static void glGetProgramBinary(GLuint @program, GLsizei @bufSize, GLsizei * @length, GLenum * @binaryFormat, void * @binary) => glGetProgramBinaryPtr(@program, @bufSize, @length, @binaryFormat, @binary);

        internal delegate void glProgramBinaryFunc(GLuint @program, GLenum @binaryFormat, const void * @binary, GLsizei @length);
        internal static glProgramBinaryFunc glProgramBinaryPtr;
        internal static void loadProgramBinary()
        {
            try
            {
                glProgramBinaryPtr = (glProgramBinaryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramBinary"), typeof(glProgramBinaryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramBinary'.");
            }
        }
        public static void glProgramBinary(GLuint @program, GLenum @binaryFormat, const void * @binary, GLsizei @length) => glProgramBinaryPtr(@program, @binaryFormat, @binary, @length);

        internal delegate void glProgramParameteriFunc(GLuint @program, GLenum @pname, GLint @value);
        internal static glProgramParameteriFunc glProgramParameteriPtr;
        internal static void loadProgramParameteri()
        {
            try
            {
                glProgramParameteriPtr = (glProgramParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramParameteri"), typeof(glProgramParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramParameteri'.");
            }
        }
        public static void glProgramParameteri(GLuint @program, GLenum @pname, GLint @value) => glProgramParameteriPtr(@program, @pname, @value);

        internal delegate void glUseProgramStagesFunc(GLuint @pipeline, GLbitfield @stages, GLuint @program);
        internal static glUseProgramStagesFunc glUseProgramStagesPtr;
        internal static void loadUseProgramStages()
        {
            try
            {
                glUseProgramStagesPtr = (glUseProgramStagesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUseProgramStages"), typeof(glUseProgramStagesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUseProgramStages'.");
            }
        }
        public static void glUseProgramStages(GLuint @pipeline, GLbitfield @stages, GLuint @program) => glUseProgramStagesPtr(@pipeline, @stages, @program);

        internal delegate void glActiveShaderProgramFunc(GLuint @pipeline, GLuint @program);
        internal static glActiveShaderProgramFunc glActiveShaderProgramPtr;
        internal static void loadActiveShaderProgram()
        {
            try
            {
                glActiveShaderProgramPtr = (glActiveShaderProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glActiveShaderProgram"), typeof(glActiveShaderProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glActiveShaderProgram'.");
            }
        }
        public static void glActiveShaderProgram(GLuint @pipeline, GLuint @program) => glActiveShaderProgramPtr(@pipeline, @program);

        internal delegate GLuint glCreateShaderProgramvFunc(GLenum @type, GLsizei @count, const GLchar *const* @strings);
        internal static glCreateShaderProgramvFunc glCreateShaderProgramvPtr;
        internal static void loadCreateShaderProgramv()
        {
            try
            {
                glCreateShaderProgramvPtr = (glCreateShaderProgramvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateShaderProgramv"), typeof(glCreateShaderProgramvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateShaderProgramv'.");
            }
        }
        public static GLuint glCreateShaderProgramv(GLenum @type, GLsizei @count, const GLchar *const* @strings) => glCreateShaderProgramvPtr(@type, @count, @strings);

        internal delegate void glBindProgramPipelineFunc(GLuint @pipeline);
        internal static glBindProgramPipelineFunc glBindProgramPipelinePtr;
        internal static void loadBindProgramPipeline()
        {
            try
            {
                glBindProgramPipelinePtr = (glBindProgramPipelineFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindProgramPipeline"), typeof(glBindProgramPipelineFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindProgramPipeline'.");
            }
        }
        public static void glBindProgramPipeline(GLuint @pipeline) => glBindProgramPipelinePtr(@pipeline);

        internal delegate void glDeleteProgramPipelinesFunc(GLsizei @n, const GLuint * @pipelines);
        internal static glDeleteProgramPipelinesFunc glDeleteProgramPipelinesPtr;
        internal static void loadDeleteProgramPipelines()
        {
            try
            {
                glDeleteProgramPipelinesPtr = (glDeleteProgramPipelinesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteProgramPipelines"), typeof(glDeleteProgramPipelinesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteProgramPipelines'.");
            }
        }
        public static void glDeleteProgramPipelines(GLsizei @n, const GLuint * @pipelines) => glDeleteProgramPipelinesPtr(@n, @pipelines);

        internal delegate void glGenProgramPipelinesFunc(GLsizei @n, GLuint * @pipelines);
        internal static glGenProgramPipelinesFunc glGenProgramPipelinesPtr;
        internal static void loadGenProgramPipelines()
        {
            try
            {
                glGenProgramPipelinesPtr = (glGenProgramPipelinesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenProgramPipelines"), typeof(glGenProgramPipelinesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenProgramPipelines'.");
            }
        }
        public static void glGenProgramPipelines(GLsizei @n, GLuint * @pipelines) => glGenProgramPipelinesPtr(@n, @pipelines);

        internal delegate GLboolean glIsProgramPipelineFunc(GLuint @pipeline);
        internal static glIsProgramPipelineFunc glIsProgramPipelinePtr;
        internal static void loadIsProgramPipeline()
        {
            try
            {
                glIsProgramPipelinePtr = (glIsProgramPipelineFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsProgramPipeline"), typeof(glIsProgramPipelineFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsProgramPipeline'.");
            }
        }
        public static GLboolean glIsProgramPipeline(GLuint @pipeline) => glIsProgramPipelinePtr(@pipeline);

        internal delegate void glGetProgramPipelineivFunc(GLuint @pipeline, GLenum @pname, GLint * @params);
        internal static glGetProgramPipelineivFunc glGetProgramPipelineivPtr;
        internal static void loadGetProgramPipelineiv()
        {
            try
            {
                glGetProgramPipelineivPtr = (glGetProgramPipelineivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramPipelineiv"), typeof(glGetProgramPipelineivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramPipelineiv'.");
            }
        }
        public static void glGetProgramPipelineiv(GLuint @pipeline, GLenum @pname, GLint * @params) => glGetProgramPipelineivPtr(@pipeline, @pname, @params);

        internal delegate void glProgramUniform1iFunc(GLuint @program, GLint @location, GLint @v0);
        internal static glProgramUniform1iFunc glProgramUniform1iPtr;
        internal static void loadProgramUniform1i()
        {
            try
            {
                glProgramUniform1iPtr = (glProgramUniform1iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1i"), typeof(glProgramUniform1iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1i'.");
            }
        }
        public static void glProgramUniform1i(GLuint @program, GLint @location, GLint @v0) => glProgramUniform1iPtr(@program, @location, @v0);

        internal delegate void glProgramUniform1ivFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform1ivFunc glProgramUniform1ivPtr;
        internal static void loadProgramUniform1iv()
        {
            try
            {
                glProgramUniform1ivPtr = (glProgramUniform1ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1iv"), typeof(glProgramUniform1ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1iv'.");
            }
        }
        public static void glProgramUniform1iv(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform1ivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform1fFunc(GLuint @program, GLint @location, GLfloat @v0);
        internal static glProgramUniform1fFunc glProgramUniform1fPtr;
        internal static void loadProgramUniform1f()
        {
            try
            {
                glProgramUniform1fPtr = (glProgramUniform1fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1f"), typeof(glProgramUniform1fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1f'.");
            }
        }
        public static void glProgramUniform1f(GLuint @program, GLint @location, GLfloat @v0) => glProgramUniform1fPtr(@program, @location, @v0);

        internal delegate void glProgramUniform1fvFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform1fvFunc glProgramUniform1fvPtr;
        internal static void loadProgramUniform1fv()
        {
            try
            {
                glProgramUniform1fvPtr = (glProgramUniform1fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1fv"), typeof(glProgramUniform1fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1fv'.");
            }
        }
        public static void glProgramUniform1fv(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform1fvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform1dFunc(GLuint @program, GLint @location, GLdouble @v0);
        internal static glProgramUniform1dFunc glProgramUniform1dPtr;
        internal static void loadProgramUniform1d()
        {
            try
            {
                glProgramUniform1dPtr = (glProgramUniform1dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1d"), typeof(glProgramUniform1dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1d'.");
            }
        }
        public static void glProgramUniform1d(GLuint @program, GLint @location, GLdouble @v0) => glProgramUniform1dPtr(@program, @location, @v0);

        internal delegate void glProgramUniform1dvFunc(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glProgramUniform1dvFunc glProgramUniform1dvPtr;
        internal static void loadProgramUniform1dv()
        {
            try
            {
                glProgramUniform1dvPtr = (glProgramUniform1dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1dv"), typeof(glProgramUniform1dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1dv'.");
            }
        }
        public static void glProgramUniform1dv(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value) => glProgramUniform1dvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform1uiFunc(GLuint @program, GLint @location, GLuint @v0);
        internal static glProgramUniform1uiFunc glProgramUniform1uiPtr;
        internal static void loadProgramUniform1ui()
        {
            try
            {
                glProgramUniform1uiPtr = (glProgramUniform1uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1ui"), typeof(glProgramUniform1uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1ui'.");
            }
        }
        public static void glProgramUniform1ui(GLuint @program, GLint @location, GLuint @v0) => glProgramUniform1uiPtr(@program, @location, @v0);

        internal delegate void glProgramUniform1uivFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform1uivFunc glProgramUniform1uivPtr;
        internal static void loadProgramUniform1uiv()
        {
            try
            {
                glProgramUniform1uivPtr = (glProgramUniform1uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1uiv"), typeof(glProgramUniform1uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1uiv'.");
            }
        }
        public static void glProgramUniform1uiv(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform1uivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2iFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1);
        internal static glProgramUniform2iFunc glProgramUniform2iPtr;
        internal static void loadProgramUniform2i()
        {
            try
            {
                glProgramUniform2iPtr = (glProgramUniform2iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2i"), typeof(glProgramUniform2iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2i'.");
            }
        }
        public static void glProgramUniform2i(GLuint @program, GLint @location, GLint @v0, GLint @v1) => glProgramUniform2iPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform2ivFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform2ivFunc glProgramUniform2ivPtr;
        internal static void loadProgramUniform2iv()
        {
            try
            {
                glProgramUniform2ivPtr = (glProgramUniform2ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2iv"), typeof(glProgramUniform2ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2iv'.");
            }
        }
        public static void glProgramUniform2iv(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform2ivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2fFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1);
        internal static glProgramUniform2fFunc glProgramUniform2fPtr;
        internal static void loadProgramUniform2f()
        {
            try
            {
                glProgramUniform2fPtr = (glProgramUniform2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2f"), typeof(glProgramUniform2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2f'.");
            }
        }
        public static void glProgramUniform2f(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1) => glProgramUniform2fPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform2fvFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform2fvFunc glProgramUniform2fvPtr;
        internal static void loadProgramUniform2fv()
        {
            try
            {
                glProgramUniform2fvPtr = (glProgramUniform2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2fv"), typeof(glProgramUniform2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2fv'.");
            }
        }
        public static void glProgramUniform2fv(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform2fvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2dFunc(GLuint @program, GLint @location, GLdouble @v0, GLdouble @v1);
        internal static glProgramUniform2dFunc glProgramUniform2dPtr;
        internal static void loadProgramUniform2d()
        {
            try
            {
                glProgramUniform2dPtr = (glProgramUniform2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2d"), typeof(glProgramUniform2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2d'.");
            }
        }
        public static void glProgramUniform2d(GLuint @program, GLint @location, GLdouble @v0, GLdouble @v1) => glProgramUniform2dPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform2dvFunc(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glProgramUniform2dvFunc glProgramUniform2dvPtr;
        internal static void loadProgramUniform2dv()
        {
            try
            {
                glProgramUniform2dvPtr = (glProgramUniform2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2dv"), typeof(glProgramUniform2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2dv'.");
            }
        }
        public static void glProgramUniform2dv(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value) => glProgramUniform2dvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2uiFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1);
        internal static glProgramUniform2uiFunc glProgramUniform2uiPtr;
        internal static void loadProgramUniform2ui()
        {
            try
            {
                glProgramUniform2uiPtr = (glProgramUniform2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2ui"), typeof(glProgramUniform2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2ui'.");
            }
        }
        public static void glProgramUniform2ui(GLuint @program, GLint @location, GLuint @v0, GLuint @v1) => glProgramUniform2uiPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform2uivFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform2uivFunc glProgramUniform2uivPtr;
        internal static void loadProgramUniform2uiv()
        {
            try
            {
                glProgramUniform2uivPtr = (glProgramUniform2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2uiv"), typeof(glProgramUniform2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2uiv'.");
            }
        }
        public static void glProgramUniform2uiv(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform2uivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3iFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2);
        internal static glProgramUniform3iFunc glProgramUniform3iPtr;
        internal static void loadProgramUniform3i()
        {
            try
            {
                glProgramUniform3iPtr = (glProgramUniform3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3i"), typeof(glProgramUniform3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3i'.");
            }
        }
        public static void glProgramUniform3i(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2) => glProgramUniform3iPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform3ivFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform3ivFunc glProgramUniform3ivPtr;
        internal static void loadProgramUniform3iv()
        {
            try
            {
                glProgramUniform3ivPtr = (glProgramUniform3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3iv"), typeof(glProgramUniform3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3iv'.");
            }
        }
        public static void glProgramUniform3iv(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform3ivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3fFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2);
        internal static glProgramUniform3fFunc glProgramUniform3fPtr;
        internal static void loadProgramUniform3f()
        {
            try
            {
                glProgramUniform3fPtr = (glProgramUniform3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3f"), typeof(glProgramUniform3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3f'.");
            }
        }
        public static void glProgramUniform3f(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2) => glProgramUniform3fPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform3fvFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform3fvFunc glProgramUniform3fvPtr;
        internal static void loadProgramUniform3fv()
        {
            try
            {
                glProgramUniform3fvPtr = (glProgramUniform3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3fv"), typeof(glProgramUniform3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3fv'.");
            }
        }
        public static void glProgramUniform3fv(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform3fvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3dFunc(GLuint @program, GLint @location, GLdouble @v0, GLdouble @v1, GLdouble @v2);
        internal static glProgramUniform3dFunc glProgramUniform3dPtr;
        internal static void loadProgramUniform3d()
        {
            try
            {
                glProgramUniform3dPtr = (glProgramUniform3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3d"), typeof(glProgramUniform3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3d'.");
            }
        }
        public static void glProgramUniform3d(GLuint @program, GLint @location, GLdouble @v0, GLdouble @v1, GLdouble @v2) => glProgramUniform3dPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform3dvFunc(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glProgramUniform3dvFunc glProgramUniform3dvPtr;
        internal static void loadProgramUniform3dv()
        {
            try
            {
                glProgramUniform3dvPtr = (glProgramUniform3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3dv"), typeof(glProgramUniform3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3dv'.");
            }
        }
        public static void glProgramUniform3dv(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value) => glProgramUniform3dvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3uiFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2);
        internal static glProgramUniform3uiFunc glProgramUniform3uiPtr;
        internal static void loadProgramUniform3ui()
        {
            try
            {
                glProgramUniform3uiPtr = (glProgramUniform3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3ui"), typeof(glProgramUniform3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3ui'.");
            }
        }
        public static void glProgramUniform3ui(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2) => glProgramUniform3uiPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform3uivFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform3uivFunc glProgramUniform3uivPtr;
        internal static void loadProgramUniform3uiv()
        {
            try
            {
                glProgramUniform3uivPtr = (glProgramUniform3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3uiv"), typeof(glProgramUniform3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3uiv'.");
            }
        }
        public static void glProgramUniform3uiv(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform3uivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4iFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3);
        internal static glProgramUniform4iFunc glProgramUniform4iPtr;
        internal static void loadProgramUniform4i()
        {
            try
            {
                glProgramUniform4iPtr = (glProgramUniform4iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4i"), typeof(glProgramUniform4iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4i'.");
            }
        }
        public static void glProgramUniform4i(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3) => glProgramUniform4iPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform4ivFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform4ivFunc glProgramUniform4ivPtr;
        internal static void loadProgramUniform4iv()
        {
            try
            {
                glProgramUniform4ivPtr = (glProgramUniform4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4iv"), typeof(glProgramUniform4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4iv'.");
            }
        }
        public static void glProgramUniform4iv(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform4ivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4fFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3);
        internal static glProgramUniform4fFunc glProgramUniform4fPtr;
        internal static void loadProgramUniform4f()
        {
            try
            {
                glProgramUniform4fPtr = (glProgramUniform4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4f"), typeof(glProgramUniform4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4f'.");
            }
        }
        public static void glProgramUniform4f(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3) => glProgramUniform4fPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform4fvFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform4fvFunc glProgramUniform4fvPtr;
        internal static void loadProgramUniform4fv()
        {
            try
            {
                glProgramUniform4fvPtr = (glProgramUniform4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4fv"), typeof(glProgramUniform4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4fv'.");
            }
        }
        public static void glProgramUniform4fv(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform4fvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4dFunc(GLuint @program, GLint @location, GLdouble @v0, GLdouble @v1, GLdouble @v2, GLdouble @v3);
        internal static glProgramUniform4dFunc glProgramUniform4dPtr;
        internal static void loadProgramUniform4d()
        {
            try
            {
                glProgramUniform4dPtr = (glProgramUniform4dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4d"), typeof(glProgramUniform4dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4d'.");
            }
        }
        public static void glProgramUniform4d(GLuint @program, GLint @location, GLdouble @v0, GLdouble @v1, GLdouble @v2, GLdouble @v3) => glProgramUniform4dPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform4dvFunc(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glProgramUniform4dvFunc glProgramUniform4dvPtr;
        internal static void loadProgramUniform4dv()
        {
            try
            {
                glProgramUniform4dvPtr = (glProgramUniform4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4dv"), typeof(glProgramUniform4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4dv'.");
            }
        }
        public static void glProgramUniform4dv(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value) => glProgramUniform4dvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4uiFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3);
        internal static glProgramUniform4uiFunc glProgramUniform4uiPtr;
        internal static void loadProgramUniform4ui()
        {
            try
            {
                glProgramUniform4uiPtr = (glProgramUniform4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4ui"), typeof(glProgramUniform4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4ui'.");
            }
        }
        public static void glProgramUniform4ui(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3) => glProgramUniform4uiPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform4uivFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform4uivFunc glProgramUniform4uivPtr;
        internal static void loadProgramUniform4uiv()
        {
            try
            {
                glProgramUniform4uivPtr = (glProgramUniform4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4uiv"), typeof(glProgramUniform4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4uiv'.");
            }
        }
        public static void glProgramUniform4uiv(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform4uivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniformMatrix2fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2fvFunc glProgramUniformMatrix2fvPtr;
        internal static void loadProgramUniformMatrix2fv()
        {
            try
            {
                glProgramUniformMatrix2fvPtr = (glProgramUniformMatrix2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2fv"), typeof(glProgramUniformMatrix2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2fv'.");
            }
        }
        public static void glProgramUniformMatrix2fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3fvFunc glProgramUniformMatrix3fvPtr;
        internal static void loadProgramUniformMatrix3fv()
        {
            try
            {
                glProgramUniformMatrix3fvPtr = (glProgramUniformMatrix3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3fv"), typeof(glProgramUniformMatrix3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3fv'.");
            }
        }
        public static void glProgramUniformMatrix3fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4fvFunc glProgramUniformMatrix4fvPtr;
        internal static void loadProgramUniformMatrix4fv()
        {
            try
            {
                glProgramUniformMatrix4fvPtr = (glProgramUniformMatrix4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4fv"), typeof(glProgramUniformMatrix4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4fv'.");
            }
        }
        public static void glProgramUniformMatrix4fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix2dvFunc glProgramUniformMatrix2dvPtr;
        internal static void loadProgramUniformMatrix2dv()
        {
            try
            {
                glProgramUniformMatrix2dvPtr = (glProgramUniformMatrix2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2dv"), typeof(glProgramUniformMatrix2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2dv'.");
            }
        }
        public static void glProgramUniformMatrix2dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix2dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix3dvFunc glProgramUniformMatrix3dvPtr;
        internal static void loadProgramUniformMatrix3dv()
        {
            try
            {
                glProgramUniformMatrix3dvPtr = (glProgramUniformMatrix3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3dv"), typeof(glProgramUniformMatrix3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3dv'.");
            }
        }
        public static void glProgramUniformMatrix3dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix3dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix4dvFunc glProgramUniformMatrix4dvPtr;
        internal static void loadProgramUniformMatrix4dv()
        {
            try
            {
                glProgramUniformMatrix4dvPtr = (glProgramUniformMatrix4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4dv"), typeof(glProgramUniformMatrix4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4dv'.");
            }
        }
        public static void glProgramUniformMatrix4dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix4dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x3fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2x3fvFunc glProgramUniformMatrix2x3fvPtr;
        internal static void loadProgramUniformMatrix2x3fv()
        {
            try
            {
                glProgramUniformMatrix2x3fvPtr = (glProgramUniformMatrix2x3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x3fv"), typeof(glProgramUniformMatrix2x3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x3fv'.");
            }
        }
        public static void glProgramUniformMatrix2x3fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2x3fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x2fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3x2fvFunc glProgramUniformMatrix3x2fvPtr;
        internal static void loadProgramUniformMatrix3x2fv()
        {
            try
            {
                glProgramUniformMatrix3x2fvPtr = (glProgramUniformMatrix3x2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x2fv"), typeof(glProgramUniformMatrix3x2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x2fv'.");
            }
        }
        public static void glProgramUniformMatrix3x2fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3x2fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x4fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2x4fvFunc glProgramUniformMatrix2x4fvPtr;
        internal static void loadProgramUniformMatrix2x4fv()
        {
            try
            {
                glProgramUniformMatrix2x4fvPtr = (glProgramUniformMatrix2x4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x4fv"), typeof(glProgramUniformMatrix2x4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x4fv'.");
            }
        }
        public static void glProgramUniformMatrix2x4fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2x4fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x2fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4x2fvFunc glProgramUniformMatrix4x2fvPtr;
        internal static void loadProgramUniformMatrix4x2fv()
        {
            try
            {
                glProgramUniformMatrix4x2fvPtr = (glProgramUniformMatrix4x2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x2fv"), typeof(glProgramUniformMatrix4x2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x2fv'.");
            }
        }
        public static void glProgramUniformMatrix4x2fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4x2fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x4fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3x4fvFunc glProgramUniformMatrix3x4fvPtr;
        internal static void loadProgramUniformMatrix3x4fv()
        {
            try
            {
                glProgramUniformMatrix3x4fvPtr = (glProgramUniformMatrix3x4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x4fv"), typeof(glProgramUniformMatrix3x4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x4fv'.");
            }
        }
        public static void glProgramUniformMatrix3x4fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3x4fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x3fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4x3fvFunc glProgramUniformMatrix4x3fvPtr;
        internal static void loadProgramUniformMatrix4x3fv()
        {
            try
            {
                glProgramUniformMatrix4x3fvPtr = (glProgramUniformMatrix4x3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x3fv"), typeof(glProgramUniformMatrix4x3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x3fv'.");
            }
        }
        public static void glProgramUniformMatrix4x3fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4x3fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x3dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix2x3dvFunc glProgramUniformMatrix2x3dvPtr;
        internal static void loadProgramUniformMatrix2x3dv()
        {
            try
            {
                glProgramUniformMatrix2x3dvPtr = (glProgramUniformMatrix2x3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x3dv"), typeof(glProgramUniformMatrix2x3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x3dv'.");
            }
        }
        public static void glProgramUniformMatrix2x3dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix2x3dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x2dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix3x2dvFunc glProgramUniformMatrix3x2dvPtr;
        internal static void loadProgramUniformMatrix3x2dv()
        {
            try
            {
                glProgramUniformMatrix3x2dvPtr = (glProgramUniformMatrix3x2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x2dv"), typeof(glProgramUniformMatrix3x2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x2dv'.");
            }
        }
        public static void glProgramUniformMatrix3x2dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix3x2dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x4dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix2x4dvFunc glProgramUniformMatrix2x4dvPtr;
        internal static void loadProgramUniformMatrix2x4dv()
        {
            try
            {
                glProgramUniformMatrix2x4dvPtr = (glProgramUniformMatrix2x4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x4dv"), typeof(glProgramUniformMatrix2x4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x4dv'.");
            }
        }
        public static void glProgramUniformMatrix2x4dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix2x4dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x2dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix4x2dvFunc glProgramUniformMatrix4x2dvPtr;
        internal static void loadProgramUniformMatrix4x2dv()
        {
            try
            {
                glProgramUniformMatrix4x2dvPtr = (glProgramUniformMatrix4x2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x2dv"), typeof(glProgramUniformMatrix4x2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x2dv'.");
            }
        }
        public static void glProgramUniformMatrix4x2dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix4x2dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x4dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix3x4dvFunc glProgramUniformMatrix3x4dvPtr;
        internal static void loadProgramUniformMatrix3x4dv()
        {
            try
            {
                glProgramUniformMatrix3x4dvPtr = (glProgramUniformMatrix3x4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x4dv"), typeof(glProgramUniformMatrix3x4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x4dv'.");
            }
        }
        public static void glProgramUniformMatrix3x4dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix3x4dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x3dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix4x3dvFunc glProgramUniformMatrix4x3dvPtr;
        internal static void loadProgramUniformMatrix4x3dv()
        {
            try
            {
                glProgramUniformMatrix4x3dvPtr = (glProgramUniformMatrix4x3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x3dv"), typeof(glProgramUniformMatrix4x3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x3dv'.");
            }
        }
        public static void glProgramUniformMatrix4x3dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix4x3dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glValidateProgramPipelineFunc(GLuint @pipeline);
        internal static glValidateProgramPipelineFunc glValidateProgramPipelinePtr;
        internal static void loadValidateProgramPipeline()
        {
            try
            {
                glValidateProgramPipelinePtr = (glValidateProgramPipelineFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glValidateProgramPipeline"), typeof(glValidateProgramPipelineFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glValidateProgramPipeline'.");
            }
        }
        public static void glValidateProgramPipeline(GLuint @pipeline) => glValidateProgramPipelinePtr(@pipeline);

        internal delegate void glGetProgramPipelineInfoLogFunc(GLuint @pipeline, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog);
        internal static glGetProgramPipelineInfoLogFunc glGetProgramPipelineInfoLogPtr;
        internal static void loadGetProgramPipelineInfoLog()
        {
            try
            {
                glGetProgramPipelineInfoLogPtr = (glGetProgramPipelineInfoLogFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramPipelineInfoLog"), typeof(glGetProgramPipelineInfoLogFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramPipelineInfoLog'.");
            }
        }
        public static void glGetProgramPipelineInfoLog(GLuint @pipeline, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog) => glGetProgramPipelineInfoLogPtr(@pipeline, @bufSize, @length, @infoLog);

        internal delegate void glVertexAttribL1dFunc(GLuint @index, GLdouble @x);
        internal static glVertexAttribL1dFunc glVertexAttribL1dPtr;
        internal static void loadVertexAttribL1d()
        {
            try
            {
                glVertexAttribL1dPtr = (glVertexAttribL1dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL1d"), typeof(glVertexAttribL1dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL1d'.");
            }
        }
        public static void glVertexAttribL1d(GLuint @index, GLdouble @x) => glVertexAttribL1dPtr(@index, @x);

        internal delegate void glVertexAttribL2dFunc(GLuint @index, GLdouble @x, GLdouble @y);
        internal static glVertexAttribL2dFunc glVertexAttribL2dPtr;
        internal static void loadVertexAttribL2d()
        {
            try
            {
                glVertexAttribL2dPtr = (glVertexAttribL2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL2d"), typeof(glVertexAttribL2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL2d'.");
            }
        }
        public static void glVertexAttribL2d(GLuint @index, GLdouble @x, GLdouble @y) => glVertexAttribL2dPtr(@index, @x, @y);

        internal delegate void glVertexAttribL3dFunc(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glVertexAttribL3dFunc glVertexAttribL3dPtr;
        internal static void loadVertexAttribL3d()
        {
            try
            {
                glVertexAttribL3dPtr = (glVertexAttribL3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL3d"), typeof(glVertexAttribL3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL3d'.");
            }
        }
        public static void glVertexAttribL3d(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z) => glVertexAttribL3dPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttribL4dFunc(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glVertexAttribL4dFunc glVertexAttribL4dPtr;
        internal static void loadVertexAttribL4d()
        {
            try
            {
                glVertexAttribL4dPtr = (glVertexAttribL4dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL4d"), typeof(glVertexAttribL4dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL4d'.");
            }
        }
        public static void glVertexAttribL4d(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glVertexAttribL4dPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttribL1dvFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttribL1dvFunc glVertexAttribL1dvPtr;
        internal static void loadVertexAttribL1dv()
        {
            try
            {
                glVertexAttribL1dvPtr = (glVertexAttribL1dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL1dv"), typeof(glVertexAttribL1dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL1dv'.");
            }
        }
        public static void glVertexAttribL1dv(GLuint @index, const GLdouble * @v) => glVertexAttribL1dvPtr(@index, @v);

        internal delegate void glVertexAttribL2dvFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttribL2dvFunc glVertexAttribL2dvPtr;
        internal static void loadVertexAttribL2dv()
        {
            try
            {
                glVertexAttribL2dvPtr = (glVertexAttribL2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL2dv"), typeof(glVertexAttribL2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL2dv'.");
            }
        }
        public static void glVertexAttribL2dv(GLuint @index, const GLdouble * @v) => glVertexAttribL2dvPtr(@index, @v);

        internal delegate void glVertexAttribL3dvFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttribL3dvFunc glVertexAttribL3dvPtr;
        internal static void loadVertexAttribL3dv()
        {
            try
            {
                glVertexAttribL3dvPtr = (glVertexAttribL3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL3dv"), typeof(glVertexAttribL3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL3dv'.");
            }
        }
        public static void glVertexAttribL3dv(GLuint @index, const GLdouble * @v) => glVertexAttribL3dvPtr(@index, @v);

        internal delegate void glVertexAttribL4dvFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttribL4dvFunc glVertexAttribL4dvPtr;
        internal static void loadVertexAttribL4dv()
        {
            try
            {
                glVertexAttribL4dvPtr = (glVertexAttribL4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL4dv"), typeof(glVertexAttribL4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL4dv'.");
            }
        }
        public static void glVertexAttribL4dv(GLuint @index, const GLdouble * @v) => glVertexAttribL4dvPtr(@index, @v);

        internal delegate void glVertexAttribLPointerFunc(GLuint @index, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glVertexAttribLPointerFunc glVertexAttribLPointerPtr;
        internal static void loadVertexAttribLPointer()
        {
            try
            {
                glVertexAttribLPointerPtr = (glVertexAttribLPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribLPointer"), typeof(glVertexAttribLPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribLPointer'.");
            }
        }
        public static void glVertexAttribLPointer(GLuint @index, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glVertexAttribLPointerPtr(@index, @size, @type, @stride, @pointer);

        internal delegate void glGetVertexAttribLdvFunc(GLuint @index, GLenum @pname, GLdouble * @params);
        internal static glGetVertexAttribLdvFunc glGetVertexAttribLdvPtr;
        internal static void loadGetVertexAttribLdv()
        {
            try
            {
                glGetVertexAttribLdvPtr = (glGetVertexAttribLdvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribLdv"), typeof(glGetVertexAttribLdvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribLdv'.");
            }
        }
        public static void glGetVertexAttribLdv(GLuint @index, GLenum @pname, GLdouble * @params) => glGetVertexAttribLdvPtr(@index, @pname, @params);

        internal delegate void glViewportArrayvFunc(GLuint @first, GLsizei @count, const GLfloat * @v);
        internal static glViewportArrayvFunc glViewportArrayvPtr;
        internal static void loadViewportArrayv()
        {
            try
            {
                glViewportArrayvPtr = (glViewportArrayvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glViewportArrayv"), typeof(glViewportArrayvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glViewportArrayv'.");
            }
        }
        public static void glViewportArrayv(GLuint @first, GLsizei @count, const GLfloat * @v) => glViewportArrayvPtr(@first, @count, @v);

        internal delegate void glViewportIndexedfFunc(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @w, GLfloat @h);
        internal static glViewportIndexedfFunc glViewportIndexedfPtr;
        internal static void loadViewportIndexedf()
        {
            try
            {
                glViewportIndexedfPtr = (glViewportIndexedfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glViewportIndexedf"), typeof(glViewportIndexedfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glViewportIndexedf'.");
            }
        }
        public static void glViewportIndexedf(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @w, GLfloat @h) => glViewportIndexedfPtr(@index, @x, @y, @w, @h);

        internal delegate void glViewportIndexedfvFunc(GLuint @index, const GLfloat * @v);
        internal static glViewportIndexedfvFunc glViewportIndexedfvPtr;
        internal static void loadViewportIndexedfv()
        {
            try
            {
                glViewportIndexedfvPtr = (glViewportIndexedfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glViewportIndexedfv"), typeof(glViewportIndexedfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glViewportIndexedfv'.");
            }
        }
        public static void glViewportIndexedfv(GLuint @index, const GLfloat * @v) => glViewportIndexedfvPtr(@index, @v);

        internal delegate void glScissorArrayvFunc(GLuint @first, GLsizei @count, const GLint * @v);
        internal static glScissorArrayvFunc glScissorArrayvPtr;
        internal static void loadScissorArrayv()
        {
            try
            {
                glScissorArrayvPtr = (glScissorArrayvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScissorArrayv"), typeof(glScissorArrayvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScissorArrayv'.");
            }
        }
        public static void glScissorArrayv(GLuint @first, GLsizei @count, const GLint * @v) => glScissorArrayvPtr(@first, @count, @v);

        internal delegate void glScissorIndexedFunc(GLuint @index, GLint @left, GLint @bottom, GLsizei @width, GLsizei @height);
        internal static glScissorIndexedFunc glScissorIndexedPtr;
        internal static void loadScissorIndexed()
        {
            try
            {
                glScissorIndexedPtr = (glScissorIndexedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScissorIndexed"), typeof(glScissorIndexedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScissorIndexed'.");
            }
        }
        public static void glScissorIndexed(GLuint @index, GLint @left, GLint @bottom, GLsizei @width, GLsizei @height) => glScissorIndexedPtr(@index, @left, @bottom, @width, @height);

        internal delegate void glScissorIndexedvFunc(GLuint @index, const GLint * @v);
        internal static glScissorIndexedvFunc glScissorIndexedvPtr;
        internal static void loadScissorIndexedv()
        {
            try
            {
                glScissorIndexedvPtr = (glScissorIndexedvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScissorIndexedv"), typeof(glScissorIndexedvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScissorIndexedv'.");
            }
        }
        public static void glScissorIndexedv(GLuint @index, const GLint * @v) => glScissorIndexedvPtr(@index, @v);

        internal delegate void glDepthRangeArrayvFunc(GLuint @first, GLsizei @count, const GLdouble * @v);
        internal static glDepthRangeArrayvFunc glDepthRangeArrayvPtr;
        internal static void loadDepthRangeArrayv()
        {
            try
            {
                glDepthRangeArrayvPtr = (glDepthRangeArrayvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangeArrayv"), typeof(glDepthRangeArrayvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangeArrayv'.");
            }
        }
        public static void glDepthRangeArrayv(GLuint @first, GLsizei @count, const GLdouble * @v) => glDepthRangeArrayvPtr(@first, @count, @v);

        internal delegate void glDepthRangeIndexedFunc(GLuint @index, GLdouble @n, GLdouble @f);
        internal static glDepthRangeIndexedFunc glDepthRangeIndexedPtr;
        internal static void loadDepthRangeIndexed()
        {
            try
            {
                glDepthRangeIndexedPtr = (glDepthRangeIndexedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangeIndexed"), typeof(glDepthRangeIndexedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangeIndexed'.");
            }
        }
        public static void glDepthRangeIndexed(GLuint @index, GLdouble @n, GLdouble @f) => glDepthRangeIndexedPtr(@index, @n, @f);

        internal delegate void glGetFloati_vFunc(GLenum @target, GLuint @index, GLfloat * @data);
        internal static glGetFloati_vFunc glGetFloati_vPtr;
        internal static void loadGetFloati_v()
        {
            try
            {
                glGetFloati_vPtr = (glGetFloati_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFloati_v"), typeof(glGetFloati_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFloati_v'.");
            }
        }
        public static void glGetFloati_v(GLenum @target, GLuint @index, GLfloat * @data) => glGetFloati_vPtr(@target, @index, @data);

        internal delegate void glGetDoublei_vFunc(GLenum @target, GLuint @index, GLdouble * @data);
        internal static glGetDoublei_vFunc glGetDoublei_vPtr;
        internal static void loadGetDoublei_v()
        {
            try
            {
                glGetDoublei_vPtr = (glGetDoublei_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDoublei_v"), typeof(glGetDoublei_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDoublei_v'.");
            }
        }
        public static void glGetDoublei_v(GLenum @target, GLuint @index, GLdouble * @data) => glGetDoublei_vPtr(@target, @index, @data);
        #endregion
    }
}
