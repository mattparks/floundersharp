using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_compressed_ATC_texture
    {
        #region Interop
        static GL_AMD_compressed_ATC_texture()
        {
            Console.WriteLine("Initalising GL_AMD_compressed_ATC_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_ATC_RGB_AMD = 0x8C92;
        public static UInt32 GL_ATC_RGBA_EXPLICIT_ALPHA_AMD = 0x8C93;
        public static UInt32 GL_ATC_RGBA_INTERPOLATED_ALPHA_AMD = 0x87EE;
        #endregion

        #region Commands
        #endregion
    }
}
