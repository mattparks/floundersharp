using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_pinned_memory
    {
        #region Interop
        static GL_AMD_pinned_memory()
        {
            Console.WriteLine("Initalising GL_AMD_pinned_memory interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_EXTERNAL_VIRTUAL_MEMORY_BUFFER_AMD = 0x9160;
        #endregion

        #region Commands
        #endregion
    }
}
