using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_stencil_operation_extended
    {
        #region Interop
        static GL_AMD_stencil_operation_extended()
        {
            Console.WriteLine("Initalising GL_AMD_stencil_operation_extended interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadStencilOpValueAMD();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SET_AMD = 0x874A;
        public static UInt32 GL_REPLACE_VALUE_AMD = 0x874B;
        public static UInt32 GL_STENCIL_OP_VALUE_AMD = 0x874C;
        public static UInt32 GL_STENCIL_BACK_OP_VALUE_AMD = 0x874D;
        #endregion

        #region Commands
        internal delegate void glStencilOpValueAMDFunc(GLenum @face, GLuint @value);
        internal static glStencilOpValueAMDFunc glStencilOpValueAMDPtr;
        internal static void loadStencilOpValueAMD()
        {
            try
            {
                glStencilOpValueAMDPtr = (glStencilOpValueAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilOpValueAMD"), typeof(glStencilOpValueAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilOpValueAMD'.");
            }
        }
        public static void glStencilOpValueAMD(GLenum @face, GLuint @value) => glStencilOpValueAMDPtr(@face, @value);
        #endregion
    }
}
