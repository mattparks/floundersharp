using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_program_binary_Z400
    {
        #region Interop
        static GL_AMD_program_binary_Z400()
        {
            Console.WriteLine("Initalising GL_AMD_program_binary_Z400 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_Z400_BINARY_AMD = 0x8740;
        #endregion

        #region Commands
        #endregion
    }
}
