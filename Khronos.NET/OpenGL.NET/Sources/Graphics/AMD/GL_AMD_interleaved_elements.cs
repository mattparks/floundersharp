using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_interleaved_elements
    {
        #region Interop
        static GL_AMD_interleaved_elements()
        {
            Console.WriteLine("Initalising GL_AMD_interleaved_elements interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexAttribParameteriAMD();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ELEMENT_SWIZZLE_AMD = 0x91A4;
        public static UInt32 GL_VERTEX_ID_SWIZZLE_AMD = 0x91A5;
        public static UInt32 GL_RED = 0x1903;
        public static UInt32 GL_GREEN = 0x1904;
        public static UInt32 GL_BLUE = 0x1905;
        public static UInt32 GL_ALPHA = 0x1906;
        public static UInt32 GL_RG8UI = 0x8238;
        public static UInt32 GL_RG16UI = 0x823A;
        public static UInt32 GL_RGBA8UI = 0x8D7C;
        #endregion

        #region Commands
        internal delegate void glVertexAttribParameteriAMDFunc(GLuint @index, GLenum @pname, GLint @param);
        internal static glVertexAttribParameteriAMDFunc glVertexAttribParameteriAMDPtr;
        internal static void loadVertexAttribParameteriAMD()
        {
            try
            {
                glVertexAttribParameteriAMDPtr = (glVertexAttribParameteriAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribParameteriAMD"), typeof(glVertexAttribParameteriAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribParameteriAMD'.");
            }
        }
        public static void glVertexAttribParameteriAMD(GLuint @index, GLenum @pname, GLint @param) => glVertexAttribParameteriAMDPtr(@index, @pname, @param);
        #endregion
    }
}
