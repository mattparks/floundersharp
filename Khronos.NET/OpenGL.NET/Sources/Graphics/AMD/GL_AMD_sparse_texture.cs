using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_sparse_texture
    {
        #region Interop
        static GL_AMD_sparse_texture()
        {
            Console.WriteLine("Initalising GL_AMD_sparse_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexStorageSparseAMD();
            loadTextureStorageSparseAMD();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VIRTUAL_PAGE_SIZE_X_AMD = 0x9195;
        public static UInt32 GL_VIRTUAL_PAGE_SIZE_Y_AMD = 0x9196;
        public static UInt32 GL_VIRTUAL_PAGE_SIZE_Z_AMD = 0x9197;
        public static UInt32 GL_MAX_SPARSE_TEXTURE_SIZE_AMD = 0x9198;
        public static UInt32 GL_MAX_SPARSE_3D_TEXTURE_SIZE_AMD = 0x9199;
        public static UInt32 GL_MAX_SPARSE_ARRAY_TEXTURE_LAYERS = 0x919A;
        public static UInt32 GL_MIN_SPARSE_LEVEL_AMD = 0x919B;
        public static UInt32 GL_MIN_LOD_WARNING_AMD = 0x919C;
        public static UInt32 GL_TEXTURE_STORAGE_SPARSE_BIT_AMD = 0x00000001;
        #endregion

        #region Commands
        internal delegate void glTexStorageSparseAMDFunc(GLenum @target, GLenum @internalFormat, GLsizei @width, GLsizei @height, GLsizei @depth, GLsizei @layers, GLbitfield @flags);
        internal static glTexStorageSparseAMDFunc glTexStorageSparseAMDPtr;
        internal static void loadTexStorageSparseAMD()
        {
            try
            {
                glTexStorageSparseAMDPtr = (glTexStorageSparseAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorageSparseAMD"), typeof(glTexStorageSparseAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorageSparseAMD'.");
            }
        }
        public static void glTexStorageSparseAMD(GLenum @target, GLenum @internalFormat, GLsizei @width, GLsizei @height, GLsizei @depth, GLsizei @layers, GLbitfield @flags) => glTexStorageSparseAMDPtr(@target, @internalFormat, @width, @height, @depth, @layers, @flags);

        internal delegate void glTextureStorageSparseAMDFunc(GLuint @texture, GLenum @target, GLenum @internalFormat, GLsizei @width, GLsizei @height, GLsizei @depth, GLsizei @layers, GLbitfield @flags);
        internal static glTextureStorageSparseAMDFunc glTextureStorageSparseAMDPtr;
        internal static void loadTextureStorageSparseAMD()
        {
            try
            {
                glTextureStorageSparseAMDPtr = (glTextureStorageSparseAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorageSparseAMD"), typeof(glTextureStorageSparseAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorageSparseAMD'.");
            }
        }
        public static void glTextureStorageSparseAMD(GLuint @texture, GLenum @target, GLenum @internalFormat, GLsizei @width, GLsizei @height, GLsizei @depth, GLsizei @layers, GLbitfield @flags) => glTextureStorageSparseAMDPtr(@texture, @target, @internalFormat, @width, @height, @depth, @layers, @flags);
        #endregion
    }
}
