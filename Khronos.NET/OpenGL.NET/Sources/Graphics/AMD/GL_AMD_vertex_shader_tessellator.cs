using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_vertex_shader_tessellator
    {
        #region Interop
        static GL_AMD_vertex_shader_tessellator()
        {
            Console.WriteLine("Initalising GL_AMD_vertex_shader_tessellator interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTessellationFactorAMD();
            loadTessellationModeAMD();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SAMPLER_BUFFER_AMD = 0x9001;
        public static UInt32 GL_INT_SAMPLER_BUFFER_AMD = 0x9002;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_BUFFER_AMD = 0x9003;
        public static UInt32 GL_TESSELLATION_MODE_AMD = 0x9004;
        public static UInt32 GL_TESSELLATION_FACTOR_AMD = 0x9005;
        public static UInt32 GL_DISCRETE_AMD = 0x9006;
        public static UInt32 GL_CONTINUOUS_AMD = 0x9007;
        #endregion

        #region Commands
        internal delegate void glTessellationFactorAMDFunc(GLfloat @factor);
        internal static glTessellationFactorAMDFunc glTessellationFactorAMDPtr;
        internal static void loadTessellationFactorAMD()
        {
            try
            {
                glTessellationFactorAMDPtr = (glTessellationFactorAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTessellationFactorAMD"), typeof(glTessellationFactorAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTessellationFactorAMD'.");
            }
        }
        public static void glTessellationFactorAMD(GLfloat @factor) => glTessellationFactorAMDPtr(@factor);

        internal delegate void glTessellationModeAMDFunc(GLenum @mode);
        internal static glTessellationModeAMDFunc glTessellationModeAMDPtr;
        internal static void loadTessellationModeAMD()
        {
            try
            {
                glTessellationModeAMDPtr = (glTessellationModeAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTessellationModeAMD"), typeof(glTessellationModeAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTessellationModeAMD'.");
            }
        }
        public static void glTessellationModeAMD(GLenum @mode) => glTessellationModeAMDPtr(@mode);
        #endregion
    }
}
