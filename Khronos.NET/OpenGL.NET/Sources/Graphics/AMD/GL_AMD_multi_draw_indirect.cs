using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_multi_draw_indirect
    {
        #region Interop
        static GL_AMD_multi_draw_indirect()
        {
            Console.WriteLine("Initalising GL_AMD_multi_draw_indirect interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMultiDrawArraysIndirectAMD();
            loadMultiDrawElementsIndirectAMD();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glMultiDrawArraysIndirectAMDFunc(GLenum @mode, const void * @indirect, GLsizei @primcount, GLsizei @stride);
        internal static glMultiDrawArraysIndirectAMDFunc glMultiDrawArraysIndirectAMDPtr;
        internal static void loadMultiDrawArraysIndirectAMD()
        {
            try
            {
                glMultiDrawArraysIndirectAMDPtr = (glMultiDrawArraysIndirectAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawArraysIndirectAMD"), typeof(glMultiDrawArraysIndirectAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawArraysIndirectAMD'.");
            }
        }
        public static void glMultiDrawArraysIndirectAMD(GLenum @mode, const void * @indirect, GLsizei @primcount, GLsizei @stride) => glMultiDrawArraysIndirectAMDPtr(@mode, @indirect, @primcount, @stride);

        internal delegate void glMultiDrawElementsIndirectAMDFunc(GLenum @mode, GLenum @type, const void * @indirect, GLsizei @primcount, GLsizei @stride);
        internal static glMultiDrawElementsIndirectAMDFunc glMultiDrawElementsIndirectAMDPtr;
        internal static void loadMultiDrawElementsIndirectAMD()
        {
            try
            {
                glMultiDrawElementsIndirectAMDPtr = (glMultiDrawElementsIndirectAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementsIndirectAMD"), typeof(glMultiDrawElementsIndirectAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementsIndirectAMD'.");
            }
        }
        public static void glMultiDrawElementsIndirectAMD(GLenum @mode, GLenum @type, const void * @indirect, GLsizei @primcount, GLsizei @stride) => glMultiDrawElementsIndirectAMDPtr(@mode, @type, @indirect, @primcount, @stride);
        #endregion
    }
}
