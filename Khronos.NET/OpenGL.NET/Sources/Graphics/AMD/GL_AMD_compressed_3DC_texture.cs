using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_compressed_3DC_texture
    {
        #region Interop
        static GL_AMD_compressed_3DC_texture()
        {
            Console.WriteLine("Initalising GL_AMD_compressed_3DC_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_3DC_X_AMD = 0x87F9;
        public static UInt32 GL_3DC_XY_AMD = 0x87FA;
        #endregion

        #region Commands
        #endregion
    }
}
