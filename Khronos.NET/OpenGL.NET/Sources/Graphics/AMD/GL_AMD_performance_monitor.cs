using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_performance_monitor
    {
        #region Interop
        static GL_AMD_performance_monitor()
        {
            Console.WriteLine("Initalising GL_AMD_performance_monitor interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetPerfMonitorGroupsAMD();
            loadGetPerfMonitorCountersAMD();
            loadGetPerfMonitorGroupStringAMD();
            loadGetPerfMonitorCounterStringAMD();
            loadGetPerfMonitorCounterInfoAMD();
            loadGenPerfMonitorsAMD();
            loadDeletePerfMonitorsAMD();
            loadSelectPerfMonitorCountersAMD();
            loadBeginPerfMonitorAMD();
            loadEndPerfMonitorAMD();
            loadGetPerfMonitorCounterDataAMD();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COUNTER_TYPE_AMD = 0x8BC0;
        public static UInt32 GL_COUNTER_RANGE_AMD = 0x8BC1;
        public static UInt32 GL_UNSIGNED_INT64_AMD = 0x8BC2;
        public static UInt32 GL_PERCENTAGE_AMD = 0x8BC3;
        public static UInt32 GL_PERFMON_RESULT_AVAILABLE_AMD = 0x8BC4;
        public static UInt32 GL_PERFMON_RESULT_SIZE_AMD = 0x8BC5;
        public static UInt32 GL_PERFMON_RESULT_AMD = 0x8BC6;
        #endregion

        #region Commands
        internal delegate void glGetPerfMonitorGroupsAMDFunc(GLint * @numGroups, GLsizei @groupsSize, GLuint * @groups);
        internal static glGetPerfMonitorGroupsAMDFunc glGetPerfMonitorGroupsAMDPtr;
        internal static void loadGetPerfMonitorGroupsAMD()
        {
            try
            {
                glGetPerfMonitorGroupsAMDPtr = (glGetPerfMonitorGroupsAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPerfMonitorGroupsAMD"), typeof(glGetPerfMonitorGroupsAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPerfMonitorGroupsAMD'.");
            }
        }
        public static void glGetPerfMonitorGroupsAMD(GLint * @numGroups, GLsizei @groupsSize, GLuint * @groups) => glGetPerfMonitorGroupsAMDPtr(@numGroups, @groupsSize, @groups);

        internal delegate void glGetPerfMonitorCountersAMDFunc(GLuint @group, GLint * @numCounters, GLint * @maxActiveCounters, GLsizei @counterSize, GLuint * @counters);
        internal static glGetPerfMonitorCountersAMDFunc glGetPerfMonitorCountersAMDPtr;
        internal static void loadGetPerfMonitorCountersAMD()
        {
            try
            {
                glGetPerfMonitorCountersAMDPtr = (glGetPerfMonitorCountersAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPerfMonitorCountersAMD"), typeof(glGetPerfMonitorCountersAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPerfMonitorCountersAMD'.");
            }
        }
        public static void glGetPerfMonitorCountersAMD(GLuint @group, GLint * @numCounters, GLint * @maxActiveCounters, GLsizei @counterSize, GLuint * @counters) => glGetPerfMonitorCountersAMDPtr(@group, @numCounters, @maxActiveCounters, @counterSize, @counters);

        internal delegate void glGetPerfMonitorGroupStringAMDFunc(GLuint @group, GLsizei @bufSize, GLsizei * @length, GLchar * @groupString);
        internal static glGetPerfMonitorGroupStringAMDFunc glGetPerfMonitorGroupStringAMDPtr;
        internal static void loadGetPerfMonitorGroupStringAMD()
        {
            try
            {
                glGetPerfMonitorGroupStringAMDPtr = (glGetPerfMonitorGroupStringAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPerfMonitorGroupStringAMD"), typeof(glGetPerfMonitorGroupStringAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPerfMonitorGroupStringAMD'.");
            }
        }
        public static void glGetPerfMonitorGroupStringAMD(GLuint @group, GLsizei @bufSize, GLsizei * @length, GLchar * @groupString) => glGetPerfMonitorGroupStringAMDPtr(@group, @bufSize, @length, @groupString);

        internal delegate void glGetPerfMonitorCounterStringAMDFunc(GLuint @group, GLuint @counter, GLsizei @bufSize, GLsizei * @length, GLchar * @counterString);
        internal static glGetPerfMonitorCounterStringAMDFunc glGetPerfMonitorCounterStringAMDPtr;
        internal static void loadGetPerfMonitorCounterStringAMD()
        {
            try
            {
                glGetPerfMonitorCounterStringAMDPtr = (glGetPerfMonitorCounterStringAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPerfMonitorCounterStringAMD"), typeof(glGetPerfMonitorCounterStringAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPerfMonitorCounterStringAMD'.");
            }
        }
        public static void glGetPerfMonitorCounterStringAMD(GLuint @group, GLuint @counter, GLsizei @bufSize, GLsizei * @length, GLchar * @counterString) => glGetPerfMonitorCounterStringAMDPtr(@group, @counter, @bufSize, @length, @counterString);

        internal delegate void glGetPerfMonitorCounterInfoAMDFunc(GLuint @group, GLuint @counter, GLenum @pname, void * @data);
        internal static glGetPerfMonitorCounterInfoAMDFunc glGetPerfMonitorCounterInfoAMDPtr;
        internal static void loadGetPerfMonitorCounterInfoAMD()
        {
            try
            {
                glGetPerfMonitorCounterInfoAMDPtr = (glGetPerfMonitorCounterInfoAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPerfMonitorCounterInfoAMD"), typeof(glGetPerfMonitorCounterInfoAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPerfMonitorCounterInfoAMD'.");
            }
        }
        public static void glGetPerfMonitorCounterInfoAMD(GLuint @group, GLuint @counter, GLenum @pname, void * @data) => glGetPerfMonitorCounterInfoAMDPtr(@group, @counter, @pname, @data);

        internal delegate void glGenPerfMonitorsAMDFunc(GLsizei @n, GLuint * @monitors);
        internal static glGenPerfMonitorsAMDFunc glGenPerfMonitorsAMDPtr;
        internal static void loadGenPerfMonitorsAMD()
        {
            try
            {
                glGenPerfMonitorsAMDPtr = (glGenPerfMonitorsAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenPerfMonitorsAMD"), typeof(glGenPerfMonitorsAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenPerfMonitorsAMD'.");
            }
        }
        public static void glGenPerfMonitorsAMD(GLsizei @n, GLuint * @monitors) => glGenPerfMonitorsAMDPtr(@n, @monitors);

        internal delegate void glDeletePerfMonitorsAMDFunc(GLsizei @n, GLuint * @monitors);
        internal static glDeletePerfMonitorsAMDFunc glDeletePerfMonitorsAMDPtr;
        internal static void loadDeletePerfMonitorsAMD()
        {
            try
            {
                glDeletePerfMonitorsAMDPtr = (glDeletePerfMonitorsAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeletePerfMonitorsAMD"), typeof(glDeletePerfMonitorsAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeletePerfMonitorsAMD'.");
            }
        }
        public static void glDeletePerfMonitorsAMD(GLsizei @n, GLuint * @monitors) => glDeletePerfMonitorsAMDPtr(@n, @monitors);

        internal delegate void glSelectPerfMonitorCountersAMDFunc(GLuint @monitor, GLboolean @enable, GLuint @group, GLint @numCounters, GLuint * @counterList);
        internal static glSelectPerfMonitorCountersAMDFunc glSelectPerfMonitorCountersAMDPtr;
        internal static void loadSelectPerfMonitorCountersAMD()
        {
            try
            {
                glSelectPerfMonitorCountersAMDPtr = (glSelectPerfMonitorCountersAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSelectPerfMonitorCountersAMD"), typeof(glSelectPerfMonitorCountersAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSelectPerfMonitorCountersAMD'.");
            }
        }
        public static void glSelectPerfMonitorCountersAMD(GLuint @monitor, GLboolean @enable, GLuint @group, GLint @numCounters, GLuint * @counterList) => glSelectPerfMonitorCountersAMDPtr(@monitor, @enable, @group, @numCounters, @counterList);

        internal delegate void glBeginPerfMonitorAMDFunc(GLuint @monitor);
        internal static glBeginPerfMonitorAMDFunc glBeginPerfMonitorAMDPtr;
        internal static void loadBeginPerfMonitorAMD()
        {
            try
            {
                glBeginPerfMonitorAMDPtr = (glBeginPerfMonitorAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginPerfMonitorAMD"), typeof(glBeginPerfMonitorAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginPerfMonitorAMD'.");
            }
        }
        public static void glBeginPerfMonitorAMD(GLuint @monitor) => glBeginPerfMonitorAMDPtr(@monitor);

        internal delegate void glEndPerfMonitorAMDFunc(GLuint @monitor);
        internal static glEndPerfMonitorAMDFunc glEndPerfMonitorAMDPtr;
        internal static void loadEndPerfMonitorAMD()
        {
            try
            {
                glEndPerfMonitorAMDPtr = (glEndPerfMonitorAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndPerfMonitorAMD"), typeof(glEndPerfMonitorAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndPerfMonitorAMD'.");
            }
        }
        public static void glEndPerfMonitorAMD(GLuint @monitor) => glEndPerfMonitorAMDPtr(@monitor);

        internal delegate void glGetPerfMonitorCounterDataAMDFunc(GLuint @monitor, GLenum @pname, GLsizei @dataSize, GLuint * @data, GLint * @bytesWritten);
        internal static glGetPerfMonitorCounterDataAMDFunc glGetPerfMonitorCounterDataAMDPtr;
        internal static void loadGetPerfMonitorCounterDataAMD()
        {
            try
            {
                glGetPerfMonitorCounterDataAMDPtr = (glGetPerfMonitorCounterDataAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPerfMonitorCounterDataAMD"), typeof(glGetPerfMonitorCounterDataAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPerfMonitorCounterDataAMD'.");
            }
        }
        public static void glGetPerfMonitorCounterDataAMD(GLuint @monitor, GLenum @pname, GLsizei @dataSize, GLuint * @data, GLint * @bytesWritten) => glGetPerfMonitorCounterDataAMDPtr(@monitor, @pname, @dataSize, @data, @bytesWritten);
        #endregion
    }
}
