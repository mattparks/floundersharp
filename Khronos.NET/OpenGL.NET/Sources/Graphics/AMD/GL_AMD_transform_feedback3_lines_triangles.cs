using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_transform_feedback3_lines_triangles
    {
        #region Interop
        static GL_AMD_transform_feedback3_lines_triangles()
        {
            Console.WriteLine("Initalising GL_AMD_transform_feedback3_lines_triangles interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        #endregion
    }
}
