using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_sample_positions
    {
        #region Interop
        static GL_AMD_sample_positions()
        {
            Console.WriteLine("Initalising GL_AMD_sample_positions interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadSetMultisamplefvAMD();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SUBSAMPLE_DISTANCE_AMD = 0x883F;
        #endregion

        #region Commands
        internal delegate void glSetMultisamplefvAMDFunc(GLenum @pname, GLuint @index, const GLfloat * @val);
        internal static glSetMultisamplefvAMDFunc glSetMultisamplefvAMDPtr;
        internal static void loadSetMultisamplefvAMD()
        {
            try
            {
                glSetMultisamplefvAMDPtr = (glSetMultisamplefvAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSetMultisamplefvAMD"), typeof(glSetMultisamplefvAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSetMultisamplefvAMD'.");
            }
        }
        public static void glSetMultisamplefvAMD(GLenum @pname, GLuint @index, const GLfloat * @val) => glSetMultisamplefvAMDPtr(@pname, @index, @val);
        #endregion
    }
}
