using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_occlusion_query_event
    {
        #region Interop
        static GL_AMD_occlusion_query_event()
        {
            Console.WriteLine("Initalising GL_AMD_occlusion_query_event interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadQueryObjectParameteruiAMD();
        }
        #endregion

        #region Enums
        public static UInt32 GL_OCCLUSION_QUERY_EVENT_MASK_AMD = 0x874F;
        public static UInt32 GL_QUERY_DEPTH_PASS_EVENT_BIT_AMD = 0x00000001;
        public static UInt32 GL_QUERY_DEPTH_FAIL_EVENT_BIT_AMD = 0x00000002;
        public static UInt32 GL_QUERY_STENCIL_FAIL_EVENT_BIT_AMD = 0x00000004;
        public static UInt32 GL_QUERY_DEPTH_BOUNDS_FAIL_EVENT_BIT_AMD = 0x00000008;
        public static UInt32 GL_QUERY_ALL_EVENT_BITS_AMD = 0xFFFFFFFF;
        #endregion

        #region Commands
        internal delegate void glQueryObjectParameteruiAMDFunc(GLenum @target, GLuint @id, GLenum @pname, GLuint @param);
        internal static glQueryObjectParameteruiAMDFunc glQueryObjectParameteruiAMDPtr;
        internal static void loadQueryObjectParameteruiAMD()
        {
            try
            {
                glQueryObjectParameteruiAMDPtr = (glQueryObjectParameteruiAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glQueryObjectParameteruiAMD"), typeof(glQueryObjectParameteruiAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glQueryObjectParameteruiAMD'.");
            }
        }
        public static void glQueryObjectParameteruiAMD(GLenum @target, GLuint @id, GLenum @pname, GLuint @param) => glQueryObjectParameteruiAMDPtr(@target, @id, @pname, @param);
        #endregion
    }
}
