using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_debug_output
    {
        #region Interop
        static GL_AMD_debug_output()
        {
            Console.WriteLine("Initalising GL_AMD_debug_output interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDebugMessageEnableAMD();
            loadDebugMessageInsertAMD();
            loadDebugMessageCallbackAMD();
            loadGetDebugMessageLogAMD();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_DEBUG_MESSAGE_LENGTH_AMD = 0x9143;
        public static UInt32 GL_MAX_DEBUG_LOGGED_MESSAGES_AMD = 0x9144;
        public static UInt32 GL_DEBUG_LOGGED_MESSAGES_AMD = 0x9145;
        public static UInt32 GL_DEBUG_SEVERITY_HIGH_AMD = 0x9146;
        public static UInt32 GL_DEBUG_SEVERITY_MEDIUM_AMD = 0x9147;
        public static UInt32 GL_DEBUG_SEVERITY_LOW_AMD = 0x9148;
        public static UInt32 GL_DEBUG_CATEGORY_API_ERROR_AMD = 0x9149;
        public static UInt32 GL_DEBUG_CATEGORY_WINDOW_SYSTEM_AMD = 0x914A;
        public static UInt32 GL_DEBUG_CATEGORY_DEPRECATION_AMD = 0x914B;
        public static UInt32 GL_DEBUG_CATEGORY_UNDEFINED_BEHAVIOR_AMD = 0x914C;
        public static UInt32 GL_DEBUG_CATEGORY_PERFORMANCE_AMD = 0x914D;
        public static UInt32 GL_DEBUG_CATEGORY_SHADER_COMPILER_AMD = 0x914E;
        public static UInt32 GL_DEBUG_CATEGORY_APPLICATION_AMD = 0x914F;
        public static UInt32 GL_DEBUG_CATEGORY_OTHER_AMD = 0x9150;
        #endregion

        #region Commands
        internal delegate void glDebugMessageEnableAMDFunc(GLenum @category, GLenum @severity, GLsizei @count, const GLuint * @ids, GLboolean @enabled);
        internal static glDebugMessageEnableAMDFunc glDebugMessageEnableAMDPtr;
        internal static void loadDebugMessageEnableAMD()
        {
            try
            {
                glDebugMessageEnableAMDPtr = (glDebugMessageEnableAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageEnableAMD"), typeof(glDebugMessageEnableAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageEnableAMD'.");
            }
        }
        public static void glDebugMessageEnableAMD(GLenum @category, GLenum @severity, GLsizei @count, const GLuint * @ids, GLboolean @enabled) => glDebugMessageEnableAMDPtr(@category, @severity, @count, @ids, @enabled);

        internal delegate void glDebugMessageInsertAMDFunc(GLenum @category, GLenum @severity, GLuint @id, GLsizei @length, const GLchar * @buf);
        internal static glDebugMessageInsertAMDFunc glDebugMessageInsertAMDPtr;
        internal static void loadDebugMessageInsertAMD()
        {
            try
            {
                glDebugMessageInsertAMDPtr = (glDebugMessageInsertAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageInsertAMD"), typeof(glDebugMessageInsertAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageInsertAMD'.");
            }
        }
        public static void glDebugMessageInsertAMD(GLenum @category, GLenum @severity, GLuint @id, GLsizei @length, const GLchar * @buf) => glDebugMessageInsertAMDPtr(@category, @severity, @id, @length, @buf);

        internal delegate void glDebugMessageCallbackAMDFunc(GLDEBUGPROCAMD @callback, void * @userParam);
        internal static glDebugMessageCallbackAMDFunc glDebugMessageCallbackAMDPtr;
        internal static void loadDebugMessageCallbackAMD()
        {
            try
            {
                glDebugMessageCallbackAMDPtr = (glDebugMessageCallbackAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageCallbackAMD"), typeof(glDebugMessageCallbackAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageCallbackAMD'.");
            }
        }
        public static void glDebugMessageCallbackAMD(GLDEBUGPROCAMD @callback, void * @userParam) => glDebugMessageCallbackAMDPtr(@callback, @userParam);

        internal delegate GLuint glGetDebugMessageLogAMDFunc(GLuint @count, GLsizei @bufsize, GLenum * @categories, GLuint * @severities, GLuint * @ids, GLsizei * @lengths, GLchar * @message);
        internal static glGetDebugMessageLogAMDFunc glGetDebugMessageLogAMDPtr;
        internal static void loadGetDebugMessageLogAMD()
        {
            try
            {
                glGetDebugMessageLogAMDPtr = (glGetDebugMessageLogAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDebugMessageLogAMD"), typeof(glGetDebugMessageLogAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDebugMessageLogAMD'.");
            }
        }
        public static GLuint glGetDebugMessageLogAMD(GLuint @count, GLsizei @bufsize, GLenum * @categories, GLuint * @severities, GLuint * @ids, GLsizei * @lengths, GLchar * @message) => glGetDebugMessageLogAMDPtr(@count, @bufsize, @categories, @severities, @ids, @lengths, @message);
        #endregion
    }
}
