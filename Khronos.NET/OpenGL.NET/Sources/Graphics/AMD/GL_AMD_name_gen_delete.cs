using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_name_gen_delete
    {
        #region Interop
        static GL_AMD_name_gen_delete()
        {
            Console.WriteLine("Initalising GL_AMD_name_gen_delete interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGenNamesAMD();
            loadDeleteNamesAMD();
            loadIsNameAMD();
        }
        #endregion

        #region Enums
        public static UInt32 GL_DATA_BUFFER_AMD = 0x9151;
        public static UInt32 GL_PERFORMANCE_MONITOR_AMD = 0x9152;
        public static UInt32 GL_QUERY_OBJECT_AMD = 0x9153;
        public static UInt32 GL_VERTEX_ARRAY_OBJECT_AMD = 0x9154;
        public static UInt32 GL_SAMPLER_OBJECT_AMD = 0x9155;
        #endregion

        #region Commands
        internal delegate void glGenNamesAMDFunc(GLenum @identifier, GLuint @num, GLuint * @names);
        internal static glGenNamesAMDFunc glGenNamesAMDPtr;
        internal static void loadGenNamesAMD()
        {
            try
            {
                glGenNamesAMDPtr = (glGenNamesAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenNamesAMD"), typeof(glGenNamesAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenNamesAMD'.");
            }
        }
        public static void glGenNamesAMD(GLenum @identifier, GLuint @num, GLuint * @names) => glGenNamesAMDPtr(@identifier, @num, @names);

        internal delegate void glDeleteNamesAMDFunc(GLenum @identifier, GLuint @num, const GLuint * @names);
        internal static glDeleteNamesAMDFunc glDeleteNamesAMDPtr;
        internal static void loadDeleteNamesAMD()
        {
            try
            {
                glDeleteNamesAMDPtr = (glDeleteNamesAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteNamesAMD"), typeof(glDeleteNamesAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteNamesAMD'.");
            }
        }
        public static void glDeleteNamesAMD(GLenum @identifier, GLuint @num, const GLuint * @names) => glDeleteNamesAMDPtr(@identifier, @num, @names);

        internal delegate GLboolean glIsNameAMDFunc(GLenum @identifier, GLuint @name);
        internal static glIsNameAMDFunc glIsNameAMDPtr;
        internal static void loadIsNameAMD()
        {
            try
            {
                glIsNameAMDPtr = (glIsNameAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsNameAMD"), typeof(glIsNameAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsNameAMD'.");
            }
        }
        public static GLboolean glIsNameAMD(GLenum @identifier, GLuint @name) => glIsNameAMDPtr(@identifier, @name);
        #endregion
    }
}
