using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_depth_clamp_separate
    {
        #region Interop
        static GL_AMD_depth_clamp_separate()
        {
            Console.WriteLine("Initalising GL_AMD_depth_clamp_separate interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_CLAMP_NEAR_AMD = 0x901E;
        public static UInt32 GL_DEPTH_CLAMP_FAR_AMD = 0x901F;
        #endregion

        #region Commands
        #endregion
    }
}
