using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_draw_buffers_blend
    {
        #region Interop
        static GL_AMD_draw_buffers_blend()
        {
            Console.WriteLine("Initalising GL_AMD_draw_buffers_blend interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendFuncIndexedAMD();
            loadBlendFuncSeparateIndexedAMD();
            loadBlendEquationIndexedAMD();
            loadBlendEquationSeparateIndexedAMD();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glBlendFuncIndexedAMDFunc(GLuint @buf, GLenum @src, GLenum @dst);
        internal static glBlendFuncIndexedAMDFunc glBlendFuncIndexedAMDPtr;
        internal static void loadBlendFuncIndexedAMD()
        {
            try
            {
                glBlendFuncIndexedAMDPtr = (glBlendFuncIndexedAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFuncIndexedAMD"), typeof(glBlendFuncIndexedAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFuncIndexedAMD'.");
            }
        }
        public static void glBlendFuncIndexedAMD(GLuint @buf, GLenum @src, GLenum @dst) => glBlendFuncIndexedAMDPtr(@buf, @src, @dst);

        internal delegate void glBlendFuncSeparateIndexedAMDFunc(GLuint @buf, GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha);
        internal static glBlendFuncSeparateIndexedAMDFunc glBlendFuncSeparateIndexedAMDPtr;
        internal static void loadBlendFuncSeparateIndexedAMD()
        {
            try
            {
                glBlendFuncSeparateIndexedAMDPtr = (glBlendFuncSeparateIndexedAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFuncSeparateIndexedAMD"), typeof(glBlendFuncSeparateIndexedAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFuncSeparateIndexedAMD'.");
            }
        }
        public static void glBlendFuncSeparateIndexedAMD(GLuint @buf, GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha) => glBlendFuncSeparateIndexedAMDPtr(@buf, @srcRGB, @dstRGB, @srcAlpha, @dstAlpha);

        internal delegate void glBlendEquationIndexedAMDFunc(GLuint @buf, GLenum @mode);
        internal static glBlendEquationIndexedAMDFunc glBlendEquationIndexedAMDPtr;
        internal static void loadBlendEquationIndexedAMD()
        {
            try
            {
                glBlendEquationIndexedAMDPtr = (glBlendEquationIndexedAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationIndexedAMD"), typeof(glBlendEquationIndexedAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationIndexedAMD'.");
            }
        }
        public static void glBlendEquationIndexedAMD(GLuint @buf, GLenum @mode) => glBlendEquationIndexedAMDPtr(@buf, @mode);

        internal delegate void glBlendEquationSeparateIndexedAMDFunc(GLuint @buf, GLenum @modeRGB, GLenum @modeAlpha);
        internal static glBlendEquationSeparateIndexedAMDFunc glBlendEquationSeparateIndexedAMDPtr;
        internal static void loadBlendEquationSeparateIndexedAMD()
        {
            try
            {
                glBlendEquationSeparateIndexedAMDPtr = (glBlendEquationSeparateIndexedAMDFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationSeparateIndexedAMD"), typeof(glBlendEquationSeparateIndexedAMDFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationSeparateIndexedAMD'.");
            }
        }
        public static void glBlendEquationSeparateIndexedAMD(GLuint @buf, GLenum @modeRGB, GLenum @modeAlpha) => glBlendEquationSeparateIndexedAMDPtr(@buf, @modeRGB, @modeAlpha);
        #endregion
    }
}
