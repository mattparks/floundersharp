using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_AMD_blend_minmax_factor
    {
        #region Interop
        static GL_AMD_blend_minmax_factor()
        {
            Console.WriteLine("Initalising GL_AMD_blend_minmax_factor interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FACTOR_MIN_AMD = 0x901C;
        public static UInt32 GL_FACTOR_MAX_AMD = 0x901D;
        #endregion

        #region Commands
        #endregion
    }
}
