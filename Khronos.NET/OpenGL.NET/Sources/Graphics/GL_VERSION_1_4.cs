using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL14
    {
        #region Interop
        static GL14()
        {
            Console.WriteLine("Initalising GL14 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendFuncSeparate();
            loadMultiDrawArrays();
            loadMultiDrawElements();
            loadPointParameterf();
            loadPointParameterfv();
            loadPointParameteri();
            loadPointParameteriv();
            loadFogCoordf();
            loadFogCoordfv();
            loadFogCoordd();
            loadFogCoorddv();
            loadFogCoordPointer();
            loadSecondaryColor3b();
            loadSecondaryColor3bv();
            loadSecondaryColor3d();
            loadSecondaryColor3dv();
            loadSecondaryColor3f();
            loadSecondaryColor3fv();
            loadSecondaryColor3i();
            loadSecondaryColor3iv();
            loadSecondaryColor3s();
            loadSecondaryColor3sv();
            loadSecondaryColor3ub();
            loadSecondaryColor3ubv();
            loadSecondaryColor3ui();
            loadSecondaryColor3uiv();
            loadSecondaryColor3us();
            loadSecondaryColor3usv();
            loadSecondaryColorPointer();
            loadWindowPos2d();
            loadWindowPos2dv();
            loadWindowPos2f();
            loadWindowPos2fv();
            loadWindowPos2i();
            loadWindowPos2iv();
            loadWindowPos2s();
            loadWindowPos2sv();
            loadWindowPos3d();
            loadWindowPos3dv();
            loadWindowPos3f();
            loadWindowPos3fv();
            loadWindowPos3i();
            loadWindowPos3iv();
            loadWindowPos3s();
            loadWindowPos3sv();
            loadBlendColor();
            loadBlendEquation();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BLEND_DST_RGB = 0x80C8;
        public static UInt32 GL_BLEND_SRC_RGB = 0x80C9;
        public static UInt32 GL_BLEND_DST_ALPHA = 0x80CA;
        public static UInt32 GL_BLEND_SRC_ALPHA = 0x80CB;
        public static UInt32 GL_POINT_FADE_THRESHOLD_SIZE = 0x8128;
        public static UInt32 GL_DEPTH_COMPONENT16 = 0x81A5;
        public static UInt32 GL_DEPTH_COMPONENT24 = 0x81A6;
        public static UInt32 GL_DEPTH_COMPONENT32 = 0x81A7;
        public static UInt32 GL_MIRRORED_REPEAT = 0x8370;
        public static UInt32 GL_MAX_TEXTURE_LOD_BIAS = 0x84FD;
        public static UInt32 GL_TEXTURE_LOD_BIAS = 0x8501;
        public static UInt32 GL_INCR_WRAP = 0x8507;
        public static UInt32 GL_DECR_WRAP = 0x8508;
        public static UInt32 GL_TEXTURE_DEPTH_SIZE = 0x884A;
        public static UInt32 GL_TEXTURE_COMPARE_MODE = 0x884C;
        public static UInt32 GL_TEXTURE_COMPARE_FUNC = 0x884D;
        public static UInt32 GL_POINT_SIZE_MIN = 0x8126;
        public static UInt32 GL_POINT_SIZE_MAX = 0x8127;
        public static UInt32 GL_POINT_DISTANCE_ATTENUATION = 0x8129;
        public static UInt32 GL_GENERATE_MIPMAP = 0x8191;
        public static UInt32 GL_GENERATE_MIPMAP_HINT = 0x8192;
        public static UInt32 GL_FOG_COORDINATE_SOURCE = 0x8450;
        public static UInt32 GL_FOG_COORDINATE = 0x8451;
        public static UInt32 GL_FRAGMENT_DEPTH = 0x8452;
        public static UInt32 GL_CURRENT_FOG_COORDINATE = 0x8453;
        public static UInt32 GL_FOG_COORDINATE_ARRAY_TYPE = 0x8454;
        public static UInt32 GL_FOG_COORDINATE_ARRAY_STRIDE = 0x8455;
        public static UInt32 GL_FOG_COORDINATE_ARRAY_POINTER = 0x8456;
        public static UInt32 GL_FOG_COORDINATE_ARRAY = 0x8457;
        public static UInt32 GL_COLOR_SUM = 0x8458;
        public static UInt32 GL_CURRENT_SECONDARY_COLOR = 0x8459;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_SIZE = 0x845A;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_TYPE = 0x845B;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_STRIDE = 0x845C;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_POINTER = 0x845D;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY = 0x845E;
        public static UInt32 GL_TEXTURE_FILTER_CONTROL = 0x8500;
        public static UInt32 GL_DEPTH_TEXTURE_MODE = 0x884B;
        public static UInt32 GL_COMPARE_R_TO_TEXTURE = 0x884E;
        public static UInt32 GL_FUNC_ADD = 0x8006;
        public static UInt32 GL_FUNC_SUBTRACT = 0x800A;
        public static UInt32 GL_FUNC_REVERSE_SUBTRACT = 0x800B;
        public static UInt32 GL_MIN = 0x8007;
        public static UInt32 GL_MAX = 0x8008;
        public static UInt32 GL_CONSTANT_COLOR = 0x8001;
        public static UInt32 GL_ONE_MINUS_CONSTANT_COLOR = 0x8002;
        public static UInt32 GL_CONSTANT_ALPHA = 0x8003;
        public static UInt32 GL_ONE_MINUS_CONSTANT_ALPHA = 0x8004;
        #endregion

        #region Commands
        internal delegate void glBlendFuncSeparateFunc(GLenum @sfactorRGB, GLenum @dfactorRGB, GLenum @sfactorAlpha, GLenum @dfactorAlpha);
        internal static glBlendFuncSeparateFunc glBlendFuncSeparatePtr;
        internal static void loadBlendFuncSeparate()
        {
            try
            {
                glBlendFuncSeparatePtr = (glBlendFuncSeparateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFuncSeparate"), typeof(glBlendFuncSeparateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFuncSeparate'.");
            }
        }
        public static void glBlendFuncSeparate(GLenum @sfactorRGB, GLenum @dfactorRGB, GLenum @sfactorAlpha, GLenum @dfactorAlpha) => glBlendFuncSeparatePtr(@sfactorRGB, @dfactorRGB, @sfactorAlpha, @dfactorAlpha);

        internal delegate void glMultiDrawArraysFunc(GLenum @mode, const GLint * @first, const GLsizei * @count, GLsizei @drawcount);
        internal static glMultiDrawArraysFunc glMultiDrawArraysPtr;
        internal static void loadMultiDrawArrays()
        {
            try
            {
                glMultiDrawArraysPtr = (glMultiDrawArraysFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawArrays"), typeof(glMultiDrawArraysFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawArrays'.");
            }
        }
        public static void glMultiDrawArrays(GLenum @mode, const GLint * @first, const GLsizei * @count, GLsizei @drawcount) => glMultiDrawArraysPtr(@mode, @first, @count, @drawcount);

        internal delegate void glMultiDrawElementsFunc(GLenum @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @drawcount);
        internal static glMultiDrawElementsFunc glMultiDrawElementsPtr;
        internal static void loadMultiDrawElements()
        {
            try
            {
                glMultiDrawElementsPtr = (glMultiDrawElementsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElements"), typeof(glMultiDrawElementsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElements'.");
            }
        }
        public static void glMultiDrawElements(GLenum @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @drawcount) => glMultiDrawElementsPtr(@mode, @count, @type, @indices, @drawcount);

        internal delegate void glPointParameterfFunc(GLenum @pname, GLfloat @param);
        internal static glPointParameterfFunc glPointParameterfPtr;
        internal static void loadPointParameterf()
        {
            try
            {
                glPointParameterfPtr = (glPointParameterfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterf"), typeof(glPointParameterfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterf'.");
            }
        }
        public static void glPointParameterf(GLenum @pname, GLfloat @param) => glPointParameterfPtr(@pname, @param);

        internal delegate void glPointParameterfvFunc(GLenum @pname, const GLfloat * @params);
        internal static glPointParameterfvFunc glPointParameterfvPtr;
        internal static void loadPointParameterfv()
        {
            try
            {
                glPointParameterfvPtr = (glPointParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterfv"), typeof(glPointParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterfv'.");
            }
        }
        public static void glPointParameterfv(GLenum @pname, const GLfloat * @params) => glPointParameterfvPtr(@pname, @params);

        internal delegate void glPointParameteriFunc(GLenum @pname, GLint @param);
        internal static glPointParameteriFunc glPointParameteriPtr;
        internal static void loadPointParameteri()
        {
            try
            {
                glPointParameteriPtr = (glPointParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameteri"), typeof(glPointParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameteri'.");
            }
        }
        public static void glPointParameteri(GLenum @pname, GLint @param) => glPointParameteriPtr(@pname, @param);

        internal delegate void glPointParameterivFunc(GLenum @pname, const GLint * @params);
        internal static glPointParameterivFunc glPointParameterivPtr;
        internal static void loadPointParameteriv()
        {
            try
            {
                glPointParameterivPtr = (glPointParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameteriv"), typeof(glPointParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameteriv'.");
            }
        }
        public static void glPointParameteriv(GLenum @pname, const GLint * @params) => glPointParameterivPtr(@pname, @params);

        internal delegate void glFogCoordfFunc(GLfloat @coord);
        internal static glFogCoordfFunc glFogCoordfPtr;
        internal static void loadFogCoordf()
        {
            try
            {
                glFogCoordfPtr = (glFogCoordfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoordf"), typeof(glFogCoordfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoordf'.");
            }
        }
        public static void glFogCoordf(GLfloat @coord) => glFogCoordfPtr(@coord);

        internal delegate void glFogCoordfvFunc(const GLfloat * @coord);
        internal static glFogCoordfvFunc glFogCoordfvPtr;
        internal static void loadFogCoordfv()
        {
            try
            {
                glFogCoordfvPtr = (glFogCoordfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoordfv"), typeof(glFogCoordfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoordfv'.");
            }
        }
        public static void glFogCoordfv(const GLfloat * @coord) => glFogCoordfvPtr(@coord);

        internal delegate void glFogCoorddFunc(GLdouble @coord);
        internal static glFogCoorddFunc glFogCoorddPtr;
        internal static void loadFogCoordd()
        {
            try
            {
                glFogCoorddPtr = (glFogCoorddFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoordd"), typeof(glFogCoorddFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoordd'.");
            }
        }
        public static void glFogCoordd(GLdouble @coord) => glFogCoorddPtr(@coord);

        internal delegate void glFogCoorddvFunc(const GLdouble * @coord);
        internal static glFogCoorddvFunc glFogCoorddvPtr;
        internal static void loadFogCoorddv()
        {
            try
            {
                glFogCoorddvPtr = (glFogCoorddvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoorddv"), typeof(glFogCoorddvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoorddv'.");
            }
        }
        public static void glFogCoorddv(const GLdouble * @coord) => glFogCoorddvPtr(@coord);

        internal delegate void glFogCoordPointerFunc(GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glFogCoordPointerFunc glFogCoordPointerPtr;
        internal static void loadFogCoordPointer()
        {
            try
            {
                glFogCoordPointerPtr = (glFogCoordPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogCoordPointer"), typeof(glFogCoordPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogCoordPointer'.");
            }
        }
        public static void glFogCoordPointer(GLenum @type, GLsizei @stride, const void * @pointer) => glFogCoordPointerPtr(@type, @stride, @pointer);

        internal delegate void glSecondaryColor3bFunc(GLbyte @red, GLbyte @green, GLbyte @blue);
        internal static glSecondaryColor3bFunc glSecondaryColor3bPtr;
        internal static void loadSecondaryColor3b()
        {
            try
            {
                glSecondaryColor3bPtr = (glSecondaryColor3bFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3b"), typeof(glSecondaryColor3bFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3b'.");
            }
        }
        public static void glSecondaryColor3b(GLbyte @red, GLbyte @green, GLbyte @blue) => glSecondaryColor3bPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3bvFunc(const GLbyte * @v);
        internal static glSecondaryColor3bvFunc glSecondaryColor3bvPtr;
        internal static void loadSecondaryColor3bv()
        {
            try
            {
                glSecondaryColor3bvPtr = (glSecondaryColor3bvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3bv"), typeof(glSecondaryColor3bvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3bv'.");
            }
        }
        public static void glSecondaryColor3bv(const GLbyte * @v) => glSecondaryColor3bvPtr(@v);

        internal delegate void glSecondaryColor3dFunc(GLdouble @red, GLdouble @green, GLdouble @blue);
        internal static glSecondaryColor3dFunc glSecondaryColor3dPtr;
        internal static void loadSecondaryColor3d()
        {
            try
            {
                glSecondaryColor3dPtr = (glSecondaryColor3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3d"), typeof(glSecondaryColor3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3d'.");
            }
        }
        public static void glSecondaryColor3d(GLdouble @red, GLdouble @green, GLdouble @blue) => glSecondaryColor3dPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3dvFunc(const GLdouble * @v);
        internal static glSecondaryColor3dvFunc glSecondaryColor3dvPtr;
        internal static void loadSecondaryColor3dv()
        {
            try
            {
                glSecondaryColor3dvPtr = (glSecondaryColor3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3dv"), typeof(glSecondaryColor3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3dv'.");
            }
        }
        public static void glSecondaryColor3dv(const GLdouble * @v) => glSecondaryColor3dvPtr(@v);

        internal delegate void glSecondaryColor3fFunc(GLfloat @red, GLfloat @green, GLfloat @blue);
        internal static glSecondaryColor3fFunc glSecondaryColor3fPtr;
        internal static void loadSecondaryColor3f()
        {
            try
            {
                glSecondaryColor3fPtr = (glSecondaryColor3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3f"), typeof(glSecondaryColor3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3f'.");
            }
        }
        public static void glSecondaryColor3f(GLfloat @red, GLfloat @green, GLfloat @blue) => glSecondaryColor3fPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3fvFunc(const GLfloat * @v);
        internal static glSecondaryColor3fvFunc glSecondaryColor3fvPtr;
        internal static void loadSecondaryColor3fv()
        {
            try
            {
                glSecondaryColor3fvPtr = (glSecondaryColor3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3fv"), typeof(glSecondaryColor3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3fv'.");
            }
        }
        public static void glSecondaryColor3fv(const GLfloat * @v) => glSecondaryColor3fvPtr(@v);

        internal delegate void glSecondaryColor3iFunc(GLint @red, GLint @green, GLint @blue);
        internal static glSecondaryColor3iFunc glSecondaryColor3iPtr;
        internal static void loadSecondaryColor3i()
        {
            try
            {
                glSecondaryColor3iPtr = (glSecondaryColor3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3i"), typeof(glSecondaryColor3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3i'.");
            }
        }
        public static void glSecondaryColor3i(GLint @red, GLint @green, GLint @blue) => glSecondaryColor3iPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3ivFunc(const GLint * @v);
        internal static glSecondaryColor3ivFunc glSecondaryColor3ivPtr;
        internal static void loadSecondaryColor3iv()
        {
            try
            {
                glSecondaryColor3ivPtr = (glSecondaryColor3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3iv"), typeof(glSecondaryColor3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3iv'.");
            }
        }
        public static void glSecondaryColor3iv(const GLint * @v) => glSecondaryColor3ivPtr(@v);

        internal delegate void glSecondaryColor3sFunc(GLshort @red, GLshort @green, GLshort @blue);
        internal static glSecondaryColor3sFunc glSecondaryColor3sPtr;
        internal static void loadSecondaryColor3s()
        {
            try
            {
                glSecondaryColor3sPtr = (glSecondaryColor3sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3s"), typeof(glSecondaryColor3sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3s'.");
            }
        }
        public static void glSecondaryColor3s(GLshort @red, GLshort @green, GLshort @blue) => glSecondaryColor3sPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3svFunc(const GLshort * @v);
        internal static glSecondaryColor3svFunc glSecondaryColor3svPtr;
        internal static void loadSecondaryColor3sv()
        {
            try
            {
                glSecondaryColor3svPtr = (glSecondaryColor3svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3sv"), typeof(glSecondaryColor3svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3sv'.");
            }
        }
        public static void glSecondaryColor3sv(const GLshort * @v) => glSecondaryColor3svPtr(@v);

        internal delegate void glSecondaryColor3ubFunc(GLubyte @red, GLubyte @green, GLubyte @blue);
        internal static glSecondaryColor3ubFunc glSecondaryColor3ubPtr;
        internal static void loadSecondaryColor3ub()
        {
            try
            {
                glSecondaryColor3ubPtr = (glSecondaryColor3ubFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3ub"), typeof(glSecondaryColor3ubFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3ub'.");
            }
        }
        public static void glSecondaryColor3ub(GLubyte @red, GLubyte @green, GLubyte @blue) => glSecondaryColor3ubPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3ubvFunc(const GLubyte * @v);
        internal static glSecondaryColor3ubvFunc glSecondaryColor3ubvPtr;
        internal static void loadSecondaryColor3ubv()
        {
            try
            {
                glSecondaryColor3ubvPtr = (glSecondaryColor3ubvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3ubv"), typeof(glSecondaryColor3ubvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3ubv'.");
            }
        }
        public static void glSecondaryColor3ubv(const GLubyte * @v) => glSecondaryColor3ubvPtr(@v);

        internal delegate void glSecondaryColor3uiFunc(GLuint @red, GLuint @green, GLuint @blue);
        internal static glSecondaryColor3uiFunc glSecondaryColor3uiPtr;
        internal static void loadSecondaryColor3ui()
        {
            try
            {
                glSecondaryColor3uiPtr = (glSecondaryColor3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3ui"), typeof(glSecondaryColor3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3ui'.");
            }
        }
        public static void glSecondaryColor3ui(GLuint @red, GLuint @green, GLuint @blue) => glSecondaryColor3uiPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3uivFunc(const GLuint * @v);
        internal static glSecondaryColor3uivFunc glSecondaryColor3uivPtr;
        internal static void loadSecondaryColor3uiv()
        {
            try
            {
                glSecondaryColor3uivPtr = (glSecondaryColor3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3uiv"), typeof(glSecondaryColor3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3uiv'.");
            }
        }
        public static void glSecondaryColor3uiv(const GLuint * @v) => glSecondaryColor3uivPtr(@v);

        internal delegate void glSecondaryColor3usFunc(GLushort @red, GLushort @green, GLushort @blue);
        internal static glSecondaryColor3usFunc glSecondaryColor3usPtr;
        internal static void loadSecondaryColor3us()
        {
            try
            {
                glSecondaryColor3usPtr = (glSecondaryColor3usFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3us"), typeof(glSecondaryColor3usFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3us'.");
            }
        }
        public static void glSecondaryColor3us(GLushort @red, GLushort @green, GLushort @blue) => glSecondaryColor3usPtr(@red, @green, @blue);

        internal delegate void glSecondaryColor3usvFunc(const GLushort * @v);
        internal static glSecondaryColor3usvFunc glSecondaryColor3usvPtr;
        internal static void loadSecondaryColor3usv()
        {
            try
            {
                glSecondaryColor3usvPtr = (glSecondaryColor3usvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColor3usv"), typeof(glSecondaryColor3usvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColor3usv'.");
            }
        }
        public static void glSecondaryColor3usv(const GLushort * @v) => glSecondaryColor3usvPtr(@v);

        internal delegate void glSecondaryColorPointerFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glSecondaryColorPointerFunc glSecondaryColorPointerPtr;
        internal static void loadSecondaryColorPointer()
        {
            try
            {
                glSecondaryColorPointerPtr = (glSecondaryColorPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColorPointer"), typeof(glSecondaryColorPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColorPointer'.");
            }
        }
        public static void glSecondaryColorPointer(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glSecondaryColorPointerPtr(@size, @type, @stride, @pointer);

        internal delegate void glWindowPos2dFunc(GLdouble @x, GLdouble @y);
        internal static glWindowPos2dFunc glWindowPos2dPtr;
        internal static void loadWindowPos2d()
        {
            try
            {
                glWindowPos2dPtr = (glWindowPos2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2d"), typeof(glWindowPos2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2d'.");
            }
        }
        public static void glWindowPos2d(GLdouble @x, GLdouble @y) => glWindowPos2dPtr(@x, @y);

        internal delegate void glWindowPos2dvFunc(const GLdouble * @v);
        internal static glWindowPos2dvFunc glWindowPos2dvPtr;
        internal static void loadWindowPos2dv()
        {
            try
            {
                glWindowPos2dvPtr = (glWindowPos2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2dv"), typeof(glWindowPos2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2dv'.");
            }
        }
        public static void glWindowPos2dv(const GLdouble * @v) => glWindowPos2dvPtr(@v);

        internal delegate void glWindowPos2fFunc(GLfloat @x, GLfloat @y);
        internal static glWindowPos2fFunc glWindowPos2fPtr;
        internal static void loadWindowPos2f()
        {
            try
            {
                glWindowPos2fPtr = (glWindowPos2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2f"), typeof(glWindowPos2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2f'.");
            }
        }
        public static void glWindowPos2f(GLfloat @x, GLfloat @y) => glWindowPos2fPtr(@x, @y);

        internal delegate void glWindowPos2fvFunc(const GLfloat * @v);
        internal static glWindowPos2fvFunc glWindowPos2fvPtr;
        internal static void loadWindowPos2fv()
        {
            try
            {
                glWindowPos2fvPtr = (glWindowPos2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2fv"), typeof(glWindowPos2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2fv'.");
            }
        }
        public static void glWindowPos2fv(const GLfloat * @v) => glWindowPos2fvPtr(@v);

        internal delegate void glWindowPos2iFunc(GLint @x, GLint @y);
        internal static glWindowPos2iFunc glWindowPos2iPtr;
        internal static void loadWindowPos2i()
        {
            try
            {
                glWindowPos2iPtr = (glWindowPos2iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2i"), typeof(glWindowPos2iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2i'.");
            }
        }
        public static void glWindowPos2i(GLint @x, GLint @y) => glWindowPos2iPtr(@x, @y);

        internal delegate void glWindowPos2ivFunc(const GLint * @v);
        internal static glWindowPos2ivFunc glWindowPos2ivPtr;
        internal static void loadWindowPos2iv()
        {
            try
            {
                glWindowPos2ivPtr = (glWindowPos2ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2iv"), typeof(glWindowPos2ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2iv'.");
            }
        }
        public static void glWindowPos2iv(const GLint * @v) => glWindowPos2ivPtr(@v);

        internal delegate void glWindowPos2sFunc(GLshort @x, GLshort @y);
        internal static glWindowPos2sFunc glWindowPos2sPtr;
        internal static void loadWindowPos2s()
        {
            try
            {
                glWindowPos2sPtr = (glWindowPos2sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2s"), typeof(glWindowPos2sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2s'.");
            }
        }
        public static void glWindowPos2s(GLshort @x, GLshort @y) => glWindowPos2sPtr(@x, @y);

        internal delegate void glWindowPos2svFunc(const GLshort * @v);
        internal static glWindowPos2svFunc glWindowPos2svPtr;
        internal static void loadWindowPos2sv()
        {
            try
            {
                glWindowPos2svPtr = (glWindowPos2svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2sv"), typeof(glWindowPos2svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2sv'.");
            }
        }
        public static void glWindowPos2sv(const GLshort * @v) => glWindowPos2svPtr(@v);

        internal delegate void glWindowPos3dFunc(GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glWindowPos3dFunc glWindowPos3dPtr;
        internal static void loadWindowPos3d()
        {
            try
            {
                glWindowPos3dPtr = (glWindowPos3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3d"), typeof(glWindowPos3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3d'.");
            }
        }
        public static void glWindowPos3d(GLdouble @x, GLdouble @y, GLdouble @z) => glWindowPos3dPtr(@x, @y, @z);

        internal delegate void glWindowPos3dvFunc(const GLdouble * @v);
        internal static glWindowPos3dvFunc glWindowPos3dvPtr;
        internal static void loadWindowPos3dv()
        {
            try
            {
                glWindowPos3dvPtr = (glWindowPos3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3dv"), typeof(glWindowPos3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3dv'.");
            }
        }
        public static void glWindowPos3dv(const GLdouble * @v) => glWindowPos3dvPtr(@v);

        internal delegate void glWindowPos3fFunc(GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glWindowPos3fFunc glWindowPos3fPtr;
        internal static void loadWindowPos3f()
        {
            try
            {
                glWindowPos3fPtr = (glWindowPos3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3f"), typeof(glWindowPos3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3f'.");
            }
        }
        public static void glWindowPos3f(GLfloat @x, GLfloat @y, GLfloat @z) => glWindowPos3fPtr(@x, @y, @z);

        internal delegate void glWindowPos3fvFunc(const GLfloat * @v);
        internal static glWindowPos3fvFunc glWindowPos3fvPtr;
        internal static void loadWindowPos3fv()
        {
            try
            {
                glWindowPos3fvPtr = (glWindowPos3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3fv"), typeof(glWindowPos3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3fv'.");
            }
        }
        public static void glWindowPos3fv(const GLfloat * @v) => glWindowPos3fvPtr(@v);

        internal delegate void glWindowPos3iFunc(GLint @x, GLint @y, GLint @z);
        internal static glWindowPos3iFunc glWindowPos3iPtr;
        internal static void loadWindowPos3i()
        {
            try
            {
                glWindowPos3iPtr = (glWindowPos3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3i"), typeof(glWindowPos3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3i'.");
            }
        }
        public static void glWindowPos3i(GLint @x, GLint @y, GLint @z) => glWindowPos3iPtr(@x, @y, @z);

        internal delegate void glWindowPos3ivFunc(const GLint * @v);
        internal static glWindowPos3ivFunc glWindowPos3ivPtr;
        internal static void loadWindowPos3iv()
        {
            try
            {
                glWindowPos3ivPtr = (glWindowPos3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3iv"), typeof(glWindowPos3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3iv'.");
            }
        }
        public static void glWindowPos3iv(const GLint * @v) => glWindowPos3ivPtr(@v);

        internal delegate void glWindowPos3sFunc(GLshort @x, GLshort @y, GLshort @z);
        internal static glWindowPos3sFunc glWindowPos3sPtr;
        internal static void loadWindowPos3s()
        {
            try
            {
                glWindowPos3sPtr = (glWindowPos3sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3s"), typeof(glWindowPos3sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3s'.");
            }
        }
        public static void glWindowPos3s(GLshort @x, GLshort @y, GLshort @z) => glWindowPos3sPtr(@x, @y, @z);

        internal delegate void glWindowPos3svFunc(const GLshort * @v);
        internal static glWindowPos3svFunc glWindowPos3svPtr;
        internal static void loadWindowPos3sv()
        {
            try
            {
                glWindowPos3svPtr = (glWindowPos3svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3sv"), typeof(glWindowPos3svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3sv'.");
            }
        }
        public static void glWindowPos3sv(const GLshort * @v) => glWindowPos3svPtr(@v);

        internal delegate void glBlendColorFunc(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha);
        internal static glBlendColorFunc glBlendColorPtr;
        internal static void loadBlendColor()
        {
            try
            {
                glBlendColorPtr = (glBlendColorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendColor"), typeof(glBlendColorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendColor'.");
            }
        }
        public static void glBlendColor(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha) => glBlendColorPtr(@red, @green, @blue, @alpha);

        internal delegate void glBlendEquationFunc(GLenum @mode);
        internal static glBlendEquationFunc glBlendEquationPtr;
        internal static void loadBlendEquation()
        {
            try
            {
                glBlendEquationPtr = (glBlendEquationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquation"), typeof(glBlendEquationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquation'.");
            }
        }
        public static void glBlendEquation(GLenum @mode) => glBlendEquationPtr(@mode);
        #endregion
    }
}
