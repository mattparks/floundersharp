using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_QCOM_extended_get
    {
        #region Interop
        static GL_QCOM_extended_get()
        {
            Console.WriteLine("Initalising GL_QCOM_extended_get interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadExtGetTexturesQCOM();
            loadExtGetBuffersQCOM();
            loadExtGetRenderbuffersQCOM();
            loadExtGetFramebuffersQCOM();
            loadExtGetTexLevelParameterivQCOM();
            loadExtTexObjectStateOverrideiQCOM();
            loadExtGetTexSubImageQCOM();
            loadExtGetBufferPointervQCOM();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_WIDTH_QCOM = 0x8BD2;
        public static UInt32 GL_TEXTURE_HEIGHT_QCOM = 0x8BD3;
        public static UInt32 GL_TEXTURE_DEPTH_QCOM = 0x8BD4;
        public static UInt32 GL_TEXTURE_INTERNAL_FORMAT_QCOM = 0x8BD5;
        public static UInt32 GL_TEXTURE_FORMAT_QCOM = 0x8BD6;
        public static UInt32 GL_TEXTURE_TYPE_QCOM = 0x8BD7;
        public static UInt32 GL_TEXTURE_IMAGE_VALID_QCOM = 0x8BD8;
        public static UInt32 GL_TEXTURE_NUM_LEVELS_QCOM = 0x8BD9;
        public static UInt32 GL_TEXTURE_TARGET_QCOM = 0x8BDA;
        public static UInt32 GL_TEXTURE_OBJECT_VALID_QCOM = 0x8BDB;
        public static UInt32 GL_STATE_RESTORE = 0x8BDC;
        #endregion

        #region Commands
        internal delegate void glExtGetTexturesQCOMFunc(GLuint * @textures, GLint @maxTextures, GLint * @numTextures);
        internal static glExtGetTexturesQCOMFunc glExtGetTexturesQCOMPtr;
        internal static void loadExtGetTexturesQCOM()
        {
            try
            {
                glExtGetTexturesQCOMPtr = (glExtGetTexturesQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtGetTexturesQCOM"), typeof(glExtGetTexturesQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtGetTexturesQCOM'.");
            }
        }
        public static void glExtGetTexturesQCOM(GLuint * @textures, GLint @maxTextures, GLint * @numTextures) => glExtGetTexturesQCOMPtr(@textures, @maxTextures, @numTextures);

        internal delegate void glExtGetBuffersQCOMFunc(GLuint * @buffers, GLint @maxBuffers, GLint * @numBuffers);
        internal static glExtGetBuffersQCOMFunc glExtGetBuffersQCOMPtr;
        internal static void loadExtGetBuffersQCOM()
        {
            try
            {
                glExtGetBuffersQCOMPtr = (glExtGetBuffersQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtGetBuffersQCOM"), typeof(glExtGetBuffersQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtGetBuffersQCOM'.");
            }
        }
        public static void glExtGetBuffersQCOM(GLuint * @buffers, GLint @maxBuffers, GLint * @numBuffers) => glExtGetBuffersQCOMPtr(@buffers, @maxBuffers, @numBuffers);

        internal delegate void glExtGetRenderbuffersQCOMFunc(GLuint * @renderbuffers, GLint @maxRenderbuffers, GLint * @numRenderbuffers);
        internal static glExtGetRenderbuffersQCOMFunc glExtGetRenderbuffersQCOMPtr;
        internal static void loadExtGetRenderbuffersQCOM()
        {
            try
            {
                glExtGetRenderbuffersQCOMPtr = (glExtGetRenderbuffersQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtGetRenderbuffersQCOM"), typeof(glExtGetRenderbuffersQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtGetRenderbuffersQCOM'.");
            }
        }
        public static void glExtGetRenderbuffersQCOM(GLuint * @renderbuffers, GLint @maxRenderbuffers, GLint * @numRenderbuffers) => glExtGetRenderbuffersQCOMPtr(@renderbuffers, @maxRenderbuffers, @numRenderbuffers);

        internal delegate void glExtGetFramebuffersQCOMFunc(GLuint * @framebuffers, GLint @maxFramebuffers, GLint * @numFramebuffers);
        internal static glExtGetFramebuffersQCOMFunc glExtGetFramebuffersQCOMPtr;
        internal static void loadExtGetFramebuffersQCOM()
        {
            try
            {
                glExtGetFramebuffersQCOMPtr = (glExtGetFramebuffersQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtGetFramebuffersQCOM"), typeof(glExtGetFramebuffersQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtGetFramebuffersQCOM'.");
            }
        }
        public static void glExtGetFramebuffersQCOM(GLuint * @framebuffers, GLint @maxFramebuffers, GLint * @numFramebuffers) => glExtGetFramebuffersQCOMPtr(@framebuffers, @maxFramebuffers, @numFramebuffers);

        internal delegate void glExtGetTexLevelParameterivQCOMFunc(GLuint @texture, GLenum @face, GLint @level, GLenum @pname, GLint * @params);
        internal static glExtGetTexLevelParameterivQCOMFunc glExtGetTexLevelParameterivQCOMPtr;
        internal static void loadExtGetTexLevelParameterivQCOM()
        {
            try
            {
                glExtGetTexLevelParameterivQCOMPtr = (glExtGetTexLevelParameterivQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtGetTexLevelParameterivQCOM"), typeof(glExtGetTexLevelParameterivQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtGetTexLevelParameterivQCOM'.");
            }
        }
        public static void glExtGetTexLevelParameterivQCOM(GLuint @texture, GLenum @face, GLint @level, GLenum @pname, GLint * @params) => glExtGetTexLevelParameterivQCOMPtr(@texture, @face, @level, @pname, @params);

        internal delegate void glExtTexObjectStateOverrideiQCOMFunc(GLenum @target, GLenum @pname, GLint @param);
        internal static glExtTexObjectStateOverrideiQCOMFunc glExtTexObjectStateOverrideiQCOMPtr;
        internal static void loadExtTexObjectStateOverrideiQCOM()
        {
            try
            {
                glExtTexObjectStateOverrideiQCOMPtr = (glExtTexObjectStateOverrideiQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtTexObjectStateOverrideiQCOM"), typeof(glExtTexObjectStateOverrideiQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtTexObjectStateOverrideiQCOM'.");
            }
        }
        public static void glExtTexObjectStateOverrideiQCOM(GLenum @target, GLenum @pname, GLint @param) => glExtTexObjectStateOverrideiQCOMPtr(@target, @pname, @param);

        internal delegate void glExtGetTexSubImageQCOMFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, void * @texels);
        internal static glExtGetTexSubImageQCOMFunc glExtGetTexSubImageQCOMPtr;
        internal static void loadExtGetTexSubImageQCOM()
        {
            try
            {
                glExtGetTexSubImageQCOMPtr = (glExtGetTexSubImageQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtGetTexSubImageQCOM"), typeof(glExtGetTexSubImageQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtGetTexSubImageQCOM'.");
            }
        }
        public static void glExtGetTexSubImageQCOM(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, void * @texels) => glExtGetTexSubImageQCOMPtr(@target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @type, @texels);

        internal delegate void glExtGetBufferPointervQCOMFunc(GLenum @target, void ** @params);
        internal static glExtGetBufferPointervQCOMFunc glExtGetBufferPointervQCOMPtr;
        internal static void loadExtGetBufferPointervQCOM()
        {
            try
            {
                glExtGetBufferPointervQCOMPtr = (glExtGetBufferPointervQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtGetBufferPointervQCOM"), typeof(glExtGetBufferPointervQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtGetBufferPointervQCOM'.");
            }
        }
        public static void glExtGetBufferPointervQCOM(GLenum @target, void ** @params) => glExtGetBufferPointervQCOMPtr(@target, @params);
        #endregion
    }
}
