using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_QCOM_extended_get2
    {
        #region Interop
        static GL_QCOM_extended_get2()
        {
            Console.WriteLine("Initalising GL_QCOM_extended_get2 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadExtGetShadersQCOM();
            loadExtGetProgramsQCOM();
            loadExtIsProgramBinaryQCOM();
            loadExtGetProgramBinarySourceQCOM();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glExtGetShadersQCOMFunc(GLuint * @shaders, GLint @maxShaders, GLint * @numShaders);
        internal static glExtGetShadersQCOMFunc glExtGetShadersQCOMPtr;
        internal static void loadExtGetShadersQCOM()
        {
            try
            {
                glExtGetShadersQCOMPtr = (glExtGetShadersQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtGetShadersQCOM"), typeof(glExtGetShadersQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtGetShadersQCOM'.");
            }
        }
        public static void glExtGetShadersQCOM(GLuint * @shaders, GLint @maxShaders, GLint * @numShaders) => glExtGetShadersQCOMPtr(@shaders, @maxShaders, @numShaders);

        internal delegate void glExtGetProgramsQCOMFunc(GLuint * @programs, GLint @maxPrograms, GLint * @numPrograms);
        internal static glExtGetProgramsQCOMFunc glExtGetProgramsQCOMPtr;
        internal static void loadExtGetProgramsQCOM()
        {
            try
            {
                glExtGetProgramsQCOMPtr = (glExtGetProgramsQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtGetProgramsQCOM"), typeof(glExtGetProgramsQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtGetProgramsQCOM'.");
            }
        }
        public static void glExtGetProgramsQCOM(GLuint * @programs, GLint @maxPrograms, GLint * @numPrograms) => glExtGetProgramsQCOMPtr(@programs, @maxPrograms, @numPrograms);

        internal delegate GLboolean glExtIsProgramBinaryQCOMFunc(GLuint @program);
        internal static glExtIsProgramBinaryQCOMFunc glExtIsProgramBinaryQCOMPtr;
        internal static void loadExtIsProgramBinaryQCOM()
        {
            try
            {
                glExtIsProgramBinaryQCOMPtr = (glExtIsProgramBinaryQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtIsProgramBinaryQCOM"), typeof(glExtIsProgramBinaryQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtIsProgramBinaryQCOM'.");
            }
        }
        public static GLboolean glExtIsProgramBinaryQCOM(GLuint @program) => glExtIsProgramBinaryQCOMPtr(@program);

        internal delegate void glExtGetProgramBinarySourceQCOMFunc(GLuint @program, GLenum @shadertype, GLchar * @source, GLint * @length);
        internal static glExtGetProgramBinarySourceQCOMFunc glExtGetProgramBinarySourceQCOMPtr;
        internal static void loadExtGetProgramBinarySourceQCOM()
        {
            try
            {
                glExtGetProgramBinarySourceQCOMPtr = (glExtGetProgramBinarySourceQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glExtGetProgramBinarySourceQCOM"), typeof(glExtGetProgramBinarySourceQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glExtGetProgramBinarySourceQCOM'.");
            }
        }
        public static void glExtGetProgramBinarySourceQCOM(GLuint @program, GLenum @shadertype, GLchar * @source, GLint * @length) => glExtGetProgramBinarySourceQCOMPtr(@program, @shadertype, @source, @length);
        #endregion
    }
}
