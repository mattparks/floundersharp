using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_QCOM_writeonly_rendering
    {
        #region Interop
        static GL_QCOM_writeonly_rendering()
        {
            Console.WriteLine("Initalising GL_QCOM_writeonly_rendering interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_WRITEONLY_RENDERING_QCOM = 0x8823;
        #endregion

        #region Commands
        #endregion
    }
}
