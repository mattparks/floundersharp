using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_QCOM_binning_control
    {
        #region Interop
        static GL_QCOM_binning_control()
        {
            Console.WriteLine("Initalising GL_QCOM_binning_control interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_BINNING_CONTROL_HINT_QCOM = 0x8FB0;
        public static UInt32 GL_CPU_OPTIMIZED_QCOM = 0x8FB1;
        public static UInt32 GL_GPU_OPTIMIZED_QCOM = 0x8FB2;
        public static UInt32 GL_RENDER_DIRECT_TO_FRAMEBUFFER_QCOM = 0x8FB3;
        #endregion

        #region Commands
        #endregion
    }
}
