using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_QCOM_tiled_rendering
    {
        #region Interop
        static GL_QCOM_tiled_rendering()
        {
            Console.WriteLine("Initalising GL_QCOM_tiled_rendering interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadStartTilingQCOM();
            loadEndTilingQCOM();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COLOR_BUFFER_BIT0_QCOM = 0x00000001;
        public static UInt32 GL_COLOR_BUFFER_BIT1_QCOM = 0x00000002;
        public static UInt32 GL_COLOR_BUFFER_BIT2_QCOM = 0x00000004;
        public static UInt32 GL_COLOR_BUFFER_BIT3_QCOM = 0x00000008;
        public static UInt32 GL_COLOR_BUFFER_BIT4_QCOM = 0x00000010;
        public static UInt32 GL_COLOR_BUFFER_BIT5_QCOM = 0x00000020;
        public static UInt32 GL_COLOR_BUFFER_BIT6_QCOM = 0x00000040;
        public static UInt32 GL_COLOR_BUFFER_BIT7_QCOM = 0x00000080;
        public static UInt32 GL_DEPTH_BUFFER_BIT0_QCOM = 0x00000100;
        public static UInt32 GL_DEPTH_BUFFER_BIT1_QCOM = 0x00000200;
        public static UInt32 GL_DEPTH_BUFFER_BIT2_QCOM = 0x00000400;
        public static UInt32 GL_DEPTH_BUFFER_BIT3_QCOM = 0x00000800;
        public static UInt32 GL_DEPTH_BUFFER_BIT4_QCOM = 0x00001000;
        public static UInt32 GL_DEPTH_BUFFER_BIT5_QCOM = 0x00002000;
        public static UInt32 GL_DEPTH_BUFFER_BIT6_QCOM = 0x00004000;
        public static UInt32 GL_DEPTH_BUFFER_BIT7_QCOM = 0x00008000;
        public static UInt32 GL_STENCIL_BUFFER_BIT0_QCOM = 0x00010000;
        public static UInt32 GL_STENCIL_BUFFER_BIT1_QCOM = 0x00020000;
        public static UInt32 GL_STENCIL_BUFFER_BIT2_QCOM = 0x00040000;
        public static UInt32 GL_STENCIL_BUFFER_BIT3_QCOM = 0x00080000;
        public static UInt32 GL_STENCIL_BUFFER_BIT4_QCOM = 0x00100000;
        public static UInt32 GL_STENCIL_BUFFER_BIT5_QCOM = 0x00200000;
        public static UInt32 GL_STENCIL_BUFFER_BIT6_QCOM = 0x00400000;
        public static UInt32 GL_STENCIL_BUFFER_BIT7_QCOM = 0x00800000;
        public static UInt32 GL_MULTISAMPLE_BUFFER_BIT0_QCOM = 0x01000000;
        public static UInt32 GL_MULTISAMPLE_BUFFER_BIT1_QCOM = 0x02000000;
        public static UInt32 GL_MULTISAMPLE_BUFFER_BIT2_QCOM = 0x04000000;
        public static UInt32 GL_MULTISAMPLE_BUFFER_BIT3_QCOM = 0x08000000;
        public static UInt32 GL_MULTISAMPLE_BUFFER_BIT4_QCOM = 0x10000000;
        public static UInt32 GL_MULTISAMPLE_BUFFER_BIT5_QCOM = 0x20000000;
        public static UInt32 GL_MULTISAMPLE_BUFFER_BIT6_QCOM = 0x40000000;
        public static UInt32 GL_MULTISAMPLE_BUFFER_BIT7_QCOM = 0x80000000;
        #endregion

        #region Commands
        internal delegate void glStartTilingQCOMFunc(GLuint @x, GLuint @y, GLuint @width, GLuint @height, GLbitfield @preserveMask);
        internal static glStartTilingQCOMFunc glStartTilingQCOMPtr;
        internal static void loadStartTilingQCOM()
        {
            try
            {
                glStartTilingQCOMPtr = (glStartTilingQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStartTilingQCOM"), typeof(glStartTilingQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStartTilingQCOM'.");
            }
        }
        public static void glStartTilingQCOM(GLuint @x, GLuint @y, GLuint @width, GLuint @height, GLbitfield @preserveMask) => glStartTilingQCOMPtr(@x, @y, @width, @height, @preserveMask);

        internal delegate void glEndTilingQCOMFunc(GLbitfield @preserveMask);
        internal static glEndTilingQCOMFunc glEndTilingQCOMPtr;
        internal static void loadEndTilingQCOM()
        {
            try
            {
                glEndTilingQCOMPtr = (glEndTilingQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndTilingQCOM"), typeof(glEndTilingQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndTilingQCOM'.");
            }
        }
        public static void glEndTilingQCOM(GLbitfield @preserveMask) => glEndTilingQCOMPtr(@preserveMask);
        #endregion
    }
}
