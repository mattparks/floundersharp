using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_QCOM_alpha_test
    {
        #region Interop
        static GL_QCOM_alpha_test()
        {
            Console.WriteLine("Initalising GL_QCOM_alpha_test interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadAlphaFuncQCOM();
        }
        #endregion

        #region Enums
        public static UInt32 GL_ALPHA_TEST_QCOM = 0x0BC0;
        public static UInt32 GL_ALPHA_TEST_FUNC_QCOM = 0x0BC1;
        public static UInt32 GL_ALPHA_TEST_REF_QCOM = 0x0BC2;
        #endregion

        #region Commands
        internal delegate void glAlphaFuncQCOMFunc(GLenum @func, GLclampf @ref);
        internal static glAlphaFuncQCOMFunc glAlphaFuncQCOMPtr;
        internal static void loadAlphaFuncQCOM()
        {
            try
            {
                glAlphaFuncQCOMPtr = (glAlphaFuncQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAlphaFuncQCOM"), typeof(glAlphaFuncQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAlphaFuncQCOM'.");
            }
        }
        public static void glAlphaFuncQCOM(GLenum @func, GLclampf @ref) => glAlphaFuncQCOMPtr(@func, @ref);
        #endregion
    }
}
