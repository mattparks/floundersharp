using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_QCOM_driver_control
    {
        #region Interop
        static GL_QCOM_driver_control()
        {
            Console.WriteLine("Initalising GL_QCOM_driver_control interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetDriverControlsQCOM();
            loadGetDriverControlStringQCOM();
            loadEnableDriverControlQCOM();
            loadDisableDriverControlQCOM();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glGetDriverControlsQCOMFunc(GLint * @num, GLsizei @size, GLuint * @driverControls);
        internal static glGetDriverControlsQCOMFunc glGetDriverControlsQCOMPtr;
        internal static void loadGetDriverControlsQCOM()
        {
            try
            {
                glGetDriverControlsQCOMPtr = (glGetDriverControlsQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDriverControlsQCOM"), typeof(glGetDriverControlsQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDriverControlsQCOM'.");
            }
        }
        public static void glGetDriverControlsQCOM(GLint * @num, GLsizei @size, GLuint * @driverControls) => glGetDriverControlsQCOMPtr(@num, @size, @driverControls);

        internal delegate void glGetDriverControlStringQCOMFunc(GLuint @driverControl, GLsizei @bufSize, GLsizei * @length, GLchar * @driverControlString);
        internal static glGetDriverControlStringQCOMFunc glGetDriverControlStringQCOMPtr;
        internal static void loadGetDriverControlStringQCOM()
        {
            try
            {
                glGetDriverControlStringQCOMPtr = (glGetDriverControlStringQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDriverControlStringQCOM"), typeof(glGetDriverControlStringQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDriverControlStringQCOM'.");
            }
        }
        public static void glGetDriverControlStringQCOM(GLuint @driverControl, GLsizei @bufSize, GLsizei * @length, GLchar * @driverControlString) => glGetDriverControlStringQCOMPtr(@driverControl, @bufSize, @length, @driverControlString);

        internal delegate void glEnableDriverControlQCOMFunc(GLuint @driverControl);
        internal static glEnableDriverControlQCOMFunc glEnableDriverControlQCOMPtr;
        internal static void loadEnableDriverControlQCOM()
        {
            try
            {
                glEnableDriverControlQCOMPtr = (glEnableDriverControlQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableDriverControlQCOM"), typeof(glEnableDriverControlQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableDriverControlQCOM'.");
            }
        }
        public static void glEnableDriverControlQCOM(GLuint @driverControl) => glEnableDriverControlQCOMPtr(@driverControl);

        internal delegate void glDisableDriverControlQCOMFunc(GLuint @driverControl);
        internal static glDisableDriverControlQCOMFunc glDisableDriverControlQCOMPtr;
        internal static void loadDisableDriverControlQCOM()
        {
            try
            {
                glDisableDriverControlQCOMPtr = (glDisableDriverControlQCOMFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableDriverControlQCOM"), typeof(glDisableDriverControlQCOMFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableDriverControlQCOM'.");
            }
        }
        public static void glDisableDriverControlQCOM(GLuint @driverControl) => glDisableDriverControlQCOMPtr(@driverControl);
        #endregion
    }
}
