using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_QCOM_perfmon_global_mode
    {
        #region Interop
        static GL_QCOM_perfmon_global_mode()
        {
            Console.WriteLine("Initalising GL_QCOM_perfmon_global_mode interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_PERFMON_GLOBAL_MODE_QCOM = 0x8FA0;
        #endregion

        #region Commands
        #endregion
    }
}
