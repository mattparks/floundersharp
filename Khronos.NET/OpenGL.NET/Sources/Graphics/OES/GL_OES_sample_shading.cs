using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_sample_shading
    {
        #region Interop
        static GL_OES_sample_shading()
        {
            Console.WriteLine("Initalising GL_OES_sample_shading interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMinSampleShadingOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SAMPLE_SHADING_OES = 0x8C36;
        public static UInt32 GL_MIN_SAMPLE_SHADING_VALUE_OES = 0x8C37;
        #endregion

        #region Commands
        internal delegate void glMinSampleShadingOESFunc(GLfloat @value);
        internal static glMinSampleShadingOESFunc glMinSampleShadingOESPtr;
        internal static void loadMinSampleShadingOES()
        {
            try
            {
                glMinSampleShadingOESPtr = (glMinSampleShadingOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMinSampleShadingOES"), typeof(glMinSampleShadingOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMinSampleShadingOES'.");
            }
        }
        public static void glMinSampleShadingOES(GLfloat @value) => glMinSampleShadingOESPtr(@value);
        #endregion
    }
}
