using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_required_internalformat
    {
        #region Interop
        static GL_OES_required_internalformat()
        {
            Console.WriteLine("Initalising GL_OES_required_internalformat interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_ALPHA8_OES = 0x803C;
        public static UInt32 GL_DEPTH_COMPONENT16_OES = 0x81A5;
        public static UInt32 GL_DEPTH_COMPONENT24_OES = 0x81A6;
        public static UInt32 GL_DEPTH24_STENCIL8_OES = 0x88F0;
        public static UInt32 GL_DEPTH_COMPONENT32_OES = 0x81A7;
        public static UInt32 GL_LUMINANCE4_ALPHA4_OES = 0x8043;
        public static UInt32 GL_LUMINANCE8_ALPHA8_OES = 0x8045;
        public static UInt32 GL_LUMINANCE8_OES = 0x8040;
        public static UInt32 GL_RGBA4_OES = 0x8056;
        public static UInt32 GL_RGB5_A1_OES = 0x8057;
        public static UInt32 GL_RGB565_OES = 0x8D62;
        public static UInt32 GL_RGB8_OES = 0x8051;
        public static UInt32 GL_RGBA8_OES = 0x8058;
        public static UInt32 GL_RGB10_EXT = 0x8052;
        public static UInt32 GL_RGB10_A2_EXT = 0x8059;
        #endregion

        #region Commands
        #endregion
    }
}
