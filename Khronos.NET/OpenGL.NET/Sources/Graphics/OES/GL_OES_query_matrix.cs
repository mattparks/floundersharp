using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_query_matrix
    {
        #region Interop
        static GL_OES_query_matrix()
        {
            Console.WriteLine("Initalising GL_OES_query_matrix interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadQueryMatrixxOES();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate GLbitfield glQueryMatrixxOESFunc(GLfixed * @mantissa, GLint * @exponent);
        internal static glQueryMatrixxOESFunc glQueryMatrixxOESPtr;
        internal static void loadQueryMatrixxOES()
        {
            try
            {
                glQueryMatrixxOESPtr = (glQueryMatrixxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glQueryMatrixxOES"), typeof(glQueryMatrixxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glQueryMatrixxOES'.");
            }
        }
        public static GLbitfield glQueryMatrixxOES(GLfixed * @mantissa, GLint * @exponent) => glQueryMatrixxOESPtr(@mantissa, @exponent);
        #endregion
    }
}
