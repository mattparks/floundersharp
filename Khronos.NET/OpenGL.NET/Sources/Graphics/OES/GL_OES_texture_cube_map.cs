using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_texture_cube_map
    {
        #region Interop
        static GL_OES_texture_cube_map()
        {
            Console.WriteLine("Initalising GL_OES_texture_cube_map interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexGenfOES();
            loadTexGenfvOES();
            loadTexGeniOES();
            loadTexGenivOES();
            loadTexGenxOES();
            loadTexGenxvOES();
            loadGetTexGenfvOES();
            loadGetTexGenivOES();
            loadGetTexGenxvOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_NORMAL_MAP_OES = 0x8511;
        public static UInt32 GL_REFLECTION_MAP_OES = 0x8512;
        public static UInt32 GL_TEXTURE_CUBE_MAP_OES = 0x8513;
        public static UInt32 GL_TEXTURE_BINDING_CUBE_MAP_OES = 0x8514;
        public static UInt32 GL_TEXTURE_CUBE_MAP_POSITIVE_X_OES = 0x8515;
        public static UInt32 GL_TEXTURE_CUBE_MAP_NEGATIVE_X_OES = 0x8516;
        public static UInt32 GL_TEXTURE_CUBE_MAP_POSITIVE_Y_OES = 0x8517;
        public static UInt32 GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_OES = 0x8518;
        public static UInt32 GL_TEXTURE_CUBE_MAP_POSITIVE_Z_OES = 0x8519;
        public static UInt32 GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_OES = 0x851A;
        public static UInt32 GL_MAX_CUBE_MAP_TEXTURE_SIZE_OES = 0x851C;
        public static UInt32 GL_TEXTURE_GEN_MODE_OES = 0x2500;
        public static UInt32 GL_TEXTURE_GEN_STR_OES = 0x8D60;
        #endregion

        #region Commands
        internal delegate void glTexGenfOESFunc(GLenum @coord, GLenum @pname, GLfloat @param);
        internal static glTexGenfOESFunc glTexGenfOESPtr;
        internal static void loadTexGenfOES()
        {
            try
            {
                glTexGenfOESPtr = (glTexGenfOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGenfOES"), typeof(glTexGenfOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGenfOES'.");
            }
        }
        public static void glTexGenfOES(GLenum @coord, GLenum @pname, GLfloat @param) => glTexGenfOESPtr(@coord, @pname, @param);

        internal delegate void glTexGenfvOESFunc(GLenum @coord, GLenum @pname, const GLfloat * @params);
        internal static glTexGenfvOESFunc glTexGenfvOESPtr;
        internal static void loadTexGenfvOES()
        {
            try
            {
                glTexGenfvOESPtr = (glTexGenfvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGenfvOES"), typeof(glTexGenfvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGenfvOES'.");
            }
        }
        public static void glTexGenfvOES(GLenum @coord, GLenum @pname, const GLfloat * @params) => glTexGenfvOESPtr(@coord, @pname, @params);

        internal delegate void glTexGeniOESFunc(GLenum @coord, GLenum @pname, GLint @param);
        internal static glTexGeniOESFunc glTexGeniOESPtr;
        internal static void loadTexGeniOES()
        {
            try
            {
                glTexGeniOESPtr = (glTexGeniOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGeniOES"), typeof(glTexGeniOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGeniOES'.");
            }
        }
        public static void glTexGeniOES(GLenum @coord, GLenum @pname, GLint @param) => glTexGeniOESPtr(@coord, @pname, @param);

        internal delegate void glTexGenivOESFunc(GLenum @coord, GLenum @pname, const GLint * @params);
        internal static glTexGenivOESFunc glTexGenivOESPtr;
        internal static void loadTexGenivOES()
        {
            try
            {
                glTexGenivOESPtr = (glTexGenivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGenivOES"), typeof(glTexGenivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGenivOES'.");
            }
        }
        public static void glTexGenivOES(GLenum @coord, GLenum @pname, const GLint * @params) => glTexGenivOESPtr(@coord, @pname, @params);

        internal delegate void glTexGenxOESFunc(GLenum @coord, GLenum @pname, GLfixed @param);
        internal static glTexGenxOESFunc glTexGenxOESPtr;
        internal static void loadTexGenxOES()
        {
            try
            {
                glTexGenxOESPtr = (glTexGenxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGenxOES"), typeof(glTexGenxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGenxOES'.");
            }
        }
        public static void glTexGenxOES(GLenum @coord, GLenum @pname, GLfixed @param) => glTexGenxOESPtr(@coord, @pname, @param);

        internal delegate void glTexGenxvOESFunc(GLenum @coord, GLenum @pname, const GLfixed * @params);
        internal static glTexGenxvOESFunc glTexGenxvOESPtr;
        internal static void loadTexGenxvOES()
        {
            try
            {
                glTexGenxvOESPtr = (glTexGenxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGenxvOES"), typeof(glTexGenxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGenxvOES'.");
            }
        }
        public static void glTexGenxvOES(GLenum @coord, GLenum @pname, const GLfixed * @params) => glTexGenxvOESPtr(@coord, @pname, @params);

        internal delegate void glGetTexGenfvOESFunc(GLenum @coord, GLenum @pname, GLfloat * @params);
        internal static glGetTexGenfvOESFunc glGetTexGenfvOESPtr;
        internal static void loadGetTexGenfvOES()
        {
            try
            {
                glGetTexGenfvOESPtr = (glGetTexGenfvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexGenfvOES"), typeof(glGetTexGenfvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexGenfvOES'.");
            }
        }
        public static void glGetTexGenfvOES(GLenum @coord, GLenum @pname, GLfloat * @params) => glGetTexGenfvOESPtr(@coord, @pname, @params);

        internal delegate void glGetTexGenivOESFunc(GLenum @coord, GLenum @pname, GLint * @params);
        internal static glGetTexGenivOESFunc glGetTexGenivOESPtr;
        internal static void loadGetTexGenivOES()
        {
            try
            {
                glGetTexGenivOESPtr = (glGetTexGenivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexGenivOES"), typeof(glGetTexGenivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexGenivOES'.");
            }
        }
        public static void glGetTexGenivOES(GLenum @coord, GLenum @pname, GLint * @params) => glGetTexGenivOESPtr(@coord, @pname, @params);

        internal delegate void glGetTexGenxvOESFunc(GLenum @coord, GLenum @pname, GLfixed * @params);
        internal static glGetTexGenxvOESFunc glGetTexGenxvOESPtr;
        internal static void loadGetTexGenxvOES()
        {
            try
            {
                glGetTexGenxvOESPtr = (glGetTexGenxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexGenxvOES"), typeof(glGetTexGenxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexGenxvOES'.");
            }
        }
        public static void glGetTexGenxvOES(GLenum @coord, GLenum @pname, GLfixed * @params) => glGetTexGenxvOESPtr(@coord, @pname, @params);
        #endregion
    }
}
