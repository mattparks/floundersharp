using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_texture_buffer
    {
        #region Interop
        static GL_OES_texture_buffer()
        {
            Console.WriteLine("Initalising GL_OES_texture_buffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexBufferOES();
            loadTexBufferRangeOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_BUFFER_OES = 0x8C2A;
        public static UInt32 GL_TEXTURE_BUFFER_BINDING_OES = 0x8C2A;
        public static UInt32 GL_MAX_TEXTURE_BUFFER_SIZE_OES = 0x8C2B;
        public static UInt32 GL_TEXTURE_BINDING_BUFFER_OES = 0x8C2C;
        public static UInt32 GL_TEXTURE_BUFFER_DATA_STORE_BINDING_OES = 0x8C2D;
        public static UInt32 GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT_OES = 0x919F;
        public static UInt32 GL_SAMPLER_BUFFER_OES = 0x8DC2;
        public static UInt32 GL_INT_SAMPLER_BUFFER_OES = 0x8DD0;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_BUFFER_OES = 0x8DD8;
        public static UInt32 GL_IMAGE_BUFFER_OES = 0x9051;
        public static UInt32 GL_INT_IMAGE_BUFFER_OES = 0x905C;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_BUFFER_OES = 0x9067;
        public static UInt32 GL_TEXTURE_BUFFER_OFFSET_OES = 0x919D;
        public static UInt32 GL_TEXTURE_BUFFER_SIZE_OES = 0x919E;
        #endregion

        #region Commands
        internal delegate void glTexBufferOESFunc(GLenum @target, GLenum @internalformat, GLuint @buffer);
        internal static glTexBufferOESFunc glTexBufferOESPtr;
        internal static void loadTexBufferOES()
        {
            try
            {
                glTexBufferOESPtr = (glTexBufferOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBufferOES"), typeof(glTexBufferOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBufferOES'.");
            }
        }
        public static void glTexBufferOES(GLenum @target, GLenum @internalformat, GLuint @buffer) => glTexBufferOESPtr(@target, @internalformat, @buffer);

        internal delegate void glTexBufferRangeOESFunc(GLenum @target, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glTexBufferRangeOESFunc glTexBufferRangeOESPtr;
        internal static void loadTexBufferRangeOES()
        {
            try
            {
                glTexBufferRangeOESPtr = (glTexBufferRangeOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBufferRangeOES"), typeof(glTexBufferRangeOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBufferRangeOES'.");
            }
        }
        public static void glTexBufferRangeOES(GLenum @target, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glTexBufferRangeOESPtr(@target, @internalformat, @buffer, @offset, @size);
        #endregion
    }
}
