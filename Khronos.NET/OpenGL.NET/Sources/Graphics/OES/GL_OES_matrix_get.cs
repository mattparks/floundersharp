using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_matrix_get
    {
        #region Interop
        static GL_OES_matrix_get()
        {
            Console.WriteLine("Initalising GL_OES_matrix_get interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MODELVIEW_MATRIX_FLOAT_AS_INT_BITS_OES = 0x898D;
        public static UInt32 GL_PROJECTION_MATRIX_FLOAT_AS_INT_BITS_OES = 0x898E;
        public static UInt32 GL_TEXTURE_MATRIX_FLOAT_AS_INT_BITS_OES = 0x898F;
        #endregion

        #region Commands
        #endregion
    }
}
