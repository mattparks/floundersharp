using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_draw_buffers_indexed
    {
        #region Interop
        static GL_OES_draw_buffers_indexed()
        {
            Console.WriteLine("Initalising GL_OES_draw_buffers_indexed interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadEnableiOES();
            loadDisableiOES();
            loadBlendEquationiOES();
            loadBlendEquationSeparateiOES();
            loadBlendFunciOES();
            loadBlendFuncSeparateiOES();
            loadColorMaskiOES();
            loadIsEnablediOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BLEND_EQUATION_RGB = 0x8009;
        public static UInt32 GL_BLEND_EQUATION_ALPHA = 0x883D;
        public static UInt32 GL_BLEND_SRC_RGB = 0x80C9;
        public static UInt32 GL_BLEND_SRC_ALPHA = 0x80CB;
        public static UInt32 GL_BLEND_DST_RGB = 0x80C8;
        public static UInt32 GL_BLEND_DST_ALPHA = 0x80CA;
        public static UInt32 GL_COLOR_WRITEMASK = 0x0C23;
        public static UInt32 GL_BLEND = 0x0BE2;
        public static UInt32 GL_FUNC_ADD = 0x8006;
        public static UInt32 GL_FUNC_SUBTRACT = 0x800A;
        public static UInt32 GL_FUNC_REVERSE_SUBTRACT = 0x800B;
        public static UInt32 GL_MIN = 0x8007;
        public static UInt32 GL_MAX = 0x8008;
        public static UInt32 GL_ZERO = 0;
        public static UInt32 GL_ONE = 1;
        public static UInt32 GL_SRC_COLOR = 0x0300;
        public static UInt32 GL_ONE_MINUS_SRC_COLOR = 0x0301;
        public static UInt32 GL_DST_COLOR = 0x0306;
        public static UInt32 GL_ONE_MINUS_DST_COLOR = 0x0307;
        public static UInt32 GL_SRC_ALPHA = 0x0302;
        public static UInt32 GL_ONE_MINUS_SRC_ALPHA = 0x0303;
        public static UInt32 GL_DST_ALPHA = 0x0304;
        public static UInt32 GL_ONE_MINUS_DST_ALPHA = 0x0305;
        public static UInt32 GL_CONSTANT_COLOR = 0x8001;
        public static UInt32 GL_ONE_MINUS_CONSTANT_COLOR = 0x8002;
        public static UInt32 GL_CONSTANT_ALPHA = 0x8003;
        public static UInt32 GL_ONE_MINUS_CONSTANT_ALPHA = 0x8004;
        public static UInt32 GL_SRC_ALPHA_SATURATE = 0x0308;
        #endregion

        #region Commands
        internal delegate void glEnableiOESFunc(GLenum @target, GLuint @index);
        internal static glEnableiOESFunc glEnableiOESPtr;
        internal static void loadEnableiOES()
        {
            try
            {
                glEnableiOESPtr = (glEnableiOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableiOES"), typeof(glEnableiOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableiOES'.");
            }
        }
        public static void glEnableiOES(GLenum @target, GLuint @index) => glEnableiOESPtr(@target, @index);

        internal delegate void glDisableiOESFunc(GLenum @target, GLuint @index);
        internal static glDisableiOESFunc glDisableiOESPtr;
        internal static void loadDisableiOES()
        {
            try
            {
                glDisableiOESPtr = (glDisableiOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableiOES"), typeof(glDisableiOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableiOES'.");
            }
        }
        public static void glDisableiOES(GLenum @target, GLuint @index) => glDisableiOESPtr(@target, @index);

        internal delegate void glBlendEquationiOESFunc(GLuint @buf, GLenum @mode);
        internal static glBlendEquationiOESFunc glBlendEquationiOESPtr;
        internal static void loadBlendEquationiOES()
        {
            try
            {
                glBlendEquationiOESPtr = (glBlendEquationiOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationiOES"), typeof(glBlendEquationiOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationiOES'.");
            }
        }
        public static void glBlendEquationiOES(GLuint @buf, GLenum @mode) => glBlendEquationiOESPtr(@buf, @mode);

        internal delegate void glBlendEquationSeparateiOESFunc(GLuint @buf, GLenum @modeRGB, GLenum @modeAlpha);
        internal static glBlendEquationSeparateiOESFunc glBlendEquationSeparateiOESPtr;
        internal static void loadBlendEquationSeparateiOES()
        {
            try
            {
                glBlendEquationSeparateiOESPtr = (glBlendEquationSeparateiOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationSeparateiOES"), typeof(glBlendEquationSeparateiOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationSeparateiOES'.");
            }
        }
        public static void glBlendEquationSeparateiOES(GLuint @buf, GLenum @modeRGB, GLenum @modeAlpha) => glBlendEquationSeparateiOESPtr(@buf, @modeRGB, @modeAlpha);

        internal delegate void glBlendFunciOESFunc(GLuint @buf, GLenum @src, GLenum @dst);
        internal static glBlendFunciOESFunc glBlendFunciOESPtr;
        internal static void loadBlendFunciOES()
        {
            try
            {
                glBlendFunciOESPtr = (glBlendFunciOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFunciOES"), typeof(glBlendFunciOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFunciOES'.");
            }
        }
        public static void glBlendFunciOES(GLuint @buf, GLenum @src, GLenum @dst) => glBlendFunciOESPtr(@buf, @src, @dst);

        internal delegate void glBlendFuncSeparateiOESFunc(GLuint @buf, GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha);
        internal static glBlendFuncSeparateiOESFunc glBlendFuncSeparateiOESPtr;
        internal static void loadBlendFuncSeparateiOES()
        {
            try
            {
                glBlendFuncSeparateiOESPtr = (glBlendFuncSeparateiOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFuncSeparateiOES"), typeof(glBlendFuncSeparateiOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFuncSeparateiOES'.");
            }
        }
        public static void glBlendFuncSeparateiOES(GLuint @buf, GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha) => glBlendFuncSeparateiOESPtr(@buf, @srcRGB, @dstRGB, @srcAlpha, @dstAlpha);

        internal delegate void glColorMaskiOESFunc(GLuint @index, GLboolean @r, GLboolean @g, GLboolean @b, GLboolean @a);
        internal static glColorMaskiOESFunc glColorMaskiOESPtr;
        internal static void loadColorMaskiOES()
        {
            try
            {
                glColorMaskiOESPtr = (glColorMaskiOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorMaskiOES"), typeof(glColorMaskiOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorMaskiOES'.");
            }
        }
        public static void glColorMaskiOES(GLuint @index, GLboolean @r, GLboolean @g, GLboolean @b, GLboolean @a) => glColorMaskiOESPtr(@index, @r, @g, @b, @a);

        internal delegate GLboolean glIsEnablediOESFunc(GLenum @target, GLuint @index);
        internal static glIsEnablediOESFunc glIsEnablediOESPtr;
        internal static void loadIsEnablediOES()
        {
            try
            {
                glIsEnablediOESPtr = (glIsEnablediOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsEnablediOES"), typeof(glIsEnablediOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsEnablediOES'.");
            }
        }
        public static GLboolean glIsEnablediOES(GLenum @target, GLuint @index) => glIsEnablediOESPtr(@target, @index);
        #endregion
    }
}
