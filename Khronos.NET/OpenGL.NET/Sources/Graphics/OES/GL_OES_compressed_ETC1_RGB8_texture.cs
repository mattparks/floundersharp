using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_compressed_ETC1_RGB8_texture
    {
        #region Interop
        static GL_OES_compressed_ETC1_RGB8_texture()
        {
            Console.WriteLine("Initalising GL_OES_compressed_ETC1_RGB8_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_ETC1_RGB8_OES = 0x8D64;
        #endregion

        #region Commands
        #endregion
    }
}
