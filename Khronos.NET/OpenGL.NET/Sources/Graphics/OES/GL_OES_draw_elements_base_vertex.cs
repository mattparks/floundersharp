using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_draw_elements_base_vertex
    {
        #region Interop
        static GL_OES_draw_elements_base_vertex()
        {
            Console.WriteLine("Initalising GL_OES_draw_elements_base_vertex interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawElementsBaseVertexOES();
            loadDrawRangeElementsBaseVertexOES();
            loadDrawElementsInstancedBaseVertexOES();
            loadMultiDrawElementsBaseVertexOES();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glDrawElementsBaseVertexOESFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex);
        internal static glDrawElementsBaseVertexOESFunc glDrawElementsBaseVertexOESPtr;
        internal static void loadDrawElementsBaseVertexOES()
        {
            try
            {
                glDrawElementsBaseVertexOESPtr = (glDrawElementsBaseVertexOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsBaseVertexOES"), typeof(glDrawElementsBaseVertexOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsBaseVertexOES'.");
            }
        }
        public static void glDrawElementsBaseVertexOES(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex) => glDrawElementsBaseVertexOESPtr(@mode, @count, @type, @indices, @basevertex);

        internal delegate void glDrawRangeElementsBaseVertexOESFunc(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex);
        internal static glDrawRangeElementsBaseVertexOESFunc glDrawRangeElementsBaseVertexOESPtr;
        internal static void loadDrawRangeElementsBaseVertexOES()
        {
            try
            {
                glDrawRangeElementsBaseVertexOESPtr = (glDrawRangeElementsBaseVertexOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawRangeElementsBaseVertexOES"), typeof(glDrawRangeElementsBaseVertexOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawRangeElementsBaseVertexOES'.");
            }
        }
        public static void glDrawRangeElementsBaseVertexOES(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex) => glDrawRangeElementsBaseVertexOESPtr(@mode, @start, @end, @count, @type, @indices, @basevertex);

        internal delegate void glDrawElementsInstancedBaseVertexOESFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex);
        internal static glDrawElementsInstancedBaseVertexOESFunc glDrawElementsInstancedBaseVertexOESPtr;
        internal static void loadDrawElementsInstancedBaseVertexOES()
        {
            try
            {
                glDrawElementsInstancedBaseVertexOESPtr = (glDrawElementsInstancedBaseVertexOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedBaseVertexOES"), typeof(glDrawElementsInstancedBaseVertexOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedBaseVertexOES'.");
            }
        }
        public static void glDrawElementsInstancedBaseVertexOES(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex) => glDrawElementsInstancedBaseVertexOESPtr(@mode, @count, @type, @indices, @instancecount, @basevertex);

        internal delegate void glMultiDrawElementsBaseVertexOESFunc(GLenum @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @primcount, const GLint * @basevertex);
        internal static glMultiDrawElementsBaseVertexOESFunc glMultiDrawElementsBaseVertexOESPtr;
        internal static void loadMultiDrawElementsBaseVertexOES()
        {
            try
            {
                glMultiDrawElementsBaseVertexOESPtr = (glMultiDrawElementsBaseVertexOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementsBaseVertexOES"), typeof(glMultiDrawElementsBaseVertexOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementsBaseVertexOES'.");
            }
        }
        public static void glMultiDrawElementsBaseVertexOES(GLenum @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @primcount, const GLint * @basevertex) => glMultiDrawElementsBaseVertexOESPtr(@mode, @count, @type, @indices, @primcount, @basevertex);
        #endregion
    }
}
