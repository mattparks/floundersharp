using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_stencil1
    {
        #region Interop
        static GL_OES_stencil1()
        {
            Console.WriteLine("Initalising GL_OES_stencil1 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_STENCIL_INDEX1_OES = 0x8D46;
        #endregion

        #region Commands
        #endregion
    }
}
