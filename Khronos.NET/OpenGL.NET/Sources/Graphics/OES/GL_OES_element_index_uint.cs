using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_element_index_uint
    {
        #region Interop
        static GL_OES_element_index_uint()
        {
            Console.WriteLine("Initalising GL_OES_element_index_uint interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_UNSIGNED_INT = 0x1405;
        #endregion

        #region Commands
        #endregion
    }
}
