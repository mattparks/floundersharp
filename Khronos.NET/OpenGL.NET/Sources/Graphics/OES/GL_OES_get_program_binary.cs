using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_get_program_binary
    {
        #region Interop
        static GL_OES_get_program_binary()
        {
            Console.WriteLine("Initalising GL_OES_get_program_binary interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetProgramBinaryOES();
            loadProgramBinaryOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PROGRAM_BINARY_LENGTH_OES = 0x8741;
        public static UInt32 GL_NUM_PROGRAM_BINARY_FORMATS_OES = 0x87FE;
        public static UInt32 GL_PROGRAM_BINARY_FORMATS_OES = 0x87FF;
        #endregion

        #region Commands
        internal delegate void glGetProgramBinaryOESFunc(GLuint @program, GLsizei @bufSize, GLsizei * @length, GLenum * @binaryFormat, void * @binary);
        internal static glGetProgramBinaryOESFunc glGetProgramBinaryOESPtr;
        internal static void loadGetProgramBinaryOES()
        {
            try
            {
                glGetProgramBinaryOESPtr = (glGetProgramBinaryOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramBinaryOES"), typeof(glGetProgramBinaryOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramBinaryOES'.");
            }
        }
        public static void glGetProgramBinaryOES(GLuint @program, GLsizei @bufSize, GLsizei * @length, GLenum * @binaryFormat, void * @binary) => glGetProgramBinaryOESPtr(@program, @bufSize, @length, @binaryFormat, @binary);

        internal delegate void glProgramBinaryOESFunc(GLuint @program, GLenum @binaryFormat, const void * @binary, GLint @length);
        internal static glProgramBinaryOESFunc glProgramBinaryOESPtr;
        internal static void loadProgramBinaryOES()
        {
            try
            {
                glProgramBinaryOESPtr = (glProgramBinaryOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramBinaryOES"), typeof(glProgramBinaryOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramBinaryOES'.");
            }
        }
        public static void glProgramBinaryOES(GLuint @program, GLenum @binaryFormat, const void * @binary, GLint @length) => glProgramBinaryOESPtr(@program, @binaryFormat, @binary, @length);
        #endregion
    }
}
