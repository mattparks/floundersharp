using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_shader_multisample_interpolation
    {
        #region Interop
        static GL_OES_shader_multisample_interpolation()
        {
            Console.WriteLine("Initalising GL_OES_shader_multisample_interpolation interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MIN_FRAGMENT_INTERPOLATION_OFFSET_OES = 0x8E5B;
        public static UInt32 GL_MAX_FRAGMENT_INTERPOLATION_OFFSET_OES = 0x8E5C;
        public static UInt32 GL_FRAGMENT_INTERPOLATION_OFFSET_BITS_OES = 0x8E5D;
        #endregion

        #region Commands
        #endregion
    }
}
