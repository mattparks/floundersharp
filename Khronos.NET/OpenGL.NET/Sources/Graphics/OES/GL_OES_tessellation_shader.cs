using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_tessellation_shader
    {
        #region Interop
        static GL_OES_tessellation_shader()
        {
            Console.WriteLine("Initalising GL_OES_tessellation_shader interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPatchParameteriOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PATCHES_OES = 0x000E;
        public static UInt32 GL_PATCH_VERTICES_OES = 0x8E72;
        public static UInt32 GL_TESS_CONTROL_OUTPUT_VERTICES_OES = 0x8E75;
        public static UInt32 GL_TESS_GEN_MODE_OES = 0x8E76;
        public static UInt32 GL_TESS_GEN_SPACING_OES = 0x8E77;
        public static UInt32 GL_TESS_GEN_VERTEX_ORDER_OES = 0x8E78;
        public static UInt32 GL_TESS_GEN_POINT_MODE_OES = 0x8E79;
        public static UInt32 GL_TRIANGLES = 0x0004;
        public static UInt32 GL_ISOLINES_OES = 0x8E7A;
        public static UInt32 GL_QUADS_OES = 0x0007;
        public static UInt32 GL_EQUAL = 0x0202;
        public static UInt32 GL_FRACTIONAL_ODD_OES = 0x8E7B;
        public static UInt32 GL_FRACTIONAL_EVEN_OES = 0x8E7C;
        public static UInt32 GL_CCW = 0x0901;
        public static UInt32 GL_CW = 0x0900;
        public static UInt32 GL_MAX_PATCH_VERTICES_OES = 0x8E7D;
        public static UInt32 GL_MAX_TESS_GEN_LEVEL_OES = 0x8E7E;
        public static UInt32 GL_MAX_TESS_CONTROL_UNIFORM_COMPONENTS_OES = 0x8E7F;
        public static UInt32 GL_MAX_TESS_EVALUATION_UNIFORM_COMPONENTS_OES = 0x8E80;
        public static UInt32 GL_MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS_OES = 0x8E81;
        public static UInt32 GL_MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS_OES = 0x8E82;
        public static UInt32 GL_MAX_TESS_CONTROL_OUTPUT_COMPONENTS_OES = 0x8E83;
        public static UInt32 GL_MAX_TESS_PATCH_COMPONENTS_OES = 0x8E84;
        public static UInt32 GL_MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS_OES = 0x8E85;
        public static UInt32 GL_MAX_TESS_EVALUATION_OUTPUT_COMPONENTS_OES = 0x8E86;
        public static UInt32 GL_MAX_TESS_CONTROL_UNIFORM_BLOCKS_OES = 0x8E89;
        public static UInt32 GL_MAX_TESS_EVALUATION_UNIFORM_BLOCKS_OES = 0x8E8A;
        public static UInt32 GL_MAX_TESS_CONTROL_INPUT_COMPONENTS_OES = 0x886C;
        public static UInt32 GL_MAX_TESS_EVALUATION_INPUT_COMPONENTS_OES = 0x886D;
        public static UInt32 GL_MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS_OES = 0x8E1E;
        public static UInt32 GL_MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS_OES = 0x8E1F;
        public static UInt32 GL_MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS_OES = 0x92CD;
        public static UInt32 GL_MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS_OES = 0x92CE;
        public static UInt32 GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS_OES = 0x92D3;
        public static UInt32 GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS_OES = 0x92D4;
        public static UInt32 GL_MAX_TESS_CONTROL_IMAGE_UNIFORMS_OES = 0x90CB;
        public static UInt32 GL_MAX_TESS_EVALUATION_IMAGE_UNIFORMS_OES = 0x90CC;
        public static UInt32 GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS_OES = 0x90D8;
        public static UInt32 GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS_OES = 0x90D9;
        public static UInt32 GL_PRIMITIVE_RESTART_FOR_PATCHES_SUPPORTED_OES = 0x8221;
        public static UInt32 GL_IS_PER_PATCH_OES = 0x92E7;
        public static UInt32 GL_REFERENCED_BY_TESS_CONTROL_SHADER_OES = 0x9307;
        public static UInt32 GL_REFERENCED_BY_TESS_EVALUATION_SHADER_OES = 0x9308;
        public static UInt32 GL_TESS_CONTROL_SHADER_OES = 0x8E88;
        public static UInt32 GL_TESS_EVALUATION_SHADER_OES = 0x8E87;
        public static UInt32 GL_TESS_CONTROL_SHADER_BIT_OES = 0x00000008;
        public static UInt32 GL_TESS_EVALUATION_SHADER_BIT_OES = 0x00000010;
        #endregion

        #region Commands
        internal delegate void glPatchParameteriOESFunc(GLenum @pname, GLint @value);
        internal static glPatchParameteriOESFunc glPatchParameteriOESPtr;
        internal static void loadPatchParameteriOES()
        {
            try
            {
                glPatchParameteriOESPtr = (glPatchParameteriOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPatchParameteriOES"), typeof(glPatchParameteriOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPatchParameteriOES'.");
            }
        }
        public static void glPatchParameteriOES(GLenum @pname, GLint @value) => glPatchParameteriOESPtr(@pname, @value);
        #endregion
    }
}
