using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_EGL_image_external
    {
        #region Interop
        static GL_OES_EGL_image_external()
        {
            Console.WriteLine("Initalising GL_OES_EGL_image_external interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_EXTERNAL_OES = 0x8D65;
        public static UInt32 GL_TEXTURE_BINDING_EXTERNAL_OES = 0x8D67;
        public static UInt32 GL_REQUIRED_TEXTURE_IMAGE_UNITS_OES = 0x8D68;
        public static UInt32 GL_SAMPLER_EXTERNAL_OES = 0x8D66;
        #endregion

        #region Commands
        #endregion
    }
}
