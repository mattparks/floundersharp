using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_depth_texture
    {
        #region Interop
        static GL_OES_depth_texture()
        {
            Console.WriteLine("Initalising GL_OES_depth_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_COMPONENT = 0x1902;
        public static UInt32 GL_UNSIGNED_SHORT = 0x1403;
        public static UInt32 GL_UNSIGNED_INT = 0x1405;
        #endregion

        #region Commands
        #endregion
    }
}
