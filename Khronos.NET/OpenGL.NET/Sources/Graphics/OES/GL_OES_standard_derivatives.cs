using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_standard_derivatives
    {
        #region Interop
        static GL_OES_standard_derivatives()
        {
            Console.WriteLine("Initalising GL_OES_standard_derivatives interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAGMENT_SHADER_DERIVATIVE_HINT_OES = 0x8B8B;
        #endregion

        #region Commands
        #endregion
    }
}
