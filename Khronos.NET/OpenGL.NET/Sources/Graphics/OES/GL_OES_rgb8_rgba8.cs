using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_rgb8_rgba8
    {
        #region Interop
        static GL_OES_rgb8_rgba8()
        {
            Console.WriteLine("Initalising GL_OES_rgb8_rgba8 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RGB8_OES = 0x8051;
        public static UInt32 GL_RGBA8_OES = 0x8058;
        #endregion

        #region Commands
        #endregion
    }
}
