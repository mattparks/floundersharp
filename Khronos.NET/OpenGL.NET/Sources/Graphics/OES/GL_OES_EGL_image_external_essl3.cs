using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_EGL_image_external_essl3
    {
        #region Interop
        static GL_OES_EGL_image_external_essl3()
        {
            Console.WriteLine("Initalising GL_OES_EGL_image_external_essl3 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        #endregion
    }
}
