using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_texture_3D
    {
        #region Interop
        static GL_OES_texture_3D()
        {
            Console.WriteLine("Initalising GL_OES_texture_3D interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexImage3DOES();
            loadTexSubImage3DOES();
            loadCopyTexSubImage3DOES();
            loadCompressedTexImage3DOES();
            loadCompressedTexSubImage3DOES();
            loadFramebufferTexture3DOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_WRAP_R_OES = 0x8072;
        public static UInt32 GL_TEXTURE_3D_OES = 0x806F;
        public static UInt32 GL_TEXTURE_BINDING_3D_OES = 0x806A;
        public static UInt32 GL_MAX_3D_TEXTURE_SIZE_OES = 0x8073;
        public static UInt32 GL_SAMPLER_3D_OES = 0x8B5F;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_3D_ZOFFSET_OES = 0x8CD4;
        #endregion

        #region Commands
        internal delegate void glTexImage3DOESFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexImage3DOESFunc glTexImage3DOESPtr;
        internal static void loadTexImage3DOES()
        {
            try
            {
                glTexImage3DOESPtr = (glTexImage3DOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage3DOES"), typeof(glTexImage3DOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage3DOES'.");
            }
        }
        public static void glTexImage3DOES(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glTexImage3DOESPtr(@target, @level, @internalformat, @width, @height, @depth, @border, @format, @type, @pixels);

        internal delegate void glTexSubImage3DOESFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexSubImage3DOESFunc glTexSubImage3DOESPtr;
        internal static void loadTexSubImage3DOES()
        {
            try
            {
                glTexSubImage3DOESPtr = (glTexSubImage3DOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexSubImage3DOES"), typeof(glTexSubImage3DOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexSubImage3DOES'.");
            }
        }
        public static void glTexSubImage3DOES(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels) => glTexSubImage3DOESPtr(@target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @type, @pixels);

        internal delegate void glCopyTexSubImage3DOESFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyTexSubImage3DOESFunc glCopyTexSubImage3DOESPtr;
        internal static void loadCopyTexSubImage3DOES()
        {
            try
            {
                glCopyTexSubImage3DOESPtr = (glCopyTexSubImage3DOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexSubImage3DOES"), typeof(glCopyTexSubImage3DOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexSubImage3DOES'.");
            }
        }
        public static void glCopyTexSubImage3DOES(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyTexSubImage3DOESPtr(@target, @level, @xoffset, @yoffset, @zoffset, @x, @y, @width, @height);

        internal delegate void glCompressedTexImage3DOESFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexImage3DOESFunc glCompressedTexImage3DOESPtr;
        internal static void loadCompressedTexImage3DOES()
        {
            try
            {
                glCompressedTexImage3DOESPtr = (glCompressedTexImage3DOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexImage3DOES"), typeof(glCompressedTexImage3DOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexImage3DOES'.");
            }
        }
        public static void glCompressedTexImage3DOES(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLsizei @imageSize, const void * @data) => glCompressedTexImage3DOESPtr(@target, @level, @internalformat, @width, @height, @depth, @border, @imageSize, @data);

        internal delegate void glCompressedTexSubImage3DOESFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexSubImage3DOESFunc glCompressedTexSubImage3DOESPtr;
        internal static void loadCompressedTexSubImage3DOES()
        {
            try
            {
                glCompressedTexSubImage3DOESPtr = (glCompressedTexSubImage3DOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexSubImage3DOES"), typeof(glCompressedTexSubImage3DOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexSubImage3DOES'.");
            }
        }
        public static void glCompressedTexSubImage3DOES(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTexSubImage3DOESPtr(@target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @imageSize, @data);

        internal delegate void glFramebufferTexture3DOESFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLint @zoffset);
        internal static glFramebufferTexture3DOESFunc glFramebufferTexture3DOESPtr;
        internal static void loadFramebufferTexture3DOES()
        {
            try
            {
                glFramebufferTexture3DOESPtr = (glFramebufferTexture3DOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture3DOES"), typeof(glFramebufferTexture3DOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture3DOES'.");
            }
        }
        public static void glFramebufferTexture3DOES(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLint @zoffset) => glFramebufferTexture3DOESPtr(@target, @attachment, @textarget, @texture, @level, @zoffset);
        #endregion
    }
}
