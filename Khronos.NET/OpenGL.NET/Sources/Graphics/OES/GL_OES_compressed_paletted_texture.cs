using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_compressed_paletted_texture
    {
        #region Interop
        static GL_OES_compressed_paletted_texture()
        {
            Console.WriteLine("Initalising GL_OES_compressed_paletted_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_PALETTE4_RGB8_OES = 0x8B90;
        public static UInt32 GL_PALETTE4_RGBA8_OES = 0x8B91;
        public static UInt32 GL_PALETTE4_R5_G6_B5_OES = 0x8B92;
        public static UInt32 GL_PALETTE4_RGBA4_OES = 0x8B93;
        public static UInt32 GL_PALETTE4_RGB5_A1_OES = 0x8B94;
        public static UInt32 GL_PALETTE8_RGB8_OES = 0x8B95;
        public static UInt32 GL_PALETTE8_RGBA8_OES = 0x8B96;
        public static UInt32 GL_PALETTE8_R5_G6_B5_OES = 0x8B97;
        public static UInt32 GL_PALETTE8_RGBA4_OES = 0x8B98;
        public static UInt32 GL_PALETTE8_RGB5_A1_OES = 0x8B99;
        #endregion

        #region Commands
        #endregion
    }
}
