using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_texture_view
    {
        #region Interop
        static GL_OES_texture_view()
        {
            Console.WriteLine("Initalising GL_OES_texture_view interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTextureViewOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_VIEW_MIN_LEVEL_OES = 0x82DB;
        public static UInt32 GL_TEXTURE_VIEW_NUM_LEVELS_OES = 0x82DC;
        public static UInt32 GL_TEXTURE_VIEW_MIN_LAYER_OES = 0x82DD;
        public static UInt32 GL_TEXTURE_VIEW_NUM_LAYERS_OES = 0x82DE;
        public static UInt32 GL_TEXTURE_IMMUTABLE_LEVELS = 0x82DF;
        #endregion

        #region Commands
        internal delegate void glTextureViewOESFunc(GLuint @texture, GLenum @target, GLuint @origtexture, GLenum @internalformat, GLuint @minlevel, GLuint @numlevels, GLuint @minlayer, GLuint @numlayers);
        internal static glTextureViewOESFunc glTextureViewOESPtr;
        internal static void loadTextureViewOES()
        {
            try
            {
                glTextureViewOESPtr = (glTextureViewOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureViewOES"), typeof(glTextureViewOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureViewOES'.");
            }
        }
        public static void glTextureViewOES(GLuint @texture, GLenum @target, GLuint @origtexture, GLenum @internalformat, GLuint @minlevel, GLuint @numlevels, GLuint @minlayer, GLuint @numlayers) => glTextureViewOESPtr(@texture, @target, @origtexture, @internalformat, @minlevel, @numlevels, @minlayer, @numlayers);
        #endregion
    }
}
