using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_EGL_image
    {
        #region Interop
        static GL_OES_EGL_image()
        {
            Console.WriteLine("Initalising GL_OES_EGL_image interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadEGLImageTargetTexture2DOES();
            loadEGLImageTargetRenderbufferStorageOES();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glEGLImageTargetTexture2DOESFunc(GLenum @target, GLeglImageOES @image);
        internal static glEGLImageTargetTexture2DOESFunc glEGLImageTargetTexture2DOESPtr;
        internal static void loadEGLImageTargetTexture2DOES()
        {
            try
            {
                glEGLImageTargetTexture2DOESPtr = (glEGLImageTargetTexture2DOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEGLImageTargetTexture2DOES"), typeof(glEGLImageTargetTexture2DOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEGLImageTargetTexture2DOES'.");
            }
        }
        public static void glEGLImageTargetTexture2DOES(GLenum @target, GLeglImageOES @image) => glEGLImageTargetTexture2DOESPtr(@target, @image);

        internal delegate void glEGLImageTargetRenderbufferStorageOESFunc(GLenum @target, GLeglImageOES @image);
        internal static glEGLImageTargetRenderbufferStorageOESFunc glEGLImageTargetRenderbufferStorageOESPtr;
        internal static void loadEGLImageTargetRenderbufferStorageOES()
        {
            try
            {
                glEGLImageTargetRenderbufferStorageOESPtr = (glEGLImageTargetRenderbufferStorageOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEGLImageTargetRenderbufferStorageOES"), typeof(glEGLImageTargetRenderbufferStorageOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEGLImageTargetRenderbufferStorageOES'.");
            }
        }
        public static void glEGLImageTargetRenderbufferStorageOES(GLenum @target, GLeglImageOES @image) => glEGLImageTargetRenderbufferStorageOESPtr(@target, @image);
        #endregion
    }
}
