using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_byte_coordinates
    {
        #region Interop
        static GL_OES_byte_coordinates()
        {
            Console.WriteLine("Initalising GL_OES_byte_coordinates interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMultiTexCoord1bOES();
            loadMultiTexCoord1bvOES();
            loadMultiTexCoord2bOES();
            loadMultiTexCoord2bvOES();
            loadMultiTexCoord3bOES();
            loadMultiTexCoord3bvOES();
            loadMultiTexCoord4bOES();
            loadMultiTexCoord4bvOES();
            loadTexCoord1bOES();
            loadTexCoord1bvOES();
            loadTexCoord2bOES();
            loadTexCoord2bvOES();
            loadTexCoord3bOES();
            loadTexCoord3bvOES();
            loadTexCoord4bOES();
            loadTexCoord4bvOES();
            loadVertex2bOES();
            loadVertex2bvOES();
            loadVertex3bOES();
            loadVertex3bvOES();
            loadVertex4bOES();
            loadVertex4bvOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BYTE = 0x1400;
        #endregion

        #region Commands
        internal delegate void glMultiTexCoord1bOESFunc(GLenum @texture, GLbyte @s);
        internal static glMultiTexCoord1bOESFunc glMultiTexCoord1bOESPtr;
        internal static void loadMultiTexCoord1bOES()
        {
            try
            {
                glMultiTexCoord1bOESPtr = (glMultiTexCoord1bOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1bOES"), typeof(glMultiTexCoord1bOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1bOES'.");
            }
        }
        public static void glMultiTexCoord1bOES(GLenum @texture, GLbyte @s) => glMultiTexCoord1bOESPtr(@texture, @s);

        internal delegate void glMultiTexCoord1bvOESFunc(GLenum @texture, const GLbyte * @coords);
        internal static glMultiTexCoord1bvOESFunc glMultiTexCoord1bvOESPtr;
        internal static void loadMultiTexCoord1bvOES()
        {
            try
            {
                glMultiTexCoord1bvOESPtr = (glMultiTexCoord1bvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1bvOES"), typeof(glMultiTexCoord1bvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1bvOES'.");
            }
        }
        public static void glMultiTexCoord1bvOES(GLenum @texture, const GLbyte * @coords) => glMultiTexCoord1bvOESPtr(@texture, @coords);

        internal delegate void glMultiTexCoord2bOESFunc(GLenum @texture, GLbyte @s, GLbyte @t);
        internal static glMultiTexCoord2bOESFunc glMultiTexCoord2bOESPtr;
        internal static void loadMultiTexCoord2bOES()
        {
            try
            {
                glMultiTexCoord2bOESPtr = (glMultiTexCoord2bOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2bOES"), typeof(glMultiTexCoord2bOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2bOES'.");
            }
        }
        public static void glMultiTexCoord2bOES(GLenum @texture, GLbyte @s, GLbyte @t) => glMultiTexCoord2bOESPtr(@texture, @s, @t);

        internal delegate void glMultiTexCoord2bvOESFunc(GLenum @texture, const GLbyte * @coords);
        internal static glMultiTexCoord2bvOESFunc glMultiTexCoord2bvOESPtr;
        internal static void loadMultiTexCoord2bvOES()
        {
            try
            {
                glMultiTexCoord2bvOESPtr = (glMultiTexCoord2bvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2bvOES"), typeof(glMultiTexCoord2bvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2bvOES'.");
            }
        }
        public static void glMultiTexCoord2bvOES(GLenum @texture, const GLbyte * @coords) => glMultiTexCoord2bvOESPtr(@texture, @coords);

        internal delegate void glMultiTexCoord3bOESFunc(GLenum @texture, GLbyte @s, GLbyte @t, GLbyte @r);
        internal static glMultiTexCoord3bOESFunc glMultiTexCoord3bOESPtr;
        internal static void loadMultiTexCoord3bOES()
        {
            try
            {
                glMultiTexCoord3bOESPtr = (glMultiTexCoord3bOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3bOES"), typeof(glMultiTexCoord3bOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3bOES'.");
            }
        }
        public static void glMultiTexCoord3bOES(GLenum @texture, GLbyte @s, GLbyte @t, GLbyte @r) => glMultiTexCoord3bOESPtr(@texture, @s, @t, @r);

        internal delegate void glMultiTexCoord3bvOESFunc(GLenum @texture, const GLbyte * @coords);
        internal static glMultiTexCoord3bvOESFunc glMultiTexCoord3bvOESPtr;
        internal static void loadMultiTexCoord3bvOES()
        {
            try
            {
                glMultiTexCoord3bvOESPtr = (glMultiTexCoord3bvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3bvOES"), typeof(glMultiTexCoord3bvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3bvOES'.");
            }
        }
        public static void glMultiTexCoord3bvOES(GLenum @texture, const GLbyte * @coords) => glMultiTexCoord3bvOESPtr(@texture, @coords);

        internal delegate void glMultiTexCoord4bOESFunc(GLenum @texture, GLbyte @s, GLbyte @t, GLbyte @r, GLbyte @q);
        internal static glMultiTexCoord4bOESFunc glMultiTexCoord4bOESPtr;
        internal static void loadMultiTexCoord4bOES()
        {
            try
            {
                glMultiTexCoord4bOESPtr = (glMultiTexCoord4bOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4bOES"), typeof(glMultiTexCoord4bOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4bOES'.");
            }
        }
        public static void glMultiTexCoord4bOES(GLenum @texture, GLbyte @s, GLbyte @t, GLbyte @r, GLbyte @q) => glMultiTexCoord4bOESPtr(@texture, @s, @t, @r, @q);

        internal delegate void glMultiTexCoord4bvOESFunc(GLenum @texture, const GLbyte * @coords);
        internal static glMultiTexCoord4bvOESFunc glMultiTexCoord4bvOESPtr;
        internal static void loadMultiTexCoord4bvOES()
        {
            try
            {
                glMultiTexCoord4bvOESPtr = (glMultiTexCoord4bvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4bvOES"), typeof(glMultiTexCoord4bvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4bvOES'.");
            }
        }
        public static void glMultiTexCoord4bvOES(GLenum @texture, const GLbyte * @coords) => glMultiTexCoord4bvOESPtr(@texture, @coords);

        internal delegate void glTexCoord1bOESFunc(GLbyte @s);
        internal static glTexCoord1bOESFunc glTexCoord1bOESPtr;
        internal static void loadTexCoord1bOES()
        {
            try
            {
                glTexCoord1bOESPtr = (glTexCoord1bOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1bOES"), typeof(glTexCoord1bOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1bOES'.");
            }
        }
        public static void glTexCoord1bOES(GLbyte @s) => glTexCoord1bOESPtr(@s);

        internal delegate void glTexCoord1bvOESFunc(const GLbyte * @coords);
        internal static glTexCoord1bvOESFunc glTexCoord1bvOESPtr;
        internal static void loadTexCoord1bvOES()
        {
            try
            {
                glTexCoord1bvOESPtr = (glTexCoord1bvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1bvOES"), typeof(glTexCoord1bvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1bvOES'.");
            }
        }
        public static void glTexCoord1bvOES(const GLbyte * @coords) => glTexCoord1bvOESPtr(@coords);

        internal delegate void glTexCoord2bOESFunc(GLbyte @s, GLbyte @t);
        internal static glTexCoord2bOESFunc glTexCoord2bOESPtr;
        internal static void loadTexCoord2bOES()
        {
            try
            {
                glTexCoord2bOESPtr = (glTexCoord2bOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2bOES"), typeof(glTexCoord2bOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2bOES'.");
            }
        }
        public static void glTexCoord2bOES(GLbyte @s, GLbyte @t) => glTexCoord2bOESPtr(@s, @t);

        internal delegate void glTexCoord2bvOESFunc(const GLbyte * @coords);
        internal static glTexCoord2bvOESFunc glTexCoord2bvOESPtr;
        internal static void loadTexCoord2bvOES()
        {
            try
            {
                glTexCoord2bvOESPtr = (glTexCoord2bvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2bvOES"), typeof(glTexCoord2bvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2bvOES'.");
            }
        }
        public static void glTexCoord2bvOES(const GLbyte * @coords) => glTexCoord2bvOESPtr(@coords);

        internal delegate void glTexCoord3bOESFunc(GLbyte @s, GLbyte @t, GLbyte @r);
        internal static glTexCoord3bOESFunc glTexCoord3bOESPtr;
        internal static void loadTexCoord3bOES()
        {
            try
            {
                glTexCoord3bOESPtr = (glTexCoord3bOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3bOES"), typeof(glTexCoord3bOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3bOES'.");
            }
        }
        public static void glTexCoord3bOES(GLbyte @s, GLbyte @t, GLbyte @r) => glTexCoord3bOESPtr(@s, @t, @r);

        internal delegate void glTexCoord3bvOESFunc(const GLbyte * @coords);
        internal static glTexCoord3bvOESFunc glTexCoord3bvOESPtr;
        internal static void loadTexCoord3bvOES()
        {
            try
            {
                glTexCoord3bvOESPtr = (glTexCoord3bvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3bvOES"), typeof(glTexCoord3bvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3bvOES'.");
            }
        }
        public static void glTexCoord3bvOES(const GLbyte * @coords) => glTexCoord3bvOESPtr(@coords);

        internal delegate void glTexCoord4bOESFunc(GLbyte @s, GLbyte @t, GLbyte @r, GLbyte @q);
        internal static glTexCoord4bOESFunc glTexCoord4bOESPtr;
        internal static void loadTexCoord4bOES()
        {
            try
            {
                glTexCoord4bOESPtr = (glTexCoord4bOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4bOES"), typeof(glTexCoord4bOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4bOES'.");
            }
        }
        public static void glTexCoord4bOES(GLbyte @s, GLbyte @t, GLbyte @r, GLbyte @q) => glTexCoord4bOESPtr(@s, @t, @r, @q);

        internal delegate void glTexCoord4bvOESFunc(const GLbyte * @coords);
        internal static glTexCoord4bvOESFunc glTexCoord4bvOESPtr;
        internal static void loadTexCoord4bvOES()
        {
            try
            {
                glTexCoord4bvOESPtr = (glTexCoord4bvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4bvOES"), typeof(glTexCoord4bvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4bvOES'.");
            }
        }
        public static void glTexCoord4bvOES(const GLbyte * @coords) => glTexCoord4bvOESPtr(@coords);

        internal delegate void glVertex2bOESFunc(GLbyte @x, GLbyte @y);
        internal static glVertex2bOESFunc glVertex2bOESPtr;
        internal static void loadVertex2bOES()
        {
            try
            {
                glVertex2bOESPtr = (glVertex2bOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2bOES"), typeof(glVertex2bOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2bOES'.");
            }
        }
        public static void glVertex2bOES(GLbyte @x, GLbyte @y) => glVertex2bOESPtr(@x, @y);

        internal delegate void glVertex2bvOESFunc(const GLbyte * @coords);
        internal static glVertex2bvOESFunc glVertex2bvOESPtr;
        internal static void loadVertex2bvOES()
        {
            try
            {
                glVertex2bvOESPtr = (glVertex2bvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2bvOES"), typeof(glVertex2bvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2bvOES'.");
            }
        }
        public static void glVertex2bvOES(const GLbyte * @coords) => glVertex2bvOESPtr(@coords);

        internal delegate void glVertex3bOESFunc(GLbyte @x, GLbyte @y, GLbyte @z);
        internal static glVertex3bOESFunc glVertex3bOESPtr;
        internal static void loadVertex3bOES()
        {
            try
            {
                glVertex3bOESPtr = (glVertex3bOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3bOES"), typeof(glVertex3bOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3bOES'.");
            }
        }
        public static void glVertex3bOES(GLbyte @x, GLbyte @y, GLbyte @z) => glVertex3bOESPtr(@x, @y, @z);

        internal delegate void glVertex3bvOESFunc(const GLbyte * @coords);
        internal static glVertex3bvOESFunc glVertex3bvOESPtr;
        internal static void loadVertex3bvOES()
        {
            try
            {
                glVertex3bvOESPtr = (glVertex3bvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3bvOES"), typeof(glVertex3bvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3bvOES'.");
            }
        }
        public static void glVertex3bvOES(const GLbyte * @coords) => glVertex3bvOESPtr(@coords);

        internal delegate void glVertex4bOESFunc(GLbyte @x, GLbyte @y, GLbyte @z, GLbyte @w);
        internal static glVertex4bOESFunc glVertex4bOESPtr;
        internal static void loadVertex4bOES()
        {
            try
            {
                glVertex4bOESPtr = (glVertex4bOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4bOES"), typeof(glVertex4bOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4bOES'.");
            }
        }
        public static void glVertex4bOES(GLbyte @x, GLbyte @y, GLbyte @z, GLbyte @w) => glVertex4bOESPtr(@x, @y, @z, @w);

        internal delegate void glVertex4bvOESFunc(const GLbyte * @coords);
        internal static glVertex4bvOESFunc glVertex4bvOESPtr;
        internal static void loadVertex4bvOES()
        {
            try
            {
                glVertex4bvOESPtr = (glVertex4bvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4bvOES"), typeof(glVertex4bvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4bvOES'.");
            }
        }
        public static void glVertex4bvOES(const GLbyte * @coords) => glVertex4bvOESPtr(@coords);
        #endregion
    }
}
