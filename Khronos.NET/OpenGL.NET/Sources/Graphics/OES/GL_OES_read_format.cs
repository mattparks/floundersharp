using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_read_format
    {
        #region Interop
        static GL_OES_read_format()
        {
            Console.WriteLine("Initalising GL_OES_read_format interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_IMPLEMENTATION_COLOR_READ_TYPE_OES = 0x8B9A;
        public static UInt32 GL_IMPLEMENTATION_COLOR_READ_FORMAT_OES = 0x8B9B;
        #endregion

        #region Commands
        #endregion
    }
}
