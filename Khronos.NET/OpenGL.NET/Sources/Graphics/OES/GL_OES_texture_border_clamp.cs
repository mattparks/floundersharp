using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_texture_border_clamp
    {
        #region Interop
        static GL_OES_texture_border_clamp()
        {
            Console.WriteLine("Initalising GL_OES_texture_border_clamp interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexParameterIivOES();
            loadTexParameterIuivOES();
            loadGetTexParameterIivOES();
            loadGetTexParameterIuivOES();
            loadSamplerParameterIivOES();
            loadSamplerParameterIuivOES();
            loadGetSamplerParameterIivOES();
            loadGetSamplerParameterIuivOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_BORDER_COLOR_OES = 0x1004;
        public static UInt32 GL_CLAMP_TO_BORDER_OES = 0x812D;
        #endregion

        #region Commands
        internal delegate void glTexParameterIivOESFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glTexParameterIivOESFunc glTexParameterIivOESPtr;
        internal static void loadTexParameterIivOES()
        {
            try
            {
                glTexParameterIivOESPtr = (glTexParameterIivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterIivOES"), typeof(glTexParameterIivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterIivOES'.");
            }
        }
        public static void glTexParameterIivOES(GLenum @target, GLenum @pname, const GLint * @params) => glTexParameterIivOESPtr(@target, @pname, @params);

        internal delegate void glTexParameterIuivOESFunc(GLenum @target, GLenum @pname, const GLuint * @params);
        internal static glTexParameterIuivOESFunc glTexParameterIuivOESPtr;
        internal static void loadTexParameterIuivOES()
        {
            try
            {
                glTexParameterIuivOESPtr = (glTexParameterIuivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterIuivOES"), typeof(glTexParameterIuivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterIuivOES'.");
            }
        }
        public static void glTexParameterIuivOES(GLenum @target, GLenum @pname, const GLuint * @params) => glTexParameterIuivOESPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterIivOESFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetTexParameterIivOESFunc glGetTexParameterIivOESPtr;
        internal static void loadGetTexParameterIivOES()
        {
            try
            {
                glGetTexParameterIivOESPtr = (glGetTexParameterIivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterIivOES"), typeof(glGetTexParameterIivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterIivOES'.");
            }
        }
        public static void glGetTexParameterIivOES(GLenum @target, GLenum @pname, GLint * @params) => glGetTexParameterIivOESPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterIuivOESFunc(GLenum @target, GLenum @pname, GLuint * @params);
        internal static glGetTexParameterIuivOESFunc glGetTexParameterIuivOESPtr;
        internal static void loadGetTexParameterIuivOES()
        {
            try
            {
                glGetTexParameterIuivOESPtr = (glGetTexParameterIuivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterIuivOES"), typeof(glGetTexParameterIuivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterIuivOES'.");
            }
        }
        public static void glGetTexParameterIuivOES(GLenum @target, GLenum @pname, GLuint * @params) => glGetTexParameterIuivOESPtr(@target, @pname, @params);

        internal delegate void glSamplerParameterIivOESFunc(GLuint @sampler, GLenum @pname, const GLint * @param);
        internal static glSamplerParameterIivOESFunc glSamplerParameterIivOESPtr;
        internal static void loadSamplerParameterIivOES()
        {
            try
            {
                glSamplerParameterIivOESPtr = (glSamplerParameterIivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterIivOES"), typeof(glSamplerParameterIivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterIivOES'.");
            }
        }
        public static void glSamplerParameterIivOES(GLuint @sampler, GLenum @pname, const GLint * @param) => glSamplerParameterIivOESPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterIuivOESFunc(GLuint @sampler, GLenum @pname, const GLuint * @param);
        internal static glSamplerParameterIuivOESFunc glSamplerParameterIuivOESPtr;
        internal static void loadSamplerParameterIuivOES()
        {
            try
            {
                glSamplerParameterIuivOESPtr = (glSamplerParameterIuivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterIuivOES"), typeof(glSamplerParameterIuivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterIuivOES'.");
            }
        }
        public static void glSamplerParameterIuivOES(GLuint @sampler, GLenum @pname, const GLuint * @param) => glSamplerParameterIuivOESPtr(@sampler, @pname, @param);

        internal delegate void glGetSamplerParameterIivOESFunc(GLuint @sampler, GLenum @pname, GLint * @params);
        internal static glGetSamplerParameterIivOESFunc glGetSamplerParameterIivOESPtr;
        internal static void loadGetSamplerParameterIivOES()
        {
            try
            {
                glGetSamplerParameterIivOESPtr = (glGetSamplerParameterIivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterIivOES"), typeof(glGetSamplerParameterIivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterIivOES'.");
            }
        }
        public static void glGetSamplerParameterIivOES(GLuint @sampler, GLenum @pname, GLint * @params) => glGetSamplerParameterIivOESPtr(@sampler, @pname, @params);

        internal delegate void glGetSamplerParameterIuivOESFunc(GLuint @sampler, GLenum @pname, GLuint * @params);
        internal static glGetSamplerParameterIuivOESFunc glGetSamplerParameterIuivOESPtr;
        internal static void loadGetSamplerParameterIuivOES()
        {
            try
            {
                glGetSamplerParameterIuivOESPtr = (glGetSamplerParameterIuivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterIuivOES"), typeof(glGetSamplerParameterIuivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterIuivOES'.");
            }
        }
        public static void glGetSamplerParameterIuivOES(GLuint @sampler, GLenum @pname, GLuint * @params) => glGetSamplerParameterIuivOESPtr(@sampler, @pname, @params);
        #endregion
    }
}
