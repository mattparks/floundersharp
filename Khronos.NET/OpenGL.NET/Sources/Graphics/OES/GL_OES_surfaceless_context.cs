using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_surfaceless_context
    {
        #region Interop
        static GL_OES_surfaceless_context()
        {
            Console.WriteLine("Initalising GL_OES_surfaceless_context interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAMEBUFFER_UNDEFINED_OES = 0x8219;
        #endregion

        #region Commands
        #endregion
    }
}
