using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_matrix_palette
    {
        #region Interop
        static GL_OES_matrix_palette()
        {
            Console.WriteLine("Initalising GL_OES_matrix_palette interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCurrentPaletteMatrixOES();
            loadLoadPaletteFromModelViewMatrixOES();
            loadMatrixIndexPointerOES();
            loadWeightPointerOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_VERTEX_UNITS_OES = 0x86A4;
        public static UInt32 GL_MAX_PALETTE_MATRICES_OES = 0x8842;
        public static UInt32 GL_MATRIX_PALETTE_OES = 0x8840;
        public static UInt32 GL_MATRIX_INDEX_ARRAY_OES = 0x8844;
        public static UInt32 GL_WEIGHT_ARRAY_OES = 0x86AD;
        public static UInt32 GL_CURRENT_PALETTE_MATRIX_OES = 0x8843;
        public static UInt32 GL_MATRIX_INDEX_ARRAY_SIZE_OES = 0x8846;
        public static UInt32 GL_MATRIX_INDEX_ARRAY_TYPE_OES = 0x8847;
        public static UInt32 GL_MATRIX_INDEX_ARRAY_STRIDE_OES = 0x8848;
        public static UInt32 GL_MATRIX_INDEX_ARRAY_POINTER_OES = 0x8849;
        public static UInt32 GL_MATRIX_INDEX_ARRAY_BUFFER_BINDING_OES = 0x8B9E;
        public static UInt32 GL_WEIGHT_ARRAY_SIZE_OES = 0x86AB;
        public static UInt32 GL_WEIGHT_ARRAY_TYPE_OES = 0x86A9;
        public static UInt32 GL_WEIGHT_ARRAY_STRIDE_OES = 0x86AA;
        public static UInt32 GL_WEIGHT_ARRAY_POINTER_OES = 0x86AC;
        public static UInt32 GL_WEIGHT_ARRAY_BUFFER_BINDING_OES = 0x889E;
        #endregion

        #region Commands
        internal delegate void glCurrentPaletteMatrixOESFunc(GLuint @matrixpaletteindex);
        internal static glCurrentPaletteMatrixOESFunc glCurrentPaletteMatrixOESPtr;
        internal static void loadCurrentPaletteMatrixOES()
        {
            try
            {
                glCurrentPaletteMatrixOESPtr = (glCurrentPaletteMatrixOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCurrentPaletteMatrixOES"), typeof(glCurrentPaletteMatrixOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCurrentPaletteMatrixOES'.");
            }
        }
        public static void glCurrentPaletteMatrixOES(GLuint @matrixpaletteindex) => glCurrentPaletteMatrixOESPtr(@matrixpaletteindex);

        internal delegate void glLoadPaletteFromModelViewMatrixOESFunc();
        internal static glLoadPaletteFromModelViewMatrixOESFunc glLoadPaletteFromModelViewMatrixOESPtr;
        internal static void loadLoadPaletteFromModelViewMatrixOES()
        {
            try
            {
                glLoadPaletteFromModelViewMatrixOESPtr = (glLoadPaletteFromModelViewMatrixOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadPaletteFromModelViewMatrixOES"), typeof(glLoadPaletteFromModelViewMatrixOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadPaletteFromModelViewMatrixOES'.");
            }
        }
        public static void glLoadPaletteFromModelViewMatrixOES() => glLoadPaletteFromModelViewMatrixOESPtr();

        internal delegate void glMatrixIndexPointerOESFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glMatrixIndexPointerOESFunc glMatrixIndexPointerOESPtr;
        internal static void loadMatrixIndexPointerOES()
        {
            try
            {
                glMatrixIndexPointerOESPtr = (glMatrixIndexPointerOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixIndexPointerOES"), typeof(glMatrixIndexPointerOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixIndexPointerOES'.");
            }
        }
        public static void glMatrixIndexPointerOES(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glMatrixIndexPointerOESPtr(@size, @type, @stride, @pointer);

        internal delegate void glWeightPointerOESFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glWeightPointerOESFunc glWeightPointerOESPtr;
        internal static void loadWeightPointerOES()
        {
            try
            {
                glWeightPointerOESPtr = (glWeightPointerOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWeightPointerOES"), typeof(glWeightPointerOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWeightPointerOES'.");
            }
        }
        public static void glWeightPointerOES(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glWeightPointerOESPtr(@size, @type, @stride, @pointer);
        #endregion
    }
}
