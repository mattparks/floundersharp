using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_fixed_point
    {
        #region Interop
        static GL_OES_fixed_point()
        {
            Console.WriteLine("Initalising GL_OES_fixed_point interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadAlphaFuncxOES();
            loadClearColorxOES();
            loadClearDepthxOES();
            loadClipPlanexOES();
            loadColor4xOES();
            loadDepthRangexOES();
            loadFogxOES();
            loadFogxvOES();
            loadFrustumxOES();
            loadGetClipPlanexOES();
            loadGetFixedvOES();
            loadGetTexEnvxvOES();
            loadGetTexParameterxvOES();
            loadLightModelxOES();
            loadLightModelxvOES();
            loadLightxOES();
            loadLightxvOES();
            loadLineWidthxOES();
            loadLoadMatrixxOES();
            loadMaterialxOES();
            loadMaterialxvOES();
            loadMultMatrixxOES();
            loadMultiTexCoord4xOES();
            loadNormal3xOES();
            loadOrthoxOES();
            loadPointParameterxvOES();
            loadPointSizexOES();
            loadPolygonOffsetxOES();
            loadRotatexOES();
            loadScalexOES();
            loadTexEnvxOES();
            loadTexEnvxvOES();
            loadTexParameterxOES();
            loadTexParameterxvOES();
            loadTranslatexOES();
            loadGetLightxvOES();
            loadGetMaterialxvOES();
            loadPointParameterxOES();
            loadSampleCoveragexOES();
            loadAccumxOES();
            loadBitmapxOES();
            loadBlendColorxOES();
            loadClearAccumxOES();
            loadColor3xOES();
            loadColor3xvOES();
            loadColor4xvOES();
            loadConvolutionParameterxOES();
            loadConvolutionParameterxvOES();
            loadEvalCoord1xOES();
            loadEvalCoord1xvOES();
            loadEvalCoord2xOES();
            loadEvalCoord2xvOES();
            loadFeedbackBufferxOES();
            loadGetConvolutionParameterxvOES();
            loadGetHistogramParameterxvOES();
            loadGetLightxOES();
            loadGetMapxvOES();
            loadGetMaterialxOES();
            loadGetPixelMapxv();
            loadGetTexGenxvOES();
            loadGetTexLevelParameterxvOES();
            loadIndexxOES();
            loadIndexxvOES();
            loadLoadTransposeMatrixxOES();
            loadMap1xOES();
            loadMap2xOES();
            loadMapGrid1xOES();
            loadMapGrid2xOES();
            loadMultTransposeMatrixxOES();
            loadMultiTexCoord1xOES();
            loadMultiTexCoord1xvOES();
            loadMultiTexCoord2xOES();
            loadMultiTexCoord2xvOES();
            loadMultiTexCoord3xOES();
            loadMultiTexCoord3xvOES();
            loadMultiTexCoord4xvOES();
            loadNormal3xvOES();
            loadPassThroughxOES();
            loadPixelMapx();
            loadPixelStorex();
            loadPixelTransferxOES();
            loadPixelZoomxOES();
            loadPrioritizeTexturesxOES();
            loadRasterPos2xOES();
            loadRasterPos2xvOES();
            loadRasterPos3xOES();
            loadRasterPos3xvOES();
            loadRasterPos4xOES();
            loadRasterPos4xvOES();
            loadRectxOES();
            loadRectxvOES();
            loadTexCoord1xOES();
            loadTexCoord1xvOES();
            loadTexCoord2xOES();
            loadTexCoord2xvOES();
            loadTexCoord3xOES();
            loadTexCoord3xvOES();
            loadTexCoord4xOES();
            loadTexCoord4xvOES();
            loadTexGenxOES();
            loadTexGenxvOES();
            loadVertex2xOES();
            loadVertex2xvOES();
            loadVertex3xOES();
            loadVertex3xvOES();
            loadVertex4xOES();
            loadVertex4xvOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FIXED_OES = 0x140C;
        #endregion

        #region Commands
        internal delegate void glAlphaFuncxOESFunc(GLenum @func, GLfixed @ref);
        internal static glAlphaFuncxOESFunc glAlphaFuncxOESPtr;
        internal static void loadAlphaFuncxOES()
        {
            try
            {
                glAlphaFuncxOESPtr = (glAlphaFuncxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAlphaFuncxOES"), typeof(glAlphaFuncxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAlphaFuncxOES'.");
            }
        }
        public static void glAlphaFuncxOES(GLenum @func, GLfixed @ref) => glAlphaFuncxOESPtr(@func, @ref);

        internal delegate void glClearColorxOESFunc(GLfixed @red, GLfixed @green, GLfixed @blue, GLfixed @alpha);
        internal static glClearColorxOESFunc glClearColorxOESPtr;
        internal static void loadClearColorxOES()
        {
            try
            {
                glClearColorxOESPtr = (glClearColorxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearColorxOES"), typeof(glClearColorxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearColorxOES'.");
            }
        }
        public static void glClearColorxOES(GLfixed @red, GLfixed @green, GLfixed @blue, GLfixed @alpha) => glClearColorxOESPtr(@red, @green, @blue, @alpha);

        internal delegate void glClearDepthxOESFunc(GLfixed @depth);
        internal static glClearDepthxOESFunc glClearDepthxOESPtr;
        internal static void loadClearDepthxOES()
        {
            try
            {
                glClearDepthxOESPtr = (glClearDepthxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearDepthxOES"), typeof(glClearDepthxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearDepthxOES'.");
            }
        }
        public static void glClearDepthxOES(GLfixed @depth) => glClearDepthxOESPtr(@depth);

        internal delegate void glClipPlanexOESFunc(GLenum @plane, const GLfixed * @equation);
        internal static glClipPlanexOESFunc glClipPlanexOESPtr;
        internal static void loadClipPlanexOES()
        {
            try
            {
                glClipPlanexOESPtr = (glClipPlanexOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClipPlanexOES"), typeof(glClipPlanexOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClipPlanexOES'.");
            }
        }
        public static void glClipPlanexOES(GLenum @plane, const GLfixed * @equation) => glClipPlanexOESPtr(@plane, @equation);

        internal delegate void glColor4xOESFunc(GLfixed @red, GLfixed @green, GLfixed @blue, GLfixed @alpha);
        internal static glColor4xOESFunc glColor4xOESPtr;
        internal static void loadColor4xOES()
        {
            try
            {
                glColor4xOESPtr = (glColor4xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4xOES"), typeof(glColor4xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4xOES'.");
            }
        }
        public static void glColor4xOES(GLfixed @red, GLfixed @green, GLfixed @blue, GLfixed @alpha) => glColor4xOESPtr(@red, @green, @blue, @alpha);

        internal delegate void glDepthRangexOESFunc(GLfixed @n, GLfixed @f);
        internal static glDepthRangexOESFunc glDepthRangexOESPtr;
        internal static void loadDepthRangexOES()
        {
            try
            {
                glDepthRangexOESPtr = (glDepthRangexOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangexOES"), typeof(glDepthRangexOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangexOES'.");
            }
        }
        public static void glDepthRangexOES(GLfixed @n, GLfixed @f) => glDepthRangexOESPtr(@n, @f);

        internal delegate void glFogxOESFunc(GLenum @pname, GLfixed @param);
        internal static glFogxOESFunc glFogxOESPtr;
        internal static void loadFogxOES()
        {
            try
            {
                glFogxOESPtr = (glFogxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogxOES"), typeof(glFogxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogxOES'.");
            }
        }
        public static void glFogxOES(GLenum @pname, GLfixed @param) => glFogxOESPtr(@pname, @param);

        internal delegate void glFogxvOESFunc(GLenum @pname, const GLfixed * @param);
        internal static glFogxvOESFunc glFogxvOESPtr;
        internal static void loadFogxvOES()
        {
            try
            {
                glFogxvOESPtr = (glFogxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogxvOES"), typeof(glFogxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogxvOES'.");
            }
        }
        public static void glFogxvOES(GLenum @pname, const GLfixed * @param) => glFogxvOESPtr(@pname, @param);

        internal delegate void glFrustumxOESFunc(GLfixed @l, GLfixed @r, GLfixed @b, GLfixed @t, GLfixed @n, GLfixed @f);
        internal static glFrustumxOESFunc glFrustumxOESPtr;
        internal static void loadFrustumxOES()
        {
            try
            {
                glFrustumxOESPtr = (glFrustumxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFrustumxOES"), typeof(glFrustumxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFrustumxOES'.");
            }
        }
        public static void glFrustumxOES(GLfixed @l, GLfixed @r, GLfixed @b, GLfixed @t, GLfixed @n, GLfixed @f) => glFrustumxOESPtr(@l, @r, @b, @t, @n, @f);

        internal delegate void glGetClipPlanexOESFunc(GLenum @plane, GLfixed * @equation);
        internal static glGetClipPlanexOESFunc glGetClipPlanexOESPtr;
        internal static void loadGetClipPlanexOES()
        {
            try
            {
                glGetClipPlanexOESPtr = (glGetClipPlanexOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetClipPlanexOES"), typeof(glGetClipPlanexOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetClipPlanexOES'.");
            }
        }
        public static void glGetClipPlanexOES(GLenum @plane, GLfixed * @equation) => glGetClipPlanexOESPtr(@plane, @equation);

        internal delegate void glGetFixedvOESFunc(GLenum @pname, GLfixed * @params);
        internal static glGetFixedvOESFunc glGetFixedvOESPtr;
        internal static void loadGetFixedvOES()
        {
            try
            {
                glGetFixedvOESPtr = (glGetFixedvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFixedvOES"), typeof(glGetFixedvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFixedvOES'.");
            }
        }
        public static void glGetFixedvOES(GLenum @pname, GLfixed * @params) => glGetFixedvOESPtr(@pname, @params);

        internal delegate void glGetTexEnvxvOESFunc(GLenum @target, GLenum @pname, GLfixed * @params);
        internal static glGetTexEnvxvOESFunc glGetTexEnvxvOESPtr;
        internal static void loadGetTexEnvxvOES()
        {
            try
            {
                glGetTexEnvxvOESPtr = (glGetTexEnvxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexEnvxvOES"), typeof(glGetTexEnvxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexEnvxvOES'.");
            }
        }
        public static void glGetTexEnvxvOES(GLenum @target, GLenum @pname, GLfixed * @params) => glGetTexEnvxvOESPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterxvOESFunc(GLenum @target, GLenum @pname, GLfixed * @params);
        internal static glGetTexParameterxvOESFunc glGetTexParameterxvOESPtr;
        internal static void loadGetTexParameterxvOES()
        {
            try
            {
                glGetTexParameterxvOESPtr = (glGetTexParameterxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterxvOES"), typeof(glGetTexParameterxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterxvOES'.");
            }
        }
        public static void glGetTexParameterxvOES(GLenum @target, GLenum @pname, GLfixed * @params) => glGetTexParameterxvOESPtr(@target, @pname, @params);

        internal delegate void glLightModelxOESFunc(GLenum @pname, GLfixed @param);
        internal static glLightModelxOESFunc glLightModelxOESPtr;
        internal static void loadLightModelxOES()
        {
            try
            {
                glLightModelxOESPtr = (glLightModelxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightModelxOES"), typeof(glLightModelxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightModelxOES'.");
            }
        }
        public static void glLightModelxOES(GLenum @pname, GLfixed @param) => glLightModelxOESPtr(@pname, @param);

        internal delegate void glLightModelxvOESFunc(GLenum @pname, const GLfixed * @param);
        internal static glLightModelxvOESFunc glLightModelxvOESPtr;
        internal static void loadLightModelxvOES()
        {
            try
            {
                glLightModelxvOESPtr = (glLightModelxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightModelxvOES"), typeof(glLightModelxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightModelxvOES'.");
            }
        }
        public static void glLightModelxvOES(GLenum @pname, const GLfixed * @param) => glLightModelxvOESPtr(@pname, @param);

        internal delegate void glLightxOESFunc(GLenum @light, GLenum @pname, GLfixed @param);
        internal static glLightxOESFunc glLightxOESPtr;
        internal static void loadLightxOES()
        {
            try
            {
                glLightxOESPtr = (glLightxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightxOES"), typeof(glLightxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightxOES'.");
            }
        }
        public static void glLightxOES(GLenum @light, GLenum @pname, GLfixed @param) => glLightxOESPtr(@light, @pname, @param);

        internal delegate void glLightxvOESFunc(GLenum @light, GLenum @pname, const GLfixed * @params);
        internal static glLightxvOESFunc glLightxvOESPtr;
        internal static void loadLightxvOES()
        {
            try
            {
                glLightxvOESPtr = (glLightxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightxvOES"), typeof(glLightxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightxvOES'.");
            }
        }
        public static void glLightxvOES(GLenum @light, GLenum @pname, const GLfixed * @params) => glLightxvOESPtr(@light, @pname, @params);

        internal delegate void glLineWidthxOESFunc(GLfixed @width);
        internal static glLineWidthxOESFunc glLineWidthxOESPtr;
        internal static void loadLineWidthxOES()
        {
            try
            {
                glLineWidthxOESPtr = (glLineWidthxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLineWidthxOES"), typeof(glLineWidthxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLineWidthxOES'.");
            }
        }
        public static void glLineWidthxOES(GLfixed @width) => glLineWidthxOESPtr(@width);

        internal delegate void glLoadMatrixxOESFunc(const GLfixed * @m);
        internal static glLoadMatrixxOESFunc glLoadMatrixxOESPtr;
        internal static void loadLoadMatrixxOES()
        {
            try
            {
                glLoadMatrixxOESPtr = (glLoadMatrixxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadMatrixxOES"), typeof(glLoadMatrixxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadMatrixxOES'.");
            }
        }
        public static void glLoadMatrixxOES(const GLfixed * @m) => glLoadMatrixxOESPtr(@m);

        internal delegate void glMaterialxOESFunc(GLenum @face, GLenum @pname, GLfixed @param);
        internal static glMaterialxOESFunc glMaterialxOESPtr;
        internal static void loadMaterialxOES()
        {
            try
            {
                glMaterialxOESPtr = (glMaterialxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMaterialxOES"), typeof(glMaterialxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMaterialxOES'.");
            }
        }
        public static void glMaterialxOES(GLenum @face, GLenum @pname, GLfixed @param) => glMaterialxOESPtr(@face, @pname, @param);

        internal delegate void glMaterialxvOESFunc(GLenum @face, GLenum @pname, const GLfixed * @param);
        internal static glMaterialxvOESFunc glMaterialxvOESPtr;
        internal static void loadMaterialxvOES()
        {
            try
            {
                glMaterialxvOESPtr = (glMaterialxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMaterialxvOES"), typeof(glMaterialxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMaterialxvOES'.");
            }
        }
        public static void glMaterialxvOES(GLenum @face, GLenum @pname, const GLfixed * @param) => glMaterialxvOESPtr(@face, @pname, @param);

        internal delegate void glMultMatrixxOESFunc(const GLfixed * @m);
        internal static glMultMatrixxOESFunc glMultMatrixxOESPtr;
        internal static void loadMultMatrixxOES()
        {
            try
            {
                glMultMatrixxOESPtr = (glMultMatrixxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultMatrixxOES"), typeof(glMultMatrixxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultMatrixxOES'.");
            }
        }
        public static void glMultMatrixxOES(const GLfixed * @m) => glMultMatrixxOESPtr(@m);

        internal delegate void glMultiTexCoord4xOESFunc(GLenum @texture, GLfixed @s, GLfixed @t, GLfixed @r, GLfixed @q);
        internal static glMultiTexCoord4xOESFunc glMultiTexCoord4xOESPtr;
        internal static void loadMultiTexCoord4xOES()
        {
            try
            {
                glMultiTexCoord4xOESPtr = (glMultiTexCoord4xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4xOES"), typeof(glMultiTexCoord4xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4xOES'.");
            }
        }
        public static void glMultiTexCoord4xOES(GLenum @texture, GLfixed @s, GLfixed @t, GLfixed @r, GLfixed @q) => glMultiTexCoord4xOESPtr(@texture, @s, @t, @r, @q);

        internal delegate void glNormal3xOESFunc(GLfixed @nx, GLfixed @ny, GLfixed @nz);
        internal static glNormal3xOESFunc glNormal3xOESPtr;
        internal static void loadNormal3xOES()
        {
            try
            {
                glNormal3xOESPtr = (glNormal3xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3xOES"), typeof(glNormal3xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3xOES'.");
            }
        }
        public static void glNormal3xOES(GLfixed @nx, GLfixed @ny, GLfixed @nz) => glNormal3xOESPtr(@nx, @ny, @nz);

        internal delegate void glOrthoxOESFunc(GLfixed @l, GLfixed @r, GLfixed @b, GLfixed @t, GLfixed @n, GLfixed @f);
        internal static glOrthoxOESFunc glOrthoxOESPtr;
        internal static void loadOrthoxOES()
        {
            try
            {
                glOrthoxOESPtr = (glOrthoxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glOrthoxOES"), typeof(glOrthoxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glOrthoxOES'.");
            }
        }
        public static void glOrthoxOES(GLfixed @l, GLfixed @r, GLfixed @b, GLfixed @t, GLfixed @n, GLfixed @f) => glOrthoxOESPtr(@l, @r, @b, @t, @n, @f);

        internal delegate void glPointParameterxvOESFunc(GLenum @pname, const GLfixed * @params);
        internal static glPointParameterxvOESFunc glPointParameterxvOESPtr;
        internal static void loadPointParameterxvOES()
        {
            try
            {
                glPointParameterxvOESPtr = (glPointParameterxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterxvOES"), typeof(glPointParameterxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterxvOES'.");
            }
        }
        public static void glPointParameterxvOES(GLenum @pname, const GLfixed * @params) => glPointParameterxvOESPtr(@pname, @params);

        internal delegate void glPointSizexOESFunc(GLfixed @size);
        internal static glPointSizexOESFunc glPointSizexOESPtr;
        internal static void loadPointSizexOES()
        {
            try
            {
                glPointSizexOESPtr = (glPointSizexOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointSizexOES"), typeof(glPointSizexOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointSizexOES'.");
            }
        }
        public static void glPointSizexOES(GLfixed @size) => glPointSizexOESPtr(@size);

        internal delegate void glPolygonOffsetxOESFunc(GLfixed @factor, GLfixed @units);
        internal static glPolygonOffsetxOESFunc glPolygonOffsetxOESPtr;
        internal static void loadPolygonOffsetxOES()
        {
            try
            {
                glPolygonOffsetxOESPtr = (glPolygonOffsetxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPolygonOffsetxOES"), typeof(glPolygonOffsetxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPolygonOffsetxOES'.");
            }
        }
        public static void glPolygonOffsetxOES(GLfixed @factor, GLfixed @units) => glPolygonOffsetxOESPtr(@factor, @units);

        internal delegate void glRotatexOESFunc(GLfixed @angle, GLfixed @x, GLfixed @y, GLfixed @z);
        internal static glRotatexOESFunc glRotatexOESPtr;
        internal static void loadRotatexOES()
        {
            try
            {
                glRotatexOESPtr = (glRotatexOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRotatexOES"), typeof(glRotatexOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRotatexOES'.");
            }
        }
        public static void glRotatexOES(GLfixed @angle, GLfixed @x, GLfixed @y, GLfixed @z) => glRotatexOESPtr(@angle, @x, @y, @z);

        internal delegate void glScalexOESFunc(GLfixed @x, GLfixed @y, GLfixed @z);
        internal static glScalexOESFunc glScalexOESPtr;
        internal static void loadScalexOES()
        {
            try
            {
                glScalexOESPtr = (glScalexOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScalexOES"), typeof(glScalexOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScalexOES'.");
            }
        }
        public static void glScalexOES(GLfixed @x, GLfixed @y, GLfixed @z) => glScalexOESPtr(@x, @y, @z);

        internal delegate void glTexEnvxOESFunc(GLenum @target, GLenum @pname, GLfixed @param);
        internal static glTexEnvxOESFunc glTexEnvxOESPtr;
        internal static void loadTexEnvxOES()
        {
            try
            {
                glTexEnvxOESPtr = (glTexEnvxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexEnvxOES"), typeof(glTexEnvxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexEnvxOES'.");
            }
        }
        public static void glTexEnvxOES(GLenum @target, GLenum @pname, GLfixed @param) => glTexEnvxOESPtr(@target, @pname, @param);

        internal delegate void glTexEnvxvOESFunc(GLenum @target, GLenum @pname, const GLfixed * @params);
        internal static glTexEnvxvOESFunc glTexEnvxvOESPtr;
        internal static void loadTexEnvxvOES()
        {
            try
            {
                glTexEnvxvOESPtr = (glTexEnvxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexEnvxvOES"), typeof(glTexEnvxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexEnvxvOES'.");
            }
        }
        public static void glTexEnvxvOES(GLenum @target, GLenum @pname, const GLfixed * @params) => glTexEnvxvOESPtr(@target, @pname, @params);

        internal delegate void glTexParameterxOESFunc(GLenum @target, GLenum @pname, GLfixed @param);
        internal static glTexParameterxOESFunc glTexParameterxOESPtr;
        internal static void loadTexParameterxOES()
        {
            try
            {
                glTexParameterxOESPtr = (glTexParameterxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterxOES"), typeof(glTexParameterxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterxOES'.");
            }
        }
        public static void glTexParameterxOES(GLenum @target, GLenum @pname, GLfixed @param) => glTexParameterxOESPtr(@target, @pname, @param);

        internal delegate void glTexParameterxvOESFunc(GLenum @target, GLenum @pname, const GLfixed * @params);
        internal static glTexParameterxvOESFunc glTexParameterxvOESPtr;
        internal static void loadTexParameterxvOES()
        {
            try
            {
                glTexParameterxvOESPtr = (glTexParameterxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterxvOES"), typeof(glTexParameterxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterxvOES'.");
            }
        }
        public static void glTexParameterxvOES(GLenum @target, GLenum @pname, const GLfixed * @params) => glTexParameterxvOESPtr(@target, @pname, @params);

        internal delegate void glTranslatexOESFunc(GLfixed @x, GLfixed @y, GLfixed @z);
        internal static glTranslatexOESFunc glTranslatexOESPtr;
        internal static void loadTranslatexOES()
        {
            try
            {
                glTranslatexOESPtr = (glTranslatexOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTranslatexOES"), typeof(glTranslatexOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTranslatexOES'.");
            }
        }
        public static void glTranslatexOES(GLfixed @x, GLfixed @y, GLfixed @z) => glTranslatexOESPtr(@x, @y, @z);

        internal delegate void glGetLightxvOESFunc(GLenum @light, GLenum @pname, GLfixed * @params);
        internal static glGetLightxvOESFunc glGetLightxvOESPtr;
        internal static void loadGetLightxvOES()
        {
            try
            {
                glGetLightxvOESPtr = (glGetLightxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetLightxvOES"), typeof(glGetLightxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetLightxvOES'.");
            }
        }
        public static void glGetLightxvOES(GLenum @light, GLenum @pname, GLfixed * @params) => glGetLightxvOESPtr(@light, @pname, @params);

        internal delegate void glGetMaterialxvOESFunc(GLenum @face, GLenum @pname, GLfixed * @params);
        internal static glGetMaterialxvOESFunc glGetMaterialxvOESPtr;
        internal static void loadGetMaterialxvOES()
        {
            try
            {
                glGetMaterialxvOESPtr = (glGetMaterialxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMaterialxvOES"), typeof(glGetMaterialxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMaterialxvOES'.");
            }
        }
        public static void glGetMaterialxvOES(GLenum @face, GLenum @pname, GLfixed * @params) => glGetMaterialxvOESPtr(@face, @pname, @params);

        internal delegate void glPointParameterxOESFunc(GLenum @pname, GLfixed @param);
        internal static glPointParameterxOESFunc glPointParameterxOESPtr;
        internal static void loadPointParameterxOES()
        {
            try
            {
                glPointParameterxOESPtr = (glPointParameterxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterxOES"), typeof(glPointParameterxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterxOES'.");
            }
        }
        public static void glPointParameterxOES(GLenum @pname, GLfixed @param) => glPointParameterxOESPtr(@pname, @param);

        internal delegate void glSampleCoveragexOESFunc(GLclampx @value, GLboolean @invert);
        internal static glSampleCoveragexOESFunc glSampleCoveragexOESPtr;
        internal static void loadSampleCoveragexOES()
        {
            try
            {
                glSampleCoveragexOESPtr = (glSampleCoveragexOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleCoveragexOES"), typeof(glSampleCoveragexOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleCoveragexOES'.");
            }
        }
        public static void glSampleCoveragexOES(GLclampx @value, GLboolean @invert) => glSampleCoveragexOESPtr(@value, @invert);

        internal delegate void glAccumxOESFunc(GLenum @op, GLfixed @value);
        internal static glAccumxOESFunc glAccumxOESPtr;
        internal static void loadAccumxOES()
        {
            try
            {
                glAccumxOESPtr = (glAccumxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAccumxOES"), typeof(glAccumxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAccumxOES'.");
            }
        }
        public static void glAccumxOES(GLenum @op, GLfixed @value) => glAccumxOESPtr(@op, @value);

        internal delegate void glBitmapxOESFunc(GLsizei @width, GLsizei @height, GLfixed @xorig, GLfixed @yorig, GLfixed @xmove, GLfixed @ymove, const GLubyte * @bitmap);
        internal static glBitmapxOESFunc glBitmapxOESPtr;
        internal static void loadBitmapxOES()
        {
            try
            {
                glBitmapxOESPtr = (glBitmapxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBitmapxOES"), typeof(glBitmapxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBitmapxOES'.");
            }
        }
        public static void glBitmapxOES(GLsizei @width, GLsizei @height, GLfixed @xorig, GLfixed @yorig, GLfixed @xmove, GLfixed @ymove, const GLubyte * @bitmap) => glBitmapxOESPtr(@width, @height, @xorig, @yorig, @xmove, @ymove, @bitmap);

        internal delegate void glBlendColorxOESFunc(GLfixed @red, GLfixed @green, GLfixed @blue, GLfixed @alpha);
        internal static glBlendColorxOESFunc glBlendColorxOESPtr;
        internal static void loadBlendColorxOES()
        {
            try
            {
                glBlendColorxOESPtr = (glBlendColorxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendColorxOES"), typeof(glBlendColorxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendColorxOES'.");
            }
        }
        public static void glBlendColorxOES(GLfixed @red, GLfixed @green, GLfixed @blue, GLfixed @alpha) => glBlendColorxOESPtr(@red, @green, @blue, @alpha);

        internal delegate void glClearAccumxOESFunc(GLfixed @red, GLfixed @green, GLfixed @blue, GLfixed @alpha);
        internal static glClearAccumxOESFunc glClearAccumxOESPtr;
        internal static void loadClearAccumxOES()
        {
            try
            {
                glClearAccumxOESPtr = (glClearAccumxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearAccumxOES"), typeof(glClearAccumxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearAccumxOES'.");
            }
        }
        public static void glClearAccumxOES(GLfixed @red, GLfixed @green, GLfixed @blue, GLfixed @alpha) => glClearAccumxOESPtr(@red, @green, @blue, @alpha);

        internal delegate void glColor3xOESFunc(GLfixed @red, GLfixed @green, GLfixed @blue);
        internal static glColor3xOESFunc glColor3xOESPtr;
        internal static void loadColor3xOES()
        {
            try
            {
                glColor3xOESPtr = (glColor3xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3xOES"), typeof(glColor3xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3xOES'.");
            }
        }
        public static void glColor3xOES(GLfixed @red, GLfixed @green, GLfixed @blue) => glColor3xOESPtr(@red, @green, @blue);

        internal delegate void glColor3xvOESFunc(const GLfixed * @components);
        internal static glColor3xvOESFunc glColor3xvOESPtr;
        internal static void loadColor3xvOES()
        {
            try
            {
                glColor3xvOESPtr = (glColor3xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor3xvOES"), typeof(glColor3xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor3xvOES'.");
            }
        }
        public static void glColor3xvOES(const GLfixed * @components) => glColor3xvOESPtr(@components);

        internal delegate void glColor4xvOESFunc(const GLfixed * @components);
        internal static glColor4xvOESFunc glColor4xvOESPtr;
        internal static void loadColor4xvOES()
        {
            try
            {
                glColor4xvOESPtr = (glColor4xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4xvOES"), typeof(glColor4xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4xvOES'.");
            }
        }
        public static void glColor4xvOES(const GLfixed * @components) => glColor4xvOESPtr(@components);

        internal delegate void glConvolutionParameterxOESFunc(GLenum @target, GLenum @pname, GLfixed @param);
        internal static glConvolutionParameterxOESFunc glConvolutionParameterxOESPtr;
        internal static void loadConvolutionParameterxOES()
        {
            try
            {
                glConvolutionParameterxOESPtr = (glConvolutionParameterxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionParameterxOES"), typeof(glConvolutionParameterxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionParameterxOES'.");
            }
        }
        public static void glConvolutionParameterxOES(GLenum @target, GLenum @pname, GLfixed @param) => glConvolutionParameterxOESPtr(@target, @pname, @param);

        internal delegate void glConvolutionParameterxvOESFunc(GLenum @target, GLenum @pname, const GLfixed * @params);
        internal static glConvolutionParameterxvOESFunc glConvolutionParameterxvOESPtr;
        internal static void loadConvolutionParameterxvOES()
        {
            try
            {
                glConvolutionParameterxvOESPtr = (glConvolutionParameterxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionParameterxvOES"), typeof(glConvolutionParameterxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionParameterxvOES'.");
            }
        }
        public static void glConvolutionParameterxvOES(GLenum @target, GLenum @pname, const GLfixed * @params) => glConvolutionParameterxvOESPtr(@target, @pname, @params);

        internal delegate void glEvalCoord1xOESFunc(GLfixed @u);
        internal static glEvalCoord1xOESFunc glEvalCoord1xOESPtr;
        internal static void loadEvalCoord1xOES()
        {
            try
            {
                glEvalCoord1xOESPtr = (glEvalCoord1xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalCoord1xOES"), typeof(glEvalCoord1xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalCoord1xOES'.");
            }
        }
        public static void glEvalCoord1xOES(GLfixed @u) => glEvalCoord1xOESPtr(@u);

        internal delegate void glEvalCoord1xvOESFunc(const GLfixed * @coords);
        internal static glEvalCoord1xvOESFunc glEvalCoord1xvOESPtr;
        internal static void loadEvalCoord1xvOES()
        {
            try
            {
                glEvalCoord1xvOESPtr = (glEvalCoord1xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalCoord1xvOES"), typeof(glEvalCoord1xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalCoord1xvOES'.");
            }
        }
        public static void glEvalCoord1xvOES(const GLfixed * @coords) => glEvalCoord1xvOESPtr(@coords);

        internal delegate void glEvalCoord2xOESFunc(GLfixed @u, GLfixed @v);
        internal static glEvalCoord2xOESFunc glEvalCoord2xOESPtr;
        internal static void loadEvalCoord2xOES()
        {
            try
            {
                glEvalCoord2xOESPtr = (glEvalCoord2xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalCoord2xOES"), typeof(glEvalCoord2xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalCoord2xOES'.");
            }
        }
        public static void glEvalCoord2xOES(GLfixed @u, GLfixed @v) => glEvalCoord2xOESPtr(@u, @v);

        internal delegate void glEvalCoord2xvOESFunc(const GLfixed * @coords);
        internal static glEvalCoord2xvOESFunc glEvalCoord2xvOESPtr;
        internal static void loadEvalCoord2xvOES()
        {
            try
            {
                glEvalCoord2xvOESPtr = (glEvalCoord2xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvalCoord2xvOES"), typeof(glEvalCoord2xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvalCoord2xvOES'.");
            }
        }
        public static void glEvalCoord2xvOES(const GLfixed * @coords) => glEvalCoord2xvOESPtr(@coords);

        internal delegate void glFeedbackBufferxOESFunc(GLsizei @n, GLenum @type, const GLfixed * @buffer);
        internal static glFeedbackBufferxOESFunc glFeedbackBufferxOESPtr;
        internal static void loadFeedbackBufferxOES()
        {
            try
            {
                glFeedbackBufferxOESPtr = (glFeedbackBufferxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFeedbackBufferxOES"), typeof(glFeedbackBufferxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFeedbackBufferxOES'.");
            }
        }
        public static void glFeedbackBufferxOES(GLsizei @n, GLenum @type, const GLfixed * @buffer) => glFeedbackBufferxOESPtr(@n, @type, @buffer);

        internal delegate void glGetConvolutionParameterxvOESFunc(GLenum @target, GLenum @pname, GLfixed * @params);
        internal static glGetConvolutionParameterxvOESFunc glGetConvolutionParameterxvOESPtr;
        internal static void loadGetConvolutionParameterxvOES()
        {
            try
            {
                glGetConvolutionParameterxvOESPtr = (glGetConvolutionParameterxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetConvolutionParameterxvOES"), typeof(glGetConvolutionParameterxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetConvolutionParameterxvOES'.");
            }
        }
        public static void glGetConvolutionParameterxvOES(GLenum @target, GLenum @pname, GLfixed * @params) => glGetConvolutionParameterxvOESPtr(@target, @pname, @params);

        internal delegate void glGetHistogramParameterxvOESFunc(GLenum @target, GLenum @pname, GLfixed * @params);
        internal static glGetHistogramParameterxvOESFunc glGetHistogramParameterxvOESPtr;
        internal static void loadGetHistogramParameterxvOES()
        {
            try
            {
                glGetHistogramParameterxvOESPtr = (glGetHistogramParameterxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetHistogramParameterxvOES"), typeof(glGetHistogramParameterxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetHistogramParameterxvOES'.");
            }
        }
        public static void glGetHistogramParameterxvOES(GLenum @target, GLenum @pname, GLfixed * @params) => glGetHistogramParameterxvOESPtr(@target, @pname, @params);

        internal delegate void glGetLightxOESFunc(GLenum @light, GLenum @pname, GLfixed * @params);
        internal static glGetLightxOESFunc glGetLightxOESPtr;
        internal static void loadGetLightxOES()
        {
            try
            {
                glGetLightxOESPtr = (glGetLightxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetLightxOES"), typeof(glGetLightxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetLightxOES'.");
            }
        }
        public static void glGetLightxOES(GLenum @light, GLenum @pname, GLfixed * @params) => glGetLightxOESPtr(@light, @pname, @params);

        internal delegate void glGetMapxvOESFunc(GLenum @target, GLenum @query, GLfixed * @v);
        internal static glGetMapxvOESFunc glGetMapxvOESPtr;
        internal static void loadGetMapxvOES()
        {
            try
            {
                glGetMapxvOESPtr = (glGetMapxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMapxvOES"), typeof(glGetMapxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMapxvOES'.");
            }
        }
        public static void glGetMapxvOES(GLenum @target, GLenum @query, GLfixed * @v) => glGetMapxvOESPtr(@target, @query, @v);

        internal delegate void glGetMaterialxOESFunc(GLenum @face, GLenum @pname, GLfixed @param);
        internal static glGetMaterialxOESFunc glGetMaterialxOESPtr;
        internal static void loadGetMaterialxOES()
        {
            try
            {
                glGetMaterialxOESPtr = (glGetMaterialxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMaterialxOES"), typeof(glGetMaterialxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMaterialxOES'.");
            }
        }
        public static void glGetMaterialxOES(GLenum @face, GLenum @pname, GLfixed @param) => glGetMaterialxOESPtr(@face, @pname, @param);

        internal delegate void glGetPixelMapxvFunc(GLenum @map, GLint @size, GLfixed * @values);
        internal static glGetPixelMapxvFunc glGetPixelMapxvPtr;
        internal static void loadGetPixelMapxv()
        {
            try
            {
                glGetPixelMapxvPtr = (glGetPixelMapxvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPixelMapxv"), typeof(glGetPixelMapxvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPixelMapxv'.");
            }
        }
        public static void glGetPixelMapxv(GLenum @map, GLint @size, GLfixed * @values) => glGetPixelMapxvPtr(@map, @size, @values);

        internal delegate void glGetTexGenxvOESFunc(GLenum @coord, GLenum @pname, GLfixed * @params);
        internal static glGetTexGenxvOESFunc glGetTexGenxvOESPtr;
        internal static void loadGetTexGenxvOES()
        {
            try
            {
                glGetTexGenxvOESPtr = (glGetTexGenxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexGenxvOES"), typeof(glGetTexGenxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexGenxvOES'.");
            }
        }
        public static void glGetTexGenxvOES(GLenum @coord, GLenum @pname, GLfixed * @params) => glGetTexGenxvOESPtr(@coord, @pname, @params);

        internal delegate void glGetTexLevelParameterxvOESFunc(GLenum @target, GLint @level, GLenum @pname, GLfixed * @params);
        internal static glGetTexLevelParameterxvOESFunc glGetTexLevelParameterxvOESPtr;
        internal static void loadGetTexLevelParameterxvOES()
        {
            try
            {
                glGetTexLevelParameterxvOESPtr = (glGetTexLevelParameterxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexLevelParameterxvOES"), typeof(glGetTexLevelParameterxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexLevelParameterxvOES'.");
            }
        }
        public static void glGetTexLevelParameterxvOES(GLenum @target, GLint @level, GLenum @pname, GLfixed * @params) => glGetTexLevelParameterxvOESPtr(@target, @level, @pname, @params);

        internal delegate void glIndexxOESFunc(GLfixed @component);
        internal static glIndexxOESFunc glIndexxOESPtr;
        internal static void loadIndexxOES()
        {
            try
            {
                glIndexxOESPtr = (glIndexxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexxOES"), typeof(glIndexxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexxOES'.");
            }
        }
        public static void glIndexxOES(GLfixed @component) => glIndexxOESPtr(@component);

        internal delegate void glIndexxvOESFunc(const GLfixed * @component);
        internal static glIndexxvOESFunc glIndexxvOESPtr;
        internal static void loadIndexxvOES()
        {
            try
            {
                glIndexxvOESPtr = (glIndexxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIndexxvOES"), typeof(glIndexxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIndexxvOES'.");
            }
        }
        public static void glIndexxvOES(const GLfixed * @component) => glIndexxvOESPtr(@component);

        internal delegate void glLoadTransposeMatrixxOESFunc(const GLfixed * @m);
        internal static glLoadTransposeMatrixxOESFunc glLoadTransposeMatrixxOESPtr;
        internal static void loadLoadTransposeMatrixxOES()
        {
            try
            {
                glLoadTransposeMatrixxOESPtr = (glLoadTransposeMatrixxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadTransposeMatrixxOES"), typeof(glLoadTransposeMatrixxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadTransposeMatrixxOES'.");
            }
        }
        public static void glLoadTransposeMatrixxOES(const GLfixed * @m) => glLoadTransposeMatrixxOESPtr(@m);

        internal delegate void glMap1xOESFunc(GLenum @target, GLfixed @u1, GLfixed @u2, GLint @stride, GLint @order, GLfixed @points);
        internal static glMap1xOESFunc glMap1xOESPtr;
        internal static void loadMap1xOES()
        {
            try
            {
                glMap1xOESPtr = (glMap1xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMap1xOES"), typeof(glMap1xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMap1xOES'.");
            }
        }
        public static void glMap1xOES(GLenum @target, GLfixed @u1, GLfixed @u2, GLint @stride, GLint @order, GLfixed @points) => glMap1xOESPtr(@target, @u1, @u2, @stride, @order, @points);

        internal delegate void glMap2xOESFunc(GLenum @target, GLfixed @u1, GLfixed @u2, GLint @ustride, GLint @uorder, GLfixed @v1, GLfixed @v2, GLint @vstride, GLint @vorder, GLfixed @points);
        internal static glMap2xOESFunc glMap2xOESPtr;
        internal static void loadMap2xOES()
        {
            try
            {
                glMap2xOESPtr = (glMap2xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMap2xOES"), typeof(glMap2xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMap2xOES'.");
            }
        }
        public static void glMap2xOES(GLenum @target, GLfixed @u1, GLfixed @u2, GLint @ustride, GLint @uorder, GLfixed @v1, GLfixed @v2, GLint @vstride, GLint @vorder, GLfixed @points) => glMap2xOESPtr(@target, @u1, @u2, @ustride, @uorder, @v1, @v2, @vstride, @vorder, @points);

        internal delegate void glMapGrid1xOESFunc(GLint @n, GLfixed @u1, GLfixed @u2);
        internal static glMapGrid1xOESFunc glMapGrid1xOESPtr;
        internal static void loadMapGrid1xOES()
        {
            try
            {
                glMapGrid1xOESPtr = (glMapGrid1xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapGrid1xOES"), typeof(glMapGrid1xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapGrid1xOES'.");
            }
        }
        public static void glMapGrid1xOES(GLint @n, GLfixed @u1, GLfixed @u2) => glMapGrid1xOESPtr(@n, @u1, @u2);

        internal delegate void glMapGrid2xOESFunc(GLint @n, GLfixed @u1, GLfixed @u2, GLfixed @v1, GLfixed @v2);
        internal static glMapGrid2xOESFunc glMapGrid2xOESPtr;
        internal static void loadMapGrid2xOES()
        {
            try
            {
                glMapGrid2xOESPtr = (glMapGrid2xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapGrid2xOES"), typeof(glMapGrid2xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapGrid2xOES'.");
            }
        }
        public static void glMapGrid2xOES(GLint @n, GLfixed @u1, GLfixed @u2, GLfixed @v1, GLfixed @v2) => glMapGrid2xOESPtr(@n, @u1, @u2, @v1, @v2);

        internal delegate void glMultTransposeMatrixxOESFunc(const GLfixed * @m);
        internal static glMultTransposeMatrixxOESFunc glMultTransposeMatrixxOESPtr;
        internal static void loadMultTransposeMatrixxOES()
        {
            try
            {
                glMultTransposeMatrixxOESPtr = (glMultTransposeMatrixxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultTransposeMatrixxOES"), typeof(glMultTransposeMatrixxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultTransposeMatrixxOES'.");
            }
        }
        public static void glMultTransposeMatrixxOES(const GLfixed * @m) => glMultTransposeMatrixxOESPtr(@m);

        internal delegate void glMultiTexCoord1xOESFunc(GLenum @texture, GLfixed @s);
        internal static glMultiTexCoord1xOESFunc glMultiTexCoord1xOESPtr;
        internal static void loadMultiTexCoord1xOES()
        {
            try
            {
                glMultiTexCoord1xOESPtr = (glMultiTexCoord1xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1xOES"), typeof(glMultiTexCoord1xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1xOES'.");
            }
        }
        public static void glMultiTexCoord1xOES(GLenum @texture, GLfixed @s) => glMultiTexCoord1xOESPtr(@texture, @s);

        internal delegate void glMultiTexCoord1xvOESFunc(GLenum @texture, const GLfixed * @coords);
        internal static glMultiTexCoord1xvOESFunc glMultiTexCoord1xvOESPtr;
        internal static void loadMultiTexCoord1xvOES()
        {
            try
            {
                glMultiTexCoord1xvOESPtr = (glMultiTexCoord1xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1xvOES"), typeof(glMultiTexCoord1xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1xvOES'.");
            }
        }
        public static void glMultiTexCoord1xvOES(GLenum @texture, const GLfixed * @coords) => glMultiTexCoord1xvOESPtr(@texture, @coords);

        internal delegate void glMultiTexCoord2xOESFunc(GLenum @texture, GLfixed @s, GLfixed @t);
        internal static glMultiTexCoord2xOESFunc glMultiTexCoord2xOESPtr;
        internal static void loadMultiTexCoord2xOES()
        {
            try
            {
                glMultiTexCoord2xOESPtr = (glMultiTexCoord2xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2xOES"), typeof(glMultiTexCoord2xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2xOES'.");
            }
        }
        public static void glMultiTexCoord2xOES(GLenum @texture, GLfixed @s, GLfixed @t) => glMultiTexCoord2xOESPtr(@texture, @s, @t);

        internal delegate void glMultiTexCoord2xvOESFunc(GLenum @texture, const GLfixed * @coords);
        internal static glMultiTexCoord2xvOESFunc glMultiTexCoord2xvOESPtr;
        internal static void loadMultiTexCoord2xvOES()
        {
            try
            {
                glMultiTexCoord2xvOESPtr = (glMultiTexCoord2xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2xvOES"), typeof(glMultiTexCoord2xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2xvOES'.");
            }
        }
        public static void glMultiTexCoord2xvOES(GLenum @texture, const GLfixed * @coords) => glMultiTexCoord2xvOESPtr(@texture, @coords);

        internal delegate void glMultiTexCoord3xOESFunc(GLenum @texture, GLfixed @s, GLfixed @t, GLfixed @r);
        internal static glMultiTexCoord3xOESFunc glMultiTexCoord3xOESPtr;
        internal static void loadMultiTexCoord3xOES()
        {
            try
            {
                glMultiTexCoord3xOESPtr = (glMultiTexCoord3xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3xOES"), typeof(glMultiTexCoord3xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3xOES'.");
            }
        }
        public static void glMultiTexCoord3xOES(GLenum @texture, GLfixed @s, GLfixed @t, GLfixed @r) => glMultiTexCoord3xOESPtr(@texture, @s, @t, @r);

        internal delegate void glMultiTexCoord3xvOESFunc(GLenum @texture, const GLfixed * @coords);
        internal static glMultiTexCoord3xvOESFunc glMultiTexCoord3xvOESPtr;
        internal static void loadMultiTexCoord3xvOES()
        {
            try
            {
                glMultiTexCoord3xvOESPtr = (glMultiTexCoord3xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3xvOES"), typeof(glMultiTexCoord3xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3xvOES'.");
            }
        }
        public static void glMultiTexCoord3xvOES(GLenum @texture, const GLfixed * @coords) => glMultiTexCoord3xvOESPtr(@texture, @coords);

        internal delegate void glMultiTexCoord4xvOESFunc(GLenum @texture, const GLfixed * @coords);
        internal static glMultiTexCoord4xvOESFunc glMultiTexCoord4xvOESPtr;
        internal static void loadMultiTexCoord4xvOES()
        {
            try
            {
                glMultiTexCoord4xvOESPtr = (glMultiTexCoord4xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4xvOES"), typeof(glMultiTexCoord4xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4xvOES'.");
            }
        }
        public static void glMultiTexCoord4xvOES(GLenum @texture, const GLfixed * @coords) => glMultiTexCoord4xvOESPtr(@texture, @coords);

        internal delegate void glNormal3xvOESFunc(const GLfixed * @coords);
        internal static glNormal3xvOESFunc glNormal3xvOESPtr;
        internal static void loadNormal3xvOES()
        {
            try
            {
                glNormal3xvOESPtr = (glNormal3xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3xvOES"), typeof(glNormal3xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3xvOES'.");
            }
        }
        public static void glNormal3xvOES(const GLfixed * @coords) => glNormal3xvOESPtr(@coords);

        internal delegate void glPassThroughxOESFunc(GLfixed @token);
        internal static glPassThroughxOESFunc glPassThroughxOESPtr;
        internal static void loadPassThroughxOES()
        {
            try
            {
                glPassThroughxOESPtr = (glPassThroughxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPassThroughxOES"), typeof(glPassThroughxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPassThroughxOES'.");
            }
        }
        public static void glPassThroughxOES(GLfixed @token) => glPassThroughxOESPtr(@token);

        internal delegate void glPixelMapxFunc(GLenum @map, GLint @size, const GLfixed * @values);
        internal static glPixelMapxFunc glPixelMapxPtr;
        internal static void loadPixelMapx()
        {
            try
            {
                glPixelMapxPtr = (glPixelMapxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelMapx"), typeof(glPixelMapxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelMapx'.");
            }
        }
        public static void glPixelMapx(GLenum @map, GLint @size, const GLfixed * @values) => glPixelMapxPtr(@map, @size, @values);

        internal delegate void glPixelStorexFunc(GLenum @pname, GLfixed @param);
        internal static glPixelStorexFunc glPixelStorexPtr;
        internal static void loadPixelStorex()
        {
            try
            {
                glPixelStorexPtr = (glPixelStorexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelStorex"), typeof(glPixelStorexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelStorex'.");
            }
        }
        public static void glPixelStorex(GLenum @pname, GLfixed @param) => glPixelStorexPtr(@pname, @param);

        internal delegate void glPixelTransferxOESFunc(GLenum @pname, GLfixed @param);
        internal static glPixelTransferxOESFunc glPixelTransferxOESPtr;
        internal static void loadPixelTransferxOES()
        {
            try
            {
                glPixelTransferxOESPtr = (glPixelTransferxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelTransferxOES"), typeof(glPixelTransferxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelTransferxOES'.");
            }
        }
        public static void glPixelTransferxOES(GLenum @pname, GLfixed @param) => glPixelTransferxOESPtr(@pname, @param);

        internal delegate void glPixelZoomxOESFunc(GLfixed @xfactor, GLfixed @yfactor);
        internal static glPixelZoomxOESFunc glPixelZoomxOESPtr;
        internal static void loadPixelZoomxOES()
        {
            try
            {
                glPixelZoomxOESPtr = (glPixelZoomxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelZoomxOES"), typeof(glPixelZoomxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelZoomxOES'.");
            }
        }
        public static void glPixelZoomxOES(GLfixed @xfactor, GLfixed @yfactor) => glPixelZoomxOESPtr(@xfactor, @yfactor);

        internal delegate void glPrioritizeTexturesxOESFunc(GLsizei @n, const GLuint * @textures, const GLfixed * @priorities);
        internal static glPrioritizeTexturesxOESFunc glPrioritizeTexturesxOESPtr;
        internal static void loadPrioritizeTexturesxOES()
        {
            try
            {
                glPrioritizeTexturesxOESPtr = (glPrioritizeTexturesxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPrioritizeTexturesxOES"), typeof(glPrioritizeTexturesxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPrioritizeTexturesxOES'.");
            }
        }
        public static void glPrioritizeTexturesxOES(GLsizei @n, const GLuint * @textures, const GLfixed * @priorities) => glPrioritizeTexturesxOESPtr(@n, @textures, @priorities);

        internal delegate void glRasterPos2xOESFunc(GLfixed @x, GLfixed @y);
        internal static glRasterPos2xOESFunc glRasterPos2xOESPtr;
        internal static void loadRasterPos2xOES()
        {
            try
            {
                glRasterPos2xOESPtr = (glRasterPos2xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos2xOES"), typeof(glRasterPos2xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos2xOES'.");
            }
        }
        public static void glRasterPos2xOES(GLfixed @x, GLfixed @y) => glRasterPos2xOESPtr(@x, @y);

        internal delegate void glRasterPos2xvOESFunc(const GLfixed * @coords);
        internal static glRasterPos2xvOESFunc glRasterPos2xvOESPtr;
        internal static void loadRasterPos2xvOES()
        {
            try
            {
                glRasterPos2xvOESPtr = (glRasterPos2xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos2xvOES"), typeof(glRasterPos2xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos2xvOES'.");
            }
        }
        public static void glRasterPos2xvOES(const GLfixed * @coords) => glRasterPos2xvOESPtr(@coords);

        internal delegate void glRasterPos3xOESFunc(GLfixed @x, GLfixed @y, GLfixed @z);
        internal static glRasterPos3xOESFunc glRasterPos3xOESPtr;
        internal static void loadRasterPos3xOES()
        {
            try
            {
                glRasterPos3xOESPtr = (glRasterPos3xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos3xOES"), typeof(glRasterPos3xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos3xOES'.");
            }
        }
        public static void glRasterPos3xOES(GLfixed @x, GLfixed @y, GLfixed @z) => glRasterPos3xOESPtr(@x, @y, @z);

        internal delegate void glRasterPos3xvOESFunc(const GLfixed * @coords);
        internal static glRasterPos3xvOESFunc glRasterPos3xvOESPtr;
        internal static void loadRasterPos3xvOES()
        {
            try
            {
                glRasterPos3xvOESPtr = (glRasterPos3xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos3xvOES"), typeof(glRasterPos3xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos3xvOES'.");
            }
        }
        public static void glRasterPos3xvOES(const GLfixed * @coords) => glRasterPos3xvOESPtr(@coords);

        internal delegate void glRasterPos4xOESFunc(GLfixed @x, GLfixed @y, GLfixed @z, GLfixed @w);
        internal static glRasterPos4xOESFunc glRasterPos4xOESPtr;
        internal static void loadRasterPos4xOES()
        {
            try
            {
                glRasterPos4xOESPtr = (glRasterPos4xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos4xOES"), typeof(glRasterPos4xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos4xOES'.");
            }
        }
        public static void glRasterPos4xOES(GLfixed @x, GLfixed @y, GLfixed @z, GLfixed @w) => glRasterPos4xOESPtr(@x, @y, @z, @w);

        internal delegate void glRasterPos4xvOESFunc(const GLfixed * @coords);
        internal static glRasterPos4xvOESFunc glRasterPos4xvOESPtr;
        internal static void loadRasterPos4xvOES()
        {
            try
            {
                glRasterPos4xvOESPtr = (glRasterPos4xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRasterPos4xvOES"), typeof(glRasterPos4xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRasterPos4xvOES'.");
            }
        }
        public static void glRasterPos4xvOES(const GLfixed * @coords) => glRasterPos4xvOESPtr(@coords);

        internal delegate void glRectxOESFunc(GLfixed @x1, GLfixed @y1, GLfixed @x2, GLfixed @y2);
        internal static glRectxOESFunc glRectxOESPtr;
        internal static void loadRectxOES()
        {
            try
            {
                glRectxOESPtr = (glRectxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRectxOES"), typeof(glRectxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRectxOES'.");
            }
        }
        public static void glRectxOES(GLfixed @x1, GLfixed @y1, GLfixed @x2, GLfixed @y2) => glRectxOESPtr(@x1, @y1, @x2, @y2);

        internal delegate void glRectxvOESFunc(const GLfixed * @v1, const GLfixed * @v2);
        internal static glRectxvOESFunc glRectxvOESPtr;
        internal static void loadRectxvOES()
        {
            try
            {
                glRectxvOESPtr = (glRectxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRectxvOES"), typeof(glRectxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRectxvOES'.");
            }
        }
        public static void glRectxvOES(const GLfixed * @v1, const GLfixed * @v2) => glRectxvOESPtr(@v1, @v2);

        internal delegate void glTexCoord1xOESFunc(GLfixed @s);
        internal static glTexCoord1xOESFunc glTexCoord1xOESPtr;
        internal static void loadTexCoord1xOES()
        {
            try
            {
                glTexCoord1xOESPtr = (glTexCoord1xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1xOES"), typeof(glTexCoord1xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1xOES'.");
            }
        }
        public static void glTexCoord1xOES(GLfixed @s) => glTexCoord1xOESPtr(@s);

        internal delegate void glTexCoord1xvOESFunc(const GLfixed * @coords);
        internal static glTexCoord1xvOESFunc glTexCoord1xvOESPtr;
        internal static void loadTexCoord1xvOES()
        {
            try
            {
                glTexCoord1xvOESPtr = (glTexCoord1xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord1xvOES"), typeof(glTexCoord1xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord1xvOES'.");
            }
        }
        public static void glTexCoord1xvOES(const GLfixed * @coords) => glTexCoord1xvOESPtr(@coords);

        internal delegate void glTexCoord2xOESFunc(GLfixed @s, GLfixed @t);
        internal static glTexCoord2xOESFunc glTexCoord2xOESPtr;
        internal static void loadTexCoord2xOES()
        {
            try
            {
                glTexCoord2xOESPtr = (glTexCoord2xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2xOES"), typeof(glTexCoord2xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2xOES'.");
            }
        }
        public static void glTexCoord2xOES(GLfixed @s, GLfixed @t) => glTexCoord2xOESPtr(@s, @t);

        internal delegate void glTexCoord2xvOESFunc(const GLfixed * @coords);
        internal static glTexCoord2xvOESFunc glTexCoord2xvOESPtr;
        internal static void loadTexCoord2xvOES()
        {
            try
            {
                glTexCoord2xvOESPtr = (glTexCoord2xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord2xvOES"), typeof(glTexCoord2xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord2xvOES'.");
            }
        }
        public static void glTexCoord2xvOES(const GLfixed * @coords) => glTexCoord2xvOESPtr(@coords);

        internal delegate void glTexCoord3xOESFunc(GLfixed @s, GLfixed @t, GLfixed @r);
        internal static glTexCoord3xOESFunc glTexCoord3xOESPtr;
        internal static void loadTexCoord3xOES()
        {
            try
            {
                glTexCoord3xOESPtr = (glTexCoord3xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3xOES"), typeof(glTexCoord3xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3xOES'.");
            }
        }
        public static void glTexCoord3xOES(GLfixed @s, GLfixed @t, GLfixed @r) => glTexCoord3xOESPtr(@s, @t, @r);

        internal delegate void glTexCoord3xvOESFunc(const GLfixed * @coords);
        internal static glTexCoord3xvOESFunc glTexCoord3xvOESPtr;
        internal static void loadTexCoord3xvOES()
        {
            try
            {
                glTexCoord3xvOESPtr = (glTexCoord3xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord3xvOES"), typeof(glTexCoord3xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord3xvOES'.");
            }
        }
        public static void glTexCoord3xvOES(const GLfixed * @coords) => glTexCoord3xvOESPtr(@coords);

        internal delegate void glTexCoord4xOESFunc(GLfixed @s, GLfixed @t, GLfixed @r, GLfixed @q);
        internal static glTexCoord4xOESFunc glTexCoord4xOESPtr;
        internal static void loadTexCoord4xOES()
        {
            try
            {
                glTexCoord4xOESPtr = (glTexCoord4xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4xOES"), typeof(glTexCoord4xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4xOES'.");
            }
        }
        public static void glTexCoord4xOES(GLfixed @s, GLfixed @t, GLfixed @r, GLfixed @q) => glTexCoord4xOESPtr(@s, @t, @r, @q);

        internal delegate void glTexCoord4xvOESFunc(const GLfixed * @coords);
        internal static glTexCoord4xvOESFunc glTexCoord4xvOESPtr;
        internal static void loadTexCoord4xvOES()
        {
            try
            {
                glTexCoord4xvOESPtr = (glTexCoord4xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoord4xvOES"), typeof(glTexCoord4xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoord4xvOES'.");
            }
        }
        public static void glTexCoord4xvOES(const GLfixed * @coords) => glTexCoord4xvOESPtr(@coords);

        internal delegate void glTexGenxOESFunc(GLenum @coord, GLenum @pname, GLfixed @param);
        internal static glTexGenxOESFunc glTexGenxOESPtr;
        internal static void loadTexGenxOES()
        {
            try
            {
                glTexGenxOESPtr = (glTexGenxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGenxOES"), typeof(glTexGenxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGenxOES'.");
            }
        }
        public static void glTexGenxOES(GLenum @coord, GLenum @pname, GLfixed @param) => glTexGenxOESPtr(@coord, @pname, @param);

        internal delegate void glTexGenxvOESFunc(GLenum @coord, GLenum @pname, const GLfixed * @params);
        internal static glTexGenxvOESFunc glTexGenxvOESPtr;
        internal static void loadTexGenxvOES()
        {
            try
            {
                glTexGenxvOESPtr = (glTexGenxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexGenxvOES"), typeof(glTexGenxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexGenxvOES'.");
            }
        }
        public static void glTexGenxvOES(GLenum @coord, GLenum @pname, const GLfixed * @params) => glTexGenxvOESPtr(@coord, @pname, @params);

        internal delegate void glVertex2xOESFunc(GLfixed @x);
        internal static glVertex2xOESFunc glVertex2xOESPtr;
        internal static void loadVertex2xOES()
        {
            try
            {
                glVertex2xOESPtr = (glVertex2xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2xOES"), typeof(glVertex2xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2xOES'.");
            }
        }
        public static void glVertex2xOES(GLfixed @x) => glVertex2xOESPtr(@x);

        internal delegate void glVertex2xvOESFunc(const GLfixed * @coords);
        internal static glVertex2xvOESFunc glVertex2xvOESPtr;
        internal static void loadVertex2xvOES()
        {
            try
            {
                glVertex2xvOESPtr = (glVertex2xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex2xvOES"), typeof(glVertex2xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex2xvOES'.");
            }
        }
        public static void glVertex2xvOES(const GLfixed * @coords) => glVertex2xvOESPtr(@coords);

        internal delegate void glVertex3xOESFunc(GLfixed @x, GLfixed @y);
        internal static glVertex3xOESFunc glVertex3xOESPtr;
        internal static void loadVertex3xOES()
        {
            try
            {
                glVertex3xOESPtr = (glVertex3xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3xOES"), typeof(glVertex3xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3xOES'.");
            }
        }
        public static void glVertex3xOES(GLfixed @x, GLfixed @y) => glVertex3xOESPtr(@x, @y);

        internal delegate void glVertex3xvOESFunc(const GLfixed * @coords);
        internal static glVertex3xvOESFunc glVertex3xvOESPtr;
        internal static void loadVertex3xvOES()
        {
            try
            {
                glVertex3xvOESPtr = (glVertex3xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex3xvOES"), typeof(glVertex3xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex3xvOES'.");
            }
        }
        public static void glVertex3xvOES(const GLfixed * @coords) => glVertex3xvOESPtr(@coords);

        internal delegate void glVertex4xOESFunc(GLfixed @x, GLfixed @y, GLfixed @z);
        internal static glVertex4xOESFunc glVertex4xOESPtr;
        internal static void loadVertex4xOES()
        {
            try
            {
                glVertex4xOESPtr = (glVertex4xOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4xOES"), typeof(glVertex4xOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4xOES'.");
            }
        }
        public static void glVertex4xOES(GLfixed @x, GLfixed @y, GLfixed @z) => glVertex4xOESPtr(@x, @y, @z);

        internal delegate void glVertex4xvOESFunc(const GLfixed * @coords);
        internal static glVertex4xvOESFunc glVertex4xvOESPtr;
        internal static void loadVertex4xvOES()
        {
            try
            {
                glVertex4xvOESPtr = (glVertex4xvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertex4xvOES"), typeof(glVertex4xvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertex4xvOES'.");
            }
        }
        public static void glVertex4xvOES(const GLfixed * @coords) => glVertex4xvOESPtr(@coords);
        #endregion
    }
}
