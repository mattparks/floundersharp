using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_single_precision
    {
        #region Interop
        static GL_OES_single_precision()
        {
            Console.WriteLine("Initalising GL_OES_single_precision interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadClearDepthfOES();
            loadClipPlanefOES();
            loadDepthRangefOES();
            loadFrustumfOES();
            loadGetClipPlanefOES();
            loadOrthofOES();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glClearDepthfOESFunc(GLclampf @depth);
        internal static glClearDepthfOESFunc glClearDepthfOESPtr;
        internal static void loadClearDepthfOES()
        {
            try
            {
                glClearDepthfOESPtr = (glClearDepthfOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearDepthfOES"), typeof(glClearDepthfOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearDepthfOES'.");
            }
        }
        public static void glClearDepthfOES(GLclampf @depth) => glClearDepthfOESPtr(@depth);

        internal delegate void glClipPlanefOESFunc(GLenum @plane, const GLfloat * @equation);
        internal static glClipPlanefOESFunc glClipPlanefOESPtr;
        internal static void loadClipPlanefOES()
        {
            try
            {
                glClipPlanefOESPtr = (glClipPlanefOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClipPlanefOES"), typeof(glClipPlanefOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClipPlanefOES'.");
            }
        }
        public static void glClipPlanefOES(GLenum @plane, const GLfloat * @equation) => glClipPlanefOESPtr(@plane, @equation);

        internal delegate void glDepthRangefOESFunc(GLclampf @n, GLclampf @f);
        internal static glDepthRangefOESFunc glDepthRangefOESPtr;
        internal static void loadDepthRangefOES()
        {
            try
            {
                glDepthRangefOESPtr = (glDepthRangefOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangefOES"), typeof(glDepthRangefOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangefOES'.");
            }
        }
        public static void glDepthRangefOES(GLclampf @n, GLclampf @f) => glDepthRangefOESPtr(@n, @f);

        internal delegate void glFrustumfOESFunc(GLfloat @l, GLfloat @r, GLfloat @b, GLfloat @t, GLfloat @n, GLfloat @f);
        internal static glFrustumfOESFunc glFrustumfOESPtr;
        internal static void loadFrustumfOES()
        {
            try
            {
                glFrustumfOESPtr = (glFrustumfOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFrustumfOES"), typeof(glFrustumfOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFrustumfOES'.");
            }
        }
        public static void glFrustumfOES(GLfloat @l, GLfloat @r, GLfloat @b, GLfloat @t, GLfloat @n, GLfloat @f) => glFrustumfOESPtr(@l, @r, @b, @t, @n, @f);

        internal delegate void glGetClipPlanefOESFunc(GLenum @plane, GLfloat * @equation);
        internal static glGetClipPlanefOESFunc glGetClipPlanefOESPtr;
        internal static void loadGetClipPlanefOES()
        {
            try
            {
                glGetClipPlanefOESPtr = (glGetClipPlanefOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetClipPlanefOES"), typeof(glGetClipPlanefOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetClipPlanefOES'.");
            }
        }
        public static void glGetClipPlanefOES(GLenum @plane, GLfloat * @equation) => glGetClipPlanefOESPtr(@plane, @equation);

        internal delegate void glOrthofOESFunc(GLfloat @l, GLfloat @r, GLfloat @b, GLfloat @t, GLfloat @n, GLfloat @f);
        internal static glOrthofOESFunc glOrthofOESPtr;
        internal static void loadOrthofOES()
        {
            try
            {
                glOrthofOESPtr = (glOrthofOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glOrthofOES"), typeof(glOrthofOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glOrthofOES'.");
            }
        }
        public static void glOrthofOES(GLfloat @l, GLfloat @r, GLfloat @b, GLfloat @t, GLfloat @n, GLfloat @f) => glOrthofOESPtr(@l, @r, @b, @t, @n, @f);
        #endregion
    }
}
