using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_texture_storage_multisample_2d_array
    {
        #region Interop
        static GL_OES_texture_storage_multisample_2d_array()
        {
            Console.WriteLine("Initalising GL_OES_texture_storage_multisample_2d_array interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexStorage3DMultisampleOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_2D_MULTISAMPLE_ARRAY_OES = 0x9102;
        public static UInt32 GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY_OES = 0x9105;
        public static UInt32 GL_SAMPLER_2D_MULTISAMPLE_ARRAY_OES = 0x910B;
        public static UInt32 GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY_OES = 0x910C;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY_OES = 0x910D;
        #endregion

        #region Commands
        internal delegate void glTexStorage3DMultisampleOESFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations);
        internal static glTexStorage3DMultisampleOESFunc glTexStorage3DMultisampleOESPtr;
        internal static void loadTexStorage3DMultisampleOES()
        {
            try
            {
                glTexStorage3DMultisampleOESPtr = (glTexStorage3DMultisampleOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage3DMultisampleOES"), typeof(glTexStorage3DMultisampleOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage3DMultisampleOES'.");
            }
        }
        public static void glTexStorage3DMultisampleOES(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations) => glTexStorage3DMultisampleOESPtr(@target, @samples, @internalformat, @width, @height, @depth, @fixedsamplelocations);
        #endregion
    }
}
