using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_blend_subtract
    {
        #region Interop
        static GL_OES_blend_subtract()
        {
            Console.WriteLine("Initalising GL_OES_blend_subtract interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendEquationOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BLEND_EQUATION_OES = 0x8009;
        public static UInt32 GL_FUNC_ADD_OES = 0x8006;
        public static UInt32 GL_FUNC_SUBTRACT_OES = 0x800A;
        public static UInt32 GL_FUNC_REVERSE_SUBTRACT_OES = 0x800B;
        #endregion

        #region Commands
        internal delegate void glBlendEquationOESFunc(GLenum @mode);
        internal static glBlendEquationOESFunc glBlendEquationOESPtr;
        internal static void loadBlendEquationOES()
        {
            try
            {
                glBlendEquationOESPtr = (glBlendEquationOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationOES"), typeof(glBlendEquationOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationOES'.");
            }
        }
        public static void glBlendEquationOES(GLenum @mode) => glBlendEquationOESPtr(@mode);
        #endregion
    }
}
