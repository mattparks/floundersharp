using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_draw_texture
    {
        #region Interop
        static GL_OES_draw_texture()
        {
            Console.WriteLine("Initalising GL_OES_draw_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawTexsOES();
            loadDrawTexiOES();
            loadDrawTexxOES();
            loadDrawTexsvOES();
            loadDrawTexivOES();
            loadDrawTexxvOES();
            loadDrawTexfOES();
            loadDrawTexfvOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_CROP_RECT_OES = 0x8B9D;
        #endregion

        #region Commands
        internal delegate void glDrawTexsOESFunc(GLshort @x, GLshort @y, GLshort @z, GLshort @width, GLshort @height);
        internal static glDrawTexsOESFunc glDrawTexsOESPtr;
        internal static void loadDrawTexsOES()
        {
            try
            {
                glDrawTexsOESPtr = (glDrawTexsOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTexsOES"), typeof(glDrawTexsOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTexsOES'.");
            }
        }
        public static void glDrawTexsOES(GLshort @x, GLshort @y, GLshort @z, GLshort @width, GLshort @height) => glDrawTexsOESPtr(@x, @y, @z, @width, @height);

        internal delegate void glDrawTexiOESFunc(GLint @x, GLint @y, GLint @z, GLint @width, GLint @height);
        internal static glDrawTexiOESFunc glDrawTexiOESPtr;
        internal static void loadDrawTexiOES()
        {
            try
            {
                glDrawTexiOESPtr = (glDrawTexiOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTexiOES"), typeof(glDrawTexiOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTexiOES'.");
            }
        }
        public static void glDrawTexiOES(GLint @x, GLint @y, GLint @z, GLint @width, GLint @height) => glDrawTexiOESPtr(@x, @y, @z, @width, @height);

        internal delegate void glDrawTexxOESFunc(GLfixed @x, GLfixed @y, GLfixed @z, GLfixed @width, GLfixed @height);
        internal static glDrawTexxOESFunc glDrawTexxOESPtr;
        internal static void loadDrawTexxOES()
        {
            try
            {
                glDrawTexxOESPtr = (glDrawTexxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTexxOES"), typeof(glDrawTexxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTexxOES'.");
            }
        }
        public static void glDrawTexxOES(GLfixed @x, GLfixed @y, GLfixed @z, GLfixed @width, GLfixed @height) => glDrawTexxOESPtr(@x, @y, @z, @width, @height);

        internal delegate void glDrawTexsvOESFunc(const GLshort * @coords);
        internal static glDrawTexsvOESFunc glDrawTexsvOESPtr;
        internal static void loadDrawTexsvOES()
        {
            try
            {
                glDrawTexsvOESPtr = (glDrawTexsvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTexsvOES"), typeof(glDrawTexsvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTexsvOES'.");
            }
        }
        public static void glDrawTexsvOES(const GLshort * @coords) => glDrawTexsvOESPtr(@coords);

        internal delegate void glDrawTexivOESFunc(const GLint * @coords);
        internal static glDrawTexivOESFunc glDrawTexivOESPtr;
        internal static void loadDrawTexivOES()
        {
            try
            {
                glDrawTexivOESPtr = (glDrawTexivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTexivOES"), typeof(glDrawTexivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTexivOES'.");
            }
        }
        public static void glDrawTexivOES(const GLint * @coords) => glDrawTexivOESPtr(@coords);

        internal delegate void glDrawTexxvOESFunc(const GLfixed * @coords);
        internal static glDrawTexxvOESFunc glDrawTexxvOESPtr;
        internal static void loadDrawTexxvOES()
        {
            try
            {
                glDrawTexxvOESPtr = (glDrawTexxvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTexxvOES"), typeof(glDrawTexxvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTexxvOES'.");
            }
        }
        public static void glDrawTexxvOES(const GLfixed * @coords) => glDrawTexxvOESPtr(@coords);

        internal delegate void glDrawTexfOESFunc(GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @width, GLfloat @height);
        internal static glDrawTexfOESFunc glDrawTexfOESPtr;
        internal static void loadDrawTexfOES()
        {
            try
            {
                glDrawTexfOESPtr = (glDrawTexfOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTexfOES"), typeof(glDrawTexfOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTexfOES'.");
            }
        }
        public static void glDrawTexfOES(GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @width, GLfloat @height) => glDrawTexfOESPtr(@x, @y, @z, @width, @height);

        internal delegate void glDrawTexfvOESFunc(const GLfloat * @coords);
        internal static glDrawTexfvOESFunc glDrawTexfvOESPtr;
        internal static void loadDrawTexfvOES()
        {
            try
            {
                glDrawTexfvOESPtr = (glDrawTexfvOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTexfvOES"), typeof(glDrawTexfvOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTexfvOES'.");
            }
        }
        public static void glDrawTexfvOES(const GLfloat * @coords) => glDrawTexfvOESPtr(@coords);
        #endregion
    }
}
