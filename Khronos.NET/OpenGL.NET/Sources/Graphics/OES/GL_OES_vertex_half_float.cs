using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_vertex_half_float
    {
        #region Interop
        static GL_OES_vertex_half_float()
        {
            Console.WriteLine("Initalising GL_OES_vertex_half_float interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_HALF_FLOAT_OES = 0x8D61;
        #endregion

        #region Commands
        #endregion
    }
}
