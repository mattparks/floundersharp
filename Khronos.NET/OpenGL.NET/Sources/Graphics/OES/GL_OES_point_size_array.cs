using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_point_size_array
    {
        #region Interop
        static GL_OES_point_size_array()
        {
            Console.WriteLine("Initalising GL_OES_point_size_array interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPointSizePointerOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_POINT_SIZE_ARRAY_OES = 0x8B9C;
        public static UInt32 GL_POINT_SIZE_ARRAY_TYPE_OES = 0x898A;
        public static UInt32 GL_POINT_SIZE_ARRAY_STRIDE_OES = 0x898B;
        public static UInt32 GL_POINT_SIZE_ARRAY_POINTER_OES = 0x898C;
        public static UInt32 GL_POINT_SIZE_ARRAY_BUFFER_BINDING_OES = 0x8B9F;
        #endregion

        #region Commands
        internal delegate void glPointSizePointerOESFunc(GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glPointSizePointerOESFunc glPointSizePointerOESPtr;
        internal static void loadPointSizePointerOES()
        {
            try
            {
                glPointSizePointerOESPtr = (glPointSizePointerOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointSizePointerOES"), typeof(glPointSizePointerOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointSizePointerOES'.");
            }
        }
        public static void glPointSizePointerOES(GLenum @type, GLsizei @stride, const void * @pointer) => glPointSizePointerOESPtr(@type, @stride, @pointer);
        #endregion
    }
}
