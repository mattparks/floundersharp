using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_copy_image
    {
        #region Interop
        static GL_OES_copy_image()
        {
            Console.WriteLine("Initalising GL_OES_copy_image interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCopyImageSubDataOES();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glCopyImageSubDataOESFunc(GLuint @srcName, GLenum @srcTarget, GLint @srcLevel, GLint @srcX, GLint @srcY, GLint @srcZ, GLuint @dstName, GLenum @dstTarget, GLint @dstLevel, GLint @dstX, GLint @dstY, GLint @dstZ, GLsizei @srcWidth, GLsizei @srcHeight, GLsizei @srcDepth);
        internal static glCopyImageSubDataOESFunc glCopyImageSubDataOESPtr;
        internal static void loadCopyImageSubDataOES()
        {
            try
            {
                glCopyImageSubDataOESPtr = (glCopyImageSubDataOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyImageSubDataOES"), typeof(glCopyImageSubDataOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyImageSubDataOES'.");
            }
        }
        public static void glCopyImageSubDataOES(GLuint @srcName, GLenum @srcTarget, GLint @srcLevel, GLint @srcX, GLint @srcY, GLint @srcZ, GLuint @dstName, GLenum @dstTarget, GLint @dstLevel, GLint @dstX, GLint @dstY, GLint @dstZ, GLsizei @srcWidth, GLsizei @srcHeight, GLsizei @srcDepth) => glCopyImageSubDataOESPtr(@srcName, @srcTarget, @srcLevel, @srcX, @srcY, @srcZ, @dstName, @dstTarget, @dstLevel, @dstX, @dstY, @dstZ, @srcWidth, @srcHeight, @srcDepth);
        #endregion
    }
}
