using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_mapbuffer
    {
        #region Interop
        static GL_OES_mapbuffer()
        {
            Console.WriteLine("Initalising GL_OES_mapbuffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMapBufferOES();
            loadUnmapBufferOES();
            loadGetBufferPointervOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_WRITE_ONLY_OES = 0x88B9;
        public static UInt32 GL_BUFFER_ACCESS_OES = 0x88BB;
        public static UInt32 GL_BUFFER_MAPPED_OES = 0x88BC;
        public static UInt32 GL_BUFFER_MAP_POINTER_OES = 0x88BD;
        #endregion

        #region Commands
        internal delegate void * glMapBufferOESFunc(GLenum @target, GLenum @access);
        internal static glMapBufferOESFunc glMapBufferOESPtr;
        internal static void loadMapBufferOES()
        {
            try
            {
                glMapBufferOESPtr = (glMapBufferOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapBufferOES"), typeof(glMapBufferOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapBufferOES'.");
            }
        }
        public static void * glMapBufferOES(GLenum @target, GLenum @access) => glMapBufferOESPtr(@target, @access);

        internal delegate GLboolean glUnmapBufferOESFunc(GLenum @target);
        internal static glUnmapBufferOESFunc glUnmapBufferOESPtr;
        internal static void loadUnmapBufferOES()
        {
            try
            {
                glUnmapBufferOESPtr = (glUnmapBufferOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUnmapBufferOES"), typeof(glUnmapBufferOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUnmapBufferOES'.");
            }
        }
        public static GLboolean glUnmapBufferOES(GLenum @target) => glUnmapBufferOESPtr(@target);

        internal delegate void glGetBufferPointervOESFunc(GLenum @target, GLenum @pname, void ** @params);
        internal static glGetBufferPointervOESFunc glGetBufferPointervOESPtr;
        internal static void loadGetBufferPointervOES()
        {
            try
            {
                glGetBufferPointervOESPtr = (glGetBufferPointervOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferPointervOES"), typeof(glGetBufferPointervOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferPointervOES'.");
            }
        }
        public static void glGetBufferPointervOES(GLenum @target, GLenum @pname, void ** @params) => glGetBufferPointervOESPtr(@target, @pname, @params);
        #endregion
    }
}
