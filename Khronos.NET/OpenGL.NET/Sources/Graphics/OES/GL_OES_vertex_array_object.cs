using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_vertex_array_object
    {
        #region Interop
        static GL_OES_vertex_array_object()
        {
            Console.WriteLine("Initalising GL_OES_vertex_array_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindVertexArrayOES();
            loadDeleteVertexArraysOES();
            loadGenVertexArraysOES();
            loadIsVertexArrayOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ARRAY_BINDING_OES = 0x85B5;
        #endregion

        #region Commands
        internal delegate void glBindVertexArrayOESFunc(GLuint @array);
        internal static glBindVertexArrayOESFunc glBindVertexArrayOESPtr;
        internal static void loadBindVertexArrayOES()
        {
            try
            {
                glBindVertexArrayOESPtr = (glBindVertexArrayOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVertexArrayOES"), typeof(glBindVertexArrayOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVertexArrayOES'.");
            }
        }
        public static void glBindVertexArrayOES(GLuint @array) => glBindVertexArrayOESPtr(@array);

        internal delegate void glDeleteVertexArraysOESFunc(GLsizei @n, const GLuint * @arrays);
        internal static glDeleteVertexArraysOESFunc glDeleteVertexArraysOESPtr;
        internal static void loadDeleteVertexArraysOES()
        {
            try
            {
                glDeleteVertexArraysOESPtr = (glDeleteVertexArraysOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteVertexArraysOES"), typeof(glDeleteVertexArraysOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteVertexArraysOES'.");
            }
        }
        public static void glDeleteVertexArraysOES(GLsizei @n, const GLuint * @arrays) => glDeleteVertexArraysOESPtr(@n, @arrays);

        internal delegate void glGenVertexArraysOESFunc(GLsizei @n, GLuint * @arrays);
        internal static glGenVertexArraysOESFunc glGenVertexArraysOESPtr;
        internal static void loadGenVertexArraysOES()
        {
            try
            {
                glGenVertexArraysOESPtr = (glGenVertexArraysOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenVertexArraysOES"), typeof(glGenVertexArraysOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenVertexArraysOES'.");
            }
        }
        public static void glGenVertexArraysOES(GLsizei @n, GLuint * @arrays) => glGenVertexArraysOESPtr(@n, @arrays);

        internal delegate GLboolean glIsVertexArrayOESFunc(GLuint @array);
        internal static glIsVertexArrayOESFunc glIsVertexArrayOESPtr;
        internal static void loadIsVertexArrayOES()
        {
            try
            {
                glIsVertexArrayOESPtr = (glIsVertexArrayOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsVertexArrayOES"), typeof(glIsVertexArrayOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsVertexArrayOES'.");
            }
        }
        public static GLboolean glIsVertexArrayOES(GLuint @array) => glIsVertexArrayOESPtr(@array);
        #endregion
    }
}
