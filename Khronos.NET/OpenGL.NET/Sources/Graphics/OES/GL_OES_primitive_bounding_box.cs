using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_primitive_bounding_box
    {
        #region Interop
        static GL_OES_primitive_bounding_box()
        {
            Console.WriteLine("Initalising GL_OES_primitive_bounding_box interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPrimitiveBoundingBoxOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PRIMITIVE_BOUNDING_BOX_OES = 0x92BE;
        #endregion

        #region Commands
        internal delegate void glPrimitiveBoundingBoxOESFunc(GLfloat @minX, GLfloat @minY, GLfloat @minZ, GLfloat @minW, GLfloat @maxX, GLfloat @maxY, GLfloat @maxZ, GLfloat @maxW);
        internal static glPrimitiveBoundingBoxOESFunc glPrimitiveBoundingBoxOESPtr;
        internal static void loadPrimitiveBoundingBoxOES()
        {
            try
            {
                glPrimitiveBoundingBoxOESPtr = (glPrimitiveBoundingBoxOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPrimitiveBoundingBoxOES"), typeof(glPrimitiveBoundingBoxOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPrimitiveBoundingBoxOES'.");
            }
        }
        public static void glPrimitiveBoundingBoxOES(GLfloat @minX, GLfloat @minY, GLfloat @minZ, GLfloat @minW, GLfloat @maxX, GLfloat @maxY, GLfloat @maxZ, GLfloat @maxW) => glPrimitiveBoundingBoxOESPtr(@minX, @minY, @minZ, @minW, @maxX, @maxY, @maxZ, @maxW);
        #endregion
    }
}
