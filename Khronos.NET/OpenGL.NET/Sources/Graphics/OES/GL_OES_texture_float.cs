using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_texture_float
    {
        #region Interop
        static GL_OES_texture_float()
        {
            Console.WriteLine("Initalising GL_OES_texture_float interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FLOAT = 0x1406;
        #endregion

        #region Commands
        #endregion
    }
}
