using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_framebuffer_object
    {
        #region Interop
        static GL_OES_framebuffer_object()
        {
            Console.WriteLine("Initalising GL_OES_framebuffer_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadIsRenderbufferOES();
            loadBindRenderbufferOES();
            loadDeleteRenderbuffersOES();
            loadGenRenderbuffersOES();
            loadRenderbufferStorageOES();
            loadGetRenderbufferParameterivOES();
            loadIsFramebufferOES();
            loadBindFramebufferOES();
            loadDeleteFramebuffersOES();
            loadGenFramebuffersOES();
            loadCheckFramebufferStatusOES();
            loadFramebufferRenderbufferOES();
            loadFramebufferTexture2DOES();
            loadGetFramebufferAttachmentParameterivOES();
            loadGenerateMipmapOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_NONE_OES = 0;
        public static UInt32 GL_FRAMEBUFFER_OES = 0x8D40;
        public static UInt32 GL_RENDERBUFFER_OES = 0x8D41;
        public static UInt32 GL_RGBA4_OES = 0x8056;
        public static UInt32 GL_RGB5_A1_OES = 0x8057;
        public static UInt32 GL_RGB565_OES = 0x8D62;
        public static UInt32 GL_DEPTH_COMPONENT16_OES = 0x81A5;
        public static UInt32 GL_RENDERBUFFER_WIDTH_OES = 0x8D42;
        public static UInt32 GL_RENDERBUFFER_HEIGHT_OES = 0x8D43;
        public static UInt32 GL_RENDERBUFFER_INTERNAL_FORMAT_OES = 0x8D44;
        public static UInt32 GL_RENDERBUFFER_RED_SIZE_OES = 0x8D50;
        public static UInt32 GL_RENDERBUFFER_GREEN_SIZE_OES = 0x8D51;
        public static UInt32 GL_RENDERBUFFER_BLUE_SIZE_OES = 0x8D52;
        public static UInt32 GL_RENDERBUFFER_ALPHA_SIZE_OES = 0x8D53;
        public static UInt32 GL_RENDERBUFFER_DEPTH_SIZE_OES = 0x8D54;
        public static UInt32 GL_RENDERBUFFER_STENCIL_SIZE_OES = 0x8D55;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_OES = 0x8CD0;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_OES = 0x8CD1;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL_OES = 0x8CD2;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE_OES = 0x8CD3;
        public static UInt32 GL_COLOR_ATTACHMENT0_OES = 0x8CE0;
        public static UInt32 GL_DEPTH_ATTACHMENT_OES = 0x8D00;
        public static UInt32 GL_STENCIL_ATTACHMENT_OES = 0x8D20;
        public static UInt32 GL_FRAMEBUFFER_COMPLETE_OES = 0x8CD5;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_OES = 0x8CD6;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_OES = 0x8CD7;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_OES = 0x8CD9;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_FORMATS_OES = 0x8CDA;
        public static UInt32 GL_FRAMEBUFFER_UNSUPPORTED_OES = 0x8CDD;
        public static UInt32 GL_FRAMEBUFFER_BINDING_OES = 0x8CA6;
        public static UInt32 GL_RENDERBUFFER_BINDING_OES = 0x8CA7;
        public static UInt32 GL_MAX_RENDERBUFFER_SIZE_OES = 0x84E8;
        public static UInt32 GL_INVALID_FRAMEBUFFER_OPERATION_OES = 0x0506;
        #endregion

        #region Commands
        internal delegate GLboolean glIsRenderbufferOESFunc(GLuint @renderbuffer);
        internal static glIsRenderbufferOESFunc glIsRenderbufferOESPtr;
        internal static void loadIsRenderbufferOES()
        {
            try
            {
                glIsRenderbufferOESPtr = (glIsRenderbufferOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsRenderbufferOES"), typeof(glIsRenderbufferOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsRenderbufferOES'.");
            }
        }
        public static GLboolean glIsRenderbufferOES(GLuint @renderbuffer) => glIsRenderbufferOESPtr(@renderbuffer);

        internal delegate void glBindRenderbufferOESFunc(GLenum @target, GLuint @renderbuffer);
        internal static glBindRenderbufferOESFunc glBindRenderbufferOESPtr;
        internal static void loadBindRenderbufferOES()
        {
            try
            {
                glBindRenderbufferOESPtr = (glBindRenderbufferOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindRenderbufferOES"), typeof(glBindRenderbufferOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindRenderbufferOES'.");
            }
        }
        public static void glBindRenderbufferOES(GLenum @target, GLuint @renderbuffer) => glBindRenderbufferOESPtr(@target, @renderbuffer);

        internal delegate void glDeleteRenderbuffersOESFunc(GLsizei @n, const GLuint * @renderbuffers);
        internal static glDeleteRenderbuffersOESFunc glDeleteRenderbuffersOESPtr;
        internal static void loadDeleteRenderbuffersOES()
        {
            try
            {
                glDeleteRenderbuffersOESPtr = (glDeleteRenderbuffersOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteRenderbuffersOES"), typeof(glDeleteRenderbuffersOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteRenderbuffersOES'.");
            }
        }
        public static void glDeleteRenderbuffersOES(GLsizei @n, const GLuint * @renderbuffers) => glDeleteRenderbuffersOESPtr(@n, @renderbuffers);

        internal delegate void glGenRenderbuffersOESFunc(GLsizei @n, GLuint * @renderbuffers);
        internal static glGenRenderbuffersOESFunc glGenRenderbuffersOESPtr;
        internal static void loadGenRenderbuffersOES()
        {
            try
            {
                glGenRenderbuffersOESPtr = (glGenRenderbuffersOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenRenderbuffersOES"), typeof(glGenRenderbuffersOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenRenderbuffersOES'.");
            }
        }
        public static void glGenRenderbuffersOES(GLsizei @n, GLuint * @renderbuffers) => glGenRenderbuffersOESPtr(@n, @renderbuffers);

        internal delegate void glRenderbufferStorageOESFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageOESFunc glRenderbufferStorageOESPtr;
        internal static void loadRenderbufferStorageOES()
        {
            try
            {
                glRenderbufferStorageOESPtr = (glRenderbufferStorageOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorageOES"), typeof(glRenderbufferStorageOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorageOES'.");
            }
        }
        public static void glRenderbufferStorageOES(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStorageOESPtr(@target, @internalformat, @width, @height);

        internal delegate void glGetRenderbufferParameterivOESFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetRenderbufferParameterivOESFunc glGetRenderbufferParameterivOESPtr;
        internal static void loadGetRenderbufferParameterivOES()
        {
            try
            {
                glGetRenderbufferParameterivOESPtr = (glGetRenderbufferParameterivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetRenderbufferParameterivOES"), typeof(glGetRenderbufferParameterivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetRenderbufferParameterivOES'.");
            }
        }
        public static void glGetRenderbufferParameterivOES(GLenum @target, GLenum @pname, GLint * @params) => glGetRenderbufferParameterivOESPtr(@target, @pname, @params);

        internal delegate GLboolean glIsFramebufferOESFunc(GLuint @framebuffer);
        internal static glIsFramebufferOESFunc glIsFramebufferOESPtr;
        internal static void loadIsFramebufferOES()
        {
            try
            {
                glIsFramebufferOESPtr = (glIsFramebufferOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsFramebufferOES"), typeof(glIsFramebufferOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsFramebufferOES'.");
            }
        }
        public static GLboolean glIsFramebufferOES(GLuint @framebuffer) => glIsFramebufferOESPtr(@framebuffer);

        internal delegate void glBindFramebufferOESFunc(GLenum @target, GLuint @framebuffer);
        internal static glBindFramebufferOESFunc glBindFramebufferOESPtr;
        internal static void loadBindFramebufferOES()
        {
            try
            {
                glBindFramebufferOESPtr = (glBindFramebufferOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindFramebufferOES"), typeof(glBindFramebufferOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindFramebufferOES'.");
            }
        }
        public static void glBindFramebufferOES(GLenum @target, GLuint @framebuffer) => glBindFramebufferOESPtr(@target, @framebuffer);

        internal delegate void glDeleteFramebuffersOESFunc(GLsizei @n, const GLuint * @framebuffers);
        internal static glDeleteFramebuffersOESFunc glDeleteFramebuffersOESPtr;
        internal static void loadDeleteFramebuffersOES()
        {
            try
            {
                glDeleteFramebuffersOESPtr = (glDeleteFramebuffersOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteFramebuffersOES"), typeof(glDeleteFramebuffersOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteFramebuffersOES'.");
            }
        }
        public static void glDeleteFramebuffersOES(GLsizei @n, const GLuint * @framebuffers) => glDeleteFramebuffersOESPtr(@n, @framebuffers);

        internal delegate void glGenFramebuffersOESFunc(GLsizei @n, GLuint * @framebuffers);
        internal static glGenFramebuffersOESFunc glGenFramebuffersOESPtr;
        internal static void loadGenFramebuffersOES()
        {
            try
            {
                glGenFramebuffersOESPtr = (glGenFramebuffersOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenFramebuffersOES"), typeof(glGenFramebuffersOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenFramebuffersOES'.");
            }
        }
        public static void glGenFramebuffersOES(GLsizei @n, GLuint * @framebuffers) => glGenFramebuffersOESPtr(@n, @framebuffers);

        internal delegate GLenum glCheckFramebufferStatusOESFunc(GLenum @target);
        internal static glCheckFramebufferStatusOESFunc glCheckFramebufferStatusOESPtr;
        internal static void loadCheckFramebufferStatusOES()
        {
            try
            {
                glCheckFramebufferStatusOESPtr = (glCheckFramebufferStatusOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCheckFramebufferStatusOES"), typeof(glCheckFramebufferStatusOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCheckFramebufferStatusOES'.");
            }
        }
        public static GLenum glCheckFramebufferStatusOES(GLenum @target) => glCheckFramebufferStatusOESPtr(@target);

        internal delegate void glFramebufferRenderbufferOESFunc(GLenum @target, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer);
        internal static glFramebufferRenderbufferOESFunc glFramebufferRenderbufferOESPtr;
        internal static void loadFramebufferRenderbufferOES()
        {
            try
            {
                glFramebufferRenderbufferOESPtr = (glFramebufferRenderbufferOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferRenderbufferOES"), typeof(glFramebufferRenderbufferOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferRenderbufferOES'.");
            }
        }
        public static void glFramebufferRenderbufferOES(GLenum @target, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer) => glFramebufferRenderbufferOESPtr(@target, @attachment, @renderbuffertarget, @renderbuffer);

        internal delegate void glFramebufferTexture2DOESFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level);
        internal static glFramebufferTexture2DOESFunc glFramebufferTexture2DOESPtr;
        internal static void loadFramebufferTexture2DOES()
        {
            try
            {
                glFramebufferTexture2DOESPtr = (glFramebufferTexture2DOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture2DOES"), typeof(glFramebufferTexture2DOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture2DOES'.");
            }
        }
        public static void glFramebufferTexture2DOES(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level) => glFramebufferTexture2DOESPtr(@target, @attachment, @textarget, @texture, @level);

        internal delegate void glGetFramebufferAttachmentParameterivOESFunc(GLenum @target, GLenum @attachment, GLenum @pname, GLint * @params);
        internal static glGetFramebufferAttachmentParameterivOESFunc glGetFramebufferAttachmentParameterivOESPtr;
        internal static void loadGetFramebufferAttachmentParameterivOES()
        {
            try
            {
                glGetFramebufferAttachmentParameterivOESPtr = (glGetFramebufferAttachmentParameterivOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFramebufferAttachmentParameterivOES"), typeof(glGetFramebufferAttachmentParameterivOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFramebufferAttachmentParameterivOES'.");
            }
        }
        public static void glGetFramebufferAttachmentParameterivOES(GLenum @target, GLenum @attachment, GLenum @pname, GLint * @params) => glGetFramebufferAttachmentParameterivOESPtr(@target, @attachment, @pname, @params);

        internal delegate void glGenerateMipmapOESFunc(GLenum @target);
        internal static glGenerateMipmapOESFunc glGenerateMipmapOESPtr;
        internal static void loadGenerateMipmapOES()
        {
            try
            {
                glGenerateMipmapOESPtr = (glGenerateMipmapOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenerateMipmapOES"), typeof(glGenerateMipmapOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenerateMipmapOES'.");
            }
        }
        public static void glGenerateMipmapOES(GLenum @target) => glGenerateMipmapOESPtr(@target);
        #endregion
    }
}
