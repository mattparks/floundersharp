using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_blend_equation_separate
    {
        #region Interop
        static GL_OES_blend_equation_separate()
        {
            Console.WriteLine("Initalising GL_OES_blend_equation_separate interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendEquationSeparateOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BLEND_EQUATION_RGB_OES = 0x8009;
        public static UInt32 GL_BLEND_EQUATION_ALPHA_OES = 0x883D;
        #endregion

        #region Commands
        internal delegate void glBlendEquationSeparateOESFunc(GLenum @modeRGB, GLenum @modeAlpha);
        internal static glBlendEquationSeparateOESFunc glBlendEquationSeparateOESPtr;
        internal static void loadBlendEquationSeparateOES()
        {
            try
            {
                glBlendEquationSeparateOESPtr = (glBlendEquationSeparateOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationSeparateOES"), typeof(glBlendEquationSeparateOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationSeparateOES'.");
            }
        }
        public static void glBlendEquationSeparateOES(GLenum @modeRGB, GLenum @modeAlpha) => glBlendEquationSeparateOESPtr(@modeRGB, @modeAlpha);
        #endregion
    }
}
