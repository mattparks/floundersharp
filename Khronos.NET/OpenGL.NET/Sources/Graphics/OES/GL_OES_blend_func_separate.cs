using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OES_blend_func_separate
    {
        #region Interop
        static GL_OES_blend_func_separate()
        {
            Console.WriteLine("Initalising GL_OES_blend_func_separate interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendFuncSeparateOES();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BLEND_DST_RGB_OES = 0x80C8;
        public static UInt32 GL_BLEND_SRC_RGB_OES = 0x80C9;
        public static UInt32 GL_BLEND_DST_ALPHA_OES = 0x80CA;
        public static UInt32 GL_BLEND_SRC_ALPHA_OES = 0x80CB;
        #endregion

        #region Commands
        internal delegate void glBlendFuncSeparateOESFunc(GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha);
        internal static glBlendFuncSeparateOESFunc glBlendFuncSeparateOESPtr;
        internal static void loadBlendFuncSeparateOES()
        {
            try
            {
                glBlendFuncSeparateOESPtr = (glBlendFuncSeparateOESFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFuncSeparateOES"), typeof(glBlendFuncSeparateOESFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFuncSeparateOES'.");
            }
        }
        public static void glBlendFuncSeparateOES(GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha) => glBlendFuncSeparateOESPtr(@srcRGB, @dstRGB, @srcAlpha, @dstAlpha);
        #endregion
    }
}
