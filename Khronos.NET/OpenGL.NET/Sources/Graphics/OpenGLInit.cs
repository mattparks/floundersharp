using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class OpenGLInit
    {
        internal static Func<string, IntPtr> GetProcAddress;

        public static void Init(Func<string, IntPtr> @procAddress)
        {
            GetProcAddress = @procAddress;
        }
    }
}
