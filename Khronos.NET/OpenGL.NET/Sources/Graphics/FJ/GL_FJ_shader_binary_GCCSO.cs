using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_FJ_shader_binary_GCCSO
    {
        #region Interop
        static GL_FJ_shader_binary_GCCSO()
        {
            Console.WriteLine("Initalising GL_FJ_shader_binary_GCCSO interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_GCCSO_SHADER_BINARY_FJ = 0x9260;
        #endregion

        #region Commands
        #endregion
    }
}
