using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_async_histogram
    {
        #region Interop
        static GL_SGIX_async_histogram()
        {
            Console.WriteLine("Initalising GL_SGIX_async_histogram interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_ASYNC_HISTOGRAM_SGIX = 0x832C;
        public static UInt32 GL_MAX_ASYNC_HISTOGRAM_SGIX = 0x832D;
        #endregion

        #region Commands
        #endregion
    }
}
