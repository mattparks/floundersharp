using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_clipmap
    {
        #region Interop
        static GL_SGIX_clipmap()
        {
            Console.WriteLine("Initalising GL_SGIX_clipmap interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_LINEAR_CLIPMAP_LINEAR_SGIX = 0x8170;
        public static UInt32 GL_TEXTURE_CLIPMAP_CENTER_SGIX = 0x8171;
        public static UInt32 GL_TEXTURE_CLIPMAP_FRAME_SGIX = 0x8172;
        public static UInt32 GL_TEXTURE_CLIPMAP_OFFSET_SGIX = 0x8173;
        public static UInt32 GL_TEXTURE_CLIPMAP_VIRTUAL_DEPTH_SGIX = 0x8174;
        public static UInt32 GL_TEXTURE_CLIPMAP_LOD_OFFSET_SGIX = 0x8175;
        public static UInt32 GL_TEXTURE_CLIPMAP_DEPTH_SGIX = 0x8176;
        public static UInt32 GL_MAX_CLIPMAP_DEPTH_SGIX = 0x8177;
        public static UInt32 GL_MAX_CLIPMAP_VIRTUAL_DEPTH_SGIX = 0x8178;
        public static UInt32 GL_NEAREST_CLIPMAP_NEAREST_SGIX = 0x844D;
        public static UInt32 GL_NEAREST_CLIPMAP_LINEAR_SGIX = 0x844E;
        public static UInt32 GL_LINEAR_CLIPMAP_NEAREST_SGIX = 0x844F;
        #endregion

        #region Commands
        #endregion
    }
}
