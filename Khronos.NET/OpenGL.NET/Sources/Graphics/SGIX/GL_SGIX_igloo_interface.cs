using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_igloo_interface
    {
        #region Interop
        static GL_SGIX_igloo_interface()
        {
            Console.WriteLine("Initalising GL_SGIX_igloo_interface interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadIooInterfaceSGIX();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glIglooInterfaceSGIXFunc(GLenum @pname, const void * @params);
        internal static glIglooInterfaceSGIXFunc glIglooInterfaceSGIXPtr;
        internal static void loadIooInterfaceSGIX()
        {
            try
            {
                glIglooInterfaceSGIXPtr = (glIglooInterfaceSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIglooInterfaceSGIX"), typeof(glIglooInterfaceSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIglooInterfaceSGIX'.");
            }
        }
        public static void glIglooInterfaceSGIX(GLenum @pname, const void * @params) => glIglooInterfaceSGIXPtr(@pname, @params);
        #endregion
    }
}
