using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_sprite
    {
        #region Interop
        static GL_SGIX_sprite()
        {
            Console.WriteLine("Initalising GL_SGIX_sprite interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadSpriteParameterfSGIX();
            loadSpriteParameterfvSGIX();
            loadSpriteParameteriSGIX();
            loadSpriteParameterivSGIX();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SPRITE_SGIX = 0x8148;
        public static UInt32 GL_SPRITE_MODE_SGIX = 0x8149;
        public static UInt32 GL_SPRITE_AXIS_SGIX = 0x814A;
        public static UInt32 GL_SPRITE_TRANSLATION_SGIX = 0x814B;
        public static UInt32 GL_SPRITE_AXIAL_SGIX = 0x814C;
        public static UInt32 GL_SPRITE_OBJECT_ALIGNED_SGIX = 0x814D;
        public static UInt32 GL_SPRITE_EYE_ALIGNED_SGIX = 0x814E;
        #endregion

        #region Commands
        internal delegate void glSpriteParameterfSGIXFunc(GLenum @pname, GLfloat @param);
        internal static glSpriteParameterfSGIXFunc glSpriteParameterfSGIXPtr;
        internal static void loadSpriteParameterfSGIX()
        {
            try
            {
                glSpriteParameterfSGIXPtr = (glSpriteParameterfSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSpriteParameterfSGIX"), typeof(glSpriteParameterfSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSpriteParameterfSGIX'.");
            }
        }
        public static void glSpriteParameterfSGIX(GLenum @pname, GLfloat @param) => glSpriteParameterfSGIXPtr(@pname, @param);

        internal delegate void glSpriteParameterfvSGIXFunc(GLenum @pname, const GLfloat * @params);
        internal static glSpriteParameterfvSGIXFunc glSpriteParameterfvSGIXPtr;
        internal static void loadSpriteParameterfvSGIX()
        {
            try
            {
                glSpriteParameterfvSGIXPtr = (glSpriteParameterfvSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSpriteParameterfvSGIX"), typeof(glSpriteParameterfvSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSpriteParameterfvSGIX'.");
            }
        }
        public static void glSpriteParameterfvSGIX(GLenum @pname, const GLfloat * @params) => glSpriteParameterfvSGIXPtr(@pname, @params);

        internal delegate void glSpriteParameteriSGIXFunc(GLenum @pname, GLint @param);
        internal static glSpriteParameteriSGIXFunc glSpriteParameteriSGIXPtr;
        internal static void loadSpriteParameteriSGIX()
        {
            try
            {
                glSpriteParameteriSGIXPtr = (glSpriteParameteriSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSpriteParameteriSGIX"), typeof(glSpriteParameteriSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSpriteParameteriSGIX'.");
            }
        }
        public static void glSpriteParameteriSGIX(GLenum @pname, GLint @param) => glSpriteParameteriSGIXPtr(@pname, @param);

        internal delegate void glSpriteParameterivSGIXFunc(GLenum @pname, const GLint * @params);
        internal static glSpriteParameterivSGIXFunc glSpriteParameterivSGIXPtr;
        internal static void loadSpriteParameterivSGIX()
        {
            try
            {
                glSpriteParameterivSGIXPtr = (glSpriteParameterivSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSpriteParameterivSGIX"), typeof(glSpriteParameterivSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSpriteParameterivSGIX'.");
            }
        }
        public static void glSpriteParameterivSGIX(GLenum @pname, const GLint * @params) => glSpriteParameterivSGIXPtr(@pname, @params);
        #endregion
    }
}
