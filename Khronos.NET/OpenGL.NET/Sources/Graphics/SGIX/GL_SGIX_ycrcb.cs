using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_ycrcb
    {
        #region Interop
        static GL_SGIX_ycrcb()
        {
            Console.WriteLine("Initalising GL_SGIX_ycrcb interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_YCRCB_422_SGIX = 0x81BB;
        public static UInt32 GL_YCRCB_444_SGIX = 0x81BC;
        #endregion

        #region Commands
        #endregion
    }
}
