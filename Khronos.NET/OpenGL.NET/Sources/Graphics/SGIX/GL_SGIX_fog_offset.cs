using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_fog_offset
    {
        #region Interop
        static GL_SGIX_fog_offset()
        {
            Console.WriteLine("Initalising GL_SGIX_fog_offset interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FOG_OFFSET_SGIX = 0x8198;
        public static UInt32 GL_FOG_OFFSET_VALUE_SGIX = 0x8199;
        #endregion

        #region Commands
        #endregion
    }
}
