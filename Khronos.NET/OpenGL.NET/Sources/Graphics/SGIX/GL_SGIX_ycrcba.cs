using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_ycrcba
    {
        #region Interop
        static GL_SGIX_ycrcba()
        {
            Console.WriteLine("Initalising GL_SGIX_ycrcba interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_YCRCB_SGIX = 0x8318;
        public static UInt32 GL_YCRCBA_SGIX = 0x8319;
        #endregion

        #region Commands
        #endregion
    }
}
