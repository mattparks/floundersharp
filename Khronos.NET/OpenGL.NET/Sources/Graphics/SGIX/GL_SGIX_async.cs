using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_async
    {
        #region Interop
        static GL_SGIX_async()
        {
            Console.WriteLine("Initalising GL_SGIX_async interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadAsyncMarkerSGIX();
            loadFinishAsyncSGIX();
            loadPollAsyncSGIX();
            loadGenAsyncMarkersSGIX();
            loadDeleteAsyncMarkersSGIX();
            loadIsAsyncMarkerSGIX();
        }
        #endregion

        #region Enums
        public static UInt32 GL_ASYNC_MARKER_SGIX = 0x8329;
        #endregion

        #region Commands
        internal delegate void glAsyncMarkerSGIXFunc(GLuint @marker);
        internal static glAsyncMarkerSGIXFunc glAsyncMarkerSGIXPtr;
        internal static void loadAsyncMarkerSGIX()
        {
            try
            {
                glAsyncMarkerSGIXPtr = (glAsyncMarkerSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAsyncMarkerSGIX"), typeof(glAsyncMarkerSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAsyncMarkerSGIX'.");
            }
        }
        public static void glAsyncMarkerSGIX(GLuint @marker) => glAsyncMarkerSGIXPtr(@marker);

        internal delegate GLint glFinishAsyncSGIXFunc(GLuint * @markerp);
        internal static glFinishAsyncSGIXFunc glFinishAsyncSGIXPtr;
        internal static void loadFinishAsyncSGIX()
        {
            try
            {
                glFinishAsyncSGIXPtr = (glFinishAsyncSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFinishAsyncSGIX"), typeof(glFinishAsyncSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFinishAsyncSGIX'.");
            }
        }
        public static GLint glFinishAsyncSGIX(GLuint * @markerp) => glFinishAsyncSGIXPtr(@markerp);

        internal delegate GLint glPollAsyncSGIXFunc(GLuint * @markerp);
        internal static glPollAsyncSGIXFunc glPollAsyncSGIXPtr;
        internal static void loadPollAsyncSGIX()
        {
            try
            {
                glPollAsyncSGIXPtr = (glPollAsyncSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPollAsyncSGIX"), typeof(glPollAsyncSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPollAsyncSGIX'.");
            }
        }
        public static GLint glPollAsyncSGIX(GLuint * @markerp) => glPollAsyncSGIXPtr(@markerp);

        internal delegate GLuint glGenAsyncMarkersSGIXFunc(GLsizei @range);
        internal static glGenAsyncMarkersSGIXFunc glGenAsyncMarkersSGIXPtr;
        internal static void loadGenAsyncMarkersSGIX()
        {
            try
            {
                glGenAsyncMarkersSGIXPtr = (glGenAsyncMarkersSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenAsyncMarkersSGIX"), typeof(glGenAsyncMarkersSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenAsyncMarkersSGIX'.");
            }
        }
        public static GLuint glGenAsyncMarkersSGIX(GLsizei @range) => glGenAsyncMarkersSGIXPtr(@range);

        internal delegate void glDeleteAsyncMarkersSGIXFunc(GLuint @marker, GLsizei @range);
        internal static glDeleteAsyncMarkersSGIXFunc glDeleteAsyncMarkersSGIXPtr;
        internal static void loadDeleteAsyncMarkersSGIX()
        {
            try
            {
                glDeleteAsyncMarkersSGIXPtr = (glDeleteAsyncMarkersSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteAsyncMarkersSGIX"), typeof(glDeleteAsyncMarkersSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteAsyncMarkersSGIX'.");
            }
        }
        public static void glDeleteAsyncMarkersSGIX(GLuint @marker, GLsizei @range) => glDeleteAsyncMarkersSGIXPtr(@marker, @range);

        internal delegate GLboolean glIsAsyncMarkerSGIXFunc(GLuint @marker);
        internal static glIsAsyncMarkerSGIXFunc glIsAsyncMarkerSGIXPtr;
        internal static void loadIsAsyncMarkerSGIX()
        {
            try
            {
                glIsAsyncMarkerSGIXPtr = (glIsAsyncMarkerSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsAsyncMarkerSGIX"), typeof(glIsAsyncMarkerSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsAsyncMarkerSGIX'.");
            }
        }
        public static GLboolean glIsAsyncMarkerSGIX(GLuint @marker) => glIsAsyncMarkerSGIXPtr(@marker);
        #endregion
    }
}
