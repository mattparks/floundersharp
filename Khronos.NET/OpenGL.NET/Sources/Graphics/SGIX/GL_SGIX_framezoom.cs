using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_framezoom
    {
        #region Interop
        static GL_SGIX_framezoom()
        {
            Console.WriteLine("Initalising GL_SGIX_framezoom interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFrameZoomSGIX();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAMEZOOM_SGIX = 0x818B;
        public static UInt32 GL_FRAMEZOOM_FACTOR_SGIX = 0x818C;
        public static UInt32 GL_MAX_FRAMEZOOM_FACTOR_SGIX = 0x818D;
        #endregion

        #region Commands
        internal delegate void glFrameZoomSGIXFunc(GLint @factor);
        internal static glFrameZoomSGIXFunc glFrameZoomSGIXPtr;
        internal static void loadFrameZoomSGIX()
        {
            try
            {
                glFrameZoomSGIXPtr = (glFrameZoomSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFrameZoomSGIX"), typeof(glFrameZoomSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFrameZoomSGIX'.");
            }
        }
        public static void glFrameZoomSGIX(GLint @factor) => glFrameZoomSGIXPtr(@factor);
        #endregion
    }
}
