using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_texture_coordinate_clamp
    {
        #region Interop
        static GL_SGIX_texture_coordinate_clamp()
        {
            Console.WriteLine("Initalising GL_SGIX_texture_coordinate_clamp interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_MAX_CLAMP_S_SGIX = 0x8369;
        public static UInt32 GL_TEXTURE_MAX_CLAMP_T_SGIX = 0x836A;
        public static UInt32 GL_TEXTURE_MAX_CLAMP_R_SGIX = 0x836B;
        #endregion

        #region Commands
        #endregion
    }
}
