using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_polynomial_ffd
    {
        #region Interop
        static GL_SGIX_polynomial_ffd()
        {
            Console.WriteLine("Initalising GL_SGIX_polynomial_ffd interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDeformationMap3dSGIX();
            loadDeformationMap3fSGIX();
            loadDeformSGIX();
            loadLoadIdentityDeformationMapSGIX();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_DEFORMATION_BIT_SGIX = 0x00000001;
        public static UInt32 GL_GEOMETRY_DEFORMATION_BIT_SGIX = 0x00000002;
        public static UInt32 GL_GEOMETRY_DEFORMATION_SGIX = 0x8194;
        public static UInt32 GL_TEXTURE_DEFORMATION_SGIX = 0x8195;
        public static UInt32 GL_DEFORMATIONS_MASK_SGIX = 0x8196;
        public static UInt32 GL_MAX_DEFORMATION_ORDER_SGIX = 0x8197;
        #endregion

        #region Commands
        internal delegate void glDeformationMap3dSGIXFunc(GLenum @target, GLdouble @u1, GLdouble @u2, GLint @ustride, GLint @uorder, GLdouble @v1, GLdouble @v2, GLint @vstride, GLint @vorder, GLdouble @w1, GLdouble @w2, GLint @wstride, GLint @worder, const GLdouble * @points);
        internal static glDeformationMap3dSGIXFunc glDeformationMap3dSGIXPtr;
        internal static void loadDeformationMap3dSGIX()
        {
            try
            {
                glDeformationMap3dSGIXPtr = (glDeformationMap3dSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeformationMap3dSGIX"), typeof(glDeformationMap3dSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeformationMap3dSGIX'.");
            }
        }
        public static void glDeformationMap3dSGIX(GLenum @target, GLdouble @u1, GLdouble @u2, GLint @ustride, GLint @uorder, GLdouble @v1, GLdouble @v2, GLint @vstride, GLint @vorder, GLdouble @w1, GLdouble @w2, GLint @wstride, GLint @worder, const GLdouble * @points) => glDeformationMap3dSGIXPtr(@target, @u1, @u2, @ustride, @uorder, @v1, @v2, @vstride, @vorder, @w1, @w2, @wstride, @worder, @points);

        internal delegate void glDeformationMap3fSGIXFunc(GLenum @target, GLfloat @u1, GLfloat @u2, GLint @ustride, GLint @uorder, GLfloat @v1, GLfloat @v2, GLint @vstride, GLint @vorder, GLfloat @w1, GLfloat @w2, GLint @wstride, GLint @worder, const GLfloat * @points);
        internal static glDeformationMap3fSGIXFunc glDeformationMap3fSGIXPtr;
        internal static void loadDeformationMap3fSGIX()
        {
            try
            {
                glDeformationMap3fSGIXPtr = (glDeformationMap3fSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeformationMap3fSGIX"), typeof(glDeformationMap3fSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeformationMap3fSGIX'.");
            }
        }
        public static void glDeformationMap3fSGIX(GLenum @target, GLfloat @u1, GLfloat @u2, GLint @ustride, GLint @uorder, GLfloat @v1, GLfloat @v2, GLint @vstride, GLint @vorder, GLfloat @w1, GLfloat @w2, GLint @wstride, GLint @worder, const GLfloat * @points) => glDeformationMap3fSGIXPtr(@target, @u1, @u2, @ustride, @uorder, @v1, @v2, @vstride, @vorder, @w1, @w2, @wstride, @worder, @points);

        internal delegate void glDeformSGIXFunc(GLbitfield @mask);
        internal static glDeformSGIXFunc glDeformSGIXPtr;
        internal static void loadDeformSGIX()
        {
            try
            {
                glDeformSGIXPtr = (glDeformSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeformSGIX"), typeof(glDeformSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeformSGIX'.");
            }
        }
        public static void glDeformSGIX(GLbitfield @mask) => glDeformSGIXPtr(@mask);

        internal delegate void glLoadIdentityDeformationMapSGIXFunc(GLbitfield @mask);
        internal static glLoadIdentityDeformationMapSGIXFunc glLoadIdentityDeformationMapSGIXPtr;
        internal static void loadLoadIdentityDeformationMapSGIX()
        {
            try
            {
                glLoadIdentityDeformationMapSGIXPtr = (glLoadIdentityDeformationMapSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadIdentityDeformationMapSGIX"), typeof(glLoadIdentityDeformationMapSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadIdentityDeformationMapSGIX'.");
            }
        }
        public static void glLoadIdentityDeformationMapSGIX(GLbitfield @mask) => glLoadIdentityDeformationMapSGIXPtr(@mask);
        #endregion
    }
}
