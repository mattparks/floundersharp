using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_fragment_lighting
    {
        #region Interop
        static GL_SGIX_fragment_lighting()
        {
            Console.WriteLine("Initalising GL_SGIX_fragment_lighting interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFragmentColorMaterialSGIX();
            loadFragmentLightfSGIX();
            loadFragmentLightfvSGIX();
            loadFragmentLightiSGIX();
            loadFragmentLightivSGIX();
            loadFragmentLightModelfSGIX();
            loadFragmentLightModelfvSGIX();
            loadFragmentLightModeliSGIX();
            loadFragmentLightModelivSGIX();
            loadFragmentMaterialfSGIX();
            loadFragmentMaterialfvSGIX();
            loadFragmentMaterialiSGIX();
            loadFragmentMaterialivSGIX();
            loadGetFragmentLightfvSGIX();
            loadGetFragmentLightivSGIX();
            loadGetFragmentMaterialfvSGIX();
            loadGetFragmentMaterialivSGIX();
            loadLightEnviSGIX();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAGMENT_LIGHTING_SGIX = 0x8400;
        public static UInt32 GL_FRAGMENT_COLOR_MATERIAL_SGIX = 0x8401;
        public static UInt32 GL_FRAGMENT_COLOR_MATERIAL_FACE_SGIX = 0x8402;
        public static UInt32 GL_FRAGMENT_COLOR_MATERIAL_PARAMETER_SGIX = 0x8403;
        public static UInt32 GL_MAX_FRAGMENT_LIGHTS_SGIX = 0x8404;
        public static UInt32 GL_MAX_ACTIVE_LIGHTS_SGIX = 0x8405;
        public static UInt32 GL_CURRENT_RASTER_NORMAL_SGIX = 0x8406;
        public static UInt32 GL_LIGHT_ENV_MODE_SGIX = 0x8407;
        public static UInt32 GL_FRAGMENT_LIGHT_MODEL_LOCAL_VIEWER_SGIX = 0x8408;
        public static UInt32 GL_FRAGMENT_LIGHT_MODEL_TWO_SIDE_SGIX = 0x8409;
        public static UInt32 GL_FRAGMENT_LIGHT_MODEL_AMBIENT_SGIX = 0x840A;
        public static UInt32 GL_FRAGMENT_LIGHT_MODEL_NORMAL_INTERPOLATION_SGIX = 0x840B;
        public static UInt32 GL_FRAGMENT_LIGHT0_SGIX = 0x840C;
        public static UInt32 GL_FRAGMENT_LIGHT1_SGIX = 0x840D;
        public static UInt32 GL_FRAGMENT_LIGHT2_SGIX = 0x840E;
        public static UInt32 GL_FRAGMENT_LIGHT3_SGIX = 0x840F;
        public static UInt32 GL_FRAGMENT_LIGHT4_SGIX = 0x8410;
        public static UInt32 GL_FRAGMENT_LIGHT5_SGIX = 0x8411;
        public static UInt32 GL_FRAGMENT_LIGHT6_SGIX = 0x8412;
        public static UInt32 GL_FRAGMENT_LIGHT7_SGIX = 0x8413;
        #endregion

        #region Commands
        internal delegate void glFragmentColorMaterialSGIXFunc(GLenum @face, GLenum @mode);
        internal static glFragmentColorMaterialSGIXFunc glFragmentColorMaterialSGIXPtr;
        internal static void loadFragmentColorMaterialSGIX()
        {
            try
            {
                glFragmentColorMaterialSGIXPtr = (glFragmentColorMaterialSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentColorMaterialSGIX"), typeof(glFragmentColorMaterialSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentColorMaterialSGIX'.");
            }
        }
        public static void glFragmentColorMaterialSGIX(GLenum @face, GLenum @mode) => glFragmentColorMaterialSGIXPtr(@face, @mode);

        internal delegate void glFragmentLightfSGIXFunc(GLenum @light, GLenum @pname, GLfloat @param);
        internal static glFragmentLightfSGIXFunc glFragmentLightfSGIXPtr;
        internal static void loadFragmentLightfSGIX()
        {
            try
            {
                glFragmentLightfSGIXPtr = (glFragmentLightfSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentLightfSGIX"), typeof(glFragmentLightfSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentLightfSGIX'.");
            }
        }
        public static void glFragmentLightfSGIX(GLenum @light, GLenum @pname, GLfloat @param) => glFragmentLightfSGIXPtr(@light, @pname, @param);

        internal delegate void glFragmentLightfvSGIXFunc(GLenum @light, GLenum @pname, const GLfloat * @params);
        internal static glFragmentLightfvSGIXFunc glFragmentLightfvSGIXPtr;
        internal static void loadFragmentLightfvSGIX()
        {
            try
            {
                glFragmentLightfvSGIXPtr = (glFragmentLightfvSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentLightfvSGIX"), typeof(glFragmentLightfvSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentLightfvSGIX'.");
            }
        }
        public static void glFragmentLightfvSGIX(GLenum @light, GLenum @pname, const GLfloat * @params) => glFragmentLightfvSGIXPtr(@light, @pname, @params);

        internal delegate void glFragmentLightiSGIXFunc(GLenum @light, GLenum @pname, GLint @param);
        internal static glFragmentLightiSGIXFunc glFragmentLightiSGIXPtr;
        internal static void loadFragmentLightiSGIX()
        {
            try
            {
                glFragmentLightiSGIXPtr = (glFragmentLightiSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentLightiSGIX"), typeof(glFragmentLightiSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentLightiSGIX'.");
            }
        }
        public static void glFragmentLightiSGIX(GLenum @light, GLenum @pname, GLint @param) => glFragmentLightiSGIXPtr(@light, @pname, @param);

        internal delegate void glFragmentLightivSGIXFunc(GLenum @light, GLenum @pname, const GLint * @params);
        internal static glFragmentLightivSGIXFunc glFragmentLightivSGIXPtr;
        internal static void loadFragmentLightivSGIX()
        {
            try
            {
                glFragmentLightivSGIXPtr = (glFragmentLightivSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentLightivSGIX"), typeof(glFragmentLightivSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentLightivSGIX'.");
            }
        }
        public static void glFragmentLightivSGIX(GLenum @light, GLenum @pname, const GLint * @params) => glFragmentLightivSGIXPtr(@light, @pname, @params);

        internal delegate void glFragmentLightModelfSGIXFunc(GLenum @pname, GLfloat @param);
        internal static glFragmentLightModelfSGIXFunc glFragmentLightModelfSGIXPtr;
        internal static void loadFragmentLightModelfSGIX()
        {
            try
            {
                glFragmentLightModelfSGIXPtr = (glFragmentLightModelfSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentLightModelfSGIX"), typeof(glFragmentLightModelfSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentLightModelfSGIX'.");
            }
        }
        public static void glFragmentLightModelfSGIX(GLenum @pname, GLfloat @param) => glFragmentLightModelfSGIXPtr(@pname, @param);

        internal delegate void glFragmentLightModelfvSGIXFunc(GLenum @pname, const GLfloat * @params);
        internal static glFragmentLightModelfvSGIXFunc glFragmentLightModelfvSGIXPtr;
        internal static void loadFragmentLightModelfvSGIX()
        {
            try
            {
                glFragmentLightModelfvSGIXPtr = (glFragmentLightModelfvSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentLightModelfvSGIX"), typeof(glFragmentLightModelfvSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentLightModelfvSGIX'.");
            }
        }
        public static void glFragmentLightModelfvSGIX(GLenum @pname, const GLfloat * @params) => glFragmentLightModelfvSGIXPtr(@pname, @params);

        internal delegate void glFragmentLightModeliSGIXFunc(GLenum @pname, GLint @param);
        internal static glFragmentLightModeliSGIXFunc glFragmentLightModeliSGIXPtr;
        internal static void loadFragmentLightModeliSGIX()
        {
            try
            {
                glFragmentLightModeliSGIXPtr = (glFragmentLightModeliSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentLightModeliSGIX"), typeof(glFragmentLightModeliSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentLightModeliSGIX'.");
            }
        }
        public static void glFragmentLightModeliSGIX(GLenum @pname, GLint @param) => glFragmentLightModeliSGIXPtr(@pname, @param);

        internal delegate void glFragmentLightModelivSGIXFunc(GLenum @pname, const GLint * @params);
        internal static glFragmentLightModelivSGIXFunc glFragmentLightModelivSGIXPtr;
        internal static void loadFragmentLightModelivSGIX()
        {
            try
            {
                glFragmentLightModelivSGIXPtr = (glFragmentLightModelivSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentLightModelivSGIX"), typeof(glFragmentLightModelivSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentLightModelivSGIX'.");
            }
        }
        public static void glFragmentLightModelivSGIX(GLenum @pname, const GLint * @params) => glFragmentLightModelivSGIXPtr(@pname, @params);

        internal delegate void glFragmentMaterialfSGIXFunc(GLenum @face, GLenum @pname, GLfloat @param);
        internal static glFragmentMaterialfSGIXFunc glFragmentMaterialfSGIXPtr;
        internal static void loadFragmentMaterialfSGIX()
        {
            try
            {
                glFragmentMaterialfSGIXPtr = (glFragmentMaterialfSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentMaterialfSGIX"), typeof(glFragmentMaterialfSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentMaterialfSGIX'.");
            }
        }
        public static void glFragmentMaterialfSGIX(GLenum @face, GLenum @pname, GLfloat @param) => glFragmentMaterialfSGIXPtr(@face, @pname, @param);

        internal delegate void glFragmentMaterialfvSGIXFunc(GLenum @face, GLenum @pname, const GLfloat * @params);
        internal static glFragmentMaterialfvSGIXFunc glFragmentMaterialfvSGIXPtr;
        internal static void loadFragmentMaterialfvSGIX()
        {
            try
            {
                glFragmentMaterialfvSGIXPtr = (glFragmentMaterialfvSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentMaterialfvSGIX"), typeof(glFragmentMaterialfvSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentMaterialfvSGIX'.");
            }
        }
        public static void glFragmentMaterialfvSGIX(GLenum @face, GLenum @pname, const GLfloat * @params) => glFragmentMaterialfvSGIXPtr(@face, @pname, @params);

        internal delegate void glFragmentMaterialiSGIXFunc(GLenum @face, GLenum @pname, GLint @param);
        internal static glFragmentMaterialiSGIXFunc glFragmentMaterialiSGIXPtr;
        internal static void loadFragmentMaterialiSGIX()
        {
            try
            {
                glFragmentMaterialiSGIXPtr = (glFragmentMaterialiSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentMaterialiSGIX"), typeof(glFragmentMaterialiSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentMaterialiSGIX'.");
            }
        }
        public static void glFragmentMaterialiSGIX(GLenum @face, GLenum @pname, GLint @param) => glFragmentMaterialiSGIXPtr(@face, @pname, @param);

        internal delegate void glFragmentMaterialivSGIXFunc(GLenum @face, GLenum @pname, const GLint * @params);
        internal static glFragmentMaterialivSGIXFunc glFragmentMaterialivSGIXPtr;
        internal static void loadFragmentMaterialivSGIX()
        {
            try
            {
                glFragmentMaterialivSGIXPtr = (glFragmentMaterialivSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFragmentMaterialivSGIX"), typeof(glFragmentMaterialivSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFragmentMaterialivSGIX'.");
            }
        }
        public static void glFragmentMaterialivSGIX(GLenum @face, GLenum @pname, const GLint * @params) => glFragmentMaterialivSGIXPtr(@face, @pname, @params);

        internal delegate void glGetFragmentLightfvSGIXFunc(GLenum @light, GLenum @pname, GLfloat * @params);
        internal static glGetFragmentLightfvSGIXFunc glGetFragmentLightfvSGIXPtr;
        internal static void loadGetFragmentLightfvSGIX()
        {
            try
            {
                glGetFragmentLightfvSGIXPtr = (glGetFragmentLightfvSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFragmentLightfvSGIX"), typeof(glGetFragmentLightfvSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFragmentLightfvSGIX'.");
            }
        }
        public static void glGetFragmentLightfvSGIX(GLenum @light, GLenum @pname, GLfloat * @params) => glGetFragmentLightfvSGIXPtr(@light, @pname, @params);

        internal delegate void glGetFragmentLightivSGIXFunc(GLenum @light, GLenum @pname, GLint * @params);
        internal static glGetFragmentLightivSGIXFunc glGetFragmentLightivSGIXPtr;
        internal static void loadGetFragmentLightivSGIX()
        {
            try
            {
                glGetFragmentLightivSGIXPtr = (glGetFragmentLightivSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFragmentLightivSGIX"), typeof(glGetFragmentLightivSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFragmentLightivSGIX'.");
            }
        }
        public static void glGetFragmentLightivSGIX(GLenum @light, GLenum @pname, GLint * @params) => glGetFragmentLightivSGIXPtr(@light, @pname, @params);

        internal delegate void glGetFragmentMaterialfvSGIXFunc(GLenum @face, GLenum @pname, GLfloat * @params);
        internal static glGetFragmentMaterialfvSGIXFunc glGetFragmentMaterialfvSGIXPtr;
        internal static void loadGetFragmentMaterialfvSGIX()
        {
            try
            {
                glGetFragmentMaterialfvSGIXPtr = (glGetFragmentMaterialfvSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFragmentMaterialfvSGIX"), typeof(glGetFragmentMaterialfvSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFragmentMaterialfvSGIX'.");
            }
        }
        public static void glGetFragmentMaterialfvSGIX(GLenum @face, GLenum @pname, GLfloat * @params) => glGetFragmentMaterialfvSGIXPtr(@face, @pname, @params);

        internal delegate void glGetFragmentMaterialivSGIXFunc(GLenum @face, GLenum @pname, GLint * @params);
        internal static glGetFragmentMaterialivSGIXFunc glGetFragmentMaterialivSGIXPtr;
        internal static void loadGetFragmentMaterialivSGIX()
        {
            try
            {
                glGetFragmentMaterialivSGIXPtr = (glGetFragmentMaterialivSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFragmentMaterialivSGIX"), typeof(glGetFragmentMaterialivSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFragmentMaterialivSGIX'.");
            }
        }
        public static void glGetFragmentMaterialivSGIX(GLenum @face, GLenum @pname, GLint * @params) => glGetFragmentMaterialivSGIXPtr(@face, @pname, @params);

        internal delegate void glLightEnviSGIXFunc(GLenum @pname, GLint @param);
        internal static glLightEnviSGIXFunc glLightEnviSGIXPtr;
        internal static void loadLightEnviSGIX()
        {
            try
            {
                glLightEnviSGIXPtr = (glLightEnviSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightEnviSGIX"), typeof(glLightEnviSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightEnviSGIX'.");
            }
        }
        public static void glLightEnviSGIX(GLenum @pname, GLint @param) => glLightEnviSGIXPtr(@pname, @param);
        #endregion
    }
}
