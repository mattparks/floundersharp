using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_convolution_accuracy
    {
        #region Interop
        static GL_SGIX_convolution_accuracy()
        {
            Console.WriteLine("Initalising GL_SGIX_convolution_accuracy interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_CONVOLUTION_HINT_SGIX = 0x8316;
        #endregion

        #region Commands
        #endregion
    }
}
