using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_instruments
    {
        #region Interop
        static GL_SGIX_instruments()
        {
            Console.WriteLine("Initalising GL_SGIX_instruments interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetInstrumentsSGIX();
            loadInstrumentsBufferSGIX();
            loadPollInstrumentsSGIX();
            loadReadInstrumentsSGIX();
            loadStartInstrumentsSGIX();
            loadStopInstrumentsSGIX();
        }
        #endregion

        #region Enums
        public static UInt32 GL_INSTRUMENT_BUFFER_POINTER_SGIX = 0x8180;
        public static UInt32 GL_INSTRUMENT_MEASUREMENTS_SGIX = 0x8181;
        #endregion

        #region Commands
        internal delegate GLint glGetInstrumentsSGIXFunc();
        internal static glGetInstrumentsSGIXFunc glGetInstrumentsSGIXPtr;
        internal static void loadGetInstrumentsSGIX()
        {
            try
            {
                glGetInstrumentsSGIXPtr = (glGetInstrumentsSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInstrumentsSGIX"), typeof(glGetInstrumentsSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInstrumentsSGIX'.");
            }
        }
        public static GLint glGetInstrumentsSGIX() => glGetInstrumentsSGIXPtr();

        internal delegate void glInstrumentsBufferSGIXFunc(GLsizei @size, GLint * @buffer);
        internal static glInstrumentsBufferSGIXFunc glInstrumentsBufferSGIXPtr;
        internal static void loadInstrumentsBufferSGIX()
        {
            try
            {
                glInstrumentsBufferSGIXPtr = (glInstrumentsBufferSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInstrumentsBufferSGIX"), typeof(glInstrumentsBufferSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInstrumentsBufferSGIX'.");
            }
        }
        public static void glInstrumentsBufferSGIX(GLsizei @size, GLint * @buffer) => glInstrumentsBufferSGIXPtr(@size, @buffer);

        internal delegate GLint glPollInstrumentsSGIXFunc(GLint * @marker_p);
        internal static glPollInstrumentsSGIXFunc glPollInstrumentsSGIXPtr;
        internal static void loadPollInstrumentsSGIX()
        {
            try
            {
                glPollInstrumentsSGIXPtr = (glPollInstrumentsSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPollInstrumentsSGIX"), typeof(glPollInstrumentsSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPollInstrumentsSGIX'.");
            }
        }
        public static GLint glPollInstrumentsSGIX(GLint * @marker_p) => glPollInstrumentsSGIXPtr(@marker_p);

        internal delegate void glReadInstrumentsSGIXFunc(GLint @marker);
        internal static glReadInstrumentsSGIXFunc glReadInstrumentsSGIXPtr;
        internal static void loadReadInstrumentsSGIX()
        {
            try
            {
                glReadInstrumentsSGIXPtr = (glReadInstrumentsSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadInstrumentsSGIX"), typeof(glReadInstrumentsSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadInstrumentsSGIX'.");
            }
        }
        public static void glReadInstrumentsSGIX(GLint @marker) => glReadInstrumentsSGIXPtr(@marker);

        internal delegate void glStartInstrumentsSGIXFunc();
        internal static glStartInstrumentsSGIXFunc glStartInstrumentsSGIXPtr;
        internal static void loadStartInstrumentsSGIX()
        {
            try
            {
                glStartInstrumentsSGIXPtr = (glStartInstrumentsSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStartInstrumentsSGIX"), typeof(glStartInstrumentsSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStartInstrumentsSGIX'.");
            }
        }
        public static void glStartInstrumentsSGIX() => glStartInstrumentsSGIXPtr();

        internal delegate void glStopInstrumentsSGIXFunc(GLint @marker);
        internal static glStopInstrumentsSGIXFunc glStopInstrumentsSGIXPtr;
        internal static void loadStopInstrumentsSGIX()
        {
            try
            {
                glStopInstrumentsSGIXPtr = (glStopInstrumentsSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStopInstrumentsSGIX"), typeof(glStopInstrumentsSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStopInstrumentsSGIX'.");
            }
        }
        public static void glStopInstrumentsSGIX(GLint @marker) => glStopInstrumentsSGIXPtr(@marker);
        #endregion
    }
}
