using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_list_priority
    {
        #region Interop
        static GL_SGIX_list_priority()
        {
            Console.WriteLine("Initalising GL_SGIX_list_priority interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetListParameterfvSGIX();
            loadGetListParameterivSGIX();
            loadListParameterfSGIX();
            loadListParameterfvSGIX();
            loadListParameteriSGIX();
            loadListParameterivSGIX();
        }
        #endregion

        #region Enums
        public static UInt32 GL_LIST_PRIORITY_SGIX = 0x8182;
        #endregion

        #region Commands
        internal delegate void glGetListParameterfvSGIXFunc(GLuint @list, GLenum @pname, GLfloat * @params);
        internal static glGetListParameterfvSGIXFunc glGetListParameterfvSGIXPtr;
        internal static void loadGetListParameterfvSGIX()
        {
            try
            {
                glGetListParameterfvSGIXPtr = (glGetListParameterfvSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetListParameterfvSGIX"), typeof(glGetListParameterfvSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetListParameterfvSGIX'.");
            }
        }
        public static void glGetListParameterfvSGIX(GLuint @list, GLenum @pname, GLfloat * @params) => glGetListParameterfvSGIXPtr(@list, @pname, @params);

        internal delegate void glGetListParameterivSGIXFunc(GLuint @list, GLenum @pname, GLint * @params);
        internal static glGetListParameterivSGIXFunc glGetListParameterivSGIXPtr;
        internal static void loadGetListParameterivSGIX()
        {
            try
            {
                glGetListParameterivSGIXPtr = (glGetListParameterivSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetListParameterivSGIX"), typeof(glGetListParameterivSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetListParameterivSGIX'.");
            }
        }
        public static void glGetListParameterivSGIX(GLuint @list, GLenum @pname, GLint * @params) => glGetListParameterivSGIXPtr(@list, @pname, @params);

        internal delegate void glListParameterfSGIXFunc(GLuint @list, GLenum @pname, GLfloat @param);
        internal static glListParameterfSGIXFunc glListParameterfSGIXPtr;
        internal static void loadListParameterfSGIX()
        {
            try
            {
                glListParameterfSGIXPtr = (glListParameterfSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glListParameterfSGIX"), typeof(glListParameterfSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glListParameterfSGIX'.");
            }
        }
        public static void glListParameterfSGIX(GLuint @list, GLenum @pname, GLfloat @param) => glListParameterfSGIXPtr(@list, @pname, @param);

        internal delegate void glListParameterfvSGIXFunc(GLuint @list, GLenum @pname, const GLfloat * @params);
        internal static glListParameterfvSGIXFunc glListParameterfvSGIXPtr;
        internal static void loadListParameterfvSGIX()
        {
            try
            {
                glListParameterfvSGIXPtr = (glListParameterfvSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glListParameterfvSGIX"), typeof(glListParameterfvSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glListParameterfvSGIX'.");
            }
        }
        public static void glListParameterfvSGIX(GLuint @list, GLenum @pname, const GLfloat * @params) => glListParameterfvSGIXPtr(@list, @pname, @params);

        internal delegate void glListParameteriSGIXFunc(GLuint @list, GLenum @pname, GLint @param);
        internal static glListParameteriSGIXFunc glListParameteriSGIXPtr;
        internal static void loadListParameteriSGIX()
        {
            try
            {
                glListParameteriSGIXPtr = (glListParameteriSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glListParameteriSGIX"), typeof(glListParameteriSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glListParameteriSGIX'.");
            }
        }
        public static void glListParameteriSGIX(GLuint @list, GLenum @pname, GLint @param) => glListParameteriSGIXPtr(@list, @pname, @param);

        internal delegate void glListParameterivSGIXFunc(GLuint @list, GLenum @pname, const GLint * @params);
        internal static glListParameterivSGIXFunc glListParameterivSGIXPtr;
        internal static void loadListParameterivSGIX()
        {
            try
            {
                glListParameterivSGIXPtr = (glListParameterivSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glListParameterivSGIX"), typeof(glListParameterivSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glListParameterivSGIX'.");
            }
        }
        public static void glListParameterivSGIX(GLuint @list, GLenum @pname, const GLint * @params) => glListParameterivSGIXPtr(@list, @pname, @params);
        #endregion
    }
}
