using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_flush_raster
    {
        #region Interop
        static GL_SGIX_flush_raster()
        {
            Console.WriteLine("Initalising GL_SGIX_flush_raster interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFlushRasterSGIX();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glFlushRasterSGIXFunc();
        internal static glFlushRasterSGIXFunc glFlushRasterSGIXPtr;
        internal static void loadFlushRasterSGIX()
        {
            try
            {
                glFlushRasterSGIXPtr = (glFlushRasterSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlushRasterSGIX"), typeof(glFlushRasterSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlushRasterSGIX'.");
            }
        }
        public static void glFlushRasterSGIX() => glFlushRasterSGIXPtr();
        #endregion
    }
}
