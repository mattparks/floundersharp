using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_blend_alpha_minmax
    {
        #region Interop
        static GL_SGIX_blend_alpha_minmax()
        {
            Console.WriteLine("Initalising GL_SGIX_blend_alpha_minmax interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_ALPHA_MIN_SGIX = 0x8320;
        public static UInt32 GL_ALPHA_MAX_SGIX = 0x8321;
        #endregion

        #region Commands
        #endregion
    }
}
