using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_ir_instrument1
    {
        #region Interop
        static GL_SGIX_ir_instrument1()
        {
            Console.WriteLine("Initalising GL_SGIX_ir_instrument1 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_IR_INSTRUMENT1_SGIX = 0x817F;
        #endregion

        #region Commands
        #endregion
    }
}
