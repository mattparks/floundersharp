using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_texture_scale_bias
    {
        #region Interop
        static GL_SGIX_texture_scale_bias()
        {
            Console.WriteLine("Initalising GL_SGIX_texture_scale_bias interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_POST_TEXTURE_FILTER_BIAS_SGIX = 0x8179;
        public static UInt32 GL_POST_TEXTURE_FILTER_SCALE_SGIX = 0x817A;
        public static UInt32 GL_POST_TEXTURE_FILTER_BIAS_RANGE_SGIX = 0x817B;
        public static UInt32 GL_POST_TEXTURE_FILTER_SCALE_RANGE_SGIX = 0x817C;
        #endregion

        #region Commands
        #endregion
    }
}
