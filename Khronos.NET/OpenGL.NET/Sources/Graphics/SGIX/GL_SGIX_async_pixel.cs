using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_async_pixel
    {
        #region Interop
        static GL_SGIX_async_pixel()
        {
            Console.WriteLine("Initalising GL_SGIX_async_pixel interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_ASYNC_TEX_IMAGE_SGIX = 0x835C;
        public static UInt32 GL_ASYNC_DRAW_PIXELS_SGIX = 0x835D;
        public static UInt32 GL_ASYNC_READ_PIXELS_SGIX = 0x835E;
        public static UInt32 GL_MAX_ASYNC_TEX_IMAGE_SGIX = 0x835F;
        public static UInt32 GL_MAX_ASYNC_DRAW_PIXELS_SGIX = 0x8360;
        public static UInt32 GL_MAX_ASYNC_READ_PIXELS_SGIX = 0x8361;
        #endregion

        #region Commands
        #endregion
    }
}
