using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_shadow_ambient
    {
        #region Interop
        static GL_SGIX_shadow_ambient()
        {
            Console.WriteLine("Initalising GL_SGIX_shadow_ambient interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SHADOW_AMBIENT_SGIX = 0x80BF;
        #endregion

        #region Commands
        #endregion
    }
}
