using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_tag_sample_buffer
    {
        #region Interop
        static GL_SGIX_tag_sample_buffer()
        {
            Console.WriteLine("Initalising GL_SGIX_tag_sample_buffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTagSampleBufferSGIX();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glTagSampleBufferSGIXFunc();
        internal static glTagSampleBufferSGIXFunc glTagSampleBufferSGIXPtr;
        internal static void loadTagSampleBufferSGIX()
        {
            try
            {
                glTagSampleBufferSGIXPtr = (glTagSampleBufferSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTagSampleBufferSGIX"), typeof(glTagSampleBufferSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTagSampleBufferSGIX'.");
            }
        }
        public static void glTagSampleBufferSGIX() => glTagSampleBufferSGIXPtr();
        #endregion
    }
}
