using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_vertex_preclip
    {
        #region Interop
        static GL_SGIX_vertex_preclip()
        {
            Console.WriteLine("Initalising GL_SGIX_vertex_preclip interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_PRECLIP_SGIX = 0x83EE;
        public static UInt32 GL_VERTEX_PRECLIP_HINT_SGIX = 0x83EF;
        #endregion

        #region Commands
        #endregion
    }
}
