using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_texture_add_env
    {
        #region Interop
        static GL_SGIX_texture_add_env()
        {
            Console.WriteLine("Initalising GL_SGIX_texture_add_env interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_ENV_BIAS_SGIX = 0x80BE;
        #endregion

        #region Commands
        #endregion
    }
}
