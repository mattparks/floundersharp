using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_texture_multi_buffer
    {
        #region Interop
        static GL_SGIX_texture_multi_buffer()
        {
            Console.WriteLine("Initalising GL_SGIX_texture_multi_buffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_MULTI_BUFFER_HINT_SGIX = 0x812E;
        #endregion

        #region Commands
        #endregion
    }
}
