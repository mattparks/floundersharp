using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_subsample
    {
        #region Interop
        static GL_SGIX_subsample()
        {
            Console.WriteLine("Initalising GL_SGIX_subsample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_PACK_SUBSAMPLE_RATE_SGIX = 0x85A0;
        public static UInt32 GL_UNPACK_SUBSAMPLE_RATE_SGIX = 0x85A1;
        public static UInt32 GL_PIXEL_SUBSAMPLE_4444_SGIX = 0x85A2;
        public static UInt32 GL_PIXEL_SUBSAMPLE_2424_SGIX = 0x85A3;
        public static UInt32 GL_PIXEL_SUBSAMPLE_4242_SGIX = 0x85A4;
        #endregion

        #region Commands
        #endregion
    }
}
