using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_pixel_texture
    {
        #region Interop
        static GL_SGIX_pixel_texture()
        {
            Console.WriteLine("Initalising GL_SGIX_pixel_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPixelTexGenSGIX();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PIXEL_TEX_GEN_SGIX = 0x8139;
        public static UInt32 GL_PIXEL_TEX_GEN_MODE_SGIX = 0x832B;
        #endregion

        #region Commands
        internal delegate void glPixelTexGenSGIXFunc(GLenum @mode);
        internal static glPixelTexGenSGIXFunc glPixelTexGenSGIXPtr;
        internal static void loadPixelTexGenSGIX()
        {
            try
            {
                glPixelTexGenSGIXPtr = (glPixelTexGenSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelTexGenSGIX"), typeof(glPixelTexGenSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelTexGenSGIX'.");
            }
        }
        public static void glPixelTexGenSGIX(GLenum @mode) => glPixelTexGenSGIXPtr(@mode);
        #endregion
    }
}
