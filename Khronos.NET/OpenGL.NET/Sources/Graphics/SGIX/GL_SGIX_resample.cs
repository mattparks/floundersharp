using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_resample
    {
        #region Interop
        static GL_SGIX_resample()
        {
            Console.WriteLine("Initalising GL_SGIX_resample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_PACK_RESAMPLE_SGIX = 0x842E;
        public static UInt32 GL_UNPACK_RESAMPLE_SGIX = 0x842F;
        public static UInt32 GL_RESAMPLE_REPLICATE_SGIX = 0x8433;
        public static UInt32 GL_RESAMPLE_ZERO_FILL_SGIX = 0x8434;
        public static UInt32 GL_RESAMPLE_DECIMATE_SGIX = 0x8430;
        #endregion

        #region Commands
        #endregion
    }
}
