using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_texture_lod_bias
    {
        #region Interop
        static GL_SGIX_texture_lod_bias()
        {
            Console.WriteLine("Initalising GL_SGIX_texture_lod_bias interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_LOD_BIAS_S_SGIX = 0x818E;
        public static UInt32 GL_TEXTURE_LOD_BIAS_T_SGIX = 0x818F;
        public static UInt32 GL_TEXTURE_LOD_BIAS_R_SGIX = 0x8190;
        #endregion

        #region Commands
        #endregion
    }
}
