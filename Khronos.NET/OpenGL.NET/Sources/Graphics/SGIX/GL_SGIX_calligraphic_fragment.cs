using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_calligraphic_fragment
    {
        #region Interop
        static GL_SGIX_calligraphic_fragment()
        {
            Console.WriteLine("Initalising GL_SGIX_calligraphic_fragment interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_CALLIGRAPHIC_FRAGMENT_SGIX = 0x8183;
        #endregion

        #region Commands
        #endregion
    }
}
