using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_pixel_tiles
    {
        #region Interop
        static GL_SGIX_pixel_tiles()
        {
            Console.WriteLine("Initalising GL_SGIX_pixel_tiles interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_PIXEL_TILE_BEST_ALIGNMENT_SGIX = 0x813E;
        public static UInt32 GL_PIXEL_TILE_CACHE_INCREMENT_SGIX = 0x813F;
        public static UInt32 GL_PIXEL_TILE_WIDTH_SGIX = 0x8140;
        public static UInt32 GL_PIXEL_TILE_HEIGHT_SGIX = 0x8141;
        public static UInt32 GL_PIXEL_TILE_GRID_WIDTH_SGIX = 0x8142;
        public static UInt32 GL_PIXEL_TILE_GRID_HEIGHT_SGIX = 0x8143;
        public static UInt32 GL_PIXEL_TILE_GRID_DEPTH_SGIX = 0x8144;
        public static UInt32 GL_PIXEL_TILE_CACHE_SIZE_SGIX = 0x8145;
        #endregion

        #region Commands
        #endregion
    }
}
