using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGIX_reference_plane
    {
        #region Interop
        static GL_SGIX_reference_plane()
        {
            Console.WriteLine("Initalising GL_SGIX_reference_plane interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadReferencePlaneSGIX();
        }
        #endregion

        #region Enums
        public static UInt32 GL_REFERENCE_PLANE_SGIX = 0x817D;
        public static UInt32 GL_REFERENCE_PLANE_EQUATION_SGIX = 0x817E;
        #endregion

        #region Commands
        internal delegate void glReferencePlaneSGIXFunc(const GLdouble * @equation);
        internal static glReferencePlaneSGIXFunc glReferencePlaneSGIXPtr;
        internal static void loadReferencePlaneSGIX()
        {
            try
            {
                glReferencePlaneSGIXPtr = (glReferencePlaneSGIXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReferencePlaneSGIX"), typeof(glReferencePlaneSGIXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReferencePlaneSGIX'.");
            }
        }
        public static void glReferencePlaneSGIX(const GLdouble * @equation) => glReferencePlaneSGIXPtr(@equation);
        #endregion
    }
}
