using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SUNX_constant_data
    {
        #region Interop
        static GL_SUNX_constant_data()
        {
            Console.WriteLine("Initalising GL_SUNX_constant_data interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFinishTextureSUNX();
        }
        #endregion

        #region Enums
        public static UInt32 GL_UNPACK_CONSTANT_DATA_SUNX = 0x81D5;
        public static UInt32 GL_TEXTURE_CONSTANT_DATA_SUNX = 0x81D6;
        #endregion

        #region Commands
        internal delegate void glFinishTextureSUNXFunc();
        internal static glFinishTextureSUNXFunc glFinishTextureSUNXPtr;
        internal static void loadFinishTextureSUNX()
        {
            try
            {
                glFinishTextureSUNXPtr = (glFinishTextureSUNXFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFinishTextureSUNX"), typeof(glFinishTextureSUNXFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFinishTextureSUNX'.");
            }
        }
        public static void glFinishTextureSUNX() => glFinishTextureSUNXPtr();
        #endregion
    }
}
