using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ANGLE_instanced_arrays
    {
        #region Interop
        static GL_ANGLE_instanced_arrays()
        {
            Console.WriteLine("Initalising GL_ANGLE_instanced_arrays interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawArraysInstancedANGLE();
            loadDrawElementsInstancedANGLE();
            loadVertexAttribDivisorANGLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_DIVISOR_ANGLE = 0x88FE;
        #endregion

        #region Commands
        internal delegate void glDrawArraysInstancedANGLEFunc(GLenum @mode, GLint @first, GLsizei @count, GLsizei @primcount);
        internal static glDrawArraysInstancedANGLEFunc glDrawArraysInstancedANGLEPtr;
        internal static void loadDrawArraysInstancedANGLE()
        {
            try
            {
                glDrawArraysInstancedANGLEPtr = (glDrawArraysInstancedANGLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysInstancedANGLE"), typeof(glDrawArraysInstancedANGLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysInstancedANGLE'.");
            }
        }
        public static void glDrawArraysInstancedANGLE(GLenum @mode, GLint @first, GLsizei @count, GLsizei @primcount) => glDrawArraysInstancedANGLEPtr(@mode, @first, @count, @primcount);

        internal delegate void glDrawElementsInstancedANGLEFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @primcount);
        internal static glDrawElementsInstancedANGLEFunc glDrawElementsInstancedANGLEPtr;
        internal static void loadDrawElementsInstancedANGLE()
        {
            try
            {
                glDrawElementsInstancedANGLEPtr = (glDrawElementsInstancedANGLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedANGLE"), typeof(glDrawElementsInstancedANGLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedANGLE'.");
            }
        }
        public static void glDrawElementsInstancedANGLE(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @primcount) => glDrawElementsInstancedANGLEPtr(@mode, @count, @type, @indices, @primcount);

        internal delegate void glVertexAttribDivisorANGLEFunc(GLuint @index, GLuint @divisor);
        internal static glVertexAttribDivisorANGLEFunc glVertexAttribDivisorANGLEPtr;
        internal static void loadVertexAttribDivisorANGLE()
        {
            try
            {
                glVertexAttribDivisorANGLEPtr = (glVertexAttribDivisorANGLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribDivisorANGLE"), typeof(glVertexAttribDivisorANGLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribDivisorANGLE'.");
            }
        }
        public static void glVertexAttribDivisorANGLE(GLuint @index, GLuint @divisor) => glVertexAttribDivisorANGLEPtr(@index, @divisor);
        #endregion
    }
}
