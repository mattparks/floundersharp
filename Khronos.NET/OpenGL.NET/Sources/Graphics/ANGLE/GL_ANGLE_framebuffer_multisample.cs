using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ANGLE_framebuffer_multisample
    {
        #region Interop
        static GL_ANGLE_framebuffer_multisample()
        {
            Console.WriteLine("Initalising GL_ANGLE_framebuffer_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadRenderbufferStorageMultisampleANGLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RENDERBUFFER_SAMPLES_ANGLE = 0x8CAB;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_ANGLE = 0x8D56;
        public static UInt32 GL_MAX_SAMPLES_ANGLE = 0x8D57;
        #endregion

        #region Commands
        internal delegate void glRenderbufferStorageMultisampleANGLEFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageMultisampleANGLEFunc glRenderbufferStorageMultisampleANGLEPtr;
        internal static void loadRenderbufferStorageMultisampleANGLE()
        {
            try
            {
                glRenderbufferStorageMultisampleANGLEPtr = (glRenderbufferStorageMultisampleANGLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorageMultisampleANGLE"), typeof(glRenderbufferStorageMultisampleANGLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorageMultisampleANGLE'.");
            }
        }
        public static void glRenderbufferStorageMultisampleANGLE(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStorageMultisampleANGLEPtr(@target, @samples, @internalformat, @width, @height);
        #endregion
    }
}
