using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ANGLE_pack_reverse_row_order
    {
        #region Interop
        static GL_ANGLE_pack_reverse_row_order()
        {
            Console.WriteLine("Initalising GL_ANGLE_pack_reverse_row_order interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_PACK_REVERSE_ROW_ORDER_ANGLE = 0x93A4;
        #endregion

        #region Commands
        #endregion
    }
}
