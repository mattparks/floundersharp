using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ANGLE_translated_shader_source
    {
        #region Interop
        static GL_ANGLE_translated_shader_source()
        {
            Console.WriteLine("Initalising GL_ANGLE_translated_shader_source interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetTranslatedShaderSourceANGLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TRANSLATED_SHADER_SOURCE_LENGTH_ANGLE = 0x93A0;
        #endregion

        #region Commands
        internal delegate void glGetTranslatedShaderSourceANGLEFunc(GLuint @shader, GLsizei @bufsize, GLsizei * @length, GLchar * @source);
        internal static glGetTranslatedShaderSourceANGLEFunc glGetTranslatedShaderSourceANGLEPtr;
        internal static void loadGetTranslatedShaderSourceANGLE()
        {
            try
            {
                glGetTranslatedShaderSourceANGLEPtr = (glGetTranslatedShaderSourceANGLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTranslatedShaderSourceANGLE"), typeof(glGetTranslatedShaderSourceANGLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTranslatedShaderSourceANGLE'.");
            }
        }
        public static void glGetTranslatedShaderSourceANGLE(GLuint @shader, GLsizei @bufsize, GLsizei * @length, GLchar * @source) => glGetTranslatedShaderSourceANGLEPtr(@shader, @bufsize, @length, @source);
        #endregion
    }
}
