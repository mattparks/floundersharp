using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ANGLE_framebuffer_blit
    {
        #region Interop
        static GL_ANGLE_framebuffer_blit()
        {
            Console.WriteLine("Initalising GL_ANGLE_framebuffer_blit interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlitFramebufferANGLE();
        }
        #endregion

        #region Enums
        public static UInt32 GL_READ_FRAMEBUFFER_ANGLE = 0x8CA8;
        public static UInt32 GL_DRAW_FRAMEBUFFER_ANGLE = 0x8CA9;
        public static UInt32 GL_DRAW_FRAMEBUFFER_BINDING_ANGLE = 0x8CA6;
        public static UInt32 GL_READ_FRAMEBUFFER_BINDING_ANGLE = 0x8CAA;
        #endregion

        #region Commands
        internal delegate void glBlitFramebufferANGLEFunc(GLint @srcX0, GLint @srcY0, GLint @srcX1, GLint @srcY1, GLint @dstX0, GLint @dstY0, GLint @dstX1, GLint @dstY1, GLbitfield @mask, GLenum @filter);
        internal static glBlitFramebufferANGLEFunc glBlitFramebufferANGLEPtr;
        internal static void loadBlitFramebufferANGLE()
        {
            try
            {
                glBlitFramebufferANGLEPtr = (glBlitFramebufferANGLEFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlitFramebufferANGLE"), typeof(glBlitFramebufferANGLEFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlitFramebufferANGLE'.");
            }
        }
        public static void glBlitFramebufferANGLE(GLint @srcX0, GLint @srcY0, GLint @srcX1, GLint @srcY1, GLint @dstX0, GLint @dstY0, GLint @dstX1, GLint @dstY1, GLbitfield @mask, GLenum @filter) => glBlitFramebufferANGLEPtr(@srcX0, @srcY0, @srcX1, @srcY1, @dstX0, @dstY0, @dstX1, @dstY1, @mask, @filter);
        #endregion
    }
}
