using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ANGLE_texture_compression_dxt5
    {
        #region Interop
        static GL_ANGLE_texture_compression_dxt5()
        {
            Console.WriteLine("Initalising GL_ANGLE_texture_compression_dxt5 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_RGBA_S3TC_DXT5_ANGLE = 0x83F3;
        #endregion

        #region Commands
        #endregion
    }
}
