using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ANGLE_texture_usage
    {
        #region Interop
        static GL_ANGLE_texture_usage()
        {
            Console.WriteLine("Initalising GL_ANGLE_texture_usage interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_USAGE_ANGLE = 0x93A2;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_ANGLE = 0x93A3;
        #endregion

        #region Commands
        #endregion
    }
}
