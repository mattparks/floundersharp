using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ANGLE_depth_texture
    {
        #region Interop
        static GL_ANGLE_depth_texture()
        {
            Console.WriteLine("Initalising GL_ANGLE_depth_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_COMPONENT = 0x1902;
        public static UInt32 GL_DEPTH_STENCIL_OES = 0x84F9;
        public static UInt32 GL_UNSIGNED_SHORT = 0x1403;
        public static UInt32 GL_UNSIGNED_INT = 0x1405;
        public static UInt32 GL_UNSIGNED_INT_24_8_OES = 0x84FA;
        public static UInt32 GL_DEPTH_COMPONENT16 = 0x81A5;
        public static UInt32 GL_DEPTH_COMPONENT32_OES = 0x81A7;
        public static UInt32 GL_DEPTH24_STENCIL8_OES = 0x88F0;
        #endregion

        #region Commands
        #endregion
    }
}
