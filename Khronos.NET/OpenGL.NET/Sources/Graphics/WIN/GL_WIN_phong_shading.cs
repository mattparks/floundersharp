using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_WIN_phong_shading
    {
        #region Interop
        static GL_WIN_phong_shading()
        {
            Console.WriteLine("Initalising GL_WIN_phong_shading interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_PHONG_WIN = 0x80EA;
        public static UInt32 GL_PHONG_HINT_WIN = 0x80EB;
        #endregion

        #region Commands
        #endregion
    }
}
