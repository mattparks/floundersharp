using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_WIN_specular_fog
    {
        #region Interop
        static GL_WIN_specular_fog()
        {
            Console.WriteLine("Initalising GL_WIN_specular_fog interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FOG_SPECULAR_TEXTURE_WIN = 0x80EC;
        #endregion

        #region Commands
        #endregion
    }
}
