using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_KHR_blend_equation_advanced_coherent
    {
        #region Interop
        static GL_KHR_blend_equation_advanced_coherent()
        {
            Console.WriteLine("Initalising GL_KHR_blend_equation_advanced_coherent interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_BLEND_ADVANCED_COHERENT_KHR = 0x9285;
        #endregion

        #region Commands
        #endregion
    }
}
