using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_KHR_debug
    {
        #region Interop
        static GL_KHR_debug()
        {
            Console.WriteLine("Initalising GL_KHR_debug interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDebugMessageControl();
            loadDebugMessageInsert();
            loadDebugMessageCallback();
            loadGetDebugMessageLog();
            loadPushDebugGroup();
            loadPopDebugGroup();
            loadObjectLabel();
            loadGetObjectLabel();
            loadObjectPtrLabel();
            loadGetObjectPtrLabel();
            loadGetPointerv();
            loadDebugMessageControlKHR();
            loadDebugMessageInsertKHR();
            loadDebugMessageCallbackKHR();
            loadGetDebugMessageLogKHR();
            loadPushDebugGroupKHR();
            loadPopDebugGroupKHR();
            loadObjectLabelKHR();
            loadGetObjectLabelKHR();
            loadObjectPtrLabelKHR();
            loadGetObjectPtrLabelKHR();
            loadGetPointervKHR();
        }
        #endregion

        #region Enums
        public static UInt32 GL_DEBUG_OUTPUT_SYNCHRONOUS = 0x8242;
        public static UInt32 GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH = 0x8243;
        public static UInt32 GL_DEBUG_CALLBACK_FUNCTION = 0x8244;
        public static UInt32 GL_DEBUG_CALLBACK_USER_PARAM = 0x8245;
        public static UInt32 GL_DEBUG_SOURCE_API = 0x8246;
        public static UInt32 GL_DEBUG_SOURCE_WINDOW_SYSTEM = 0x8247;
        public static UInt32 GL_DEBUG_SOURCE_SHADER_COMPILER = 0x8248;
        public static UInt32 GL_DEBUG_SOURCE_THIRD_PARTY = 0x8249;
        public static UInt32 GL_DEBUG_SOURCE_APPLICATION = 0x824A;
        public static UInt32 GL_DEBUG_SOURCE_OTHER = 0x824B;
        public static UInt32 GL_DEBUG_TYPE_ERROR = 0x824C;
        public static UInt32 GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR = 0x824D;
        public static UInt32 GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR = 0x824E;
        public static UInt32 GL_DEBUG_TYPE_PORTABILITY = 0x824F;
        public static UInt32 GL_DEBUG_TYPE_PERFORMANCE = 0x8250;
        public static UInt32 GL_DEBUG_TYPE_OTHER = 0x8251;
        public static UInt32 GL_DEBUG_TYPE_MARKER = 0x8268;
        public static UInt32 GL_DEBUG_TYPE_PUSH_GROUP = 0x8269;
        public static UInt32 GL_DEBUG_TYPE_POP_GROUP = 0x826A;
        public static UInt32 GL_DEBUG_SEVERITY_NOTIFICATION = 0x826B;
        public static UInt32 GL_MAX_DEBUG_GROUP_STACK_DEPTH = 0x826C;
        public static UInt32 GL_DEBUG_GROUP_STACK_DEPTH = 0x826D;
        public static UInt32 GL_BUFFER = 0x82E0;
        public static UInt32 GL_SHADER = 0x82E1;
        public static UInt32 GL_PROGRAM = 0x82E2;
        public static UInt32 GL_VERTEX_ARRAY = 0x8074;
        public static UInt32 GL_QUERY = 0x82E3;
        public static UInt32 GL_PROGRAM_PIPELINE = 0x82E4;
        public static UInt32 GL_SAMPLER = 0x82E6;
        public static UInt32 GL_MAX_LABEL_LENGTH = 0x82E8;
        public static UInt32 GL_MAX_DEBUG_MESSAGE_LENGTH = 0x9143;
        public static UInt32 GL_MAX_DEBUG_LOGGED_MESSAGES = 0x9144;
        public static UInt32 GL_DEBUG_LOGGED_MESSAGES = 0x9145;
        public static UInt32 GL_DEBUG_SEVERITY_HIGH = 0x9146;
        public static UInt32 GL_DEBUG_SEVERITY_MEDIUM = 0x9147;
        public static UInt32 GL_DEBUG_SEVERITY_LOW = 0x9148;
        public static UInt32 GL_DEBUG_OUTPUT = 0x92E0;
        public static UInt32 GL_CONTEXT_FLAG_DEBUG_BIT = 0x00000002;
        public static UInt32 GL_STACK_OVERFLOW = 0x0503;
        public static UInt32 GL_STACK_UNDERFLOW = 0x0504;
        public static UInt32 GL_DEBUG_OUTPUT_SYNCHRONOUS_KHR = 0x8242;
        public static UInt32 GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH_KHR = 0x8243;
        public static UInt32 GL_DEBUG_CALLBACK_FUNCTION_KHR = 0x8244;
        public static UInt32 GL_DEBUG_CALLBACK_USER_PARAM_KHR = 0x8245;
        public static UInt32 GL_DEBUG_SOURCE_API_KHR = 0x8246;
        public static UInt32 GL_DEBUG_SOURCE_WINDOW_SYSTEM_KHR = 0x8247;
        public static UInt32 GL_DEBUG_SOURCE_SHADER_COMPILER_KHR = 0x8248;
        public static UInt32 GL_DEBUG_SOURCE_THIRD_PARTY_KHR = 0x8249;
        public static UInt32 GL_DEBUG_SOURCE_APPLICATION_KHR = 0x824A;
        public static UInt32 GL_DEBUG_SOURCE_OTHER_KHR = 0x824B;
        public static UInt32 GL_DEBUG_TYPE_ERROR_KHR = 0x824C;
        public static UInt32 GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_KHR = 0x824D;
        public static UInt32 GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_KHR = 0x824E;
        public static UInt32 GL_DEBUG_TYPE_PORTABILITY_KHR = 0x824F;
        public static UInt32 GL_DEBUG_TYPE_PERFORMANCE_KHR = 0x8250;
        public static UInt32 GL_DEBUG_TYPE_OTHER_KHR = 0x8251;
        public static UInt32 GL_DEBUG_TYPE_MARKER_KHR = 0x8268;
        public static UInt32 GL_DEBUG_TYPE_PUSH_GROUP_KHR = 0x8269;
        public static UInt32 GL_DEBUG_TYPE_POP_GROUP_KHR = 0x826A;
        public static UInt32 GL_DEBUG_SEVERITY_NOTIFICATION_KHR = 0x826B;
        public static UInt32 GL_MAX_DEBUG_GROUP_STACK_DEPTH_KHR = 0x826C;
        public static UInt32 GL_DEBUG_GROUP_STACK_DEPTH_KHR = 0x826D;
        public static UInt32 GL_BUFFER_KHR = 0x82E0;
        public static UInt32 GL_SHADER_KHR = 0x82E1;
        public static UInt32 GL_PROGRAM_KHR = 0x82E2;
        public static UInt32 GL_VERTEX_ARRAY_KHR = 0x8074;
        public static UInt32 GL_QUERY_KHR = 0x82E3;
        public static UInt32 GL_PROGRAM_PIPELINE_KHR = 0x82E4;
        public static UInt32 GL_SAMPLER_KHR = 0x82E6;
        public static UInt32 GL_MAX_LABEL_LENGTH_KHR = 0x82E8;
        public static UInt32 GL_MAX_DEBUG_MESSAGE_LENGTH_KHR = 0x9143;
        public static UInt32 GL_MAX_DEBUG_LOGGED_MESSAGES_KHR = 0x9144;
        public static UInt32 GL_DEBUG_LOGGED_MESSAGES_KHR = 0x9145;
        public static UInt32 GL_DEBUG_SEVERITY_HIGH_KHR = 0x9146;
        public static UInt32 GL_DEBUG_SEVERITY_MEDIUM_KHR = 0x9147;
        public static UInt32 GL_DEBUG_SEVERITY_LOW_KHR = 0x9148;
        public static UInt32 GL_DEBUG_OUTPUT_KHR = 0x92E0;
        public static UInt32 GL_CONTEXT_FLAG_DEBUG_BIT_KHR = 0x00000002;
        public static UInt32 GL_STACK_OVERFLOW_KHR = 0x0503;
        public static UInt32 GL_STACK_UNDERFLOW_KHR = 0x0504;
        public static UInt32 GL_DISPLAY_LIST = 0x82E7;
        #endregion

        #region Commands
        internal delegate void glDebugMessageControlFunc(GLenum @source, GLenum @type, GLenum @severity, GLsizei @count, const GLuint * @ids, GLboolean @enabled);
        internal static glDebugMessageControlFunc glDebugMessageControlPtr;
        internal static void loadDebugMessageControl()
        {
            try
            {
                glDebugMessageControlPtr = (glDebugMessageControlFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageControl"), typeof(glDebugMessageControlFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageControl'.");
            }
        }
        public static void glDebugMessageControl(GLenum @source, GLenum @type, GLenum @severity, GLsizei @count, const GLuint * @ids, GLboolean @enabled) => glDebugMessageControlPtr(@source, @type, @severity, @count, @ids, @enabled);

        internal delegate void glDebugMessageInsertFunc(GLenum @source, GLenum @type, GLuint @id, GLenum @severity, GLsizei @length, const GLchar * @buf);
        internal static glDebugMessageInsertFunc glDebugMessageInsertPtr;
        internal static void loadDebugMessageInsert()
        {
            try
            {
                glDebugMessageInsertPtr = (glDebugMessageInsertFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageInsert"), typeof(glDebugMessageInsertFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageInsert'.");
            }
        }
        public static void glDebugMessageInsert(GLenum @source, GLenum @type, GLuint @id, GLenum @severity, GLsizei @length, const GLchar * @buf) => glDebugMessageInsertPtr(@source, @type, @id, @severity, @length, @buf);

        internal delegate void glDebugMessageCallbackFunc(GLDEBUGPROC @callback, const void * @userParam);
        internal static glDebugMessageCallbackFunc glDebugMessageCallbackPtr;
        internal static void loadDebugMessageCallback()
        {
            try
            {
                glDebugMessageCallbackPtr = (glDebugMessageCallbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageCallback"), typeof(glDebugMessageCallbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageCallback'.");
            }
        }
        public static void glDebugMessageCallback(GLDEBUGPROC @callback, const void * @userParam) => glDebugMessageCallbackPtr(@callback, @userParam);

        internal delegate GLuint glGetDebugMessageLogFunc(GLuint @count, GLsizei @bufSize, GLenum * @sources, GLenum * @types, GLuint * @ids, GLenum * @severities, GLsizei * @lengths, GLchar * @messageLog);
        internal static glGetDebugMessageLogFunc glGetDebugMessageLogPtr;
        internal static void loadGetDebugMessageLog()
        {
            try
            {
                glGetDebugMessageLogPtr = (glGetDebugMessageLogFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDebugMessageLog"), typeof(glGetDebugMessageLogFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDebugMessageLog'.");
            }
        }
        public static GLuint glGetDebugMessageLog(GLuint @count, GLsizei @bufSize, GLenum * @sources, GLenum * @types, GLuint * @ids, GLenum * @severities, GLsizei * @lengths, GLchar * @messageLog) => glGetDebugMessageLogPtr(@count, @bufSize, @sources, @types, @ids, @severities, @lengths, @messageLog);

        internal delegate void glPushDebugGroupFunc(GLenum @source, GLuint @id, GLsizei @length, const GLchar * @message);
        internal static glPushDebugGroupFunc glPushDebugGroupPtr;
        internal static void loadPushDebugGroup()
        {
            try
            {
                glPushDebugGroupPtr = (glPushDebugGroupFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPushDebugGroup"), typeof(glPushDebugGroupFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPushDebugGroup'.");
            }
        }
        public static void glPushDebugGroup(GLenum @source, GLuint @id, GLsizei @length, const GLchar * @message) => glPushDebugGroupPtr(@source, @id, @length, @message);

        internal delegate void glPopDebugGroupFunc();
        internal static glPopDebugGroupFunc glPopDebugGroupPtr;
        internal static void loadPopDebugGroup()
        {
            try
            {
                glPopDebugGroupPtr = (glPopDebugGroupFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPopDebugGroup"), typeof(glPopDebugGroupFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPopDebugGroup'.");
            }
        }
        public static void glPopDebugGroup() => glPopDebugGroupPtr();

        internal delegate void glObjectLabelFunc(GLenum @identifier, GLuint @name, GLsizei @length, const GLchar * @label);
        internal static glObjectLabelFunc glObjectLabelPtr;
        internal static void loadObjectLabel()
        {
            try
            {
                glObjectLabelPtr = (glObjectLabelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glObjectLabel"), typeof(glObjectLabelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glObjectLabel'.");
            }
        }
        public static void glObjectLabel(GLenum @identifier, GLuint @name, GLsizei @length, const GLchar * @label) => glObjectLabelPtr(@identifier, @name, @length, @label);

        internal delegate void glGetObjectLabelFunc(GLenum @identifier, GLuint @name, GLsizei @bufSize, GLsizei * @length, GLchar * @label);
        internal static glGetObjectLabelFunc glGetObjectLabelPtr;
        internal static void loadGetObjectLabel()
        {
            try
            {
                glGetObjectLabelPtr = (glGetObjectLabelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectLabel"), typeof(glGetObjectLabelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectLabel'.");
            }
        }
        public static void glGetObjectLabel(GLenum @identifier, GLuint @name, GLsizei @bufSize, GLsizei * @length, GLchar * @label) => glGetObjectLabelPtr(@identifier, @name, @bufSize, @length, @label);

        internal delegate void glObjectPtrLabelFunc(const void * @ptr, GLsizei @length, const GLchar * @label);
        internal static glObjectPtrLabelFunc glObjectPtrLabelPtr;
        internal static void loadObjectPtrLabel()
        {
            try
            {
                glObjectPtrLabelPtr = (glObjectPtrLabelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glObjectPtrLabel"), typeof(glObjectPtrLabelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glObjectPtrLabel'.");
            }
        }
        public static void glObjectPtrLabel(const void * @ptr, GLsizei @length, const GLchar * @label) => glObjectPtrLabelPtr(@ptr, @length, @label);

        internal delegate void glGetObjectPtrLabelFunc(const void * @ptr, GLsizei @bufSize, GLsizei * @length, GLchar * @label);
        internal static glGetObjectPtrLabelFunc glGetObjectPtrLabelPtr;
        internal static void loadGetObjectPtrLabel()
        {
            try
            {
                glGetObjectPtrLabelPtr = (glGetObjectPtrLabelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectPtrLabel"), typeof(glGetObjectPtrLabelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectPtrLabel'.");
            }
        }
        public static void glGetObjectPtrLabel(const void * @ptr, GLsizei @bufSize, GLsizei * @length, GLchar * @label) => glGetObjectPtrLabelPtr(@ptr, @bufSize, @length, @label);

        internal delegate void glGetPointervFunc(GLenum @pname, void ** @params);
        internal static glGetPointervFunc glGetPointervPtr;
        internal static void loadGetPointerv()
        {
            try
            {
                glGetPointervPtr = (glGetPointervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPointerv"), typeof(glGetPointervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPointerv'.");
            }
        }
        public static void glGetPointerv(GLenum @pname, void ** @params) => glGetPointervPtr(@pname, @params);

        internal delegate void glDebugMessageControlKHRFunc(GLenum @source, GLenum @type, GLenum @severity, GLsizei @count, const GLuint * @ids, GLboolean @enabled);
        internal static glDebugMessageControlKHRFunc glDebugMessageControlKHRPtr;
        internal static void loadDebugMessageControlKHR()
        {
            try
            {
                glDebugMessageControlKHRPtr = (glDebugMessageControlKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageControlKHR"), typeof(glDebugMessageControlKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageControlKHR'.");
            }
        }
        public static void glDebugMessageControlKHR(GLenum @source, GLenum @type, GLenum @severity, GLsizei @count, const GLuint * @ids, GLboolean @enabled) => glDebugMessageControlKHRPtr(@source, @type, @severity, @count, @ids, @enabled);

        internal delegate void glDebugMessageInsertKHRFunc(GLenum @source, GLenum @type, GLuint @id, GLenum @severity, GLsizei @length, const GLchar * @buf);
        internal static glDebugMessageInsertKHRFunc glDebugMessageInsertKHRPtr;
        internal static void loadDebugMessageInsertKHR()
        {
            try
            {
                glDebugMessageInsertKHRPtr = (glDebugMessageInsertKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageInsertKHR"), typeof(glDebugMessageInsertKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageInsertKHR'.");
            }
        }
        public static void glDebugMessageInsertKHR(GLenum @source, GLenum @type, GLuint @id, GLenum @severity, GLsizei @length, const GLchar * @buf) => glDebugMessageInsertKHRPtr(@source, @type, @id, @severity, @length, @buf);

        internal delegate void glDebugMessageCallbackKHRFunc(GLDEBUGPROCKHR @callback, const void * @userParam);
        internal static glDebugMessageCallbackKHRFunc glDebugMessageCallbackKHRPtr;
        internal static void loadDebugMessageCallbackKHR()
        {
            try
            {
                glDebugMessageCallbackKHRPtr = (glDebugMessageCallbackKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageCallbackKHR"), typeof(glDebugMessageCallbackKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageCallbackKHR'.");
            }
        }
        public static void glDebugMessageCallbackKHR(GLDEBUGPROCKHR @callback, const void * @userParam) => glDebugMessageCallbackKHRPtr(@callback, @userParam);

        internal delegate GLuint glGetDebugMessageLogKHRFunc(GLuint @count, GLsizei @bufSize, GLenum * @sources, GLenum * @types, GLuint * @ids, GLenum * @severities, GLsizei * @lengths, GLchar * @messageLog);
        internal static glGetDebugMessageLogKHRFunc glGetDebugMessageLogKHRPtr;
        internal static void loadGetDebugMessageLogKHR()
        {
            try
            {
                glGetDebugMessageLogKHRPtr = (glGetDebugMessageLogKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDebugMessageLogKHR"), typeof(glGetDebugMessageLogKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDebugMessageLogKHR'.");
            }
        }
        public static GLuint glGetDebugMessageLogKHR(GLuint @count, GLsizei @bufSize, GLenum * @sources, GLenum * @types, GLuint * @ids, GLenum * @severities, GLsizei * @lengths, GLchar * @messageLog) => glGetDebugMessageLogKHRPtr(@count, @bufSize, @sources, @types, @ids, @severities, @lengths, @messageLog);

        internal delegate void glPushDebugGroupKHRFunc(GLenum @source, GLuint @id, GLsizei @length, const GLchar * @message);
        internal static glPushDebugGroupKHRFunc glPushDebugGroupKHRPtr;
        internal static void loadPushDebugGroupKHR()
        {
            try
            {
                glPushDebugGroupKHRPtr = (glPushDebugGroupKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPushDebugGroupKHR"), typeof(glPushDebugGroupKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPushDebugGroupKHR'.");
            }
        }
        public static void glPushDebugGroupKHR(GLenum @source, GLuint @id, GLsizei @length, const GLchar * @message) => glPushDebugGroupKHRPtr(@source, @id, @length, @message);

        internal delegate void glPopDebugGroupKHRFunc();
        internal static glPopDebugGroupKHRFunc glPopDebugGroupKHRPtr;
        internal static void loadPopDebugGroupKHR()
        {
            try
            {
                glPopDebugGroupKHRPtr = (glPopDebugGroupKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPopDebugGroupKHR"), typeof(glPopDebugGroupKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPopDebugGroupKHR'.");
            }
        }
        public static void glPopDebugGroupKHR() => glPopDebugGroupKHRPtr();

        internal delegate void glObjectLabelKHRFunc(GLenum @identifier, GLuint @name, GLsizei @length, const GLchar * @label);
        internal static glObjectLabelKHRFunc glObjectLabelKHRPtr;
        internal static void loadObjectLabelKHR()
        {
            try
            {
                glObjectLabelKHRPtr = (glObjectLabelKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glObjectLabelKHR"), typeof(glObjectLabelKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glObjectLabelKHR'.");
            }
        }
        public static void glObjectLabelKHR(GLenum @identifier, GLuint @name, GLsizei @length, const GLchar * @label) => glObjectLabelKHRPtr(@identifier, @name, @length, @label);

        internal delegate void glGetObjectLabelKHRFunc(GLenum @identifier, GLuint @name, GLsizei @bufSize, GLsizei * @length, GLchar * @label);
        internal static glGetObjectLabelKHRFunc glGetObjectLabelKHRPtr;
        internal static void loadGetObjectLabelKHR()
        {
            try
            {
                glGetObjectLabelKHRPtr = (glGetObjectLabelKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectLabelKHR"), typeof(glGetObjectLabelKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectLabelKHR'.");
            }
        }
        public static void glGetObjectLabelKHR(GLenum @identifier, GLuint @name, GLsizei @bufSize, GLsizei * @length, GLchar * @label) => glGetObjectLabelKHRPtr(@identifier, @name, @bufSize, @length, @label);

        internal delegate void glObjectPtrLabelKHRFunc(const void * @ptr, GLsizei @length, const GLchar * @label);
        internal static glObjectPtrLabelKHRFunc glObjectPtrLabelKHRPtr;
        internal static void loadObjectPtrLabelKHR()
        {
            try
            {
                glObjectPtrLabelKHRPtr = (glObjectPtrLabelKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glObjectPtrLabelKHR"), typeof(glObjectPtrLabelKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glObjectPtrLabelKHR'.");
            }
        }
        public static void glObjectPtrLabelKHR(const void * @ptr, GLsizei @length, const GLchar * @label) => glObjectPtrLabelKHRPtr(@ptr, @length, @label);

        internal delegate void glGetObjectPtrLabelKHRFunc(const void * @ptr, GLsizei @bufSize, GLsizei * @length, GLchar * @label);
        internal static glGetObjectPtrLabelKHRFunc glGetObjectPtrLabelKHRPtr;
        internal static void loadGetObjectPtrLabelKHR()
        {
            try
            {
                glGetObjectPtrLabelKHRPtr = (glGetObjectPtrLabelKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectPtrLabelKHR"), typeof(glGetObjectPtrLabelKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectPtrLabelKHR'.");
            }
        }
        public static void glGetObjectPtrLabelKHR(const void * @ptr, GLsizei @bufSize, GLsizei * @length, GLchar * @label) => glGetObjectPtrLabelKHRPtr(@ptr, @bufSize, @length, @label);

        internal delegate void glGetPointervKHRFunc(GLenum @pname, void ** @params);
        internal static glGetPointervKHRFunc glGetPointervKHRPtr;
        internal static void loadGetPointervKHR()
        {
            try
            {
                glGetPointervKHRPtr = (glGetPointervKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPointervKHR"), typeof(glGetPointervKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPointervKHR'.");
            }
        }
        public static void glGetPointervKHR(GLenum @pname, void ** @params) => glGetPointervKHRPtr(@pname, @params);
        #endregion
    }
}
