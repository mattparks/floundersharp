using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_KHR_no_error
    {
        #region Interop
        static GL_KHR_no_error()
        {
            Console.WriteLine("Initalising GL_KHR_no_error interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_CONTEXT_FLAG_NO_ERROR_BIT_KHR = 0x00000008;
        #endregion

        #region Commands
        #endregion
    }
}
