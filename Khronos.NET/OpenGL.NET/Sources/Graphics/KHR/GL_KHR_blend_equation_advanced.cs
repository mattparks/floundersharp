using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_KHR_blend_equation_advanced
    {
        #region Interop
        static GL_KHR_blend_equation_advanced()
        {
            Console.WriteLine("Initalising GL_KHR_blend_equation_advanced interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendBarrierKHR();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MULTIPLY_KHR = 0x9294;
        public static UInt32 GL_SCREEN_KHR = 0x9295;
        public static UInt32 GL_OVERLAY_KHR = 0x9296;
        public static UInt32 GL_DARKEN_KHR = 0x9297;
        public static UInt32 GL_LIGHTEN_KHR = 0x9298;
        public static UInt32 GL_COLORDODGE_KHR = 0x9299;
        public static UInt32 GL_COLORBURN_KHR = 0x929A;
        public static UInt32 GL_HARDLIGHT_KHR = 0x929B;
        public static UInt32 GL_SOFTLIGHT_KHR = 0x929C;
        public static UInt32 GL_DIFFERENCE_KHR = 0x929E;
        public static UInt32 GL_EXCLUSION_KHR = 0x92A0;
        public static UInt32 GL_HSL_HUE_KHR = 0x92AD;
        public static UInt32 GL_HSL_SATURATION_KHR = 0x92AE;
        public static UInt32 GL_HSL_COLOR_KHR = 0x92AF;
        public static UInt32 GL_HSL_LUMINOSITY_KHR = 0x92B0;
        #endregion

        #region Commands
        internal delegate void glBlendBarrierKHRFunc();
        internal static glBlendBarrierKHRFunc glBlendBarrierKHRPtr;
        internal static void loadBlendBarrierKHR()
        {
            try
            {
                glBlendBarrierKHRPtr = (glBlendBarrierKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendBarrierKHR"), typeof(glBlendBarrierKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendBarrierKHR'.");
            }
        }
        public static void glBlendBarrierKHR() => glBlendBarrierKHRPtr();
        #endregion
    }
}
