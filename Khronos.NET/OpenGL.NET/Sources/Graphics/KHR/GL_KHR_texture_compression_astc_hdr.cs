using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_KHR_texture_compression_astc_hdr
    {
        #region Interop
        static GL_KHR_texture_compression_astc_hdr()
        {
            Console.WriteLine("Initalising GL_KHR_texture_compression_astc_hdr interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_4x4_KHR = 0x93B0;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_5x4_KHR = 0x93B1;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_5x5_KHR = 0x93B2;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_6x5_KHR = 0x93B3;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_6x6_KHR = 0x93B4;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_8x5_KHR = 0x93B5;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_8x6_KHR = 0x93B6;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_8x8_KHR = 0x93B7;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_10x5_KHR = 0x93B8;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_10x6_KHR = 0x93B9;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_10x8_KHR = 0x93BA;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_10x10_KHR = 0x93BB;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_12x10_KHR = 0x93BC;
        public static UInt32 GL_COMPRESSED_RGBA_ASTC_12x12_KHR = 0x93BD;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_4x4_KHR = 0x93D0;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x4_KHR = 0x93D1;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x5_KHR = 0x93D2;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x5_KHR = 0x93D3;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x6_KHR = 0x93D4;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x5_KHR = 0x93D5;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x6_KHR = 0x93D6;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x8_KHR = 0x93D7;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x5_KHR = 0x93D8;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x6_KHR = 0x93D9;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x8_KHR = 0x93DA;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x10_KHR = 0x93DB;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x10_KHR = 0x93DC;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x12_KHR = 0x93DD;
        #endregion

        #region Commands
        #endregion
    }
}
