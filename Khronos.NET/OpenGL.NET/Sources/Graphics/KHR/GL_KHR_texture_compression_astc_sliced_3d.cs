using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_KHR_texture_compression_astc_sliced_3d
    {
        #region Interop
        static GL_KHR_texture_compression_astc_sliced_3d()
        {
            Console.WriteLine("Initalising GL_KHR_texture_compression_astc_sliced_3d interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        #endregion
    }
}
