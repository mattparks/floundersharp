using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_KHR_robustness
    {
        #region Interop
        static GL_KHR_robustness()
        {
            Console.WriteLine("Initalising GL_KHR_robustness interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetGraphicsResetStatus();
            loadReadnPixels();
            loadGetnUniformfv();
            loadGetnUniformiv();
            loadGetnUniformuiv();
            loadGetGraphicsResetStatusKHR();
            loadReadnPixelsKHR();
            loadGetnUniformfvKHR();
            loadGetnUniformivKHR();
            loadGetnUniformuivKHR();
        }
        #endregion

        #region Enums
        public static UInt32 GL_NO_ERROR = 0;
        public static UInt32 GL_CONTEXT_ROBUST_ACCESS = 0x90F3;
        public static UInt32 GL_LOSE_CONTEXT_ON_RESET = 0x8252;
        public static UInt32 GL_GUILTY_CONTEXT_RESET = 0x8253;
        public static UInt32 GL_INNOCENT_CONTEXT_RESET = 0x8254;
        public static UInt32 GL_UNKNOWN_CONTEXT_RESET = 0x8255;
        public static UInt32 GL_RESET_NOTIFICATION_STRATEGY = 0x8256;
        public static UInt32 GL_NO_RESET_NOTIFICATION = 0x8261;
        public static UInt32 GL_CONTEXT_LOST = 0x0507;
        public static UInt32 GL_CONTEXT_ROBUST_ACCESS_KHR = 0x90F3;
        public static UInt32 GL_LOSE_CONTEXT_ON_RESET_KHR = 0x8252;
        public static UInt32 GL_GUILTY_CONTEXT_RESET_KHR = 0x8253;
        public static UInt32 GL_INNOCENT_CONTEXT_RESET_KHR = 0x8254;
        public static UInt32 GL_UNKNOWN_CONTEXT_RESET_KHR = 0x8255;
        public static UInt32 GL_RESET_NOTIFICATION_STRATEGY_KHR = 0x8256;
        public static UInt32 GL_NO_RESET_NOTIFICATION_KHR = 0x8261;
        public static UInt32 GL_CONTEXT_LOST_KHR = 0x0507;
        #endregion

        #region Commands
        internal delegate GLenum glGetGraphicsResetStatusFunc();
        internal static glGetGraphicsResetStatusFunc glGetGraphicsResetStatusPtr;
        internal static void loadGetGraphicsResetStatus()
        {
            try
            {
                glGetGraphicsResetStatusPtr = (glGetGraphicsResetStatusFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetGraphicsResetStatus"), typeof(glGetGraphicsResetStatusFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetGraphicsResetStatus'.");
            }
        }
        public static GLenum glGetGraphicsResetStatus() => glGetGraphicsResetStatusPtr();

        internal delegate void glReadnPixelsFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, GLsizei @bufSize, void * @data);
        internal static glReadnPixelsFunc glReadnPixelsPtr;
        internal static void loadReadnPixels()
        {
            try
            {
                glReadnPixelsPtr = (glReadnPixelsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadnPixels"), typeof(glReadnPixelsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadnPixels'.");
            }
        }
        public static void glReadnPixels(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, GLsizei @bufSize, void * @data) => glReadnPixelsPtr(@x, @y, @width, @height, @format, @type, @bufSize, @data);

        internal delegate void glGetnUniformfvFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLfloat * @params);
        internal static glGetnUniformfvFunc glGetnUniformfvPtr;
        internal static void loadGetnUniformfv()
        {
            try
            {
                glGetnUniformfvPtr = (glGetnUniformfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformfv"), typeof(glGetnUniformfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformfv'.");
            }
        }
        public static void glGetnUniformfv(GLuint @program, GLint @location, GLsizei @bufSize, GLfloat * @params) => glGetnUniformfvPtr(@program, @location, @bufSize, @params);

        internal delegate void glGetnUniformivFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLint * @params);
        internal static glGetnUniformivFunc glGetnUniformivPtr;
        internal static void loadGetnUniformiv()
        {
            try
            {
                glGetnUniformivPtr = (glGetnUniformivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformiv"), typeof(glGetnUniformivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformiv'.");
            }
        }
        public static void glGetnUniformiv(GLuint @program, GLint @location, GLsizei @bufSize, GLint * @params) => glGetnUniformivPtr(@program, @location, @bufSize, @params);

        internal delegate void glGetnUniformuivFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLuint * @params);
        internal static glGetnUniformuivFunc glGetnUniformuivPtr;
        internal static void loadGetnUniformuiv()
        {
            try
            {
                glGetnUniformuivPtr = (glGetnUniformuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformuiv"), typeof(glGetnUniformuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformuiv'.");
            }
        }
        public static void glGetnUniformuiv(GLuint @program, GLint @location, GLsizei @bufSize, GLuint * @params) => glGetnUniformuivPtr(@program, @location, @bufSize, @params);

        internal delegate GLenum glGetGraphicsResetStatusKHRFunc();
        internal static glGetGraphicsResetStatusKHRFunc glGetGraphicsResetStatusKHRPtr;
        internal static void loadGetGraphicsResetStatusKHR()
        {
            try
            {
                glGetGraphicsResetStatusKHRPtr = (glGetGraphicsResetStatusKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetGraphicsResetStatusKHR"), typeof(glGetGraphicsResetStatusKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetGraphicsResetStatusKHR'.");
            }
        }
        public static GLenum glGetGraphicsResetStatusKHR() => glGetGraphicsResetStatusKHRPtr();

        internal delegate void glReadnPixelsKHRFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, GLsizei @bufSize, void * @data);
        internal static glReadnPixelsKHRFunc glReadnPixelsKHRPtr;
        internal static void loadReadnPixelsKHR()
        {
            try
            {
                glReadnPixelsKHRPtr = (glReadnPixelsKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadnPixelsKHR"), typeof(glReadnPixelsKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadnPixelsKHR'.");
            }
        }
        public static void glReadnPixelsKHR(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, GLsizei @bufSize, void * @data) => glReadnPixelsKHRPtr(@x, @y, @width, @height, @format, @type, @bufSize, @data);

        internal delegate void glGetnUniformfvKHRFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLfloat * @params);
        internal static glGetnUniformfvKHRFunc glGetnUniformfvKHRPtr;
        internal static void loadGetnUniformfvKHR()
        {
            try
            {
                glGetnUniformfvKHRPtr = (glGetnUniformfvKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformfvKHR"), typeof(glGetnUniformfvKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformfvKHR'.");
            }
        }
        public static void glGetnUniformfvKHR(GLuint @program, GLint @location, GLsizei @bufSize, GLfloat * @params) => glGetnUniformfvKHRPtr(@program, @location, @bufSize, @params);

        internal delegate void glGetnUniformivKHRFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLint * @params);
        internal static glGetnUniformivKHRFunc glGetnUniformivKHRPtr;
        internal static void loadGetnUniformivKHR()
        {
            try
            {
                glGetnUniformivKHRPtr = (glGetnUniformivKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformivKHR"), typeof(glGetnUniformivKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformivKHR'.");
            }
        }
        public static void glGetnUniformivKHR(GLuint @program, GLint @location, GLsizei @bufSize, GLint * @params) => glGetnUniformivKHRPtr(@program, @location, @bufSize, @params);

        internal delegate void glGetnUniformuivKHRFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLuint * @params);
        internal static glGetnUniformuivKHRFunc glGetnUniformuivKHRPtr;
        internal static void loadGetnUniformuivKHR()
        {
            try
            {
                glGetnUniformuivKHRPtr = (glGetnUniformuivKHRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformuivKHR"), typeof(glGetnUniformuivKHRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformuivKHR'.");
            }
        }
        public static void glGetnUniformuivKHR(GLuint @program, GLint @location, GLsizei @bufSize, GLuint * @params) => glGetnUniformuivKHRPtr(@program, @location, @bufSize, @params);
        #endregion
    }
}
