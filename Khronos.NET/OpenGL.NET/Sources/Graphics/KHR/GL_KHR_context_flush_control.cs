using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_KHR_context_flush_control
    {
        #region Interop
        static GL_KHR_context_flush_control()
        {
            Console.WriteLine("Initalising GL_KHR_context_flush_control interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_CONTEXT_RELEASE_BEHAVIOR = 0x82FB;
        public static UInt32 GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH = 0x82FC;
        public static UInt32 GL_NONE = 0;
        public static UInt32 GL_CONTEXT_RELEASE_BEHAVIOR_KHR = 0x82FB;
        public static UInt32 GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH_KHR = 0x82FC;
        #endregion

        #region Commands
        #endregion
    }
}
