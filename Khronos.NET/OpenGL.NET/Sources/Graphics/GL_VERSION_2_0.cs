using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL20
    {
        #region Interop
        static GL20()
        {
            Console.WriteLine("Initalising GL20 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendEquationSeparate();
            loadDrawBuffers();
            loadStencilOpSeparate();
            loadStencilFuncSeparate();
            loadStencilMaskSeparate();
            loadAttachShader();
            loadBindAttribLocation();
            loadCompileShader();
            loadCreateProgram();
            loadCreateShader();
            loadDeleteProgram();
            loadDeleteShader();
            loadDetachShader();
            loadDisableVertexAttribArray();
            loadEnableVertexAttribArray();
            loadGetActiveAttrib();
            loadGetActiveUniform();
            loadGetAttachedShaders();
            loadGetAttribLocation();
            loadGetProgramiv();
            loadGetProgramInfoLog();
            loadGetShaderiv();
            loadGetShaderInfoLog();
            loadGetShaderSource();
            loadGetUniformLocation();
            loadGetUniformfv();
            loadGetUniformiv();
            loadGetVertexAttribdv();
            loadGetVertexAttribfv();
            loadGetVertexAttribiv();
            loadGetVertexAttribPointerv();
            loadIsProgram();
            loadIsShader();
            loadLinkProgram();
            loadShaderSource();
            loadUseProgram();
            loadUniform1f();
            loadUniform2f();
            loadUniform3f();
            loadUniform4f();
            loadUniform1i();
            loadUniform2i();
            loadUniform3i();
            loadUniform4i();
            loadUniform1fv();
            loadUniform2fv();
            loadUniform3fv();
            loadUniform4fv();
            loadUniform1iv();
            loadUniform2iv();
            loadUniform3iv();
            loadUniform4iv();
            loadUniformMatrix2fv();
            loadUniformMatrix3fv();
            loadUniformMatrix4fv();
            loadValidateProgram();
            loadVertexAttrib1d();
            loadVertexAttrib1dv();
            loadVertexAttrib1f();
            loadVertexAttrib1fv();
            loadVertexAttrib1s();
            loadVertexAttrib1sv();
            loadVertexAttrib2d();
            loadVertexAttrib2dv();
            loadVertexAttrib2f();
            loadVertexAttrib2fv();
            loadVertexAttrib2s();
            loadVertexAttrib2sv();
            loadVertexAttrib3d();
            loadVertexAttrib3dv();
            loadVertexAttrib3f();
            loadVertexAttrib3fv();
            loadVertexAttrib3s();
            loadVertexAttrib3sv();
            loadVertexAttrib4Nbv();
            loadVertexAttrib4Niv();
            loadVertexAttrib4Nsv();
            loadVertexAttrib4Nub();
            loadVertexAttrib4Nubv();
            loadVertexAttrib4Nuiv();
            loadVertexAttrib4Nusv();
            loadVertexAttrib4bv();
            loadVertexAttrib4d();
            loadVertexAttrib4dv();
            loadVertexAttrib4f();
            loadVertexAttrib4fv();
            loadVertexAttrib4iv();
            loadVertexAttrib4s();
            loadVertexAttrib4sv();
            loadVertexAttrib4ubv();
            loadVertexAttrib4uiv();
            loadVertexAttrib4usv();
            loadVertexAttribPointer();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BLEND_EQUATION_RGB = 0x8009;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_ENABLED = 0x8622;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_SIZE = 0x8623;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_STRIDE = 0x8624;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_TYPE = 0x8625;
        public static UInt32 GL_CURRENT_VERTEX_ATTRIB = 0x8626;
        public static UInt32 GL_VERTEX_PROGRAM_POINT_SIZE = 0x8642;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_POINTER = 0x8645;
        public static UInt32 GL_STENCIL_BACK_FUNC = 0x8800;
        public static UInt32 GL_STENCIL_BACK_FAIL = 0x8801;
        public static UInt32 GL_STENCIL_BACK_PASS_DEPTH_FAIL = 0x8802;
        public static UInt32 GL_STENCIL_BACK_PASS_DEPTH_PASS = 0x8803;
        public static UInt32 GL_MAX_DRAW_BUFFERS = 0x8824;
        public static UInt32 GL_DRAW_BUFFER0 = 0x8825;
        public static UInt32 GL_DRAW_BUFFER1 = 0x8826;
        public static UInt32 GL_DRAW_BUFFER2 = 0x8827;
        public static UInt32 GL_DRAW_BUFFER3 = 0x8828;
        public static UInt32 GL_DRAW_BUFFER4 = 0x8829;
        public static UInt32 GL_DRAW_BUFFER5 = 0x882A;
        public static UInt32 GL_DRAW_BUFFER6 = 0x882B;
        public static UInt32 GL_DRAW_BUFFER7 = 0x882C;
        public static UInt32 GL_DRAW_BUFFER8 = 0x882D;
        public static UInt32 GL_DRAW_BUFFER9 = 0x882E;
        public static UInt32 GL_DRAW_BUFFER10 = 0x882F;
        public static UInt32 GL_DRAW_BUFFER11 = 0x8830;
        public static UInt32 GL_DRAW_BUFFER12 = 0x8831;
        public static UInt32 GL_DRAW_BUFFER13 = 0x8832;
        public static UInt32 GL_DRAW_BUFFER14 = 0x8833;
        public static UInt32 GL_DRAW_BUFFER15 = 0x8834;
        public static UInt32 GL_BLEND_EQUATION_ALPHA = 0x883D;
        public static UInt32 GL_MAX_VERTEX_ATTRIBS = 0x8869;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_NORMALIZED = 0x886A;
        public static UInt32 GL_MAX_TEXTURE_IMAGE_UNITS = 0x8872;
        public static UInt32 GL_FRAGMENT_SHADER = 0x8B30;
        public static UInt32 GL_VERTEX_SHADER = 0x8B31;
        public static UInt32 GL_MAX_FRAGMENT_UNIFORM_COMPONENTS = 0x8B49;
        public static UInt32 GL_MAX_VERTEX_UNIFORM_COMPONENTS = 0x8B4A;
        public static UInt32 GL_MAX_VARYING_FLOATS = 0x8B4B;
        public static UInt32 GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS = 0x8B4C;
        public static UInt32 GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS = 0x8B4D;
        public static UInt32 GL_SHADER_TYPE = 0x8B4F;
        public static UInt32 GL_FLOAT_VEC2 = 0x8B50;
        public static UInt32 GL_FLOAT_VEC3 = 0x8B51;
        public static UInt32 GL_FLOAT_VEC4 = 0x8B52;
        public static UInt32 GL_INT_VEC2 = 0x8B53;
        public static UInt32 GL_INT_VEC3 = 0x8B54;
        public static UInt32 GL_INT_VEC4 = 0x8B55;
        public static UInt32 GL_BOOL = 0x8B56;
        public static UInt32 GL_BOOL_VEC2 = 0x8B57;
        public static UInt32 GL_BOOL_VEC3 = 0x8B58;
        public static UInt32 GL_BOOL_VEC4 = 0x8B59;
        public static UInt32 GL_FLOAT_MAT2 = 0x8B5A;
        public static UInt32 GL_FLOAT_MAT3 = 0x8B5B;
        public static UInt32 GL_FLOAT_MAT4 = 0x8B5C;
        public static UInt32 GL_SAMPLER_1D = 0x8B5D;
        public static UInt32 GL_SAMPLER_2D = 0x8B5E;
        public static UInt32 GL_SAMPLER_3D = 0x8B5F;
        public static UInt32 GL_SAMPLER_CUBE = 0x8B60;
        public static UInt32 GL_SAMPLER_1D_SHADOW = 0x8B61;
        public static UInt32 GL_SAMPLER_2D_SHADOW = 0x8B62;
        public static UInt32 GL_DELETE_STATUS = 0x8B80;
        public static UInt32 GL_COMPILE_STATUS = 0x8B81;
        public static UInt32 GL_LINK_STATUS = 0x8B82;
        public static UInt32 GL_VALIDATE_STATUS = 0x8B83;
        public static UInt32 GL_INFO_LOG_LENGTH = 0x8B84;
        public static UInt32 GL_ATTACHED_SHADERS = 0x8B85;
        public static UInt32 GL_ACTIVE_UNIFORMS = 0x8B86;
        public static UInt32 GL_ACTIVE_UNIFORM_MAX_LENGTH = 0x8B87;
        public static UInt32 GL_SHADER_SOURCE_LENGTH = 0x8B88;
        public static UInt32 GL_ACTIVE_ATTRIBUTES = 0x8B89;
        public static UInt32 GL_ACTIVE_ATTRIBUTE_MAX_LENGTH = 0x8B8A;
        public static UInt32 GL_FRAGMENT_SHADER_DERIVATIVE_HINT = 0x8B8B;
        public static UInt32 GL_SHADING_LANGUAGE_VERSION = 0x8B8C;
        public static UInt32 GL_CURRENT_PROGRAM = 0x8B8D;
        public static UInt32 GL_POINT_SPRITE_COORD_ORIGIN = 0x8CA0;
        public static UInt32 GL_LOWER_LEFT = 0x8CA1;
        public static UInt32 GL_UPPER_LEFT = 0x8CA2;
        public static UInt32 GL_STENCIL_BACK_REF = 0x8CA3;
        public static UInt32 GL_STENCIL_BACK_VALUE_MASK = 0x8CA4;
        public static UInt32 GL_STENCIL_BACK_WRITEMASK = 0x8CA5;
        public static UInt32 GL_VERTEX_PROGRAM_TWO_SIDE = 0x8643;
        public static UInt32 GL_POINT_SPRITE = 0x8861;
        public static UInt32 GL_COORD_REPLACE = 0x8862;
        public static UInt32 GL_MAX_TEXTURE_COORDS = 0x8871;
        #endregion

        #region Commands
        internal delegate void glBlendEquationSeparateFunc(GLenum @modeRGB, GLenum @modeAlpha);
        internal static glBlendEquationSeparateFunc glBlendEquationSeparatePtr;
        internal static void loadBlendEquationSeparate()
        {
            try
            {
                glBlendEquationSeparatePtr = (glBlendEquationSeparateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationSeparate"), typeof(glBlendEquationSeparateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationSeparate'.");
            }
        }
        public static void glBlendEquationSeparate(GLenum @modeRGB, GLenum @modeAlpha) => glBlendEquationSeparatePtr(@modeRGB, @modeAlpha);

        internal delegate void glDrawBuffersFunc(GLsizei @n, const GLenum * @bufs);
        internal static glDrawBuffersFunc glDrawBuffersPtr;
        internal static void loadDrawBuffers()
        {
            try
            {
                glDrawBuffersPtr = (glDrawBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawBuffers"), typeof(glDrawBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawBuffers'.");
            }
        }
        public static void glDrawBuffers(GLsizei @n, const GLenum * @bufs) => glDrawBuffersPtr(@n, @bufs);

        internal delegate void glStencilOpSeparateFunc(GLenum @face, GLenum @sfail, GLenum @dpfail, GLenum @dppass);
        internal static glStencilOpSeparateFunc glStencilOpSeparatePtr;
        internal static void loadStencilOpSeparate()
        {
            try
            {
                glStencilOpSeparatePtr = (glStencilOpSeparateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilOpSeparate"), typeof(glStencilOpSeparateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilOpSeparate'.");
            }
        }
        public static void glStencilOpSeparate(GLenum @face, GLenum @sfail, GLenum @dpfail, GLenum @dppass) => glStencilOpSeparatePtr(@face, @sfail, @dpfail, @dppass);

        internal delegate void glStencilFuncSeparateFunc(GLenum @face, GLenum @func, GLint @ref, GLuint @mask);
        internal static glStencilFuncSeparateFunc glStencilFuncSeparatePtr;
        internal static void loadStencilFuncSeparate()
        {
            try
            {
                glStencilFuncSeparatePtr = (glStencilFuncSeparateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilFuncSeparate"), typeof(glStencilFuncSeparateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilFuncSeparate'.");
            }
        }
        public static void glStencilFuncSeparate(GLenum @face, GLenum @func, GLint @ref, GLuint @mask) => glStencilFuncSeparatePtr(@face, @func, @ref, @mask);

        internal delegate void glStencilMaskSeparateFunc(GLenum @face, GLuint @mask);
        internal static glStencilMaskSeparateFunc glStencilMaskSeparatePtr;
        internal static void loadStencilMaskSeparate()
        {
            try
            {
                glStencilMaskSeparatePtr = (glStencilMaskSeparateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilMaskSeparate"), typeof(glStencilMaskSeparateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilMaskSeparate'.");
            }
        }
        public static void glStencilMaskSeparate(GLenum @face, GLuint @mask) => glStencilMaskSeparatePtr(@face, @mask);

        internal delegate void glAttachShaderFunc(GLuint @program, GLuint @shader);
        internal static glAttachShaderFunc glAttachShaderPtr;
        internal static void loadAttachShader()
        {
            try
            {
                glAttachShaderPtr = (glAttachShaderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAttachShader"), typeof(glAttachShaderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAttachShader'.");
            }
        }
        public static void glAttachShader(GLuint @program, GLuint @shader) => glAttachShaderPtr(@program, @shader);

        internal delegate void glBindAttribLocationFunc(GLuint @program, GLuint @index, const GLchar * @name);
        internal static glBindAttribLocationFunc glBindAttribLocationPtr;
        internal static void loadBindAttribLocation()
        {
            try
            {
                glBindAttribLocationPtr = (glBindAttribLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindAttribLocation"), typeof(glBindAttribLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindAttribLocation'.");
            }
        }
        public static void glBindAttribLocation(GLuint @program, GLuint @index, const GLchar * @name) => glBindAttribLocationPtr(@program, @index, @name);

        internal delegate void glCompileShaderFunc(GLuint @shader);
        internal static glCompileShaderFunc glCompileShaderPtr;
        internal static void loadCompileShader()
        {
            try
            {
                glCompileShaderPtr = (glCompileShaderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompileShader"), typeof(glCompileShaderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompileShader'.");
            }
        }
        public static void glCompileShader(GLuint @shader) => glCompileShaderPtr(@shader);

        internal delegate GLuint glCreateProgramFunc();
        internal static glCreateProgramFunc glCreateProgramPtr;
        internal static void loadCreateProgram()
        {
            try
            {
                glCreateProgramPtr = (glCreateProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateProgram"), typeof(glCreateProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateProgram'.");
            }
        }
        public static GLuint glCreateProgram() => glCreateProgramPtr();

        internal delegate GLuint glCreateShaderFunc(GLenum @type);
        internal static glCreateShaderFunc glCreateShaderPtr;
        internal static void loadCreateShader()
        {
            try
            {
                glCreateShaderPtr = (glCreateShaderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateShader"), typeof(glCreateShaderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateShader'.");
            }
        }
        public static GLuint glCreateShader(GLenum @type) => glCreateShaderPtr(@type);

        internal delegate void glDeleteProgramFunc(GLuint @program);
        internal static glDeleteProgramFunc glDeleteProgramPtr;
        internal static void loadDeleteProgram()
        {
            try
            {
                glDeleteProgramPtr = (glDeleteProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteProgram"), typeof(glDeleteProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteProgram'.");
            }
        }
        public static void glDeleteProgram(GLuint @program) => glDeleteProgramPtr(@program);

        internal delegate void glDeleteShaderFunc(GLuint @shader);
        internal static glDeleteShaderFunc glDeleteShaderPtr;
        internal static void loadDeleteShader()
        {
            try
            {
                glDeleteShaderPtr = (glDeleteShaderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteShader"), typeof(glDeleteShaderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteShader'.");
            }
        }
        public static void glDeleteShader(GLuint @shader) => glDeleteShaderPtr(@shader);

        internal delegate void glDetachShaderFunc(GLuint @program, GLuint @shader);
        internal static glDetachShaderFunc glDetachShaderPtr;
        internal static void loadDetachShader()
        {
            try
            {
                glDetachShaderPtr = (glDetachShaderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDetachShader"), typeof(glDetachShaderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDetachShader'.");
            }
        }
        public static void glDetachShader(GLuint @program, GLuint @shader) => glDetachShaderPtr(@program, @shader);

        internal delegate void glDisableVertexAttribArrayFunc(GLuint @index);
        internal static glDisableVertexAttribArrayFunc glDisableVertexAttribArrayPtr;
        internal static void loadDisableVertexAttribArray()
        {
            try
            {
                glDisableVertexAttribArrayPtr = (glDisableVertexAttribArrayFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableVertexAttribArray"), typeof(glDisableVertexAttribArrayFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableVertexAttribArray'.");
            }
        }
        public static void glDisableVertexAttribArray(GLuint @index) => glDisableVertexAttribArrayPtr(@index);

        internal delegate void glEnableVertexAttribArrayFunc(GLuint @index);
        internal static glEnableVertexAttribArrayFunc glEnableVertexAttribArrayPtr;
        internal static void loadEnableVertexAttribArray()
        {
            try
            {
                glEnableVertexAttribArrayPtr = (glEnableVertexAttribArrayFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableVertexAttribArray"), typeof(glEnableVertexAttribArrayFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableVertexAttribArray'.");
            }
        }
        public static void glEnableVertexAttribArray(GLuint @index) => glEnableVertexAttribArrayPtr(@index);

        internal delegate void glGetActiveAttribFunc(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLint * @size, GLenum * @type, GLchar * @name);
        internal static glGetActiveAttribFunc glGetActiveAttribPtr;
        internal static void loadGetActiveAttrib()
        {
            try
            {
                glGetActiveAttribPtr = (glGetActiveAttribFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveAttrib"), typeof(glGetActiveAttribFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveAttrib'.");
            }
        }
        public static void glGetActiveAttrib(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLint * @size, GLenum * @type, GLchar * @name) => glGetActiveAttribPtr(@program, @index, @bufSize, @length, @size, @type, @name);

        internal delegate void glGetActiveUniformFunc(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLint * @size, GLenum * @type, GLchar * @name);
        internal static glGetActiveUniformFunc glGetActiveUniformPtr;
        internal static void loadGetActiveUniform()
        {
            try
            {
                glGetActiveUniformPtr = (glGetActiveUniformFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveUniform"), typeof(glGetActiveUniformFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveUniform'.");
            }
        }
        public static void glGetActiveUniform(GLuint @program, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLint * @size, GLenum * @type, GLchar * @name) => glGetActiveUniformPtr(@program, @index, @bufSize, @length, @size, @type, @name);

        internal delegate void glGetAttachedShadersFunc(GLuint @program, GLsizei @maxCount, GLsizei * @count, GLuint * @shaders);
        internal static glGetAttachedShadersFunc glGetAttachedShadersPtr;
        internal static void loadGetAttachedShaders()
        {
            try
            {
                glGetAttachedShadersPtr = (glGetAttachedShadersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetAttachedShaders"), typeof(glGetAttachedShadersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetAttachedShaders'.");
            }
        }
        public static void glGetAttachedShaders(GLuint @program, GLsizei @maxCount, GLsizei * @count, GLuint * @shaders) => glGetAttachedShadersPtr(@program, @maxCount, @count, @shaders);

        internal delegate GLint glGetAttribLocationFunc(GLuint @program, const GLchar * @name);
        internal static glGetAttribLocationFunc glGetAttribLocationPtr;
        internal static void loadGetAttribLocation()
        {
            try
            {
                glGetAttribLocationPtr = (glGetAttribLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetAttribLocation"), typeof(glGetAttribLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetAttribLocation'.");
            }
        }
        public static GLint glGetAttribLocation(GLuint @program, const GLchar * @name) => glGetAttribLocationPtr(@program, @name);

        internal delegate void glGetProgramivFunc(GLuint @program, GLenum @pname, GLint * @params);
        internal static glGetProgramivFunc glGetProgramivPtr;
        internal static void loadGetProgramiv()
        {
            try
            {
                glGetProgramivPtr = (glGetProgramivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramiv"), typeof(glGetProgramivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramiv'.");
            }
        }
        public static void glGetProgramiv(GLuint @program, GLenum @pname, GLint * @params) => glGetProgramivPtr(@program, @pname, @params);

        internal delegate void glGetProgramInfoLogFunc(GLuint @program, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog);
        internal static glGetProgramInfoLogFunc glGetProgramInfoLogPtr;
        internal static void loadGetProgramInfoLog()
        {
            try
            {
                glGetProgramInfoLogPtr = (glGetProgramInfoLogFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramInfoLog"), typeof(glGetProgramInfoLogFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramInfoLog'.");
            }
        }
        public static void glGetProgramInfoLog(GLuint @program, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog) => glGetProgramInfoLogPtr(@program, @bufSize, @length, @infoLog);

        internal delegate void glGetShaderivFunc(GLuint @shader, GLenum @pname, GLint * @params);
        internal static glGetShaderivFunc glGetShaderivPtr;
        internal static void loadGetShaderiv()
        {
            try
            {
                glGetShaderivPtr = (glGetShaderivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetShaderiv"), typeof(glGetShaderivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetShaderiv'.");
            }
        }
        public static void glGetShaderiv(GLuint @shader, GLenum @pname, GLint * @params) => glGetShaderivPtr(@shader, @pname, @params);

        internal delegate void glGetShaderInfoLogFunc(GLuint @shader, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog);
        internal static glGetShaderInfoLogFunc glGetShaderInfoLogPtr;
        internal static void loadGetShaderInfoLog()
        {
            try
            {
                glGetShaderInfoLogPtr = (glGetShaderInfoLogFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetShaderInfoLog"), typeof(glGetShaderInfoLogFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetShaderInfoLog'.");
            }
        }
        public static void glGetShaderInfoLog(GLuint @shader, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog) => glGetShaderInfoLogPtr(@shader, @bufSize, @length, @infoLog);

        internal delegate void glGetShaderSourceFunc(GLuint @shader, GLsizei @bufSize, GLsizei * @length, GLchar * @source);
        internal static glGetShaderSourceFunc glGetShaderSourcePtr;
        internal static void loadGetShaderSource()
        {
            try
            {
                glGetShaderSourcePtr = (glGetShaderSourceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetShaderSource"), typeof(glGetShaderSourceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetShaderSource'.");
            }
        }
        public static void glGetShaderSource(GLuint @shader, GLsizei @bufSize, GLsizei * @length, GLchar * @source) => glGetShaderSourcePtr(@shader, @bufSize, @length, @source);

        internal delegate GLint glGetUniformLocationFunc(GLuint @program, const GLchar * @name);
        internal static glGetUniformLocationFunc glGetUniformLocationPtr;
        internal static void loadGetUniformLocation()
        {
            try
            {
                glGetUniformLocationPtr = (glGetUniformLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformLocation"), typeof(glGetUniformLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformLocation'.");
            }
        }
        public static GLint glGetUniformLocation(GLuint @program, const GLchar * @name) => glGetUniformLocationPtr(@program, @name);

        internal delegate void glGetUniformfvFunc(GLuint @program, GLint @location, GLfloat * @params);
        internal static glGetUniformfvFunc glGetUniformfvPtr;
        internal static void loadGetUniformfv()
        {
            try
            {
                glGetUniformfvPtr = (glGetUniformfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformfv"), typeof(glGetUniformfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformfv'.");
            }
        }
        public static void glGetUniformfv(GLuint @program, GLint @location, GLfloat * @params) => glGetUniformfvPtr(@program, @location, @params);

        internal delegate void glGetUniformivFunc(GLuint @program, GLint @location, GLint * @params);
        internal static glGetUniformivFunc glGetUniformivPtr;
        internal static void loadGetUniformiv()
        {
            try
            {
                glGetUniformivPtr = (glGetUniformivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformiv"), typeof(glGetUniformivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformiv'.");
            }
        }
        public static void glGetUniformiv(GLuint @program, GLint @location, GLint * @params) => glGetUniformivPtr(@program, @location, @params);

        internal delegate void glGetVertexAttribdvFunc(GLuint @index, GLenum @pname, GLdouble * @params);
        internal static glGetVertexAttribdvFunc glGetVertexAttribdvPtr;
        internal static void loadGetVertexAttribdv()
        {
            try
            {
                glGetVertexAttribdvPtr = (glGetVertexAttribdvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribdv"), typeof(glGetVertexAttribdvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribdv'.");
            }
        }
        public static void glGetVertexAttribdv(GLuint @index, GLenum @pname, GLdouble * @params) => glGetVertexAttribdvPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribfvFunc(GLuint @index, GLenum @pname, GLfloat * @params);
        internal static glGetVertexAttribfvFunc glGetVertexAttribfvPtr;
        internal static void loadGetVertexAttribfv()
        {
            try
            {
                glGetVertexAttribfvPtr = (glGetVertexAttribfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribfv"), typeof(glGetVertexAttribfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribfv'.");
            }
        }
        public static void glGetVertexAttribfv(GLuint @index, GLenum @pname, GLfloat * @params) => glGetVertexAttribfvPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribivFunc(GLuint @index, GLenum @pname, GLint * @params);
        internal static glGetVertexAttribivFunc glGetVertexAttribivPtr;
        internal static void loadGetVertexAttribiv()
        {
            try
            {
                glGetVertexAttribivPtr = (glGetVertexAttribivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribiv"), typeof(glGetVertexAttribivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribiv'.");
            }
        }
        public static void glGetVertexAttribiv(GLuint @index, GLenum @pname, GLint * @params) => glGetVertexAttribivPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribPointervFunc(GLuint @index, GLenum @pname, void ** @pointer);
        internal static glGetVertexAttribPointervFunc glGetVertexAttribPointervPtr;
        internal static void loadGetVertexAttribPointerv()
        {
            try
            {
                glGetVertexAttribPointervPtr = (glGetVertexAttribPointervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribPointerv"), typeof(glGetVertexAttribPointervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribPointerv'.");
            }
        }
        public static void glGetVertexAttribPointerv(GLuint @index, GLenum @pname, void ** @pointer) => glGetVertexAttribPointervPtr(@index, @pname, @pointer);

        internal delegate GLboolean glIsProgramFunc(GLuint @program);
        internal static glIsProgramFunc glIsProgramPtr;
        internal static void loadIsProgram()
        {
            try
            {
                glIsProgramPtr = (glIsProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsProgram"), typeof(glIsProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsProgram'.");
            }
        }
        public static GLboolean glIsProgram(GLuint @program) => glIsProgramPtr(@program);

        internal delegate GLboolean glIsShaderFunc(GLuint @shader);
        internal static glIsShaderFunc glIsShaderPtr;
        internal static void loadIsShader()
        {
            try
            {
                glIsShaderPtr = (glIsShaderFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsShader"), typeof(glIsShaderFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsShader'.");
            }
        }
        public static GLboolean glIsShader(GLuint @shader) => glIsShaderPtr(@shader);

        internal delegate void glLinkProgramFunc(GLuint @program);
        internal static glLinkProgramFunc glLinkProgramPtr;
        internal static void loadLinkProgram()
        {
            try
            {
                glLinkProgramPtr = (glLinkProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLinkProgram"), typeof(glLinkProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLinkProgram'.");
            }
        }
        public static void glLinkProgram(GLuint @program) => glLinkProgramPtr(@program);

        internal delegate void glShaderSourceFunc(GLuint @shader, GLsizei @count, const GLchar *const* @string, const GLint * @length);
        internal static glShaderSourceFunc glShaderSourcePtr;
        internal static void loadShaderSource()
        {
            try
            {
                glShaderSourcePtr = (glShaderSourceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShaderSource"), typeof(glShaderSourceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShaderSource'.");
            }
        }
        public static void glShaderSource(GLuint @shader, GLsizei @count, const GLchar *const* @string, const GLint * @length) => glShaderSourcePtr(@shader, @count, @string, @length);

        internal delegate void glUseProgramFunc(GLuint @program);
        internal static glUseProgramFunc glUseProgramPtr;
        internal static void loadUseProgram()
        {
            try
            {
                glUseProgramPtr = (glUseProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUseProgram"), typeof(glUseProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUseProgram'.");
            }
        }
        public static void glUseProgram(GLuint @program) => glUseProgramPtr(@program);

        internal delegate void glUniform1fFunc(GLint @location, GLfloat @v0);
        internal static glUniform1fFunc glUniform1fPtr;
        internal static void loadUniform1f()
        {
            try
            {
                glUniform1fPtr = (glUniform1fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1f"), typeof(glUniform1fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1f'.");
            }
        }
        public static void glUniform1f(GLint @location, GLfloat @v0) => glUniform1fPtr(@location, @v0);

        internal delegate void glUniform2fFunc(GLint @location, GLfloat @v0, GLfloat @v1);
        internal static glUniform2fFunc glUniform2fPtr;
        internal static void loadUniform2f()
        {
            try
            {
                glUniform2fPtr = (glUniform2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2f"), typeof(glUniform2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2f'.");
            }
        }
        public static void glUniform2f(GLint @location, GLfloat @v0, GLfloat @v1) => glUniform2fPtr(@location, @v0, @v1);

        internal delegate void glUniform3fFunc(GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2);
        internal static glUniform3fFunc glUniform3fPtr;
        internal static void loadUniform3f()
        {
            try
            {
                glUniform3fPtr = (glUniform3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3f"), typeof(glUniform3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3f'.");
            }
        }
        public static void glUniform3f(GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2) => glUniform3fPtr(@location, @v0, @v1, @v2);

        internal delegate void glUniform4fFunc(GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3);
        internal static glUniform4fFunc glUniform4fPtr;
        internal static void loadUniform4f()
        {
            try
            {
                glUniform4fPtr = (glUniform4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4f"), typeof(glUniform4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4f'.");
            }
        }
        public static void glUniform4f(GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3) => glUniform4fPtr(@location, @v0, @v1, @v2, @v3);

        internal delegate void glUniform1iFunc(GLint @location, GLint @v0);
        internal static glUniform1iFunc glUniform1iPtr;
        internal static void loadUniform1i()
        {
            try
            {
                glUniform1iPtr = (glUniform1iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1i"), typeof(glUniform1iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1i'.");
            }
        }
        public static void glUniform1i(GLint @location, GLint @v0) => glUniform1iPtr(@location, @v0);

        internal delegate void glUniform2iFunc(GLint @location, GLint @v0, GLint @v1);
        internal static glUniform2iFunc glUniform2iPtr;
        internal static void loadUniform2i()
        {
            try
            {
                glUniform2iPtr = (glUniform2iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2i"), typeof(glUniform2iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2i'.");
            }
        }
        public static void glUniform2i(GLint @location, GLint @v0, GLint @v1) => glUniform2iPtr(@location, @v0, @v1);

        internal delegate void glUniform3iFunc(GLint @location, GLint @v0, GLint @v1, GLint @v2);
        internal static glUniform3iFunc glUniform3iPtr;
        internal static void loadUniform3i()
        {
            try
            {
                glUniform3iPtr = (glUniform3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3i"), typeof(glUniform3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3i'.");
            }
        }
        public static void glUniform3i(GLint @location, GLint @v0, GLint @v1, GLint @v2) => glUniform3iPtr(@location, @v0, @v1, @v2);

        internal delegate void glUniform4iFunc(GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3);
        internal static glUniform4iFunc glUniform4iPtr;
        internal static void loadUniform4i()
        {
            try
            {
                glUniform4iPtr = (glUniform4iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4i"), typeof(glUniform4iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4i'.");
            }
        }
        public static void glUniform4i(GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3) => glUniform4iPtr(@location, @v0, @v1, @v2, @v3);

        internal delegate void glUniform1fvFunc(GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glUniform1fvFunc glUniform1fvPtr;
        internal static void loadUniform1fv()
        {
            try
            {
                glUniform1fvPtr = (glUniform1fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1fv"), typeof(glUniform1fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1fv'.");
            }
        }
        public static void glUniform1fv(GLint @location, GLsizei @count, const GLfloat * @value) => glUniform1fvPtr(@location, @count, @value);

        internal delegate void glUniform2fvFunc(GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glUniform2fvFunc glUniform2fvPtr;
        internal static void loadUniform2fv()
        {
            try
            {
                glUniform2fvPtr = (glUniform2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2fv"), typeof(glUniform2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2fv'.");
            }
        }
        public static void glUniform2fv(GLint @location, GLsizei @count, const GLfloat * @value) => glUniform2fvPtr(@location, @count, @value);

        internal delegate void glUniform3fvFunc(GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glUniform3fvFunc glUniform3fvPtr;
        internal static void loadUniform3fv()
        {
            try
            {
                glUniform3fvPtr = (glUniform3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3fv"), typeof(glUniform3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3fv'.");
            }
        }
        public static void glUniform3fv(GLint @location, GLsizei @count, const GLfloat * @value) => glUniform3fvPtr(@location, @count, @value);

        internal delegate void glUniform4fvFunc(GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glUniform4fvFunc glUniform4fvPtr;
        internal static void loadUniform4fv()
        {
            try
            {
                glUniform4fvPtr = (glUniform4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4fv"), typeof(glUniform4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4fv'.");
            }
        }
        public static void glUniform4fv(GLint @location, GLsizei @count, const GLfloat * @value) => glUniform4fvPtr(@location, @count, @value);

        internal delegate void glUniform1ivFunc(GLint @location, GLsizei @count, const GLint * @value);
        internal static glUniform1ivFunc glUniform1ivPtr;
        internal static void loadUniform1iv()
        {
            try
            {
                glUniform1ivPtr = (glUniform1ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1iv"), typeof(glUniform1ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1iv'.");
            }
        }
        public static void glUniform1iv(GLint @location, GLsizei @count, const GLint * @value) => glUniform1ivPtr(@location, @count, @value);

        internal delegate void glUniform2ivFunc(GLint @location, GLsizei @count, const GLint * @value);
        internal static glUniform2ivFunc glUniform2ivPtr;
        internal static void loadUniform2iv()
        {
            try
            {
                glUniform2ivPtr = (glUniform2ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2iv"), typeof(glUniform2ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2iv'.");
            }
        }
        public static void glUniform2iv(GLint @location, GLsizei @count, const GLint * @value) => glUniform2ivPtr(@location, @count, @value);

        internal delegate void glUniform3ivFunc(GLint @location, GLsizei @count, const GLint * @value);
        internal static glUniform3ivFunc glUniform3ivPtr;
        internal static void loadUniform3iv()
        {
            try
            {
                glUniform3ivPtr = (glUniform3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3iv"), typeof(glUniform3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3iv'.");
            }
        }
        public static void glUniform3iv(GLint @location, GLsizei @count, const GLint * @value) => glUniform3ivPtr(@location, @count, @value);

        internal delegate void glUniform4ivFunc(GLint @location, GLsizei @count, const GLint * @value);
        internal static glUniform4ivFunc glUniform4ivPtr;
        internal static void loadUniform4iv()
        {
            try
            {
                glUniform4ivPtr = (glUniform4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4iv"), typeof(glUniform4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4iv'.");
            }
        }
        public static void glUniform4iv(GLint @location, GLsizei @count, const GLint * @value) => glUniform4ivPtr(@location, @count, @value);

        internal delegate void glUniformMatrix2fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix2fvFunc glUniformMatrix2fvPtr;
        internal static void loadUniformMatrix2fv()
        {
            try
            {
                glUniformMatrix2fvPtr = (glUniformMatrix2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2fv"), typeof(glUniformMatrix2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2fv'.");
            }
        }
        public static void glUniformMatrix2fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix2fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix3fvFunc glUniformMatrix3fvPtr;
        internal static void loadUniformMatrix3fv()
        {
            try
            {
                glUniformMatrix3fvPtr = (glUniformMatrix3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3fv"), typeof(glUniformMatrix3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3fv'.");
            }
        }
        public static void glUniformMatrix3fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix3fvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4fvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix4fvFunc glUniformMatrix4fvPtr;
        internal static void loadUniformMatrix4fv()
        {
            try
            {
                glUniformMatrix4fvPtr = (glUniformMatrix4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4fv"), typeof(glUniformMatrix4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4fv'.");
            }
        }
        public static void glUniformMatrix4fv(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix4fvPtr(@location, @count, @transpose, @value);

        internal delegate void glValidateProgramFunc(GLuint @program);
        internal static glValidateProgramFunc glValidateProgramPtr;
        internal static void loadValidateProgram()
        {
            try
            {
                glValidateProgramPtr = (glValidateProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glValidateProgram"), typeof(glValidateProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glValidateProgram'.");
            }
        }
        public static void glValidateProgram(GLuint @program) => glValidateProgramPtr(@program);

        internal delegate void glVertexAttrib1dFunc(GLuint @index, GLdouble @x);
        internal static glVertexAttrib1dFunc glVertexAttrib1dPtr;
        internal static void loadVertexAttrib1d()
        {
            try
            {
                glVertexAttrib1dPtr = (glVertexAttrib1dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1d"), typeof(glVertexAttrib1dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1d'.");
            }
        }
        public static void glVertexAttrib1d(GLuint @index, GLdouble @x) => glVertexAttrib1dPtr(@index, @x);

        internal delegate void glVertexAttrib1dvFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttrib1dvFunc glVertexAttrib1dvPtr;
        internal static void loadVertexAttrib1dv()
        {
            try
            {
                glVertexAttrib1dvPtr = (glVertexAttrib1dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1dv"), typeof(glVertexAttrib1dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1dv'.");
            }
        }
        public static void glVertexAttrib1dv(GLuint @index, const GLdouble * @v) => glVertexAttrib1dvPtr(@index, @v);

        internal delegate void glVertexAttrib1fFunc(GLuint @index, GLfloat @x);
        internal static glVertexAttrib1fFunc glVertexAttrib1fPtr;
        internal static void loadVertexAttrib1f()
        {
            try
            {
                glVertexAttrib1fPtr = (glVertexAttrib1fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1f"), typeof(glVertexAttrib1fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1f'.");
            }
        }
        public static void glVertexAttrib1f(GLuint @index, GLfloat @x) => glVertexAttrib1fPtr(@index, @x);

        internal delegate void glVertexAttrib1fvFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib1fvFunc glVertexAttrib1fvPtr;
        internal static void loadVertexAttrib1fv()
        {
            try
            {
                glVertexAttrib1fvPtr = (glVertexAttrib1fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1fv"), typeof(glVertexAttrib1fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1fv'.");
            }
        }
        public static void glVertexAttrib1fv(GLuint @index, const GLfloat * @v) => glVertexAttrib1fvPtr(@index, @v);

        internal delegate void glVertexAttrib1sFunc(GLuint @index, GLshort @x);
        internal static glVertexAttrib1sFunc glVertexAttrib1sPtr;
        internal static void loadVertexAttrib1s()
        {
            try
            {
                glVertexAttrib1sPtr = (glVertexAttrib1sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1s"), typeof(glVertexAttrib1sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1s'.");
            }
        }
        public static void glVertexAttrib1s(GLuint @index, GLshort @x) => glVertexAttrib1sPtr(@index, @x);

        internal delegate void glVertexAttrib1svFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib1svFunc glVertexAttrib1svPtr;
        internal static void loadVertexAttrib1sv()
        {
            try
            {
                glVertexAttrib1svPtr = (glVertexAttrib1svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1sv"), typeof(glVertexAttrib1svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1sv'.");
            }
        }
        public static void glVertexAttrib1sv(GLuint @index, const GLshort * @v) => glVertexAttrib1svPtr(@index, @v);

        internal delegate void glVertexAttrib2dFunc(GLuint @index, GLdouble @x, GLdouble @y);
        internal static glVertexAttrib2dFunc glVertexAttrib2dPtr;
        internal static void loadVertexAttrib2d()
        {
            try
            {
                glVertexAttrib2dPtr = (glVertexAttrib2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2d"), typeof(glVertexAttrib2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2d'.");
            }
        }
        public static void glVertexAttrib2d(GLuint @index, GLdouble @x, GLdouble @y) => glVertexAttrib2dPtr(@index, @x, @y);

        internal delegate void glVertexAttrib2dvFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttrib2dvFunc glVertexAttrib2dvPtr;
        internal static void loadVertexAttrib2dv()
        {
            try
            {
                glVertexAttrib2dvPtr = (glVertexAttrib2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2dv"), typeof(glVertexAttrib2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2dv'.");
            }
        }
        public static void glVertexAttrib2dv(GLuint @index, const GLdouble * @v) => glVertexAttrib2dvPtr(@index, @v);

        internal delegate void glVertexAttrib2fFunc(GLuint @index, GLfloat @x, GLfloat @y);
        internal static glVertexAttrib2fFunc glVertexAttrib2fPtr;
        internal static void loadVertexAttrib2f()
        {
            try
            {
                glVertexAttrib2fPtr = (glVertexAttrib2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2f"), typeof(glVertexAttrib2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2f'.");
            }
        }
        public static void glVertexAttrib2f(GLuint @index, GLfloat @x, GLfloat @y) => glVertexAttrib2fPtr(@index, @x, @y);

        internal delegate void glVertexAttrib2fvFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib2fvFunc glVertexAttrib2fvPtr;
        internal static void loadVertexAttrib2fv()
        {
            try
            {
                glVertexAttrib2fvPtr = (glVertexAttrib2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2fv"), typeof(glVertexAttrib2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2fv'.");
            }
        }
        public static void glVertexAttrib2fv(GLuint @index, const GLfloat * @v) => glVertexAttrib2fvPtr(@index, @v);

        internal delegate void glVertexAttrib2sFunc(GLuint @index, GLshort @x, GLshort @y);
        internal static glVertexAttrib2sFunc glVertexAttrib2sPtr;
        internal static void loadVertexAttrib2s()
        {
            try
            {
                glVertexAttrib2sPtr = (glVertexAttrib2sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2s"), typeof(glVertexAttrib2sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2s'.");
            }
        }
        public static void glVertexAttrib2s(GLuint @index, GLshort @x, GLshort @y) => glVertexAttrib2sPtr(@index, @x, @y);

        internal delegate void glVertexAttrib2svFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib2svFunc glVertexAttrib2svPtr;
        internal static void loadVertexAttrib2sv()
        {
            try
            {
                glVertexAttrib2svPtr = (glVertexAttrib2svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2sv"), typeof(glVertexAttrib2svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2sv'.");
            }
        }
        public static void glVertexAttrib2sv(GLuint @index, const GLshort * @v) => glVertexAttrib2svPtr(@index, @v);

        internal delegate void glVertexAttrib3dFunc(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glVertexAttrib3dFunc glVertexAttrib3dPtr;
        internal static void loadVertexAttrib3d()
        {
            try
            {
                glVertexAttrib3dPtr = (glVertexAttrib3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3d"), typeof(glVertexAttrib3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3d'.");
            }
        }
        public static void glVertexAttrib3d(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z) => glVertexAttrib3dPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttrib3dvFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttrib3dvFunc glVertexAttrib3dvPtr;
        internal static void loadVertexAttrib3dv()
        {
            try
            {
                glVertexAttrib3dvPtr = (glVertexAttrib3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3dv"), typeof(glVertexAttrib3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3dv'.");
            }
        }
        public static void glVertexAttrib3dv(GLuint @index, const GLdouble * @v) => glVertexAttrib3dvPtr(@index, @v);

        internal delegate void glVertexAttrib3fFunc(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glVertexAttrib3fFunc glVertexAttrib3fPtr;
        internal static void loadVertexAttrib3f()
        {
            try
            {
                glVertexAttrib3fPtr = (glVertexAttrib3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3f"), typeof(glVertexAttrib3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3f'.");
            }
        }
        public static void glVertexAttrib3f(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z) => glVertexAttrib3fPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttrib3fvFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib3fvFunc glVertexAttrib3fvPtr;
        internal static void loadVertexAttrib3fv()
        {
            try
            {
                glVertexAttrib3fvPtr = (glVertexAttrib3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3fv"), typeof(glVertexAttrib3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3fv'.");
            }
        }
        public static void glVertexAttrib3fv(GLuint @index, const GLfloat * @v) => glVertexAttrib3fvPtr(@index, @v);

        internal delegate void glVertexAttrib3sFunc(GLuint @index, GLshort @x, GLshort @y, GLshort @z);
        internal static glVertexAttrib3sFunc glVertexAttrib3sPtr;
        internal static void loadVertexAttrib3s()
        {
            try
            {
                glVertexAttrib3sPtr = (glVertexAttrib3sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3s"), typeof(glVertexAttrib3sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3s'.");
            }
        }
        public static void glVertexAttrib3s(GLuint @index, GLshort @x, GLshort @y, GLshort @z) => glVertexAttrib3sPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttrib3svFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib3svFunc glVertexAttrib3svPtr;
        internal static void loadVertexAttrib3sv()
        {
            try
            {
                glVertexAttrib3svPtr = (glVertexAttrib3svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3sv"), typeof(glVertexAttrib3svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3sv'.");
            }
        }
        public static void glVertexAttrib3sv(GLuint @index, const GLshort * @v) => glVertexAttrib3svPtr(@index, @v);

        internal delegate void glVertexAttrib4NbvFunc(GLuint @index, const GLbyte * @v);
        internal static glVertexAttrib4NbvFunc glVertexAttrib4NbvPtr;
        internal static void loadVertexAttrib4Nbv()
        {
            try
            {
                glVertexAttrib4NbvPtr = (glVertexAttrib4NbvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4Nbv"), typeof(glVertexAttrib4NbvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4Nbv'.");
            }
        }
        public static void glVertexAttrib4Nbv(GLuint @index, const GLbyte * @v) => glVertexAttrib4NbvPtr(@index, @v);

        internal delegate void glVertexAttrib4NivFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttrib4NivFunc glVertexAttrib4NivPtr;
        internal static void loadVertexAttrib4Niv()
        {
            try
            {
                glVertexAttrib4NivPtr = (glVertexAttrib4NivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4Niv"), typeof(glVertexAttrib4NivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4Niv'.");
            }
        }
        public static void glVertexAttrib4Niv(GLuint @index, const GLint * @v) => glVertexAttrib4NivPtr(@index, @v);

        internal delegate void glVertexAttrib4NsvFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib4NsvFunc glVertexAttrib4NsvPtr;
        internal static void loadVertexAttrib4Nsv()
        {
            try
            {
                glVertexAttrib4NsvPtr = (glVertexAttrib4NsvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4Nsv"), typeof(glVertexAttrib4NsvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4Nsv'.");
            }
        }
        public static void glVertexAttrib4Nsv(GLuint @index, const GLshort * @v) => glVertexAttrib4NsvPtr(@index, @v);

        internal delegate void glVertexAttrib4NubFunc(GLuint @index, GLubyte @x, GLubyte @y, GLubyte @z, GLubyte @w);
        internal static glVertexAttrib4NubFunc glVertexAttrib4NubPtr;
        internal static void loadVertexAttrib4Nub()
        {
            try
            {
                glVertexAttrib4NubPtr = (glVertexAttrib4NubFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4Nub"), typeof(glVertexAttrib4NubFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4Nub'.");
            }
        }
        public static void glVertexAttrib4Nub(GLuint @index, GLubyte @x, GLubyte @y, GLubyte @z, GLubyte @w) => glVertexAttrib4NubPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4NubvFunc(GLuint @index, const GLubyte * @v);
        internal static glVertexAttrib4NubvFunc glVertexAttrib4NubvPtr;
        internal static void loadVertexAttrib4Nubv()
        {
            try
            {
                glVertexAttrib4NubvPtr = (glVertexAttrib4NubvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4Nubv"), typeof(glVertexAttrib4NubvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4Nubv'.");
            }
        }
        public static void glVertexAttrib4Nubv(GLuint @index, const GLubyte * @v) => glVertexAttrib4NubvPtr(@index, @v);

        internal delegate void glVertexAttrib4NuivFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttrib4NuivFunc glVertexAttrib4NuivPtr;
        internal static void loadVertexAttrib4Nuiv()
        {
            try
            {
                glVertexAttrib4NuivPtr = (glVertexAttrib4NuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4Nuiv"), typeof(glVertexAttrib4NuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4Nuiv'.");
            }
        }
        public static void glVertexAttrib4Nuiv(GLuint @index, const GLuint * @v) => glVertexAttrib4NuivPtr(@index, @v);

        internal delegate void glVertexAttrib4NusvFunc(GLuint @index, const GLushort * @v);
        internal static glVertexAttrib4NusvFunc glVertexAttrib4NusvPtr;
        internal static void loadVertexAttrib4Nusv()
        {
            try
            {
                glVertexAttrib4NusvPtr = (glVertexAttrib4NusvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4Nusv"), typeof(glVertexAttrib4NusvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4Nusv'.");
            }
        }
        public static void glVertexAttrib4Nusv(GLuint @index, const GLushort * @v) => glVertexAttrib4NusvPtr(@index, @v);

        internal delegate void glVertexAttrib4bvFunc(GLuint @index, const GLbyte * @v);
        internal static glVertexAttrib4bvFunc glVertexAttrib4bvPtr;
        internal static void loadVertexAttrib4bv()
        {
            try
            {
                glVertexAttrib4bvPtr = (glVertexAttrib4bvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4bv"), typeof(glVertexAttrib4bvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4bv'.");
            }
        }
        public static void glVertexAttrib4bv(GLuint @index, const GLbyte * @v) => glVertexAttrib4bvPtr(@index, @v);

        internal delegate void glVertexAttrib4dFunc(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glVertexAttrib4dFunc glVertexAttrib4dPtr;
        internal static void loadVertexAttrib4d()
        {
            try
            {
                glVertexAttrib4dPtr = (glVertexAttrib4dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4d"), typeof(glVertexAttrib4dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4d'.");
            }
        }
        public static void glVertexAttrib4d(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glVertexAttrib4dPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4dvFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttrib4dvFunc glVertexAttrib4dvPtr;
        internal static void loadVertexAttrib4dv()
        {
            try
            {
                glVertexAttrib4dvPtr = (glVertexAttrib4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4dv"), typeof(glVertexAttrib4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4dv'.");
            }
        }
        public static void glVertexAttrib4dv(GLuint @index, const GLdouble * @v) => glVertexAttrib4dvPtr(@index, @v);

        internal delegate void glVertexAttrib4fFunc(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glVertexAttrib4fFunc glVertexAttrib4fPtr;
        internal static void loadVertexAttrib4f()
        {
            try
            {
                glVertexAttrib4fPtr = (glVertexAttrib4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4f"), typeof(glVertexAttrib4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4f'.");
            }
        }
        public static void glVertexAttrib4f(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glVertexAttrib4fPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4fvFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib4fvFunc glVertexAttrib4fvPtr;
        internal static void loadVertexAttrib4fv()
        {
            try
            {
                glVertexAttrib4fvPtr = (glVertexAttrib4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4fv"), typeof(glVertexAttrib4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4fv'.");
            }
        }
        public static void glVertexAttrib4fv(GLuint @index, const GLfloat * @v) => glVertexAttrib4fvPtr(@index, @v);

        internal delegate void glVertexAttrib4ivFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttrib4ivFunc glVertexAttrib4ivPtr;
        internal static void loadVertexAttrib4iv()
        {
            try
            {
                glVertexAttrib4ivPtr = (glVertexAttrib4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4iv"), typeof(glVertexAttrib4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4iv'.");
            }
        }
        public static void glVertexAttrib4iv(GLuint @index, const GLint * @v) => glVertexAttrib4ivPtr(@index, @v);

        internal delegate void glVertexAttrib4sFunc(GLuint @index, GLshort @x, GLshort @y, GLshort @z, GLshort @w);
        internal static glVertexAttrib4sFunc glVertexAttrib4sPtr;
        internal static void loadVertexAttrib4s()
        {
            try
            {
                glVertexAttrib4sPtr = (glVertexAttrib4sFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4s"), typeof(glVertexAttrib4sFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4s'.");
            }
        }
        public static void glVertexAttrib4s(GLuint @index, GLshort @x, GLshort @y, GLshort @z, GLshort @w) => glVertexAttrib4sPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4svFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib4svFunc glVertexAttrib4svPtr;
        internal static void loadVertexAttrib4sv()
        {
            try
            {
                glVertexAttrib4svPtr = (glVertexAttrib4svFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4sv"), typeof(glVertexAttrib4svFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4sv'.");
            }
        }
        public static void glVertexAttrib4sv(GLuint @index, const GLshort * @v) => glVertexAttrib4svPtr(@index, @v);

        internal delegate void glVertexAttrib4ubvFunc(GLuint @index, const GLubyte * @v);
        internal static glVertexAttrib4ubvFunc glVertexAttrib4ubvPtr;
        internal static void loadVertexAttrib4ubv()
        {
            try
            {
                glVertexAttrib4ubvPtr = (glVertexAttrib4ubvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4ubv"), typeof(glVertexAttrib4ubvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4ubv'.");
            }
        }
        public static void glVertexAttrib4ubv(GLuint @index, const GLubyte * @v) => glVertexAttrib4ubvPtr(@index, @v);

        internal delegate void glVertexAttrib4uivFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttrib4uivFunc glVertexAttrib4uivPtr;
        internal static void loadVertexAttrib4uiv()
        {
            try
            {
                glVertexAttrib4uivPtr = (glVertexAttrib4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4uiv"), typeof(glVertexAttrib4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4uiv'.");
            }
        }
        public static void glVertexAttrib4uiv(GLuint @index, const GLuint * @v) => glVertexAttrib4uivPtr(@index, @v);

        internal delegate void glVertexAttrib4usvFunc(GLuint @index, const GLushort * @v);
        internal static glVertexAttrib4usvFunc glVertexAttrib4usvPtr;
        internal static void loadVertexAttrib4usv()
        {
            try
            {
                glVertexAttrib4usvPtr = (glVertexAttrib4usvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4usv"), typeof(glVertexAttrib4usvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4usv'.");
            }
        }
        public static void glVertexAttrib4usv(GLuint @index, const GLushort * @v) => glVertexAttrib4usvPtr(@index, @v);

        internal delegate void glVertexAttribPointerFunc(GLuint @index, GLint @size, GLenum @type, GLboolean @normalized, GLsizei @stride, const void * @pointer);
        internal static glVertexAttribPointerFunc glVertexAttribPointerPtr;
        internal static void loadVertexAttribPointer()
        {
            try
            {
                glVertexAttribPointerPtr = (glVertexAttribPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribPointer"), typeof(glVertexAttribPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribPointer'.");
            }
        }
        public static void glVertexAttribPointer(GLuint @index, GLint @size, GLenum @type, GLboolean @normalized, GLsizei @stride, const void * @pointer) => glVertexAttribPointerPtr(@index, @size, @type, @normalized, @stride, @pointer);
        #endregion
    }
}
