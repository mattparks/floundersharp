using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL44
    {
        #region Interop
        static GL44()
        {
            Console.WriteLine("Initalising GL44 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBufferStorage();
            loadClearTexImage();
            loadClearTexSubImage();
            loadBindBuffersBase();
            loadBindBuffersRange();
            loadBindTextures();
            loadBindSamplers();
            loadBindImageTextures();
            loadBindVertexBuffers();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_VERTEX_ATTRIB_STRIDE = 0x82E5;
        public static UInt32 GL_PRIMITIVE_RESTART_FOR_PATCHES_SUPPORTED = 0x8221;
        public static UInt32 GL_TEXTURE_BUFFER_BINDING = 0x8C2A;
        public static UInt32 GL_MAP_READ_BIT = 0x0001;
        public static UInt32 GL_MAP_WRITE_BIT = 0x0002;
        public static UInt32 GL_MAP_PERSISTENT_BIT = 0x0040;
        public static UInt32 GL_MAP_COHERENT_BIT = 0x0080;
        public static UInt32 GL_DYNAMIC_STORAGE_BIT = 0x0100;
        public static UInt32 GL_CLIENT_STORAGE_BIT = 0x0200;
        public static UInt32 GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT = 0x00004000;
        public static UInt32 GL_BUFFER_IMMUTABLE_STORAGE = 0x821F;
        public static UInt32 GL_BUFFER_STORAGE_FLAGS = 0x8220;
        public static UInt32 GL_CLEAR_TEXTURE = 0x9365;
        public static UInt32 GL_LOCATION_COMPONENT = 0x934A;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER = 0x8C8E;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_INDEX = 0x934B;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_STRIDE = 0x934C;
        public static UInt32 GL_QUERY_BUFFER = 0x9192;
        public static UInt32 GL_QUERY_BUFFER_BARRIER_BIT = 0x00008000;
        public static UInt32 GL_QUERY_BUFFER_BINDING = 0x9193;
        public static UInt32 GL_QUERY_RESULT_NO_WAIT = 0x9194;
        public static UInt32 GL_MIRROR_CLAMP_TO_EDGE = 0x8743;
        public static UInt32 GL_STENCIL_INDEX = 0x1901;
        public static UInt32 GL_STENCIL_INDEX8 = 0x8D48;
        public static UInt32 GL_UNSIGNED_INT_10F_11F_11F_REV = 0x8C3B;
        #endregion

        #region Commands
        internal delegate void glBufferStorageFunc(GLenum @target, GLsizeiptr @size, const void * @data, GLbitfield @flags);
        internal static glBufferStorageFunc glBufferStoragePtr;
        internal static void loadBufferStorage()
        {
            try
            {
                glBufferStoragePtr = (glBufferStorageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferStorage"), typeof(glBufferStorageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferStorage'.");
            }
        }
        public static void glBufferStorage(GLenum @target, GLsizeiptr @size, const void * @data, GLbitfield @flags) => glBufferStoragePtr(@target, @size, @data, @flags);

        internal delegate void glClearTexImageFunc(GLuint @texture, GLint @level, GLenum @format, GLenum @type, const void * @data);
        internal static glClearTexImageFunc glClearTexImagePtr;
        internal static void loadClearTexImage()
        {
            try
            {
                glClearTexImagePtr = (glClearTexImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearTexImage"), typeof(glClearTexImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearTexImage'.");
            }
        }
        public static void glClearTexImage(GLuint @texture, GLint @level, GLenum @format, GLenum @type, const void * @data) => glClearTexImagePtr(@texture, @level, @format, @type, @data);

        internal delegate void glClearTexSubImageFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @data);
        internal static glClearTexSubImageFunc glClearTexSubImagePtr;
        internal static void loadClearTexSubImage()
        {
            try
            {
                glClearTexSubImagePtr = (glClearTexSubImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearTexSubImage"), typeof(glClearTexSubImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearTexSubImage'.");
            }
        }
        public static void glClearTexSubImage(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @data) => glClearTexSubImagePtr(@texture, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @type, @data);

        internal delegate void glBindBuffersBaseFunc(GLenum @target, GLuint @first, GLsizei @count, const GLuint * @buffers);
        internal static glBindBuffersBaseFunc glBindBuffersBasePtr;
        internal static void loadBindBuffersBase()
        {
            try
            {
                glBindBuffersBasePtr = (glBindBuffersBaseFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBuffersBase"), typeof(glBindBuffersBaseFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBuffersBase'.");
            }
        }
        public static void glBindBuffersBase(GLenum @target, GLuint @first, GLsizei @count, const GLuint * @buffers) => glBindBuffersBasePtr(@target, @first, @count, @buffers);

        internal delegate void glBindBuffersRangeFunc(GLenum @target, GLuint @first, GLsizei @count, const GLuint * @buffers, const GLintptr * @offsets, const GLsizeiptr * @sizes);
        internal static glBindBuffersRangeFunc glBindBuffersRangePtr;
        internal static void loadBindBuffersRange()
        {
            try
            {
                glBindBuffersRangePtr = (glBindBuffersRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBuffersRange"), typeof(glBindBuffersRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBuffersRange'.");
            }
        }
        public static void glBindBuffersRange(GLenum @target, GLuint @first, GLsizei @count, const GLuint * @buffers, const GLintptr * @offsets, const GLsizeiptr * @sizes) => glBindBuffersRangePtr(@target, @first, @count, @buffers, @offsets, @sizes);

        internal delegate void glBindTexturesFunc(GLuint @first, GLsizei @count, const GLuint * @textures);
        internal static glBindTexturesFunc glBindTexturesPtr;
        internal static void loadBindTextures()
        {
            try
            {
                glBindTexturesPtr = (glBindTexturesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTextures"), typeof(glBindTexturesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTextures'.");
            }
        }
        public static void glBindTextures(GLuint @first, GLsizei @count, const GLuint * @textures) => glBindTexturesPtr(@first, @count, @textures);

        internal delegate void glBindSamplersFunc(GLuint @first, GLsizei @count, const GLuint * @samplers);
        internal static glBindSamplersFunc glBindSamplersPtr;
        internal static void loadBindSamplers()
        {
            try
            {
                glBindSamplersPtr = (glBindSamplersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindSamplers"), typeof(glBindSamplersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindSamplers'.");
            }
        }
        public static void glBindSamplers(GLuint @first, GLsizei @count, const GLuint * @samplers) => glBindSamplersPtr(@first, @count, @samplers);

        internal delegate void glBindImageTexturesFunc(GLuint @first, GLsizei @count, const GLuint * @textures);
        internal static glBindImageTexturesFunc glBindImageTexturesPtr;
        internal static void loadBindImageTextures()
        {
            try
            {
                glBindImageTexturesPtr = (glBindImageTexturesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindImageTextures"), typeof(glBindImageTexturesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindImageTextures'.");
            }
        }
        public static void glBindImageTextures(GLuint @first, GLsizei @count, const GLuint * @textures) => glBindImageTexturesPtr(@first, @count, @textures);

        internal delegate void glBindVertexBuffersFunc(GLuint @first, GLsizei @count, const GLuint * @buffers, const GLintptr * @offsets, const GLsizei * @strides);
        internal static glBindVertexBuffersFunc glBindVertexBuffersPtr;
        internal static void loadBindVertexBuffers()
        {
            try
            {
                glBindVertexBuffersPtr = (glBindVertexBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVertexBuffers"), typeof(glBindVertexBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVertexBuffers'.");
            }
        }
        public static void glBindVertexBuffers(GLuint @first, GLsizei @count, const GLuint * @buffers, const GLintptr * @offsets, const GLsizei * @strides) => glBindVertexBuffersPtr(@first, @count, @buffers, @offsets, @strides);
        #endregion
    }
}
