using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL42
    {
        #region Interop
        static GL42()
        {
            Console.WriteLine("Initalising GL42 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawArraysInstancedBaseInstance();
            loadDrawElementsInstancedBaseInstance();
            loadDrawElementsInstancedBaseVertexBaseInstance();
            loadGetInternalformativ();
            loadGetActiveAtomicCounterBufferiv();
            loadBindImageTexture();
            loadMemoryBarrier();
            loadTexStorage1D();
            loadTexStorage2D();
            loadTexStorage3D();
            loadDrawTransformFeedbackInstanced();
            loadDrawTransformFeedbackStreamInstanced();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COPY_READ_BUFFER_BINDING = 0x8F36;
        public static UInt32 GL_COPY_WRITE_BUFFER_BINDING = 0x8F37;
        public static UInt32 GL_TRANSFORM_FEEDBACK_ACTIVE = 0x8E24;
        public static UInt32 GL_TRANSFORM_FEEDBACK_PAUSED = 0x8E23;
        public static UInt32 GL_UNPACK_COMPRESSED_BLOCK_WIDTH = 0x9127;
        public static UInt32 GL_UNPACK_COMPRESSED_BLOCK_HEIGHT = 0x9128;
        public static UInt32 GL_UNPACK_COMPRESSED_BLOCK_DEPTH = 0x9129;
        public static UInt32 GL_UNPACK_COMPRESSED_BLOCK_SIZE = 0x912A;
        public static UInt32 GL_PACK_COMPRESSED_BLOCK_WIDTH = 0x912B;
        public static UInt32 GL_PACK_COMPRESSED_BLOCK_HEIGHT = 0x912C;
        public static UInt32 GL_PACK_COMPRESSED_BLOCK_DEPTH = 0x912D;
        public static UInt32 GL_PACK_COMPRESSED_BLOCK_SIZE = 0x912E;
        public static UInt32 GL_NUM_SAMPLE_COUNTS = 0x9380;
        public static UInt32 GL_MIN_MAP_BUFFER_ALIGNMENT = 0x90BC;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER = 0x92C0;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_BINDING = 0x92C1;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_START = 0x92C2;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_SIZE = 0x92C3;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_DATA_SIZE = 0x92C4;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_ACTIVE_ATOMIC_COUNTERS = 0x92C5;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_ACTIVE_ATOMIC_COUNTER_INDICES = 0x92C6;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_VERTEX_SHADER = 0x92C7;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_TESS_CONTROL_SHADER = 0x92C8;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_TESS_EVALUATION_SHADER = 0x92C9;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_GEOMETRY_SHADER = 0x92CA;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_FRAGMENT_SHADER = 0x92CB;
        public static UInt32 GL_MAX_VERTEX_ATOMIC_COUNTER_BUFFERS = 0x92CC;
        public static UInt32 GL_MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS = 0x92CD;
        public static UInt32 GL_MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS = 0x92CE;
        public static UInt32 GL_MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS = 0x92CF;
        public static UInt32 GL_MAX_FRAGMENT_ATOMIC_COUNTER_BUFFERS = 0x92D0;
        public static UInt32 GL_MAX_COMBINED_ATOMIC_COUNTER_BUFFERS = 0x92D1;
        public static UInt32 GL_MAX_VERTEX_ATOMIC_COUNTERS = 0x92D2;
        public static UInt32 GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS = 0x92D3;
        public static UInt32 GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS = 0x92D4;
        public static UInt32 GL_MAX_GEOMETRY_ATOMIC_COUNTERS = 0x92D5;
        public static UInt32 GL_MAX_FRAGMENT_ATOMIC_COUNTERS = 0x92D6;
        public static UInt32 GL_MAX_COMBINED_ATOMIC_COUNTERS = 0x92D7;
        public static UInt32 GL_MAX_ATOMIC_COUNTER_BUFFER_SIZE = 0x92D8;
        public static UInt32 GL_MAX_ATOMIC_COUNTER_BUFFER_BINDINGS = 0x92DC;
        public static UInt32 GL_ACTIVE_ATOMIC_COUNTER_BUFFERS = 0x92D9;
        public static UInt32 GL_UNIFORM_ATOMIC_COUNTER_BUFFER_INDEX = 0x92DA;
        public static UInt32 GL_UNSIGNED_INT_ATOMIC_COUNTER = 0x92DB;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT = 0x00000001;
        public static UInt32 GL_ELEMENT_ARRAY_BARRIER_BIT = 0x00000002;
        public static UInt32 GL_UNIFORM_BARRIER_BIT = 0x00000004;
        public static UInt32 GL_TEXTURE_FETCH_BARRIER_BIT = 0x00000008;
        public static UInt32 GL_SHADER_IMAGE_ACCESS_BARRIER_BIT = 0x00000020;
        public static UInt32 GL_COMMAND_BARRIER_BIT = 0x00000040;
        public static UInt32 GL_PIXEL_BUFFER_BARRIER_BIT = 0x00000080;
        public static UInt32 GL_TEXTURE_UPDATE_BARRIER_BIT = 0x00000100;
        public static UInt32 GL_BUFFER_UPDATE_BARRIER_BIT = 0x00000200;
        public static UInt32 GL_FRAMEBUFFER_BARRIER_BIT = 0x00000400;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BARRIER_BIT = 0x00000800;
        public static UInt32 GL_ATOMIC_COUNTER_BARRIER_BIT = 0x00001000;
        public static UInt32 GL_ALL_BARRIER_BITS = 0xFFFFFFFF;
        public static UInt32 GL_MAX_IMAGE_UNITS = 0x8F38;
        public static UInt32 GL_MAX_COMBINED_IMAGE_UNITS_AND_FRAGMENT_OUTPUTS = 0x8F39;
        public static UInt32 GL_IMAGE_BINDING_NAME = 0x8F3A;
        public static UInt32 GL_IMAGE_BINDING_LEVEL = 0x8F3B;
        public static UInt32 GL_IMAGE_BINDING_LAYERED = 0x8F3C;
        public static UInt32 GL_IMAGE_BINDING_LAYER = 0x8F3D;
        public static UInt32 GL_IMAGE_BINDING_ACCESS = 0x8F3E;
        public static UInt32 GL_IMAGE_1D = 0x904C;
        public static UInt32 GL_IMAGE_2D = 0x904D;
        public static UInt32 GL_IMAGE_3D = 0x904E;
        public static UInt32 GL_IMAGE_2D_RECT = 0x904F;
        public static UInt32 GL_IMAGE_CUBE = 0x9050;
        public static UInt32 GL_IMAGE_BUFFER = 0x9051;
        public static UInt32 GL_IMAGE_1D_ARRAY = 0x9052;
        public static UInt32 GL_IMAGE_2D_ARRAY = 0x9053;
        public static UInt32 GL_IMAGE_CUBE_MAP_ARRAY = 0x9054;
        public static UInt32 GL_IMAGE_2D_MULTISAMPLE = 0x9055;
        public static UInt32 GL_IMAGE_2D_MULTISAMPLE_ARRAY = 0x9056;
        public static UInt32 GL_INT_IMAGE_1D = 0x9057;
        public static UInt32 GL_INT_IMAGE_2D = 0x9058;
        public static UInt32 GL_INT_IMAGE_3D = 0x9059;
        public static UInt32 GL_INT_IMAGE_2D_RECT = 0x905A;
        public static UInt32 GL_INT_IMAGE_CUBE = 0x905B;
        public static UInt32 GL_INT_IMAGE_BUFFER = 0x905C;
        public static UInt32 GL_INT_IMAGE_1D_ARRAY = 0x905D;
        public static UInt32 GL_INT_IMAGE_2D_ARRAY = 0x905E;
        public static UInt32 GL_INT_IMAGE_CUBE_MAP_ARRAY = 0x905F;
        public static UInt32 GL_INT_IMAGE_2D_MULTISAMPLE = 0x9060;
        public static UInt32 GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY = 0x9061;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_1D = 0x9062;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D = 0x9063;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_3D = 0x9064;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_RECT = 0x9065;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_CUBE = 0x9066;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_BUFFER = 0x9067;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_1D_ARRAY = 0x9068;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_ARRAY = 0x9069;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_CUBE_MAP_ARRAY = 0x906A;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE = 0x906B;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY = 0x906C;
        public static UInt32 GL_MAX_IMAGE_SAMPLES = 0x906D;
        public static UInt32 GL_IMAGE_BINDING_FORMAT = 0x906E;
        public static UInt32 GL_IMAGE_FORMAT_COMPATIBILITY_TYPE = 0x90C7;
        public static UInt32 GL_IMAGE_FORMAT_COMPATIBILITY_BY_SIZE = 0x90C8;
        public static UInt32 GL_IMAGE_FORMAT_COMPATIBILITY_BY_CLASS = 0x90C9;
        public static UInt32 GL_MAX_VERTEX_IMAGE_UNIFORMS = 0x90CA;
        public static UInt32 GL_MAX_TESS_CONTROL_IMAGE_UNIFORMS = 0x90CB;
        public static UInt32 GL_MAX_TESS_EVALUATION_IMAGE_UNIFORMS = 0x90CC;
        public static UInt32 GL_MAX_GEOMETRY_IMAGE_UNIFORMS = 0x90CD;
        public static UInt32 GL_MAX_FRAGMENT_IMAGE_UNIFORMS = 0x90CE;
        public static UInt32 GL_MAX_COMBINED_IMAGE_UNIFORMS = 0x90CF;
        public static UInt32 GL_COMPRESSED_RGBA_BPTC_UNORM = 0x8E8C;
        public static UInt32 GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM = 0x8E8D;
        public static UInt32 GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT = 0x8E8E;
        public static UInt32 GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT = 0x8E8F;
        public static UInt32 GL_TEXTURE_IMMUTABLE_FORMAT = 0x912F;
        #endregion

        #region Commands
        internal delegate void glDrawArraysInstancedBaseInstanceFunc(GLenum @mode, GLint @first, GLsizei @count, GLsizei @instancecount, GLuint @baseinstance);
        internal static glDrawArraysInstancedBaseInstanceFunc glDrawArraysInstancedBaseInstancePtr;
        internal static void loadDrawArraysInstancedBaseInstance()
        {
            try
            {
                glDrawArraysInstancedBaseInstancePtr = (glDrawArraysInstancedBaseInstanceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysInstancedBaseInstance"), typeof(glDrawArraysInstancedBaseInstanceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysInstancedBaseInstance'.");
            }
        }
        public static void glDrawArraysInstancedBaseInstance(GLenum @mode, GLint @first, GLsizei @count, GLsizei @instancecount, GLuint @baseinstance) => glDrawArraysInstancedBaseInstancePtr(@mode, @first, @count, @instancecount, @baseinstance);

        internal delegate void glDrawElementsInstancedBaseInstanceFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLuint @baseinstance);
        internal static glDrawElementsInstancedBaseInstanceFunc glDrawElementsInstancedBaseInstancePtr;
        internal static void loadDrawElementsInstancedBaseInstance()
        {
            try
            {
                glDrawElementsInstancedBaseInstancePtr = (glDrawElementsInstancedBaseInstanceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedBaseInstance"), typeof(glDrawElementsInstancedBaseInstanceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedBaseInstance'.");
            }
        }
        public static void glDrawElementsInstancedBaseInstance(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLuint @baseinstance) => glDrawElementsInstancedBaseInstancePtr(@mode, @count, @type, @indices, @instancecount, @baseinstance);

        internal delegate void glDrawElementsInstancedBaseVertexBaseInstanceFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex, GLuint @baseinstance);
        internal static glDrawElementsInstancedBaseVertexBaseInstanceFunc glDrawElementsInstancedBaseVertexBaseInstancePtr;
        internal static void loadDrawElementsInstancedBaseVertexBaseInstance()
        {
            try
            {
                glDrawElementsInstancedBaseVertexBaseInstancePtr = (glDrawElementsInstancedBaseVertexBaseInstanceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedBaseVertexBaseInstance"), typeof(glDrawElementsInstancedBaseVertexBaseInstanceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedBaseVertexBaseInstance'.");
            }
        }
        public static void glDrawElementsInstancedBaseVertexBaseInstance(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex, GLuint @baseinstance) => glDrawElementsInstancedBaseVertexBaseInstancePtr(@mode, @count, @type, @indices, @instancecount, @basevertex, @baseinstance);

        internal delegate void glGetInternalformativFunc(GLenum @target, GLenum @internalformat, GLenum @pname, GLsizei @bufSize, GLint * @params);
        internal static glGetInternalformativFunc glGetInternalformativPtr;
        internal static void loadGetInternalformativ()
        {
            try
            {
                glGetInternalformativPtr = (glGetInternalformativFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInternalformativ"), typeof(glGetInternalformativFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInternalformativ'.");
            }
        }
        public static void glGetInternalformativ(GLenum @target, GLenum @internalformat, GLenum @pname, GLsizei @bufSize, GLint * @params) => glGetInternalformativPtr(@target, @internalformat, @pname, @bufSize, @params);

        internal delegate void glGetActiveAtomicCounterBufferivFunc(GLuint @program, GLuint @bufferIndex, GLenum @pname, GLint * @params);
        internal static glGetActiveAtomicCounterBufferivFunc glGetActiveAtomicCounterBufferivPtr;
        internal static void loadGetActiveAtomicCounterBufferiv()
        {
            try
            {
                glGetActiveAtomicCounterBufferivPtr = (glGetActiveAtomicCounterBufferivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveAtomicCounterBufferiv"), typeof(glGetActiveAtomicCounterBufferivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveAtomicCounterBufferiv'.");
            }
        }
        public static void glGetActiveAtomicCounterBufferiv(GLuint @program, GLuint @bufferIndex, GLenum @pname, GLint * @params) => glGetActiveAtomicCounterBufferivPtr(@program, @bufferIndex, @pname, @params);

        internal delegate void glBindImageTextureFunc(GLuint @unit, GLuint @texture, GLint @level, GLboolean @layered, GLint @layer, GLenum @access, GLenum @format);
        internal static glBindImageTextureFunc glBindImageTexturePtr;
        internal static void loadBindImageTexture()
        {
            try
            {
                glBindImageTexturePtr = (glBindImageTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindImageTexture"), typeof(glBindImageTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindImageTexture'.");
            }
        }
        public static void glBindImageTexture(GLuint @unit, GLuint @texture, GLint @level, GLboolean @layered, GLint @layer, GLenum @access, GLenum @format) => glBindImageTexturePtr(@unit, @texture, @level, @layered, @layer, @access, @format);

        internal delegate void glMemoryBarrierFunc(GLbitfield @barriers);
        internal static glMemoryBarrierFunc glMemoryBarrierPtr;
        internal static void loadMemoryBarrier()
        {
            try
            {
                glMemoryBarrierPtr = (glMemoryBarrierFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMemoryBarrier"), typeof(glMemoryBarrierFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMemoryBarrier'.");
            }
        }
        public static void glMemoryBarrier(GLbitfield @barriers) => glMemoryBarrierPtr(@barriers);

        internal delegate void glTexStorage1DFunc(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width);
        internal static glTexStorage1DFunc glTexStorage1DPtr;
        internal static void loadTexStorage1D()
        {
            try
            {
                glTexStorage1DPtr = (glTexStorage1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage1D"), typeof(glTexStorage1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage1D'.");
            }
        }
        public static void glTexStorage1D(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width) => glTexStorage1DPtr(@target, @levels, @internalformat, @width);

        internal delegate void glTexStorage2DFunc(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glTexStorage2DFunc glTexStorage2DPtr;
        internal static void loadTexStorage2D()
        {
            try
            {
                glTexStorage2DPtr = (glTexStorage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage2D"), typeof(glTexStorage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage2D'.");
            }
        }
        public static void glTexStorage2D(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height) => glTexStorage2DPtr(@target, @levels, @internalformat, @width, @height);

        internal delegate void glTexStorage3DFunc(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth);
        internal static glTexStorage3DFunc glTexStorage3DPtr;
        internal static void loadTexStorage3D()
        {
            try
            {
                glTexStorage3DPtr = (glTexStorage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage3D"), typeof(glTexStorage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage3D'.");
            }
        }
        public static void glTexStorage3D(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth) => glTexStorage3DPtr(@target, @levels, @internalformat, @width, @height, @depth);

        internal delegate void glDrawTransformFeedbackInstancedFunc(GLenum @mode, GLuint @id, GLsizei @instancecount);
        internal static glDrawTransformFeedbackInstancedFunc glDrawTransformFeedbackInstancedPtr;
        internal static void loadDrawTransformFeedbackInstanced()
        {
            try
            {
                glDrawTransformFeedbackInstancedPtr = (glDrawTransformFeedbackInstancedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTransformFeedbackInstanced"), typeof(glDrawTransformFeedbackInstancedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTransformFeedbackInstanced'.");
            }
        }
        public static void glDrawTransformFeedbackInstanced(GLenum @mode, GLuint @id, GLsizei @instancecount) => glDrawTransformFeedbackInstancedPtr(@mode, @id, @instancecount);

        internal delegate void glDrawTransformFeedbackStreamInstancedFunc(GLenum @mode, GLuint @id, GLuint @stream, GLsizei @instancecount);
        internal static glDrawTransformFeedbackStreamInstancedFunc glDrawTransformFeedbackStreamInstancedPtr;
        internal static void loadDrawTransformFeedbackStreamInstanced()
        {
            try
            {
                glDrawTransformFeedbackStreamInstancedPtr = (glDrawTransformFeedbackStreamInstancedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTransformFeedbackStreamInstanced"), typeof(glDrawTransformFeedbackStreamInstancedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTransformFeedbackStreamInstanced'.");
            }
        }
        public static void glDrawTransformFeedbackStreamInstanced(GLenum @mode, GLuint @id, GLuint @stream, GLsizei @instancecount) => glDrawTransformFeedbackStreamInstancedPtr(@mode, @id, @stream, @instancecount);
        #endregion
    }
}
