using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL15
    {
        #region Interop
        static GL15()
        {
            Console.WriteLine("Initalising GL15 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGenQueries();
            loadDeleteQueries();
            loadIsQuery();
            loadBeginQuery();
            loadEndQuery();
            loadGetQueryiv();
            loadGetQueryObjectiv();
            loadGetQueryObjectuiv();
            loadBindBuffer();
            loadDeleteBuffers();
            loadGenBuffers();
            loadIsBuffer();
            loadBufferData();
            loadBufferSubData();
            loadGetBufferSubData();
            loadMapBuffer();
            loadUnmapBuffer();
            loadGetBufferParameteriv();
            loadGetBufferPointerv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BUFFER_SIZE = 0x8764;
        public static UInt32 GL_BUFFER_USAGE = 0x8765;
        public static UInt32 GL_QUERY_COUNTER_BITS = 0x8864;
        public static UInt32 GL_CURRENT_QUERY = 0x8865;
        public static UInt32 GL_QUERY_RESULT = 0x8866;
        public static UInt32 GL_QUERY_RESULT_AVAILABLE = 0x8867;
        public static UInt32 GL_ARRAY_BUFFER = 0x8892;
        public static UInt32 GL_ELEMENT_ARRAY_BUFFER = 0x8893;
        public static UInt32 GL_ARRAY_BUFFER_BINDING = 0x8894;
        public static UInt32 GL_ELEMENT_ARRAY_BUFFER_BINDING = 0x8895;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING = 0x889F;
        public static UInt32 GL_READ_ONLY = 0x88B8;
        public static UInt32 GL_WRITE_ONLY = 0x88B9;
        public static UInt32 GL_READ_WRITE = 0x88BA;
        public static UInt32 GL_BUFFER_ACCESS = 0x88BB;
        public static UInt32 GL_BUFFER_MAPPED = 0x88BC;
        public static UInt32 GL_BUFFER_MAP_POINTER = 0x88BD;
        public static UInt32 GL_STREAM_DRAW = 0x88E0;
        public static UInt32 GL_STREAM_READ = 0x88E1;
        public static UInt32 GL_STREAM_COPY = 0x88E2;
        public static UInt32 GL_STATIC_DRAW = 0x88E4;
        public static UInt32 GL_STATIC_READ = 0x88E5;
        public static UInt32 GL_STATIC_COPY = 0x88E6;
        public static UInt32 GL_DYNAMIC_DRAW = 0x88E8;
        public static UInt32 GL_DYNAMIC_READ = 0x88E9;
        public static UInt32 GL_DYNAMIC_COPY = 0x88EA;
        public static UInt32 GL_SAMPLES_PASSED = 0x8914;
        public static UInt32 GL_SRC1_ALPHA = 0x8589;
        public static UInt32 GL_VERTEX_ARRAY_BUFFER_BINDING = 0x8896;
        public static UInt32 GL_NORMAL_ARRAY_BUFFER_BINDING = 0x8897;
        public static UInt32 GL_COLOR_ARRAY_BUFFER_BINDING = 0x8898;
        public static UInt32 GL_INDEX_ARRAY_BUFFER_BINDING = 0x8899;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_BUFFER_BINDING = 0x889A;
        public static UInt32 GL_EDGE_FLAG_ARRAY_BUFFER_BINDING = 0x889B;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_BUFFER_BINDING = 0x889C;
        public static UInt32 GL_FOG_COORDINATE_ARRAY_BUFFER_BINDING = 0x889D;
        public static UInt32 GL_WEIGHT_ARRAY_BUFFER_BINDING = 0x889E;
        public static UInt32 GL_FOG_COORD_SRC = 0x8450;
        public static UInt32 GL_FOG_COORD = 0x8451;
        public static UInt32 GL_CURRENT_FOG_COORD = 0x8453;
        public static UInt32 GL_FOG_COORD_ARRAY_TYPE = 0x8454;
        public static UInt32 GL_FOG_COORD_ARRAY_STRIDE = 0x8455;
        public static UInt32 GL_FOG_COORD_ARRAY_POINTER = 0x8456;
        public static UInt32 GL_FOG_COORD_ARRAY = 0x8457;
        public static UInt32 GL_FOG_COORD_ARRAY_BUFFER_BINDING = 0x889D;
        public static UInt32 GL_SRC0_RGB = 0x8580;
        public static UInt32 GL_SRC1_RGB = 0x8581;
        public static UInt32 GL_SRC2_RGB = 0x8582;
        public static UInt32 GL_SRC0_ALPHA = 0x8588;
        public static UInt32 GL_SRC2_ALPHA = 0x858A;
        #endregion

        #region Commands
        internal delegate void glGenQueriesFunc(GLsizei @n, GLuint * @ids);
        internal static glGenQueriesFunc glGenQueriesPtr;
        internal static void loadGenQueries()
        {
            try
            {
                glGenQueriesPtr = (glGenQueriesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenQueries"), typeof(glGenQueriesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenQueries'.");
            }
        }
        public static void glGenQueries(GLsizei @n, GLuint * @ids) => glGenQueriesPtr(@n, @ids);

        internal delegate void glDeleteQueriesFunc(GLsizei @n, const GLuint * @ids);
        internal static glDeleteQueriesFunc glDeleteQueriesPtr;
        internal static void loadDeleteQueries()
        {
            try
            {
                glDeleteQueriesPtr = (glDeleteQueriesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteQueries"), typeof(glDeleteQueriesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteQueries'.");
            }
        }
        public static void glDeleteQueries(GLsizei @n, const GLuint * @ids) => glDeleteQueriesPtr(@n, @ids);

        internal delegate GLboolean glIsQueryFunc(GLuint @id);
        internal static glIsQueryFunc glIsQueryPtr;
        internal static void loadIsQuery()
        {
            try
            {
                glIsQueryPtr = (glIsQueryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsQuery"), typeof(glIsQueryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsQuery'.");
            }
        }
        public static GLboolean glIsQuery(GLuint @id) => glIsQueryPtr(@id);

        internal delegate void glBeginQueryFunc(GLenum @target, GLuint @id);
        internal static glBeginQueryFunc glBeginQueryPtr;
        internal static void loadBeginQuery()
        {
            try
            {
                glBeginQueryPtr = (glBeginQueryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginQuery"), typeof(glBeginQueryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginQuery'.");
            }
        }
        public static void glBeginQuery(GLenum @target, GLuint @id) => glBeginQueryPtr(@target, @id);

        internal delegate void glEndQueryFunc(GLenum @target);
        internal static glEndQueryFunc glEndQueryPtr;
        internal static void loadEndQuery()
        {
            try
            {
                glEndQueryPtr = (glEndQueryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndQuery"), typeof(glEndQueryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndQuery'.");
            }
        }
        public static void glEndQuery(GLenum @target) => glEndQueryPtr(@target);

        internal delegate void glGetQueryivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetQueryivFunc glGetQueryivPtr;
        internal static void loadGetQueryiv()
        {
            try
            {
                glGetQueryivPtr = (glGetQueryivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryiv"), typeof(glGetQueryivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryiv'.");
            }
        }
        public static void glGetQueryiv(GLenum @target, GLenum @pname, GLint * @params) => glGetQueryivPtr(@target, @pname, @params);

        internal delegate void glGetQueryObjectivFunc(GLuint @id, GLenum @pname, GLint * @params);
        internal static glGetQueryObjectivFunc glGetQueryObjectivPtr;
        internal static void loadGetQueryObjectiv()
        {
            try
            {
                glGetQueryObjectivPtr = (glGetQueryObjectivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjectiv"), typeof(glGetQueryObjectivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjectiv'.");
            }
        }
        public static void glGetQueryObjectiv(GLuint @id, GLenum @pname, GLint * @params) => glGetQueryObjectivPtr(@id, @pname, @params);

        internal delegate void glGetQueryObjectuivFunc(GLuint @id, GLenum @pname, GLuint * @params);
        internal static glGetQueryObjectuivFunc glGetQueryObjectuivPtr;
        internal static void loadGetQueryObjectuiv()
        {
            try
            {
                glGetQueryObjectuivPtr = (glGetQueryObjectuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjectuiv"), typeof(glGetQueryObjectuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjectuiv'.");
            }
        }
        public static void glGetQueryObjectuiv(GLuint @id, GLenum @pname, GLuint * @params) => glGetQueryObjectuivPtr(@id, @pname, @params);

        internal delegate void glBindBufferFunc(GLenum @target, GLuint @buffer);
        internal static glBindBufferFunc glBindBufferPtr;
        internal static void loadBindBuffer()
        {
            try
            {
                glBindBufferPtr = (glBindBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBuffer"), typeof(glBindBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBuffer'.");
            }
        }
        public static void glBindBuffer(GLenum @target, GLuint @buffer) => glBindBufferPtr(@target, @buffer);

        internal delegate void glDeleteBuffersFunc(GLsizei @n, const GLuint * @buffers);
        internal static glDeleteBuffersFunc glDeleteBuffersPtr;
        internal static void loadDeleteBuffers()
        {
            try
            {
                glDeleteBuffersPtr = (glDeleteBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteBuffers"), typeof(glDeleteBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteBuffers'.");
            }
        }
        public static void glDeleteBuffers(GLsizei @n, const GLuint * @buffers) => glDeleteBuffersPtr(@n, @buffers);

        internal delegate void glGenBuffersFunc(GLsizei @n, GLuint * @buffers);
        internal static glGenBuffersFunc glGenBuffersPtr;
        internal static void loadGenBuffers()
        {
            try
            {
                glGenBuffersPtr = (glGenBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenBuffers"), typeof(glGenBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenBuffers'.");
            }
        }
        public static void glGenBuffers(GLsizei @n, GLuint * @buffers) => glGenBuffersPtr(@n, @buffers);

        internal delegate GLboolean glIsBufferFunc(GLuint @buffer);
        internal static glIsBufferFunc glIsBufferPtr;
        internal static void loadIsBuffer()
        {
            try
            {
                glIsBufferPtr = (glIsBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsBuffer"), typeof(glIsBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsBuffer'.");
            }
        }
        public static GLboolean glIsBuffer(GLuint @buffer) => glIsBufferPtr(@buffer);

        internal delegate void glBufferDataFunc(GLenum @target, GLsizeiptr @size, const void * @data, GLenum @usage);
        internal static glBufferDataFunc glBufferDataPtr;
        internal static void loadBufferData()
        {
            try
            {
                glBufferDataPtr = (glBufferDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferData"), typeof(glBufferDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferData'.");
            }
        }
        public static void glBufferData(GLenum @target, GLsizeiptr @size, const void * @data, GLenum @usage) => glBufferDataPtr(@target, @size, @data, @usage);

        internal delegate void glBufferSubDataFunc(GLenum @target, GLintptr @offset, GLsizeiptr @size, const void * @data);
        internal static glBufferSubDataFunc glBufferSubDataPtr;
        internal static void loadBufferSubData()
        {
            try
            {
                glBufferSubDataPtr = (glBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferSubData"), typeof(glBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferSubData'.");
            }
        }
        public static void glBufferSubData(GLenum @target, GLintptr @offset, GLsizeiptr @size, const void * @data) => glBufferSubDataPtr(@target, @offset, @size, @data);

        internal delegate void glGetBufferSubDataFunc(GLenum @target, GLintptr @offset, GLsizeiptr @size, void * @data);
        internal static glGetBufferSubDataFunc glGetBufferSubDataPtr;
        internal static void loadGetBufferSubData()
        {
            try
            {
                glGetBufferSubDataPtr = (glGetBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferSubData"), typeof(glGetBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferSubData'.");
            }
        }
        public static void glGetBufferSubData(GLenum @target, GLintptr @offset, GLsizeiptr @size, void * @data) => glGetBufferSubDataPtr(@target, @offset, @size, @data);

        internal delegate void * glMapBufferFunc(GLenum @target, GLenum @access);
        internal static glMapBufferFunc glMapBufferPtr;
        internal static void loadMapBuffer()
        {
            try
            {
                glMapBufferPtr = (glMapBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapBuffer"), typeof(glMapBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapBuffer'.");
            }
        }
        public static void * glMapBuffer(GLenum @target, GLenum @access) => glMapBufferPtr(@target, @access);

        internal delegate GLboolean glUnmapBufferFunc(GLenum @target);
        internal static glUnmapBufferFunc glUnmapBufferPtr;
        internal static void loadUnmapBuffer()
        {
            try
            {
                glUnmapBufferPtr = (glUnmapBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUnmapBuffer"), typeof(glUnmapBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUnmapBuffer'.");
            }
        }
        public static GLboolean glUnmapBuffer(GLenum @target) => glUnmapBufferPtr(@target);

        internal delegate void glGetBufferParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetBufferParameterivFunc glGetBufferParameterivPtr;
        internal static void loadGetBufferParameteriv()
        {
            try
            {
                glGetBufferParameterivPtr = (glGetBufferParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferParameteriv"), typeof(glGetBufferParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferParameteriv'.");
            }
        }
        public static void glGetBufferParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetBufferParameterivPtr(@target, @pname, @params);

        internal delegate void glGetBufferPointervFunc(GLenum @target, GLenum @pname, void ** @params);
        internal static glGetBufferPointervFunc glGetBufferPointervPtr;
        internal static void loadGetBufferPointerv()
        {
            try
            {
                glGetBufferPointervPtr = (glGetBufferPointervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferPointerv"), typeof(glGetBufferPointervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferPointerv'.");
            }
        }
        public static void glGetBufferPointerv(GLenum @target, GLenum @pname, void ** @params) => glGetBufferPointervPtr(@target, @pname, @params);
        #endregion
    }
}
