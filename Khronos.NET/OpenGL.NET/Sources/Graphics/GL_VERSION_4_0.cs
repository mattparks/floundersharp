using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL40
    {
        #region Interop
        static GL40()
        {
            Console.WriteLine("Initalising GL40 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMinSampleShading();
            loadBlendEquationi();
            loadBlendEquationSeparatei();
            loadBlendFunci();
            loadBlendFuncSeparatei();
            loadDrawArraysIndirect();
            loadDrawElementsIndirect();
            loadUniform1d();
            loadUniform2d();
            loadUniform3d();
            loadUniform4d();
            loadUniform1dv();
            loadUniform2dv();
            loadUniform3dv();
            loadUniform4dv();
            loadUniformMatrix2dv();
            loadUniformMatrix3dv();
            loadUniformMatrix4dv();
            loadUniformMatrix2x3dv();
            loadUniformMatrix2x4dv();
            loadUniformMatrix3x2dv();
            loadUniformMatrix3x4dv();
            loadUniformMatrix4x2dv();
            loadUniformMatrix4x3dv();
            loadGetUniformdv();
            loadGetSubroutineUniformLocation();
            loadGetSubroutineIndex();
            loadGetActiveSubroutineUniformiv();
            loadGetActiveSubroutineUniformName();
            loadGetActiveSubroutineName();
            loadUniformSubroutinesuiv();
            loadGetUniformSubroutineuiv();
            loadGetProgramStageiv();
            loadPatchParameteri();
            loadPatchParameterfv();
            loadBindTransformFeedback();
            loadDeleteTransformFeedbacks();
            loadGenTransformFeedbacks();
            loadIsTransformFeedback();
            loadPauseTransformFeedback();
            loadResumeTransformFeedback();
            loadDrawTransformFeedback();
            loadDrawTransformFeedbackStream();
            loadBeginQueryIndexed();
            loadEndQueryIndexed();
            loadGetQueryIndexediv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SAMPLE_SHADING = 0x8C36;
        public static UInt32 GL_MIN_SAMPLE_SHADING_VALUE = 0x8C37;
        public static UInt32 GL_MIN_PROGRAM_TEXTURE_GATHER_OFFSET = 0x8E5E;
        public static UInt32 GL_MAX_PROGRAM_TEXTURE_GATHER_OFFSET = 0x8E5F;
        public static UInt32 GL_TEXTURE_CUBE_MAP_ARRAY = 0x9009;
        public static UInt32 GL_TEXTURE_BINDING_CUBE_MAP_ARRAY = 0x900A;
        public static UInt32 GL_PROXY_TEXTURE_CUBE_MAP_ARRAY = 0x900B;
        public static UInt32 GL_SAMPLER_CUBE_MAP_ARRAY = 0x900C;
        public static UInt32 GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW = 0x900D;
        public static UInt32 GL_INT_SAMPLER_CUBE_MAP_ARRAY = 0x900E;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY = 0x900F;
        public static UInt32 GL_DRAW_INDIRECT_BUFFER = 0x8F3F;
        public static UInt32 GL_DRAW_INDIRECT_BUFFER_BINDING = 0x8F43;
        public static UInt32 GL_GEOMETRY_SHADER_INVOCATIONS = 0x887F;
        public static UInt32 GL_MAX_GEOMETRY_SHADER_INVOCATIONS = 0x8E5A;
        public static UInt32 GL_MIN_FRAGMENT_INTERPOLATION_OFFSET = 0x8E5B;
        public static UInt32 GL_MAX_FRAGMENT_INTERPOLATION_OFFSET = 0x8E5C;
        public static UInt32 GL_FRAGMENT_INTERPOLATION_OFFSET_BITS = 0x8E5D;
        public static UInt32 GL_MAX_VERTEX_STREAMS = 0x8E71;
        public static UInt32 GL_DOUBLE_VEC2 = 0x8FFC;
        public static UInt32 GL_DOUBLE_VEC3 = 0x8FFD;
        public static UInt32 GL_DOUBLE_VEC4 = 0x8FFE;
        public static UInt32 GL_DOUBLE_MAT2 = 0x8F46;
        public static UInt32 GL_DOUBLE_MAT3 = 0x8F47;
        public static UInt32 GL_DOUBLE_MAT4 = 0x8F48;
        public static UInt32 GL_DOUBLE_MAT2x3 = 0x8F49;
        public static UInt32 GL_DOUBLE_MAT2x4 = 0x8F4A;
        public static UInt32 GL_DOUBLE_MAT3x2 = 0x8F4B;
        public static UInt32 GL_DOUBLE_MAT3x4 = 0x8F4C;
        public static UInt32 GL_DOUBLE_MAT4x2 = 0x8F4D;
        public static UInt32 GL_DOUBLE_MAT4x3 = 0x8F4E;
        public static UInt32 GL_ACTIVE_SUBROUTINES = 0x8DE5;
        public static UInt32 GL_ACTIVE_SUBROUTINE_UNIFORMS = 0x8DE6;
        public static UInt32 GL_ACTIVE_SUBROUTINE_UNIFORM_LOCATIONS = 0x8E47;
        public static UInt32 GL_ACTIVE_SUBROUTINE_MAX_LENGTH = 0x8E48;
        public static UInt32 GL_ACTIVE_SUBROUTINE_UNIFORM_MAX_LENGTH = 0x8E49;
        public static UInt32 GL_MAX_SUBROUTINES = 0x8DE7;
        public static UInt32 GL_MAX_SUBROUTINE_UNIFORM_LOCATIONS = 0x8DE8;
        public static UInt32 GL_NUM_COMPATIBLE_SUBROUTINES = 0x8E4A;
        public static UInt32 GL_COMPATIBLE_SUBROUTINES = 0x8E4B;
        public static UInt32 GL_PATCHES = 0x000E;
        public static UInt32 GL_PATCH_VERTICES = 0x8E72;
        public static UInt32 GL_PATCH_DEFAULT_INNER_LEVEL = 0x8E73;
        public static UInt32 GL_PATCH_DEFAULT_OUTER_LEVEL = 0x8E74;
        public static UInt32 GL_TESS_CONTROL_OUTPUT_VERTICES = 0x8E75;
        public static UInt32 GL_TESS_GEN_MODE = 0x8E76;
        public static UInt32 GL_TESS_GEN_SPACING = 0x8E77;
        public static UInt32 GL_TESS_GEN_VERTEX_ORDER = 0x8E78;
        public static UInt32 GL_TESS_GEN_POINT_MODE = 0x8E79;
        public static UInt32 GL_ISOLINES = 0x8E7A;
        public static UInt32 GL_QUADS = 0x0007;
        public static UInt32 GL_FRACTIONAL_ODD = 0x8E7B;
        public static UInt32 GL_FRACTIONAL_EVEN = 0x8E7C;
        public static UInt32 GL_MAX_PATCH_VERTICES = 0x8E7D;
        public static UInt32 GL_MAX_TESS_GEN_LEVEL = 0x8E7E;
        public static UInt32 GL_MAX_TESS_CONTROL_UNIFORM_COMPONENTS = 0x8E7F;
        public static UInt32 GL_MAX_TESS_EVALUATION_UNIFORM_COMPONENTS = 0x8E80;
        public static UInt32 GL_MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS = 0x8E81;
        public static UInt32 GL_MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS = 0x8E82;
        public static UInt32 GL_MAX_TESS_CONTROL_OUTPUT_COMPONENTS = 0x8E83;
        public static UInt32 GL_MAX_TESS_PATCH_COMPONENTS = 0x8E84;
        public static UInt32 GL_MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS = 0x8E85;
        public static UInt32 GL_MAX_TESS_EVALUATION_OUTPUT_COMPONENTS = 0x8E86;
        public static UInt32 GL_MAX_TESS_CONTROL_UNIFORM_BLOCKS = 0x8E89;
        public static UInt32 GL_MAX_TESS_EVALUATION_UNIFORM_BLOCKS = 0x8E8A;
        public static UInt32 GL_MAX_TESS_CONTROL_INPUT_COMPONENTS = 0x886C;
        public static UInt32 GL_MAX_TESS_EVALUATION_INPUT_COMPONENTS = 0x886D;
        public static UInt32 GL_MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS = 0x8E1E;
        public static UInt32 GL_MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS = 0x8E1F;
        public static UInt32 GL_UNIFORM_BLOCK_REFERENCED_BY_TESS_CONTROL_SHADER = 0x84F0;
        public static UInt32 GL_UNIFORM_BLOCK_REFERENCED_BY_TESS_EVALUATION_SHADER = 0x84F1;
        public static UInt32 GL_TESS_EVALUATION_SHADER = 0x8E87;
        public static UInt32 GL_TESS_CONTROL_SHADER = 0x8E88;
        public static UInt32 GL_TRANSFORM_FEEDBACK = 0x8E22;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_PAUSED = 0x8E23;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_ACTIVE = 0x8E24;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BINDING = 0x8E25;
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_BUFFERS = 0x8E70;
        #endregion

        #region Commands
        internal delegate void glMinSampleShadingFunc(GLfloat @value);
        internal static glMinSampleShadingFunc glMinSampleShadingPtr;
        internal static void loadMinSampleShading()
        {
            try
            {
                glMinSampleShadingPtr = (glMinSampleShadingFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMinSampleShading"), typeof(glMinSampleShadingFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMinSampleShading'.");
            }
        }
        public static void glMinSampleShading(GLfloat @value) => glMinSampleShadingPtr(@value);

        internal delegate void glBlendEquationiFunc(GLuint @buf, GLenum @mode);
        internal static glBlendEquationiFunc glBlendEquationiPtr;
        internal static void loadBlendEquationi()
        {
            try
            {
                glBlendEquationiPtr = (glBlendEquationiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationi"), typeof(glBlendEquationiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationi'.");
            }
        }
        public static void glBlendEquationi(GLuint @buf, GLenum @mode) => glBlendEquationiPtr(@buf, @mode);

        internal delegate void glBlendEquationSeparateiFunc(GLuint @buf, GLenum @modeRGB, GLenum @modeAlpha);
        internal static glBlendEquationSeparateiFunc glBlendEquationSeparateiPtr;
        internal static void loadBlendEquationSeparatei()
        {
            try
            {
                glBlendEquationSeparateiPtr = (glBlendEquationSeparateiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationSeparatei"), typeof(glBlendEquationSeparateiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationSeparatei'.");
            }
        }
        public static void glBlendEquationSeparatei(GLuint @buf, GLenum @modeRGB, GLenum @modeAlpha) => glBlendEquationSeparateiPtr(@buf, @modeRGB, @modeAlpha);

        internal delegate void glBlendFunciFunc(GLuint @buf, GLenum @src, GLenum @dst);
        internal static glBlendFunciFunc glBlendFunciPtr;
        internal static void loadBlendFunci()
        {
            try
            {
                glBlendFunciPtr = (glBlendFunciFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFunci"), typeof(glBlendFunciFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFunci'.");
            }
        }
        public static void glBlendFunci(GLuint @buf, GLenum @src, GLenum @dst) => glBlendFunciPtr(@buf, @src, @dst);

        internal delegate void glBlendFuncSeparateiFunc(GLuint @buf, GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha);
        internal static glBlendFuncSeparateiFunc glBlendFuncSeparateiPtr;
        internal static void loadBlendFuncSeparatei()
        {
            try
            {
                glBlendFuncSeparateiPtr = (glBlendFuncSeparateiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFuncSeparatei"), typeof(glBlendFuncSeparateiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFuncSeparatei'.");
            }
        }
        public static void glBlendFuncSeparatei(GLuint @buf, GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha) => glBlendFuncSeparateiPtr(@buf, @srcRGB, @dstRGB, @srcAlpha, @dstAlpha);

        internal delegate void glDrawArraysIndirectFunc(GLenum @mode, const void * @indirect);
        internal static glDrawArraysIndirectFunc glDrawArraysIndirectPtr;
        internal static void loadDrawArraysIndirect()
        {
            try
            {
                glDrawArraysIndirectPtr = (glDrawArraysIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysIndirect"), typeof(glDrawArraysIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysIndirect'.");
            }
        }
        public static void glDrawArraysIndirect(GLenum @mode, const void * @indirect) => glDrawArraysIndirectPtr(@mode, @indirect);

        internal delegate void glDrawElementsIndirectFunc(GLenum @mode, GLenum @type, const void * @indirect);
        internal static glDrawElementsIndirectFunc glDrawElementsIndirectPtr;
        internal static void loadDrawElementsIndirect()
        {
            try
            {
                glDrawElementsIndirectPtr = (glDrawElementsIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsIndirect"), typeof(glDrawElementsIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsIndirect'.");
            }
        }
        public static void glDrawElementsIndirect(GLenum @mode, GLenum @type, const void * @indirect) => glDrawElementsIndirectPtr(@mode, @type, @indirect);

        internal delegate void glUniform1dFunc(GLint @location, GLdouble @x);
        internal static glUniform1dFunc glUniform1dPtr;
        internal static void loadUniform1d()
        {
            try
            {
                glUniform1dPtr = (glUniform1dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1d"), typeof(glUniform1dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1d'.");
            }
        }
        public static void glUniform1d(GLint @location, GLdouble @x) => glUniform1dPtr(@location, @x);

        internal delegate void glUniform2dFunc(GLint @location, GLdouble @x, GLdouble @y);
        internal static glUniform2dFunc glUniform2dPtr;
        internal static void loadUniform2d()
        {
            try
            {
                glUniform2dPtr = (glUniform2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2d"), typeof(glUniform2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2d'.");
            }
        }
        public static void glUniform2d(GLint @location, GLdouble @x, GLdouble @y) => glUniform2dPtr(@location, @x, @y);

        internal delegate void glUniform3dFunc(GLint @location, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glUniform3dFunc glUniform3dPtr;
        internal static void loadUniform3d()
        {
            try
            {
                glUniform3dPtr = (glUniform3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3d"), typeof(glUniform3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3d'.");
            }
        }
        public static void glUniform3d(GLint @location, GLdouble @x, GLdouble @y, GLdouble @z) => glUniform3dPtr(@location, @x, @y, @z);

        internal delegate void glUniform4dFunc(GLint @location, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glUniform4dFunc glUniform4dPtr;
        internal static void loadUniform4d()
        {
            try
            {
                glUniform4dPtr = (glUniform4dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4d"), typeof(glUniform4dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4d'.");
            }
        }
        public static void glUniform4d(GLint @location, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glUniform4dPtr(@location, @x, @y, @z, @w);

        internal delegate void glUniform1dvFunc(GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glUniform1dvFunc glUniform1dvPtr;
        internal static void loadUniform1dv()
        {
            try
            {
                glUniform1dvPtr = (glUniform1dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1dv"), typeof(glUniform1dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1dv'.");
            }
        }
        public static void glUniform1dv(GLint @location, GLsizei @count, const GLdouble * @value) => glUniform1dvPtr(@location, @count, @value);

        internal delegate void glUniform2dvFunc(GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glUniform2dvFunc glUniform2dvPtr;
        internal static void loadUniform2dv()
        {
            try
            {
                glUniform2dvPtr = (glUniform2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2dv"), typeof(glUniform2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2dv'.");
            }
        }
        public static void glUniform2dv(GLint @location, GLsizei @count, const GLdouble * @value) => glUniform2dvPtr(@location, @count, @value);

        internal delegate void glUniform3dvFunc(GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glUniform3dvFunc glUniform3dvPtr;
        internal static void loadUniform3dv()
        {
            try
            {
                glUniform3dvPtr = (glUniform3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3dv"), typeof(glUniform3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3dv'.");
            }
        }
        public static void glUniform3dv(GLint @location, GLsizei @count, const GLdouble * @value) => glUniform3dvPtr(@location, @count, @value);

        internal delegate void glUniform4dvFunc(GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glUniform4dvFunc glUniform4dvPtr;
        internal static void loadUniform4dv()
        {
            try
            {
                glUniform4dvPtr = (glUniform4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4dv"), typeof(glUniform4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4dv'.");
            }
        }
        public static void glUniform4dv(GLint @location, GLsizei @count, const GLdouble * @value) => glUniform4dvPtr(@location, @count, @value);

        internal delegate void glUniformMatrix2dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix2dvFunc glUniformMatrix2dvPtr;
        internal static void loadUniformMatrix2dv()
        {
            try
            {
                glUniformMatrix2dvPtr = (glUniformMatrix2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2dv"), typeof(glUniformMatrix2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2dv'.");
            }
        }
        public static void glUniformMatrix2dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix2dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix3dvFunc glUniformMatrix3dvPtr;
        internal static void loadUniformMatrix3dv()
        {
            try
            {
                glUniformMatrix3dvPtr = (glUniformMatrix3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3dv"), typeof(glUniformMatrix3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3dv'.");
            }
        }
        public static void glUniformMatrix3dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix3dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix4dvFunc glUniformMatrix4dvPtr;
        internal static void loadUniformMatrix4dv()
        {
            try
            {
                glUniformMatrix4dvPtr = (glUniformMatrix4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4dv"), typeof(glUniformMatrix4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4dv'.");
            }
        }
        public static void glUniformMatrix4dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix4dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix2x3dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix2x3dvFunc glUniformMatrix2x3dvPtr;
        internal static void loadUniformMatrix2x3dv()
        {
            try
            {
                glUniformMatrix2x3dvPtr = (glUniformMatrix2x3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2x3dv"), typeof(glUniformMatrix2x3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2x3dv'.");
            }
        }
        public static void glUniformMatrix2x3dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix2x3dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix2x4dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix2x4dvFunc glUniformMatrix2x4dvPtr;
        internal static void loadUniformMatrix2x4dv()
        {
            try
            {
                glUniformMatrix2x4dvPtr = (glUniformMatrix2x4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2x4dv"), typeof(glUniformMatrix2x4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2x4dv'.");
            }
        }
        public static void glUniformMatrix2x4dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix2x4dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3x2dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix3x2dvFunc glUniformMatrix3x2dvPtr;
        internal static void loadUniformMatrix3x2dv()
        {
            try
            {
                glUniformMatrix3x2dvPtr = (glUniformMatrix3x2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3x2dv"), typeof(glUniformMatrix3x2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3x2dv'.");
            }
        }
        public static void glUniformMatrix3x2dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix3x2dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3x4dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix3x4dvFunc glUniformMatrix3x4dvPtr;
        internal static void loadUniformMatrix3x4dv()
        {
            try
            {
                glUniformMatrix3x4dvPtr = (glUniformMatrix3x4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3x4dv"), typeof(glUniformMatrix3x4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3x4dv'.");
            }
        }
        public static void glUniformMatrix3x4dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix3x4dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4x2dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix4x2dvFunc glUniformMatrix4x2dvPtr;
        internal static void loadUniformMatrix4x2dv()
        {
            try
            {
                glUniformMatrix4x2dvPtr = (glUniformMatrix4x2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4x2dv"), typeof(glUniformMatrix4x2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4x2dv'.");
            }
        }
        public static void glUniformMatrix4x2dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix4x2dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4x3dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix4x3dvFunc glUniformMatrix4x3dvPtr;
        internal static void loadUniformMatrix4x3dv()
        {
            try
            {
                glUniformMatrix4x3dvPtr = (glUniformMatrix4x3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4x3dv"), typeof(glUniformMatrix4x3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4x3dv'.");
            }
        }
        public static void glUniformMatrix4x3dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix4x3dvPtr(@location, @count, @transpose, @value);

        internal delegate void glGetUniformdvFunc(GLuint @program, GLint @location, GLdouble * @params);
        internal static glGetUniformdvFunc glGetUniformdvPtr;
        internal static void loadGetUniformdv()
        {
            try
            {
                glGetUniformdvPtr = (glGetUniformdvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformdv"), typeof(glGetUniformdvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformdv'.");
            }
        }
        public static void glGetUniformdv(GLuint @program, GLint @location, GLdouble * @params) => glGetUniformdvPtr(@program, @location, @params);

        internal delegate GLint glGetSubroutineUniformLocationFunc(GLuint @program, GLenum @shadertype, const GLchar * @name);
        internal static glGetSubroutineUniformLocationFunc glGetSubroutineUniformLocationPtr;
        internal static void loadGetSubroutineUniformLocation()
        {
            try
            {
                glGetSubroutineUniformLocationPtr = (glGetSubroutineUniformLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSubroutineUniformLocation"), typeof(glGetSubroutineUniformLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSubroutineUniformLocation'.");
            }
        }
        public static GLint glGetSubroutineUniformLocation(GLuint @program, GLenum @shadertype, const GLchar * @name) => glGetSubroutineUniformLocationPtr(@program, @shadertype, @name);

        internal delegate GLuint glGetSubroutineIndexFunc(GLuint @program, GLenum @shadertype, const GLchar * @name);
        internal static glGetSubroutineIndexFunc glGetSubroutineIndexPtr;
        internal static void loadGetSubroutineIndex()
        {
            try
            {
                glGetSubroutineIndexPtr = (glGetSubroutineIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSubroutineIndex"), typeof(glGetSubroutineIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSubroutineIndex'.");
            }
        }
        public static GLuint glGetSubroutineIndex(GLuint @program, GLenum @shadertype, const GLchar * @name) => glGetSubroutineIndexPtr(@program, @shadertype, @name);

        internal delegate void glGetActiveSubroutineUniformivFunc(GLuint @program, GLenum @shadertype, GLuint @index, GLenum @pname, GLint * @values);
        internal static glGetActiveSubroutineUniformivFunc glGetActiveSubroutineUniformivPtr;
        internal static void loadGetActiveSubroutineUniformiv()
        {
            try
            {
                glGetActiveSubroutineUniformivPtr = (glGetActiveSubroutineUniformivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveSubroutineUniformiv"), typeof(glGetActiveSubroutineUniformivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveSubroutineUniformiv'.");
            }
        }
        public static void glGetActiveSubroutineUniformiv(GLuint @program, GLenum @shadertype, GLuint @index, GLenum @pname, GLint * @values) => glGetActiveSubroutineUniformivPtr(@program, @shadertype, @index, @pname, @values);

        internal delegate void glGetActiveSubroutineUniformNameFunc(GLuint @program, GLenum @shadertype, GLuint @index, GLsizei @bufsize, GLsizei * @length, GLchar * @name);
        internal static glGetActiveSubroutineUniformNameFunc glGetActiveSubroutineUniformNamePtr;
        internal static void loadGetActiveSubroutineUniformName()
        {
            try
            {
                glGetActiveSubroutineUniformNamePtr = (glGetActiveSubroutineUniformNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveSubroutineUniformName"), typeof(glGetActiveSubroutineUniformNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveSubroutineUniformName'.");
            }
        }
        public static void glGetActiveSubroutineUniformName(GLuint @program, GLenum @shadertype, GLuint @index, GLsizei @bufsize, GLsizei * @length, GLchar * @name) => glGetActiveSubroutineUniformNamePtr(@program, @shadertype, @index, @bufsize, @length, @name);

        internal delegate void glGetActiveSubroutineNameFunc(GLuint @program, GLenum @shadertype, GLuint @index, GLsizei @bufsize, GLsizei * @length, GLchar * @name);
        internal static glGetActiveSubroutineNameFunc glGetActiveSubroutineNamePtr;
        internal static void loadGetActiveSubroutineName()
        {
            try
            {
                glGetActiveSubroutineNamePtr = (glGetActiveSubroutineNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveSubroutineName"), typeof(glGetActiveSubroutineNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveSubroutineName'.");
            }
        }
        public static void glGetActiveSubroutineName(GLuint @program, GLenum @shadertype, GLuint @index, GLsizei @bufsize, GLsizei * @length, GLchar * @name) => glGetActiveSubroutineNamePtr(@program, @shadertype, @index, @bufsize, @length, @name);

        internal delegate void glUniformSubroutinesuivFunc(GLenum @shadertype, GLsizei @count, const GLuint * @indices);
        internal static glUniformSubroutinesuivFunc glUniformSubroutinesuivPtr;
        internal static void loadUniformSubroutinesuiv()
        {
            try
            {
                glUniformSubroutinesuivPtr = (glUniformSubroutinesuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformSubroutinesuiv"), typeof(glUniformSubroutinesuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformSubroutinesuiv'.");
            }
        }
        public static void glUniformSubroutinesuiv(GLenum @shadertype, GLsizei @count, const GLuint * @indices) => glUniformSubroutinesuivPtr(@shadertype, @count, @indices);

        internal delegate void glGetUniformSubroutineuivFunc(GLenum @shadertype, GLint @location, GLuint * @params);
        internal static glGetUniformSubroutineuivFunc glGetUniformSubroutineuivPtr;
        internal static void loadGetUniformSubroutineuiv()
        {
            try
            {
                glGetUniformSubroutineuivPtr = (glGetUniformSubroutineuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformSubroutineuiv"), typeof(glGetUniformSubroutineuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformSubroutineuiv'.");
            }
        }
        public static void glGetUniformSubroutineuiv(GLenum @shadertype, GLint @location, GLuint * @params) => glGetUniformSubroutineuivPtr(@shadertype, @location, @params);

        internal delegate void glGetProgramStageivFunc(GLuint @program, GLenum @shadertype, GLenum @pname, GLint * @values);
        internal static glGetProgramStageivFunc glGetProgramStageivPtr;
        internal static void loadGetProgramStageiv()
        {
            try
            {
                glGetProgramStageivPtr = (glGetProgramStageivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramStageiv"), typeof(glGetProgramStageivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramStageiv'.");
            }
        }
        public static void glGetProgramStageiv(GLuint @program, GLenum @shadertype, GLenum @pname, GLint * @values) => glGetProgramStageivPtr(@program, @shadertype, @pname, @values);

        internal delegate void glPatchParameteriFunc(GLenum @pname, GLint @value);
        internal static glPatchParameteriFunc glPatchParameteriPtr;
        internal static void loadPatchParameteri()
        {
            try
            {
                glPatchParameteriPtr = (glPatchParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPatchParameteri"), typeof(glPatchParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPatchParameteri'.");
            }
        }
        public static void glPatchParameteri(GLenum @pname, GLint @value) => glPatchParameteriPtr(@pname, @value);

        internal delegate void glPatchParameterfvFunc(GLenum @pname, const GLfloat * @values);
        internal static glPatchParameterfvFunc glPatchParameterfvPtr;
        internal static void loadPatchParameterfv()
        {
            try
            {
                glPatchParameterfvPtr = (glPatchParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPatchParameterfv"), typeof(glPatchParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPatchParameterfv'.");
            }
        }
        public static void glPatchParameterfv(GLenum @pname, const GLfloat * @values) => glPatchParameterfvPtr(@pname, @values);

        internal delegate void glBindTransformFeedbackFunc(GLenum @target, GLuint @id);
        internal static glBindTransformFeedbackFunc glBindTransformFeedbackPtr;
        internal static void loadBindTransformFeedback()
        {
            try
            {
                glBindTransformFeedbackPtr = (glBindTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTransformFeedback"), typeof(glBindTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTransformFeedback'.");
            }
        }
        public static void glBindTransformFeedback(GLenum @target, GLuint @id) => glBindTransformFeedbackPtr(@target, @id);

        internal delegate void glDeleteTransformFeedbacksFunc(GLsizei @n, const GLuint * @ids);
        internal static glDeleteTransformFeedbacksFunc glDeleteTransformFeedbacksPtr;
        internal static void loadDeleteTransformFeedbacks()
        {
            try
            {
                glDeleteTransformFeedbacksPtr = (glDeleteTransformFeedbacksFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteTransformFeedbacks"), typeof(glDeleteTransformFeedbacksFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteTransformFeedbacks'.");
            }
        }
        public static void glDeleteTransformFeedbacks(GLsizei @n, const GLuint * @ids) => glDeleteTransformFeedbacksPtr(@n, @ids);

        internal delegate void glGenTransformFeedbacksFunc(GLsizei @n, GLuint * @ids);
        internal static glGenTransformFeedbacksFunc glGenTransformFeedbacksPtr;
        internal static void loadGenTransformFeedbacks()
        {
            try
            {
                glGenTransformFeedbacksPtr = (glGenTransformFeedbacksFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenTransformFeedbacks"), typeof(glGenTransformFeedbacksFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenTransformFeedbacks'.");
            }
        }
        public static void glGenTransformFeedbacks(GLsizei @n, GLuint * @ids) => glGenTransformFeedbacksPtr(@n, @ids);

        internal delegate GLboolean glIsTransformFeedbackFunc(GLuint @id);
        internal static glIsTransformFeedbackFunc glIsTransformFeedbackPtr;
        internal static void loadIsTransformFeedback()
        {
            try
            {
                glIsTransformFeedbackPtr = (glIsTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsTransformFeedback"), typeof(glIsTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsTransformFeedback'.");
            }
        }
        public static GLboolean glIsTransformFeedback(GLuint @id) => glIsTransformFeedbackPtr(@id);

        internal delegate void glPauseTransformFeedbackFunc();
        internal static glPauseTransformFeedbackFunc glPauseTransformFeedbackPtr;
        internal static void loadPauseTransformFeedback()
        {
            try
            {
                glPauseTransformFeedbackPtr = (glPauseTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPauseTransformFeedback"), typeof(glPauseTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPauseTransformFeedback'.");
            }
        }
        public static void glPauseTransformFeedback() => glPauseTransformFeedbackPtr();

        internal delegate void glResumeTransformFeedbackFunc();
        internal static glResumeTransformFeedbackFunc glResumeTransformFeedbackPtr;
        internal static void loadResumeTransformFeedback()
        {
            try
            {
                glResumeTransformFeedbackPtr = (glResumeTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glResumeTransformFeedback"), typeof(glResumeTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glResumeTransformFeedback'.");
            }
        }
        public static void glResumeTransformFeedback() => glResumeTransformFeedbackPtr();

        internal delegate void glDrawTransformFeedbackFunc(GLenum @mode, GLuint @id);
        internal static glDrawTransformFeedbackFunc glDrawTransformFeedbackPtr;
        internal static void loadDrawTransformFeedback()
        {
            try
            {
                glDrawTransformFeedbackPtr = (glDrawTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTransformFeedback"), typeof(glDrawTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTransformFeedback'.");
            }
        }
        public static void glDrawTransformFeedback(GLenum @mode, GLuint @id) => glDrawTransformFeedbackPtr(@mode, @id);

        internal delegate void glDrawTransformFeedbackStreamFunc(GLenum @mode, GLuint @id, GLuint @stream);
        internal static glDrawTransformFeedbackStreamFunc glDrawTransformFeedbackStreamPtr;
        internal static void loadDrawTransformFeedbackStream()
        {
            try
            {
                glDrawTransformFeedbackStreamPtr = (glDrawTransformFeedbackStreamFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTransformFeedbackStream"), typeof(glDrawTransformFeedbackStreamFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTransformFeedbackStream'.");
            }
        }
        public static void glDrawTransformFeedbackStream(GLenum @mode, GLuint @id, GLuint @stream) => glDrawTransformFeedbackStreamPtr(@mode, @id, @stream);

        internal delegate void glBeginQueryIndexedFunc(GLenum @target, GLuint @index, GLuint @id);
        internal static glBeginQueryIndexedFunc glBeginQueryIndexedPtr;
        internal static void loadBeginQueryIndexed()
        {
            try
            {
                glBeginQueryIndexedPtr = (glBeginQueryIndexedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginQueryIndexed"), typeof(glBeginQueryIndexedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginQueryIndexed'.");
            }
        }
        public static void glBeginQueryIndexed(GLenum @target, GLuint @index, GLuint @id) => glBeginQueryIndexedPtr(@target, @index, @id);

        internal delegate void glEndQueryIndexedFunc(GLenum @target, GLuint @index);
        internal static glEndQueryIndexedFunc glEndQueryIndexedPtr;
        internal static void loadEndQueryIndexed()
        {
            try
            {
                glEndQueryIndexedPtr = (glEndQueryIndexedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndQueryIndexed"), typeof(glEndQueryIndexedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndQueryIndexed'.");
            }
        }
        public static void glEndQueryIndexed(GLenum @target, GLuint @index) => glEndQueryIndexedPtr(@target, @index);

        internal delegate void glGetQueryIndexedivFunc(GLenum @target, GLuint @index, GLenum @pname, GLint * @params);
        internal static glGetQueryIndexedivFunc glGetQueryIndexedivPtr;
        internal static void loadGetQueryIndexediv()
        {
            try
            {
                glGetQueryIndexedivPtr = (glGetQueryIndexedivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryIndexediv"), typeof(glGetQueryIndexedivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryIndexediv'.");
            }
        }
        public static void glGetQueryIndexediv(GLenum @target, GLuint @index, GLenum @pname, GLint * @params) => glGetQueryIndexedivPtr(@target, @index, @pname, @params);
        #endregion
    }
}
