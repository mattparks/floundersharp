using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_copy_image
    {
        #region Interop
        static GL_ARB_copy_image()
        {
            Console.WriteLine("Initalising GL_ARB_copy_image interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCopyImageSubData();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glCopyImageSubDataFunc(GLuint @srcName, GLenum @srcTarget, GLint @srcLevel, GLint @srcX, GLint @srcY, GLint @srcZ, GLuint @dstName, GLenum @dstTarget, GLint @dstLevel, GLint @dstX, GLint @dstY, GLint @dstZ, GLsizei @srcWidth, GLsizei @srcHeight, GLsizei @srcDepth);
        internal static glCopyImageSubDataFunc glCopyImageSubDataPtr;
        internal static void loadCopyImageSubData()
        {
            try
            {
                glCopyImageSubDataPtr = (glCopyImageSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyImageSubData"), typeof(glCopyImageSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyImageSubData'.");
            }
        }
        public static void glCopyImageSubData(GLuint @srcName, GLenum @srcTarget, GLint @srcLevel, GLint @srcX, GLint @srcY, GLint @srcZ, GLuint @dstName, GLenum @dstTarget, GLint @dstLevel, GLint @dstX, GLint @dstY, GLint @dstZ, GLsizei @srcWidth, GLsizei @srcHeight, GLsizei @srcDepth) => glCopyImageSubDataPtr(@srcName, @srcTarget, @srcLevel, @srcX, @srcY, @srcZ, @dstName, @dstTarget, @dstLevel, @dstX, @dstY, @dstZ, @srcWidth, @srcHeight, @srcDepth);
        #endregion
    }
}
