using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_vertex_blend
    {
        #region Interop
        static GL_ARB_vertex_blend()
        {
            Console.WriteLine("Initalising GL_ARB_vertex_blend interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadWeightbvARB();
            loadWeightsvARB();
            loadWeightivARB();
            loadWeightfvARB();
            loadWeightdvARB();
            loadWeightubvARB();
            loadWeightusvARB();
            loadWeightuivARB();
            loadWeightPointerARB();
            loadVertexBlendARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_VERTEX_UNITS_ARB = 0x86A4;
        public static UInt32 GL_ACTIVE_VERTEX_UNITS_ARB = 0x86A5;
        public static UInt32 GL_WEIGHT_SUM_UNITY_ARB = 0x86A6;
        public static UInt32 GL_VERTEX_BLEND_ARB = 0x86A7;
        public static UInt32 GL_CURRENT_WEIGHT_ARB = 0x86A8;
        public static UInt32 GL_WEIGHT_ARRAY_TYPE_ARB = 0x86A9;
        public static UInt32 GL_WEIGHT_ARRAY_STRIDE_ARB = 0x86AA;
        public static UInt32 GL_WEIGHT_ARRAY_SIZE_ARB = 0x86AB;
        public static UInt32 GL_WEIGHT_ARRAY_POINTER_ARB = 0x86AC;
        public static UInt32 GL_WEIGHT_ARRAY_ARB = 0x86AD;
        public static UInt32 GL_MODELVIEW0_ARB = 0x1700;
        public static UInt32 GL_MODELVIEW1_ARB = 0x850A;
        public static UInt32 GL_MODELVIEW2_ARB = 0x8722;
        public static UInt32 GL_MODELVIEW3_ARB = 0x8723;
        public static UInt32 GL_MODELVIEW4_ARB = 0x8724;
        public static UInt32 GL_MODELVIEW5_ARB = 0x8725;
        public static UInt32 GL_MODELVIEW6_ARB = 0x8726;
        public static UInt32 GL_MODELVIEW7_ARB = 0x8727;
        public static UInt32 GL_MODELVIEW8_ARB = 0x8728;
        public static UInt32 GL_MODELVIEW9_ARB = 0x8729;
        public static UInt32 GL_MODELVIEW10_ARB = 0x872A;
        public static UInt32 GL_MODELVIEW11_ARB = 0x872B;
        public static UInt32 GL_MODELVIEW12_ARB = 0x872C;
        public static UInt32 GL_MODELVIEW13_ARB = 0x872D;
        public static UInt32 GL_MODELVIEW14_ARB = 0x872E;
        public static UInt32 GL_MODELVIEW15_ARB = 0x872F;
        public static UInt32 GL_MODELVIEW16_ARB = 0x8730;
        public static UInt32 GL_MODELVIEW17_ARB = 0x8731;
        public static UInt32 GL_MODELVIEW18_ARB = 0x8732;
        public static UInt32 GL_MODELVIEW19_ARB = 0x8733;
        public static UInt32 GL_MODELVIEW20_ARB = 0x8734;
        public static UInt32 GL_MODELVIEW21_ARB = 0x8735;
        public static UInt32 GL_MODELVIEW22_ARB = 0x8736;
        public static UInt32 GL_MODELVIEW23_ARB = 0x8737;
        public static UInt32 GL_MODELVIEW24_ARB = 0x8738;
        public static UInt32 GL_MODELVIEW25_ARB = 0x8739;
        public static UInt32 GL_MODELVIEW26_ARB = 0x873A;
        public static UInt32 GL_MODELVIEW27_ARB = 0x873B;
        public static UInt32 GL_MODELVIEW28_ARB = 0x873C;
        public static UInt32 GL_MODELVIEW29_ARB = 0x873D;
        public static UInt32 GL_MODELVIEW30_ARB = 0x873E;
        public static UInt32 GL_MODELVIEW31_ARB = 0x873F;
        #endregion

        #region Commands
        internal delegate void glWeightbvARBFunc(GLint @size, const GLbyte * @weights);
        internal static glWeightbvARBFunc glWeightbvARBPtr;
        internal static void loadWeightbvARB()
        {
            try
            {
                glWeightbvARBPtr = (glWeightbvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWeightbvARB"), typeof(glWeightbvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWeightbvARB'.");
            }
        }
        public static void glWeightbvARB(GLint @size, const GLbyte * @weights) => glWeightbvARBPtr(@size, @weights);

        internal delegate void glWeightsvARBFunc(GLint @size, const GLshort * @weights);
        internal static glWeightsvARBFunc glWeightsvARBPtr;
        internal static void loadWeightsvARB()
        {
            try
            {
                glWeightsvARBPtr = (glWeightsvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWeightsvARB"), typeof(glWeightsvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWeightsvARB'.");
            }
        }
        public static void glWeightsvARB(GLint @size, const GLshort * @weights) => glWeightsvARBPtr(@size, @weights);

        internal delegate void glWeightivARBFunc(GLint @size, const GLint * @weights);
        internal static glWeightivARBFunc glWeightivARBPtr;
        internal static void loadWeightivARB()
        {
            try
            {
                glWeightivARBPtr = (glWeightivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWeightivARB"), typeof(glWeightivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWeightivARB'.");
            }
        }
        public static void glWeightivARB(GLint @size, const GLint * @weights) => glWeightivARBPtr(@size, @weights);

        internal delegate void glWeightfvARBFunc(GLint @size, const GLfloat * @weights);
        internal static glWeightfvARBFunc glWeightfvARBPtr;
        internal static void loadWeightfvARB()
        {
            try
            {
                glWeightfvARBPtr = (glWeightfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWeightfvARB"), typeof(glWeightfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWeightfvARB'.");
            }
        }
        public static void glWeightfvARB(GLint @size, const GLfloat * @weights) => glWeightfvARBPtr(@size, @weights);

        internal delegate void glWeightdvARBFunc(GLint @size, const GLdouble * @weights);
        internal static glWeightdvARBFunc glWeightdvARBPtr;
        internal static void loadWeightdvARB()
        {
            try
            {
                glWeightdvARBPtr = (glWeightdvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWeightdvARB"), typeof(glWeightdvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWeightdvARB'.");
            }
        }
        public static void glWeightdvARB(GLint @size, const GLdouble * @weights) => glWeightdvARBPtr(@size, @weights);

        internal delegate void glWeightubvARBFunc(GLint @size, const GLubyte * @weights);
        internal static glWeightubvARBFunc glWeightubvARBPtr;
        internal static void loadWeightubvARB()
        {
            try
            {
                glWeightubvARBPtr = (glWeightubvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWeightubvARB"), typeof(glWeightubvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWeightubvARB'.");
            }
        }
        public static void glWeightubvARB(GLint @size, const GLubyte * @weights) => glWeightubvARBPtr(@size, @weights);

        internal delegate void glWeightusvARBFunc(GLint @size, const GLushort * @weights);
        internal static glWeightusvARBFunc glWeightusvARBPtr;
        internal static void loadWeightusvARB()
        {
            try
            {
                glWeightusvARBPtr = (glWeightusvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWeightusvARB"), typeof(glWeightusvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWeightusvARB'.");
            }
        }
        public static void glWeightusvARB(GLint @size, const GLushort * @weights) => glWeightusvARBPtr(@size, @weights);

        internal delegate void glWeightuivARBFunc(GLint @size, const GLuint * @weights);
        internal static glWeightuivARBFunc glWeightuivARBPtr;
        internal static void loadWeightuivARB()
        {
            try
            {
                glWeightuivARBPtr = (glWeightuivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWeightuivARB"), typeof(glWeightuivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWeightuivARB'.");
            }
        }
        public static void glWeightuivARB(GLint @size, const GLuint * @weights) => glWeightuivARBPtr(@size, @weights);

        internal delegate void glWeightPointerARBFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glWeightPointerARBFunc glWeightPointerARBPtr;
        internal static void loadWeightPointerARB()
        {
            try
            {
                glWeightPointerARBPtr = (glWeightPointerARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWeightPointerARB"), typeof(glWeightPointerARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWeightPointerARB'.");
            }
        }
        public static void glWeightPointerARB(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glWeightPointerARBPtr(@size, @type, @stride, @pointer);

        internal delegate void glVertexBlendARBFunc(GLint @count);
        internal static glVertexBlendARBFunc glVertexBlendARBPtr;
        internal static void loadVertexBlendARB()
        {
            try
            {
                glVertexBlendARBPtr = (glVertexBlendARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexBlendARB"), typeof(glVertexBlendARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexBlendARB'.");
            }
        }
        public static void glVertexBlendARB(GLint @count) => glVertexBlendARBPtr(@count);
        #endregion
    }
}
