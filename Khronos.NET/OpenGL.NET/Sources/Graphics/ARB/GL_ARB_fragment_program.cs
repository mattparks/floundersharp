using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_fragment_program
    {
        #region Interop
        static GL_ARB_fragment_program()
        {
            Console.WriteLine("Initalising GL_ARB_fragment_program interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadProgramStringARB();
            loadBindProgramARB();
            loadDeleteProgramsARB();
            loadGenProgramsARB();
            loadProgramEnvParameter4dARB();
            loadProgramEnvParameter4dvARB();
            loadProgramEnvParameter4fARB();
            loadProgramEnvParameter4fvARB();
            loadProgramLocalParameter4dARB();
            loadProgramLocalParameter4dvARB();
            loadProgramLocalParameter4fARB();
            loadProgramLocalParameter4fvARB();
            loadGetProgramEnvParameterdvARB();
            loadGetProgramEnvParameterfvARB();
            loadGetProgramLocalParameterdvARB();
            loadGetProgramLocalParameterfvARB();
            loadGetProgramivARB();
            loadGetProgramStringARB();
            loadIsProgramARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAGMENT_PROGRAM_ARB = 0x8804;
        public static UInt32 GL_PROGRAM_FORMAT_ASCII_ARB = 0x8875;
        public static UInt32 GL_PROGRAM_LENGTH_ARB = 0x8627;
        public static UInt32 GL_PROGRAM_FORMAT_ARB = 0x8876;
        public static UInt32 GL_PROGRAM_BINDING_ARB = 0x8677;
        public static UInt32 GL_PROGRAM_INSTRUCTIONS_ARB = 0x88A0;
        public static UInt32 GL_MAX_PROGRAM_INSTRUCTIONS_ARB = 0x88A1;
        public static UInt32 GL_PROGRAM_NATIVE_INSTRUCTIONS_ARB = 0x88A2;
        public static UInt32 GL_MAX_PROGRAM_NATIVE_INSTRUCTIONS_ARB = 0x88A3;
        public static UInt32 GL_PROGRAM_TEMPORARIES_ARB = 0x88A4;
        public static UInt32 GL_MAX_PROGRAM_TEMPORARIES_ARB = 0x88A5;
        public static UInt32 GL_PROGRAM_NATIVE_TEMPORARIES_ARB = 0x88A6;
        public static UInt32 GL_MAX_PROGRAM_NATIVE_TEMPORARIES_ARB = 0x88A7;
        public static UInt32 GL_PROGRAM_PARAMETERS_ARB = 0x88A8;
        public static UInt32 GL_MAX_PROGRAM_PARAMETERS_ARB = 0x88A9;
        public static UInt32 GL_PROGRAM_NATIVE_PARAMETERS_ARB = 0x88AA;
        public static UInt32 GL_MAX_PROGRAM_NATIVE_PARAMETERS_ARB = 0x88AB;
        public static UInt32 GL_PROGRAM_ATTRIBS_ARB = 0x88AC;
        public static UInt32 GL_MAX_PROGRAM_ATTRIBS_ARB = 0x88AD;
        public static UInt32 GL_PROGRAM_NATIVE_ATTRIBS_ARB = 0x88AE;
        public static UInt32 GL_MAX_PROGRAM_NATIVE_ATTRIBS_ARB = 0x88AF;
        public static UInt32 GL_MAX_PROGRAM_LOCAL_PARAMETERS_ARB = 0x88B4;
        public static UInt32 GL_MAX_PROGRAM_ENV_PARAMETERS_ARB = 0x88B5;
        public static UInt32 GL_PROGRAM_UNDER_NATIVE_LIMITS_ARB = 0x88B6;
        public static UInt32 GL_PROGRAM_ALU_INSTRUCTIONS_ARB = 0x8805;
        public static UInt32 GL_PROGRAM_TEX_INSTRUCTIONS_ARB = 0x8806;
        public static UInt32 GL_PROGRAM_TEX_INDIRECTIONS_ARB = 0x8807;
        public static UInt32 GL_PROGRAM_NATIVE_ALU_INSTRUCTIONS_ARB = 0x8808;
        public static UInt32 GL_PROGRAM_NATIVE_TEX_INSTRUCTIONS_ARB = 0x8809;
        public static UInt32 GL_PROGRAM_NATIVE_TEX_INDIRECTIONS_ARB = 0x880A;
        public static UInt32 GL_MAX_PROGRAM_ALU_INSTRUCTIONS_ARB = 0x880B;
        public static UInt32 GL_MAX_PROGRAM_TEX_INSTRUCTIONS_ARB = 0x880C;
        public static UInt32 GL_MAX_PROGRAM_TEX_INDIRECTIONS_ARB = 0x880D;
        public static UInt32 GL_MAX_PROGRAM_NATIVE_ALU_INSTRUCTIONS_ARB = 0x880E;
        public static UInt32 GL_MAX_PROGRAM_NATIVE_TEX_INSTRUCTIONS_ARB = 0x880F;
        public static UInt32 GL_MAX_PROGRAM_NATIVE_TEX_INDIRECTIONS_ARB = 0x8810;
        public static UInt32 GL_PROGRAM_STRING_ARB = 0x8628;
        public static UInt32 GL_PROGRAM_ERROR_POSITION_ARB = 0x864B;
        public static UInt32 GL_CURRENT_MATRIX_ARB = 0x8641;
        public static UInt32 GL_TRANSPOSE_CURRENT_MATRIX_ARB = 0x88B7;
        public static UInt32 GL_CURRENT_MATRIX_STACK_DEPTH_ARB = 0x8640;
        public static UInt32 GL_MAX_PROGRAM_MATRICES_ARB = 0x862F;
        public static UInt32 GL_MAX_PROGRAM_MATRIX_STACK_DEPTH_ARB = 0x862E;
        public static UInt32 GL_MAX_TEXTURE_COORDS_ARB = 0x8871;
        public static UInt32 GL_MAX_TEXTURE_IMAGE_UNITS_ARB = 0x8872;
        public static UInt32 GL_PROGRAM_ERROR_STRING_ARB = 0x8874;
        public static UInt32 GL_MATRIX0_ARB = 0x88C0;
        public static UInt32 GL_MATRIX1_ARB = 0x88C1;
        public static UInt32 GL_MATRIX2_ARB = 0x88C2;
        public static UInt32 GL_MATRIX3_ARB = 0x88C3;
        public static UInt32 GL_MATRIX4_ARB = 0x88C4;
        public static UInt32 GL_MATRIX5_ARB = 0x88C5;
        public static UInt32 GL_MATRIX6_ARB = 0x88C6;
        public static UInt32 GL_MATRIX7_ARB = 0x88C7;
        public static UInt32 GL_MATRIX8_ARB = 0x88C8;
        public static UInt32 GL_MATRIX9_ARB = 0x88C9;
        public static UInt32 GL_MATRIX10_ARB = 0x88CA;
        public static UInt32 GL_MATRIX11_ARB = 0x88CB;
        public static UInt32 GL_MATRIX12_ARB = 0x88CC;
        public static UInt32 GL_MATRIX13_ARB = 0x88CD;
        public static UInt32 GL_MATRIX14_ARB = 0x88CE;
        public static UInt32 GL_MATRIX15_ARB = 0x88CF;
        public static UInt32 GL_MATRIX16_ARB = 0x88D0;
        public static UInt32 GL_MATRIX17_ARB = 0x88D1;
        public static UInt32 GL_MATRIX18_ARB = 0x88D2;
        public static UInt32 GL_MATRIX19_ARB = 0x88D3;
        public static UInt32 GL_MATRIX20_ARB = 0x88D4;
        public static UInt32 GL_MATRIX21_ARB = 0x88D5;
        public static UInt32 GL_MATRIX22_ARB = 0x88D6;
        public static UInt32 GL_MATRIX23_ARB = 0x88D7;
        public static UInt32 GL_MATRIX24_ARB = 0x88D8;
        public static UInt32 GL_MATRIX25_ARB = 0x88D9;
        public static UInt32 GL_MATRIX26_ARB = 0x88DA;
        public static UInt32 GL_MATRIX27_ARB = 0x88DB;
        public static UInt32 GL_MATRIX28_ARB = 0x88DC;
        public static UInt32 GL_MATRIX29_ARB = 0x88DD;
        public static UInt32 GL_MATRIX30_ARB = 0x88DE;
        public static UInt32 GL_MATRIX31_ARB = 0x88DF;
        #endregion

        #region Commands
        internal delegate void glProgramStringARBFunc(GLenum @target, GLenum @format, GLsizei @len, const void * @string);
        internal static glProgramStringARBFunc glProgramStringARBPtr;
        internal static void loadProgramStringARB()
        {
            try
            {
                glProgramStringARBPtr = (glProgramStringARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramStringARB"), typeof(glProgramStringARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramStringARB'.");
            }
        }
        public static void glProgramStringARB(GLenum @target, GLenum @format, GLsizei @len, const void * @string) => glProgramStringARBPtr(@target, @format, @len, @string);

        internal delegate void glBindProgramARBFunc(GLenum @target, GLuint @program);
        internal static glBindProgramARBFunc glBindProgramARBPtr;
        internal static void loadBindProgramARB()
        {
            try
            {
                glBindProgramARBPtr = (glBindProgramARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindProgramARB"), typeof(glBindProgramARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindProgramARB'.");
            }
        }
        public static void glBindProgramARB(GLenum @target, GLuint @program) => glBindProgramARBPtr(@target, @program);

        internal delegate void glDeleteProgramsARBFunc(GLsizei @n, const GLuint * @programs);
        internal static glDeleteProgramsARBFunc glDeleteProgramsARBPtr;
        internal static void loadDeleteProgramsARB()
        {
            try
            {
                glDeleteProgramsARBPtr = (glDeleteProgramsARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteProgramsARB"), typeof(glDeleteProgramsARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteProgramsARB'.");
            }
        }
        public static void glDeleteProgramsARB(GLsizei @n, const GLuint * @programs) => glDeleteProgramsARBPtr(@n, @programs);

        internal delegate void glGenProgramsARBFunc(GLsizei @n, GLuint * @programs);
        internal static glGenProgramsARBFunc glGenProgramsARBPtr;
        internal static void loadGenProgramsARB()
        {
            try
            {
                glGenProgramsARBPtr = (glGenProgramsARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenProgramsARB"), typeof(glGenProgramsARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenProgramsARB'.");
            }
        }
        public static void glGenProgramsARB(GLsizei @n, GLuint * @programs) => glGenProgramsARBPtr(@n, @programs);

        internal delegate void glProgramEnvParameter4dARBFunc(GLenum @target, GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glProgramEnvParameter4dARBFunc glProgramEnvParameter4dARBPtr;
        internal static void loadProgramEnvParameter4dARB()
        {
            try
            {
                glProgramEnvParameter4dARBPtr = (glProgramEnvParameter4dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameter4dARB"), typeof(glProgramEnvParameter4dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameter4dARB'.");
            }
        }
        public static void glProgramEnvParameter4dARB(GLenum @target, GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glProgramEnvParameter4dARBPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramEnvParameter4dvARBFunc(GLenum @target, GLuint @index, const GLdouble * @params);
        internal static glProgramEnvParameter4dvARBFunc glProgramEnvParameter4dvARBPtr;
        internal static void loadProgramEnvParameter4dvARB()
        {
            try
            {
                glProgramEnvParameter4dvARBPtr = (glProgramEnvParameter4dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameter4dvARB"), typeof(glProgramEnvParameter4dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameter4dvARB'.");
            }
        }
        public static void glProgramEnvParameter4dvARB(GLenum @target, GLuint @index, const GLdouble * @params) => glProgramEnvParameter4dvARBPtr(@target, @index, @params);

        internal delegate void glProgramEnvParameter4fARBFunc(GLenum @target, GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glProgramEnvParameter4fARBFunc glProgramEnvParameter4fARBPtr;
        internal static void loadProgramEnvParameter4fARB()
        {
            try
            {
                glProgramEnvParameter4fARBPtr = (glProgramEnvParameter4fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameter4fARB"), typeof(glProgramEnvParameter4fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameter4fARB'.");
            }
        }
        public static void glProgramEnvParameter4fARB(GLenum @target, GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glProgramEnvParameter4fARBPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramEnvParameter4fvARBFunc(GLenum @target, GLuint @index, const GLfloat * @params);
        internal static glProgramEnvParameter4fvARBFunc glProgramEnvParameter4fvARBPtr;
        internal static void loadProgramEnvParameter4fvARB()
        {
            try
            {
                glProgramEnvParameter4fvARBPtr = (glProgramEnvParameter4fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameter4fvARB"), typeof(glProgramEnvParameter4fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameter4fvARB'.");
            }
        }
        public static void glProgramEnvParameter4fvARB(GLenum @target, GLuint @index, const GLfloat * @params) => glProgramEnvParameter4fvARBPtr(@target, @index, @params);

        internal delegate void glProgramLocalParameter4dARBFunc(GLenum @target, GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glProgramLocalParameter4dARBFunc glProgramLocalParameter4dARBPtr;
        internal static void loadProgramLocalParameter4dARB()
        {
            try
            {
                glProgramLocalParameter4dARBPtr = (glProgramLocalParameter4dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameter4dARB"), typeof(glProgramLocalParameter4dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameter4dARB'.");
            }
        }
        public static void glProgramLocalParameter4dARB(GLenum @target, GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glProgramLocalParameter4dARBPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramLocalParameter4dvARBFunc(GLenum @target, GLuint @index, const GLdouble * @params);
        internal static glProgramLocalParameter4dvARBFunc glProgramLocalParameter4dvARBPtr;
        internal static void loadProgramLocalParameter4dvARB()
        {
            try
            {
                glProgramLocalParameter4dvARBPtr = (glProgramLocalParameter4dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameter4dvARB"), typeof(glProgramLocalParameter4dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameter4dvARB'.");
            }
        }
        public static void glProgramLocalParameter4dvARB(GLenum @target, GLuint @index, const GLdouble * @params) => glProgramLocalParameter4dvARBPtr(@target, @index, @params);

        internal delegate void glProgramLocalParameter4fARBFunc(GLenum @target, GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glProgramLocalParameter4fARBFunc glProgramLocalParameter4fARBPtr;
        internal static void loadProgramLocalParameter4fARB()
        {
            try
            {
                glProgramLocalParameter4fARBPtr = (glProgramLocalParameter4fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameter4fARB"), typeof(glProgramLocalParameter4fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameter4fARB'.");
            }
        }
        public static void glProgramLocalParameter4fARB(GLenum @target, GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glProgramLocalParameter4fARBPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramLocalParameter4fvARBFunc(GLenum @target, GLuint @index, const GLfloat * @params);
        internal static glProgramLocalParameter4fvARBFunc glProgramLocalParameter4fvARBPtr;
        internal static void loadProgramLocalParameter4fvARB()
        {
            try
            {
                glProgramLocalParameter4fvARBPtr = (glProgramLocalParameter4fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameter4fvARB"), typeof(glProgramLocalParameter4fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameter4fvARB'.");
            }
        }
        public static void glProgramLocalParameter4fvARB(GLenum @target, GLuint @index, const GLfloat * @params) => glProgramLocalParameter4fvARBPtr(@target, @index, @params);

        internal delegate void glGetProgramEnvParameterdvARBFunc(GLenum @target, GLuint @index, GLdouble * @params);
        internal static glGetProgramEnvParameterdvARBFunc glGetProgramEnvParameterdvARBPtr;
        internal static void loadGetProgramEnvParameterdvARB()
        {
            try
            {
                glGetProgramEnvParameterdvARBPtr = (glGetProgramEnvParameterdvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramEnvParameterdvARB"), typeof(glGetProgramEnvParameterdvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramEnvParameterdvARB'.");
            }
        }
        public static void glGetProgramEnvParameterdvARB(GLenum @target, GLuint @index, GLdouble * @params) => glGetProgramEnvParameterdvARBPtr(@target, @index, @params);

        internal delegate void glGetProgramEnvParameterfvARBFunc(GLenum @target, GLuint @index, GLfloat * @params);
        internal static glGetProgramEnvParameterfvARBFunc glGetProgramEnvParameterfvARBPtr;
        internal static void loadGetProgramEnvParameterfvARB()
        {
            try
            {
                glGetProgramEnvParameterfvARBPtr = (glGetProgramEnvParameterfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramEnvParameterfvARB"), typeof(glGetProgramEnvParameterfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramEnvParameterfvARB'.");
            }
        }
        public static void glGetProgramEnvParameterfvARB(GLenum @target, GLuint @index, GLfloat * @params) => glGetProgramEnvParameterfvARBPtr(@target, @index, @params);

        internal delegate void glGetProgramLocalParameterdvARBFunc(GLenum @target, GLuint @index, GLdouble * @params);
        internal static glGetProgramLocalParameterdvARBFunc glGetProgramLocalParameterdvARBPtr;
        internal static void loadGetProgramLocalParameterdvARB()
        {
            try
            {
                glGetProgramLocalParameterdvARBPtr = (glGetProgramLocalParameterdvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramLocalParameterdvARB"), typeof(glGetProgramLocalParameterdvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramLocalParameterdvARB'.");
            }
        }
        public static void glGetProgramLocalParameterdvARB(GLenum @target, GLuint @index, GLdouble * @params) => glGetProgramLocalParameterdvARBPtr(@target, @index, @params);

        internal delegate void glGetProgramLocalParameterfvARBFunc(GLenum @target, GLuint @index, GLfloat * @params);
        internal static glGetProgramLocalParameterfvARBFunc glGetProgramLocalParameterfvARBPtr;
        internal static void loadGetProgramLocalParameterfvARB()
        {
            try
            {
                glGetProgramLocalParameterfvARBPtr = (glGetProgramLocalParameterfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramLocalParameterfvARB"), typeof(glGetProgramLocalParameterfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramLocalParameterfvARB'.");
            }
        }
        public static void glGetProgramLocalParameterfvARB(GLenum @target, GLuint @index, GLfloat * @params) => glGetProgramLocalParameterfvARBPtr(@target, @index, @params);

        internal delegate void glGetProgramivARBFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetProgramivARBFunc glGetProgramivARBPtr;
        internal static void loadGetProgramivARB()
        {
            try
            {
                glGetProgramivARBPtr = (glGetProgramivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramivARB"), typeof(glGetProgramivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramivARB'.");
            }
        }
        public static void glGetProgramivARB(GLenum @target, GLenum @pname, GLint * @params) => glGetProgramivARBPtr(@target, @pname, @params);

        internal delegate void glGetProgramStringARBFunc(GLenum @target, GLenum @pname, void * @string);
        internal static glGetProgramStringARBFunc glGetProgramStringARBPtr;
        internal static void loadGetProgramStringARB()
        {
            try
            {
                glGetProgramStringARBPtr = (glGetProgramStringARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramStringARB"), typeof(glGetProgramStringARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramStringARB'.");
            }
        }
        public static void glGetProgramStringARB(GLenum @target, GLenum @pname, void * @string) => glGetProgramStringARBPtr(@target, @pname, @string);

        internal delegate GLboolean glIsProgramARBFunc(GLuint @program);
        internal static glIsProgramARBFunc glIsProgramARBPtr;
        internal static void loadIsProgramARB()
        {
            try
            {
                glIsProgramARBPtr = (glIsProgramARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsProgramARB"), typeof(glIsProgramARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsProgramARB'.");
            }
        }
        public static GLboolean glIsProgramARB(GLuint @program) => glIsProgramARBPtr(@program);
        #endregion
    }
}
