using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_viewport_array
    {
        #region Interop
        static GL_ARB_viewport_array()
        {
            Console.WriteLine("Initalising GL_ARB_viewport_array interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadViewportArrayv();
            loadViewportIndexedf();
            loadViewportIndexedfv();
            loadScissorArrayv();
            loadScissorIndexed();
            loadScissorIndexedv();
            loadDepthRangeArrayv();
            loadDepthRangeIndexed();
            loadGetFloati_v();
            loadGetDoublei_v();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SCISSOR_BOX = 0x0C10;
        public static UInt32 GL_VIEWPORT = 0x0BA2;
        public static UInt32 GL_DEPTH_RANGE = 0x0B70;
        public static UInt32 GL_SCISSOR_TEST = 0x0C11;
        public static UInt32 GL_MAX_VIEWPORTS = 0x825B;
        public static UInt32 GL_VIEWPORT_SUBPIXEL_BITS = 0x825C;
        public static UInt32 GL_VIEWPORT_BOUNDS_RANGE = 0x825D;
        public static UInt32 GL_LAYER_PROVOKING_VERTEX = 0x825E;
        public static UInt32 GL_VIEWPORT_INDEX_PROVOKING_VERTEX = 0x825F;
        public static UInt32 GL_UNDEFINED_VERTEX = 0x8260;
        public static UInt32 GL_FIRST_VERTEX_CONVENTION = 0x8E4D;
        public static UInt32 GL_LAST_VERTEX_CONVENTION = 0x8E4E;
        public static UInt32 GL_PROVOKING_VERTEX = 0x8E4F;
        #endregion

        #region Commands
        internal delegate void glViewportArrayvFunc(GLuint @first, GLsizei @count, const GLfloat * @v);
        internal static glViewportArrayvFunc glViewportArrayvPtr;
        internal static void loadViewportArrayv()
        {
            try
            {
                glViewportArrayvPtr = (glViewportArrayvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glViewportArrayv"), typeof(glViewportArrayvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glViewportArrayv'.");
            }
        }
        public static void glViewportArrayv(GLuint @first, GLsizei @count, const GLfloat * @v) => glViewportArrayvPtr(@first, @count, @v);

        internal delegate void glViewportIndexedfFunc(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @w, GLfloat @h);
        internal static glViewportIndexedfFunc glViewportIndexedfPtr;
        internal static void loadViewportIndexedf()
        {
            try
            {
                glViewportIndexedfPtr = (glViewportIndexedfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glViewportIndexedf"), typeof(glViewportIndexedfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glViewportIndexedf'.");
            }
        }
        public static void glViewportIndexedf(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @w, GLfloat @h) => glViewportIndexedfPtr(@index, @x, @y, @w, @h);

        internal delegate void glViewportIndexedfvFunc(GLuint @index, const GLfloat * @v);
        internal static glViewportIndexedfvFunc glViewportIndexedfvPtr;
        internal static void loadViewportIndexedfv()
        {
            try
            {
                glViewportIndexedfvPtr = (glViewportIndexedfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glViewportIndexedfv"), typeof(glViewportIndexedfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glViewportIndexedfv'.");
            }
        }
        public static void glViewportIndexedfv(GLuint @index, const GLfloat * @v) => glViewportIndexedfvPtr(@index, @v);

        internal delegate void glScissorArrayvFunc(GLuint @first, GLsizei @count, const GLint * @v);
        internal static glScissorArrayvFunc glScissorArrayvPtr;
        internal static void loadScissorArrayv()
        {
            try
            {
                glScissorArrayvPtr = (glScissorArrayvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScissorArrayv"), typeof(glScissorArrayvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScissorArrayv'.");
            }
        }
        public static void glScissorArrayv(GLuint @first, GLsizei @count, const GLint * @v) => glScissorArrayvPtr(@first, @count, @v);

        internal delegate void glScissorIndexedFunc(GLuint @index, GLint @left, GLint @bottom, GLsizei @width, GLsizei @height);
        internal static glScissorIndexedFunc glScissorIndexedPtr;
        internal static void loadScissorIndexed()
        {
            try
            {
                glScissorIndexedPtr = (glScissorIndexedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScissorIndexed"), typeof(glScissorIndexedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScissorIndexed'.");
            }
        }
        public static void glScissorIndexed(GLuint @index, GLint @left, GLint @bottom, GLsizei @width, GLsizei @height) => glScissorIndexedPtr(@index, @left, @bottom, @width, @height);

        internal delegate void glScissorIndexedvFunc(GLuint @index, const GLint * @v);
        internal static glScissorIndexedvFunc glScissorIndexedvPtr;
        internal static void loadScissorIndexedv()
        {
            try
            {
                glScissorIndexedvPtr = (glScissorIndexedvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScissorIndexedv"), typeof(glScissorIndexedvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScissorIndexedv'.");
            }
        }
        public static void glScissorIndexedv(GLuint @index, const GLint * @v) => glScissorIndexedvPtr(@index, @v);

        internal delegate void glDepthRangeArrayvFunc(GLuint @first, GLsizei @count, const GLdouble * @v);
        internal static glDepthRangeArrayvFunc glDepthRangeArrayvPtr;
        internal static void loadDepthRangeArrayv()
        {
            try
            {
                glDepthRangeArrayvPtr = (glDepthRangeArrayvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangeArrayv"), typeof(glDepthRangeArrayvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangeArrayv'.");
            }
        }
        public static void glDepthRangeArrayv(GLuint @first, GLsizei @count, const GLdouble * @v) => glDepthRangeArrayvPtr(@first, @count, @v);

        internal delegate void glDepthRangeIndexedFunc(GLuint @index, GLdouble @n, GLdouble @f);
        internal static glDepthRangeIndexedFunc glDepthRangeIndexedPtr;
        internal static void loadDepthRangeIndexed()
        {
            try
            {
                glDepthRangeIndexedPtr = (glDepthRangeIndexedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangeIndexed"), typeof(glDepthRangeIndexedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangeIndexed'.");
            }
        }
        public static void glDepthRangeIndexed(GLuint @index, GLdouble @n, GLdouble @f) => glDepthRangeIndexedPtr(@index, @n, @f);

        internal delegate void glGetFloati_vFunc(GLenum @target, GLuint @index, GLfloat * @data);
        internal static glGetFloati_vFunc glGetFloati_vPtr;
        internal static void loadGetFloati_v()
        {
            try
            {
                glGetFloati_vPtr = (glGetFloati_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFloati_v"), typeof(glGetFloati_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFloati_v'.");
            }
        }
        public static void glGetFloati_v(GLenum @target, GLuint @index, GLfloat * @data) => glGetFloati_vPtr(@target, @index, @data);

        internal delegate void glGetDoublei_vFunc(GLenum @target, GLuint @index, GLdouble * @data);
        internal static glGetDoublei_vFunc glGetDoublei_vPtr;
        internal static void loadGetDoublei_v()
        {
            try
            {
                glGetDoublei_vPtr = (glGetDoublei_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDoublei_v"), typeof(glGetDoublei_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDoublei_v'.");
            }
        }
        public static void glGetDoublei_v(GLenum @target, GLuint @index, GLdouble * @data) => glGetDoublei_vPtr(@target, @index, @data);
        #endregion
    }
}
