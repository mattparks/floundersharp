using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_sparse_texture
    {
        #region Interop
        static GL_ARB_sparse_texture()
        {
            Console.WriteLine("Initalising GL_ARB_sparse_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexPageCommitmentARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_SPARSE_ARB = 0x91A6;
        public static UInt32 GL_VIRTUAL_PAGE_SIZE_INDEX_ARB = 0x91A7;
        public static UInt32 GL_NUM_SPARSE_LEVELS_ARB = 0x91AA;
        public static UInt32 GL_NUM_VIRTUAL_PAGE_SIZES_ARB = 0x91A8;
        public static UInt32 GL_VIRTUAL_PAGE_SIZE_X_ARB = 0x9195;
        public static UInt32 GL_VIRTUAL_PAGE_SIZE_Y_ARB = 0x9196;
        public static UInt32 GL_VIRTUAL_PAGE_SIZE_Z_ARB = 0x9197;
        public static UInt32 GL_MAX_SPARSE_TEXTURE_SIZE_ARB = 0x9198;
        public static UInt32 GL_MAX_SPARSE_3D_TEXTURE_SIZE_ARB = 0x9199;
        public static UInt32 GL_MAX_SPARSE_ARRAY_TEXTURE_LAYERS_ARB = 0x919A;
        public static UInt32 GL_SPARSE_TEXTURE_FULL_ARRAY_CUBE_MIPMAPS_ARB = 0x91A9;
        #endregion

        #region Commands
        internal delegate void glTexPageCommitmentARBFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @commit);
        internal static glTexPageCommitmentARBFunc glTexPageCommitmentARBPtr;
        internal static void loadTexPageCommitmentARB()
        {
            try
            {
                glTexPageCommitmentARBPtr = (glTexPageCommitmentARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexPageCommitmentARB"), typeof(glTexPageCommitmentARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexPageCommitmentARB'.");
            }
        }
        public static void glTexPageCommitmentARB(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @commit) => glTexPageCommitmentARBPtr(@target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @commit);
        #endregion
    }
}
