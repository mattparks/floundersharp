using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_ES3_1_compatibility
    {
        #region Interop
        static GL_ARB_ES3_1_compatibility()
        {
            Console.WriteLine("Initalising GL_ARB_ES3_1_compatibility interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMemoryBarrierByRegion();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BACK = 0x0405;
        #endregion

        #region Commands
        internal delegate void glMemoryBarrierByRegionFunc(GLbitfield @barriers);
        internal static glMemoryBarrierByRegionFunc glMemoryBarrierByRegionPtr;
        internal static void loadMemoryBarrierByRegion()
        {
            try
            {
                glMemoryBarrierByRegionPtr = (glMemoryBarrierByRegionFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMemoryBarrierByRegion"), typeof(glMemoryBarrierByRegionFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMemoryBarrierByRegion'.");
            }
        }
        public static void glMemoryBarrierByRegion(GLbitfield @barriers) => glMemoryBarrierByRegionPtr(@barriers);
        #endregion
    }
}
