using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_pipeline_statistics_query
    {
        #region Interop
        static GL_ARB_pipeline_statistics_query()
        {
            Console.WriteLine("Initalising GL_ARB_pipeline_statistics_query interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTICES_SUBMITTED_ARB = 0x82EE;
        public static UInt32 GL_PRIMITIVES_SUBMITTED_ARB = 0x82EF;
        public static UInt32 GL_VERTEX_SHADER_INVOCATIONS_ARB = 0x82F0;
        public static UInt32 GL_TESS_CONTROL_SHADER_PATCHES_ARB = 0x82F1;
        public static UInt32 GL_TESS_EVALUATION_SHADER_INVOCATIONS_ARB = 0x82F2;
        public static UInt32 GL_GEOMETRY_SHADER_INVOCATIONS = 0x887F;
        public static UInt32 GL_GEOMETRY_SHADER_PRIMITIVES_EMITTED_ARB = 0x82F3;
        public static UInt32 GL_FRAGMENT_SHADER_INVOCATIONS_ARB = 0x82F4;
        public static UInt32 GL_COMPUTE_SHADER_INVOCATIONS_ARB = 0x82F5;
        public static UInt32 GL_CLIPPING_INPUT_PRIMITIVES_ARB = 0x82F6;
        public static UInt32 GL_CLIPPING_OUTPUT_PRIMITIVES_ARB = 0x82F7;
        #endregion

        #region Commands
        #endregion
    }
}
