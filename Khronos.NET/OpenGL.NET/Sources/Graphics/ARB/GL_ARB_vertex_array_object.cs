using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_vertex_array_object
    {
        #region Interop
        static GL_ARB_vertex_array_object()
        {
            Console.WriteLine("Initalising GL_ARB_vertex_array_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindVertexArray();
            loadDeleteVertexArrays();
            loadGenVertexArrays();
            loadIsVertexArray();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ARRAY_BINDING = 0x85B5;
        #endregion

        #region Commands
        internal delegate void glBindVertexArrayFunc(GLuint @array);
        internal static glBindVertexArrayFunc glBindVertexArrayPtr;
        internal static void loadBindVertexArray()
        {
            try
            {
                glBindVertexArrayPtr = (glBindVertexArrayFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVertexArray"), typeof(glBindVertexArrayFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVertexArray'.");
            }
        }
        public static void glBindVertexArray(GLuint @array) => glBindVertexArrayPtr(@array);

        internal delegate void glDeleteVertexArraysFunc(GLsizei @n, const GLuint * @arrays);
        internal static glDeleteVertexArraysFunc glDeleteVertexArraysPtr;
        internal static void loadDeleteVertexArrays()
        {
            try
            {
                glDeleteVertexArraysPtr = (glDeleteVertexArraysFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteVertexArrays"), typeof(glDeleteVertexArraysFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteVertexArrays'.");
            }
        }
        public static void glDeleteVertexArrays(GLsizei @n, const GLuint * @arrays) => glDeleteVertexArraysPtr(@n, @arrays);

        internal delegate void glGenVertexArraysFunc(GLsizei @n, GLuint * @arrays);
        internal static glGenVertexArraysFunc glGenVertexArraysPtr;
        internal static void loadGenVertexArrays()
        {
            try
            {
                glGenVertexArraysPtr = (glGenVertexArraysFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenVertexArrays"), typeof(glGenVertexArraysFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenVertexArrays'.");
            }
        }
        public static void glGenVertexArrays(GLsizei @n, GLuint * @arrays) => glGenVertexArraysPtr(@n, @arrays);

        internal delegate GLboolean glIsVertexArrayFunc(GLuint @array);
        internal static glIsVertexArrayFunc glIsVertexArrayPtr;
        internal static void loadIsVertexArray()
        {
            try
            {
                glIsVertexArrayPtr = (glIsVertexArrayFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsVertexArray"), typeof(glIsVertexArrayFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsVertexArray'.");
            }
        }
        public static GLboolean glIsVertexArray(GLuint @array) => glIsVertexArrayPtr(@array);
        #endregion
    }
}
