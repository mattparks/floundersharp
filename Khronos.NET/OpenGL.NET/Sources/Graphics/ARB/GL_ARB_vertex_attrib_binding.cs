using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_vertex_attrib_binding
    {
        #region Interop
        static GL_ARB_vertex_attrib_binding()
        {
            Console.WriteLine("Initalising GL_ARB_vertex_attrib_binding interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindVertexBuffer();
            loadVertexAttribFormat();
            loadVertexAttribIFormat();
            loadVertexAttribLFormat();
            loadVertexAttribBinding();
            loadVertexBindingDivisor();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ATTRIB_BINDING = 0x82D4;
        public static UInt32 GL_VERTEX_ATTRIB_RELATIVE_OFFSET = 0x82D5;
        public static UInt32 GL_VERTEX_BINDING_DIVISOR = 0x82D6;
        public static UInt32 GL_VERTEX_BINDING_OFFSET = 0x82D7;
        public static UInt32 GL_VERTEX_BINDING_STRIDE = 0x82D8;
        public static UInt32 GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET = 0x82D9;
        public static UInt32 GL_MAX_VERTEX_ATTRIB_BINDINGS = 0x82DA;
        #endregion

        #region Commands
        internal delegate void glBindVertexBufferFunc(GLuint @bindingindex, GLuint @buffer, GLintptr @offset, GLsizei @stride);
        internal static glBindVertexBufferFunc glBindVertexBufferPtr;
        internal static void loadBindVertexBuffer()
        {
            try
            {
                glBindVertexBufferPtr = (glBindVertexBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVertexBuffer"), typeof(glBindVertexBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVertexBuffer'.");
            }
        }
        public static void glBindVertexBuffer(GLuint @bindingindex, GLuint @buffer, GLintptr @offset, GLsizei @stride) => glBindVertexBufferPtr(@bindingindex, @buffer, @offset, @stride);

        internal delegate void glVertexAttribFormatFunc(GLuint @attribindex, GLint @size, GLenum @type, GLboolean @normalized, GLuint @relativeoffset);
        internal static glVertexAttribFormatFunc glVertexAttribFormatPtr;
        internal static void loadVertexAttribFormat()
        {
            try
            {
                glVertexAttribFormatPtr = (glVertexAttribFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribFormat"), typeof(glVertexAttribFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribFormat'.");
            }
        }
        public static void glVertexAttribFormat(GLuint @attribindex, GLint @size, GLenum @type, GLboolean @normalized, GLuint @relativeoffset) => glVertexAttribFormatPtr(@attribindex, @size, @type, @normalized, @relativeoffset);

        internal delegate void glVertexAttribIFormatFunc(GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset);
        internal static glVertexAttribIFormatFunc glVertexAttribIFormatPtr;
        internal static void loadVertexAttribIFormat()
        {
            try
            {
                glVertexAttribIFormatPtr = (glVertexAttribIFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribIFormat"), typeof(glVertexAttribIFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribIFormat'.");
            }
        }
        public static void glVertexAttribIFormat(GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset) => glVertexAttribIFormatPtr(@attribindex, @size, @type, @relativeoffset);

        internal delegate void glVertexAttribLFormatFunc(GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset);
        internal static glVertexAttribLFormatFunc glVertexAttribLFormatPtr;
        internal static void loadVertexAttribLFormat()
        {
            try
            {
                glVertexAttribLFormatPtr = (glVertexAttribLFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribLFormat"), typeof(glVertexAttribLFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribLFormat'.");
            }
        }
        public static void glVertexAttribLFormat(GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset) => glVertexAttribLFormatPtr(@attribindex, @size, @type, @relativeoffset);

        internal delegate void glVertexAttribBindingFunc(GLuint @attribindex, GLuint @bindingindex);
        internal static glVertexAttribBindingFunc glVertexAttribBindingPtr;
        internal static void loadVertexAttribBinding()
        {
            try
            {
                glVertexAttribBindingPtr = (glVertexAttribBindingFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribBinding"), typeof(glVertexAttribBindingFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribBinding'.");
            }
        }
        public static void glVertexAttribBinding(GLuint @attribindex, GLuint @bindingindex) => glVertexAttribBindingPtr(@attribindex, @bindingindex);

        internal delegate void glVertexBindingDivisorFunc(GLuint @bindingindex, GLuint @divisor);
        internal static glVertexBindingDivisorFunc glVertexBindingDivisorPtr;
        internal static void loadVertexBindingDivisor()
        {
            try
            {
                glVertexBindingDivisorPtr = (glVertexBindingDivisorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexBindingDivisor"), typeof(glVertexBindingDivisorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexBindingDivisor'.");
            }
        }
        public static void glVertexBindingDivisor(GLuint @bindingindex, GLuint @divisor) => glVertexBindingDivisorPtr(@bindingindex, @divisor);
        #endregion
    }
}
