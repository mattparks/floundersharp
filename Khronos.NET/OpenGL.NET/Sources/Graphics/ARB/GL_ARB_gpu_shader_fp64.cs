using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_gpu_shader_fp64
    {
        #region Interop
        static GL_ARB_gpu_shader_fp64()
        {
            Console.WriteLine("Initalising GL_ARB_gpu_shader_fp64 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadUniform1d();
            loadUniform2d();
            loadUniform3d();
            loadUniform4d();
            loadUniform1dv();
            loadUniform2dv();
            loadUniform3dv();
            loadUniform4dv();
            loadUniformMatrix2dv();
            loadUniformMatrix3dv();
            loadUniformMatrix4dv();
            loadUniformMatrix2x3dv();
            loadUniformMatrix2x4dv();
            loadUniformMatrix3x2dv();
            loadUniformMatrix3x4dv();
            loadUniformMatrix4x2dv();
            loadUniformMatrix4x3dv();
            loadGetUniformdv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_DOUBLE = 0x140A;
        public static UInt32 GL_DOUBLE_VEC2 = 0x8FFC;
        public static UInt32 GL_DOUBLE_VEC3 = 0x8FFD;
        public static UInt32 GL_DOUBLE_VEC4 = 0x8FFE;
        public static UInt32 GL_DOUBLE_MAT2 = 0x8F46;
        public static UInt32 GL_DOUBLE_MAT3 = 0x8F47;
        public static UInt32 GL_DOUBLE_MAT4 = 0x8F48;
        public static UInt32 GL_DOUBLE_MAT2x3 = 0x8F49;
        public static UInt32 GL_DOUBLE_MAT2x4 = 0x8F4A;
        public static UInt32 GL_DOUBLE_MAT3x2 = 0x8F4B;
        public static UInt32 GL_DOUBLE_MAT3x4 = 0x8F4C;
        public static UInt32 GL_DOUBLE_MAT4x2 = 0x8F4D;
        public static UInt32 GL_DOUBLE_MAT4x3 = 0x8F4E;
        #endregion

        #region Commands
        internal delegate void glUniform1dFunc(GLint @location, GLdouble @x);
        internal static glUniform1dFunc glUniform1dPtr;
        internal static void loadUniform1d()
        {
            try
            {
                glUniform1dPtr = (glUniform1dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1d"), typeof(glUniform1dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1d'.");
            }
        }
        public static void glUniform1d(GLint @location, GLdouble @x) => glUniform1dPtr(@location, @x);

        internal delegate void glUniform2dFunc(GLint @location, GLdouble @x, GLdouble @y);
        internal static glUniform2dFunc glUniform2dPtr;
        internal static void loadUniform2d()
        {
            try
            {
                glUniform2dPtr = (glUniform2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2d"), typeof(glUniform2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2d'.");
            }
        }
        public static void glUniform2d(GLint @location, GLdouble @x, GLdouble @y) => glUniform2dPtr(@location, @x, @y);

        internal delegate void glUniform3dFunc(GLint @location, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glUniform3dFunc glUniform3dPtr;
        internal static void loadUniform3d()
        {
            try
            {
                glUniform3dPtr = (glUniform3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3d"), typeof(glUniform3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3d'.");
            }
        }
        public static void glUniform3d(GLint @location, GLdouble @x, GLdouble @y, GLdouble @z) => glUniform3dPtr(@location, @x, @y, @z);

        internal delegate void glUniform4dFunc(GLint @location, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glUniform4dFunc glUniform4dPtr;
        internal static void loadUniform4d()
        {
            try
            {
                glUniform4dPtr = (glUniform4dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4d"), typeof(glUniform4dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4d'.");
            }
        }
        public static void glUniform4d(GLint @location, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glUniform4dPtr(@location, @x, @y, @z, @w);

        internal delegate void glUniform1dvFunc(GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glUniform1dvFunc glUniform1dvPtr;
        internal static void loadUniform1dv()
        {
            try
            {
                glUniform1dvPtr = (glUniform1dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1dv"), typeof(glUniform1dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1dv'.");
            }
        }
        public static void glUniform1dv(GLint @location, GLsizei @count, const GLdouble * @value) => glUniform1dvPtr(@location, @count, @value);

        internal delegate void glUniform2dvFunc(GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glUniform2dvFunc glUniform2dvPtr;
        internal static void loadUniform2dv()
        {
            try
            {
                glUniform2dvPtr = (glUniform2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2dv"), typeof(glUniform2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2dv'.");
            }
        }
        public static void glUniform2dv(GLint @location, GLsizei @count, const GLdouble * @value) => glUniform2dvPtr(@location, @count, @value);

        internal delegate void glUniform3dvFunc(GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glUniform3dvFunc glUniform3dvPtr;
        internal static void loadUniform3dv()
        {
            try
            {
                glUniform3dvPtr = (glUniform3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3dv"), typeof(glUniform3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3dv'.");
            }
        }
        public static void glUniform3dv(GLint @location, GLsizei @count, const GLdouble * @value) => glUniform3dvPtr(@location, @count, @value);

        internal delegate void glUniform4dvFunc(GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glUniform4dvFunc glUniform4dvPtr;
        internal static void loadUniform4dv()
        {
            try
            {
                glUniform4dvPtr = (glUniform4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4dv"), typeof(glUniform4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4dv'.");
            }
        }
        public static void glUniform4dv(GLint @location, GLsizei @count, const GLdouble * @value) => glUniform4dvPtr(@location, @count, @value);

        internal delegate void glUniformMatrix2dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix2dvFunc glUniformMatrix2dvPtr;
        internal static void loadUniformMatrix2dv()
        {
            try
            {
                glUniformMatrix2dvPtr = (glUniformMatrix2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2dv"), typeof(glUniformMatrix2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2dv'.");
            }
        }
        public static void glUniformMatrix2dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix2dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix3dvFunc glUniformMatrix3dvPtr;
        internal static void loadUniformMatrix3dv()
        {
            try
            {
                glUniformMatrix3dvPtr = (glUniformMatrix3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3dv"), typeof(glUniformMatrix3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3dv'.");
            }
        }
        public static void glUniformMatrix3dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix3dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix4dvFunc glUniformMatrix4dvPtr;
        internal static void loadUniformMatrix4dv()
        {
            try
            {
                glUniformMatrix4dvPtr = (glUniformMatrix4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4dv"), typeof(glUniformMatrix4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4dv'.");
            }
        }
        public static void glUniformMatrix4dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix4dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix2x3dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix2x3dvFunc glUniformMatrix2x3dvPtr;
        internal static void loadUniformMatrix2x3dv()
        {
            try
            {
                glUniformMatrix2x3dvPtr = (glUniformMatrix2x3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2x3dv"), typeof(glUniformMatrix2x3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2x3dv'.");
            }
        }
        public static void glUniformMatrix2x3dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix2x3dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix2x4dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix2x4dvFunc glUniformMatrix2x4dvPtr;
        internal static void loadUniformMatrix2x4dv()
        {
            try
            {
                glUniformMatrix2x4dvPtr = (glUniformMatrix2x4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2x4dv"), typeof(glUniformMatrix2x4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2x4dv'.");
            }
        }
        public static void glUniformMatrix2x4dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix2x4dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3x2dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix3x2dvFunc glUniformMatrix3x2dvPtr;
        internal static void loadUniformMatrix3x2dv()
        {
            try
            {
                glUniformMatrix3x2dvPtr = (glUniformMatrix3x2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3x2dv"), typeof(glUniformMatrix3x2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3x2dv'.");
            }
        }
        public static void glUniformMatrix3x2dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix3x2dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3x4dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix3x4dvFunc glUniformMatrix3x4dvPtr;
        internal static void loadUniformMatrix3x4dv()
        {
            try
            {
                glUniformMatrix3x4dvPtr = (glUniformMatrix3x4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3x4dv"), typeof(glUniformMatrix3x4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3x4dv'.");
            }
        }
        public static void glUniformMatrix3x4dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix3x4dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4x2dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix4x2dvFunc glUniformMatrix4x2dvPtr;
        internal static void loadUniformMatrix4x2dv()
        {
            try
            {
                glUniformMatrix4x2dvPtr = (glUniformMatrix4x2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4x2dv"), typeof(glUniformMatrix4x2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4x2dv'.");
            }
        }
        public static void glUniformMatrix4x2dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix4x2dvPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4x3dvFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glUniformMatrix4x3dvFunc glUniformMatrix4x3dvPtr;
        internal static void loadUniformMatrix4x3dv()
        {
            try
            {
                glUniformMatrix4x3dvPtr = (glUniformMatrix4x3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4x3dv"), typeof(glUniformMatrix4x3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4x3dv'.");
            }
        }
        public static void glUniformMatrix4x3dv(GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glUniformMatrix4x3dvPtr(@location, @count, @transpose, @value);

        internal delegate void glGetUniformdvFunc(GLuint @program, GLint @location, GLdouble * @params);
        internal static glGetUniformdvFunc glGetUniformdvPtr;
        internal static void loadGetUniformdv()
        {
            try
            {
                glGetUniformdvPtr = (glGetUniformdvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformdv"), typeof(glGetUniformdvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformdv'.");
            }
        }
        public static void glGetUniformdv(GLuint @program, GLint @location, GLdouble * @params) => glGetUniformdvPtr(@program, @location, @params);
        #endregion
    }
}
