using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_explicit_uniform_location
    {
        #region Interop
        static GL_ARB_explicit_uniform_location()
        {
            Console.WriteLine("Initalising GL_ARB_explicit_uniform_location interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_UNIFORM_LOCATIONS = 0x826E;
        #endregion

        #region Commands
        #endregion
    }
}
