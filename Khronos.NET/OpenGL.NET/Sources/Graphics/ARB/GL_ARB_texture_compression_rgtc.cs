using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_compression_rgtc
    {
        #region Interop
        static GL_ARB_texture_compression_rgtc()
        {
            Console.WriteLine("Initalising GL_ARB_texture_compression_rgtc interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_RED_RGTC1 = 0x8DBB;
        public static UInt32 GL_COMPRESSED_SIGNED_RED_RGTC1 = 0x8DBC;
        public static UInt32 GL_COMPRESSED_RG_RGTC2 = 0x8DBD;
        public static UInt32 GL_COMPRESSED_SIGNED_RG_RGTC2 = 0x8DBE;
        #endregion

        #region Commands
        #endregion
    }
}
