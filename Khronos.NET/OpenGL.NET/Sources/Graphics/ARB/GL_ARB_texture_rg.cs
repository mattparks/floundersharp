using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_rg
    {
        #region Interop
        static GL_ARB_texture_rg()
        {
            Console.WriteLine("Initalising GL_ARB_texture_rg interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RG = 0x8227;
        public static UInt32 GL_RG_INTEGER = 0x8228;
        public static UInt32 GL_R8 = 0x8229;
        public static UInt32 GL_R16 = 0x822A;
        public static UInt32 GL_RG8 = 0x822B;
        public static UInt32 GL_RG16 = 0x822C;
        public static UInt32 GL_R16F = 0x822D;
        public static UInt32 GL_R32F = 0x822E;
        public static UInt32 GL_RG16F = 0x822F;
        public static UInt32 GL_RG32F = 0x8230;
        public static UInt32 GL_R8I = 0x8231;
        public static UInt32 GL_R8UI = 0x8232;
        public static UInt32 GL_R16I = 0x8233;
        public static UInt32 GL_R16UI = 0x8234;
        public static UInt32 GL_R32I = 0x8235;
        public static UInt32 GL_R32UI = 0x8236;
        public static UInt32 GL_RG8I = 0x8237;
        public static UInt32 GL_RG8UI = 0x8238;
        public static UInt32 GL_RG16I = 0x8239;
        public static UInt32 GL_RG16UI = 0x823A;
        public static UInt32 GL_RG32I = 0x823B;
        public static UInt32 GL_RG32UI = 0x823C;
        #endregion

        #region Commands
        #endregion
    }
}
