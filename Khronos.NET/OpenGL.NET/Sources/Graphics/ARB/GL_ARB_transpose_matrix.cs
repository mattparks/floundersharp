using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_transpose_matrix
    {
        #region Interop
        static GL_ARB_transpose_matrix()
        {
            Console.WriteLine("Initalising GL_ARB_transpose_matrix interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadLoadTransposeMatrixfARB();
            loadLoadTransposeMatrixdARB();
            loadMultTransposeMatrixfARB();
            loadMultTransposeMatrixdARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TRANSPOSE_MODELVIEW_MATRIX_ARB = 0x84E3;
        public static UInt32 GL_TRANSPOSE_PROJECTION_MATRIX_ARB = 0x84E4;
        public static UInt32 GL_TRANSPOSE_TEXTURE_MATRIX_ARB = 0x84E5;
        public static UInt32 GL_TRANSPOSE_COLOR_MATRIX_ARB = 0x84E6;
        #endregion

        #region Commands
        internal delegate void glLoadTransposeMatrixfARBFunc(const GLfloat * @m);
        internal static glLoadTransposeMatrixfARBFunc glLoadTransposeMatrixfARBPtr;
        internal static void loadLoadTransposeMatrixfARB()
        {
            try
            {
                glLoadTransposeMatrixfARBPtr = (glLoadTransposeMatrixfARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadTransposeMatrixfARB"), typeof(glLoadTransposeMatrixfARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadTransposeMatrixfARB'.");
            }
        }
        public static void glLoadTransposeMatrixfARB(const GLfloat * @m) => glLoadTransposeMatrixfARBPtr(@m);

        internal delegate void glLoadTransposeMatrixdARBFunc(const GLdouble * @m);
        internal static glLoadTransposeMatrixdARBFunc glLoadTransposeMatrixdARBPtr;
        internal static void loadLoadTransposeMatrixdARB()
        {
            try
            {
                glLoadTransposeMatrixdARBPtr = (glLoadTransposeMatrixdARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadTransposeMatrixdARB"), typeof(glLoadTransposeMatrixdARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadTransposeMatrixdARB'.");
            }
        }
        public static void glLoadTransposeMatrixdARB(const GLdouble * @m) => glLoadTransposeMatrixdARBPtr(@m);

        internal delegate void glMultTransposeMatrixfARBFunc(const GLfloat * @m);
        internal static glMultTransposeMatrixfARBFunc glMultTransposeMatrixfARBPtr;
        internal static void loadMultTransposeMatrixfARB()
        {
            try
            {
                glMultTransposeMatrixfARBPtr = (glMultTransposeMatrixfARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultTransposeMatrixfARB"), typeof(glMultTransposeMatrixfARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultTransposeMatrixfARB'.");
            }
        }
        public static void glMultTransposeMatrixfARB(const GLfloat * @m) => glMultTransposeMatrixfARBPtr(@m);

        internal delegate void glMultTransposeMatrixdARBFunc(const GLdouble * @m);
        internal static glMultTransposeMatrixdARBFunc glMultTransposeMatrixdARBPtr;
        internal static void loadMultTransposeMatrixdARB()
        {
            try
            {
                glMultTransposeMatrixdARBPtr = (glMultTransposeMatrixdARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultTransposeMatrixdARB"), typeof(glMultTransposeMatrixdARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultTransposeMatrixdARB'.");
            }
        }
        public static void glMultTransposeMatrixdARB(const GLdouble * @m) => glMultTransposeMatrixdARBPtr(@m);
        #endregion
    }
}
