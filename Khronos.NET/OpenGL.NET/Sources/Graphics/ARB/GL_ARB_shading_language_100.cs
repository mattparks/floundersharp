using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_shading_language_100
    {
        #region Interop
        static GL_ARB_shading_language_100()
        {
            Console.WriteLine("Initalising GL_ARB_shading_language_100 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_SHADING_LANGUAGE_VERSION_ARB = 0x8B8C;
        #endregion

        #region Commands
        #endregion
    }
}
