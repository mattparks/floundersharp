using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_env_combine
    {
        #region Interop
        static GL_ARB_texture_env_combine()
        {
            Console.WriteLine("Initalising GL_ARB_texture_env_combine interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMBINE_ARB = 0x8570;
        public static UInt32 GL_COMBINE_RGB_ARB = 0x8571;
        public static UInt32 GL_COMBINE_ALPHA_ARB = 0x8572;
        public static UInt32 GL_SOURCE0_RGB_ARB = 0x8580;
        public static UInt32 GL_SOURCE1_RGB_ARB = 0x8581;
        public static UInt32 GL_SOURCE2_RGB_ARB = 0x8582;
        public static UInt32 GL_SOURCE0_ALPHA_ARB = 0x8588;
        public static UInt32 GL_SOURCE1_ALPHA_ARB = 0x8589;
        public static UInt32 GL_SOURCE2_ALPHA_ARB = 0x858A;
        public static UInt32 GL_OPERAND0_RGB_ARB = 0x8590;
        public static UInt32 GL_OPERAND1_RGB_ARB = 0x8591;
        public static UInt32 GL_OPERAND2_RGB_ARB = 0x8592;
        public static UInt32 GL_OPERAND0_ALPHA_ARB = 0x8598;
        public static UInt32 GL_OPERAND1_ALPHA_ARB = 0x8599;
        public static UInt32 GL_OPERAND2_ALPHA_ARB = 0x859A;
        public static UInt32 GL_RGB_SCALE_ARB = 0x8573;
        public static UInt32 GL_ADD_SIGNED_ARB = 0x8574;
        public static UInt32 GL_INTERPOLATE_ARB = 0x8575;
        public static UInt32 GL_SUBTRACT_ARB = 0x84E7;
        public static UInt32 GL_CONSTANT_ARB = 0x8576;
        public static UInt32 GL_PRIMARY_COLOR_ARB = 0x8577;
        public static UInt32 GL_PREVIOUS_ARB = 0x8578;
        #endregion

        #region Commands
        #endregion
    }
}
