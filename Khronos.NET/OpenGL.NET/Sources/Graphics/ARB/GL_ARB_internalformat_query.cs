using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_internalformat_query
    {
        #region Interop
        static GL_ARB_internalformat_query()
        {
            Console.WriteLine("Initalising GL_ARB_internalformat_query interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetInternalformativ();
        }
        #endregion

        #region Enums
        public static UInt32 GL_NUM_SAMPLE_COUNTS = 0x9380;
        #endregion

        #region Commands
        internal delegate void glGetInternalformativFunc(GLenum @target, GLenum @internalformat, GLenum @pname, GLsizei @bufSize, GLint * @params);
        internal static glGetInternalformativFunc glGetInternalformativPtr;
        internal static void loadGetInternalformativ()
        {
            try
            {
                glGetInternalformativPtr = (glGetInternalformativFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInternalformativ"), typeof(glGetInternalformativFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInternalformativ'.");
            }
        }
        public static void glGetInternalformativ(GLenum @target, GLenum @internalformat, GLenum @pname, GLsizei @bufSize, GLint * @params) => glGetInternalformativPtr(@target, @internalformat, @pname, @bufSize, @params);
        #endregion
    }
}
