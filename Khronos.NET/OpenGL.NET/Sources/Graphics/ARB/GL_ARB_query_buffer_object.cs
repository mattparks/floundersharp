using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_query_buffer_object
    {
        #region Interop
        static GL_ARB_query_buffer_object()
        {
            Console.WriteLine("Initalising GL_ARB_query_buffer_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_QUERY_BUFFER = 0x9192;
        public static UInt32 GL_QUERY_BUFFER_BARRIER_BIT = 0x00008000;
        public static UInt32 GL_QUERY_BUFFER_BINDING = 0x9193;
        public static UInt32 GL_QUERY_RESULT_NO_WAIT = 0x9194;
        #endregion

        #region Commands
        #endregion
    }
}
