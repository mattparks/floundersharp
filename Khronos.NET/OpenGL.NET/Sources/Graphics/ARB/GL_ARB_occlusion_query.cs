using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_occlusion_query
    {
        #region Interop
        static GL_ARB_occlusion_query()
        {
            Console.WriteLine("Initalising GL_ARB_occlusion_query interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGenQueriesARB();
            loadDeleteQueriesARB();
            loadIsQueryARB();
            loadBeginQueryARB();
            loadEndQueryARB();
            loadGetQueryivARB();
            loadGetQueryObjectivARB();
            loadGetQueryObjectuivARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_QUERY_COUNTER_BITS_ARB = 0x8864;
        public static UInt32 GL_CURRENT_QUERY_ARB = 0x8865;
        public static UInt32 GL_QUERY_RESULT_ARB = 0x8866;
        public static UInt32 GL_QUERY_RESULT_AVAILABLE_ARB = 0x8867;
        public static UInt32 GL_SAMPLES_PASSED_ARB = 0x8914;
        #endregion

        #region Commands
        internal delegate void glGenQueriesARBFunc(GLsizei @n, GLuint * @ids);
        internal static glGenQueriesARBFunc glGenQueriesARBPtr;
        internal static void loadGenQueriesARB()
        {
            try
            {
                glGenQueriesARBPtr = (glGenQueriesARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenQueriesARB"), typeof(glGenQueriesARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenQueriesARB'.");
            }
        }
        public static void glGenQueriesARB(GLsizei @n, GLuint * @ids) => glGenQueriesARBPtr(@n, @ids);

        internal delegate void glDeleteQueriesARBFunc(GLsizei @n, const GLuint * @ids);
        internal static glDeleteQueriesARBFunc glDeleteQueriesARBPtr;
        internal static void loadDeleteQueriesARB()
        {
            try
            {
                glDeleteQueriesARBPtr = (glDeleteQueriesARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteQueriesARB"), typeof(glDeleteQueriesARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteQueriesARB'.");
            }
        }
        public static void glDeleteQueriesARB(GLsizei @n, const GLuint * @ids) => glDeleteQueriesARBPtr(@n, @ids);

        internal delegate GLboolean glIsQueryARBFunc(GLuint @id);
        internal static glIsQueryARBFunc glIsQueryARBPtr;
        internal static void loadIsQueryARB()
        {
            try
            {
                glIsQueryARBPtr = (glIsQueryARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsQueryARB"), typeof(glIsQueryARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsQueryARB'.");
            }
        }
        public static GLboolean glIsQueryARB(GLuint @id) => glIsQueryARBPtr(@id);

        internal delegate void glBeginQueryARBFunc(GLenum @target, GLuint @id);
        internal static glBeginQueryARBFunc glBeginQueryARBPtr;
        internal static void loadBeginQueryARB()
        {
            try
            {
                glBeginQueryARBPtr = (glBeginQueryARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginQueryARB"), typeof(glBeginQueryARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginQueryARB'.");
            }
        }
        public static void glBeginQueryARB(GLenum @target, GLuint @id) => glBeginQueryARBPtr(@target, @id);

        internal delegate void glEndQueryARBFunc(GLenum @target);
        internal static glEndQueryARBFunc glEndQueryARBPtr;
        internal static void loadEndQueryARB()
        {
            try
            {
                glEndQueryARBPtr = (glEndQueryARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndQueryARB"), typeof(glEndQueryARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndQueryARB'.");
            }
        }
        public static void glEndQueryARB(GLenum @target) => glEndQueryARBPtr(@target);

        internal delegate void glGetQueryivARBFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetQueryivARBFunc glGetQueryivARBPtr;
        internal static void loadGetQueryivARB()
        {
            try
            {
                glGetQueryivARBPtr = (glGetQueryivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryivARB"), typeof(glGetQueryivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryivARB'.");
            }
        }
        public static void glGetQueryivARB(GLenum @target, GLenum @pname, GLint * @params) => glGetQueryivARBPtr(@target, @pname, @params);

        internal delegate void glGetQueryObjectivARBFunc(GLuint @id, GLenum @pname, GLint * @params);
        internal static glGetQueryObjectivARBFunc glGetQueryObjectivARBPtr;
        internal static void loadGetQueryObjectivARB()
        {
            try
            {
                glGetQueryObjectivARBPtr = (glGetQueryObjectivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjectivARB"), typeof(glGetQueryObjectivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjectivARB'.");
            }
        }
        public static void glGetQueryObjectivARB(GLuint @id, GLenum @pname, GLint * @params) => glGetQueryObjectivARBPtr(@id, @pname, @params);

        internal delegate void glGetQueryObjectuivARBFunc(GLuint @id, GLenum @pname, GLuint * @params);
        internal static glGetQueryObjectuivARBFunc glGetQueryObjectuivARBPtr;
        internal static void loadGetQueryObjectuivARB()
        {
            try
            {
                glGetQueryObjectuivARBPtr = (glGetQueryObjectuivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjectuivARB"), typeof(glGetQueryObjectuivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjectuivARB'.");
            }
        }
        public static void glGetQueryObjectuivARB(GLuint @id, GLenum @pname, GLuint * @params) => glGetQueryObjectuivARBPtr(@id, @pname, @params);
        #endregion
    }
}
