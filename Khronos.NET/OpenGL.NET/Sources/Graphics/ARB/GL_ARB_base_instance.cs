using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_base_instance
    {
        #region Interop
        static GL_ARB_base_instance()
        {
            Console.WriteLine("Initalising GL_ARB_base_instance interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawArraysInstancedBaseInstance();
            loadDrawElementsInstancedBaseInstance();
            loadDrawElementsInstancedBaseVertexBaseInstance();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glDrawArraysInstancedBaseInstanceFunc(GLenum @mode, GLint @first, GLsizei @count, GLsizei @instancecount, GLuint @baseinstance);
        internal static glDrawArraysInstancedBaseInstanceFunc glDrawArraysInstancedBaseInstancePtr;
        internal static void loadDrawArraysInstancedBaseInstance()
        {
            try
            {
                glDrawArraysInstancedBaseInstancePtr = (glDrawArraysInstancedBaseInstanceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysInstancedBaseInstance"), typeof(glDrawArraysInstancedBaseInstanceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysInstancedBaseInstance'.");
            }
        }
        public static void glDrawArraysInstancedBaseInstance(GLenum @mode, GLint @first, GLsizei @count, GLsizei @instancecount, GLuint @baseinstance) => glDrawArraysInstancedBaseInstancePtr(@mode, @first, @count, @instancecount, @baseinstance);

        internal delegate void glDrawElementsInstancedBaseInstanceFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLuint @baseinstance);
        internal static glDrawElementsInstancedBaseInstanceFunc glDrawElementsInstancedBaseInstancePtr;
        internal static void loadDrawElementsInstancedBaseInstance()
        {
            try
            {
                glDrawElementsInstancedBaseInstancePtr = (glDrawElementsInstancedBaseInstanceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedBaseInstance"), typeof(glDrawElementsInstancedBaseInstanceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedBaseInstance'.");
            }
        }
        public static void glDrawElementsInstancedBaseInstance(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLuint @baseinstance) => glDrawElementsInstancedBaseInstancePtr(@mode, @count, @type, @indices, @instancecount, @baseinstance);

        internal delegate void glDrawElementsInstancedBaseVertexBaseInstanceFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex, GLuint @baseinstance);
        internal static glDrawElementsInstancedBaseVertexBaseInstanceFunc glDrawElementsInstancedBaseVertexBaseInstancePtr;
        internal static void loadDrawElementsInstancedBaseVertexBaseInstance()
        {
            try
            {
                glDrawElementsInstancedBaseVertexBaseInstancePtr = (glDrawElementsInstancedBaseVertexBaseInstanceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedBaseVertexBaseInstance"), typeof(glDrawElementsInstancedBaseVertexBaseInstanceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedBaseVertexBaseInstance'.");
            }
        }
        public static void glDrawElementsInstancedBaseVertexBaseInstance(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex, GLuint @baseinstance) => glDrawElementsInstancedBaseVertexBaseInstancePtr(@mode, @count, @type, @indices, @instancecount, @basevertex, @baseinstance);
        #endregion
    }
}
