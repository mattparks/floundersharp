using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_ES2_compatibility
    {
        #region Interop
        static GL_ARB_ES2_compatibility()
        {
            Console.WriteLine("Initalising GL_ARB_ES2_compatibility interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadReleaseShaderCompiler();
            loadShaderBinary();
            loadGetShaderPrecisionFormat();
            loadDepthRangef();
            loadClearDepthf();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FIXED = 0x140C;
        public static UInt32 GL_IMPLEMENTATION_COLOR_READ_TYPE = 0x8B9A;
        public static UInt32 GL_IMPLEMENTATION_COLOR_READ_FORMAT = 0x8B9B;
        public static UInt32 GL_LOW_FLOAT = 0x8DF0;
        public static UInt32 GL_MEDIUM_FLOAT = 0x8DF1;
        public static UInt32 GL_HIGH_FLOAT = 0x8DF2;
        public static UInt32 GL_LOW_INT = 0x8DF3;
        public static UInt32 GL_MEDIUM_INT = 0x8DF4;
        public static UInt32 GL_HIGH_INT = 0x8DF5;
        public static UInt32 GL_SHADER_COMPILER = 0x8DFA;
        public static UInt32 GL_SHADER_BINARY_FORMATS = 0x8DF8;
        public static UInt32 GL_NUM_SHADER_BINARY_FORMATS = 0x8DF9;
        public static UInt32 GL_MAX_VERTEX_UNIFORM_VECTORS = 0x8DFB;
        public static UInt32 GL_MAX_VARYING_VECTORS = 0x8DFC;
        public static UInt32 GL_MAX_FRAGMENT_UNIFORM_VECTORS = 0x8DFD;
        public static UInt32 GL_RGB565 = 0x8D62;
        #endregion

        #region Commands
        internal delegate void glReleaseShaderCompilerFunc();
        internal static glReleaseShaderCompilerFunc glReleaseShaderCompilerPtr;
        internal static void loadReleaseShaderCompiler()
        {
            try
            {
                glReleaseShaderCompilerPtr = (glReleaseShaderCompilerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReleaseShaderCompiler"), typeof(glReleaseShaderCompilerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReleaseShaderCompiler'.");
            }
        }
        public static void glReleaseShaderCompiler() => glReleaseShaderCompilerPtr();

        internal delegate void glShaderBinaryFunc(GLsizei @count, const GLuint * @shaders, GLenum @binaryformat, const void * @binary, GLsizei @length);
        internal static glShaderBinaryFunc glShaderBinaryPtr;
        internal static void loadShaderBinary()
        {
            try
            {
                glShaderBinaryPtr = (glShaderBinaryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShaderBinary"), typeof(glShaderBinaryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShaderBinary'.");
            }
        }
        public static void glShaderBinary(GLsizei @count, const GLuint * @shaders, GLenum @binaryformat, const void * @binary, GLsizei @length) => glShaderBinaryPtr(@count, @shaders, @binaryformat, @binary, @length);

        internal delegate void glGetShaderPrecisionFormatFunc(GLenum @shadertype, GLenum @precisiontype, GLint * @range, GLint * @precision);
        internal static glGetShaderPrecisionFormatFunc glGetShaderPrecisionFormatPtr;
        internal static void loadGetShaderPrecisionFormat()
        {
            try
            {
                glGetShaderPrecisionFormatPtr = (glGetShaderPrecisionFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetShaderPrecisionFormat"), typeof(glGetShaderPrecisionFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetShaderPrecisionFormat'.");
            }
        }
        public static void glGetShaderPrecisionFormat(GLenum @shadertype, GLenum @precisiontype, GLint * @range, GLint * @precision) => glGetShaderPrecisionFormatPtr(@shadertype, @precisiontype, @range, @precision);

        internal delegate void glDepthRangefFunc(GLfloat @n, GLfloat @f);
        internal static glDepthRangefFunc glDepthRangefPtr;
        internal static void loadDepthRangef()
        {
            try
            {
                glDepthRangefPtr = (glDepthRangefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangef"), typeof(glDepthRangefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangef'.");
            }
        }
        public static void glDepthRangef(GLfloat @n, GLfloat @f) => glDepthRangefPtr(@n, @f);

        internal delegate void glClearDepthfFunc(GLfloat @d);
        internal static glClearDepthfFunc glClearDepthfPtr;
        internal static void loadClearDepthf()
        {
            try
            {
                glClearDepthfPtr = (glClearDepthfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearDepthf"), typeof(glClearDepthfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearDepthf'.");
            }
        }
        public static void glClearDepthf(GLfloat @d) => glClearDepthfPtr(@d);
        #endregion
    }
}
