using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_stencil8
    {
        #region Interop
        static GL_ARB_texture_stencil8()
        {
            Console.WriteLine("Initalising GL_ARB_texture_stencil8 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_STENCIL_INDEX = 0x1901;
        public static UInt32 GL_STENCIL_INDEX8 = 0x8D48;
        #endregion

        #region Commands
        #endregion
    }
}
