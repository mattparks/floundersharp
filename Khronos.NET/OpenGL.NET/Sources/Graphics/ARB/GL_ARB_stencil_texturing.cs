using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_stencil_texturing
    {
        #region Interop
        static GL_ARB_stencil_texturing()
        {
            Console.WriteLine("Initalising GL_ARB_stencil_texturing interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_STENCIL_TEXTURE_MODE = 0x90EA;
        #endregion

        #region Commands
        #endregion
    }
}
