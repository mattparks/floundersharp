using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_gpu_shader_int64
    {
        #region Interop
        static GL_ARB_gpu_shader_int64()
        {
            Console.WriteLine("Initalising GL_ARB_gpu_shader_int64 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadUniform1i64ARB();
            loadUniform2i64ARB();
            loadUniform3i64ARB();
            loadUniform4i64ARB();
            loadUniform1i64vARB();
            loadUniform2i64vARB();
            loadUniform3i64vARB();
            loadUniform4i64vARB();
            loadUniform1ui64ARB();
            loadUniform2ui64ARB();
            loadUniform3ui64ARB();
            loadUniform4ui64ARB();
            loadUniform1ui64vARB();
            loadUniform2ui64vARB();
            loadUniform3ui64vARB();
            loadUniform4ui64vARB();
            loadGetUniformi64vARB();
            loadGetUniformui64vARB();
            loadGetnUniformi64vARB();
            loadGetnUniformui64vARB();
            loadProgramUniform1i64ARB();
            loadProgramUniform2i64ARB();
            loadProgramUniform3i64ARB();
            loadProgramUniform4i64ARB();
            loadProgramUniform1i64vARB();
            loadProgramUniform2i64vARB();
            loadProgramUniform3i64vARB();
            loadProgramUniform4i64vARB();
            loadProgramUniform1ui64ARB();
            loadProgramUniform2ui64ARB();
            loadProgramUniform3ui64ARB();
            loadProgramUniform4ui64ARB();
            loadProgramUniform1ui64vARB();
            loadProgramUniform2ui64vARB();
            loadProgramUniform3ui64vARB();
            loadProgramUniform4ui64vARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_INT64_ARB = 0x140E;
        public static UInt32 GL_UNSIGNED_INT64_ARB = 0x140F;
        public static UInt32 GL_INT64_VEC2_ARB = 0x8FE9;
        public static UInt32 GL_INT64_VEC3_ARB = 0x8FEA;
        public static UInt32 GL_INT64_VEC4_ARB = 0x8FEB;
        public static UInt32 GL_UNSIGNED_INT64_VEC2_ARB = 0x8FF5;
        public static UInt32 GL_UNSIGNED_INT64_VEC3_ARB = 0x8FF6;
        public static UInt32 GL_UNSIGNED_INT64_VEC4_ARB = 0x8FF7;
        #endregion

        #region Commands
        internal delegate void glUniform1i64ARBFunc(GLint @location, GLint64 @x);
        internal static glUniform1i64ARBFunc glUniform1i64ARBPtr;
        internal static void loadUniform1i64ARB()
        {
            try
            {
                glUniform1i64ARBPtr = (glUniform1i64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1i64ARB"), typeof(glUniform1i64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1i64ARB'.");
            }
        }
        public static void glUniform1i64ARB(GLint @location, GLint64 @x) => glUniform1i64ARBPtr(@location, @x);

        internal delegate void glUniform2i64ARBFunc(GLint @location, GLint64 @x, GLint64 @y);
        internal static glUniform2i64ARBFunc glUniform2i64ARBPtr;
        internal static void loadUniform2i64ARB()
        {
            try
            {
                glUniform2i64ARBPtr = (glUniform2i64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2i64ARB"), typeof(glUniform2i64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2i64ARB'.");
            }
        }
        public static void glUniform2i64ARB(GLint @location, GLint64 @x, GLint64 @y) => glUniform2i64ARBPtr(@location, @x, @y);

        internal delegate void glUniform3i64ARBFunc(GLint @location, GLint64 @x, GLint64 @y, GLint64 @z);
        internal static glUniform3i64ARBFunc glUniform3i64ARBPtr;
        internal static void loadUniform3i64ARB()
        {
            try
            {
                glUniform3i64ARBPtr = (glUniform3i64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3i64ARB"), typeof(glUniform3i64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3i64ARB'.");
            }
        }
        public static void glUniform3i64ARB(GLint @location, GLint64 @x, GLint64 @y, GLint64 @z) => glUniform3i64ARBPtr(@location, @x, @y, @z);

        internal delegate void glUniform4i64ARBFunc(GLint @location, GLint64 @x, GLint64 @y, GLint64 @z, GLint64 @w);
        internal static glUniform4i64ARBFunc glUniform4i64ARBPtr;
        internal static void loadUniform4i64ARB()
        {
            try
            {
                glUniform4i64ARBPtr = (glUniform4i64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4i64ARB"), typeof(glUniform4i64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4i64ARB'.");
            }
        }
        public static void glUniform4i64ARB(GLint @location, GLint64 @x, GLint64 @y, GLint64 @z, GLint64 @w) => glUniform4i64ARBPtr(@location, @x, @y, @z, @w);

        internal delegate void glUniform1i64vARBFunc(GLint @location, GLsizei @count, const GLint64 * @value);
        internal static glUniform1i64vARBFunc glUniform1i64vARBPtr;
        internal static void loadUniform1i64vARB()
        {
            try
            {
                glUniform1i64vARBPtr = (glUniform1i64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1i64vARB"), typeof(glUniform1i64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1i64vARB'.");
            }
        }
        public static void glUniform1i64vARB(GLint @location, GLsizei @count, const GLint64 * @value) => glUniform1i64vARBPtr(@location, @count, @value);

        internal delegate void glUniform2i64vARBFunc(GLint @location, GLsizei @count, const GLint64 * @value);
        internal static glUniform2i64vARBFunc glUniform2i64vARBPtr;
        internal static void loadUniform2i64vARB()
        {
            try
            {
                glUniform2i64vARBPtr = (glUniform2i64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2i64vARB"), typeof(glUniform2i64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2i64vARB'.");
            }
        }
        public static void glUniform2i64vARB(GLint @location, GLsizei @count, const GLint64 * @value) => glUniform2i64vARBPtr(@location, @count, @value);

        internal delegate void glUniform3i64vARBFunc(GLint @location, GLsizei @count, const GLint64 * @value);
        internal static glUniform3i64vARBFunc glUniform3i64vARBPtr;
        internal static void loadUniform3i64vARB()
        {
            try
            {
                glUniform3i64vARBPtr = (glUniform3i64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3i64vARB"), typeof(glUniform3i64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3i64vARB'.");
            }
        }
        public static void glUniform3i64vARB(GLint @location, GLsizei @count, const GLint64 * @value) => glUniform3i64vARBPtr(@location, @count, @value);

        internal delegate void glUniform4i64vARBFunc(GLint @location, GLsizei @count, const GLint64 * @value);
        internal static glUniform4i64vARBFunc glUniform4i64vARBPtr;
        internal static void loadUniform4i64vARB()
        {
            try
            {
                glUniform4i64vARBPtr = (glUniform4i64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4i64vARB"), typeof(glUniform4i64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4i64vARB'.");
            }
        }
        public static void glUniform4i64vARB(GLint @location, GLsizei @count, const GLint64 * @value) => glUniform4i64vARBPtr(@location, @count, @value);

        internal delegate void glUniform1ui64ARBFunc(GLint @location, GLuint64 @x);
        internal static glUniform1ui64ARBFunc glUniform1ui64ARBPtr;
        internal static void loadUniform1ui64ARB()
        {
            try
            {
                glUniform1ui64ARBPtr = (glUniform1ui64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1ui64ARB"), typeof(glUniform1ui64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1ui64ARB'.");
            }
        }
        public static void glUniform1ui64ARB(GLint @location, GLuint64 @x) => glUniform1ui64ARBPtr(@location, @x);

        internal delegate void glUniform2ui64ARBFunc(GLint @location, GLuint64 @x, GLuint64 @y);
        internal static glUniform2ui64ARBFunc glUniform2ui64ARBPtr;
        internal static void loadUniform2ui64ARB()
        {
            try
            {
                glUniform2ui64ARBPtr = (glUniform2ui64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2ui64ARB"), typeof(glUniform2ui64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2ui64ARB'.");
            }
        }
        public static void glUniform2ui64ARB(GLint @location, GLuint64 @x, GLuint64 @y) => glUniform2ui64ARBPtr(@location, @x, @y);

        internal delegate void glUniform3ui64ARBFunc(GLint @location, GLuint64 @x, GLuint64 @y, GLuint64 @z);
        internal static glUniform3ui64ARBFunc glUniform3ui64ARBPtr;
        internal static void loadUniform3ui64ARB()
        {
            try
            {
                glUniform3ui64ARBPtr = (glUniform3ui64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3ui64ARB"), typeof(glUniform3ui64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3ui64ARB'.");
            }
        }
        public static void glUniform3ui64ARB(GLint @location, GLuint64 @x, GLuint64 @y, GLuint64 @z) => glUniform3ui64ARBPtr(@location, @x, @y, @z);

        internal delegate void glUniform4ui64ARBFunc(GLint @location, GLuint64 @x, GLuint64 @y, GLuint64 @z, GLuint64 @w);
        internal static glUniform4ui64ARBFunc glUniform4ui64ARBPtr;
        internal static void loadUniform4ui64ARB()
        {
            try
            {
                glUniform4ui64ARBPtr = (glUniform4ui64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4ui64ARB"), typeof(glUniform4ui64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4ui64ARB'.");
            }
        }
        public static void glUniform4ui64ARB(GLint @location, GLuint64 @x, GLuint64 @y, GLuint64 @z, GLuint64 @w) => glUniform4ui64ARBPtr(@location, @x, @y, @z, @w);

        internal delegate void glUniform1ui64vARBFunc(GLint @location, GLsizei @count, const GLuint64 * @value);
        internal static glUniform1ui64vARBFunc glUniform1ui64vARBPtr;
        internal static void loadUniform1ui64vARB()
        {
            try
            {
                glUniform1ui64vARBPtr = (glUniform1ui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1ui64vARB"), typeof(glUniform1ui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1ui64vARB'.");
            }
        }
        public static void glUniform1ui64vARB(GLint @location, GLsizei @count, const GLuint64 * @value) => glUniform1ui64vARBPtr(@location, @count, @value);

        internal delegate void glUniform2ui64vARBFunc(GLint @location, GLsizei @count, const GLuint64 * @value);
        internal static glUniform2ui64vARBFunc glUniform2ui64vARBPtr;
        internal static void loadUniform2ui64vARB()
        {
            try
            {
                glUniform2ui64vARBPtr = (glUniform2ui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2ui64vARB"), typeof(glUniform2ui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2ui64vARB'.");
            }
        }
        public static void glUniform2ui64vARB(GLint @location, GLsizei @count, const GLuint64 * @value) => glUniform2ui64vARBPtr(@location, @count, @value);

        internal delegate void glUniform3ui64vARBFunc(GLint @location, GLsizei @count, const GLuint64 * @value);
        internal static glUniform3ui64vARBFunc glUniform3ui64vARBPtr;
        internal static void loadUniform3ui64vARB()
        {
            try
            {
                glUniform3ui64vARBPtr = (glUniform3ui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3ui64vARB"), typeof(glUniform3ui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3ui64vARB'.");
            }
        }
        public static void glUniform3ui64vARB(GLint @location, GLsizei @count, const GLuint64 * @value) => glUniform3ui64vARBPtr(@location, @count, @value);

        internal delegate void glUniform4ui64vARBFunc(GLint @location, GLsizei @count, const GLuint64 * @value);
        internal static glUniform4ui64vARBFunc glUniform4ui64vARBPtr;
        internal static void loadUniform4ui64vARB()
        {
            try
            {
                glUniform4ui64vARBPtr = (glUniform4ui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4ui64vARB"), typeof(glUniform4ui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4ui64vARB'.");
            }
        }
        public static void glUniform4ui64vARB(GLint @location, GLsizei @count, const GLuint64 * @value) => glUniform4ui64vARBPtr(@location, @count, @value);

        internal delegate void glGetUniformi64vARBFunc(GLuint @program, GLint @location, GLint64 * @params);
        internal static glGetUniformi64vARBFunc glGetUniformi64vARBPtr;
        internal static void loadGetUniformi64vARB()
        {
            try
            {
                glGetUniformi64vARBPtr = (glGetUniformi64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformi64vARB"), typeof(glGetUniformi64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformi64vARB'.");
            }
        }
        public static void glGetUniformi64vARB(GLuint @program, GLint @location, GLint64 * @params) => glGetUniformi64vARBPtr(@program, @location, @params);

        internal delegate void glGetUniformui64vARBFunc(GLuint @program, GLint @location, GLuint64 * @params);
        internal static glGetUniformui64vARBFunc glGetUniformui64vARBPtr;
        internal static void loadGetUniformui64vARB()
        {
            try
            {
                glGetUniformui64vARBPtr = (glGetUniformui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformui64vARB"), typeof(glGetUniformui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformui64vARB'.");
            }
        }
        public static void glGetUniformui64vARB(GLuint @program, GLint @location, GLuint64 * @params) => glGetUniformui64vARBPtr(@program, @location, @params);

        internal delegate void glGetnUniformi64vARBFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLint64 * @params);
        internal static glGetnUniformi64vARBFunc glGetnUniformi64vARBPtr;
        internal static void loadGetnUniformi64vARB()
        {
            try
            {
                glGetnUniformi64vARBPtr = (glGetnUniformi64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformi64vARB"), typeof(glGetnUniformi64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformi64vARB'.");
            }
        }
        public static void glGetnUniformi64vARB(GLuint @program, GLint @location, GLsizei @bufSize, GLint64 * @params) => glGetnUniformi64vARBPtr(@program, @location, @bufSize, @params);

        internal delegate void glGetnUniformui64vARBFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLuint64 * @params);
        internal static glGetnUniformui64vARBFunc glGetnUniformui64vARBPtr;
        internal static void loadGetnUniformui64vARB()
        {
            try
            {
                glGetnUniformui64vARBPtr = (glGetnUniformui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformui64vARB"), typeof(glGetnUniformui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformui64vARB'.");
            }
        }
        public static void glGetnUniformui64vARB(GLuint @program, GLint @location, GLsizei @bufSize, GLuint64 * @params) => glGetnUniformui64vARBPtr(@program, @location, @bufSize, @params);

        internal delegate void glProgramUniform1i64ARBFunc(GLuint @program, GLint @location, GLint64 @x);
        internal static glProgramUniform1i64ARBFunc glProgramUniform1i64ARBPtr;
        internal static void loadProgramUniform1i64ARB()
        {
            try
            {
                glProgramUniform1i64ARBPtr = (glProgramUniform1i64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1i64ARB"), typeof(glProgramUniform1i64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1i64ARB'.");
            }
        }
        public static void glProgramUniform1i64ARB(GLuint @program, GLint @location, GLint64 @x) => glProgramUniform1i64ARBPtr(@program, @location, @x);

        internal delegate void glProgramUniform2i64ARBFunc(GLuint @program, GLint @location, GLint64 @x, GLint64 @y);
        internal static glProgramUniform2i64ARBFunc glProgramUniform2i64ARBPtr;
        internal static void loadProgramUniform2i64ARB()
        {
            try
            {
                glProgramUniform2i64ARBPtr = (glProgramUniform2i64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2i64ARB"), typeof(glProgramUniform2i64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2i64ARB'.");
            }
        }
        public static void glProgramUniform2i64ARB(GLuint @program, GLint @location, GLint64 @x, GLint64 @y) => glProgramUniform2i64ARBPtr(@program, @location, @x, @y);

        internal delegate void glProgramUniform3i64ARBFunc(GLuint @program, GLint @location, GLint64 @x, GLint64 @y, GLint64 @z);
        internal static glProgramUniform3i64ARBFunc glProgramUniform3i64ARBPtr;
        internal static void loadProgramUniform3i64ARB()
        {
            try
            {
                glProgramUniform3i64ARBPtr = (glProgramUniform3i64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3i64ARB"), typeof(glProgramUniform3i64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3i64ARB'.");
            }
        }
        public static void glProgramUniform3i64ARB(GLuint @program, GLint @location, GLint64 @x, GLint64 @y, GLint64 @z) => glProgramUniform3i64ARBPtr(@program, @location, @x, @y, @z);

        internal delegate void glProgramUniform4i64ARBFunc(GLuint @program, GLint @location, GLint64 @x, GLint64 @y, GLint64 @z, GLint64 @w);
        internal static glProgramUniform4i64ARBFunc glProgramUniform4i64ARBPtr;
        internal static void loadProgramUniform4i64ARB()
        {
            try
            {
                glProgramUniform4i64ARBPtr = (glProgramUniform4i64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4i64ARB"), typeof(glProgramUniform4i64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4i64ARB'.");
            }
        }
        public static void glProgramUniform4i64ARB(GLuint @program, GLint @location, GLint64 @x, GLint64 @y, GLint64 @z, GLint64 @w) => glProgramUniform4i64ARBPtr(@program, @location, @x, @y, @z, @w);

        internal delegate void glProgramUniform1i64vARBFunc(GLuint @program, GLint @location, GLsizei @count, const GLint64 * @value);
        internal static glProgramUniform1i64vARBFunc glProgramUniform1i64vARBPtr;
        internal static void loadProgramUniform1i64vARB()
        {
            try
            {
                glProgramUniform1i64vARBPtr = (glProgramUniform1i64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1i64vARB"), typeof(glProgramUniform1i64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1i64vARB'.");
            }
        }
        public static void glProgramUniform1i64vARB(GLuint @program, GLint @location, GLsizei @count, const GLint64 * @value) => glProgramUniform1i64vARBPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2i64vARBFunc(GLuint @program, GLint @location, GLsizei @count, const GLint64 * @value);
        internal static glProgramUniform2i64vARBFunc glProgramUniform2i64vARBPtr;
        internal static void loadProgramUniform2i64vARB()
        {
            try
            {
                glProgramUniform2i64vARBPtr = (glProgramUniform2i64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2i64vARB"), typeof(glProgramUniform2i64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2i64vARB'.");
            }
        }
        public static void glProgramUniform2i64vARB(GLuint @program, GLint @location, GLsizei @count, const GLint64 * @value) => glProgramUniform2i64vARBPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3i64vARBFunc(GLuint @program, GLint @location, GLsizei @count, const GLint64 * @value);
        internal static glProgramUniform3i64vARBFunc glProgramUniform3i64vARBPtr;
        internal static void loadProgramUniform3i64vARB()
        {
            try
            {
                glProgramUniform3i64vARBPtr = (glProgramUniform3i64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3i64vARB"), typeof(glProgramUniform3i64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3i64vARB'.");
            }
        }
        public static void glProgramUniform3i64vARB(GLuint @program, GLint @location, GLsizei @count, const GLint64 * @value) => glProgramUniform3i64vARBPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4i64vARBFunc(GLuint @program, GLint @location, GLsizei @count, const GLint64 * @value);
        internal static glProgramUniform4i64vARBFunc glProgramUniform4i64vARBPtr;
        internal static void loadProgramUniform4i64vARB()
        {
            try
            {
                glProgramUniform4i64vARBPtr = (glProgramUniform4i64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4i64vARB"), typeof(glProgramUniform4i64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4i64vARB'.");
            }
        }
        public static void glProgramUniform4i64vARB(GLuint @program, GLint @location, GLsizei @count, const GLint64 * @value) => glProgramUniform4i64vARBPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform1ui64ARBFunc(GLuint @program, GLint @location, GLuint64 @x);
        internal static glProgramUniform1ui64ARBFunc glProgramUniform1ui64ARBPtr;
        internal static void loadProgramUniform1ui64ARB()
        {
            try
            {
                glProgramUniform1ui64ARBPtr = (glProgramUniform1ui64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1ui64ARB"), typeof(glProgramUniform1ui64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1ui64ARB'.");
            }
        }
        public static void glProgramUniform1ui64ARB(GLuint @program, GLint @location, GLuint64 @x) => glProgramUniform1ui64ARBPtr(@program, @location, @x);

        internal delegate void glProgramUniform2ui64ARBFunc(GLuint @program, GLint @location, GLuint64 @x, GLuint64 @y);
        internal static glProgramUniform2ui64ARBFunc glProgramUniform2ui64ARBPtr;
        internal static void loadProgramUniform2ui64ARB()
        {
            try
            {
                glProgramUniform2ui64ARBPtr = (glProgramUniform2ui64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2ui64ARB"), typeof(glProgramUniform2ui64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2ui64ARB'.");
            }
        }
        public static void glProgramUniform2ui64ARB(GLuint @program, GLint @location, GLuint64 @x, GLuint64 @y) => glProgramUniform2ui64ARBPtr(@program, @location, @x, @y);

        internal delegate void glProgramUniform3ui64ARBFunc(GLuint @program, GLint @location, GLuint64 @x, GLuint64 @y, GLuint64 @z);
        internal static glProgramUniform3ui64ARBFunc glProgramUniform3ui64ARBPtr;
        internal static void loadProgramUniform3ui64ARB()
        {
            try
            {
                glProgramUniform3ui64ARBPtr = (glProgramUniform3ui64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3ui64ARB"), typeof(glProgramUniform3ui64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3ui64ARB'.");
            }
        }
        public static void glProgramUniform3ui64ARB(GLuint @program, GLint @location, GLuint64 @x, GLuint64 @y, GLuint64 @z) => glProgramUniform3ui64ARBPtr(@program, @location, @x, @y, @z);

        internal delegate void glProgramUniform4ui64ARBFunc(GLuint @program, GLint @location, GLuint64 @x, GLuint64 @y, GLuint64 @z, GLuint64 @w);
        internal static glProgramUniform4ui64ARBFunc glProgramUniform4ui64ARBPtr;
        internal static void loadProgramUniform4ui64ARB()
        {
            try
            {
                glProgramUniform4ui64ARBPtr = (glProgramUniform4ui64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4ui64ARB"), typeof(glProgramUniform4ui64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4ui64ARB'.");
            }
        }
        public static void glProgramUniform4ui64ARB(GLuint @program, GLint @location, GLuint64 @x, GLuint64 @y, GLuint64 @z, GLuint64 @w) => glProgramUniform4ui64ARBPtr(@program, @location, @x, @y, @z, @w);

        internal delegate void glProgramUniform1ui64vARBFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint64 * @value);
        internal static glProgramUniform1ui64vARBFunc glProgramUniform1ui64vARBPtr;
        internal static void loadProgramUniform1ui64vARB()
        {
            try
            {
                glProgramUniform1ui64vARBPtr = (glProgramUniform1ui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1ui64vARB"), typeof(glProgramUniform1ui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1ui64vARB'.");
            }
        }
        public static void glProgramUniform1ui64vARB(GLuint @program, GLint @location, GLsizei @count, const GLuint64 * @value) => glProgramUniform1ui64vARBPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2ui64vARBFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint64 * @value);
        internal static glProgramUniform2ui64vARBFunc glProgramUniform2ui64vARBPtr;
        internal static void loadProgramUniform2ui64vARB()
        {
            try
            {
                glProgramUniform2ui64vARBPtr = (glProgramUniform2ui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2ui64vARB"), typeof(glProgramUniform2ui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2ui64vARB'.");
            }
        }
        public static void glProgramUniform2ui64vARB(GLuint @program, GLint @location, GLsizei @count, const GLuint64 * @value) => glProgramUniform2ui64vARBPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3ui64vARBFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint64 * @value);
        internal static glProgramUniform3ui64vARBFunc glProgramUniform3ui64vARBPtr;
        internal static void loadProgramUniform3ui64vARB()
        {
            try
            {
                glProgramUniform3ui64vARBPtr = (glProgramUniform3ui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3ui64vARB"), typeof(glProgramUniform3ui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3ui64vARB'.");
            }
        }
        public static void glProgramUniform3ui64vARB(GLuint @program, GLint @location, GLsizei @count, const GLuint64 * @value) => glProgramUniform3ui64vARBPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4ui64vARBFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint64 * @value);
        internal static glProgramUniform4ui64vARBFunc glProgramUniform4ui64vARBPtr;
        internal static void loadProgramUniform4ui64vARB()
        {
            try
            {
                glProgramUniform4ui64vARBPtr = (glProgramUniform4ui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4ui64vARB"), typeof(glProgramUniform4ui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4ui64vARB'.");
            }
        }
        public static void glProgramUniform4ui64vARB(GLuint @program, GLint @location, GLsizei @count, const GLuint64 * @value) => glProgramUniform4ui64vARBPtr(@program, @location, @count, @value);
        #endregion
    }
}
