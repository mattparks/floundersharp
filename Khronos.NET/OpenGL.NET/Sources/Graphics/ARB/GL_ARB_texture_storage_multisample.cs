using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_storage_multisample
    {
        #region Interop
        static GL_ARB_texture_storage_multisample()
        {
            Console.WriteLine("Initalising GL_ARB_texture_storage_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexStorage2DMultisample();
            loadTexStorage3DMultisample();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glTexStorage2DMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations);
        internal static glTexStorage2DMultisampleFunc glTexStorage2DMultisamplePtr;
        internal static void loadTexStorage2DMultisample()
        {
            try
            {
                glTexStorage2DMultisamplePtr = (glTexStorage2DMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage2DMultisample"), typeof(glTexStorage2DMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage2DMultisample'.");
            }
        }
        public static void glTexStorage2DMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations) => glTexStorage2DMultisamplePtr(@target, @samples, @internalformat, @width, @height, @fixedsamplelocations);

        internal delegate void glTexStorage3DMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations);
        internal static glTexStorage3DMultisampleFunc glTexStorage3DMultisamplePtr;
        internal static void loadTexStorage3DMultisample()
        {
            try
            {
                glTexStorage3DMultisamplePtr = (glTexStorage3DMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage3DMultisample"), typeof(glTexStorage3DMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage3DMultisample'.");
            }
        }
        public static void glTexStorage3DMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations) => glTexStorage3DMultisamplePtr(@target, @samples, @internalformat, @width, @height, @depth, @fixedsamplelocations);
        #endregion
    }
}
