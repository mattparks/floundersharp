using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_color_buffer_float
    {
        #region Interop
        static GL_ARB_color_buffer_float()
        {
            Console.WriteLine("Initalising GL_ARB_color_buffer_float interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadClampColorARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RGBA_FLOAT_MODE_ARB = 0x8820;
        public static UInt32 GL_CLAMP_VERTEX_COLOR_ARB = 0x891A;
        public static UInt32 GL_CLAMP_FRAGMENT_COLOR_ARB = 0x891B;
        public static UInt32 GL_CLAMP_READ_COLOR_ARB = 0x891C;
        public static UInt32 GL_FIXED_ONLY_ARB = 0x891D;
        #endregion

        #region Commands
        internal delegate void glClampColorARBFunc(GLenum @target, GLenum @clamp);
        internal static glClampColorARBFunc glClampColorARBPtr;
        internal static void loadClampColorARB()
        {
            try
            {
                glClampColorARBPtr = (glClampColorARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClampColorARB"), typeof(glClampColorARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClampColorARB'.");
            }
        }
        public static void glClampColorARB(GLenum @target, GLenum @clamp) => glClampColorARBPtr(@target, @clamp);
        #endregion
    }
}
