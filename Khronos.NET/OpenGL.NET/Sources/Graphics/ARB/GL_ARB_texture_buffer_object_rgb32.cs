using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_buffer_object_rgb32
    {
        #region Interop
        static GL_ARB_texture_buffer_object_rgb32()
        {
            Console.WriteLine("Initalising GL_ARB_texture_buffer_object_rgb32 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RGB32F = 0x8815;
        public static UInt32 GL_RGB32UI = 0x8D71;
        public static UInt32 GL_RGB32I = 0x8D83;
        #endregion

        #region Commands
        #endregion
    }
}
