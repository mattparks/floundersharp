using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_ES3_2_compatibility
    {
        #region Interop
        static GL_ARB_ES3_2_compatibility()
        {
            Console.WriteLine("Initalising GL_ARB_ES3_2_compatibility interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPrimitiveBoundingBoxARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PRIMITIVE_BOUNDING_BOX_ARB = 0x92BE;
        public static UInt32 GL_MULTISAMPLE_LINE_WIDTH_RANGE_ARB = 0x9381;
        public static UInt32 GL_MULTISAMPLE_LINE_WIDTH_GRANULARITY_ARB = 0x9382;
        #endregion

        #region Commands
        internal delegate void glPrimitiveBoundingBoxARBFunc(GLfloat @minX, GLfloat @minY, GLfloat @minZ, GLfloat @minW, GLfloat @maxX, GLfloat @maxY, GLfloat @maxZ, GLfloat @maxW);
        internal static glPrimitiveBoundingBoxARBFunc glPrimitiveBoundingBoxARBPtr;
        internal static void loadPrimitiveBoundingBoxARB()
        {
            try
            {
                glPrimitiveBoundingBoxARBPtr = (glPrimitiveBoundingBoxARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPrimitiveBoundingBoxARB"), typeof(glPrimitiveBoundingBoxARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPrimitiveBoundingBoxARB'.");
            }
        }
        public static void glPrimitiveBoundingBoxARB(GLfloat @minX, GLfloat @minY, GLfloat @minZ, GLfloat @minW, GLfloat @maxX, GLfloat @maxY, GLfloat @maxZ, GLfloat @maxW) => glPrimitiveBoundingBoxARBPtr(@minX, @minY, @minZ, @minW, @maxX, @maxY, @maxZ, @maxW);
        #endregion
    }
}
