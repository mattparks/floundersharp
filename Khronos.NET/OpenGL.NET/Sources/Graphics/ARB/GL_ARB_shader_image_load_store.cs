using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_shader_image_load_store
    {
        #region Interop
        static GL_ARB_shader_image_load_store()
        {
            Console.WriteLine("Initalising GL_ARB_shader_image_load_store interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindImageTexture();
            loadMemoryBarrier();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT = 0x00000001;
        public static UInt32 GL_ELEMENT_ARRAY_BARRIER_BIT = 0x00000002;
        public static UInt32 GL_UNIFORM_BARRIER_BIT = 0x00000004;
        public static UInt32 GL_TEXTURE_FETCH_BARRIER_BIT = 0x00000008;
        public static UInt32 GL_SHADER_IMAGE_ACCESS_BARRIER_BIT = 0x00000020;
        public static UInt32 GL_COMMAND_BARRIER_BIT = 0x00000040;
        public static UInt32 GL_PIXEL_BUFFER_BARRIER_BIT = 0x00000080;
        public static UInt32 GL_TEXTURE_UPDATE_BARRIER_BIT = 0x00000100;
        public static UInt32 GL_BUFFER_UPDATE_BARRIER_BIT = 0x00000200;
        public static UInt32 GL_FRAMEBUFFER_BARRIER_BIT = 0x00000400;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BARRIER_BIT = 0x00000800;
        public static UInt32 GL_ATOMIC_COUNTER_BARRIER_BIT = 0x00001000;
        public static UInt32 GL_ALL_BARRIER_BITS = 0xFFFFFFFF;
        public static UInt32 GL_MAX_IMAGE_UNITS = 0x8F38;
        public static UInt32 GL_MAX_COMBINED_IMAGE_UNITS_AND_FRAGMENT_OUTPUTS = 0x8F39;
        public static UInt32 GL_IMAGE_BINDING_NAME = 0x8F3A;
        public static UInt32 GL_IMAGE_BINDING_LEVEL = 0x8F3B;
        public static UInt32 GL_IMAGE_BINDING_LAYERED = 0x8F3C;
        public static UInt32 GL_IMAGE_BINDING_LAYER = 0x8F3D;
        public static UInt32 GL_IMAGE_BINDING_ACCESS = 0x8F3E;
        public static UInt32 GL_IMAGE_1D = 0x904C;
        public static UInt32 GL_IMAGE_2D = 0x904D;
        public static UInt32 GL_IMAGE_3D = 0x904E;
        public static UInt32 GL_IMAGE_2D_RECT = 0x904F;
        public static UInt32 GL_IMAGE_CUBE = 0x9050;
        public static UInt32 GL_IMAGE_BUFFER = 0x9051;
        public static UInt32 GL_IMAGE_1D_ARRAY = 0x9052;
        public static UInt32 GL_IMAGE_2D_ARRAY = 0x9053;
        public static UInt32 GL_IMAGE_CUBE_MAP_ARRAY = 0x9054;
        public static UInt32 GL_IMAGE_2D_MULTISAMPLE = 0x9055;
        public static UInt32 GL_IMAGE_2D_MULTISAMPLE_ARRAY = 0x9056;
        public static UInt32 GL_INT_IMAGE_1D = 0x9057;
        public static UInt32 GL_INT_IMAGE_2D = 0x9058;
        public static UInt32 GL_INT_IMAGE_3D = 0x9059;
        public static UInt32 GL_INT_IMAGE_2D_RECT = 0x905A;
        public static UInt32 GL_INT_IMAGE_CUBE = 0x905B;
        public static UInt32 GL_INT_IMAGE_BUFFER = 0x905C;
        public static UInt32 GL_INT_IMAGE_1D_ARRAY = 0x905D;
        public static UInt32 GL_INT_IMAGE_2D_ARRAY = 0x905E;
        public static UInt32 GL_INT_IMAGE_CUBE_MAP_ARRAY = 0x905F;
        public static UInt32 GL_INT_IMAGE_2D_MULTISAMPLE = 0x9060;
        public static UInt32 GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY = 0x9061;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_1D = 0x9062;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D = 0x9063;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_3D = 0x9064;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_RECT = 0x9065;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_CUBE = 0x9066;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_BUFFER = 0x9067;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_1D_ARRAY = 0x9068;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_ARRAY = 0x9069;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_CUBE_MAP_ARRAY = 0x906A;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE = 0x906B;
        public static UInt32 GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY = 0x906C;
        public static UInt32 GL_MAX_IMAGE_SAMPLES = 0x906D;
        public static UInt32 GL_IMAGE_BINDING_FORMAT = 0x906E;
        public static UInt32 GL_IMAGE_FORMAT_COMPATIBILITY_TYPE = 0x90C7;
        public static UInt32 GL_IMAGE_FORMAT_COMPATIBILITY_BY_SIZE = 0x90C8;
        public static UInt32 GL_IMAGE_FORMAT_COMPATIBILITY_BY_CLASS = 0x90C9;
        public static UInt32 GL_MAX_VERTEX_IMAGE_UNIFORMS = 0x90CA;
        public static UInt32 GL_MAX_TESS_CONTROL_IMAGE_UNIFORMS = 0x90CB;
        public static UInt32 GL_MAX_TESS_EVALUATION_IMAGE_UNIFORMS = 0x90CC;
        public static UInt32 GL_MAX_GEOMETRY_IMAGE_UNIFORMS = 0x90CD;
        public static UInt32 GL_MAX_FRAGMENT_IMAGE_UNIFORMS = 0x90CE;
        public static UInt32 GL_MAX_COMBINED_IMAGE_UNIFORMS = 0x90CF;
        #endregion

        #region Commands
        internal delegate void glBindImageTextureFunc(GLuint @unit, GLuint @texture, GLint @level, GLboolean @layered, GLint @layer, GLenum @access, GLenum @format);
        internal static glBindImageTextureFunc glBindImageTexturePtr;
        internal static void loadBindImageTexture()
        {
            try
            {
                glBindImageTexturePtr = (glBindImageTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindImageTexture"), typeof(glBindImageTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindImageTexture'.");
            }
        }
        public static void glBindImageTexture(GLuint @unit, GLuint @texture, GLint @level, GLboolean @layered, GLint @layer, GLenum @access, GLenum @format) => glBindImageTexturePtr(@unit, @texture, @level, @layered, @layer, @access, @format);

        internal delegate void glMemoryBarrierFunc(GLbitfield @barriers);
        internal static glMemoryBarrierFunc glMemoryBarrierPtr;
        internal static void loadMemoryBarrier()
        {
            try
            {
                glMemoryBarrierPtr = (glMemoryBarrierFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMemoryBarrier"), typeof(glMemoryBarrierFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMemoryBarrier'.");
            }
        }
        public static void glMemoryBarrier(GLbitfield @barriers) => glMemoryBarrierPtr(@barriers);
        #endregion
    }
}
