using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_timer_query
    {
        #region Interop
        static GL_ARB_timer_query()
        {
            Console.WriteLine("Initalising GL_ARB_timer_query interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadQueryCounter();
            loadGetQueryObjecti64v();
            loadGetQueryObjectui64v();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TIME_ELAPSED = 0x88BF;
        public static UInt32 GL_TIMESTAMP = 0x8E28;
        #endregion

        #region Commands
        internal delegate void glQueryCounterFunc(GLuint @id, GLenum @target);
        internal static glQueryCounterFunc glQueryCounterPtr;
        internal static void loadQueryCounter()
        {
            try
            {
                glQueryCounterPtr = (glQueryCounterFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glQueryCounter"), typeof(glQueryCounterFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glQueryCounter'.");
            }
        }
        public static void glQueryCounter(GLuint @id, GLenum @target) => glQueryCounterPtr(@id, @target);

        internal delegate void glGetQueryObjecti64vFunc(GLuint @id, GLenum @pname, GLint64 * @params);
        internal static glGetQueryObjecti64vFunc glGetQueryObjecti64vPtr;
        internal static void loadGetQueryObjecti64v()
        {
            try
            {
                glGetQueryObjecti64vPtr = (glGetQueryObjecti64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjecti64v"), typeof(glGetQueryObjecti64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjecti64v'.");
            }
        }
        public static void glGetQueryObjecti64v(GLuint @id, GLenum @pname, GLint64 * @params) => glGetQueryObjecti64vPtr(@id, @pname, @params);

        internal delegate void glGetQueryObjectui64vFunc(GLuint @id, GLenum @pname, GLuint64 * @params);
        internal static glGetQueryObjectui64vFunc glGetQueryObjectui64vPtr;
        internal static void loadGetQueryObjectui64v()
        {
            try
            {
                glGetQueryObjectui64vPtr = (glGetQueryObjectui64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryObjectui64v"), typeof(glGetQueryObjectui64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryObjectui64v'.");
            }
        }
        public static void glGetQueryObjectui64v(GLuint @id, GLenum @pname, GLuint64 * @params) => glGetQueryObjectui64vPtr(@id, @pname, @params);
        #endregion
    }
}
