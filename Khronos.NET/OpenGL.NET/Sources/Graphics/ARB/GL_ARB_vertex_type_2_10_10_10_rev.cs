using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_vertex_type_2_10_10_10_rev
    {
        #region Interop
        static GL_ARB_vertex_type_2_10_10_10_rev()
        {
            Console.WriteLine("Initalising GL_ARB_vertex_type_2_10_10_10_rev interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexAttribP1ui();
            loadVertexAttribP1uiv();
            loadVertexAttribP2ui();
            loadVertexAttribP2uiv();
            loadVertexAttribP3ui();
            loadVertexAttribP3uiv();
            loadVertexAttribP4ui();
            loadVertexAttribP4uiv();
            loadVertexP2ui();
            loadVertexP2uiv();
            loadVertexP3ui();
            loadVertexP3uiv();
            loadVertexP4ui();
            loadVertexP4uiv();
            loadTexCoordP1ui();
            loadTexCoordP1uiv();
            loadTexCoordP2ui();
            loadTexCoordP2uiv();
            loadTexCoordP3ui();
            loadTexCoordP3uiv();
            loadTexCoordP4ui();
            loadTexCoordP4uiv();
            loadMultiTexCoordP1ui();
            loadMultiTexCoordP1uiv();
            loadMultiTexCoordP2ui();
            loadMultiTexCoordP2uiv();
            loadMultiTexCoordP3ui();
            loadMultiTexCoordP3uiv();
            loadMultiTexCoordP4ui();
            loadMultiTexCoordP4uiv();
            loadNormalP3ui();
            loadNormalP3uiv();
            loadColorP3ui();
            loadColorP3uiv();
            loadColorP4ui();
            loadColorP4uiv();
            loadSecondaryColorP3ui();
            loadSecondaryColorP3uiv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_UNSIGNED_INT_2_10_10_10_REV = 0x8368;
        public static UInt32 GL_INT_2_10_10_10_REV = 0x8D9F;
        #endregion

        #region Commands
        internal delegate void glVertexAttribP1uiFunc(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value);
        internal static glVertexAttribP1uiFunc glVertexAttribP1uiPtr;
        internal static void loadVertexAttribP1ui()
        {
            try
            {
                glVertexAttribP1uiPtr = (glVertexAttribP1uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP1ui"), typeof(glVertexAttribP1uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP1ui'.");
            }
        }
        public static void glVertexAttribP1ui(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value) => glVertexAttribP1uiPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP1uivFunc(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value);
        internal static glVertexAttribP1uivFunc glVertexAttribP1uivPtr;
        internal static void loadVertexAttribP1uiv()
        {
            try
            {
                glVertexAttribP1uivPtr = (glVertexAttribP1uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP1uiv"), typeof(glVertexAttribP1uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP1uiv'.");
            }
        }
        public static void glVertexAttribP1uiv(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value) => glVertexAttribP1uivPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP2uiFunc(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value);
        internal static glVertexAttribP2uiFunc glVertexAttribP2uiPtr;
        internal static void loadVertexAttribP2ui()
        {
            try
            {
                glVertexAttribP2uiPtr = (glVertexAttribP2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP2ui"), typeof(glVertexAttribP2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP2ui'.");
            }
        }
        public static void glVertexAttribP2ui(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value) => glVertexAttribP2uiPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP2uivFunc(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value);
        internal static glVertexAttribP2uivFunc glVertexAttribP2uivPtr;
        internal static void loadVertexAttribP2uiv()
        {
            try
            {
                glVertexAttribP2uivPtr = (glVertexAttribP2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP2uiv"), typeof(glVertexAttribP2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP2uiv'.");
            }
        }
        public static void glVertexAttribP2uiv(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value) => glVertexAttribP2uivPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP3uiFunc(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value);
        internal static glVertexAttribP3uiFunc glVertexAttribP3uiPtr;
        internal static void loadVertexAttribP3ui()
        {
            try
            {
                glVertexAttribP3uiPtr = (glVertexAttribP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP3ui"), typeof(glVertexAttribP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP3ui'.");
            }
        }
        public static void glVertexAttribP3ui(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value) => glVertexAttribP3uiPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP3uivFunc(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value);
        internal static glVertexAttribP3uivFunc glVertexAttribP3uivPtr;
        internal static void loadVertexAttribP3uiv()
        {
            try
            {
                glVertexAttribP3uivPtr = (glVertexAttribP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP3uiv"), typeof(glVertexAttribP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP3uiv'.");
            }
        }
        public static void glVertexAttribP3uiv(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value) => glVertexAttribP3uivPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP4uiFunc(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value);
        internal static glVertexAttribP4uiFunc glVertexAttribP4uiPtr;
        internal static void loadVertexAttribP4ui()
        {
            try
            {
                glVertexAttribP4uiPtr = (glVertexAttribP4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP4ui"), typeof(glVertexAttribP4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP4ui'.");
            }
        }
        public static void glVertexAttribP4ui(GLuint @index, GLenum @type, GLboolean @normalized, GLuint @value) => glVertexAttribP4uiPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexAttribP4uivFunc(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value);
        internal static glVertexAttribP4uivFunc glVertexAttribP4uivPtr;
        internal static void loadVertexAttribP4uiv()
        {
            try
            {
                glVertexAttribP4uivPtr = (glVertexAttribP4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribP4uiv"), typeof(glVertexAttribP4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribP4uiv'.");
            }
        }
        public static void glVertexAttribP4uiv(GLuint @index, GLenum @type, GLboolean @normalized, const GLuint * @value) => glVertexAttribP4uivPtr(@index, @type, @normalized, @value);

        internal delegate void glVertexP2uiFunc(GLenum @type, GLuint @value);
        internal static glVertexP2uiFunc glVertexP2uiPtr;
        internal static void loadVertexP2ui()
        {
            try
            {
                glVertexP2uiPtr = (glVertexP2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexP2ui"), typeof(glVertexP2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexP2ui'.");
            }
        }
        public static void glVertexP2ui(GLenum @type, GLuint @value) => glVertexP2uiPtr(@type, @value);

        internal delegate void glVertexP2uivFunc(GLenum @type, const GLuint * @value);
        internal static glVertexP2uivFunc glVertexP2uivPtr;
        internal static void loadVertexP2uiv()
        {
            try
            {
                glVertexP2uivPtr = (glVertexP2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexP2uiv"), typeof(glVertexP2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexP2uiv'.");
            }
        }
        public static void glVertexP2uiv(GLenum @type, const GLuint * @value) => glVertexP2uivPtr(@type, @value);

        internal delegate void glVertexP3uiFunc(GLenum @type, GLuint @value);
        internal static glVertexP3uiFunc glVertexP3uiPtr;
        internal static void loadVertexP3ui()
        {
            try
            {
                glVertexP3uiPtr = (glVertexP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexP3ui"), typeof(glVertexP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexP3ui'.");
            }
        }
        public static void glVertexP3ui(GLenum @type, GLuint @value) => glVertexP3uiPtr(@type, @value);

        internal delegate void glVertexP3uivFunc(GLenum @type, const GLuint * @value);
        internal static glVertexP3uivFunc glVertexP3uivPtr;
        internal static void loadVertexP3uiv()
        {
            try
            {
                glVertexP3uivPtr = (glVertexP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexP3uiv"), typeof(glVertexP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexP3uiv'.");
            }
        }
        public static void glVertexP3uiv(GLenum @type, const GLuint * @value) => glVertexP3uivPtr(@type, @value);

        internal delegate void glVertexP4uiFunc(GLenum @type, GLuint @value);
        internal static glVertexP4uiFunc glVertexP4uiPtr;
        internal static void loadVertexP4ui()
        {
            try
            {
                glVertexP4uiPtr = (glVertexP4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexP4ui"), typeof(glVertexP4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexP4ui'.");
            }
        }
        public static void glVertexP4ui(GLenum @type, GLuint @value) => glVertexP4uiPtr(@type, @value);

        internal delegate void glVertexP4uivFunc(GLenum @type, const GLuint * @value);
        internal static glVertexP4uivFunc glVertexP4uivPtr;
        internal static void loadVertexP4uiv()
        {
            try
            {
                glVertexP4uivPtr = (glVertexP4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexP4uiv"), typeof(glVertexP4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexP4uiv'.");
            }
        }
        public static void glVertexP4uiv(GLenum @type, const GLuint * @value) => glVertexP4uivPtr(@type, @value);

        internal delegate void glTexCoordP1uiFunc(GLenum @type, GLuint @coords);
        internal static glTexCoordP1uiFunc glTexCoordP1uiPtr;
        internal static void loadTexCoordP1ui()
        {
            try
            {
                glTexCoordP1uiPtr = (glTexCoordP1uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP1ui"), typeof(glTexCoordP1uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP1ui'.");
            }
        }
        public static void glTexCoordP1ui(GLenum @type, GLuint @coords) => glTexCoordP1uiPtr(@type, @coords);

        internal delegate void glTexCoordP1uivFunc(GLenum @type, const GLuint * @coords);
        internal static glTexCoordP1uivFunc glTexCoordP1uivPtr;
        internal static void loadTexCoordP1uiv()
        {
            try
            {
                glTexCoordP1uivPtr = (glTexCoordP1uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP1uiv"), typeof(glTexCoordP1uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP1uiv'.");
            }
        }
        public static void glTexCoordP1uiv(GLenum @type, const GLuint * @coords) => glTexCoordP1uivPtr(@type, @coords);

        internal delegate void glTexCoordP2uiFunc(GLenum @type, GLuint @coords);
        internal static glTexCoordP2uiFunc glTexCoordP2uiPtr;
        internal static void loadTexCoordP2ui()
        {
            try
            {
                glTexCoordP2uiPtr = (glTexCoordP2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP2ui"), typeof(glTexCoordP2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP2ui'.");
            }
        }
        public static void glTexCoordP2ui(GLenum @type, GLuint @coords) => glTexCoordP2uiPtr(@type, @coords);

        internal delegate void glTexCoordP2uivFunc(GLenum @type, const GLuint * @coords);
        internal static glTexCoordP2uivFunc glTexCoordP2uivPtr;
        internal static void loadTexCoordP2uiv()
        {
            try
            {
                glTexCoordP2uivPtr = (glTexCoordP2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP2uiv"), typeof(glTexCoordP2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP2uiv'.");
            }
        }
        public static void glTexCoordP2uiv(GLenum @type, const GLuint * @coords) => glTexCoordP2uivPtr(@type, @coords);

        internal delegate void glTexCoordP3uiFunc(GLenum @type, GLuint @coords);
        internal static glTexCoordP3uiFunc glTexCoordP3uiPtr;
        internal static void loadTexCoordP3ui()
        {
            try
            {
                glTexCoordP3uiPtr = (glTexCoordP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP3ui"), typeof(glTexCoordP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP3ui'.");
            }
        }
        public static void glTexCoordP3ui(GLenum @type, GLuint @coords) => glTexCoordP3uiPtr(@type, @coords);

        internal delegate void glTexCoordP3uivFunc(GLenum @type, const GLuint * @coords);
        internal static glTexCoordP3uivFunc glTexCoordP3uivPtr;
        internal static void loadTexCoordP3uiv()
        {
            try
            {
                glTexCoordP3uivPtr = (glTexCoordP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP3uiv"), typeof(glTexCoordP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP3uiv'.");
            }
        }
        public static void glTexCoordP3uiv(GLenum @type, const GLuint * @coords) => glTexCoordP3uivPtr(@type, @coords);

        internal delegate void glTexCoordP4uiFunc(GLenum @type, GLuint @coords);
        internal static glTexCoordP4uiFunc glTexCoordP4uiPtr;
        internal static void loadTexCoordP4ui()
        {
            try
            {
                glTexCoordP4uiPtr = (glTexCoordP4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP4ui"), typeof(glTexCoordP4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP4ui'.");
            }
        }
        public static void glTexCoordP4ui(GLenum @type, GLuint @coords) => glTexCoordP4uiPtr(@type, @coords);

        internal delegate void glTexCoordP4uivFunc(GLenum @type, const GLuint * @coords);
        internal static glTexCoordP4uivFunc glTexCoordP4uivPtr;
        internal static void loadTexCoordP4uiv()
        {
            try
            {
                glTexCoordP4uivPtr = (glTexCoordP4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordP4uiv"), typeof(glTexCoordP4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordP4uiv'.");
            }
        }
        public static void glTexCoordP4uiv(GLenum @type, const GLuint * @coords) => glTexCoordP4uivPtr(@type, @coords);

        internal delegate void glMultiTexCoordP1uiFunc(GLenum @texture, GLenum @type, GLuint @coords);
        internal static glMultiTexCoordP1uiFunc glMultiTexCoordP1uiPtr;
        internal static void loadMultiTexCoordP1ui()
        {
            try
            {
                glMultiTexCoordP1uiPtr = (glMultiTexCoordP1uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP1ui"), typeof(glMultiTexCoordP1uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP1ui'.");
            }
        }
        public static void glMultiTexCoordP1ui(GLenum @texture, GLenum @type, GLuint @coords) => glMultiTexCoordP1uiPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP1uivFunc(GLenum @texture, GLenum @type, const GLuint * @coords);
        internal static glMultiTexCoordP1uivFunc glMultiTexCoordP1uivPtr;
        internal static void loadMultiTexCoordP1uiv()
        {
            try
            {
                glMultiTexCoordP1uivPtr = (glMultiTexCoordP1uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP1uiv"), typeof(glMultiTexCoordP1uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP1uiv'.");
            }
        }
        public static void glMultiTexCoordP1uiv(GLenum @texture, GLenum @type, const GLuint * @coords) => glMultiTexCoordP1uivPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP2uiFunc(GLenum @texture, GLenum @type, GLuint @coords);
        internal static glMultiTexCoordP2uiFunc glMultiTexCoordP2uiPtr;
        internal static void loadMultiTexCoordP2ui()
        {
            try
            {
                glMultiTexCoordP2uiPtr = (glMultiTexCoordP2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP2ui"), typeof(glMultiTexCoordP2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP2ui'.");
            }
        }
        public static void glMultiTexCoordP2ui(GLenum @texture, GLenum @type, GLuint @coords) => glMultiTexCoordP2uiPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP2uivFunc(GLenum @texture, GLenum @type, const GLuint * @coords);
        internal static glMultiTexCoordP2uivFunc glMultiTexCoordP2uivPtr;
        internal static void loadMultiTexCoordP2uiv()
        {
            try
            {
                glMultiTexCoordP2uivPtr = (glMultiTexCoordP2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP2uiv"), typeof(glMultiTexCoordP2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP2uiv'.");
            }
        }
        public static void glMultiTexCoordP2uiv(GLenum @texture, GLenum @type, const GLuint * @coords) => glMultiTexCoordP2uivPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP3uiFunc(GLenum @texture, GLenum @type, GLuint @coords);
        internal static glMultiTexCoordP3uiFunc glMultiTexCoordP3uiPtr;
        internal static void loadMultiTexCoordP3ui()
        {
            try
            {
                glMultiTexCoordP3uiPtr = (glMultiTexCoordP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP3ui"), typeof(glMultiTexCoordP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP3ui'.");
            }
        }
        public static void glMultiTexCoordP3ui(GLenum @texture, GLenum @type, GLuint @coords) => glMultiTexCoordP3uiPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP3uivFunc(GLenum @texture, GLenum @type, const GLuint * @coords);
        internal static glMultiTexCoordP3uivFunc glMultiTexCoordP3uivPtr;
        internal static void loadMultiTexCoordP3uiv()
        {
            try
            {
                glMultiTexCoordP3uivPtr = (glMultiTexCoordP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP3uiv"), typeof(glMultiTexCoordP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP3uiv'.");
            }
        }
        public static void glMultiTexCoordP3uiv(GLenum @texture, GLenum @type, const GLuint * @coords) => glMultiTexCoordP3uivPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP4uiFunc(GLenum @texture, GLenum @type, GLuint @coords);
        internal static glMultiTexCoordP4uiFunc glMultiTexCoordP4uiPtr;
        internal static void loadMultiTexCoordP4ui()
        {
            try
            {
                glMultiTexCoordP4uiPtr = (glMultiTexCoordP4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP4ui"), typeof(glMultiTexCoordP4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP4ui'.");
            }
        }
        public static void glMultiTexCoordP4ui(GLenum @texture, GLenum @type, GLuint @coords) => glMultiTexCoordP4uiPtr(@texture, @type, @coords);

        internal delegate void glMultiTexCoordP4uivFunc(GLenum @texture, GLenum @type, const GLuint * @coords);
        internal static glMultiTexCoordP4uivFunc glMultiTexCoordP4uivPtr;
        internal static void loadMultiTexCoordP4uiv()
        {
            try
            {
                glMultiTexCoordP4uivPtr = (glMultiTexCoordP4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoordP4uiv"), typeof(glMultiTexCoordP4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoordP4uiv'.");
            }
        }
        public static void glMultiTexCoordP4uiv(GLenum @texture, GLenum @type, const GLuint * @coords) => glMultiTexCoordP4uivPtr(@texture, @type, @coords);

        internal delegate void glNormalP3uiFunc(GLenum @type, GLuint @coords);
        internal static glNormalP3uiFunc glNormalP3uiPtr;
        internal static void loadNormalP3ui()
        {
            try
            {
                glNormalP3uiPtr = (glNormalP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalP3ui"), typeof(glNormalP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalP3ui'.");
            }
        }
        public static void glNormalP3ui(GLenum @type, GLuint @coords) => glNormalP3uiPtr(@type, @coords);

        internal delegate void glNormalP3uivFunc(GLenum @type, const GLuint * @coords);
        internal static glNormalP3uivFunc glNormalP3uivPtr;
        internal static void loadNormalP3uiv()
        {
            try
            {
                glNormalP3uivPtr = (glNormalP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalP3uiv"), typeof(glNormalP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalP3uiv'.");
            }
        }
        public static void glNormalP3uiv(GLenum @type, const GLuint * @coords) => glNormalP3uivPtr(@type, @coords);

        internal delegate void glColorP3uiFunc(GLenum @type, GLuint @color);
        internal static glColorP3uiFunc glColorP3uiPtr;
        internal static void loadColorP3ui()
        {
            try
            {
                glColorP3uiPtr = (glColorP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorP3ui"), typeof(glColorP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorP3ui'.");
            }
        }
        public static void glColorP3ui(GLenum @type, GLuint @color) => glColorP3uiPtr(@type, @color);

        internal delegate void glColorP3uivFunc(GLenum @type, const GLuint * @color);
        internal static glColorP3uivFunc glColorP3uivPtr;
        internal static void loadColorP3uiv()
        {
            try
            {
                glColorP3uivPtr = (glColorP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorP3uiv"), typeof(glColorP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorP3uiv'.");
            }
        }
        public static void glColorP3uiv(GLenum @type, const GLuint * @color) => glColorP3uivPtr(@type, @color);

        internal delegate void glColorP4uiFunc(GLenum @type, GLuint @color);
        internal static glColorP4uiFunc glColorP4uiPtr;
        internal static void loadColorP4ui()
        {
            try
            {
                glColorP4uiPtr = (glColorP4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorP4ui"), typeof(glColorP4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorP4ui'.");
            }
        }
        public static void glColorP4ui(GLenum @type, GLuint @color) => glColorP4uiPtr(@type, @color);

        internal delegate void glColorP4uivFunc(GLenum @type, const GLuint * @color);
        internal static glColorP4uivFunc glColorP4uivPtr;
        internal static void loadColorP4uiv()
        {
            try
            {
                glColorP4uivPtr = (glColorP4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorP4uiv"), typeof(glColorP4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorP4uiv'.");
            }
        }
        public static void glColorP4uiv(GLenum @type, const GLuint * @color) => glColorP4uivPtr(@type, @color);

        internal delegate void glSecondaryColorP3uiFunc(GLenum @type, GLuint @color);
        internal static glSecondaryColorP3uiFunc glSecondaryColorP3uiPtr;
        internal static void loadSecondaryColorP3ui()
        {
            try
            {
                glSecondaryColorP3uiPtr = (glSecondaryColorP3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColorP3ui"), typeof(glSecondaryColorP3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColorP3ui'.");
            }
        }
        public static void glSecondaryColorP3ui(GLenum @type, GLuint @color) => glSecondaryColorP3uiPtr(@type, @color);

        internal delegate void glSecondaryColorP3uivFunc(GLenum @type, const GLuint * @color);
        internal static glSecondaryColorP3uivFunc glSecondaryColorP3uivPtr;
        internal static void loadSecondaryColorP3uiv()
        {
            try
            {
                glSecondaryColorP3uivPtr = (glSecondaryColorP3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSecondaryColorP3uiv"), typeof(glSecondaryColorP3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSecondaryColorP3uiv'.");
            }
        }
        public static void glSecondaryColorP3uiv(GLenum @type, const GLuint * @color) => glSecondaryColorP3uivPtr(@type, @color);
        #endregion
    }
}
