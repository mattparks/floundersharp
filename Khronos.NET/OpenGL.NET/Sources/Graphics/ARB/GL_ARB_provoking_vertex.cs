using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_provoking_vertex
    {
        #region Interop
        static GL_ARB_provoking_vertex()
        {
            Console.WriteLine("Initalising GL_ARB_provoking_vertex interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadProvokingVertex();
        }
        #endregion

        #region Enums
        public static UInt32 GL_QUADS_FOLLOW_PROVOKING_VERTEX_CONVENTION = 0x8E4C;
        public static UInt32 GL_FIRST_VERTEX_CONVENTION = 0x8E4D;
        public static UInt32 GL_LAST_VERTEX_CONVENTION = 0x8E4E;
        public static UInt32 GL_PROVOKING_VERTEX = 0x8E4F;
        #endregion

        #region Commands
        internal delegate void glProvokingVertexFunc(GLenum @mode);
        internal static glProvokingVertexFunc glProvokingVertexPtr;
        internal static void loadProvokingVertex()
        {
            try
            {
                glProvokingVertexPtr = (glProvokingVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProvokingVertex"), typeof(glProvokingVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProvokingVertex'.");
            }
        }
        public static void glProvokingVertex(GLenum @mode) => glProvokingVertexPtr(@mode);
        #endregion
    }
}
