using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_draw_elements_base_vertex
    {
        #region Interop
        static GL_ARB_draw_elements_base_vertex()
        {
            Console.WriteLine("Initalising GL_ARB_draw_elements_base_vertex interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawElementsBaseVertex();
            loadDrawRangeElementsBaseVertex();
            loadDrawElementsInstancedBaseVertex();
            loadMultiDrawElementsBaseVertex();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glDrawElementsBaseVertexFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex);
        internal static glDrawElementsBaseVertexFunc glDrawElementsBaseVertexPtr;
        internal static void loadDrawElementsBaseVertex()
        {
            try
            {
                glDrawElementsBaseVertexPtr = (glDrawElementsBaseVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsBaseVertex"), typeof(glDrawElementsBaseVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsBaseVertex'.");
            }
        }
        public static void glDrawElementsBaseVertex(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex) => glDrawElementsBaseVertexPtr(@mode, @count, @type, @indices, @basevertex);

        internal delegate void glDrawRangeElementsBaseVertexFunc(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex);
        internal static glDrawRangeElementsBaseVertexFunc glDrawRangeElementsBaseVertexPtr;
        internal static void loadDrawRangeElementsBaseVertex()
        {
            try
            {
                glDrawRangeElementsBaseVertexPtr = (glDrawRangeElementsBaseVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawRangeElementsBaseVertex"), typeof(glDrawRangeElementsBaseVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawRangeElementsBaseVertex'.");
            }
        }
        public static void glDrawRangeElementsBaseVertex(GLenum @mode, GLuint @start, GLuint @end, GLsizei @count, GLenum @type, const void * @indices, GLint @basevertex) => glDrawRangeElementsBaseVertexPtr(@mode, @start, @end, @count, @type, @indices, @basevertex);

        internal delegate void glDrawElementsInstancedBaseVertexFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex);
        internal static glDrawElementsInstancedBaseVertexFunc glDrawElementsInstancedBaseVertexPtr;
        internal static void loadDrawElementsInstancedBaseVertex()
        {
            try
            {
                glDrawElementsInstancedBaseVertexPtr = (glDrawElementsInstancedBaseVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedBaseVertex"), typeof(glDrawElementsInstancedBaseVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedBaseVertex'.");
            }
        }
        public static void glDrawElementsInstancedBaseVertex(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @instancecount, GLint @basevertex) => glDrawElementsInstancedBaseVertexPtr(@mode, @count, @type, @indices, @instancecount, @basevertex);

        internal delegate void glMultiDrawElementsBaseVertexFunc(GLenum @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @drawcount, const GLint * @basevertex);
        internal static glMultiDrawElementsBaseVertexFunc glMultiDrawElementsBaseVertexPtr;
        internal static void loadMultiDrawElementsBaseVertex()
        {
            try
            {
                glMultiDrawElementsBaseVertexPtr = (glMultiDrawElementsBaseVertexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementsBaseVertex"), typeof(glMultiDrawElementsBaseVertexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementsBaseVertex'.");
            }
        }
        public static void glMultiDrawElementsBaseVertex(GLenum @mode, const GLsizei * @count, GLenum @type, const void *const* @indices, GLsizei @drawcount, const GLint * @basevertex) => glMultiDrawElementsBaseVertexPtr(@mode, @count, @type, @indices, @drawcount, @basevertex);
        #endregion
    }
}
