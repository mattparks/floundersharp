using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_clear_buffer_object
    {
        #region Interop
        static GL_ARB_clear_buffer_object()
        {
            Console.WriteLine("Initalising GL_ARB_clear_buffer_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadClearBufferData();
            loadClearBufferSubData();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glClearBufferDataFunc(GLenum @target, GLenum @internalformat, GLenum @format, GLenum @type, const void * @data);
        internal static glClearBufferDataFunc glClearBufferDataPtr;
        internal static void loadClearBufferData()
        {
            try
            {
                glClearBufferDataPtr = (glClearBufferDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearBufferData"), typeof(glClearBufferDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearBufferData'.");
            }
        }
        public static void glClearBufferData(GLenum @target, GLenum @internalformat, GLenum @format, GLenum @type, const void * @data) => glClearBufferDataPtr(@target, @internalformat, @format, @type, @data);

        internal delegate void glClearBufferSubDataFunc(GLenum @target, GLenum @internalformat, GLintptr @offset, GLsizeiptr @size, GLenum @format, GLenum @type, const void * @data);
        internal static glClearBufferSubDataFunc glClearBufferSubDataPtr;
        internal static void loadClearBufferSubData()
        {
            try
            {
                glClearBufferSubDataPtr = (glClearBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearBufferSubData"), typeof(glClearBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearBufferSubData'.");
            }
        }
        public static void glClearBufferSubData(GLenum @target, GLenum @internalformat, GLintptr @offset, GLsizeiptr @size, GLenum @format, GLenum @type, const void * @data) => glClearBufferSubDataPtr(@target, @internalformat, @offset, @size, @format, @type, @data);
        #endregion
    }
}
