using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_separate_shader_objects
    {
        #region Interop
        static GL_ARB_separate_shader_objects()
        {
            Console.WriteLine("Initalising GL_ARB_separate_shader_objects interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadUseProgramStages();
            loadActiveShaderProgram();
            loadCreateShaderProgramv();
            loadBindProgramPipeline();
            loadDeleteProgramPipelines();
            loadGenProgramPipelines();
            loadIsProgramPipeline();
            loadGetProgramPipelineiv();
            loadProgramUniform1i();
            loadProgramUniform1iv();
            loadProgramUniform1f();
            loadProgramUniform1fv();
            loadProgramUniform1d();
            loadProgramUniform1dv();
            loadProgramUniform1ui();
            loadProgramUniform1uiv();
            loadProgramUniform2i();
            loadProgramUniform2iv();
            loadProgramUniform2f();
            loadProgramUniform2fv();
            loadProgramUniform2d();
            loadProgramUniform2dv();
            loadProgramUniform2ui();
            loadProgramUniform2uiv();
            loadProgramUniform3i();
            loadProgramUniform3iv();
            loadProgramUniform3f();
            loadProgramUniform3fv();
            loadProgramUniform3d();
            loadProgramUniform3dv();
            loadProgramUniform3ui();
            loadProgramUniform3uiv();
            loadProgramUniform4i();
            loadProgramUniform4iv();
            loadProgramUniform4f();
            loadProgramUniform4fv();
            loadProgramUniform4d();
            loadProgramUniform4dv();
            loadProgramUniform4ui();
            loadProgramUniform4uiv();
            loadProgramUniformMatrix2fv();
            loadProgramUniformMatrix3fv();
            loadProgramUniformMatrix4fv();
            loadProgramUniformMatrix2dv();
            loadProgramUniformMatrix3dv();
            loadProgramUniformMatrix4dv();
            loadProgramUniformMatrix2x3fv();
            loadProgramUniformMatrix3x2fv();
            loadProgramUniformMatrix2x4fv();
            loadProgramUniformMatrix4x2fv();
            loadProgramUniformMatrix3x4fv();
            loadProgramUniformMatrix4x3fv();
            loadProgramUniformMatrix2x3dv();
            loadProgramUniformMatrix3x2dv();
            loadProgramUniformMatrix2x4dv();
            loadProgramUniformMatrix4x2dv();
            loadProgramUniformMatrix3x4dv();
            loadProgramUniformMatrix4x3dv();
            loadValidateProgramPipeline();
            loadGetProgramPipelineInfoLog();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_SHADER_BIT = 0x00000001;
        public static UInt32 GL_FRAGMENT_SHADER_BIT = 0x00000002;
        public static UInt32 GL_GEOMETRY_SHADER_BIT = 0x00000004;
        public static UInt32 GL_TESS_CONTROL_SHADER_BIT = 0x00000008;
        public static UInt32 GL_TESS_EVALUATION_SHADER_BIT = 0x00000010;
        public static UInt32 GL_ALL_SHADER_BITS = 0xFFFFFFFF;
        public static UInt32 GL_PROGRAM_SEPARABLE = 0x8258;
        public static UInt32 GL_ACTIVE_PROGRAM = 0x8259;
        public static UInt32 GL_PROGRAM_PIPELINE_BINDING = 0x825A;
        #endregion

        #region Commands
        internal delegate void glUseProgramStagesFunc(GLuint @pipeline, GLbitfield @stages, GLuint @program);
        internal static glUseProgramStagesFunc glUseProgramStagesPtr;
        internal static void loadUseProgramStages()
        {
            try
            {
                glUseProgramStagesPtr = (glUseProgramStagesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUseProgramStages"), typeof(glUseProgramStagesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUseProgramStages'.");
            }
        }
        public static void glUseProgramStages(GLuint @pipeline, GLbitfield @stages, GLuint @program) => glUseProgramStagesPtr(@pipeline, @stages, @program);

        internal delegate void glActiveShaderProgramFunc(GLuint @pipeline, GLuint @program);
        internal static glActiveShaderProgramFunc glActiveShaderProgramPtr;
        internal static void loadActiveShaderProgram()
        {
            try
            {
                glActiveShaderProgramPtr = (glActiveShaderProgramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glActiveShaderProgram"), typeof(glActiveShaderProgramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glActiveShaderProgram'.");
            }
        }
        public static void glActiveShaderProgram(GLuint @pipeline, GLuint @program) => glActiveShaderProgramPtr(@pipeline, @program);

        internal delegate GLuint glCreateShaderProgramvFunc(GLenum @type, GLsizei @count, const GLchar *const* @strings);
        internal static glCreateShaderProgramvFunc glCreateShaderProgramvPtr;
        internal static void loadCreateShaderProgramv()
        {
            try
            {
                glCreateShaderProgramvPtr = (glCreateShaderProgramvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateShaderProgramv"), typeof(glCreateShaderProgramvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateShaderProgramv'.");
            }
        }
        public static GLuint glCreateShaderProgramv(GLenum @type, GLsizei @count, const GLchar *const* @strings) => glCreateShaderProgramvPtr(@type, @count, @strings);

        internal delegate void glBindProgramPipelineFunc(GLuint @pipeline);
        internal static glBindProgramPipelineFunc glBindProgramPipelinePtr;
        internal static void loadBindProgramPipeline()
        {
            try
            {
                glBindProgramPipelinePtr = (glBindProgramPipelineFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindProgramPipeline"), typeof(glBindProgramPipelineFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindProgramPipeline'.");
            }
        }
        public static void glBindProgramPipeline(GLuint @pipeline) => glBindProgramPipelinePtr(@pipeline);

        internal delegate void glDeleteProgramPipelinesFunc(GLsizei @n, const GLuint * @pipelines);
        internal static glDeleteProgramPipelinesFunc glDeleteProgramPipelinesPtr;
        internal static void loadDeleteProgramPipelines()
        {
            try
            {
                glDeleteProgramPipelinesPtr = (glDeleteProgramPipelinesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteProgramPipelines"), typeof(glDeleteProgramPipelinesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteProgramPipelines'.");
            }
        }
        public static void glDeleteProgramPipelines(GLsizei @n, const GLuint * @pipelines) => glDeleteProgramPipelinesPtr(@n, @pipelines);

        internal delegate void glGenProgramPipelinesFunc(GLsizei @n, GLuint * @pipelines);
        internal static glGenProgramPipelinesFunc glGenProgramPipelinesPtr;
        internal static void loadGenProgramPipelines()
        {
            try
            {
                glGenProgramPipelinesPtr = (glGenProgramPipelinesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenProgramPipelines"), typeof(glGenProgramPipelinesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenProgramPipelines'.");
            }
        }
        public static void glGenProgramPipelines(GLsizei @n, GLuint * @pipelines) => glGenProgramPipelinesPtr(@n, @pipelines);

        internal delegate GLboolean glIsProgramPipelineFunc(GLuint @pipeline);
        internal static glIsProgramPipelineFunc glIsProgramPipelinePtr;
        internal static void loadIsProgramPipeline()
        {
            try
            {
                glIsProgramPipelinePtr = (glIsProgramPipelineFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsProgramPipeline"), typeof(glIsProgramPipelineFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsProgramPipeline'.");
            }
        }
        public static GLboolean glIsProgramPipeline(GLuint @pipeline) => glIsProgramPipelinePtr(@pipeline);

        internal delegate void glGetProgramPipelineivFunc(GLuint @pipeline, GLenum @pname, GLint * @params);
        internal static glGetProgramPipelineivFunc glGetProgramPipelineivPtr;
        internal static void loadGetProgramPipelineiv()
        {
            try
            {
                glGetProgramPipelineivPtr = (glGetProgramPipelineivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramPipelineiv"), typeof(glGetProgramPipelineivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramPipelineiv'.");
            }
        }
        public static void glGetProgramPipelineiv(GLuint @pipeline, GLenum @pname, GLint * @params) => glGetProgramPipelineivPtr(@pipeline, @pname, @params);

        internal delegate void glProgramUniform1iFunc(GLuint @program, GLint @location, GLint @v0);
        internal static glProgramUniform1iFunc glProgramUniform1iPtr;
        internal static void loadProgramUniform1i()
        {
            try
            {
                glProgramUniform1iPtr = (glProgramUniform1iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1i"), typeof(glProgramUniform1iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1i'.");
            }
        }
        public static void glProgramUniform1i(GLuint @program, GLint @location, GLint @v0) => glProgramUniform1iPtr(@program, @location, @v0);

        internal delegate void glProgramUniform1ivFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform1ivFunc glProgramUniform1ivPtr;
        internal static void loadProgramUniform1iv()
        {
            try
            {
                glProgramUniform1ivPtr = (glProgramUniform1ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1iv"), typeof(glProgramUniform1ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1iv'.");
            }
        }
        public static void glProgramUniform1iv(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform1ivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform1fFunc(GLuint @program, GLint @location, GLfloat @v0);
        internal static glProgramUniform1fFunc glProgramUniform1fPtr;
        internal static void loadProgramUniform1f()
        {
            try
            {
                glProgramUniform1fPtr = (glProgramUniform1fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1f"), typeof(glProgramUniform1fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1f'.");
            }
        }
        public static void glProgramUniform1f(GLuint @program, GLint @location, GLfloat @v0) => glProgramUniform1fPtr(@program, @location, @v0);

        internal delegate void glProgramUniform1fvFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform1fvFunc glProgramUniform1fvPtr;
        internal static void loadProgramUniform1fv()
        {
            try
            {
                glProgramUniform1fvPtr = (glProgramUniform1fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1fv"), typeof(glProgramUniform1fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1fv'.");
            }
        }
        public static void glProgramUniform1fv(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform1fvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform1dFunc(GLuint @program, GLint @location, GLdouble @v0);
        internal static glProgramUniform1dFunc glProgramUniform1dPtr;
        internal static void loadProgramUniform1d()
        {
            try
            {
                glProgramUniform1dPtr = (glProgramUniform1dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1d"), typeof(glProgramUniform1dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1d'.");
            }
        }
        public static void glProgramUniform1d(GLuint @program, GLint @location, GLdouble @v0) => glProgramUniform1dPtr(@program, @location, @v0);

        internal delegate void glProgramUniform1dvFunc(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glProgramUniform1dvFunc glProgramUniform1dvPtr;
        internal static void loadProgramUniform1dv()
        {
            try
            {
                glProgramUniform1dvPtr = (glProgramUniform1dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1dv"), typeof(glProgramUniform1dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1dv'.");
            }
        }
        public static void glProgramUniform1dv(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value) => glProgramUniform1dvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform1uiFunc(GLuint @program, GLint @location, GLuint @v0);
        internal static glProgramUniform1uiFunc glProgramUniform1uiPtr;
        internal static void loadProgramUniform1ui()
        {
            try
            {
                glProgramUniform1uiPtr = (glProgramUniform1uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1ui"), typeof(glProgramUniform1uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1ui'.");
            }
        }
        public static void glProgramUniform1ui(GLuint @program, GLint @location, GLuint @v0) => glProgramUniform1uiPtr(@program, @location, @v0);

        internal delegate void glProgramUniform1uivFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform1uivFunc glProgramUniform1uivPtr;
        internal static void loadProgramUniform1uiv()
        {
            try
            {
                glProgramUniform1uivPtr = (glProgramUniform1uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform1uiv"), typeof(glProgramUniform1uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform1uiv'.");
            }
        }
        public static void glProgramUniform1uiv(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform1uivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2iFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1);
        internal static glProgramUniform2iFunc glProgramUniform2iPtr;
        internal static void loadProgramUniform2i()
        {
            try
            {
                glProgramUniform2iPtr = (glProgramUniform2iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2i"), typeof(glProgramUniform2iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2i'.");
            }
        }
        public static void glProgramUniform2i(GLuint @program, GLint @location, GLint @v0, GLint @v1) => glProgramUniform2iPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform2ivFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform2ivFunc glProgramUniform2ivPtr;
        internal static void loadProgramUniform2iv()
        {
            try
            {
                glProgramUniform2ivPtr = (glProgramUniform2ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2iv"), typeof(glProgramUniform2ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2iv'.");
            }
        }
        public static void glProgramUniform2iv(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform2ivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2fFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1);
        internal static glProgramUniform2fFunc glProgramUniform2fPtr;
        internal static void loadProgramUniform2f()
        {
            try
            {
                glProgramUniform2fPtr = (glProgramUniform2fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2f"), typeof(glProgramUniform2fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2f'.");
            }
        }
        public static void glProgramUniform2f(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1) => glProgramUniform2fPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform2fvFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform2fvFunc glProgramUniform2fvPtr;
        internal static void loadProgramUniform2fv()
        {
            try
            {
                glProgramUniform2fvPtr = (glProgramUniform2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2fv"), typeof(glProgramUniform2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2fv'.");
            }
        }
        public static void glProgramUniform2fv(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform2fvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2dFunc(GLuint @program, GLint @location, GLdouble @v0, GLdouble @v1);
        internal static glProgramUniform2dFunc glProgramUniform2dPtr;
        internal static void loadProgramUniform2d()
        {
            try
            {
                glProgramUniform2dPtr = (glProgramUniform2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2d"), typeof(glProgramUniform2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2d'.");
            }
        }
        public static void glProgramUniform2d(GLuint @program, GLint @location, GLdouble @v0, GLdouble @v1) => glProgramUniform2dPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform2dvFunc(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glProgramUniform2dvFunc glProgramUniform2dvPtr;
        internal static void loadProgramUniform2dv()
        {
            try
            {
                glProgramUniform2dvPtr = (glProgramUniform2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2dv"), typeof(glProgramUniform2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2dv'.");
            }
        }
        public static void glProgramUniform2dv(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value) => glProgramUniform2dvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform2uiFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1);
        internal static glProgramUniform2uiFunc glProgramUniform2uiPtr;
        internal static void loadProgramUniform2ui()
        {
            try
            {
                glProgramUniform2uiPtr = (glProgramUniform2uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2ui"), typeof(glProgramUniform2uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2ui'.");
            }
        }
        public static void glProgramUniform2ui(GLuint @program, GLint @location, GLuint @v0, GLuint @v1) => glProgramUniform2uiPtr(@program, @location, @v0, @v1);

        internal delegate void glProgramUniform2uivFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform2uivFunc glProgramUniform2uivPtr;
        internal static void loadProgramUniform2uiv()
        {
            try
            {
                glProgramUniform2uivPtr = (glProgramUniform2uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform2uiv"), typeof(glProgramUniform2uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform2uiv'.");
            }
        }
        public static void glProgramUniform2uiv(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform2uivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3iFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2);
        internal static glProgramUniform3iFunc glProgramUniform3iPtr;
        internal static void loadProgramUniform3i()
        {
            try
            {
                glProgramUniform3iPtr = (glProgramUniform3iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3i"), typeof(glProgramUniform3iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3i'.");
            }
        }
        public static void glProgramUniform3i(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2) => glProgramUniform3iPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform3ivFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform3ivFunc glProgramUniform3ivPtr;
        internal static void loadProgramUniform3iv()
        {
            try
            {
                glProgramUniform3ivPtr = (glProgramUniform3ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3iv"), typeof(glProgramUniform3ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3iv'.");
            }
        }
        public static void glProgramUniform3iv(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform3ivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3fFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2);
        internal static glProgramUniform3fFunc glProgramUniform3fPtr;
        internal static void loadProgramUniform3f()
        {
            try
            {
                glProgramUniform3fPtr = (glProgramUniform3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3f"), typeof(glProgramUniform3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3f'.");
            }
        }
        public static void glProgramUniform3f(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2) => glProgramUniform3fPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform3fvFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform3fvFunc glProgramUniform3fvPtr;
        internal static void loadProgramUniform3fv()
        {
            try
            {
                glProgramUniform3fvPtr = (glProgramUniform3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3fv"), typeof(glProgramUniform3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3fv'.");
            }
        }
        public static void glProgramUniform3fv(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform3fvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3dFunc(GLuint @program, GLint @location, GLdouble @v0, GLdouble @v1, GLdouble @v2);
        internal static glProgramUniform3dFunc glProgramUniform3dPtr;
        internal static void loadProgramUniform3d()
        {
            try
            {
                glProgramUniform3dPtr = (glProgramUniform3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3d"), typeof(glProgramUniform3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3d'.");
            }
        }
        public static void glProgramUniform3d(GLuint @program, GLint @location, GLdouble @v0, GLdouble @v1, GLdouble @v2) => glProgramUniform3dPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform3dvFunc(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glProgramUniform3dvFunc glProgramUniform3dvPtr;
        internal static void loadProgramUniform3dv()
        {
            try
            {
                glProgramUniform3dvPtr = (glProgramUniform3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3dv"), typeof(glProgramUniform3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3dv'.");
            }
        }
        public static void glProgramUniform3dv(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value) => glProgramUniform3dvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform3uiFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2);
        internal static glProgramUniform3uiFunc glProgramUniform3uiPtr;
        internal static void loadProgramUniform3ui()
        {
            try
            {
                glProgramUniform3uiPtr = (glProgramUniform3uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3ui"), typeof(glProgramUniform3uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3ui'.");
            }
        }
        public static void glProgramUniform3ui(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2) => glProgramUniform3uiPtr(@program, @location, @v0, @v1, @v2);

        internal delegate void glProgramUniform3uivFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform3uivFunc glProgramUniform3uivPtr;
        internal static void loadProgramUniform3uiv()
        {
            try
            {
                glProgramUniform3uivPtr = (glProgramUniform3uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform3uiv"), typeof(glProgramUniform3uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform3uiv'.");
            }
        }
        public static void glProgramUniform3uiv(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform3uivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4iFunc(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3);
        internal static glProgramUniform4iFunc glProgramUniform4iPtr;
        internal static void loadProgramUniform4i()
        {
            try
            {
                glProgramUniform4iPtr = (glProgramUniform4iFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4i"), typeof(glProgramUniform4iFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4i'.");
            }
        }
        public static void glProgramUniform4i(GLuint @program, GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3) => glProgramUniform4iPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform4ivFunc(GLuint @program, GLint @location, GLsizei @count, const GLint * @value);
        internal static glProgramUniform4ivFunc glProgramUniform4ivPtr;
        internal static void loadProgramUniform4iv()
        {
            try
            {
                glProgramUniform4ivPtr = (glProgramUniform4ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4iv"), typeof(glProgramUniform4ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4iv'.");
            }
        }
        public static void glProgramUniform4iv(GLuint @program, GLint @location, GLsizei @count, const GLint * @value) => glProgramUniform4ivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4fFunc(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3);
        internal static glProgramUniform4fFunc glProgramUniform4fPtr;
        internal static void loadProgramUniform4f()
        {
            try
            {
                glProgramUniform4fPtr = (glProgramUniform4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4f"), typeof(glProgramUniform4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4f'.");
            }
        }
        public static void glProgramUniform4f(GLuint @program, GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3) => glProgramUniform4fPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform4fvFunc(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glProgramUniform4fvFunc glProgramUniform4fvPtr;
        internal static void loadProgramUniform4fv()
        {
            try
            {
                glProgramUniform4fvPtr = (glProgramUniform4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4fv"), typeof(glProgramUniform4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4fv'.");
            }
        }
        public static void glProgramUniform4fv(GLuint @program, GLint @location, GLsizei @count, const GLfloat * @value) => glProgramUniform4fvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4dFunc(GLuint @program, GLint @location, GLdouble @v0, GLdouble @v1, GLdouble @v2, GLdouble @v3);
        internal static glProgramUniform4dFunc glProgramUniform4dPtr;
        internal static void loadProgramUniform4d()
        {
            try
            {
                glProgramUniform4dPtr = (glProgramUniform4dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4d"), typeof(glProgramUniform4dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4d'.");
            }
        }
        public static void glProgramUniform4d(GLuint @program, GLint @location, GLdouble @v0, GLdouble @v1, GLdouble @v2, GLdouble @v3) => glProgramUniform4dPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform4dvFunc(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value);
        internal static glProgramUniform4dvFunc glProgramUniform4dvPtr;
        internal static void loadProgramUniform4dv()
        {
            try
            {
                glProgramUniform4dvPtr = (glProgramUniform4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4dv"), typeof(glProgramUniform4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4dv'.");
            }
        }
        public static void glProgramUniform4dv(GLuint @program, GLint @location, GLsizei @count, const GLdouble * @value) => glProgramUniform4dvPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniform4uiFunc(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3);
        internal static glProgramUniform4uiFunc glProgramUniform4uiPtr;
        internal static void loadProgramUniform4ui()
        {
            try
            {
                glProgramUniform4uiPtr = (glProgramUniform4uiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4ui"), typeof(glProgramUniform4uiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4ui'.");
            }
        }
        public static void glProgramUniform4ui(GLuint @program, GLint @location, GLuint @v0, GLuint @v1, GLuint @v2, GLuint @v3) => glProgramUniform4uiPtr(@program, @location, @v0, @v1, @v2, @v3);

        internal delegate void glProgramUniform4uivFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value);
        internal static glProgramUniform4uivFunc glProgramUniform4uivPtr;
        internal static void loadProgramUniform4uiv()
        {
            try
            {
                glProgramUniform4uivPtr = (glProgramUniform4uivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniform4uiv"), typeof(glProgramUniform4uivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniform4uiv'.");
            }
        }
        public static void glProgramUniform4uiv(GLuint @program, GLint @location, GLsizei @count, const GLuint * @value) => glProgramUniform4uivPtr(@program, @location, @count, @value);

        internal delegate void glProgramUniformMatrix2fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2fvFunc glProgramUniformMatrix2fvPtr;
        internal static void loadProgramUniformMatrix2fv()
        {
            try
            {
                glProgramUniformMatrix2fvPtr = (glProgramUniformMatrix2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2fv"), typeof(glProgramUniformMatrix2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2fv'.");
            }
        }
        public static void glProgramUniformMatrix2fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3fvFunc glProgramUniformMatrix3fvPtr;
        internal static void loadProgramUniformMatrix3fv()
        {
            try
            {
                glProgramUniformMatrix3fvPtr = (glProgramUniformMatrix3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3fv"), typeof(glProgramUniformMatrix3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3fv'.");
            }
        }
        public static void glProgramUniformMatrix3fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4fvFunc glProgramUniformMatrix4fvPtr;
        internal static void loadProgramUniformMatrix4fv()
        {
            try
            {
                glProgramUniformMatrix4fvPtr = (glProgramUniformMatrix4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4fv"), typeof(glProgramUniformMatrix4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4fv'.");
            }
        }
        public static void glProgramUniformMatrix4fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix2dvFunc glProgramUniformMatrix2dvPtr;
        internal static void loadProgramUniformMatrix2dv()
        {
            try
            {
                glProgramUniformMatrix2dvPtr = (glProgramUniformMatrix2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2dv"), typeof(glProgramUniformMatrix2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2dv'.");
            }
        }
        public static void glProgramUniformMatrix2dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix2dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix3dvFunc glProgramUniformMatrix3dvPtr;
        internal static void loadProgramUniformMatrix3dv()
        {
            try
            {
                glProgramUniformMatrix3dvPtr = (glProgramUniformMatrix3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3dv"), typeof(glProgramUniformMatrix3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3dv'.");
            }
        }
        public static void glProgramUniformMatrix3dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix3dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix4dvFunc glProgramUniformMatrix4dvPtr;
        internal static void loadProgramUniformMatrix4dv()
        {
            try
            {
                glProgramUniformMatrix4dvPtr = (glProgramUniformMatrix4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4dv"), typeof(glProgramUniformMatrix4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4dv'.");
            }
        }
        public static void glProgramUniformMatrix4dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix4dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x3fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2x3fvFunc glProgramUniformMatrix2x3fvPtr;
        internal static void loadProgramUniformMatrix2x3fv()
        {
            try
            {
                glProgramUniformMatrix2x3fvPtr = (glProgramUniformMatrix2x3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x3fv"), typeof(glProgramUniformMatrix2x3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x3fv'.");
            }
        }
        public static void glProgramUniformMatrix2x3fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2x3fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x2fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3x2fvFunc glProgramUniformMatrix3x2fvPtr;
        internal static void loadProgramUniformMatrix3x2fv()
        {
            try
            {
                glProgramUniformMatrix3x2fvPtr = (glProgramUniformMatrix3x2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x2fv"), typeof(glProgramUniformMatrix3x2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x2fv'.");
            }
        }
        public static void glProgramUniformMatrix3x2fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3x2fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x4fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix2x4fvFunc glProgramUniformMatrix2x4fvPtr;
        internal static void loadProgramUniformMatrix2x4fv()
        {
            try
            {
                glProgramUniformMatrix2x4fvPtr = (glProgramUniformMatrix2x4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x4fv"), typeof(glProgramUniformMatrix2x4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x4fv'.");
            }
        }
        public static void glProgramUniformMatrix2x4fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix2x4fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x2fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4x2fvFunc glProgramUniformMatrix4x2fvPtr;
        internal static void loadProgramUniformMatrix4x2fv()
        {
            try
            {
                glProgramUniformMatrix4x2fvPtr = (glProgramUniformMatrix4x2fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x2fv"), typeof(glProgramUniformMatrix4x2fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x2fv'.");
            }
        }
        public static void glProgramUniformMatrix4x2fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4x2fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x4fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix3x4fvFunc glProgramUniformMatrix3x4fvPtr;
        internal static void loadProgramUniformMatrix3x4fv()
        {
            try
            {
                glProgramUniformMatrix3x4fvPtr = (glProgramUniformMatrix3x4fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x4fv"), typeof(glProgramUniformMatrix3x4fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x4fv'.");
            }
        }
        public static void glProgramUniformMatrix3x4fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix3x4fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x3fvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glProgramUniformMatrix4x3fvFunc glProgramUniformMatrix4x3fvPtr;
        internal static void loadProgramUniformMatrix4x3fv()
        {
            try
            {
                glProgramUniformMatrix4x3fvPtr = (glProgramUniformMatrix4x3fvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x3fv"), typeof(glProgramUniformMatrix4x3fvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x3fv'.");
            }
        }
        public static void glProgramUniformMatrix4x3fv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glProgramUniformMatrix4x3fvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x3dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix2x3dvFunc glProgramUniformMatrix2x3dvPtr;
        internal static void loadProgramUniformMatrix2x3dv()
        {
            try
            {
                glProgramUniformMatrix2x3dvPtr = (glProgramUniformMatrix2x3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x3dv"), typeof(glProgramUniformMatrix2x3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x3dv'.");
            }
        }
        public static void glProgramUniformMatrix2x3dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix2x3dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x2dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix3x2dvFunc glProgramUniformMatrix3x2dvPtr;
        internal static void loadProgramUniformMatrix3x2dv()
        {
            try
            {
                glProgramUniformMatrix3x2dvPtr = (glProgramUniformMatrix3x2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x2dv"), typeof(glProgramUniformMatrix3x2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x2dv'.");
            }
        }
        public static void glProgramUniformMatrix3x2dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix3x2dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix2x4dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix2x4dvFunc glProgramUniformMatrix2x4dvPtr;
        internal static void loadProgramUniformMatrix2x4dv()
        {
            try
            {
                glProgramUniformMatrix2x4dvPtr = (glProgramUniformMatrix2x4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix2x4dv"), typeof(glProgramUniformMatrix2x4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix2x4dv'.");
            }
        }
        public static void glProgramUniformMatrix2x4dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix2x4dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x2dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix4x2dvFunc glProgramUniformMatrix4x2dvPtr;
        internal static void loadProgramUniformMatrix4x2dv()
        {
            try
            {
                glProgramUniformMatrix4x2dvPtr = (glProgramUniformMatrix4x2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x2dv"), typeof(glProgramUniformMatrix4x2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x2dv'.");
            }
        }
        public static void glProgramUniformMatrix4x2dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix4x2dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix3x4dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix3x4dvFunc glProgramUniformMatrix3x4dvPtr;
        internal static void loadProgramUniformMatrix3x4dv()
        {
            try
            {
                glProgramUniformMatrix3x4dvPtr = (glProgramUniformMatrix3x4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix3x4dv"), typeof(glProgramUniformMatrix3x4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix3x4dv'.");
            }
        }
        public static void glProgramUniformMatrix3x4dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix3x4dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glProgramUniformMatrix4x3dvFunc(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value);
        internal static glProgramUniformMatrix4x3dvFunc glProgramUniformMatrix4x3dvPtr;
        internal static void loadProgramUniformMatrix4x3dv()
        {
            try
            {
                glProgramUniformMatrix4x3dvPtr = (glProgramUniformMatrix4x3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformMatrix4x3dv"), typeof(glProgramUniformMatrix4x3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformMatrix4x3dv'.");
            }
        }
        public static void glProgramUniformMatrix4x3dv(GLuint @program, GLint @location, GLsizei @count, GLboolean @transpose, const GLdouble * @value) => glProgramUniformMatrix4x3dvPtr(@program, @location, @count, @transpose, @value);

        internal delegate void glValidateProgramPipelineFunc(GLuint @pipeline);
        internal static glValidateProgramPipelineFunc glValidateProgramPipelinePtr;
        internal static void loadValidateProgramPipeline()
        {
            try
            {
                glValidateProgramPipelinePtr = (glValidateProgramPipelineFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glValidateProgramPipeline"), typeof(glValidateProgramPipelineFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glValidateProgramPipeline'.");
            }
        }
        public static void glValidateProgramPipeline(GLuint @pipeline) => glValidateProgramPipelinePtr(@pipeline);

        internal delegate void glGetProgramPipelineInfoLogFunc(GLuint @pipeline, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog);
        internal static glGetProgramPipelineInfoLogFunc glGetProgramPipelineInfoLogPtr;
        internal static void loadGetProgramPipelineInfoLog()
        {
            try
            {
                glGetProgramPipelineInfoLogPtr = (glGetProgramPipelineInfoLogFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramPipelineInfoLog"), typeof(glGetProgramPipelineInfoLogFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramPipelineInfoLog'.");
            }
        }
        public static void glGetProgramPipelineInfoLog(GLuint @pipeline, GLsizei @bufSize, GLsizei * @length, GLchar * @infoLog) => glGetProgramPipelineInfoLogPtr(@pipeline, @bufSize, @length, @infoLog);
        #endregion
    }
}
