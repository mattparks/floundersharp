using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_compute_shader
    {
        #region Interop
        static GL_ARB_compute_shader()
        {
            Console.WriteLine("Initalising GL_ARB_compute_shader interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDispatchCompute();
            loadDispatchComputeIndirect();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPUTE_SHADER = 0x91B9;
        public static UInt32 GL_MAX_COMPUTE_UNIFORM_BLOCKS = 0x91BB;
        public static UInt32 GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS = 0x91BC;
        public static UInt32 GL_MAX_COMPUTE_IMAGE_UNIFORMS = 0x91BD;
        public static UInt32 GL_MAX_COMPUTE_SHARED_MEMORY_SIZE = 0x8262;
        public static UInt32 GL_MAX_COMPUTE_UNIFORM_COMPONENTS = 0x8263;
        public static UInt32 GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS = 0x8264;
        public static UInt32 GL_MAX_COMPUTE_ATOMIC_COUNTERS = 0x8265;
        public static UInt32 GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS = 0x8266;
        public static UInt32 GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS = 0x90EB;
        public static UInt32 GL_MAX_COMPUTE_WORK_GROUP_COUNT = 0x91BE;
        public static UInt32 GL_MAX_COMPUTE_WORK_GROUP_SIZE = 0x91BF;
        public static UInt32 GL_COMPUTE_WORK_GROUP_SIZE = 0x8267;
        public static UInt32 GL_UNIFORM_BLOCK_REFERENCED_BY_COMPUTE_SHADER = 0x90EC;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_COMPUTE_SHADER = 0x90ED;
        public static UInt32 GL_DISPATCH_INDIRECT_BUFFER = 0x90EE;
        public static UInt32 GL_DISPATCH_INDIRECT_BUFFER_BINDING = 0x90EF;
        public static UInt32 GL_COMPUTE_SHADER_BIT = 0x00000020;
        #endregion

        #region Commands
        internal delegate void glDispatchComputeFunc(GLuint @num_groups_x, GLuint @num_groups_y, GLuint @num_groups_z);
        internal static glDispatchComputeFunc glDispatchComputePtr;
        internal static void loadDispatchCompute()
        {
            try
            {
                glDispatchComputePtr = (glDispatchComputeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDispatchCompute"), typeof(glDispatchComputeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDispatchCompute'.");
            }
        }
        public static void glDispatchCompute(GLuint @num_groups_x, GLuint @num_groups_y, GLuint @num_groups_z) => glDispatchComputePtr(@num_groups_x, @num_groups_y, @num_groups_z);

        internal delegate void glDispatchComputeIndirectFunc(GLintptr @indirect);
        internal static glDispatchComputeIndirectFunc glDispatchComputeIndirectPtr;
        internal static void loadDispatchComputeIndirect()
        {
            try
            {
                glDispatchComputeIndirectPtr = (glDispatchComputeIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDispatchComputeIndirect"), typeof(glDispatchComputeIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDispatchComputeIndirect'.");
            }
        }
        public static void glDispatchComputeIndirect(GLintptr @indirect) => glDispatchComputeIndirectPtr(@indirect);
        #endregion
    }
}
