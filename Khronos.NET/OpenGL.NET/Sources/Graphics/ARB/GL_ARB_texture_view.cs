using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_view
    {
        #region Interop
        static GL_ARB_texture_view()
        {
            Console.WriteLine("Initalising GL_ARB_texture_view interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTextureView();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_VIEW_MIN_LEVEL = 0x82DB;
        public static UInt32 GL_TEXTURE_VIEW_NUM_LEVELS = 0x82DC;
        public static UInt32 GL_TEXTURE_VIEW_MIN_LAYER = 0x82DD;
        public static UInt32 GL_TEXTURE_VIEW_NUM_LAYERS = 0x82DE;
        public static UInt32 GL_TEXTURE_IMMUTABLE_LEVELS = 0x82DF;
        #endregion

        #region Commands
        internal delegate void glTextureViewFunc(GLuint @texture, GLenum @target, GLuint @origtexture, GLenum @internalformat, GLuint @minlevel, GLuint @numlevels, GLuint @minlayer, GLuint @numlayers);
        internal static glTextureViewFunc glTextureViewPtr;
        internal static void loadTextureView()
        {
            try
            {
                glTextureViewPtr = (glTextureViewFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureView"), typeof(glTextureViewFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureView'.");
            }
        }
        public static void glTextureView(GLuint @texture, GLenum @target, GLuint @origtexture, GLenum @internalformat, GLuint @minlevel, GLuint @numlevels, GLuint @minlayer, GLuint @numlayers) => glTextureViewPtr(@texture, @target, @origtexture, @internalformat, @minlevel, @numlevels, @minlayer, @numlayers);
        #endregion
    }
}
