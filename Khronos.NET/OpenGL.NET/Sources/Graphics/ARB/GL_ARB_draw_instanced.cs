using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_draw_instanced
    {
        #region Interop
        static GL_ARB_draw_instanced()
        {
            Console.WriteLine("Initalising GL_ARB_draw_instanced interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawArraysInstancedARB();
            loadDrawElementsInstancedARB();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glDrawArraysInstancedARBFunc(GLenum @mode, GLint @first, GLsizei @count, GLsizei @primcount);
        internal static glDrawArraysInstancedARBFunc glDrawArraysInstancedARBPtr;
        internal static void loadDrawArraysInstancedARB()
        {
            try
            {
                glDrawArraysInstancedARBPtr = (glDrawArraysInstancedARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysInstancedARB"), typeof(glDrawArraysInstancedARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysInstancedARB'.");
            }
        }
        public static void glDrawArraysInstancedARB(GLenum @mode, GLint @first, GLsizei @count, GLsizei @primcount) => glDrawArraysInstancedARBPtr(@mode, @first, @count, @primcount);

        internal delegate void glDrawElementsInstancedARBFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @primcount);
        internal static glDrawElementsInstancedARBFunc glDrawElementsInstancedARBPtr;
        internal static void loadDrawElementsInstancedARB()
        {
            try
            {
                glDrawElementsInstancedARBPtr = (glDrawElementsInstancedARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsInstancedARB"), typeof(glDrawElementsInstancedARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsInstancedARB'.");
            }
        }
        public static void glDrawElementsInstancedARB(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices, GLsizei @primcount) => glDrawElementsInstancedARBPtr(@mode, @count, @type, @indices, @primcount);
        #endregion
    }
}
