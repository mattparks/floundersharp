using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_transform_feedback_overflow_query
    {
        #region Interop
        static GL_ARB_transform_feedback_overflow_query()
        {
            Console.WriteLine("Initalising GL_ARB_transform_feedback_overflow_query interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TRANSFORM_FEEDBACK_OVERFLOW_ARB = 0x82EC;
        public static UInt32 GL_TRANSFORM_FEEDBACK_STREAM_OVERFLOW_ARB = 0x82ED;
        #endregion

        #region Commands
        #endregion
    }
}
