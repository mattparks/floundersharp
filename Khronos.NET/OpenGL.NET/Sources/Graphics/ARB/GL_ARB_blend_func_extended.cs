using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_blend_func_extended
    {
        #region Interop
        static GL_ARB_blend_func_extended()
        {
            Console.WriteLine("Initalising GL_ARB_blend_func_extended interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindFragDataLocationIndexed();
            loadGetFragDataIndex();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SRC1_COLOR = 0x88F9;
        public static UInt32 GL_SRC1_ALPHA = 0x8589;
        public static UInt32 GL_ONE_MINUS_SRC1_COLOR = 0x88FA;
        public static UInt32 GL_ONE_MINUS_SRC1_ALPHA = 0x88FB;
        public static UInt32 GL_MAX_DUAL_SOURCE_DRAW_BUFFERS = 0x88FC;
        #endregion

        #region Commands
        internal delegate void glBindFragDataLocationIndexedFunc(GLuint @program, GLuint @colorNumber, GLuint @index, const GLchar * @name);
        internal static glBindFragDataLocationIndexedFunc glBindFragDataLocationIndexedPtr;
        internal static void loadBindFragDataLocationIndexed()
        {
            try
            {
                glBindFragDataLocationIndexedPtr = (glBindFragDataLocationIndexedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindFragDataLocationIndexed"), typeof(glBindFragDataLocationIndexedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindFragDataLocationIndexed'.");
            }
        }
        public static void glBindFragDataLocationIndexed(GLuint @program, GLuint @colorNumber, GLuint @index, const GLchar * @name) => glBindFragDataLocationIndexedPtr(@program, @colorNumber, @index, @name);

        internal delegate GLint glGetFragDataIndexFunc(GLuint @program, const GLchar * @name);
        internal static glGetFragDataIndexFunc glGetFragDataIndexPtr;
        internal static void loadGetFragDataIndex()
        {
            try
            {
                glGetFragDataIndexPtr = (glGetFragDataIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFragDataIndex"), typeof(glGetFragDataIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFragDataIndex'.");
            }
        }
        public static GLint glGetFragDataIndex(GLuint @program, const GLchar * @name) => glGetFragDataIndexPtr(@program, @name);
        #endregion
    }
}
