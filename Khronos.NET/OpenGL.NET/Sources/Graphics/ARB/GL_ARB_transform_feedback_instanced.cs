using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_transform_feedback_instanced
    {
        #region Interop
        static GL_ARB_transform_feedback_instanced()
        {
            Console.WriteLine("Initalising GL_ARB_transform_feedback_instanced interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawTransformFeedbackInstanced();
            loadDrawTransformFeedbackStreamInstanced();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glDrawTransformFeedbackInstancedFunc(GLenum @mode, GLuint @id, GLsizei @instancecount);
        internal static glDrawTransformFeedbackInstancedFunc glDrawTransformFeedbackInstancedPtr;
        internal static void loadDrawTransformFeedbackInstanced()
        {
            try
            {
                glDrawTransformFeedbackInstancedPtr = (glDrawTransformFeedbackInstancedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTransformFeedbackInstanced"), typeof(glDrawTransformFeedbackInstancedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTransformFeedbackInstanced'.");
            }
        }
        public static void glDrawTransformFeedbackInstanced(GLenum @mode, GLuint @id, GLsizei @instancecount) => glDrawTransformFeedbackInstancedPtr(@mode, @id, @instancecount);

        internal delegate void glDrawTransformFeedbackStreamInstancedFunc(GLenum @mode, GLuint @id, GLuint @stream, GLsizei @instancecount);
        internal static glDrawTransformFeedbackStreamInstancedFunc glDrawTransformFeedbackStreamInstancedPtr;
        internal static void loadDrawTransformFeedbackStreamInstanced()
        {
            try
            {
                glDrawTransformFeedbackStreamInstancedPtr = (glDrawTransformFeedbackStreamInstancedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTransformFeedbackStreamInstanced"), typeof(glDrawTransformFeedbackStreamInstancedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTransformFeedbackStreamInstanced'.");
            }
        }
        public static void glDrawTransformFeedbackStreamInstanced(GLenum @mode, GLuint @id, GLuint @stream, GLsizei @instancecount) => glDrawTransformFeedbackStreamInstancedPtr(@mode, @id, @stream, @instancecount);
        #endregion
    }
}
