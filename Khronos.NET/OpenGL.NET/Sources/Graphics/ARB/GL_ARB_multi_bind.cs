using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_multi_bind
    {
        #region Interop
        static GL_ARB_multi_bind()
        {
            Console.WriteLine("Initalising GL_ARB_multi_bind interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindBuffersBase();
            loadBindBuffersRange();
            loadBindTextures();
            loadBindSamplers();
            loadBindImageTextures();
            loadBindVertexBuffers();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glBindBuffersBaseFunc(GLenum @target, GLuint @first, GLsizei @count, const GLuint * @buffers);
        internal static glBindBuffersBaseFunc glBindBuffersBasePtr;
        internal static void loadBindBuffersBase()
        {
            try
            {
                glBindBuffersBasePtr = (glBindBuffersBaseFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBuffersBase"), typeof(glBindBuffersBaseFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBuffersBase'.");
            }
        }
        public static void glBindBuffersBase(GLenum @target, GLuint @first, GLsizei @count, const GLuint * @buffers) => glBindBuffersBasePtr(@target, @first, @count, @buffers);

        internal delegate void glBindBuffersRangeFunc(GLenum @target, GLuint @first, GLsizei @count, const GLuint * @buffers, const GLintptr * @offsets, const GLsizeiptr * @sizes);
        internal static glBindBuffersRangeFunc glBindBuffersRangePtr;
        internal static void loadBindBuffersRange()
        {
            try
            {
                glBindBuffersRangePtr = (glBindBuffersRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBuffersRange"), typeof(glBindBuffersRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBuffersRange'.");
            }
        }
        public static void glBindBuffersRange(GLenum @target, GLuint @first, GLsizei @count, const GLuint * @buffers, const GLintptr * @offsets, const GLsizeiptr * @sizes) => glBindBuffersRangePtr(@target, @first, @count, @buffers, @offsets, @sizes);

        internal delegate void glBindTexturesFunc(GLuint @first, GLsizei @count, const GLuint * @textures);
        internal static glBindTexturesFunc glBindTexturesPtr;
        internal static void loadBindTextures()
        {
            try
            {
                glBindTexturesPtr = (glBindTexturesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTextures"), typeof(glBindTexturesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTextures'.");
            }
        }
        public static void glBindTextures(GLuint @first, GLsizei @count, const GLuint * @textures) => glBindTexturesPtr(@first, @count, @textures);

        internal delegate void glBindSamplersFunc(GLuint @first, GLsizei @count, const GLuint * @samplers);
        internal static glBindSamplersFunc glBindSamplersPtr;
        internal static void loadBindSamplers()
        {
            try
            {
                glBindSamplersPtr = (glBindSamplersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindSamplers"), typeof(glBindSamplersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindSamplers'.");
            }
        }
        public static void glBindSamplers(GLuint @first, GLsizei @count, const GLuint * @samplers) => glBindSamplersPtr(@first, @count, @samplers);

        internal delegate void glBindImageTexturesFunc(GLuint @first, GLsizei @count, const GLuint * @textures);
        internal static glBindImageTexturesFunc glBindImageTexturesPtr;
        internal static void loadBindImageTextures()
        {
            try
            {
                glBindImageTexturesPtr = (glBindImageTexturesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindImageTextures"), typeof(glBindImageTexturesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindImageTextures'.");
            }
        }
        public static void glBindImageTextures(GLuint @first, GLsizei @count, const GLuint * @textures) => glBindImageTexturesPtr(@first, @count, @textures);

        internal delegate void glBindVertexBuffersFunc(GLuint @first, GLsizei @count, const GLuint * @buffers, const GLintptr * @offsets, const GLsizei * @strides);
        internal static glBindVertexBuffersFunc glBindVertexBuffersPtr;
        internal static void loadBindVertexBuffers()
        {
            try
            {
                glBindVertexBuffersPtr = (glBindVertexBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindVertexBuffers"), typeof(glBindVertexBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindVertexBuffers'.");
            }
        }
        public static void glBindVertexBuffers(GLuint @first, GLsizei @count, const GLuint * @buffers, const GLintptr * @offsets, const GLsizei * @strides) => glBindVertexBuffersPtr(@first, @count, @buffers, @offsets, @strides);
        #endregion
    }
}
