using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_robustness
    {
        #region Interop
        static GL_ARB_robustness()
        {
            Console.WriteLine("Initalising GL_ARB_robustness interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetGraphicsResetStatusARB();
            loadGetnTexImageARB();
            loadReadnPixelsARB();
            loadGetnCompressedTexImageARB();
            loadGetnUniformfvARB();
            loadGetnUniformivARB();
            loadGetnUniformuivARB();
            loadGetnUniformdvARB();
            loadGetnMapdvARB();
            loadGetnMapfvARB();
            loadGetnMapivARB();
            loadGetnPixelMapfvARB();
            loadGetnPixelMapuivARB();
            loadGetnPixelMapusvARB();
            loadGetnPolygonStippleARB();
            loadGetnColorTableARB();
            loadGetnConvolutionFilterARB();
            loadGetnSeparableFilterARB();
            loadGetnHistogramARB();
            loadGetnMinmaxARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_NO_ERROR = 0;
        public static UInt32 GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT_ARB = 0x00000004;
        public static UInt32 GL_LOSE_CONTEXT_ON_RESET_ARB = 0x8252;
        public static UInt32 GL_GUILTY_CONTEXT_RESET_ARB = 0x8253;
        public static UInt32 GL_INNOCENT_CONTEXT_RESET_ARB = 0x8254;
        public static UInt32 GL_UNKNOWN_CONTEXT_RESET_ARB = 0x8255;
        public static UInt32 GL_RESET_NOTIFICATION_STRATEGY_ARB = 0x8256;
        public static UInt32 GL_NO_RESET_NOTIFICATION_ARB = 0x8261;
        #endregion

        #region Commands
        internal delegate GLenum glGetGraphicsResetStatusARBFunc();
        internal static glGetGraphicsResetStatusARBFunc glGetGraphicsResetStatusARBPtr;
        internal static void loadGetGraphicsResetStatusARB()
        {
            try
            {
                glGetGraphicsResetStatusARBPtr = (glGetGraphicsResetStatusARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetGraphicsResetStatusARB"), typeof(glGetGraphicsResetStatusARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetGraphicsResetStatusARB'.");
            }
        }
        public static GLenum glGetGraphicsResetStatusARB() => glGetGraphicsResetStatusARBPtr();

        internal delegate void glGetnTexImageARBFunc(GLenum @target, GLint @level, GLenum @format, GLenum @type, GLsizei @bufSize, void * @img);
        internal static glGetnTexImageARBFunc glGetnTexImageARBPtr;
        internal static void loadGetnTexImageARB()
        {
            try
            {
                glGetnTexImageARBPtr = (glGetnTexImageARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnTexImageARB"), typeof(glGetnTexImageARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnTexImageARB'.");
            }
        }
        public static void glGetnTexImageARB(GLenum @target, GLint @level, GLenum @format, GLenum @type, GLsizei @bufSize, void * @img) => glGetnTexImageARBPtr(@target, @level, @format, @type, @bufSize, @img);

        internal delegate void glReadnPixelsARBFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, GLsizei @bufSize, void * @data);
        internal static glReadnPixelsARBFunc glReadnPixelsARBPtr;
        internal static void loadReadnPixelsARB()
        {
            try
            {
                glReadnPixelsARBPtr = (glReadnPixelsARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadnPixelsARB"), typeof(glReadnPixelsARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadnPixelsARB'.");
            }
        }
        public static void glReadnPixelsARB(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, GLsizei @bufSize, void * @data) => glReadnPixelsARBPtr(@x, @y, @width, @height, @format, @type, @bufSize, @data);

        internal delegate void glGetnCompressedTexImageARBFunc(GLenum @target, GLint @lod, GLsizei @bufSize, void * @img);
        internal static glGetnCompressedTexImageARBFunc glGetnCompressedTexImageARBPtr;
        internal static void loadGetnCompressedTexImageARB()
        {
            try
            {
                glGetnCompressedTexImageARBPtr = (glGetnCompressedTexImageARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnCompressedTexImageARB"), typeof(glGetnCompressedTexImageARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnCompressedTexImageARB'.");
            }
        }
        public static void glGetnCompressedTexImageARB(GLenum @target, GLint @lod, GLsizei @bufSize, void * @img) => glGetnCompressedTexImageARBPtr(@target, @lod, @bufSize, @img);

        internal delegate void glGetnUniformfvARBFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLfloat * @params);
        internal static glGetnUniformfvARBFunc glGetnUniformfvARBPtr;
        internal static void loadGetnUniformfvARB()
        {
            try
            {
                glGetnUniformfvARBPtr = (glGetnUniformfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformfvARB"), typeof(glGetnUniformfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformfvARB'.");
            }
        }
        public static void glGetnUniformfvARB(GLuint @program, GLint @location, GLsizei @bufSize, GLfloat * @params) => glGetnUniformfvARBPtr(@program, @location, @bufSize, @params);

        internal delegate void glGetnUniformivARBFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLint * @params);
        internal static glGetnUniformivARBFunc glGetnUniformivARBPtr;
        internal static void loadGetnUniformivARB()
        {
            try
            {
                glGetnUniformivARBPtr = (glGetnUniformivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformivARB"), typeof(glGetnUniformivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformivARB'.");
            }
        }
        public static void glGetnUniformivARB(GLuint @program, GLint @location, GLsizei @bufSize, GLint * @params) => glGetnUniformivARBPtr(@program, @location, @bufSize, @params);

        internal delegate void glGetnUniformuivARBFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLuint * @params);
        internal static glGetnUniformuivARBFunc glGetnUniformuivARBPtr;
        internal static void loadGetnUniformuivARB()
        {
            try
            {
                glGetnUniformuivARBPtr = (glGetnUniformuivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformuivARB"), typeof(glGetnUniformuivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformuivARB'.");
            }
        }
        public static void glGetnUniformuivARB(GLuint @program, GLint @location, GLsizei @bufSize, GLuint * @params) => glGetnUniformuivARBPtr(@program, @location, @bufSize, @params);

        internal delegate void glGetnUniformdvARBFunc(GLuint @program, GLint @location, GLsizei @bufSize, GLdouble * @params);
        internal static glGetnUniformdvARBFunc glGetnUniformdvARBPtr;
        internal static void loadGetnUniformdvARB()
        {
            try
            {
                glGetnUniformdvARBPtr = (glGetnUniformdvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnUniformdvARB"), typeof(glGetnUniformdvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnUniformdvARB'.");
            }
        }
        public static void glGetnUniformdvARB(GLuint @program, GLint @location, GLsizei @bufSize, GLdouble * @params) => glGetnUniformdvARBPtr(@program, @location, @bufSize, @params);

        internal delegate void glGetnMapdvARBFunc(GLenum @target, GLenum @query, GLsizei @bufSize, GLdouble * @v);
        internal static glGetnMapdvARBFunc glGetnMapdvARBPtr;
        internal static void loadGetnMapdvARB()
        {
            try
            {
                glGetnMapdvARBPtr = (glGetnMapdvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnMapdvARB"), typeof(glGetnMapdvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnMapdvARB'.");
            }
        }
        public static void glGetnMapdvARB(GLenum @target, GLenum @query, GLsizei @bufSize, GLdouble * @v) => glGetnMapdvARBPtr(@target, @query, @bufSize, @v);

        internal delegate void glGetnMapfvARBFunc(GLenum @target, GLenum @query, GLsizei @bufSize, GLfloat * @v);
        internal static glGetnMapfvARBFunc glGetnMapfvARBPtr;
        internal static void loadGetnMapfvARB()
        {
            try
            {
                glGetnMapfvARBPtr = (glGetnMapfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnMapfvARB"), typeof(glGetnMapfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnMapfvARB'.");
            }
        }
        public static void glGetnMapfvARB(GLenum @target, GLenum @query, GLsizei @bufSize, GLfloat * @v) => glGetnMapfvARBPtr(@target, @query, @bufSize, @v);

        internal delegate void glGetnMapivARBFunc(GLenum @target, GLenum @query, GLsizei @bufSize, GLint * @v);
        internal static glGetnMapivARBFunc glGetnMapivARBPtr;
        internal static void loadGetnMapivARB()
        {
            try
            {
                glGetnMapivARBPtr = (glGetnMapivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnMapivARB"), typeof(glGetnMapivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnMapivARB'.");
            }
        }
        public static void glGetnMapivARB(GLenum @target, GLenum @query, GLsizei @bufSize, GLint * @v) => glGetnMapivARBPtr(@target, @query, @bufSize, @v);

        internal delegate void glGetnPixelMapfvARBFunc(GLenum @map, GLsizei @bufSize, GLfloat * @values);
        internal static glGetnPixelMapfvARBFunc glGetnPixelMapfvARBPtr;
        internal static void loadGetnPixelMapfvARB()
        {
            try
            {
                glGetnPixelMapfvARBPtr = (glGetnPixelMapfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnPixelMapfvARB"), typeof(glGetnPixelMapfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnPixelMapfvARB'.");
            }
        }
        public static void glGetnPixelMapfvARB(GLenum @map, GLsizei @bufSize, GLfloat * @values) => glGetnPixelMapfvARBPtr(@map, @bufSize, @values);

        internal delegate void glGetnPixelMapuivARBFunc(GLenum @map, GLsizei @bufSize, GLuint * @values);
        internal static glGetnPixelMapuivARBFunc glGetnPixelMapuivARBPtr;
        internal static void loadGetnPixelMapuivARB()
        {
            try
            {
                glGetnPixelMapuivARBPtr = (glGetnPixelMapuivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnPixelMapuivARB"), typeof(glGetnPixelMapuivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnPixelMapuivARB'.");
            }
        }
        public static void glGetnPixelMapuivARB(GLenum @map, GLsizei @bufSize, GLuint * @values) => glGetnPixelMapuivARBPtr(@map, @bufSize, @values);

        internal delegate void glGetnPixelMapusvARBFunc(GLenum @map, GLsizei @bufSize, GLushort * @values);
        internal static glGetnPixelMapusvARBFunc glGetnPixelMapusvARBPtr;
        internal static void loadGetnPixelMapusvARB()
        {
            try
            {
                glGetnPixelMapusvARBPtr = (glGetnPixelMapusvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnPixelMapusvARB"), typeof(glGetnPixelMapusvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnPixelMapusvARB'.");
            }
        }
        public static void glGetnPixelMapusvARB(GLenum @map, GLsizei @bufSize, GLushort * @values) => glGetnPixelMapusvARBPtr(@map, @bufSize, @values);

        internal delegate void glGetnPolygonStippleARBFunc(GLsizei @bufSize, GLubyte * @pattern);
        internal static glGetnPolygonStippleARBFunc glGetnPolygonStippleARBPtr;
        internal static void loadGetnPolygonStippleARB()
        {
            try
            {
                glGetnPolygonStippleARBPtr = (glGetnPolygonStippleARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnPolygonStippleARB"), typeof(glGetnPolygonStippleARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnPolygonStippleARB'.");
            }
        }
        public static void glGetnPolygonStippleARB(GLsizei @bufSize, GLubyte * @pattern) => glGetnPolygonStippleARBPtr(@bufSize, @pattern);

        internal delegate void glGetnColorTableARBFunc(GLenum @target, GLenum @format, GLenum @type, GLsizei @bufSize, void * @table);
        internal static glGetnColorTableARBFunc glGetnColorTableARBPtr;
        internal static void loadGetnColorTableARB()
        {
            try
            {
                glGetnColorTableARBPtr = (glGetnColorTableARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnColorTableARB"), typeof(glGetnColorTableARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnColorTableARB'.");
            }
        }
        public static void glGetnColorTableARB(GLenum @target, GLenum @format, GLenum @type, GLsizei @bufSize, void * @table) => glGetnColorTableARBPtr(@target, @format, @type, @bufSize, @table);

        internal delegate void glGetnConvolutionFilterARBFunc(GLenum @target, GLenum @format, GLenum @type, GLsizei @bufSize, void * @image);
        internal static glGetnConvolutionFilterARBFunc glGetnConvolutionFilterARBPtr;
        internal static void loadGetnConvolutionFilterARB()
        {
            try
            {
                glGetnConvolutionFilterARBPtr = (glGetnConvolutionFilterARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnConvolutionFilterARB"), typeof(glGetnConvolutionFilterARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnConvolutionFilterARB'.");
            }
        }
        public static void glGetnConvolutionFilterARB(GLenum @target, GLenum @format, GLenum @type, GLsizei @bufSize, void * @image) => glGetnConvolutionFilterARBPtr(@target, @format, @type, @bufSize, @image);

        internal delegate void glGetnSeparableFilterARBFunc(GLenum @target, GLenum @format, GLenum @type, GLsizei @rowBufSize, void * @row, GLsizei @columnBufSize, void * @column, void * @span);
        internal static glGetnSeparableFilterARBFunc glGetnSeparableFilterARBPtr;
        internal static void loadGetnSeparableFilterARB()
        {
            try
            {
                glGetnSeparableFilterARBPtr = (glGetnSeparableFilterARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnSeparableFilterARB"), typeof(glGetnSeparableFilterARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnSeparableFilterARB'.");
            }
        }
        public static void glGetnSeparableFilterARB(GLenum @target, GLenum @format, GLenum @type, GLsizei @rowBufSize, void * @row, GLsizei @columnBufSize, void * @column, void * @span) => glGetnSeparableFilterARBPtr(@target, @format, @type, @rowBufSize, @row, @columnBufSize, @column, @span);

        internal delegate void glGetnHistogramARBFunc(GLenum @target, GLboolean @reset, GLenum @format, GLenum @type, GLsizei @bufSize, void * @values);
        internal static glGetnHistogramARBFunc glGetnHistogramARBPtr;
        internal static void loadGetnHistogramARB()
        {
            try
            {
                glGetnHistogramARBPtr = (glGetnHistogramARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnHistogramARB"), typeof(glGetnHistogramARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnHistogramARB'.");
            }
        }
        public static void glGetnHistogramARB(GLenum @target, GLboolean @reset, GLenum @format, GLenum @type, GLsizei @bufSize, void * @values) => glGetnHistogramARBPtr(@target, @reset, @format, @type, @bufSize, @values);

        internal delegate void glGetnMinmaxARBFunc(GLenum @target, GLboolean @reset, GLenum @format, GLenum @type, GLsizei @bufSize, void * @values);
        internal static glGetnMinmaxARBFunc glGetnMinmaxARBPtr;
        internal static void loadGetnMinmaxARB()
        {
            try
            {
                glGetnMinmaxARBPtr = (glGetnMinmaxARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetnMinmaxARB"), typeof(glGetnMinmaxARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetnMinmaxARB'.");
            }
        }
        public static void glGetnMinmaxARB(GLenum @target, GLboolean @reset, GLenum @format, GLenum @type, GLsizei @bufSize, void * @values) => glGetnMinmaxARBPtr(@target, @reset, @format, @type, @bufSize, @values);
        #endregion
    }
}
