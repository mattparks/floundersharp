using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_shader_atomic_counters
    {
        #region Interop
        static GL_ARB_shader_atomic_counters()
        {
            Console.WriteLine("Initalising GL_ARB_shader_atomic_counters interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetActiveAtomicCounterBufferiv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER = 0x92C0;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_BINDING = 0x92C1;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_START = 0x92C2;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_SIZE = 0x92C3;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_DATA_SIZE = 0x92C4;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_ACTIVE_ATOMIC_COUNTERS = 0x92C5;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_ACTIVE_ATOMIC_COUNTER_INDICES = 0x92C6;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_VERTEX_SHADER = 0x92C7;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_TESS_CONTROL_SHADER = 0x92C8;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_TESS_EVALUATION_SHADER = 0x92C9;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_GEOMETRY_SHADER = 0x92CA;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_FRAGMENT_SHADER = 0x92CB;
        public static UInt32 GL_MAX_VERTEX_ATOMIC_COUNTER_BUFFERS = 0x92CC;
        public static UInt32 GL_MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS = 0x92CD;
        public static UInt32 GL_MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS = 0x92CE;
        public static UInt32 GL_MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS = 0x92CF;
        public static UInt32 GL_MAX_FRAGMENT_ATOMIC_COUNTER_BUFFERS = 0x92D0;
        public static UInt32 GL_MAX_COMBINED_ATOMIC_COUNTER_BUFFERS = 0x92D1;
        public static UInt32 GL_MAX_VERTEX_ATOMIC_COUNTERS = 0x92D2;
        public static UInt32 GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS = 0x92D3;
        public static UInt32 GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS = 0x92D4;
        public static UInt32 GL_MAX_GEOMETRY_ATOMIC_COUNTERS = 0x92D5;
        public static UInt32 GL_MAX_FRAGMENT_ATOMIC_COUNTERS = 0x92D6;
        public static UInt32 GL_MAX_COMBINED_ATOMIC_COUNTERS = 0x92D7;
        public static UInt32 GL_MAX_ATOMIC_COUNTER_BUFFER_SIZE = 0x92D8;
        public static UInt32 GL_MAX_ATOMIC_COUNTER_BUFFER_BINDINGS = 0x92DC;
        public static UInt32 GL_ACTIVE_ATOMIC_COUNTER_BUFFERS = 0x92D9;
        public static UInt32 GL_UNIFORM_ATOMIC_COUNTER_BUFFER_INDEX = 0x92DA;
        public static UInt32 GL_UNSIGNED_INT_ATOMIC_COUNTER = 0x92DB;
        #endregion

        #region Commands
        internal delegate void glGetActiveAtomicCounterBufferivFunc(GLuint @program, GLuint @bufferIndex, GLenum @pname, GLint * @params);
        internal static glGetActiveAtomicCounterBufferivFunc glGetActiveAtomicCounterBufferivPtr;
        internal static void loadGetActiveAtomicCounterBufferiv()
        {
            try
            {
                glGetActiveAtomicCounterBufferivPtr = (glGetActiveAtomicCounterBufferivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveAtomicCounterBufferiv"), typeof(glGetActiveAtomicCounterBufferivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveAtomicCounterBufferiv'.");
            }
        }
        public static void glGetActiveAtomicCounterBufferiv(GLuint @program, GLuint @bufferIndex, GLenum @pname, GLint * @params) => glGetActiveAtomicCounterBufferivPtr(@program, @bufferIndex, @pname, @params);
        #endregion
    }
}
