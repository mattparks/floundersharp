using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_clear_texture
    {
        #region Interop
        static GL_ARB_clear_texture()
        {
            Console.WriteLine("Initalising GL_ARB_clear_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadClearTexImage();
            loadClearTexSubImage();
        }
        #endregion

        #region Enums
        public static UInt32 GL_CLEAR_TEXTURE = 0x9365;
        #endregion

        #region Commands
        internal delegate void glClearTexImageFunc(GLuint @texture, GLint @level, GLenum @format, GLenum @type, const void * @data);
        internal static glClearTexImageFunc glClearTexImagePtr;
        internal static void loadClearTexImage()
        {
            try
            {
                glClearTexImagePtr = (glClearTexImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearTexImage"), typeof(glClearTexImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearTexImage'.");
            }
        }
        public static void glClearTexImage(GLuint @texture, GLint @level, GLenum @format, GLenum @type, const void * @data) => glClearTexImagePtr(@texture, @level, @format, @type, @data);

        internal delegate void glClearTexSubImageFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @data);
        internal static glClearTexSubImageFunc glClearTexSubImagePtr;
        internal static void loadClearTexSubImage()
        {
            try
            {
                glClearTexSubImagePtr = (glClearTexSubImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearTexSubImage"), typeof(glClearTexSubImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearTexSubImage'.");
            }
        }
        public static void glClearTexSubImage(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @data) => glClearTexSubImagePtr(@texture, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @type, @data);
        #endregion
    }
}
