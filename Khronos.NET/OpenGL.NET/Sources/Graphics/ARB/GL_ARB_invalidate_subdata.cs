using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_invalidate_subdata
    {
        #region Interop
        static GL_ARB_invalidate_subdata()
        {
            Console.WriteLine("Initalising GL_ARB_invalidate_subdata interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadInvalidateTexSubImage();
            loadInvalidateTexImage();
            loadInvalidateBufferSubData();
            loadInvalidateBufferData();
            loadInvalidateFramebuffer();
            loadInvalidateSubFramebuffer();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glInvalidateTexSubImageFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth);
        internal static glInvalidateTexSubImageFunc glInvalidateTexSubImagePtr;
        internal static void loadInvalidateTexSubImage()
        {
            try
            {
                glInvalidateTexSubImagePtr = (glInvalidateTexSubImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateTexSubImage"), typeof(glInvalidateTexSubImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateTexSubImage'.");
            }
        }
        public static void glInvalidateTexSubImage(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth) => glInvalidateTexSubImagePtr(@texture, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth);

        internal delegate void glInvalidateTexImageFunc(GLuint @texture, GLint @level);
        internal static glInvalidateTexImageFunc glInvalidateTexImagePtr;
        internal static void loadInvalidateTexImage()
        {
            try
            {
                glInvalidateTexImagePtr = (glInvalidateTexImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateTexImage"), typeof(glInvalidateTexImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateTexImage'.");
            }
        }
        public static void glInvalidateTexImage(GLuint @texture, GLint @level) => glInvalidateTexImagePtr(@texture, @level);

        internal delegate void glInvalidateBufferSubDataFunc(GLuint @buffer, GLintptr @offset, GLsizeiptr @length);
        internal static glInvalidateBufferSubDataFunc glInvalidateBufferSubDataPtr;
        internal static void loadInvalidateBufferSubData()
        {
            try
            {
                glInvalidateBufferSubDataPtr = (glInvalidateBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateBufferSubData"), typeof(glInvalidateBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateBufferSubData'.");
            }
        }
        public static void glInvalidateBufferSubData(GLuint @buffer, GLintptr @offset, GLsizeiptr @length) => glInvalidateBufferSubDataPtr(@buffer, @offset, @length);

        internal delegate void glInvalidateBufferDataFunc(GLuint @buffer);
        internal static glInvalidateBufferDataFunc glInvalidateBufferDataPtr;
        internal static void loadInvalidateBufferData()
        {
            try
            {
                glInvalidateBufferDataPtr = (glInvalidateBufferDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateBufferData"), typeof(glInvalidateBufferDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateBufferData'.");
            }
        }
        public static void glInvalidateBufferData(GLuint @buffer) => glInvalidateBufferDataPtr(@buffer);

        internal delegate void glInvalidateFramebufferFunc(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments);
        internal static glInvalidateFramebufferFunc glInvalidateFramebufferPtr;
        internal static void loadInvalidateFramebuffer()
        {
            try
            {
                glInvalidateFramebufferPtr = (glInvalidateFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateFramebuffer"), typeof(glInvalidateFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateFramebuffer'.");
            }
        }
        public static void glInvalidateFramebuffer(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments) => glInvalidateFramebufferPtr(@target, @numAttachments, @attachments);

        internal delegate void glInvalidateSubFramebufferFunc(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glInvalidateSubFramebufferFunc glInvalidateSubFramebufferPtr;
        internal static void loadInvalidateSubFramebuffer()
        {
            try
            {
                glInvalidateSubFramebufferPtr = (glInvalidateSubFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateSubFramebuffer"), typeof(glInvalidateSubFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateSubFramebuffer'.");
            }
        }
        public static void glInvalidateSubFramebuffer(GLenum @target, GLsizei @numAttachments, const GLenum * @attachments, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glInvalidateSubFramebufferPtr(@target, @numAttachments, @attachments, @x, @y, @width, @height);
        #endregion
    }
}
