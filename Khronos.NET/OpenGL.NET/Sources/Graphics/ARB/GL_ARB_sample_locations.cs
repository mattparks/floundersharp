using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_sample_locations
    {
        #region Interop
        static GL_ARB_sample_locations()
        {
            Console.WriteLine("Initalising GL_ARB_sample_locations interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFramebufferSampleLocationsfvARB();
            loadNamedFramebufferSampleLocationsfvARB();
            loadEvaluateDepthValuesARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SAMPLE_LOCATION_SUBPIXEL_BITS_ARB = 0x933D;
        public static UInt32 GL_SAMPLE_LOCATION_PIXEL_GRID_WIDTH_ARB = 0x933E;
        public static UInt32 GL_SAMPLE_LOCATION_PIXEL_GRID_HEIGHT_ARB = 0x933F;
        public static UInt32 GL_PROGRAMMABLE_SAMPLE_LOCATION_TABLE_SIZE_ARB = 0x9340;
        public static UInt32 GL_SAMPLE_LOCATION_ARB = 0x8E50;
        public static UInt32 GL_PROGRAMMABLE_SAMPLE_LOCATION_ARB = 0x9341;
        public static UInt32 GL_FRAMEBUFFER_PROGRAMMABLE_SAMPLE_LOCATIONS_ARB = 0x9342;
        public static UInt32 GL_FRAMEBUFFER_SAMPLE_LOCATION_PIXEL_GRID_ARB = 0x9343;
        #endregion

        #region Commands
        internal delegate void glFramebufferSampleLocationsfvARBFunc(GLenum @target, GLuint @start, GLsizei @count, const GLfloat * @v);
        internal static glFramebufferSampleLocationsfvARBFunc glFramebufferSampleLocationsfvARBPtr;
        internal static void loadFramebufferSampleLocationsfvARB()
        {
            try
            {
                glFramebufferSampleLocationsfvARBPtr = (glFramebufferSampleLocationsfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferSampleLocationsfvARB"), typeof(glFramebufferSampleLocationsfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferSampleLocationsfvARB'.");
            }
        }
        public static void glFramebufferSampleLocationsfvARB(GLenum @target, GLuint @start, GLsizei @count, const GLfloat * @v) => glFramebufferSampleLocationsfvARBPtr(@target, @start, @count, @v);

        internal delegate void glNamedFramebufferSampleLocationsfvARBFunc(GLuint @framebuffer, GLuint @start, GLsizei @count, const GLfloat * @v);
        internal static glNamedFramebufferSampleLocationsfvARBFunc glNamedFramebufferSampleLocationsfvARBPtr;
        internal static void loadNamedFramebufferSampleLocationsfvARB()
        {
            try
            {
                glNamedFramebufferSampleLocationsfvARBPtr = (glNamedFramebufferSampleLocationsfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferSampleLocationsfvARB"), typeof(glNamedFramebufferSampleLocationsfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferSampleLocationsfvARB'.");
            }
        }
        public static void glNamedFramebufferSampleLocationsfvARB(GLuint @framebuffer, GLuint @start, GLsizei @count, const GLfloat * @v) => glNamedFramebufferSampleLocationsfvARBPtr(@framebuffer, @start, @count, @v);

        internal delegate void glEvaluateDepthValuesARBFunc();
        internal static glEvaluateDepthValuesARBFunc glEvaluateDepthValuesARBPtr;
        internal static void loadEvaluateDepthValuesARB()
        {
            try
            {
                glEvaluateDepthValuesARBPtr = (glEvaluateDepthValuesARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEvaluateDepthValuesARB"), typeof(glEvaluateDepthValuesARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEvaluateDepthValuesARB'.");
            }
        }
        public static void glEvaluateDepthValuesARB() => glEvaluateDepthValuesARBPtr();
        #endregion
    }
}
