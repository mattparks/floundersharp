using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_cull_distance
    {
        #region Interop
        static GL_ARB_cull_distance()
        {
            Console.WriteLine("Initalising GL_ARB_cull_distance interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_CULL_DISTANCES = 0x82F9;
        public static UInt32 GL_MAX_COMBINED_CLIP_AND_CULL_DISTANCES = 0x82FA;
        #endregion

        #region Commands
        #endregion
    }
}
