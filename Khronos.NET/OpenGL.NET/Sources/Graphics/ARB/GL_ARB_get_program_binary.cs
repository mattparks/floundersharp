using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_get_program_binary
    {
        #region Interop
        static GL_ARB_get_program_binary()
        {
            Console.WriteLine("Initalising GL_ARB_get_program_binary interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetProgramBinary();
            loadProgramBinary();
            loadProgramParameteri();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PROGRAM_BINARY_RETRIEVABLE_HINT = 0x8257;
        public static UInt32 GL_PROGRAM_BINARY_LENGTH = 0x8741;
        public static UInt32 GL_NUM_PROGRAM_BINARY_FORMATS = 0x87FE;
        public static UInt32 GL_PROGRAM_BINARY_FORMATS = 0x87FF;
        #endregion

        #region Commands
        internal delegate void glGetProgramBinaryFunc(GLuint @program, GLsizei @bufSize, GLsizei * @length, GLenum * @binaryFormat, void * @binary);
        internal static glGetProgramBinaryFunc glGetProgramBinaryPtr;
        internal static void loadGetProgramBinary()
        {
            try
            {
                glGetProgramBinaryPtr = (glGetProgramBinaryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramBinary"), typeof(glGetProgramBinaryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramBinary'.");
            }
        }
        public static void glGetProgramBinary(GLuint @program, GLsizei @bufSize, GLsizei * @length, GLenum * @binaryFormat, void * @binary) => glGetProgramBinaryPtr(@program, @bufSize, @length, @binaryFormat, @binary);

        internal delegate void glProgramBinaryFunc(GLuint @program, GLenum @binaryFormat, const void * @binary, GLsizei @length);
        internal static glProgramBinaryFunc glProgramBinaryPtr;
        internal static void loadProgramBinary()
        {
            try
            {
                glProgramBinaryPtr = (glProgramBinaryFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramBinary"), typeof(glProgramBinaryFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramBinary'.");
            }
        }
        public static void glProgramBinary(GLuint @program, GLenum @binaryFormat, const void * @binary, GLsizei @length) => glProgramBinaryPtr(@program, @binaryFormat, @binary, @length);

        internal delegate void glProgramParameteriFunc(GLuint @program, GLenum @pname, GLint @value);
        internal static glProgramParameteriFunc glProgramParameteriPtr;
        internal static void loadProgramParameteri()
        {
            try
            {
                glProgramParameteriPtr = (glProgramParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramParameteri"), typeof(glProgramParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramParameteri'.");
            }
        }
        public static void glProgramParameteri(GLuint @program, GLenum @pname, GLint @value) => glProgramParameteriPtr(@program, @pname, @value);
        #endregion
    }
}
