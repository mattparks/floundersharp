using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_direct_state_access
    {
        #region Interop
        static GL_ARB_direct_state_access()
        {
            Console.WriteLine("Initalising GL_ARB_direct_state_access interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCreateTransformFeedbacks();
            loadTransformFeedbackBufferBase();
            loadTransformFeedbackBufferRange();
            loadGetTransformFeedbackiv();
            loadGetTransformFeedbacki_v();
            loadGetTransformFeedbacki64_v();
            loadCreateBuffers();
            loadNamedBufferStorage();
            loadNamedBufferData();
            loadNamedBufferSubData();
            loadCopyNamedBufferSubData();
            loadClearNamedBufferData();
            loadClearNamedBufferSubData();
            loadMapNamedBuffer();
            loadMapNamedBufferRange();
            loadUnmapNamedBuffer();
            loadFlushMappedNamedBufferRange();
            loadGetNamedBufferParameteriv();
            loadGetNamedBufferParameteri64v();
            loadGetNamedBufferPointerv();
            loadGetNamedBufferSubData();
            loadCreateFramebuffers();
            loadNamedFramebufferRenderbuffer();
            loadNamedFramebufferParameteri();
            loadNamedFramebufferTexture();
            loadNamedFramebufferTextureLayer();
            loadNamedFramebufferDrawBuffer();
            loadNamedFramebufferDrawBuffers();
            loadNamedFramebufferReadBuffer();
            loadInvalidateNamedFramebufferData();
            loadInvalidateNamedFramebufferSubData();
            loadClearNamedFramebufferiv();
            loadClearNamedFramebufferuiv();
            loadClearNamedFramebufferfv();
            loadClearNamedFramebufferfi();
            loadBlitNamedFramebuffer();
            loadCheckNamedFramebufferStatus();
            loadGetNamedFramebufferParameteriv();
            loadGetNamedFramebufferAttachmentParameteriv();
            loadCreateRenderbuffers();
            loadNamedRenderbufferStorage();
            loadNamedRenderbufferStorageMultisample();
            loadGetNamedRenderbufferParameteriv();
            loadCreateTextures();
            loadTextureBuffer();
            loadTextureBufferRange();
            loadTextureStorage1D();
            loadTextureStorage2D();
            loadTextureStorage3D();
            loadTextureStorage2DMultisample();
            loadTextureStorage3DMultisample();
            loadTextureSubImage1D();
            loadTextureSubImage2D();
            loadTextureSubImage3D();
            loadCompressedTextureSubImage1D();
            loadCompressedTextureSubImage2D();
            loadCompressedTextureSubImage3D();
            loadCopyTextureSubImage1D();
            loadCopyTextureSubImage2D();
            loadCopyTextureSubImage3D();
            loadTextureParameterf();
            loadTextureParameterfv();
            loadTextureParameteri();
            loadTextureParameterIiv();
            loadTextureParameterIuiv();
            loadTextureParameteriv();
            loadGenerateTextureMipmap();
            loadBindTextureUnit();
            loadGetTextureImage();
            loadGetCompressedTextureImage();
            loadGetTextureLevelParameterfv();
            loadGetTextureLevelParameteriv();
            loadGetTextureParameterfv();
            loadGetTextureParameterIiv();
            loadGetTextureParameterIuiv();
            loadGetTextureParameteriv();
            loadCreateVertexArrays();
            loadDisableVertexArrayAttrib();
            loadEnableVertexArrayAttrib();
            loadVertexArrayElementBuffer();
            loadVertexArrayVertexBuffer();
            loadVertexArrayVertexBuffers();
            loadVertexArrayAttribBinding();
            loadVertexArrayAttribFormat();
            loadVertexArrayAttribIFormat();
            loadVertexArrayAttribLFormat();
            loadVertexArrayBindingDivisor();
            loadGetVertexArrayiv();
            loadGetVertexArrayIndexediv();
            loadGetVertexArrayIndexed64iv();
            loadCreateSamplers();
            loadCreateProgramPipelines();
            loadCreateQueries();
            loadGetQueryBufferObjecti64v();
            loadGetQueryBufferObjectiv();
            loadGetQueryBufferObjectui64v();
            loadGetQueryBufferObjectuiv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_TARGET = 0x1006;
        public static UInt32 GL_QUERY_TARGET = 0x82EA;
        public static UInt32 GL_TEXTURE_BINDING_1D = 0x8068;
        public static UInt32 GL_TEXTURE_BINDING_1D_ARRAY = 0x8C1C;
        public static UInt32 GL_TEXTURE_BINDING_2D = 0x8069;
        public static UInt32 GL_TEXTURE_BINDING_2D_ARRAY = 0x8C1D;
        public static UInt32 GL_TEXTURE_BINDING_2D_MULTISAMPLE = 0x9104;
        public static UInt32 GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY = 0x9105;
        public static UInt32 GL_TEXTURE_BINDING_3D = 0x806A;
        public static UInt32 GL_TEXTURE_BINDING_BUFFER = 0x8C2C;
        public static UInt32 GL_TEXTURE_BINDING_CUBE_MAP = 0x8514;
        public static UInt32 GL_TEXTURE_BINDING_CUBE_MAP_ARRAY = 0x900A;
        public static UInt32 GL_TEXTURE_BINDING_RECTANGLE = 0x84F6;
        #endregion

        #region Commands
        internal delegate void glCreateTransformFeedbacksFunc(GLsizei @n, GLuint * @ids);
        internal static glCreateTransformFeedbacksFunc glCreateTransformFeedbacksPtr;
        internal static void loadCreateTransformFeedbacks()
        {
            try
            {
                glCreateTransformFeedbacksPtr = (glCreateTransformFeedbacksFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateTransformFeedbacks"), typeof(glCreateTransformFeedbacksFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateTransformFeedbacks'.");
            }
        }
        public static void glCreateTransformFeedbacks(GLsizei @n, GLuint * @ids) => glCreateTransformFeedbacksPtr(@n, @ids);

        internal delegate void glTransformFeedbackBufferBaseFunc(GLuint @xfb, GLuint @index, GLuint @buffer);
        internal static glTransformFeedbackBufferBaseFunc glTransformFeedbackBufferBasePtr;
        internal static void loadTransformFeedbackBufferBase()
        {
            try
            {
                glTransformFeedbackBufferBasePtr = (glTransformFeedbackBufferBaseFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTransformFeedbackBufferBase"), typeof(glTransformFeedbackBufferBaseFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTransformFeedbackBufferBase'.");
            }
        }
        public static void glTransformFeedbackBufferBase(GLuint @xfb, GLuint @index, GLuint @buffer) => glTransformFeedbackBufferBasePtr(@xfb, @index, @buffer);

        internal delegate void glTransformFeedbackBufferRangeFunc(GLuint @xfb, GLuint @index, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glTransformFeedbackBufferRangeFunc glTransformFeedbackBufferRangePtr;
        internal static void loadTransformFeedbackBufferRange()
        {
            try
            {
                glTransformFeedbackBufferRangePtr = (glTransformFeedbackBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTransformFeedbackBufferRange"), typeof(glTransformFeedbackBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTransformFeedbackBufferRange'.");
            }
        }
        public static void glTransformFeedbackBufferRange(GLuint @xfb, GLuint @index, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glTransformFeedbackBufferRangePtr(@xfb, @index, @buffer, @offset, @size);

        internal delegate void glGetTransformFeedbackivFunc(GLuint @xfb, GLenum @pname, GLint * @param);
        internal static glGetTransformFeedbackivFunc glGetTransformFeedbackivPtr;
        internal static void loadGetTransformFeedbackiv()
        {
            try
            {
                glGetTransformFeedbackivPtr = (glGetTransformFeedbackivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTransformFeedbackiv"), typeof(glGetTransformFeedbackivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTransformFeedbackiv'.");
            }
        }
        public static void glGetTransformFeedbackiv(GLuint @xfb, GLenum @pname, GLint * @param) => glGetTransformFeedbackivPtr(@xfb, @pname, @param);

        internal delegate void glGetTransformFeedbacki_vFunc(GLuint @xfb, GLenum @pname, GLuint @index, GLint * @param);
        internal static glGetTransformFeedbacki_vFunc glGetTransformFeedbacki_vPtr;
        internal static void loadGetTransformFeedbacki_v()
        {
            try
            {
                glGetTransformFeedbacki_vPtr = (glGetTransformFeedbacki_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTransformFeedbacki_v"), typeof(glGetTransformFeedbacki_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTransformFeedbacki_v'.");
            }
        }
        public static void glGetTransformFeedbacki_v(GLuint @xfb, GLenum @pname, GLuint @index, GLint * @param) => glGetTransformFeedbacki_vPtr(@xfb, @pname, @index, @param);

        internal delegate void glGetTransformFeedbacki64_vFunc(GLuint @xfb, GLenum @pname, GLuint @index, GLint64 * @param);
        internal static glGetTransformFeedbacki64_vFunc glGetTransformFeedbacki64_vPtr;
        internal static void loadGetTransformFeedbacki64_v()
        {
            try
            {
                glGetTransformFeedbacki64_vPtr = (glGetTransformFeedbacki64_vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTransformFeedbacki64_v"), typeof(glGetTransformFeedbacki64_vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTransformFeedbacki64_v'.");
            }
        }
        public static void glGetTransformFeedbacki64_v(GLuint @xfb, GLenum @pname, GLuint @index, GLint64 * @param) => glGetTransformFeedbacki64_vPtr(@xfb, @pname, @index, @param);

        internal delegate void glCreateBuffersFunc(GLsizei @n, GLuint * @buffers);
        internal static glCreateBuffersFunc glCreateBuffersPtr;
        internal static void loadCreateBuffers()
        {
            try
            {
                glCreateBuffersPtr = (glCreateBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateBuffers"), typeof(glCreateBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateBuffers'.");
            }
        }
        public static void glCreateBuffers(GLsizei @n, GLuint * @buffers) => glCreateBuffersPtr(@n, @buffers);

        internal delegate void glNamedBufferStorageFunc(GLuint @buffer, GLsizeiptr @size, const void * @data, GLbitfield @flags);
        internal static glNamedBufferStorageFunc glNamedBufferStoragePtr;
        internal static void loadNamedBufferStorage()
        {
            try
            {
                glNamedBufferStoragePtr = (glNamedBufferStorageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedBufferStorage"), typeof(glNamedBufferStorageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedBufferStorage'.");
            }
        }
        public static void glNamedBufferStorage(GLuint @buffer, GLsizeiptr @size, const void * @data, GLbitfield @flags) => glNamedBufferStoragePtr(@buffer, @size, @data, @flags);

        internal delegate void glNamedBufferDataFunc(GLuint @buffer, GLsizeiptr @size, const void * @data, GLenum @usage);
        internal static glNamedBufferDataFunc glNamedBufferDataPtr;
        internal static void loadNamedBufferData()
        {
            try
            {
                glNamedBufferDataPtr = (glNamedBufferDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedBufferData"), typeof(glNamedBufferDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedBufferData'.");
            }
        }
        public static void glNamedBufferData(GLuint @buffer, GLsizeiptr @size, const void * @data, GLenum @usage) => glNamedBufferDataPtr(@buffer, @size, @data, @usage);

        internal delegate void glNamedBufferSubDataFunc(GLuint @buffer, GLintptr @offset, GLsizeiptr @size, const void * @data);
        internal static glNamedBufferSubDataFunc glNamedBufferSubDataPtr;
        internal static void loadNamedBufferSubData()
        {
            try
            {
                glNamedBufferSubDataPtr = (glNamedBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedBufferSubData"), typeof(glNamedBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedBufferSubData'.");
            }
        }
        public static void glNamedBufferSubData(GLuint @buffer, GLintptr @offset, GLsizeiptr @size, const void * @data) => glNamedBufferSubDataPtr(@buffer, @offset, @size, @data);

        internal delegate void glCopyNamedBufferSubDataFunc(GLuint @readBuffer, GLuint @writeBuffer, GLintptr @readOffset, GLintptr @writeOffset, GLsizeiptr @size);
        internal static glCopyNamedBufferSubDataFunc glCopyNamedBufferSubDataPtr;
        internal static void loadCopyNamedBufferSubData()
        {
            try
            {
                glCopyNamedBufferSubDataPtr = (glCopyNamedBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyNamedBufferSubData"), typeof(glCopyNamedBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyNamedBufferSubData'.");
            }
        }
        public static void glCopyNamedBufferSubData(GLuint @readBuffer, GLuint @writeBuffer, GLintptr @readOffset, GLintptr @writeOffset, GLsizeiptr @size) => glCopyNamedBufferSubDataPtr(@readBuffer, @writeBuffer, @readOffset, @writeOffset, @size);

        internal delegate void glClearNamedBufferDataFunc(GLuint @buffer, GLenum @internalformat, GLenum @format, GLenum @type, const void * @data);
        internal static glClearNamedBufferDataFunc glClearNamedBufferDataPtr;
        internal static void loadClearNamedBufferData()
        {
            try
            {
                glClearNamedBufferDataPtr = (glClearNamedBufferDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearNamedBufferData"), typeof(glClearNamedBufferDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearNamedBufferData'.");
            }
        }
        public static void glClearNamedBufferData(GLuint @buffer, GLenum @internalformat, GLenum @format, GLenum @type, const void * @data) => glClearNamedBufferDataPtr(@buffer, @internalformat, @format, @type, @data);

        internal delegate void glClearNamedBufferSubDataFunc(GLuint @buffer, GLenum @internalformat, GLintptr @offset, GLsizeiptr @size, GLenum @format, GLenum @type, const void * @data);
        internal static glClearNamedBufferSubDataFunc glClearNamedBufferSubDataPtr;
        internal static void loadClearNamedBufferSubData()
        {
            try
            {
                glClearNamedBufferSubDataPtr = (glClearNamedBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearNamedBufferSubData"), typeof(glClearNamedBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearNamedBufferSubData'.");
            }
        }
        public static void glClearNamedBufferSubData(GLuint @buffer, GLenum @internalformat, GLintptr @offset, GLsizeiptr @size, GLenum @format, GLenum @type, const void * @data) => glClearNamedBufferSubDataPtr(@buffer, @internalformat, @offset, @size, @format, @type, @data);

        internal delegate void * glMapNamedBufferFunc(GLuint @buffer, GLenum @access);
        internal static glMapNamedBufferFunc glMapNamedBufferPtr;
        internal static void loadMapNamedBuffer()
        {
            try
            {
                glMapNamedBufferPtr = (glMapNamedBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapNamedBuffer"), typeof(glMapNamedBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapNamedBuffer'.");
            }
        }
        public static void * glMapNamedBuffer(GLuint @buffer, GLenum @access) => glMapNamedBufferPtr(@buffer, @access);

        internal delegate void * glMapNamedBufferRangeFunc(GLuint @buffer, GLintptr @offset, GLsizeiptr @length, GLbitfield @access);
        internal static glMapNamedBufferRangeFunc glMapNamedBufferRangePtr;
        internal static void loadMapNamedBufferRange()
        {
            try
            {
                glMapNamedBufferRangePtr = (glMapNamedBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapNamedBufferRange"), typeof(glMapNamedBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapNamedBufferRange'.");
            }
        }
        public static void * glMapNamedBufferRange(GLuint @buffer, GLintptr @offset, GLsizeiptr @length, GLbitfield @access) => glMapNamedBufferRangePtr(@buffer, @offset, @length, @access);

        internal delegate GLboolean glUnmapNamedBufferFunc(GLuint @buffer);
        internal static glUnmapNamedBufferFunc glUnmapNamedBufferPtr;
        internal static void loadUnmapNamedBuffer()
        {
            try
            {
                glUnmapNamedBufferPtr = (glUnmapNamedBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUnmapNamedBuffer"), typeof(glUnmapNamedBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUnmapNamedBuffer'.");
            }
        }
        public static GLboolean glUnmapNamedBuffer(GLuint @buffer) => glUnmapNamedBufferPtr(@buffer);

        internal delegate void glFlushMappedNamedBufferRangeFunc(GLuint @buffer, GLintptr @offset, GLsizeiptr @length);
        internal static glFlushMappedNamedBufferRangeFunc glFlushMappedNamedBufferRangePtr;
        internal static void loadFlushMappedNamedBufferRange()
        {
            try
            {
                glFlushMappedNamedBufferRangePtr = (glFlushMappedNamedBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlushMappedNamedBufferRange"), typeof(glFlushMappedNamedBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlushMappedNamedBufferRange'.");
            }
        }
        public static void glFlushMappedNamedBufferRange(GLuint @buffer, GLintptr @offset, GLsizeiptr @length) => glFlushMappedNamedBufferRangePtr(@buffer, @offset, @length);

        internal delegate void glGetNamedBufferParameterivFunc(GLuint @buffer, GLenum @pname, GLint * @params);
        internal static glGetNamedBufferParameterivFunc glGetNamedBufferParameterivPtr;
        internal static void loadGetNamedBufferParameteriv()
        {
            try
            {
                glGetNamedBufferParameterivPtr = (glGetNamedBufferParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedBufferParameteriv"), typeof(glGetNamedBufferParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedBufferParameteriv'.");
            }
        }
        public static void glGetNamedBufferParameteriv(GLuint @buffer, GLenum @pname, GLint * @params) => glGetNamedBufferParameterivPtr(@buffer, @pname, @params);

        internal delegate void glGetNamedBufferParameteri64vFunc(GLuint @buffer, GLenum @pname, GLint64 * @params);
        internal static glGetNamedBufferParameteri64vFunc glGetNamedBufferParameteri64vPtr;
        internal static void loadGetNamedBufferParameteri64v()
        {
            try
            {
                glGetNamedBufferParameteri64vPtr = (glGetNamedBufferParameteri64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedBufferParameteri64v"), typeof(glGetNamedBufferParameteri64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedBufferParameteri64v'.");
            }
        }
        public static void glGetNamedBufferParameteri64v(GLuint @buffer, GLenum @pname, GLint64 * @params) => glGetNamedBufferParameteri64vPtr(@buffer, @pname, @params);

        internal delegate void glGetNamedBufferPointervFunc(GLuint @buffer, GLenum @pname, void ** @params);
        internal static glGetNamedBufferPointervFunc glGetNamedBufferPointervPtr;
        internal static void loadGetNamedBufferPointerv()
        {
            try
            {
                glGetNamedBufferPointervPtr = (glGetNamedBufferPointervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedBufferPointerv"), typeof(glGetNamedBufferPointervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedBufferPointerv'.");
            }
        }
        public static void glGetNamedBufferPointerv(GLuint @buffer, GLenum @pname, void ** @params) => glGetNamedBufferPointervPtr(@buffer, @pname, @params);

        internal delegate void glGetNamedBufferSubDataFunc(GLuint @buffer, GLintptr @offset, GLsizeiptr @size, void * @data);
        internal static glGetNamedBufferSubDataFunc glGetNamedBufferSubDataPtr;
        internal static void loadGetNamedBufferSubData()
        {
            try
            {
                glGetNamedBufferSubDataPtr = (glGetNamedBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedBufferSubData"), typeof(glGetNamedBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedBufferSubData'.");
            }
        }
        public static void glGetNamedBufferSubData(GLuint @buffer, GLintptr @offset, GLsizeiptr @size, void * @data) => glGetNamedBufferSubDataPtr(@buffer, @offset, @size, @data);

        internal delegate void glCreateFramebuffersFunc(GLsizei @n, GLuint * @framebuffers);
        internal static glCreateFramebuffersFunc glCreateFramebuffersPtr;
        internal static void loadCreateFramebuffers()
        {
            try
            {
                glCreateFramebuffersPtr = (glCreateFramebuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateFramebuffers"), typeof(glCreateFramebuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateFramebuffers'.");
            }
        }
        public static void glCreateFramebuffers(GLsizei @n, GLuint * @framebuffers) => glCreateFramebuffersPtr(@n, @framebuffers);

        internal delegate void glNamedFramebufferRenderbufferFunc(GLuint @framebuffer, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer);
        internal static glNamedFramebufferRenderbufferFunc glNamedFramebufferRenderbufferPtr;
        internal static void loadNamedFramebufferRenderbuffer()
        {
            try
            {
                glNamedFramebufferRenderbufferPtr = (glNamedFramebufferRenderbufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferRenderbuffer"), typeof(glNamedFramebufferRenderbufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferRenderbuffer'.");
            }
        }
        public static void glNamedFramebufferRenderbuffer(GLuint @framebuffer, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer) => glNamedFramebufferRenderbufferPtr(@framebuffer, @attachment, @renderbuffertarget, @renderbuffer);

        internal delegate void glNamedFramebufferParameteriFunc(GLuint @framebuffer, GLenum @pname, GLint @param);
        internal static glNamedFramebufferParameteriFunc glNamedFramebufferParameteriPtr;
        internal static void loadNamedFramebufferParameteri()
        {
            try
            {
                glNamedFramebufferParameteriPtr = (glNamedFramebufferParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferParameteri"), typeof(glNamedFramebufferParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferParameteri'.");
            }
        }
        public static void glNamedFramebufferParameteri(GLuint @framebuffer, GLenum @pname, GLint @param) => glNamedFramebufferParameteriPtr(@framebuffer, @pname, @param);

        internal delegate void glNamedFramebufferTextureFunc(GLuint @framebuffer, GLenum @attachment, GLuint @texture, GLint @level);
        internal static glNamedFramebufferTextureFunc glNamedFramebufferTexturePtr;
        internal static void loadNamedFramebufferTexture()
        {
            try
            {
                glNamedFramebufferTexturePtr = (glNamedFramebufferTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferTexture"), typeof(glNamedFramebufferTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferTexture'.");
            }
        }
        public static void glNamedFramebufferTexture(GLuint @framebuffer, GLenum @attachment, GLuint @texture, GLint @level) => glNamedFramebufferTexturePtr(@framebuffer, @attachment, @texture, @level);

        internal delegate void glNamedFramebufferTextureLayerFunc(GLuint @framebuffer, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer);
        internal static glNamedFramebufferTextureLayerFunc glNamedFramebufferTextureLayerPtr;
        internal static void loadNamedFramebufferTextureLayer()
        {
            try
            {
                glNamedFramebufferTextureLayerPtr = (glNamedFramebufferTextureLayerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferTextureLayer"), typeof(glNamedFramebufferTextureLayerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferTextureLayer'.");
            }
        }
        public static void glNamedFramebufferTextureLayer(GLuint @framebuffer, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer) => glNamedFramebufferTextureLayerPtr(@framebuffer, @attachment, @texture, @level, @layer);

        internal delegate void glNamedFramebufferDrawBufferFunc(GLuint @framebuffer, GLenum @buf);
        internal static glNamedFramebufferDrawBufferFunc glNamedFramebufferDrawBufferPtr;
        internal static void loadNamedFramebufferDrawBuffer()
        {
            try
            {
                glNamedFramebufferDrawBufferPtr = (glNamedFramebufferDrawBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferDrawBuffer"), typeof(glNamedFramebufferDrawBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferDrawBuffer'.");
            }
        }
        public static void glNamedFramebufferDrawBuffer(GLuint @framebuffer, GLenum @buf) => glNamedFramebufferDrawBufferPtr(@framebuffer, @buf);

        internal delegate void glNamedFramebufferDrawBuffersFunc(GLuint @framebuffer, GLsizei @n, const GLenum * @bufs);
        internal static glNamedFramebufferDrawBuffersFunc glNamedFramebufferDrawBuffersPtr;
        internal static void loadNamedFramebufferDrawBuffers()
        {
            try
            {
                glNamedFramebufferDrawBuffersPtr = (glNamedFramebufferDrawBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferDrawBuffers"), typeof(glNamedFramebufferDrawBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferDrawBuffers'.");
            }
        }
        public static void glNamedFramebufferDrawBuffers(GLuint @framebuffer, GLsizei @n, const GLenum * @bufs) => glNamedFramebufferDrawBuffersPtr(@framebuffer, @n, @bufs);

        internal delegate void glNamedFramebufferReadBufferFunc(GLuint @framebuffer, GLenum @src);
        internal static glNamedFramebufferReadBufferFunc glNamedFramebufferReadBufferPtr;
        internal static void loadNamedFramebufferReadBuffer()
        {
            try
            {
                glNamedFramebufferReadBufferPtr = (glNamedFramebufferReadBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedFramebufferReadBuffer"), typeof(glNamedFramebufferReadBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedFramebufferReadBuffer'.");
            }
        }
        public static void glNamedFramebufferReadBuffer(GLuint @framebuffer, GLenum @src) => glNamedFramebufferReadBufferPtr(@framebuffer, @src);

        internal delegate void glInvalidateNamedFramebufferDataFunc(GLuint @framebuffer, GLsizei @numAttachments, const GLenum * @attachments);
        internal static glInvalidateNamedFramebufferDataFunc glInvalidateNamedFramebufferDataPtr;
        internal static void loadInvalidateNamedFramebufferData()
        {
            try
            {
                glInvalidateNamedFramebufferDataPtr = (glInvalidateNamedFramebufferDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateNamedFramebufferData"), typeof(glInvalidateNamedFramebufferDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateNamedFramebufferData'.");
            }
        }
        public static void glInvalidateNamedFramebufferData(GLuint @framebuffer, GLsizei @numAttachments, const GLenum * @attachments) => glInvalidateNamedFramebufferDataPtr(@framebuffer, @numAttachments, @attachments);

        internal delegate void glInvalidateNamedFramebufferSubDataFunc(GLuint @framebuffer, GLsizei @numAttachments, const GLenum * @attachments, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glInvalidateNamedFramebufferSubDataFunc glInvalidateNamedFramebufferSubDataPtr;
        internal static void loadInvalidateNamedFramebufferSubData()
        {
            try
            {
                glInvalidateNamedFramebufferSubDataPtr = (glInvalidateNamedFramebufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glInvalidateNamedFramebufferSubData"), typeof(glInvalidateNamedFramebufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glInvalidateNamedFramebufferSubData'.");
            }
        }
        public static void glInvalidateNamedFramebufferSubData(GLuint @framebuffer, GLsizei @numAttachments, const GLenum * @attachments, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glInvalidateNamedFramebufferSubDataPtr(@framebuffer, @numAttachments, @attachments, @x, @y, @width, @height);

        internal delegate void glClearNamedFramebufferivFunc(GLuint @framebuffer, GLenum @buffer, GLint @drawbuffer, const GLint * @value);
        internal static glClearNamedFramebufferivFunc glClearNamedFramebufferivPtr;
        internal static void loadClearNamedFramebufferiv()
        {
            try
            {
                glClearNamedFramebufferivPtr = (glClearNamedFramebufferivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearNamedFramebufferiv"), typeof(glClearNamedFramebufferivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearNamedFramebufferiv'.");
            }
        }
        public static void glClearNamedFramebufferiv(GLuint @framebuffer, GLenum @buffer, GLint @drawbuffer, const GLint * @value) => glClearNamedFramebufferivPtr(@framebuffer, @buffer, @drawbuffer, @value);

        internal delegate void glClearNamedFramebufferuivFunc(GLuint @framebuffer, GLenum @buffer, GLint @drawbuffer, const GLuint * @value);
        internal static glClearNamedFramebufferuivFunc glClearNamedFramebufferuivPtr;
        internal static void loadClearNamedFramebufferuiv()
        {
            try
            {
                glClearNamedFramebufferuivPtr = (glClearNamedFramebufferuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearNamedFramebufferuiv"), typeof(glClearNamedFramebufferuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearNamedFramebufferuiv'.");
            }
        }
        public static void glClearNamedFramebufferuiv(GLuint @framebuffer, GLenum @buffer, GLint @drawbuffer, const GLuint * @value) => glClearNamedFramebufferuivPtr(@framebuffer, @buffer, @drawbuffer, @value);

        internal delegate void glClearNamedFramebufferfvFunc(GLuint @framebuffer, GLenum @buffer, GLint @drawbuffer, const GLfloat * @value);
        internal static glClearNamedFramebufferfvFunc glClearNamedFramebufferfvPtr;
        internal static void loadClearNamedFramebufferfv()
        {
            try
            {
                glClearNamedFramebufferfvPtr = (glClearNamedFramebufferfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearNamedFramebufferfv"), typeof(glClearNamedFramebufferfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearNamedFramebufferfv'.");
            }
        }
        public static void glClearNamedFramebufferfv(GLuint @framebuffer, GLenum @buffer, GLint @drawbuffer, const GLfloat * @value) => glClearNamedFramebufferfvPtr(@framebuffer, @buffer, @drawbuffer, @value);

        internal delegate void glClearNamedFramebufferfiFunc(GLuint @framebuffer, GLenum @buffer, GLint @drawbuffer, GLfloat @depth, GLint @stencil);
        internal static glClearNamedFramebufferfiFunc glClearNamedFramebufferfiPtr;
        internal static void loadClearNamedFramebufferfi()
        {
            try
            {
                glClearNamedFramebufferfiPtr = (glClearNamedFramebufferfiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearNamedFramebufferfi"), typeof(glClearNamedFramebufferfiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearNamedFramebufferfi'.");
            }
        }
        public static void glClearNamedFramebufferfi(GLuint @framebuffer, GLenum @buffer, GLint @drawbuffer, GLfloat @depth, GLint @stencil) => glClearNamedFramebufferfiPtr(@framebuffer, @buffer, @drawbuffer, @depth, @stencil);

        internal delegate void glBlitNamedFramebufferFunc(GLuint @readFramebuffer, GLuint @drawFramebuffer, GLint @srcX0, GLint @srcY0, GLint @srcX1, GLint @srcY1, GLint @dstX0, GLint @dstY0, GLint @dstX1, GLint @dstY1, GLbitfield @mask, GLenum @filter);
        internal static glBlitNamedFramebufferFunc glBlitNamedFramebufferPtr;
        internal static void loadBlitNamedFramebuffer()
        {
            try
            {
                glBlitNamedFramebufferPtr = (glBlitNamedFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlitNamedFramebuffer"), typeof(glBlitNamedFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlitNamedFramebuffer'.");
            }
        }
        public static void glBlitNamedFramebuffer(GLuint @readFramebuffer, GLuint @drawFramebuffer, GLint @srcX0, GLint @srcY0, GLint @srcX1, GLint @srcY1, GLint @dstX0, GLint @dstY0, GLint @dstX1, GLint @dstY1, GLbitfield @mask, GLenum @filter) => glBlitNamedFramebufferPtr(@readFramebuffer, @drawFramebuffer, @srcX0, @srcY0, @srcX1, @srcY1, @dstX0, @dstY0, @dstX1, @dstY1, @mask, @filter);

        internal delegate GLenum glCheckNamedFramebufferStatusFunc(GLuint @framebuffer, GLenum @target);
        internal static glCheckNamedFramebufferStatusFunc glCheckNamedFramebufferStatusPtr;
        internal static void loadCheckNamedFramebufferStatus()
        {
            try
            {
                glCheckNamedFramebufferStatusPtr = (glCheckNamedFramebufferStatusFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCheckNamedFramebufferStatus"), typeof(glCheckNamedFramebufferStatusFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCheckNamedFramebufferStatus'.");
            }
        }
        public static GLenum glCheckNamedFramebufferStatus(GLuint @framebuffer, GLenum @target) => glCheckNamedFramebufferStatusPtr(@framebuffer, @target);

        internal delegate void glGetNamedFramebufferParameterivFunc(GLuint @framebuffer, GLenum @pname, GLint * @param);
        internal static glGetNamedFramebufferParameterivFunc glGetNamedFramebufferParameterivPtr;
        internal static void loadGetNamedFramebufferParameteriv()
        {
            try
            {
                glGetNamedFramebufferParameterivPtr = (glGetNamedFramebufferParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedFramebufferParameteriv"), typeof(glGetNamedFramebufferParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedFramebufferParameteriv'.");
            }
        }
        public static void glGetNamedFramebufferParameteriv(GLuint @framebuffer, GLenum @pname, GLint * @param) => glGetNamedFramebufferParameterivPtr(@framebuffer, @pname, @param);

        internal delegate void glGetNamedFramebufferAttachmentParameterivFunc(GLuint @framebuffer, GLenum @attachment, GLenum @pname, GLint * @params);
        internal static glGetNamedFramebufferAttachmentParameterivFunc glGetNamedFramebufferAttachmentParameterivPtr;
        internal static void loadGetNamedFramebufferAttachmentParameteriv()
        {
            try
            {
                glGetNamedFramebufferAttachmentParameterivPtr = (glGetNamedFramebufferAttachmentParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedFramebufferAttachmentParameteriv"), typeof(glGetNamedFramebufferAttachmentParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedFramebufferAttachmentParameteriv'.");
            }
        }
        public static void glGetNamedFramebufferAttachmentParameteriv(GLuint @framebuffer, GLenum @attachment, GLenum @pname, GLint * @params) => glGetNamedFramebufferAttachmentParameterivPtr(@framebuffer, @attachment, @pname, @params);

        internal delegate void glCreateRenderbuffersFunc(GLsizei @n, GLuint * @renderbuffers);
        internal static glCreateRenderbuffersFunc glCreateRenderbuffersPtr;
        internal static void loadCreateRenderbuffers()
        {
            try
            {
                glCreateRenderbuffersPtr = (glCreateRenderbuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateRenderbuffers"), typeof(glCreateRenderbuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateRenderbuffers'.");
            }
        }
        public static void glCreateRenderbuffers(GLsizei @n, GLuint * @renderbuffers) => glCreateRenderbuffersPtr(@n, @renderbuffers);

        internal delegate void glNamedRenderbufferStorageFunc(GLuint @renderbuffer, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glNamedRenderbufferStorageFunc glNamedRenderbufferStoragePtr;
        internal static void loadNamedRenderbufferStorage()
        {
            try
            {
                glNamedRenderbufferStoragePtr = (glNamedRenderbufferStorageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedRenderbufferStorage"), typeof(glNamedRenderbufferStorageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedRenderbufferStorage'.");
            }
        }
        public static void glNamedRenderbufferStorage(GLuint @renderbuffer, GLenum @internalformat, GLsizei @width, GLsizei @height) => glNamedRenderbufferStoragePtr(@renderbuffer, @internalformat, @width, @height);

        internal delegate void glNamedRenderbufferStorageMultisampleFunc(GLuint @renderbuffer, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glNamedRenderbufferStorageMultisampleFunc glNamedRenderbufferStorageMultisamplePtr;
        internal static void loadNamedRenderbufferStorageMultisample()
        {
            try
            {
                glNamedRenderbufferStorageMultisamplePtr = (glNamedRenderbufferStorageMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedRenderbufferStorageMultisample"), typeof(glNamedRenderbufferStorageMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedRenderbufferStorageMultisample'.");
            }
        }
        public static void glNamedRenderbufferStorageMultisample(GLuint @renderbuffer, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glNamedRenderbufferStorageMultisamplePtr(@renderbuffer, @samples, @internalformat, @width, @height);

        internal delegate void glGetNamedRenderbufferParameterivFunc(GLuint @renderbuffer, GLenum @pname, GLint * @params);
        internal static glGetNamedRenderbufferParameterivFunc glGetNamedRenderbufferParameterivPtr;
        internal static void loadGetNamedRenderbufferParameteriv()
        {
            try
            {
                glGetNamedRenderbufferParameterivPtr = (glGetNamedRenderbufferParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedRenderbufferParameteriv"), typeof(glGetNamedRenderbufferParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedRenderbufferParameteriv'.");
            }
        }
        public static void glGetNamedRenderbufferParameteriv(GLuint @renderbuffer, GLenum @pname, GLint * @params) => glGetNamedRenderbufferParameterivPtr(@renderbuffer, @pname, @params);

        internal delegate void glCreateTexturesFunc(GLenum @target, GLsizei @n, GLuint * @textures);
        internal static glCreateTexturesFunc glCreateTexturesPtr;
        internal static void loadCreateTextures()
        {
            try
            {
                glCreateTexturesPtr = (glCreateTexturesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateTextures"), typeof(glCreateTexturesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateTextures'.");
            }
        }
        public static void glCreateTextures(GLenum @target, GLsizei @n, GLuint * @textures) => glCreateTexturesPtr(@target, @n, @textures);

        internal delegate void glTextureBufferFunc(GLuint @texture, GLenum @internalformat, GLuint @buffer);
        internal static glTextureBufferFunc glTextureBufferPtr;
        internal static void loadTextureBuffer()
        {
            try
            {
                glTextureBufferPtr = (glTextureBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureBuffer"), typeof(glTextureBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureBuffer'.");
            }
        }
        public static void glTextureBuffer(GLuint @texture, GLenum @internalformat, GLuint @buffer) => glTextureBufferPtr(@texture, @internalformat, @buffer);

        internal delegate void glTextureBufferRangeFunc(GLuint @texture, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glTextureBufferRangeFunc glTextureBufferRangePtr;
        internal static void loadTextureBufferRange()
        {
            try
            {
                glTextureBufferRangePtr = (glTextureBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureBufferRange"), typeof(glTextureBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureBufferRange'.");
            }
        }
        public static void glTextureBufferRange(GLuint @texture, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glTextureBufferRangePtr(@texture, @internalformat, @buffer, @offset, @size);

        internal delegate void glTextureStorage1DFunc(GLuint @texture, GLsizei @levels, GLenum @internalformat, GLsizei @width);
        internal static glTextureStorage1DFunc glTextureStorage1DPtr;
        internal static void loadTextureStorage1D()
        {
            try
            {
                glTextureStorage1DPtr = (glTextureStorage1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage1D"), typeof(glTextureStorage1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage1D'.");
            }
        }
        public static void glTextureStorage1D(GLuint @texture, GLsizei @levels, GLenum @internalformat, GLsizei @width) => glTextureStorage1DPtr(@texture, @levels, @internalformat, @width);

        internal delegate void glTextureStorage2DFunc(GLuint @texture, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glTextureStorage2DFunc glTextureStorage2DPtr;
        internal static void loadTextureStorage2D()
        {
            try
            {
                glTextureStorage2DPtr = (glTextureStorage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage2D"), typeof(glTextureStorage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage2D'.");
            }
        }
        public static void glTextureStorage2D(GLuint @texture, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height) => glTextureStorage2DPtr(@texture, @levels, @internalformat, @width, @height);

        internal delegate void glTextureStorage3DFunc(GLuint @texture, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth);
        internal static glTextureStorage3DFunc glTextureStorage3DPtr;
        internal static void loadTextureStorage3D()
        {
            try
            {
                glTextureStorage3DPtr = (glTextureStorage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage3D"), typeof(glTextureStorage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage3D'.");
            }
        }
        public static void glTextureStorage3D(GLuint @texture, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth) => glTextureStorage3DPtr(@texture, @levels, @internalformat, @width, @height, @depth);

        internal delegate void glTextureStorage2DMultisampleFunc(GLuint @texture, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations);
        internal static glTextureStorage2DMultisampleFunc glTextureStorage2DMultisamplePtr;
        internal static void loadTextureStorage2DMultisample()
        {
            try
            {
                glTextureStorage2DMultisamplePtr = (glTextureStorage2DMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage2DMultisample"), typeof(glTextureStorage2DMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage2DMultisample'.");
            }
        }
        public static void glTextureStorage2DMultisample(GLuint @texture, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations) => glTextureStorage2DMultisamplePtr(@texture, @samples, @internalformat, @width, @height, @fixedsamplelocations);

        internal delegate void glTextureStorage3DMultisampleFunc(GLuint @texture, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations);
        internal static glTextureStorage3DMultisampleFunc glTextureStorage3DMultisamplePtr;
        internal static void loadTextureStorage3DMultisample()
        {
            try
            {
                glTextureStorage3DMultisamplePtr = (glTextureStorage3DMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureStorage3DMultisample"), typeof(glTextureStorage3DMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureStorage3DMultisample'.");
            }
        }
        public static void glTextureStorage3DMultisample(GLuint @texture, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations) => glTextureStorage3DMultisamplePtr(@texture, @samples, @internalformat, @width, @height, @depth, @fixedsamplelocations);

        internal delegate void glTextureSubImage1DFunc(GLuint @texture, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTextureSubImage1DFunc glTextureSubImage1DPtr;
        internal static void loadTextureSubImage1D()
        {
            try
            {
                glTextureSubImage1DPtr = (glTextureSubImage1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureSubImage1D"), typeof(glTextureSubImage1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureSubImage1D'.");
            }
        }
        public static void glTextureSubImage1D(GLuint @texture, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLenum @type, const void * @pixels) => glTextureSubImage1DPtr(@texture, @level, @xoffset, @width, @format, @type, @pixels);

        internal delegate void glTextureSubImage2DFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTextureSubImage2DFunc glTextureSubImage2DPtr;
        internal static void loadTextureSubImage2D()
        {
            try
            {
                glTextureSubImage2DPtr = (glTextureSubImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureSubImage2D"), typeof(glTextureSubImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureSubImage2D'.");
            }
        }
        public static void glTextureSubImage2D(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels) => glTextureSubImage2DPtr(@texture, @level, @xoffset, @yoffset, @width, @height, @format, @type, @pixels);

        internal delegate void glTextureSubImage3DFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTextureSubImage3DFunc glTextureSubImage3DPtr;
        internal static void loadTextureSubImage3D()
        {
            try
            {
                glTextureSubImage3DPtr = (glTextureSubImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureSubImage3D"), typeof(glTextureSubImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureSubImage3D'.");
            }
        }
        public static void glTextureSubImage3D(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, const void * @pixels) => glTextureSubImage3DPtr(@texture, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @type, @pixels);

        internal delegate void glCompressedTextureSubImage1DFunc(GLuint @texture, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTextureSubImage1DFunc glCompressedTextureSubImage1DPtr;
        internal static void loadCompressedTextureSubImage1D()
        {
            try
            {
                glCompressedTextureSubImage1DPtr = (glCompressedTextureSubImage1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTextureSubImage1D"), typeof(glCompressedTextureSubImage1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTextureSubImage1D'.");
            }
        }
        public static void glCompressedTextureSubImage1D(GLuint @texture, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTextureSubImage1DPtr(@texture, @level, @xoffset, @width, @format, @imageSize, @data);

        internal delegate void glCompressedTextureSubImage2DFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTextureSubImage2DFunc glCompressedTextureSubImage2DPtr;
        internal static void loadCompressedTextureSubImage2D()
        {
            try
            {
                glCompressedTextureSubImage2DPtr = (glCompressedTextureSubImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTextureSubImage2D"), typeof(glCompressedTextureSubImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTextureSubImage2D'.");
            }
        }
        public static void glCompressedTextureSubImage2D(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTextureSubImage2DPtr(@texture, @level, @xoffset, @yoffset, @width, @height, @format, @imageSize, @data);

        internal delegate void glCompressedTextureSubImage3DFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTextureSubImage3DFunc glCompressedTextureSubImage3DPtr;
        internal static void loadCompressedTextureSubImage3D()
        {
            try
            {
                glCompressedTextureSubImage3DPtr = (glCompressedTextureSubImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTextureSubImage3D"), typeof(glCompressedTextureSubImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTextureSubImage3D'.");
            }
        }
        public static void glCompressedTextureSubImage3D(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTextureSubImage3DPtr(@texture, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @imageSize, @data);

        internal delegate void glCopyTextureSubImage1DFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @x, GLint @y, GLsizei @width);
        internal static glCopyTextureSubImage1DFunc glCopyTextureSubImage1DPtr;
        internal static void loadCopyTextureSubImage1D()
        {
            try
            {
                glCopyTextureSubImage1DPtr = (glCopyTextureSubImage1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTextureSubImage1D"), typeof(glCopyTextureSubImage1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTextureSubImage1D'.");
            }
        }
        public static void glCopyTextureSubImage1D(GLuint @texture, GLint @level, GLint @xoffset, GLint @x, GLint @y, GLsizei @width) => glCopyTextureSubImage1DPtr(@texture, @level, @xoffset, @x, @y, @width);

        internal delegate void glCopyTextureSubImage2DFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyTextureSubImage2DFunc glCopyTextureSubImage2DPtr;
        internal static void loadCopyTextureSubImage2D()
        {
            try
            {
                glCopyTextureSubImage2DPtr = (glCopyTextureSubImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTextureSubImage2D"), typeof(glCopyTextureSubImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTextureSubImage2D'.");
            }
        }
        public static void glCopyTextureSubImage2D(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyTextureSubImage2DPtr(@texture, @level, @xoffset, @yoffset, @x, @y, @width, @height);

        internal delegate void glCopyTextureSubImage3DFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyTextureSubImage3DFunc glCopyTextureSubImage3DPtr;
        internal static void loadCopyTextureSubImage3D()
        {
            try
            {
                glCopyTextureSubImage3DPtr = (glCopyTextureSubImage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTextureSubImage3D"), typeof(glCopyTextureSubImage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTextureSubImage3D'.");
            }
        }
        public static void glCopyTextureSubImage3D(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyTextureSubImage3DPtr(@texture, @level, @xoffset, @yoffset, @zoffset, @x, @y, @width, @height);

        internal delegate void glTextureParameterfFunc(GLuint @texture, GLenum @pname, GLfloat @param);
        internal static glTextureParameterfFunc glTextureParameterfPtr;
        internal static void loadTextureParameterf()
        {
            try
            {
                glTextureParameterfPtr = (glTextureParameterfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureParameterf"), typeof(glTextureParameterfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureParameterf'.");
            }
        }
        public static void glTextureParameterf(GLuint @texture, GLenum @pname, GLfloat @param) => glTextureParameterfPtr(@texture, @pname, @param);

        internal delegate void glTextureParameterfvFunc(GLuint @texture, GLenum @pname, const GLfloat * @param);
        internal static glTextureParameterfvFunc glTextureParameterfvPtr;
        internal static void loadTextureParameterfv()
        {
            try
            {
                glTextureParameterfvPtr = (glTextureParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureParameterfv"), typeof(glTextureParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureParameterfv'.");
            }
        }
        public static void glTextureParameterfv(GLuint @texture, GLenum @pname, const GLfloat * @param) => glTextureParameterfvPtr(@texture, @pname, @param);

        internal delegate void glTextureParameteriFunc(GLuint @texture, GLenum @pname, GLint @param);
        internal static glTextureParameteriFunc glTextureParameteriPtr;
        internal static void loadTextureParameteri()
        {
            try
            {
                glTextureParameteriPtr = (glTextureParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureParameteri"), typeof(glTextureParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureParameteri'.");
            }
        }
        public static void glTextureParameteri(GLuint @texture, GLenum @pname, GLint @param) => glTextureParameteriPtr(@texture, @pname, @param);

        internal delegate void glTextureParameterIivFunc(GLuint @texture, GLenum @pname, const GLint * @params);
        internal static glTextureParameterIivFunc glTextureParameterIivPtr;
        internal static void loadTextureParameterIiv()
        {
            try
            {
                glTextureParameterIivPtr = (glTextureParameterIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureParameterIiv"), typeof(glTextureParameterIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureParameterIiv'.");
            }
        }
        public static void glTextureParameterIiv(GLuint @texture, GLenum @pname, const GLint * @params) => glTextureParameterIivPtr(@texture, @pname, @params);

        internal delegate void glTextureParameterIuivFunc(GLuint @texture, GLenum @pname, const GLuint * @params);
        internal static glTextureParameterIuivFunc glTextureParameterIuivPtr;
        internal static void loadTextureParameterIuiv()
        {
            try
            {
                glTextureParameterIuivPtr = (glTextureParameterIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureParameterIuiv"), typeof(glTextureParameterIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureParameterIuiv'.");
            }
        }
        public static void glTextureParameterIuiv(GLuint @texture, GLenum @pname, const GLuint * @params) => glTextureParameterIuivPtr(@texture, @pname, @params);

        internal delegate void glTextureParameterivFunc(GLuint @texture, GLenum @pname, const GLint * @param);
        internal static glTextureParameterivFunc glTextureParameterivPtr;
        internal static void loadTextureParameteriv()
        {
            try
            {
                glTextureParameterivPtr = (glTextureParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureParameteriv"), typeof(glTextureParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureParameteriv'.");
            }
        }
        public static void glTextureParameteriv(GLuint @texture, GLenum @pname, const GLint * @param) => glTextureParameterivPtr(@texture, @pname, @param);

        internal delegate void glGenerateTextureMipmapFunc(GLuint @texture);
        internal static glGenerateTextureMipmapFunc glGenerateTextureMipmapPtr;
        internal static void loadGenerateTextureMipmap()
        {
            try
            {
                glGenerateTextureMipmapPtr = (glGenerateTextureMipmapFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenerateTextureMipmap"), typeof(glGenerateTextureMipmapFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenerateTextureMipmap'.");
            }
        }
        public static void glGenerateTextureMipmap(GLuint @texture) => glGenerateTextureMipmapPtr(@texture);

        internal delegate void glBindTextureUnitFunc(GLuint @unit, GLuint @texture);
        internal static glBindTextureUnitFunc glBindTextureUnitPtr;
        internal static void loadBindTextureUnit()
        {
            try
            {
                glBindTextureUnitPtr = (glBindTextureUnitFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTextureUnit"), typeof(glBindTextureUnitFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTextureUnit'.");
            }
        }
        public static void glBindTextureUnit(GLuint @unit, GLuint @texture) => glBindTextureUnitPtr(@unit, @texture);

        internal delegate void glGetTextureImageFunc(GLuint @texture, GLint @level, GLenum @format, GLenum @type, GLsizei @bufSize, void * @pixels);
        internal static glGetTextureImageFunc glGetTextureImagePtr;
        internal static void loadGetTextureImage()
        {
            try
            {
                glGetTextureImagePtr = (glGetTextureImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureImage"), typeof(glGetTextureImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureImage'.");
            }
        }
        public static void glGetTextureImage(GLuint @texture, GLint @level, GLenum @format, GLenum @type, GLsizei @bufSize, void * @pixels) => glGetTextureImagePtr(@texture, @level, @format, @type, @bufSize, @pixels);

        internal delegate void glGetCompressedTextureImageFunc(GLuint @texture, GLint @level, GLsizei @bufSize, void * @pixels);
        internal static glGetCompressedTextureImageFunc glGetCompressedTextureImagePtr;
        internal static void loadGetCompressedTextureImage()
        {
            try
            {
                glGetCompressedTextureImagePtr = (glGetCompressedTextureImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCompressedTextureImage"), typeof(glGetCompressedTextureImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCompressedTextureImage'.");
            }
        }
        public static void glGetCompressedTextureImage(GLuint @texture, GLint @level, GLsizei @bufSize, void * @pixels) => glGetCompressedTextureImagePtr(@texture, @level, @bufSize, @pixels);

        internal delegate void glGetTextureLevelParameterfvFunc(GLuint @texture, GLint @level, GLenum @pname, GLfloat * @params);
        internal static glGetTextureLevelParameterfvFunc glGetTextureLevelParameterfvPtr;
        internal static void loadGetTextureLevelParameterfv()
        {
            try
            {
                glGetTextureLevelParameterfvPtr = (glGetTextureLevelParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureLevelParameterfv"), typeof(glGetTextureLevelParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureLevelParameterfv'.");
            }
        }
        public static void glGetTextureLevelParameterfv(GLuint @texture, GLint @level, GLenum @pname, GLfloat * @params) => glGetTextureLevelParameterfvPtr(@texture, @level, @pname, @params);

        internal delegate void glGetTextureLevelParameterivFunc(GLuint @texture, GLint @level, GLenum @pname, GLint * @params);
        internal static glGetTextureLevelParameterivFunc glGetTextureLevelParameterivPtr;
        internal static void loadGetTextureLevelParameteriv()
        {
            try
            {
                glGetTextureLevelParameterivPtr = (glGetTextureLevelParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureLevelParameteriv"), typeof(glGetTextureLevelParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureLevelParameteriv'.");
            }
        }
        public static void glGetTextureLevelParameteriv(GLuint @texture, GLint @level, GLenum @pname, GLint * @params) => glGetTextureLevelParameterivPtr(@texture, @level, @pname, @params);

        internal delegate void glGetTextureParameterfvFunc(GLuint @texture, GLenum @pname, GLfloat * @params);
        internal static glGetTextureParameterfvFunc glGetTextureParameterfvPtr;
        internal static void loadGetTextureParameterfv()
        {
            try
            {
                glGetTextureParameterfvPtr = (glGetTextureParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureParameterfv"), typeof(glGetTextureParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureParameterfv'.");
            }
        }
        public static void glGetTextureParameterfv(GLuint @texture, GLenum @pname, GLfloat * @params) => glGetTextureParameterfvPtr(@texture, @pname, @params);

        internal delegate void glGetTextureParameterIivFunc(GLuint @texture, GLenum @pname, GLint * @params);
        internal static glGetTextureParameterIivFunc glGetTextureParameterIivPtr;
        internal static void loadGetTextureParameterIiv()
        {
            try
            {
                glGetTextureParameterIivPtr = (glGetTextureParameterIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureParameterIiv"), typeof(glGetTextureParameterIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureParameterIiv'.");
            }
        }
        public static void glGetTextureParameterIiv(GLuint @texture, GLenum @pname, GLint * @params) => glGetTextureParameterIivPtr(@texture, @pname, @params);

        internal delegate void glGetTextureParameterIuivFunc(GLuint @texture, GLenum @pname, GLuint * @params);
        internal static glGetTextureParameterIuivFunc glGetTextureParameterIuivPtr;
        internal static void loadGetTextureParameterIuiv()
        {
            try
            {
                glGetTextureParameterIuivPtr = (glGetTextureParameterIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureParameterIuiv"), typeof(glGetTextureParameterIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureParameterIuiv'.");
            }
        }
        public static void glGetTextureParameterIuiv(GLuint @texture, GLenum @pname, GLuint * @params) => glGetTextureParameterIuivPtr(@texture, @pname, @params);

        internal delegate void glGetTextureParameterivFunc(GLuint @texture, GLenum @pname, GLint * @params);
        internal static glGetTextureParameterivFunc glGetTextureParameterivPtr;
        internal static void loadGetTextureParameteriv()
        {
            try
            {
                glGetTextureParameterivPtr = (glGetTextureParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureParameteriv"), typeof(glGetTextureParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureParameteriv'.");
            }
        }
        public static void glGetTextureParameteriv(GLuint @texture, GLenum @pname, GLint * @params) => glGetTextureParameterivPtr(@texture, @pname, @params);

        internal delegate void glCreateVertexArraysFunc(GLsizei @n, GLuint * @arrays);
        internal static glCreateVertexArraysFunc glCreateVertexArraysPtr;
        internal static void loadCreateVertexArrays()
        {
            try
            {
                glCreateVertexArraysPtr = (glCreateVertexArraysFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateVertexArrays"), typeof(glCreateVertexArraysFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateVertexArrays'.");
            }
        }
        public static void glCreateVertexArrays(GLsizei @n, GLuint * @arrays) => glCreateVertexArraysPtr(@n, @arrays);

        internal delegate void glDisableVertexArrayAttribFunc(GLuint @vaobj, GLuint @index);
        internal static glDisableVertexArrayAttribFunc glDisableVertexArrayAttribPtr;
        internal static void loadDisableVertexArrayAttrib()
        {
            try
            {
                glDisableVertexArrayAttribPtr = (glDisableVertexArrayAttribFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableVertexArrayAttrib"), typeof(glDisableVertexArrayAttribFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableVertexArrayAttrib'.");
            }
        }
        public static void glDisableVertexArrayAttrib(GLuint @vaobj, GLuint @index) => glDisableVertexArrayAttribPtr(@vaobj, @index);

        internal delegate void glEnableVertexArrayAttribFunc(GLuint @vaobj, GLuint @index);
        internal static glEnableVertexArrayAttribFunc glEnableVertexArrayAttribPtr;
        internal static void loadEnableVertexArrayAttrib()
        {
            try
            {
                glEnableVertexArrayAttribPtr = (glEnableVertexArrayAttribFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableVertexArrayAttrib"), typeof(glEnableVertexArrayAttribFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableVertexArrayAttrib'.");
            }
        }
        public static void glEnableVertexArrayAttrib(GLuint @vaobj, GLuint @index) => glEnableVertexArrayAttribPtr(@vaobj, @index);

        internal delegate void glVertexArrayElementBufferFunc(GLuint @vaobj, GLuint @buffer);
        internal static glVertexArrayElementBufferFunc glVertexArrayElementBufferPtr;
        internal static void loadVertexArrayElementBuffer()
        {
            try
            {
                glVertexArrayElementBufferPtr = (glVertexArrayElementBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayElementBuffer"), typeof(glVertexArrayElementBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayElementBuffer'.");
            }
        }
        public static void glVertexArrayElementBuffer(GLuint @vaobj, GLuint @buffer) => glVertexArrayElementBufferPtr(@vaobj, @buffer);

        internal delegate void glVertexArrayVertexBufferFunc(GLuint @vaobj, GLuint @bindingindex, GLuint @buffer, GLintptr @offset, GLsizei @stride);
        internal static glVertexArrayVertexBufferFunc glVertexArrayVertexBufferPtr;
        internal static void loadVertexArrayVertexBuffer()
        {
            try
            {
                glVertexArrayVertexBufferPtr = (glVertexArrayVertexBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayVertexBuffer"), typeof(glVertexArrayVertexBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayVertexBuffer'.");
            }
        }
        public static void glVertexArrayVertexBuffer(GLuint @vaobj, GLuint @bindingindex, GLuint @buffer, GLintptr @offset, GLsizei @stride) => glVertexArrayVertexBufferPtr(@vaobj, @bindingindex, @buffer, @offset, @stride);

        internal delegate void glVertexArrayVertexBuffersFunc(GLuint @vaobj, GLuint @first, GLsizei @count, const GLuint * @buffers, const GLintptr * @offsets, const GLsizei * @strides);
        internal static glVertexArrayVertexBuffersFunc glVertexArrayVertexBuffersPtr;
        internal static void loadVertexArrayVertexBuffers()
        {
            try
            {
                glVertexArrayVertexBuffersPtr = (glVertexArrayVertexBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayVertexBuffers"), typeof(glVertexArrayVertexBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayVertexBuffers'.");
            }
        }
        public static void glVertexArrayVertexBuffers(GLuint @vaobj, GLuint @first, GLsizei @count, const GLuint * @buffers, const GLintptr * @offsets, const GLsizei * @strides) => glVertexArrayVertexBuffersPtr(@vaobj, @first, @count, @buffers, @offsets, @strides);

        internal delegate void glVertexArrayAttribBindingFunc(GLuint @vaobj, GLuint @attribindex, GLuint @bindingindex);
        internal static glVertexArrayAttribBindingFunc glVertexArrayAttribBindingPtr;
        internal static void loadVertexArrayAttribBinding()
        {
            try
            {
                glVertexArrayAttribBindingPtr = (glVertexArrayAttribBindingFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayAttribBinding"), typeof(glVertexArrayAttribBindingFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayAttribBinding'.");
            }
        }
        public static void glVertexArrayAttribBinding(GLuint @vaobj, GLuint @attribindex, GLuint @bindingindex) => glVertexArrayAttribBindingPtr(@vaobj, @attribindex, @bindingindex);

        internal delegate void glVertexArrayAttribFormatFunc(GLuint @vaobj, GLuint @attribindex, GLint @size, GLenum @type, GLboolean @normalized, GLuint @relativeoffset);
        internal static glVertexArrayAttribFormatFunc glVertexArrayAttribFormatPtr;
        internal static void loadVertexArrayAttribFormat()
        {
            try
            {
                glVertexArrayAttribFormatPtr = (glVertexArrayAttribFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayAttribFormat"), typeof(glVertexArrayAttribFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayAttribFormat'.");
            }
        }
        public static void glVertexArrayAttribFormat(GLuint @vaobj, GLuint @attribindex, GLint @size, GLenum @type, GLboolean @normalized, GLuint @relativeoffset) => glVertexArrayAttribFormatPtr(@vaobj, @attribindex, @size, @type, @normalized, @relativeoffset);

        internal delegate void glVertexArrayAttribIFormatFunc(GLuint @vaobj, GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset);
        internal static glVertexArrayAttribIFormatFunc glVertexArrayAttribIFormatPtr;
        internal static void loadVertexArrayAttribIFormat()
        {
            try
            {
                glVertexArrayAttribIFormatPtr = (glVertexArrayAttribIFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayAttribIFormat"), typeof(glVertexArrayAttribIFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayAttribIFormat'.");
            }
        }
        public static void glVertexArrayAttribIFormat(GLuint @vaobj, GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset) => glVertexArrayAttribIFormatPtr(@vaobj, @attribindex, @size, @type, @relativeoffset);

        internal delegate void glVertexArrayAttribLFormatFunc(GLuint @vaobj, GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset);
        internal static glVertexArrayAttribLFormatFunc glVertexArrayAttribLFormatPtr;
        internal static void loadVertexArrayAttribLFormat()
        {
            try
            {
                glVertexArrayAttribLFormatPtr = (glVertexArrayAttribLFormatFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayAttribLFormat"), typeof(glVertexArrayAttribLFormatFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayAttribLFormat'.");
            }
        }
        public static void glVertexArrayAttribLFormat(GLuint @vaobj, GLuint @attribindex, GLint @size, GLenum @type, GLuint @relativeoffset) => glVertexArrayAttribLFormatPtr(@vaobj, @attribindex, @size, @type, @relativeoffset);

        internal delegate void glVertexArrayBindingDivisorFunc(GLuint @vaobj, GLuint @bindingindex, GLuint @divisor);
        internal static glVertexArrayBindingDivisorFunc glVertexArrayBindingDivisorPtr;
        internal static void loadVertexArrayBindingDivisor()
        {
            try
            {
                glVertexArrayBindingDivisorPtr = (glVertexArrayBindingDivisorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexArrayBindingDivisor"), typeof(glVertexArrayBindingDivisorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexArrayBindingDivisor'.");
            }
        }
        public static void glVertexArrayBindingDivisor(GLuint @vaobj, GLuint @bindingindex, GLuint @divisor) => glVertexArrayBindingDivisorPtr(@vaobj, @bindingindex, @divisor);

        internal delegate void glGetVertexArrayivFunc(GLuint @vaobj, GLenum @pname, GLint * @param);
        internal static glGetVertexArrayivFunc glGetVertexArrayivPtr;
        internal static void loadGetVertexArrayiv()
        {
            try
            {
                glGetVertexArrayivPtr = (glGetVertexArrayivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexArrayiv"), typeof(glGetVertexArrayivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexArrayiv'.");
            }
        }
        public static void glGetVertexArrayiv(GLuint @vaobj, GLenum @pname, GLint * @param) => glGetVertexArrayivPtr(@vaobj, @pname, @param);

        internal delegate void glGetVertexArrayIndexedivFunc(GLuint @vaobj, GLuint @index, GLenum @pname, GLint * @param);
        internal static glGetVertexArrayIndexedivFunc glGetVertexArrayIndexedivPtr;
        internal static void loadGetVertexArrayIndexediv()
        {
            try
            {
                glGetVertexArrayIndexedivPtr = (glGetVertexArrayIndexedivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexArrayIndexediv"), typeof(glGetVertexArrayIndexedivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexArrayIndexediv'.");
            }
        }
        public static void glGetVertexArrayIndexediv(GLuint @vaobj, GLuint @index, GLenum @pname, GLint * @param) => glGetVertexArrayIndexedivPtr(@vaobj, @index, @pname, @param);

        internal delegate void glGetVertexArrayIndexed64ivFunc(GLuint @vaobj, GLuint @index, GLenum @pname, GLint64 * @param);
        internal static glGetVertexArrayIndexed64ivFunc glGetVertexArrayIndexed64ivPtr;
        internal static void loadGetVertexArrayIndexed64iv()
        {
            try
            {
                glGetVertexArrayIndexed64ivPtr = (glGetVertexArrayIndexed64ivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexArrayIndexed64iv"), typeof(glGetVertexArrayIndexed64ivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexArrayIndexed64iv'.");
            }
        }
        public static void glGetVertexArrayIndexed64iv(GLuint @vaobj, GLuint @index, GLenum @pname, GLint64 * @param) => glGetVertexArrayIndexed64ivPtr(@vaobj, @index, @pname, @param);

        internal delegate void glCreateSamplersFunc(GLsizei @n, GLuint * @samplers);
        internal static glCreateSamplersFunc glCreateSamplersPtr;
        internal static void loadCreateSamplers()
        {
            try
            {
                glCreateSamplersPtr = (glCreateSamplersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateSamplers"), typeof(glCreateSamplersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateSamplers'.");
            }
        }
        public static void glCreateSamplers(GLsizei @n, GLuint * @samplers) => glCreateSamplersPtr(@n, @samplers);

        internal delegate void glCreateProgramPipelinesFunc(GLsizei @n, GLuint * @pipelines);
        internal static glCreateProgramPipelinesFunc glCreateProgramPipelinesPtr;
        internal static void loadCreateProgramPipelines()
        {
            try
            {
                glCreateProgramPipelinesPtr = (glCreateProgramPipelinesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateProgramPipelines"), typeof(glCreateProgramPipelinesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateProgramPipelines'.");
            }
        }
        public static void glCreateProgramPipelines(GLsizei @n, GLuint * @pipelines) => glCreateProgramPipelinesPtr(@n, @pipelines);

        internal delegate void glCreateQueriesFunc(GLenum @target, GLsizei @n, GLuint * @ids);
        internal static glCreateQueriesFunc glCreateQueriesPtr;
        internal static void loadCreateQueries()
        {
            try
            {
                glCreateQueriesPtr = (glCreateQueriesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateQueries"), typeof(glCreateQueriesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateQueries'.");
            }
        }
        public static void glCreateQueries(GLenum @target, GLsizei @n, GLuint * @ids) => glCreateQueriesPtr(@target, @n, @ids);

        internal delegate void glGetQueryBufferObjecti64vFunc(GLuint @id, GLuint @buffer, GLenum @pname, GLintptr @offset);
        internal static glGetQueryBufferObjecti64vFunc glGetQueryBufferObjecti64vPtr;
        internal static void loadGetQueryBufferObjecti64v()
        {
            try
            {
                glGetQueryBufferObjecti64vPtr = (glGetQueryBufferObjecti64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryBufferObjecti64v"), typeof(glGetQueryBufferObjecti64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryBufferObjecti64v'.");
            }
        }
        public static void glGetQueryBufferObjecti64v(GLuint @id, GLuint @buffer, GLenum @pname, GLintptr @offset) => glGetQueryBufferObjecti64vPtr(@id, @buffer, @pname, @offset);

        internal delegate void glGetQueryBufferObjectivFunc(GLuint @id, GLuint @buffer, GLenum @pname, GLintptr @offset);
        internal static glGetQueryBufferObjectivFunc glGetQueryBufferObjectivPtr;
        internal static void loadGetQueryBufferObjectiv()
        {
            try
            {
                glGetQueryBufferObjectivPtr = (glGetQueryBufferObjectivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryBufferObjectiv"), typeof(glGetQueryBufferObjectivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryBufferObjectiv'.");
            }
        }
        public static void glGetQueryBufferObjectiv(GLuint @id, GLuint @buffer, GLenum @pname, GLintptr @offset) => glGetQueryBufferObjectivPtr(@id, @buffer, @pname, @offset);

        internal delegate void glGetQueryBufferObjectui64vFunc(GLuint @id, GLuint @buffer, GLenum @pname, GLintptr @offset);
        internal static glGetQueryBufferObjectui64vFunc glGetQueryBufferObjectui64vPtr;
        internal static void loadGetQueryBufferObjectui64v()
        {
            try
            {
                glGetQueryBufferObjectui64vPtr = (glGetQueryBufferObjectui64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryBufferObjectui64v"), typeof(glGetQueryBufferObjectui64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryBufferObjectui64v'.");
            }
        }
        public static void glGetQueryBufferObjectui64v(GLuint @id, GLuint @buffer, GLenum @pname, GLintptr @offset) => glGetQueryBufferObjectui64vPtr(@id, @buffer, @pname, @offset);

        internal delegate void glGetQueryBufferObjectuivFunc(GLuint @id, GLuint @buffer, GLenum @pname, GLintptr @offset);
        internal static glGetQueryBufferObjectuivFunc glGetQueryBufferObjectuivPtr;
        internal static void loadGetQueryBufferObjectuiv()
        {
            try
            {
                glGetQueryBufferObjectuivPtr = (glGetQueryBufferObjectuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryBufferObjectuiv"), typeof(glGetQueryBufferObjectuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryBufferObjectuiv'.");
            }
        }
        public static void glGetQueryBufferObjectuiv(GLuint @id, GLuint @buffer, GLenum @pname, GLintptr @offset) => glGetQueryBufferObjectuivPtr(@id, @buffer, @pname, @offset);
        #endregion
    }
}
