using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_sample_shading
    {
        #region Interop
        static GL_ARB_sample_shading()
        {
            Console.WriteLine("Initalising GL_ARB_sample_shading interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMinSampleShadingARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SAMPLE_SHADING_ARB = 0x8C36;
        public static UInt32 GL_MIN_SAMPLE_SHADING_VALUE_ARB = 0x8C37;
        #endregion

        #region Commands
        internal delegate void glMinSampleShadingARBFunc(GLfloat @value);
        internal static glMinSampleShadingARBFunc glMinSampleShadingARBPtr;
        internal static void loadMinSampleShadingARB()
        {
            try
            {
                glMinSampleShadingARBPtr = (glMinSampleShadingARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMinSampleShadingARB"), typeof(glMinSampleShadingARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMinSampleShadingARB'.");
            }
        }
        public static void glMinSampleShadingARB(GLfloat @value) => glMinSampleShadingARBPtr(@value);
        #endregion
    }
}
