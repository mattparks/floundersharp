using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_matrix_palette
    {
        #region Interop
        static GL_ARB_matrix_palette()
        {
            Console.WriteLine("Initalising GL_ARB_matrix_palette interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCurrentPaletteMatrixARB();
            loadMatrixIndexubvARB();
            loadMatrixIndexusvARB();
            loadMatrixIndexuivARB();
            loadMatrixIndexPointerARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MATRIX_PALETTE_ARB = 0x8840;
        public static UInt32 GL_MAX_MATRIX_PALETTE_STACK_DEPTH_ARB = 0x8841;
        public static UInt32 GL_MAX_PALETTE_MATRICES_ARB = 0x8842;
        public static UInt32 GL_CURRENT_PALETTE_MATRIX_ARB = 0x8843;
        public static UInt32 GL_MATRIX_INDEX_ARRAY_ARB = 0x8844;
        public static UInt32 GL_CURRENT_MATRIX_INDEX_ARB = 0x8845;
        public static UInt32 GL_MATRIX_INDEX_ARRAY_SIZE_ARB = 0x8846;
        public static UInt32 GL_MATRIX_INDEX_ARRAY_TYPE_ARB = 0x8847;
        public static UInt32 GL_MATRIX_INDEX_ARRAY_STRIDE_ARB = 0x8848;
        public static UInt32 GL_MATRIX_INDEX_ARRAY_POINTER_ARB = 0x8849;
        #endregion

        #region Commands
        internal delegate void glCurrentPaletteMatrixARBFunc(GLint @index);
        internal static glCurrentPaletteMatrixARBFunc glCurrentPaletteMatrixARBPtr;
        internal static void loadCurrentPaletteMatrixARB()
        {
            try
            {
                glCurrentPaletteMatrixARBPtr = (glCurrentPaletteMatrixARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCurrentPaletteMatrixARB"), typeof(glCurrentPaletteMatrixARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCurrentPaletteMatrixARB'.");
            }
        }
        public static void glCurrentPaletteMatrixARB(GLint @index) => glCurrentPaletteMatrixARBPtr(@index);

        internal delegate void glMatrixIndexubvARBFunc(GLint @size, const GLubyte * @indices);
        internal static glMatrixIndexubvARBFunc glMatrixIndexubvARBPtr;
        internal static void loadMatrixIndexubvARB()
        {
            try
            {
                glMatrixIndexubvARBPtr = (glMatrixIndexubvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixIndexubvARB"), typeof(glMatrixIndexubvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixIndexubvARB'.");
            }
        }
        public static void glMatrixIndexubvARB(GLint @size, const GLubyte * @indices) => glMatrixIndexubvARBPtr(@size, @indices);

        internal delegate void glMatrixIndexusvARBFunc(GLint @size, const GLushort * @indices);
        internal static glMatrixIndexusvARBFunc glMatrixIndexusvARBPtr;
        internal static void loadMatrixIndexusvARB()
        {
            try
            {
                glMatrixIndexusvARBPtr = (glMatrixIndexusvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixIndexusvARB"), typeof(glMatrixIndexusvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixIndexusvARB'.");
            }
        }
        public static void glMatrixIndexusvARB(GLint @size, const GLushort * @indices) => glMatrixIndexusvARBPtr(@size, @indices);

        internal delegate void glMatrixIndexuivARBFunc(GLint @size, const GLuint * @indices);
        internal static glMatrixIndexuivARBFunc glMatrixIndexuivARBPtr;
        internal static void loadMatrixIndexuivARB()
        {
            try
            {
                glMatrixIndexuivARBPtr = (glMatrixIndexuivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixIndexuivARB"), typeof(glMatrixIndexuivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixIndexuivARB'.");
            }
        }
        public static void glMatrixIndexuivARB(GLint @size, const GLuint * @indices) => glMatrixIndexuivARBPtr(@size, @indices);

        internal delegate void glMatrixIndexPointerARBFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glMatrixIndexPointerARBFunc glMatrixIndexPointerARBPtr;
        internal static void loadMatrixIndexPointerARB()
        {
            try
            {
                glMatrixIndexPointerARBPtr = (glMatrixIndexPointerARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixIndexPointerARB"), typeof(glMatrixIndexPointerARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixIndexPointerARB'.");
            }
        }
        public static void glMatrixIndexPointerARB(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glMatrixIndexPointerARBPtr(@size, @type, @stride, @pointer);
        #endregion
    }
}
