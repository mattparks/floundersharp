using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_barrier
    {
        #region Interop
        static GL_ARB_texture_barrier()
        {
            Console.WriteLine("Initalising GL_ARB_texture_barrier interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTextureBarrier();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glTextureBarrierFunc();
        internal static glTextureBarrierFunc glTextureBarrierPtr;
        internal static void loadTextureBarrier()
        {
            try
            {
                glTextureBarrierPtr = (glTextureBarrierFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTextureBarrier"), typeof(glTextureBarrierFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTextureBarrier'.");
            }
        }
        public static void glTextureBarrier() => glTextureBarrierPtr();
        #endregion
    }
}
