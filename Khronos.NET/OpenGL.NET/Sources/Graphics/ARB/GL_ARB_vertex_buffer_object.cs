using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_vertex_buffer_object
    {
        #region Interop
        static GL_ARB_vertex_buffer_object()
        {
            Console.WriteLine("Initalising GL_ARB_vertex_buffer_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindBufferARB();
            loadDeleteBuffersARB();
            loadGenBuffersARB();
            loadIsBufferARB();
            loadBufferDataARB();
            loadBufferSubDataARB();
            loadGetBufferSubDataARB();
            loadMapBufferARB();
            loadUnmapBufferARB();
            loadGetBufferParameterivARB();
            loadGetBufferPointervARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_BUFFER_SIZE_ARB = 0x8764;
        public static UInt32 GL_BUFFER_USAGE_ARB = 0x8765;
        public static UInt32 GL_ARRAY_BUFFER_ARB = 0x8892;
        public static UInt32 GL_ELEMENT_ARRAY_BUFFER_ARB = 0x8893;
        public static UInt32 GL_ARRAY_BUFFER_BINDING_ARB = 0x8894;
        public static UInt32 GL_ELEMENT_ARRAY_BUFFER_BINDING_ARB = 0x8895;
        public static UInt32 GL_VERTEX_ARRAY_BUFFER_BINDING_ARB = 0x8896;
        public static UInt32 GL_NORMAL_ARRAY_BUFFER_BINDING_ARB = 0x8897;
        public static UInt32 GL_COLOR_ARRAY_BUFFER_BINDING_ARB = 0x8898;
        public static UInt32 GL_INDEX_ARRAY_BUFFER_BINDING_ARB = 0x8899;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_BUFFER_BINDING_ARB = 0x889A;
        public static UInt32 GL_EDGE_FLAG_ARRAY_BUFFER_BINDING_ARB = 0x889B;
        public static UInt32 GL_SECONDARY_COLOR_ARRAY_BUFFER_BINDING_ARB = 0x889C;
        public static UInt32 GL_FOG_COORDINATE_ARRAY_BUFFER_BINDING_ARB = 0x889D;
        public static UInt32 GL_WEIGHT_ARRAY_BUFFER_BINDING_ARB = 0x889E;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING_ARB = 0x889F;
        public static UInt32 GL_READ_ONLY_ARB = 0x88B8;
        public static UInt32 GL_WRITE_ONLY_ARB = 0x88B9;
        public static UInt32 GL_READ_WRITE_ARB = 0x88BA;
        public static UInt32 GL_BUFFER_ACCESS_ARB = 0x88BB;
        public static UInt32 GL_BUFFER_MAPPED_ARB = 0x88BC;
        public static UInt32 GL_BUFFER_MAP_POINTER_ARB = 0x88BD;
        public static UInt32 GL_STREAM_DRAW_ARB = 0x88E0;
        public static UInt32 GL_STREAM_READ_ARB = 0x88E1;
        public static UInt32 GL_STREAM_COPY_ARB = 0x88E2;
        public static UInt32 GL_STATIC_DRAW_ARB = 0x88E4;
        public static UInt32 GL_STATIC_READ_ARB = 0x88E5;
        public static UInt32 GL_STATIC_COPY_ARB = 0x88E6;
        public static UInt32 GL_DYNAMIC_DRAW_ARB = 0x88E8;
        public static UInt32 GL_DYNAMIC_READ_ARB = 0x88E9;
        public static UInt32 GL_DYNAMIC_COPY_ARB = 0x88EA;
        #endregion

        #region Commands
        internal delegate void glBindBufferARBFunc(GLenum @target, GLuint @buffer);
        internal static glBindBufferARBFunc glBindBufferARBPtr;
        internal static void loadBindBufferARB()
        {
            try
            {
                glBindBufferARBPtr = (glBindBufferARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBufferARB"), typeof(glBindBufferARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBufferARB'.");
            }
        }
        public static void glBindBufferARB(GLenum @target, GLuint @buffer) => glBindBufferARBPtr(@target, @buffer);

        internal delegate void glDeleteBuffersARBFunc(GLsizei @n, const GLuint * @buffers);
        internal static glDeleteBuffersARBFunc glDeleteBuffersARBPtr;
        internal static void loadDeleteBuffersARB()
        {
            try
            {
                glDeleteBuffersARBPtr = (glDeleteBuffersARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteBuffersARB"), typeof(glDeleteBuffersARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteBuffersARB'.");
            }
        }
        public static void glDeleteBuffersARB(GLsizei @n, const GLuint * @buffers) => glDeleteBuffersARBPtr(@n, @buffers);

        internal delegate void glGenBuffersARBFunc(GLsizei @n, GLuint * @buffers);
        internal static glGenBuffersARBFunc glGenBuffersARBPtr;
        internal static void loadGenBuffersARB()
        {
            try
            {
                glGenBuffersARBPtr = (glGenBuffersARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenBuffersARB"), typeof(glGenBuffersARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenBuffersARB'.");
            }
        }
        public static void glGenBuffersARB(GLsizei @n, GLuint * @buffers) => glGenBuffersARBPtr(@n, @buffers);

        internal delegate GLboolean glIsBufferARBFunc(GLuint @buffer);
        internal static glIsBufferARBFunc glIsBufferARBPtr;
        internal static void loadIsBufferARB()
        {
            try
            {
                glIsBufferARBPtr = (glIsBufferARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsBufferARB"), typeof(glIsBufferARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsBufferARB'.");
            }
        }
        public static GLboolean glIsBufferARB(GLuint @buffer) => glIsBufferARBPtr(@buffer);

        internal delegate void glBufferDataARBFunc(GLenum @target, GLsizeiptrARB @size, const void * @data, GLenum @usage);
        internal static glBufferDataARBFunc glBufferDataARBPtr;
        internal static void loadBufferDataARB()
        {
            try
            {
                glBufferDataARBPtr = (glBufferDataARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferDataARB"), typeof(glBufferDataARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferDataARB'.");
            }
        }
        public static void glBufferDataARB(GLenum @target, GLsizeiptrARB @size, const void * @data, GLenum @usage) => glBufferDataARBPtr(@target, @size, @data, @usage);

        internal delegate void glBufferSubDataARBFunc(GLenum @target, GLintptrARB @offset, GLsizeiptrARB @size, const void * @data);
        internal static glBufferSubDataARBFunc glBufferSubDataARBPtr;
        internal static void loadBufferSubDataARB()
        {
            try
            {
                glBufferSubDataARBPtr = (glBufferSubDataARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferSubDataARB"), typeof(glBufferSubDataARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferSubDataARB'.");
            }
        }
        public static void glBufferSubDataARB(GLenum @target, GLintptrARB @offset, GLsizeiptrARB @size, const void * @data) => glBufferSubDataARBPtr(@target, @offset, @size, @data);

        internal delegate void glGetBufferSubDataARBFunc(GLenum @target, GLintptrARB @offset, GLsizeiptrARB @size, void * @data);
        internal static glGetBufferSubDataARBFunc glGetBufferSubDataARBPtr;
        internal static void loadGetBufferSubDataARB()
        {
            try
            {
                glGetBufferSubDataARBPtr = (glGetBufferSubDataARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferSubDataARB"), typeof(glGetBufferSubDataARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferSubDataARB'.");
            }
        }
        public static void glGetBufferSubDataARB(GLenum @target, GLintptrARB @offset, GLsizeiptrARB @size, void * @data) => glGetBufferSubDataARBPtr(@target, @offset, @size, @data);

        internal delegate void * glMapBufferARBFunc(GLenum @target, GLenum @access);
        internal static glMapBufferARBFunc glMapBufferARBPtr;
        internal static void loadMapBufferARB()
        {
            try
            {
                glMapBufferARBPtr = (glMapBufferARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapBufferARB"), typeof(glMapBufferARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapBufferARB'.");
            }
        }
        public static void * glMapBufferARB(GLenum @target, GLenum @access) => glMapBufferARBPtr(@target, @access);

        internal delegate GLboolean glUnmapBufferARBFunc(GLenum @target);
        internal static glUnmapBufferARBFunc glUnmapBufferARBPtr;
        internal static void loadUnmapBufferARB()
        {
            try
            {
                glUnmapBufferARBPtr = (glUnmapBufferARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUnmapBufferARB"), typeof(glUnmapBufferARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUnmapBufferARB'.");
            }
        }
        public static GLboolean glUnmapBufferARB(GLenum @target) => glUnmapBufferARBPtr(@target);

        internal delegate void glGetBufferParameterivARBFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetBufferParameterivARBFunc glGetBufferParameterivARBPtr;
        internal static void loadGetBufferParameterivARB()
        {
            try
            {
                glGetBufferParameterivARBPtr = (glGetBufferParameterivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferParameterivARB"), typeof(glGetBufferParameterivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferParameterivARB'.");
            }
        }
        public static void glGetBufferParameterivARB(GLenum @target, GLenum @pname, GLint * @params) => glGetBufferParameterivARBPtr(@target, @pname, @params);

        internal delegate void glGetBufferPointervARBFunc(GLenum @target, GLenum @pname, void ** @params);
        internal static glGetBufferPointervARBFunc glGetBufferPointervARBPtr;
        internal static void loadGetBufferPointervARB()
        {
            try
            {
                glGetBufferPointervARBPtr = (glGetBufferPointervARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferPointervARB"), typeof(glGetBufferPointervARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferPointervARB'.");
            }
        }
        public static void glGetBufferPointervARB(GLenum @target, GLenum @pname, void ** @params) => glGetBufferPointervARBPtr(@target, @pname, @params);
        #endregion
    }
}
