using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_bindless_texture
    {
        #region Interop
        static GL_ARB_bindless_texture()
        {
            Console.WriteLine("Initalising GL_ARB_bindless_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetTextureHandleARB();
            loadGetTextureSamplerHandleARB();
            loadMakeTextureHandleResidentARB();
            loadMakeTextureHandleNonResidentARB();
            loadGetImageHandleARB();
            loadMakeImageHandleResidentARB();
            loadMakeImageHandleNonResidentARB();
            loadUniformHandleui64ARB();
            loadUniformHandleui64vARB();
            loadProgramUniformHandleui64ARB();
            loadProgramUniformHandleui64vARB();
            loadIsTextureHandleResidentARB();
            loadIsImageHandleResidentARB();
            loadVertexAttribL1ui64ARB();
            loadVertexAttribL1ui64vARB();
            loadGetVertexAttribLui64vARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_UNSIGNED_INT64_ARB = 0x140F;
        #endregion

        #region Commands
        internal delegate GLuint64 glGetTextureHandleARBFunc(GLuint @texture);
        internal static glGetTextureHandleARBFunc glGetTextureHandleARBPtr;
        internal static void loadGetTextureHandleARB()
        {
            try
            {
                glGetTextureHandleARBPtr = (glGetTextureHandleARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureHandleARB"), typeof(glGetTextureHandleARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureHandleARB'.");
            }
        }
        public static GLuint64 glGetTextureHandleARB(GLuint @texture) => glGetTextureHandleARBPtr(@texture);

        internal delegate GLuint64 glGetTextureSamplerHandleARBFunc(GLuint @texture, GLuint @sampler);
        internal static glGetTextureSamplerHandleARBFunc glGetTextureSamplerHandleARBPtr;
        internal static void loadGetTextureSamplerHandleARB()
        {
            try
            {
                glGetTextureSamplerHandleARBPtr = (glGetTextureSamplerHandleARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureSamplerHandleARB"), typeof(glGetTextureSamplerHandleARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureSamplerHandleARB'.");
            }
        }
        public static GLuint64 glGetTextureSamplerHandleARB(GLuint @texture, GLuint @sampler) => glGetTextureSamplerHandleARBPtr(@texture, @sampler);

        internal delegate void glMakeTextureHandleResidentARBFunc(GLuint64 @handle);
        internal static glMakeTextureHandleResidentARBFunc glMakeTextureHandleResidentARBPtr;
        internal static void loadMakeTextureHandleResidentARB()
        {
            try
            {
                glMakeTextureHandleResidentARBPtr = (glMakeTextureHandleResidentARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMakeTextureHandleResidentARB"), typeof(glMakeTextureHandleResidentARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMakeTextureHandleResidentARB'.");
            }
        }
        public static void glMakeTextureHandleResidentARB(GLuint64 @handle) => glMakeTextureHandleResidentARBPtr(@handle);

        internal delegate void glMakeTextureHandleNonResidentARBFunc(GLuint64 @handle);
        internal static glMakeTextureHandleNonResidentARBFunc glMakeTextureHandleNonResidentARBPtr;
        internal static void loadMakeTextureHandleNonResidentARB()
        {
            try
            {
                glMakeTextureHandleNonResidentARBPtr = (glMakeTextureHandleNonResidentARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMakeTextureHandleNonResidentARB"), typeof(glMakeTextureHandleNonResidentARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMakeTextureHandleNonResidentARB'.");
            }
        }
        public static void glMakeTextureHandleNonResidentARB(GLuint64 @handle) => glMakeTextureHandleNonResidentARBPtr(@handle);

        internal delegate GLuint64 glGetImageHandleARBFunc(GLuint @texture, GLint @level, GLboolean @layered, GLint @layer, GLenum @format);
        internal static glGetImageHandleARBFunc glGetImageHandleARBPtr;
        internal static void loadGetImageHandleARB()
        {
            try
            {
                glGetImageHandleARBPtr = (glGetImageHandleARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetImageHandleARB"), typeof(glGetImageHandleARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetImageHandleARB'.");
            }
        }
        public static GLuint64 glGetImageHandleARB(GLuint @texture, GLint @level, GLboolean @layered, GLint @layer, GLenum @format) => glGetImageHandleARBPtr(@texture, @level, @layered, @layer, @format);

        internal delegate void glMakeImageHandleResidentARBFunc(GLuint64 @handle, GLenum @access);
        internal static glMakeImageHandleResidentARBFunc glMakeImageHandleResidentARBPtr;
        internal static void loadMakeImageHandleResidentARB()
        {
            try
            {
                glMakeImageHandleResidentARBPtr = (glMakeImageHandleResidentARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMakeImageHandleResidentARB"), typeof(glMakeImageHandleResidentARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMakeImageHandleResidentARB'.");
            }
        }
        public static void glMakeImageHandleResidentARB(GLuint64 @handle, GLenum @access) => glMakeImageHandleResidentARBPtr(@handle, @access);

        internal delegate void glMakeImageHandleNonResidentARBFunc(GLuint64 @handle);
        internal static glMakeImageHandleNonResidentARBFunc glMakeImageHandleNonResidentARBPtr;
        internal static void loadMakeImageHandleNonResidentARB()
        {
            try
            {
                glMakeImageHandleNonResidentARBPtr = (glMakeImageHandleNonResidentARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMakeImageHandleNonResidentARB"), typeof(glMakeImageHandleNonResidentARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMakeImageHandleNonResidentARB'.");
            }
        }
        public static void glMakeImageHandleNonResidentARB(GLuint64 @handle) => glMakeImageHandleNonResidentARBPtr(@handle);

        internal delegate void glUniformHandleui64ARBFunc(GLint @location, GLuint64 @value);
        internal static glUniformHandleui64ARBFunc glUniformHandleui64ARBPtr;
        internal static void loadUniformHandleui64ARB()
        {
            try
            {
                glUniformHandleui64ARBPtr = (glUniformHandleui64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformHandleui64ARB"), typeof(glUniformHandleui64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformHandleui64ARB'.");
            }
        }
        public static void glUniformHandleui64ARB(GLint @location, GLuint64 @value) => glUniformHandleui64ARBPtr(@location, @value);

        internal delegate void glUniformHandleui64vARBFunc(GLint @location, GLsizei @count, const GLuint64 * @value);
        internal static glUniformHandleui64vARBFunc glUniformHandleui64vARBPtr;
        internal static void loadUniformHandleui64vARB()
        {
            try
            {
                glUniformHandleui64vARBPtr = (glUniformHandleui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformHandleui64vARB"), typeof(glUniformHandleui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformHandleui64vARB'.");
            }
        }
        public static void glUniformHandleui64vARB(GLint @location, GLsizei @count, const GLuint64 * @value) => glUniformHandleui64vARBPtr(@location, @count, @value);

        internal delegate void glProgramUniformHandleui64ARBFunc(GLuint @program, GLint @location, GLuint64 @value);
        internal static glProgramUniformHandleui64ARBFunc glProgramUniformHandleui64ARBPtr;
        internal static void loadProgramUniformHandleui64ARB()
        {
            try
            {
                glProgramUniformHandleui64ARBPtr = (glProgramUniformHandleui64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformHandleui64ARB"), typeof(glProgramUniformHandleui64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformHandleui64ARB'.");
            }
        }
        public static void glProgramUniformHandleui64ARB(GLuint @program, GLint @location, GLuint64 @value) => glProgramUniformHandleui64ARBPtr(@program, @location, @value);

        internal delegate void glProgramUniformHandleui64vARBFunc(GLuint @program, GLint @location, GLsizei @count, const GLuint64 * @values);
        internal static glProgramUniformHandleui64vARBFunc glProgramUniformHandleui64vARBPtr;
        internal static void loadProgramUniformHandleui64vARB()
        {
            try
            {
                glProgramUniformHandleui64vARBPtr = (glProgramUniformHandleui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramUniformHandleui64vARB"), typeof(glProgramUniformHandleui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramUniformHandleui64vARB'.");
            }
        }
        public static void glProgramUniformHandleui64vARB(GLuint @program, GLint @location, GLsizei @count, const GLuint64 * @values) => glProgramUniformHandleui64vARBPtr(@program, @location, @count, @values);

        internal delegate GLboolean glIsTextureHandleResidentARBFunc(GLuint64 @handle);
        internal static glIsTextureHandleResidentARBFunc glIsTextureHandleResidentARBPtr;
        internal static void loadIsTextureHandleResidentARB()
        {
            try
            {
                glIsTextureHandleResidentARBPtr = (glIsTextureHandleResidentARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsTextureHandleResidentARB"), typeof(glIsTextureHandleResidentARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsTextureHandleResidentARB'.");
            }
        }
        public static GLboolean glIsTextureHandleResidentARB(GLuint64 @handle) => glIsTextureHandleResidentARBPtr(@handle);

        internal delegate GLboolean glIsImageHandleResidentARBFunc(GLuint64 @handle);
        internal static glIsImageHandleResidentARBFunc glIsImageHandleResidentARBPtr;
        internal static void loadIsImageHandleResidentARB()
        {
            try
            {
                glIsImageHandleResidentARBPtr = (glIsImageHandleResidentARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsImageHandleResidentARB"), typeof(glIsImageHandleResidentARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsImageHandleResidentARB'.");
            }
        }
        public static GLboolean glIsImageHandleResidentARB(GLuint64 @handle) => glIsImageHandleResidentARBPtr(@handle);

        internal delegate void glVertexAttribL1ui64ARBFunc(GLuint @index, GLuint64EXT @x);
        internal static glVertexAttribL1ui64ARBFunc glVertexAttribL1ui64ARBPtr;
        internal static void loadVertexAttribL1ui64ARB()
        {
            try
            {
                glVertexAttribL1ui64ARBPtr = (glVertexAttribL1ui64ARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL1ui64ARB"), typeof(glVertexAttribL1ui64ARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL1ui64ARB'.");
            }
        }
        public static void glVertexAttribL1ui64ARB(GLuint @index, GLuint64EXT @x) => glVertexAttribL1ui64ARBPtr(@index, @x);

        internal delegate void glVertexAttribL1ui64vARBFunc(GLuint @index, const GLuint64EXT * @v);
        internal static glVertexAttribL1ui64vARBFunc glVertexAttribL1ui64vARBPtr;
        internal static void loadVertexAttribL1ui64vARB()
        {
            try
            {
                glVertexAttribL1ui64vARBPtr = (glVertexAttribL1ui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL1ui64vARB"), typeof(glVertexAttribL1ui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL1ui64vARB'.");
            }
        }
        public static void glVertexAttribL1ui64vARB(GLuint @index, const GLuint64EXT * @v) => glVertexAttribL1ui64vARBPtr(@index, @v);

        internal delegate void glGetVertexAttribLui64vARBFunc(GLuint @index, GLenum @pname, GLuint64EXT * @params);
        internal static glGetVertexAttribLui64vARBFunc glGetVertexAttribLui64vARBPtr;
        internal static void loadGetVertexAttribLui64vARB()
        {
            try
            {
                glGetVertexAttribLui64vARBPtr = (glGetVertexAttribLui64vARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribLui64vARB"), typeof(glGetVertexAttribLui64vARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribLui64vARB'.");
            }
        }
        public static void glGetVertexAttribLui64vARB(GLuint @index, GLenum @pname, GLuint64EXT * @params) => glGetVertexAttribLui64vARBPtr(@index, @pname, @params);
        #endregion
    }
}
