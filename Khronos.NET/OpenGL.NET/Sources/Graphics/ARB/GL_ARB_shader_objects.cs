using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_shader_objects
    {
        #region Interop
        static GL_ARB_shader_objects()
        {
            Console.WriteLine("Initalising GL_ARB_shader_objects interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDeleteObjectARB();
            loadGetHandleARB();
            loadDetachObjectARB();
            loadCreateShaderObjectARB();
            loadShaderSourceARB();
            loadCompileShaderARB();
            loadCreateProgramObjectARB();
            loadAttachObjectARB();
            loadLinkProgramARB();
            loadUseProgramObjectARB();
            loadValidateProgramARB();
            loadUniform1fARB();
            loadUniform2fARB();
            loadUniform3fARB();
            loadUniform4fARB();
            loadUniform1iARB();
            loadUniform2iARB();
            loadUniform3iARB();
            loadUniform4iARB();
            loadUniform1fvARB();
            loadUniform2fvARB();
            loadUniform3fvARB();
            loadUniform4fvARB();
            loadUniform1ivARB();
            loadUniform2ivARB();
            loadUniform3ivARB();
            loadUniform4ivARB();
            loadUniformMatrix2fvARB();
            loadUniformMatrix3fvARB();
            loadUniformMatrix4fvARB();
            loadGetObjectParameterfvARB();
            loadGetObjectParameterivARB();
            loadGetInfoLogARB();
            loadGetAttachedObjectsARB();
            loadGetUniformLocationARB();
            loadGetActiveUniformARB();
            loadGetUniformfvARB();
            loadGetUniformivARB();
            loadGetShaderSourceARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PROGRAM_OBJECT_ARB = 0x8B40;
        public static UInt32 GL_SHADER_OBJECT_ARB = 0x8B48;
        public static UInt32 GL_OBJECT_TYPE_ARB = 0x8B4E;
        public static UInt32 GL_OBJECT_SUBTYPE_ARB = 0x8B4F;
        public static UInt32 GL_FLOAT_VEC2_ARB = 0x8B50;
        public static UInt32 GL_FLOAT_VEC3_ARB = 0x8B51;
        public static UInt32 GL_FLOAT_VEC4_ARB = 0x8B52;
        public static UInt32 GL_INT_VEC2_ARB = 0x8B53;
        public static UInt32 GL_INT_VEC3_ARB = 0x8B54;
        public static UInt32 GL_INT_VEC4_ARB = 0x8B55;
        public static UInt32 GL_BOOL_ARB = 0x8B56;
        public static UInt32 GL_BOOL_VEC2_ARB = 0x8B57;
        public static UInt32 GL_BOOL_VEC3_ARB = 0x8B58;
        public static UInt32 GL_BOOL_VEC4_ARB = 0x8B59;
        public static UInt32 GL_FLOAT_MAT2_ARB = 0x8B5A;
        public static UInt32 GL_FLOAT_MAT3_ARB = 0x8B5B;
        public static UInt32 GL_FLOAT_MAT4_ARB = 0x8B5C;
        public static UInt32 GL_SAMPLER_1D_ARB = 0x8B5D;
        public static UInt32 GL_SAMPLER_2D_ARB = 0x8B5E;
        public static UInt32 GL_SAMPLER_3D_ARB = 0x8B5F;
        public static UInt32 GL_SAMPLER_CUBE_ARB = 0x8B60;
        public static UInt32 GL_SAMPLER_1D_SHADOW_ARB = 0x8B61;
        public static UInt32 GL_SAMPLER_2D_SHADOW_ARB = 0x8B62;
        public static UInt32 GL_SAMPLER_2D_RECT_ARB = 0x8B63;
        public static UInt32 GL_SAMPLER_2D_RECT_SHADOW_ARB = 0x8B64;
        public static UInt32 GL_OBJECT_DELETE_STATUS_ARB = 0x8B80;
        public static UInt32 GL_OBJECT_COMPILE_STATUS_ARB = 0x8B81;
        public static UInt32 GL_OBJECT_LINK_STATUS_ARB = 0x8B82;
        public static UInt32 GL_OBJECT_VALIDATE_STATUS_ARB = 0x8B83;
        public static UInt32 GL_OBJECT_INFO_LOG_LENGTH_ARB = 0x8B84;
        public static UInt32 GL_OBJECT_ATTACHED_OBJECTS_ARB = 0x8B85;
        public static UInt32 GL_OBJECT_ACTIVE_UNIFORMS_ARB = 0x8B86;
        public static UInt32 GL_OBJECT_ACTIVE_UNIFORM_MAX_LENGTH_ARB = 0x8B87;
        public static UInt32 GL_OBJECT_SHADER_SOURCE_LENGTH_ARB = 0x8B88;
        #endregion

        #region Commands
        internal delegate void glDeleteObjectARBFunc(GLhandleARB @obj);
        internal static glDeleteObjectARBFunc glDeleteObjectARBPtr;
        internal static void loadDeleteObjectARB()
        {
            try
            {
                glDeleteObjectARBPtr = (glDeleteObjectARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteObjectARB"), typeof(glDeleteObjectARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteObjectARB'.");
            }
        }
        public static void glDeleteObjectARB(GLhandleARB @obj) => glDeleteObjectARBPtr(@obj);

        internal delegate GLhandleARB glGetHandleARBFunc(GLenum @pname);
        internal static glGetHandleARBFunc glGetHandleARBPtr;
        internal static void loadGetHandleARB()
        {
            try
            {
                glGetHandleARBPtr = (glGetHandleARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetHandleARB"), typeof(glGetHandleARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetHandleARB'.");
            }
        }
        public static GLhandleARB glGetHandleARB(GLenum @pname) => glGetHandleARBPtr(@pname);

        internal delegate void glDetachObjectARBFunc(GLhandleARB @containerObj, GLhandleARB @attachedObj);
        internal static glDetachObjectARBFunc glDetachObjectARBPtr;
        internal static void loadDetachObjectARB()
        {
            try
            {
                glDetachObjectARBPtr = (glDetachObjectARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDetachObjectARB"), typeof(glDetachObjectARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDetachObjectARB'.");
            }
        }
        public static void glDetachObjectARB(GLhandleARB @containerObj, GLhandleARB @attachedObj) => glDetachObjectARBPtr(@containerObj, @attachedObj);

        internal delegate GLhandleARB glCreateShaderObjectARBFunc(GLenum @shaderType);
        internal static glCreateShaderObjectARBFunc glCreateShaderObjectARBPtr;
        internal static void loadCreateShaderObjectARB()
        {
            try
            {
                glCreateShaderObjectARBPtr = (glCreateShaderObjectARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateShaderObjectARB"), typeof(glCreateShaderObjectARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateShaderObjectARB'.");
            }
        }
        public static GLhandleARB glCreateShaderObjectARB(GLenum @shaderType) => glCreateShaderObjectARBPtr(@shaderType);

        internal delegate void glShaderSourceARBFunc(GLhandleARB @shaderObj, GLsizei @count, const GLcharARB ** @string, const GLint * @length);
        internal static glShaderSourceARBFunc glShaderSourceARBPtr;
        internal static void loadShaderSourceARB()
        {
            try
            {
                glShaderSourceARBPtr = (glShaderSourceARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShaderSourceARB"), typeof(glShaderSourceARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShaderSourceARB'.");
            }
        }
        public static void glShaderSourceARB(GLhandleARB @shaderObj, GLsizei @count, const GLcharARB ** @string, const GLint * @length) => glShaderSourceARBPtr(@shaderObj, @count, @string, @length);

        internal delegate void glCompileShaderARBFunc(GLhandleARB @shaderObj);
        internal static glCompileShaderARBFunc glCompileShaderARBPtr;
        internal static void loadCompileShaderARB()
        {
            try
            {
                glCompileShaderARBPtr = (glCompileShaderARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompileShaderARB"), typeof(glCompileShaderARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompileShaderARB'.");
            }
        }
        public static void glCompileShaderARB(GLhandleARB @shaderObj) => glCompileShaderARBPtr(@shaderObj);

        internal delegate GLhandleARB glCreateProgramObjectARBFunc();
        internal static glCreateProgramObjectARBFunc glCreateProgramObjectARBPtr;
        internal static void loadCreateProgramObjectARB()
        {
            try
            {
                glCreateProgramObjectARBPtr = (glCreateProgramObjectARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateProgramObjectARB"), typeof(glCreateProgramObjectARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateProgramObjectARB'.");
            }
        }
        public static GLhandleARB glCreateProgramObjectARB() => glCreateProgramObjectARBPtr();

        internal delegate void glAttachObjectARBFunc(GLhandleARB @containerObj, GLhandleARB @obj);
        internal static glAttachObjectARBFunc glAttachObjectARBPtr;
        internal static void loadAttachObjectARB()
        {
            try
            {
                glAttachObjectARBPtr = (glAttachObjectARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAttachObjectARB"), typeof(glAttachObjectARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAttachObjectARB'.");
            }
        }
        public static void glAttachObjectARB(GLhandleARB @containerObj, GLhandleARB @obj) => glAttachObjectARBPtr(@containerObj, @obj);

        internal delegate void glLinkProgramARBFunc(GLhandleARB @programObj);
        internal static glLinkProgramARBFunc glLinkProgramARBPtr;
        internal static void loadLinkProgramARB()
        {
            try
            {
                glLinkProgramARBPtr = (glLinkProgramARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLinkProgramARB"), typeof(glLinkProgramARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLinkProgramARB'.");
            }
        }
        public static void glLinkProgramARB(GLhandleARB @programObj) => glLinkProgramARBPtr(@programObj);

        internal delegate void glUseProgramObjectARBFunc(GLhandleARB @programObj);
        internal static glUseProgramObjectARBFunc glUseProgramObjectARBPtr;
        internal static void loadUseProgramObjectARB()
        {
            try
            {
                glUseProgramObjectARBPtr = (glUseProgramObjectARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUseProgramObjectARB"), typeof(glUseProgramObjectARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUseProgramObjectARB'.");
            }
        }
        public static void glUseProgramObjectARB(GLhandleARB @programObj) => glUseProgramObjectARBPtr(@programObj);

        internal delegate void glValidateProgramARBFunc(GLhandleARB @programObj);
        internal static glValidateProgramARBFunc glValidateProgramARBPtr;
        internal static void loadValidateProgramARB()
        {
            try
            {
                glValidateProgramARBPtr = (glValidateProgramARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glValidateProgramARB"), typeof(glValidateProgramARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glValidateProgramARB'.");
            }
        }
        public static void glValidateProgramARB(GLhandleARB @programObj) => glValidateProgramARBPtr(@programObj);

        internal delegate void glUniform1fARBFunc(GLint @location, GLfloat @v0);
        internal static glUniform1fARBFunc glUniform1fARBPtr;
        internal static void loadUniform1fARB()
        {
            try
            {
                glUniform1fARBPtr = (glUniform1fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1fARB"), typeof(glUniform1fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1fARB'.");
            }
        }
        public static void glUniform1fARB(GLint @location, GLfloat @v0) => glUniform1fARBPtr(@location, @v0);

        internal delegate void glUniform2fARBFunc(GLint @location, GLfloat @v0, GLfloat @v1);
        internal static glUniform2fARBFunc glUniform2fARBPtr;
        internal static void loadUniform2fARB()
        {
            try
            {
                glUniform2fARBPtr = (glUniform2fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2fARB"), typeof(glUniform2fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2fARB'.");
            }
        }
        public static void glUniform2fARB(GLint @location, GLfloat @v0, GLfloat @v1) => glUniform2fARBPtr(@location, @v0, @v1);

        internal delegate void glUniform3fARBFunc(GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2);
        internal static glUniform3fARBFunc glUniform3fARBPtr;
        internal static void loadUniform3fARB()
        {
            try
            {
                glUniform3fARBPtr = (glUniform3fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3fARB"), typeof(glUniform3fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3fARB'.");
            }
        }
        public static void glUniform3fARB(GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2) => glUniform3fARBPtr(@location, @v0, @v1, @v2);

        internal delegate void glUniform4fARBFunc(GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3);
        internal static glUniform4fARBFunc glUniform4fARBPtr;
        internal static void loadUniform4fARB()
        {
            try
            {
                glUniform4fARBPtr = (glUniform4fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4fARB"), typeof(glUniform4fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4fARB'.");
            }
        }
        public static void glUniform4fARB(GLint @location, GLfloat @v0, GLfloat @v1, GLfloat @v2, GLfloat @v3) => glUniform4fARBPtr(@location, @v0, @v1, @v2, @v3);

        internal delegate void glUniform1iARBFunc(GLint @location, GLint @v0);
        internal static glUniform1iARBFunc glUniform1iARBPtr;
        internal static void loadUniform1iARB()
        {
            try
            {
                glUniform1iARBPtr = (glUniform1iARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1iARB"), typeof(glUniform1iARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1iARB'.");
            }
        }
        public static void glUniform1iARB(GLint @location, GLint @v0) => glUniform1iARBPtr(@location, @v0);

        internal delegate void glUniform2iARBFunc(GLint @location, GLint @v0, GLint @v1);
        internal static glUniform2iARBFunc glUniform2iARBPtr;
        internal static void loadUniform2iARB()
        {
            try
            {
                glUniform2iARBPtr = (glUniform2iARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2iARB"), typeof(glUniform2iARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2iARB'.");
            }
        }
        public static void glUniform2iARB(GLint @location, GLint @v0, GLint @v1) => glUniform2iARBPtr(@location, @v0, @v1);

        internal delegate void glUniform3iARBFunc(GLint @location, GLint @v0, GLint @v1, GLint @v2);
        internal static glUniform3iARBFunc glUniform3iARBPtr;
        internal static void loadUniform3iARB()
        {
            try
            {
                glUniform3iARBPtr = (glUniform3iARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3iARB"), typeof(glUniform3iARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3iARB'.");
            }
        }
        public static void glUniform3iARB(GLint @location, GLint @v0, GLint @v1, GLint @v2) => glUniform3iARBPtr(@location, @v0, @v1, @v2);

        internal delegate void glUniform4iARBFunc(GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3);
        internal static glUniform4iARBFunc glUniform4iARBPtr;
        internal static void loadUniform4iARB()
        {
            try
            {
                glUniform4iARBPtr = (glUniform4iARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4iARB"), typeof(glUniform4iARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4iARB'.");
            }
        }
        public static void glUniform4iARB(GLint @location, GLint @v0, GLint @v1, GLint @v2, GLint @v3) => glUniform4iARBPtr(@location, @v0, @v1, @v2, @v3);

        internal delegate void glUniform1fvARBFunc(GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glUniform1fvARBFunc glUniform1fvARBPtr;
        internal static void loadUniform1fvARB()
        {
            try
            {
                glUniform1fvARBPtr = (glUniform1fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1fvARB"), typeof(glUniform1fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1fvARB'.");
            }
        }
        public static void glUniform1fvARB(GLint @location, GLsizei @count, const GLfloat * @value) => glUniform1fvARBPtr(@location, @count, @value);

        internal delegate void glUniform2fvARBFunc(GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glUniform2fvARBFunc glUniform2fvARBPtr;
        internal static void loadUniform2fvARB()
        {
            try
            {
                glUniform2fvARBPtr = (glUniform2fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2fvARB"), typeof(glUniform2fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2fvARB'.");
            }
        }
        public static void glUniform2fvARB(GLint @location, GLsizei @count, const GLfloat * @value) => glUniform2fvARBPtr(@location, @count, @value);

        internal delegate void glUniform3fvARBFunc(GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glUniform3fvARBFunc glUniform3fvARBPtr;
        internal static void loadUniform3fvARB()
        {
            try
            {
                glUniform3fvARBPtr = (glUniform3fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3fvARB"), typeof(glUniform3fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3fvARB'.");
            }
        }
        public static void glUniform3fvARB(GLint @location, GLsizei @count, const GLfloat * @value) => glUniform3fvARBPtr(@location, @count, @value);

        internal delegate void glUniform4fvARBFunc(GLint @location, GLsizei @count, const GLfloat * @value);
        internal static glUniform4fvARBFunc glUniform4fvARBPtr;
        internal static void loadUniform4fvARB()
        {
            try
            {
                glUniform4fvARBPtr = (glUniform4fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4fvARB"), typeof(glUniform4fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4fvARB'.");
            }
        }
        public static void glUniform4fvARB(GLint @location, GLsizei @count, const GLfloat * @value) => glUniform4fvARBPtr(@location, @count, @value);

        internal delegate void glUniform1ivARBFunc(GLint @location, GLsizei @count, const GLint * @value);
        internal static glUniform1ivARBFunc glUniform1ivARBPtr;
        internal static void loadUniform1ivARB()
        {
            try
            {
                glUniform1ivARBPtr = (glUniform1ivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform1ivARB"), typeof(glUniform1ivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform1ivARB'.");
            }
        }
        public static void glUniform1ivARB(GLint @location, GLsizei @count, const GLint * @value) => glUniform1ivARBPtr(@location, @count, @value);

        internal delegate void glUniform2ivARBFunc(GLint @location, GLsizei @count, const GLint * @value);
        internal static glUniform2ivARBFunc glUniform2ivARBPtr;
        internal static void loadUniform2ivARB()
        {
            try
            {
                glUniform2ivARBPtr = (glUniform2ivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform2ivARB"), typeof(glUniform2ivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform2ivARB'.");
            }
        }
        public static void glUniform2ivARB(GLint @location, GLsizei @count, const GLint * @value) => glUniform2ivARBPtr(@location, @count, @value);

        internal delegate void glUniform3ivARBFunc(GLint @location, GLsizei @count, const GLint * @value);
        internal static glUniform3ivARBFunc glUniform3ivARBPtr;
        internal static void loadUniform3ivARB()
        {
            try
            {
                glUniform3ivARBPtr = (glUniform3ivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform3ivARB"), typeof(glUniform3ivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform3ivARB'.");
            }
        }
        public static void glUniform3ivARB(GLint @location, GLsizei @count, const GLint * @value) => glUniform3ivARBPtr(@location, @count, @value);

        internal delegate void glUniform4ivARBFunc(GLint @location, GLsizei @count, const GLint * @value);
        internal static glUniform4ivARBFunc glUniform4ivARBPtr;
        internal static void loadUniform4ivARB()
        {
            try
            {
                glUniform4ivARBPtr = (glUniform4ivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniform4ivARB"), typeof(glUniform4ivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniform4ivARB'.");
            }
        }
        public static void glUniform4ivARB(GLint @location, GLsizei @count, const GLint * @value) => glUniform4ivARBPtr(@location, @count, @value);

        internal delegate void glUniformMatrix2fvARBFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix2fvARBFunc glUniformMatrix2fvARBPtr;
        internal static void loadUniformMatrix2fvARB()
        {
            try
            {
                glUniformMatrix2fvARBPtr = (glUniformMatrix2fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix2fvARB"), typeof(glUniformMatrix2fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix2fvARB'.");
            }
        }
        public static void glUniformMatrix2fvARB(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix2fvARBPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix3fvARBFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix3fvARBFunc glUniformMatrix3fvARBPtr;
        internal static void loadUniformMatrix3fvARB()
        {
            try
            {
                glUniformMatrix3fvARBPtr = (glUniformMatrix3fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix3fvARB"), typeof(glUniformMatrix3fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix3fvARB'.");
            }
        }
        public static void glUniformMatrix3fvARB(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix3fvARBPtr(@location, @count, @transpose, @value);

        internal delegate void glUniformMatrix4fvARBFunc(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value);
        internal static glUniformMatrix4fvARBFunc glUniformMatrix4fvARBPtr;
        internal static void loadUniformMatrix4fvARB()
        {
            try
            {
                glUniformMatrix4fvARBPtr = (glUniformMatrix4fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformMatrix4fvARB"), typeof(glUniformMatrix4fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformMatrix4fvARB'.");
            }
        }
        public static void glUniformMatrix4fvARB(GLint @location, GLsizei @count, GLboolean @transpose, const GLfloat * @value) => glUniformMatrix4fvARBPtr(@location, @count, @transpose, @value);

        internal delegate void glGetObjectParameterfvARBFunc(GLhandleARB @obj, GLenum @pname, GLfloat * @params);
        internal static glGetObjectParameterfvARBFunc glGetObjectParameterfvARBPtr;
        internal static void loadGetObjectParameterfvARB()
        {
            try
            {
                glGetObjectParameterfvARBPtr = (glGetObjectParameterfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectParameterfvARB"), typeof(glGetObjectParameterfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectParameterfvARB'.");
            }
        }
        public static void glGetObjectParameterfvARB(GLhandleARB @obj, GLenum @pname, GLfloat * @params) => glGetObjectParameterfvARBPtr(@obj, @pname, @params);

        internal delegate void glGetObjectParameterivARBFunc(GLhandleARB @obj, GLenum @pname, GLint * @params);
        internal static glGetObjectParameterivARBFunc glGetObjectParameterivARBPtr;
        internal static void loadGetObjectParameterivARB()
        {
            try
            {
                glGetObjectParameterivARBPtr = (glGetObjectParameterivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetObjectParameterivARB"), typeof(glGetObjectParameterivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetObjectParameterivARB'.");
            }
        }
        public static void glGetObjectParameterivARB(GLhandleARB @obj, GLenum @pname, GLint * @params) => glGetObjectParameterivARBPtr(@obj, @pname, @params);

        internal delegate void glGetInfoLogARBFunc(GLhandleARB @obj, GLsizei @maxLength, GLsizei * @length, GLcharARB * @infoLog);
        internal static glGetInfoLogARBFunc glGetInfoLogARBPtr;
        internal static void loadGetInfoLogARB()
        {
            try
            {
                glGetInfoLogARBPtr = (glGetInfoLogARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInfoLogARB"), typeof(glGetInfoLogARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInfoLogARB'.");
            }
        }
        public static void glGetInfoLogARB(GLhandleARB @obj, GLsizei @maxLength, GLsizei * @length, GLcharARB * @infoLog) => glGetInfoLogARBPtr(@obj, @maxLength, @length, @infoLog);

        internal delegate void glGetAttachedObjectsARBFunc(GLhandleARB @containerObj, GLsizei @maxCount, GLsizei * @count, GLhandleARB * @obj);
        internal static glGetAttachedObjectsARBFunc glGetAttachedObjectsARBPtr;
        internal static void loadGetAttachedObjectsARB()
        {
            try
            {
                glGetAttachedObjectsARBPtr = (glGetAttachedObjectsARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetAttachedObjectsARB"), typeof(glGetAttachedObjectsARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetAttachedObjectsARB'.");
            }
        }
        public static void glGetAttachedObjectsARB(GLhandleARB @containerObj, GLsizei @maxCount, GLsizei * @count, GLhandleARB * @obj) => glGetAttachedObjectsARBPtr(@containerObj, @maxCount, @count, @obj);

        internal delegate GLint glGetUniformLocationARBFunc(GLhandleARB @programObj, const GLcharARB * @name);
        internal static glGetUniformLocationARBFunc glGetUniformLocationARBPtr;
        internal static void loadGetUniformLocationARB()
        {
            try
            {
                glGetUniformLocationARBPtr = (glGetUniformLocationARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformLocationARB"), typeof(glGetUniformLocationARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformLocationARB'.");
            }
        }
        public static GLint glGetUniformLocationARB(GLhandleARB @programObj, const GLcharARB * @name) => glGetUniformLocationARBPtr(@programObj, @name);

        internal delegate void glGetActiveUniformARBFunc(GLhandleARB @programObj, GLuint @index, GLsizei @maxLength, GLsizei * @length, GLint * @size, GLenum * @type, GLcharARB * @name);
        internal static glGetActiveUniformARBFunc glGetActiveUniformARBPtr;
        internal static void loadGetActiveUniformARB()
        {
            try
            {
                glGetActiveUniformARBPtr = (glGetActiveUniformARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveUniformARB"), typeof(glGetActiveUniformARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveUniformARB'.");
            }
        }
        public static void glGetActiveUniformARB(GLhandleARB @programObj, GLuint @index, GLsizei @maxLength, GLsizei * @length, GLint * @size, GLenum * @type, GLcharARB * @name) => glGetActiveUniformARBPtr(@programObj, @index, @maxLength, @length, @size, @type, @name);

        internal delegate void glGetUniformfvARBFunc(GLhandleARB @programObj, GLint @location, GLfloat * @params);
        internal static glGetUniformfvARBFunc glGetUniformfvARBPtr;
        internal static void loadGetUniformfvARB()
        {
            try
            {
                glGetUniformfvARBPtr = (glGetUniformfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformfvARB"), typeof(glGetUniformfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformfvARB'.");
            }
        }
        public static void glGetUniformfvARB(GLhandleARB @programObj, GLint @location, GLfloat * @params) => glGetUniformfvARBPtr(@programObj, @location, @params);

        internal delegate void glGetUniformivARBFunc(GLhandleARB @programObj, GLint @location, GLint * @params);
        internal static glGetUniformivARBFunc glGetUniformivARBPtr;
        internal static void loadGetUniformivARB()
        {
            try
            {
                glGetUniformivARBPtr = (glGetUniformivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformivARB"), typeof(glGetUniformivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformivARB'.");
            }
        }
        public static void glGetUniformivARB(GLhandleARB @programObj, GLint @location, GLint * @params) => glGetUniformivARBPtr(@programObj, @location, @params);

        internal delegate void glGetShaderSourceARBFunc(GLhandleARB @obj, GLsizei @maxLength, GLsizei * @length, GLcharARB * @source);
        internal static glGetShaderSourceARBFunc glGetShaderSourceARBPtr;
        internal static void loadGetShaderSourceARB()
        {
            try
            {
                glGetShaderSourceARBPtr = (glGetShaderSourceARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetShaderSourceARB"), typeof(glGetShaderSourceARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetShaderSourceARB'.");
            }
        }
        public static void glGetShaderSourceARB(GLhandleARB @obj, GLsizei @maxLength, GLsizei * @length, GLcharARB * @source) => glGetShaderSourceARBPtr(@obj, @maxLength, @length, @source);
        #endregion
    }
}
