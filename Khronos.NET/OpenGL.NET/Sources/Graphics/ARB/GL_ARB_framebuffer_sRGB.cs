using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_framebuffer_sRGB
    {
        #region Interop
        static GL_ARB_framebuffer_sRGB()
        {
            Console.WriteLine("Initalising GL_ARB_framebuffer_sRGB interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAMEBUFFER_SRGB = 0x8DB9;
        #endregion

        #region Commands
        #endregion
    }
}
