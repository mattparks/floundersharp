using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_shadow_ambient
    {
        #region Interop
        static GL_ARB_shadow_ambient()
        {
            Console.WriteLine("Initalising GL_ARB_shadow_ambient interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_COMPARE_FAIL_VALUE_ARB = 0x80BF;
        #endregion

        #region Commands
        #endregion
    }
}
