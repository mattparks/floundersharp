using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_multi_draw_indirect
    {
        #region Interop
        static GL_ARB_multi_draw_indirect()
        {
            Console.WriteLine("Initalising GL_ARB_multi_draw_indirect interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMultiDrawArraysIndirect();
            loadMultiDrawElementsIndirect();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glMultiDrawArraysIndirectFunc(GLenum @mode, const void * @indirect, GLsizei @drawcount, GLsizei @stride);
        internal static glMultiDrawArraysIndirectFunc glMultiDrawArraysIndirectPtr;
        internal static void loadMultiDrawArraysIndirect()
        {
            try
            {
                glMultiDrawArraysIndirectPtr = (glMultiDrawArraysIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawArraysIndirect"), typeof(glMultiDrawArraysIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawArraysIndirect'.");
            }
        }
        public static void glMultiDrawArraysIndirect(GLenum @mode, const void * @indirect, GLsizei @drawcount, GLsizei @stride) => glMultiDrawArraysIndirectPtr(@mode, @indirect, @drawcount, @stride);

        internal delegate void glMultiDrawElementsIndirectFunc(GLenum @mode, GLenum @type, const void * @indirect, GLsizei @drawcount, GLsizei @stride);
        internal static glMultiDrawElementsIndirectFunc glMultiDrawElementsIndirectPtr;
        internal static void loadMultiDrawElementsIndirect()
        {
            try
            {
                glMultiDrawElementsIndirectPtr = (glMultiDrawElementsIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementsIndirect"), typeof(glMultiDrawElementsIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementsIndirect'.");
            }
        }
        public static void glMultiDrawElementsIndirect(GLenum @mode, GLenum @type, const void * @indirect, GLsizei @drawcount, GLsizei @stride) => glMultiDrawElementsIndirectPtr(@mode, @type, @indirect, @drawcount, @stride);
        #endregion
    }
}
