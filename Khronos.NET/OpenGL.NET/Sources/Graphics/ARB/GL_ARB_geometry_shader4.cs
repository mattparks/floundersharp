using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_geometry_shader4
    {
        #region Interop
        static GL_ARB_geometry_shader4()
        {
            Console.WriteLine("Initalising GL_ARB_geometry_shader4 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadProgramParameteriARB();
            loadFramebufferTextureARB();
            loadFramebufferTextureLayerARB();
            loadFramebufferTextureFaceARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_LINES_ADJACENCY_ARB = 0x000A;
        public static UInt32 GL_LINE_STRIP_ADJACENCY_ARB = 0x000B;
        public static UInt32 GL_TRIANGLES_ADJACENCY_ARB = 0x000C;
        public static UInt32 GL_TRIANGLE_STRIP_ADJACENCY_ARB = 0x000D;
        public static UInt32 GL_PROGRAM_POINT_SIZE_ARB = 0x8642;
        public static UInt32 GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS_ARB = 0x8C29;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_LAYERED_ARB = 0x8DA7;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS_ARB = 0x8DA8;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_LAYER_COUNT_ARB = 0x8DA9;
        public static UInt32 GL_GEOMETRY_SHADER_ARB = 0x8DD9;
        public static UInt32 GL_GEOMETRY_VERTICES_OUT_ARB = 0x8DDA;
        public static UInt32 GL_GEOMETRY_INPUT_TYPE_ARB = 0x8DDB;
        public static UInt32 GL_GEOMETRY_OUTPUT_TYPE_ARB = 0x8DDC;
        public static UInt32 GL_MAX_GEOMETRY_VARYING_COMPONENTS_ARB = 0x8DDD;
        public static UInt32 GL_MAX_VERTEX_VARYING_COMPONENTS_ARB = 0x8DDE;
        public static UInt32 GL_MAX_GEOMETRY_UNIFORM_COMPONENTS_ARB = 0x8DDF;
        public static UInt32 GL_MAX_GEOMETRY_OUTPUT_VERTICES_ARB = 0x8DE0;
        public static UInt32 GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS_ARB = 0x8DE1;
        public static UInt32 GL_MAX_VARYING_COMPONENTS = 0x8B4B;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER = 0x8CD4;
        #endregion

        #region Commands
        internal delegate void glProgramParameteriARBFunc(GLuint @program, GLenum @pname, GLint @value);
        internal static glProgramParameteriARBFunc glProgramParameteriARBPtr;
        internal static void loadProgramParameteriARB()
        {
            try
            {
                glProgramParameteriARBPtr = (glProgramParameteriARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramParameteriARB"), typeof(glProgramParameteriARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramParameteriARB'.");
            }
        }
        public static void glProgramParameteriARB(GLuint @program, GLenum @pname, GLint @value) => glProgramParameteriARBPtr(@program, @pname, @value);

        internal delegate void glFramebufferTextureARBFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level);
        internal static glFramebufferTextureARBFunc glFramebufferTextureARBPtr;
        internal static void loadFramebufferTextureARB()
        {
            try
            {
                glFramebufferTextureARBPtr = (glFramebufferTextureARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureARB"), typeof(glFramebufferTextureARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureARB'.");
            }
        }
        public static void glFramebufferTextureARB(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level) => glFramebufferTextureARBPtr(@target, @attachment, @texture, @level);

        internal delegate void glFramebufferTextureLayerARBFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer);
        internal static glFramebufferTextureLayerARBFunc glFramebufferTextureLayerARBPtr;
        internal static void loadFramebufferTextureLayerARB()
        {
            try
            {
                glFramebufferTextureLayerARBPtr = (glFramebufferTextureLayerARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureLayerARB"), typeof(glFramebufferTextureLayerARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureLayerARB'.");
            }
        }
        public static void glFramebufferTextureLayerARB(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer) => glFramebufferTextureLayerARBPtr(@target, @attachment, @texture, @level, @layer);

        internal delegate void glFramebufferTextureFaceARBFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLenum @face);
        internal static glFramebufferTextureFaceARBFunc glFramebufferTextureFaceARBPtr;
        internal static void loadFramebufferTextureFaceARB()
        {
            try
            {
                glFramebufferTextureFaceARBPtr = (glFramebufferTextureFaceARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureFaceARB"), typeof(glFramebufferTextureFaceARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureFaceARB'.");
            }
        }
        public static void glFramebufferTextureFaceARB(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLenum @face) => glFramebufferTextureFaceARBPtr(@target, @attachment, @texture, @level, @face);
        #endregion
    }
}
