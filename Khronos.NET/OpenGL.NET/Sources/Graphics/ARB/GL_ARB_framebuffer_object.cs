using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_framebuffer_object
    {
        #region Interop
        static GL_ARB_framebuffer_object()
        {
            Console.WriteLine("Initalising GL_ARB_framebuffer_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadIsRenderbuffer();
            loadBindRenderbuffer();
            loadDeleteRenderbuffers();
            loadGenRenderbuffers();
            loadRenderbufferStorage();
            loadGetRenderbufferParameteriv();
            loadIsFramebuffer();
            loadBindFramebuffer();
            loadDeleteFramebuffers();
            loadGenFramebuffers();
            loadCheckFramebufferStatus();
            loadFramebufferTexture1D();
            loadFramebufferTexture2D();
            loadFramebufferTexture3D();
            loadFramebufferRenderbuffer();
            loadGetFramebufferAttachmentParameteriv();
            loadGenerateMipmap();
            loadBlitFramebuffer();
            loadRenderbufferStorageMultisample();
            loadFramebufferTextureLayer();
        }
        #endregion

        #region Enums
        public static UInt32 GL_INVALID_FRAMEBUFFER_OPERATION = 0x0506;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING = 0x8210;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE = 0x8211;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_RED_SIZE = 0x8212;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_GREEN_SIZE = 0x8213;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_BLUE_SIZE = 0x8214;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE = 0x8215;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE = 0x8216;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE = 0x8217;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT = 0x8218;
        public static UInt32 GL_FRAMEBUFFER_UNDEFINED = 0x8219;
        public static UInt32 GL_DEPTH_STENCIL_ATTACHMENT = 0x821A;
        public static UInt32 GL_MAX_RENDERBUFFER_SIZE = 0x84E8;
        public static UInt32 GL_DEPTH_STENCIL = 0x84F9;
        public static UInt32 GL_UNSIGNED_INT_24_8 = 0x84FA;
        public static UInt32 GL_DEPTH24_STENCIL8 = 0x88F0;
        public static UInt32 GL_TEXTURE_STENCIL_SIZE = 0x88F1;
        public static UInt32 GL_UNSIGNED_NORMALIZED = 0x8C17;
        public static UInt32 GL_FRAMEBUFFER_BINDING = 0x8CA6;
        public static UInt32 GL_DRAW_FRAMEBUFFER_BINDING = 0x8CA6;
        public static UInt32 GL_RENDERBUFFER_BINDING = 0x8CA7;
        public static UInt32 GL_READ_FRAMEBUFFER = 0x8CA8;
        public static UInt32 GL_DRAW_FRAMEBUFFER = 0x8CA9;
        public static UInt32 GL_READ_FRAMEBUFFER_BINDING = 0x8CAA;
        public static UInt32 GL_RENDERBUFFER_SAMPLES = 0x8CAB;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE = 0x8CD0;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME = 0x8CD1;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL = 0x8CD2;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE = 0x8CD3;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER = 0x8CD4;
        public static UInt32 GL_FRAMEBUFFER_COMPLETE = 0x8CD5;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT = 0x8CD6;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT = 0x8CD7;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER = 0x8CDB;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER = 0x8CDC;
        public static UInt32 GL_FRAMEBUFFER_UNSUPPORTED = 0x8CDD;
        public static UInt32 GL_MAX_COLOR_ATTACHMENTS = 0x8CDF;
        public static UInt32 GL_COLOR_ATTACHMENT0 = 0x8CE0;
        public static UInt32 GL_COLOR_ATTACHMENT1 = 0x8CE1;
        public static UInt32 GL_COLOR_ATTACHMENT2 = 0x8CE2;
        public static UInt32 GL_COLOR_ATTACHMENT3 = 0x8CE3;
        public static UInt32 GL_COLOR_ATTACHMENT4 = 0x8CE4;
        public static UInt32 GL_COLOR_ATTACHMENT5 = 0x8CE5;
        public static UInt32 GL_COLOR_ATTACHMENT6 = 0x8CE6;
        public static UInt32 GL_COLOR_ATTACHMENT7 = 0x8CE7;
        public static UInt32 GL_COLOR_ATTACHMENT8 = 0x8CE8;
        public static UInt32 GL_COLOR_ATTACHMENT9 = 0x8CE9;
        public static UInt32 GL_COLOR_ATTACHMENT10 = 0x8CEA;
        public static UInt32 GL_COLOR_ATTACHMENT11 = 0x8CEB;
        public static UInt32 GL_COLOR_ATTACHMENT12 = 0x8CEC;
        public static UInt32 GL_COLOR_ATTACHMENT13 = 0x8CED;
        public static UInt32 GL_COLOR_ATTACHMENT14 = 0x8CEE;
        public static UInt32 GL_COLOR_ATTACHMENT15 = 0x8CEF;
        public static UInt32 GL_DEPTH_ATTACHMENT = 0x8D00;
        public static UInt32 GL_STENCIL_ATTACHMENT = 0x8D20;
        public static UInt32 GL_FRAMEBUFFER = 0x8D40;
        public static UInt32 GL_RENDERBUFFER = 0x8D41;
        public static UInt32 GL_RENDERBUFFER_WIDTH = 0x8D42;
        public static UInt32 GL_RENDERBUFFER_HEIGHT = 0x8D43;
        public static UInt32 GL_RENDERBUFFER_INTERNAL_FORMAT = 0x8D44;
        public static UInt32 GL_STENCIL_INDEX1 = 0x8D46;
        public static UInt32 GL_STENCIL_INDEX4 = 0x8D47;
        public static UInt32 GL_STENCIL_INDEX8 = 0x8D48;
        public static UInt32 GL_STENCIL_INDEX16 = 0x8D49;
        public static UInt32 GL_RENDERBUFFER_RED_SIZE = 0x8D50;
        public static UInt32 GL_RENDERBUFFER_GREEN_SIZE = 0x8D51;
        public static UInt32 GL_RENDERBUFFER_BLUE_SIZE = 0x8D52;
        public static UInt32 GL_RENDERBUFFER_ALPHA_SIZE = 0x8D53;
        public static UInt32 GL_RENDERBUFFER_DEPTH_SIZE = 0x8D54;
        public static UInt32 GL_RENDERBUFFER_STENCIL_SIZE = 0x8D55;
        public static UInt32 GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE = 0x8D56;
        public static UInt32 GL_MAX_SAMPLES = 0x8D57;
        public static UInt32 GL_INDEX = 0x8222;
        #endregion

        #region Commands
        internal delegate GLboolean glIsRenderbufferFunc(GLuint @renderbuffer);
        internal static glIsRenderbufferFunc glIsRenderbufferPtr;
        internal static void loadIsRenderbuffer()
        {
            try
            {
                glIsRenderbufferPtr = (glIsRenderbufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsRenderbuffer"), typeof(glIsRenderbufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsRenderbuffer'.");
            }
        }
        public static GLboolean glIsRenderbuffer(GLuint @renderbuffer) => glIsRenderbufferPtr(@renderbuffer);

        internal delegate void glBindRenderbufferFunc(GLenum @target, GLuint @renderbuffer);
        internal static glBindRenderbufferFunc glBindRenderbufferPtr;
        internal static void loadBindRenderbuffer()
        {
            try
            {
                glBindRenderbufferPtr = (glBindRenderbufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindRenderbuffer"), typeof(glBindRenderbufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindRenderbuffer'.");
            }
        }
        public static void glBindRenderbuffer(GLenum @target, GLuint @renderbuffer) => glBindRenderbufferPtr(@target, @renderbuffer);

        internal delegate void glDeleteRenderbuffersFunc(GLsizei @n, const GLuint * @renderbuffers);
        internal static glDeleteRenderbuffersFunc glDeleteRenderbuffersPtr;
        internal static void loadDeleteRenderbuffers()
        {
            try
            {
                glDeleteRenderbuffersPtr = (glDeleteRenderbuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteRenderbuffers"), typeof(glDeleteRenderbuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteRenderbuffers'.");
            }
        }
        public static void glDeleteRenderbuffers(GLsizei @n, const GLuint * @renderbuffers) => glDeleteRenderbuffersPtr(@n, @renderbuffers);

        internal delegate void glGenRenderbuffersFunc(GLsizei @n, GLuint * @renderbuffers);
        internal static glGenRenderbuffersFunc glGenRenderbuffersPtr;
        internal static void loadGenRenderbuffers()
        {
            try
            {
                glGenRenderbuffersPtr = (glGenRenderbuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenRenderbuffers"), typeof(glGenRenderbuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenRenderbuffers'.");
            }
        }
        public static void glGenRenderbuffers(GLsizei @n, GLuint * @renderbuffers) => glGenRenderbuffersPtr(@n, @renderbuffers);

        internal delegate void glRenderbufferStorageFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageFunc glRenderbufferStoragePtr;
        internal static void loadRenderbufferStorage()
        {
            try
            {
                glRenderbufferStoragePtr = (glRenderbufferStorageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorage"), typeof(glRenderbufferStorageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorage'.");
            }
        }
        public static void glRenderbufferStorage(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStoragePtr(@target, @internalformat, @width, @height);

        internal delegate void glGetRenderbufferParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetRenderbufferParameterivFunc glGetRenderbufferParameterivPtr;
        internal static void loadGetRenderbufferParameteriv()
        {
            try
            {
                glGetRenderbufferParameterivPtr = (glGetRenderbufferParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetRenderbufferParameteriv"), typeof(glGetRenderbufferParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetRenderbufferParameteriv'.");
            }
        }
        public static void glGetRenderbufferParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetRenderbufferParameterivPtr(@target, @pname, @params);

        internal delegate GLboolean glIsFramebufferFunc(GLuint @framebuffer);
        internal static glIsFramebufferFunc glIsFramebufferPtr;
        internal static void loadIsFramebuffer()
        {
            try
            {
                glIsFramebufferPtr = (glIsFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsFramebuffer"), typeof(glIsFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsFramebuffer'.");
            }
        }
        public static GLboolean glIsFramebuffer(GLuint @framebuffer) => glIsFramebufferPtr(@framebuffer);

        internal delegate void glBindFramebufferFunc(GLenum @target, GLuint @framebuffer);
        internal static glBindFramebufferFunc glBindFramebufferPtr;
        internal static void loadBindFramebuffer()
        {
            try
            {
                glBindFramebufferPtr = (glBindFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindFramebuffer"), typeof(glBindFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindFramebuffer'.");
            }
        }
        public static void glBindFramebuffer(GLenum @target, GLuint @framebuffer) => glBindFramebufferPtr(@target, @framebuffer);

        internal delegate void glDeleteFramebuffersFunc(GLsizei @n, const GLuint * @framebuffers);
        internal static glDeleteFramebuffersFunc glDeleteFramebuffersPtr;
        internal static void loadDeleteFramebuffers()
        {
            try
            {
                glDeleteFramebuffersPtr = (glDeleteFramebuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteFramebuffers"), typeof(glDeleteFramebuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteFramebuffers'.");
            }
        }
        public static void glDeleteFramebuffers(GLsizei @n, const GLuint * @framebuffers) => glDeleteFramebuffersPtr(@n, @framebuffers);

        internal delegate void glGenFramebuffersFunc(GLsizei @n, GLuint * @framebuffers);
        internal static glGenFramebuffersFunc glGenFramebuffersPtr;
        internal static void loadGenFramebuffers()
        {
            try
            {
                glGenFramebuffersPtr = (glGenFramebuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenFramebuffers"), typeof(glGenFramebuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenFramebuffers'.");
            }
        }
        public static void glGenFramebuffers(GLsizei @n, GLuint * @framebuffers) => glGenFramebuffersPtr(@n, @framebuffers);

        internal delegate GLenum glCheckFramebufferStatusFunc(GLenum @target);
        internal static glCheckFramebufferStatusFunc glCheckFramebufferStatusPtr;
        internal static void loadCheckFramebufferStatus()
        {
            try
            {
                glCheckFramebufferStatusPtr = (glCheckFramebufferStatusFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCheckFramebufferStatus"), typeof(glCheckFramebufferStatusFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCheckFramebufferStatus'.");
            }
        }
        public static GLenum glCheckFramebufferStatus(GLenum @target) => glCheckFramebufferStatusPtr(@target);

        internal delegate void glFramebufferTexture1DFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level);
        internal static glFramebufferTexture1DFunc glFramebufferTexture1DPtr;
        internal static void loadFramebufferTexture1D()
        {
            try
            {
                glFramebufferTexture1DPtr = (glFramebufferTexture1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture1D"), typeof(glFramebufferTexture1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture1D'.");
            }
        }
        public static void glFramebufferTexture1D(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level) => glFramebufferTexture1DPtr(@target, @attachment, @textarget, @texture, @level);

        internal delegate void glFramebufferTexture2DFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level);
        internal static glFramebufferTexture2DFunc glFramebufferTexture2DPtr;
        internal static void loadFramebufferTexture2D()
        {
            try
            {
                glFramebufferTexture2DPtr = (glFramebufferTexture2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture2D"), typeof(glFramebufferTexture2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture2D'.");
            }
        }
        public static void glFramebufferTexture2D(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level) => glFramebufferTexture2DPtr(@target, @attachment, @textarget, @texture, @level);

        internal delegate void glFramebufferTexture3DFunc(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLint @zoffset);
        internal static glFramebufferTexture3DFunc glFramebufferTexture3DPtr;
        internal static void loadFramebufferTexture3D()
        {
            try
            {
                glFramebufferTexture3DPtr = (glFramebufferTexture3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTexture3D"), typeof(glFramebufferTexture3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTexture3D'.");
            }
        }
        public static void glFramebufferTexture3D(GLenum @target, GLenum @attachment, GLenum @textarget, GLuint @texture, GLint @level, GLint @zoffset) => glFramebufferTexture3DPtr(@target, @attachment, @textarget, @texture, @level, @zoffset);

        internal delegate void glFramebufferRenderbufferFunc(GLenum @target, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer);
        internal static glFramebufferRenderbufferFunc glFramebufferRenderbufferPtr;
        internal static void loadFramebufferRenderbuffer()
        {
            try
            {
                glFramebufferRenderbufferPtr = (glFramebufferRenderbufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferRenderbuffer"), typeof(glFramebufferRenderbufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferRenderbuffer'.");
            }
        }
        public static void glFramebufferRenderbuffer(GLenum @target, GLenum @attachment, GLenum @renderbuffertarget, GLuint @renderbuffer) => glFramebufferRenderbufferPtr(@target, @attachment, @renderbuffertarget, @renderbuffer);

        internal delegate void glGetFramebufferAttachmentParameterivFunc(GLenum @target, GLenum @attachment, GLenum @pname, GLint * @params);
        internal static glGetFramebufferAttachmentParameterivFunc glGetFramebufferAttachmentParameterivPtr;
        internal static void loadGetFramebufferAttachmentParameteriv()
        {
            try
            {
                glGetFramebufferAttachmentParameterivPtr = (glGetFramebufferAttachmentParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFramebufferAttachmentParameteriv"), typeof(glGetFramebufferAttachmentParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFramebufferAttachmentParameteriv'.");
            }
        }
        public static void glGetFramebufferAttachmentParameteriv(GLenum @target, GLenum @attachment, GLenum @pname, GLint * @params) => glGetFramebufferAttachmentParameterivPtr(@target, @attachment, @pname, @params);

        internal delegate void glGenerateMipmapFunc(GLenum @target);
        internal static glGenerateMipmapFunc glGenerateMipmapPtr;
        internal static void loadGenerateMipmap()
        {
            try
            {
                glGenerateMipmapPtr = (glGenerateMipmapFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenerateMipmap"), typeof(glGenerateMipmapFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenerateMipmap'.");
            }
        }
        public static void glGenerateMipmap(GLenum @target) => glGenerateMipmapPtr(@target);

        internal delegate void glBlitFramebufferFunc(GLint @srcX0, GLint @srcY0, GLint @srcX1, GLint @srcY1, GLint @dstX0, GLint @dstY0, GLint @dstX1, GLint @dstY1, GLbitfield @mask, GLenum @filter);
        internal static glBlitFramebufferFunc glBlitFramebufferPtr;
        internal static void loadBlitFramebuffer()
        {
            try
            {
                glBlitFramebufferPtr = (glBlitFramebufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlitFramebuffer"), typeof(glBlitFramebufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlitFramebuffer'.");
            }
        }
        public static void glBlitFramebuffer(GLint @srcX0, GLint @srcY0, GLint @srcX1, GLint @srcY1, GLint @dstX0, GLint @dstY0, GLint @dstX1, GLint @dstY1, GLbitfield @mask, GLenum @filter) => glBlitFramebufferPtr(@srcX0, @srcY0, @srcX1, @srcY1, @dstX0, @dstY0, @dstX1, @dstY1, @mask, @filter);

        internal delegate void glRenderbufferStorageMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glRenderbufferStorageMultisampleFunc glRenderbufferStorageMultisamplePtr;
        internal static void loadRenderbufferStorageMultisample()
        {
            try
            {
                glRenderbufferStorageMultisamplePtr = (glRenderbufferStorageMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRenderbufferStorageMultisample"), typeof(glRenderbufferStorageMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRenderbufferStorageMultisample'.");
            }
        }
        public static void glRenderbufferStorageMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height) => glRenderbufferStorageMultisamplePtr(@target, @samples, @internalformat, @width, @height);

        internal delegate void glFramebufferTextureLayerFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer);
        internal static glFramebufferTextureLayerFunc glFramebufferTextureLayerPtr;
        internal static void loadFramebufferTextureLayer()
        {
            try
            {
                glFramebufferTextureLayerPtr = (glFramebufferTextureLayerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureLayer"), typeof(glFramebufferTextureLayerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureLayer'.");
            }
        }
        public static void glFramebufferTextureLayer(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @layer) => glFramebufferTextureLayerPtr(@target, @attachment, @texture, @level, @layer);
        #endregion
    }
}
