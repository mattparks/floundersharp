using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_draw_buffers_blend
    {
        #region Interop
        static GL_ARB_draw_buffers_blend()
        {
            Console.WriteLine("Initalising GL_ARB_draw_buffers_blend interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendEquationiARB();
            loadBlendEquationSeparateiARB();
            loadBlendFunciARB();
            loadBlendFuncSeparateiARB();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glBlendEquationiARBFunc(GLuint @buf, GLenum @mode);
        internal static glBlendEquationiARBFunc glBlendEquationiARBPtr;
        internal static void loadBlendEquationiARB()
        {
            try
            {
                glBlendEquationiARBPtr = (glBlendEquationiARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationiARB"), typeof(glBlendEquationiARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationiARB'.");
            }
        }
        public static void glBlendEquationiARB(GLuint @buf, GLenum @mode) => glBlendEquationiARBPtr(@buf, @mode);

        internal delegate void glBlendEquationSeparateiARBFunc(GLuint @buf, GLenum @modeRGB, GLenum @modeAlpha);
        internal static glBlendEquationSeparateiARBFunc glBlendEquationSeparateiARBPtr;
        internal static void loadBlendEquationSeparateiARB()
        {
            try
            {
                glBlendEquationSeparateiARBPtr = (glBlendEquationSeparateiARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquationSeparateiARB"), typeof(glBlendEquationSeparateiARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquationSeparateiARB'.");
            }
        }
        public static void glBlendEquationSeparateiARB(GLuint @buf, GLenum @modeRGB, GLenum @modeAlpha) => glBlendEquationSeparateiARBPtr(@buf, @modeRGB, @modeAlpha);

        internal delegate void glBlendFunciARBFunc(GLuint @buf, GLenum @src, GLenum @dst);
        internal static glBlendFunciARBFunc glBlendFunciARBPtr;
        internal static void loadBlendFunciARB()
        {
            try
            {
                glBlendFunciARBPtr = (glBlendFunciARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFunciARB"), typeof(glBlendFunciARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFunciARB'.");
            }
        }
        public static void glBlendFunciARB(GLuint @buf, GLenum @src, GLenum @dst) => glBlendFunciARBPtr(@buf, @src, @dst);

        internal delegate void glBlendFuncSeparateiARBFunc(GLuint @buf, GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha);
        internal static glBlendFuncSeparateiARBFunc glBlendFuncSeparateiARBPtr;
        internal static void loadBlendFuncSeparateiARB()
        {
            try
            {
                glBlendFuncSeparateiARBPtr = (glBlendFuncSeparateiARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFuncSeparateiARB"), typeof(glBlendFuncSeparateiARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFuncSeparateiARB'.");
            }
        }
        public static void glBlendFuncSeparateiARB(GLuint @buf, GLenum @srcRGB, GLenum @dstRGB, GLenum @srcAlpha, GLenum @dstAlpha) => glBlendFuncSeparateiARBPtr(@buf, @srcRGB, @dstRGB, @srcAlpha, @dstAlpha);
        #endregion
    }
}
