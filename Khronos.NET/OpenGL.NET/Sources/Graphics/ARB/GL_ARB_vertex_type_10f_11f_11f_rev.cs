using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_vertex_type_10f_11f_11f_rev
    {
        #region Interop
        static GL_ARB_vertex_type_10f_11f_11f_rev()
        {
            Console.WriteLine("Initalising GL_ARB_vertex_type_10f_11f_11f_rev interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_UNSIGNED_INT_10F_11F_11F_REV = 0x8C3B;
        #endregion

        #region Commands
        #endregion
    }
}
