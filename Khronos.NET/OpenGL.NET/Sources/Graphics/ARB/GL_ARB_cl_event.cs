using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_cl_event
    {
        #region Interop
        static GL_ARB_cl_event()
        {
            Console.WriteLine("Initalising GL_ARB_cl_event interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCreateSyncFromCLeventARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SYNC_CL_EVENT_ARB = 0x8240;
        public static UInt32 GL_SYNC_CL_EVENT_COMPLETE_ARB = 0x8241;
        #endregion

        #region Commands
        internal delegate GLsync glCreateSyncFromCLeventARBFunc(struct _cl_context * @context, struct _cl_event * @event, GLbitfield @flags);
        internal static glCreateSyncFromCLeventARBFunc glCreateSyncFromCLeventARBPtr;
        internal static void loadCreateSyncFromCLeventARB()
        {
            try
            {
                glCreateSyncFromCLeventARBPtr = (glCreateSyncFromCLeventARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCreateSyncFromCLeventARB"), typeof(glCreateSyncFromCLeventARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCreateSyncFromCLeventARB'.");
            }
        }
        public static GLsync glCreateSyncFromCLeventARB(struct _cl_context * @context, struct _cl_event * @event, GLbitfield @flags) => glCreateSyncFromCLeventARBPtr(@context, @event, @flags);
        #endregion
    }
}
