using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_debug_output
    {
        #region Interop
        static GL_ARB_debug_output()
        {
            Console.WriteLine("Initalising GL_ARB_debug_output interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDebugMessageControlARB();
            loadDebugMessageInsertARB();
            loadDebugMessageCallbackARB();
            loadGetDebugMessageLogARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB = 0x8242;
        public static UInt32 GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH_ARB = 0x8243;
        public static UInt32 GL_DEBUG_CALLBACK_FUNCTION_ARB = 0x8244;
        public static UInt32 GL_DEBUG_CALLBACK_USER_PARAM_ARB = 0x8245;
        public static UInt32 GL_DEBUG_SOURCE_API_ARB = 0x8246;
        public static UInt32 GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB = 0x8247;
        public static UInt32 GL_DEBUG_SOURCE_SHADER_COMPILER_ARB = 0x8248;
        public static UInt32 GL_DEBUG_SOURCE_THIRD_PARTY_ARB = 0x8249;
        public static UInt32 GL_DEBUG_SOURCE_APPLICATION_ARB = 0x824A;
        public static UInt32 GL_DEBUG_SOURCE_OTHER_ARB = 0x824B;
        public static UInt32 GL_DEBUG_TYPE_ERROR_ARB = 0x824C;
        public static UInt32 GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB = 0x824D;
        public static UInt32 GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB = 0x824E;
        public static UInt32 GL_DEBUG_TYPE_PORTABILITY_ARB = 0x824F;
        public static UInt32 GL_DEBUG_TYPE_PERFORMANCE_ARB = 0x8250;
        public static UInt32 GL_DEBUG_TYPE_OTHER_ARB = 0x8251;
        public static UInt32 GL_MAX_DEBUG_MESSAGE_LENGTH_ARB = 0x9143;
        public static UInt32 GL_MAX_DEBUG_LOGGED_MESSAGES_ARB = 0x9144;
        public static UInt32 GL_DEBUG_LOGGED_MESSAGES_ARB = 0x9145;
        public static UInt32 GL_DEBUG_SEVERITY_HIGH_ARB = 0x9146;
        public static UInt32 GL_DEBUG_SEVERITY_MEDIUM_ARB = 0x9147;
        public static UInt32 GL_DEBUG_SEVERITY_LOW_ARB = 0x9148;
        #endregion

        #region Commands
        internal delegate void glDebugMessageControlARBFunc(GLenum @source, GLenum @type, GLenum @severity, GLsizei @count, const GLuint * @ids, GLboolean @enabled);
        internal static glDebugMessageControlARBFunc glDebugMessageControlARBPtr;
        internal static void loadDebugMessageControlARB()
        {
            try
            {
                glDebugMessageControlARBPtr = (glDebugMessageControlARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageControlARB"), typeof(glDebugMessageControlARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageControlARB'.");
            }
        }
        public static void glDebugMessageControlARB(GLenum @source, GLenum @type, GLenum @severity, GLsizei @count, const GLuint * @ids, GLboolean @enabled) => glDebugMessageControlARBPtr(@source, @type, @severity, @count, @ids, @enabled);

        internal delegate void glDebugMessageInsertARBFunc(GLenum @source, GLenum @type, GLuint @id, GLenum @severity, GLsizei @length, const GLchar * @buf);
        internal static glDebugMessageInsertARBFunc glDebugMessageInsertARBPtr;
        internal static void loadDebugMessageInsertARB()
        {
            try
            {
                glDebugMessageInsertARBPtr = (glDebugMessageInsertARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageInsertARB"), typeof(glDebugMessageInsertARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageInsertARB'.");
            }
        }
        public static void glDebugMessageInsertARB(GLenum @source, GLenum @type, GLuint @id, GLenum @severity, GLsizei @length, const GLchar * @buf) => glDebugMessageInsertARBPtr(@source, @type, @id, @severity, @length, @buf);

        internal delegate void glDebugMessageCallbackARBFunc(GLDEBUGPROCARB @callback, const void * @userParam);
        internal static glDebugMessageCallbackARBFunc glDebugMessageCallbackARBPtr;
        internal static void loadDebugMessageCallbackARB()
        {
            try
            {
                glDebugMessageCallbackARBPtr = (glDebugMessageCallbackARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDebugMessageCallbackARB"), typeof(glDebugMessageCallbackARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDebugMessageCallbackARB'.");
            }
        }
        public static void glDebugMessageCallbackARB(GLDEBUGPROCARB @callback, const void * @userParam) => glDebugMessageCallbackARBPtr(@callback, @userParam);

        internal delegate GLuint glGetDebugMessageLogARBFunc(GLuint @count, GLsizei @bufSize, GLenum * @sources, GLenum * @types, GLuint * @ids, GLenum * @severities, GLsizei * @lengths, GLchar * @messageLog);
        internal static glGetDebugMessageLogARBFunc glGetDebugMessageLogARBPtr;
        internal static void loadGetDebugMessageLogARB()
        {
            try
            {
                glGetDebugMessageLogARBPtr = (glGetDebugMessageLogARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetDebugMessageLogARB"), typeof(glGetDebugMessageLogARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetDebugMessageLogARB'.");
            }
        }
        public static GLuint glGetDebugMessageLogARB(GLuint @count, GLsizei @bufSize, GLenum * @sources, GLenum * @types, GLuint * @ids, GLenum * @severities, GLsizei * @lengths, GLchar * @messageLog) => glGetDebugMessageLogARBPtr(@count, @bufSize, @sources, @types, @ids, @severities, @lengths, @messageLog);
        #endregion
    }
}
