using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_window_pos
    {
        #region Interop
        static GL_ARB_window_pos()
        {
            Console.WriteLine("Initalising GL_ARB_window_pos interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadWindowPos2dARB();
            loadWindowPos2dvARB();
            loadWindowPos2fARB();
            loadWindowPos2fvARB();
            loadWindowPos2iARB();
            loadWindowPos2ivARB();
            loadWindowPos2sARB();
            loadWindowPos2svARB();
            loadWindowPos3dARB();
            loadWindowPos3dvARB();
            loadWindowPos3fARB();
            loadWindowPos3fvARB();
            loadWindowPos3iARB();
            loadWindowPos3ivARB();
            loadWindowPos3sARB();
            loadWindowPos3svARB();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glWindowPos2dARBFunc(GLdouble @x, GLdouble @y);
        internal static glWindowPos2dARBFunc glWindowPos2dARBPtr;
        internal static void loadWindowPos2dARB()
        {
            try
            {
                glWindowPos2dARBPtr = (glWindowPos2dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2dARB"), typeof(glWindowPos2dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2dARB'.");
            }
        }
        public static void glWindowPos2dARB(GLdouble @x, GLdouble @y) => glWindowPos2dARBPtr(@x, @y);

        internal delegate void glWindowPos2dvARBFunc(const GLdouble * @v);
        internal static glWindowPos2dvARBFunc glWindowPos2dvARBPtr;
        internal static void loadWindowPos2dvARB()
        {
            try
            {
                glWindowPos2dvARBPtr = (glWindowPos2dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2dvARB"), typeof(glWindowPos2dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2dvARB'.");
            }
        }
        public static void glWindowPos2dvARB(const GLdouble * @v) => glWindowPos2dvARBPtr(@v);

        internal delegate void glWindowPos2fARBFunc(GLfloat @x, GLfloat @y);
        internal static glWindowPos2fARBFunc glWindowPos2fARBPtr;
        internal static void loadWindowPos2fARB()
        {
            try
            {
                glWindowPos2fARBPtr = (glWindowPos2fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2fARB"), typeof(glWindowPos2fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2fARB'.");
            }
        }
        public static void glWindowPos2fARB(GLfloat @x, GLfloat @y) => glWindowPos2fARBPtr(@x, @y);

        internal delegate void glWindowPos2fvARBFunc(const GLfloat * @v);
        internal static glWindowPos2fvARBFunc glWindowPos2fvARBPtr;
        internal static void loadWindowPos2fvARB()
        {
            try
            {
                glWindowPos2fvARBPtr = (glWindowPos2fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2fvARB"), typeof(glWindowPos2fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2fvARB'.");
            }
        }
        public static void glWindowPos2fvARB(const GLfloat * @v) => glWindowPos2fvARBPtr(@v);

        internal delegate void glWindowPos2iARBFunc(GLint @x, GLint @y);
        internal static glWindowPos2iARBFunc glWindowPos2iARBPtr;
        internal static void loadWindowPos2iARB()
        {
            try
            {
                glWindowPos2iARBPtr = (glWindowPos2iARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2iARB"), typeof(glWindowPos2iARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2iARB'.");
            }
        }
        public static void glWindowPos2iARB(GLint @x, GLint @y) => glWindowPos2iARBPtr(@x, @y);

        internal delegate void glWindowPos2ivARBFunc(const GLint * @v);
        internal static glWindowPos2ivARBFunc glWindowPos2ivARBPtr;
        internal static void loadWindowPos2ivARB()
        {
            try
            {
                glWindowPos2ivARBPtr = (glWindowPos2ivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2ivARB"), typeof(glWindowPos2ivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2ivARB'.");
            }
        }
        public static void glWindowPos2ivARB(const GLint * @v) => glWindowPos2ivARBPtr(@v);

        internal delegate void glWindowPos2sARBFunc(GLshort @x, GLshort @y);
        internal static glWindowPos2sARBFunc glWindowPos2sARBPtr;
        internal static void loadWindowPos2sARB()
        {
            try
            {
                glWindowPos2sARBPtr = (glWindowPos2sARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2sARB"), typeof(glWindowPos2sARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2sARB'.");
            }
        }
        public static void glWindowPos2sARB(GLshort @x, GLshort @y) => glWindowPos2sARBPtr(@x, @y);

        internal delegate void glWindowPos2svARBFunc(const GLshort * @v);
        internal static glWindowPos2svARBFunc glWindowPos2svARBPtr;
        internal static void loadWindowPos2svARB()
        {
            try
            {
                glWindowPos2svARBPtr = (glWindowPos2svARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2svARB"), typeof(glWindowPos2svARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2svARB'.");
            }
        }
        public static void glWindowPos2svARB(const GLshort * @v) => glWindowPos2svARBPtr(@v);

        internal delegate void glWindowPos3dARBFunc(GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glWindowPos3dARBFunc glWindowPos3dARBPtr;
        internal static void loadWindowPos3dARB()
        {
            try
            {
                glWindowPos3dARBPtr = (glWindowPos3dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3dARB"), typeof(glWindowPos3dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3dARB'.");
            }
        }
        public static void glWindowPos3dARB(GLdouble @x, GLdouble @y, GLdouble @z) => glWindowPos3dARBPtr(@x, @y, @z);

        internal delegate void glWindowPos3dvARBFunc(const GLdouble * @v);
        internal static glWindowPos3dvARBFunc glWindowPos3dvARBPtr;
        internal static void loadWindowPos3dvARB()
        {
            try
            {
                glWindowPos3dvARBPtr = (glWindowPos3dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3dvARB"), typeof(glWindowPos3dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3dvARB'.");
            }
        }
        public static void glWindowPos3dvARB(const GLdouble * @v) => glWindowPos3dvARBPtr(@v);

        internal delegate void glWindowPos3fARBFunc(GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glWindowPos3fARBFunc glWindowPos3fARBPtr;
        internal static void loadWindowPos3fARB()
        {
            try
            {
                glWindowPos3fARBPtr = (glWindowPos3fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3fARB"), typeof(glWindowPos3fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3fARB'.");
            }
        }
        public static void glWindowPos3fARB(GLfloat @x, GLfloat @y, GLfloat @z) => glWindowPos3fARBPtr(@x, @y, @z);

        internal delegate void glWindowPos3fvARBFunc(const GLfloat * @v);
        internal static glWindowPos3fvARBFunc glWindowPos3fvARBPtr;
        internal static void loadWindowPos3fvARB()
        {
            try
            {
                glWindowPos3fvARBPtr = (glWindowPos3fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3fvARB"), typeof(glWindowPos3fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3fvARB'.");
            }
        }
        public static void glWindowPos3fvARB(const GLfloat * @v) => glWindowPos3fvARBPtr(@v);

        internal delegate void glWindowPos3iARBFunc(GLint @x, GLint @y, GLint @z);
        internal static glWindowPos3iARBFunc glWindowPos3iARBPtr;
        internal static void loadWindowPos3iARB()
        {
            try
            {
                glWindowPos3iARBPtr = (glWindowPos3iARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3iARB"), typeof(glWindowPos3iARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3iARB'.");
            }
        }
        public static void glWindowPos3iARB(GLint @x, GLint @y, GLint @z) => glWindowPos3iARBPtr(@x, @y, @z);

        internal delegate void glWindowPos3ivARBFunc(const GLint * @v);
        internal static glWindowPos3ivARBFunc glWindowPos3ivARBPtr;
        internal static void loadWindowPos3ivARB()
        {
            try
            {
                glWindowPos3ivARBPtr = (glWindowPos3ivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3ivARB"), typeof(glWindowPos3ivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3ivARB'.");
            }
        }
        public static void glWindowPos3ivARB(const GLint * @v) => glWindowPos3ivARBPtr(@v);

        internal delegate void glWindowPos3sARBFunc(GLshort @x, GLshort @y, GLshort @z);
        internal static glWindowPos3sARBFunc glWindowPos3sARBPtr;
        internal static void loadWindowPos3sARB()
        {
            try
            {
                glWindowPos3sARBPtr = (glWindowPos3sARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3sARB"), typeof(glWindowPos3sARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3sARB'.");
            }
        }
        public static void glWindowPos3sARB(GLshort @x, GLshort @y, GLshort @z) => glWindowPos3sARBPtr(@x, @y, @z);

        internal delegate void glWindowPos3svARBFunc(const GLshort * @v);
        internal static glWindowPos3svARBFunc glWindowPos3svARBPtr;
        internal static void loadWindowPos3svARB()
        {
            try
            {
                glWindowPos3svARBPtr = (glWindowPos3svARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3svARB"), typeof(glWindowPos3svARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3svARB'.");
            }
        }
        public static void glWindowPos3svARB(const GLshort * @v) => glWindowPos3svARBPtr(@v);
        #endregion
    }
}
