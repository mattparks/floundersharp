using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_indirect_parameters
    {
        #region Interop
        static GL_ARB_indirect_parameters()
        {
            Console.WriteLine("Initalising GL_ARB_indirect_parameters interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMultiDrawArraysIndirectCountARB();
            loadMultiDrawElementsIndirectCountARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PARAMETER_BUFFER_ARB = 0x80EE;
        public static UInt32 GL_PARAMETER_BUFFER_BINDING_ARB = 0x80EF;
        #endregion

        #region Commands
        internal delegate void glMultiDrawArraysIndirectCountARBFunc(GLenum @mode, GLintptr @indirect, GLintptr @drawcount, GLsizei @maxdrawcount, GLsizei @stride);
        internal static glMultiDrawArraysIndirectCountARBFunc glMultiDrawArraysIndirectCountARBPtr;
        internal static void loadMultiDrawArraysIndirectCountARB()
        {
            try
            {
                glMultiDrawArraysIndirectCountARBPtr = (glMultiDrawArraysIndirectCountARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawArraysIndirectCountARB"), typeof(glMultiDrawArraysIndirectCountARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawArraysIndirectCountARB'.");
            }
        }
        public static void glMultiDrawArraysIndirectCountARB(GLenum @mode, GLintptr @indirect, GLintptr @drawcount, GLsizei @maxdrawcount, GLsizei @stride) => glMultiDrawArraysIndirectCountARBPtr(@mode, @indirect, @drawcount, @maxdrawcount, @stride);

        internal delegate void glMultiDrawElementsIndirectCountARBFunc(GLenum @mode, GLenum @type, GLintptr @indirect, GLintptr @drawcount, GLsizei @maxdrawcount, GLsizei @stride);
        internal static glMultiDrawElementsIndirectCountARBFunc glMultiDrawElementsIndirectCountARBPtr;
        internal static void loadMultiDrawElementsIndirectCountARB()
        {
            try
            {
                glMultiDrawElementsIndirectCountARBPtr = (glMultiDrawElementsIndirectCountARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiDrawElementsIndirectCountARB"), typeof(glMultiDrawElementsIndirectCountARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiDrawElementsIndirectCountARB'.");
            }
        }
        public static void glMultiDrawElementsIndirectCountARB(GLenum @mode, GLenum @type, GLintptr @indirect, GLintptr @drawcount, GLsizei @maxdrawcount, GLsizei @stride) => glMultiDrawElementsIndirectCountARBPtr(@mode, @type, @indirect, @drawcount, @maxdrawcount, @stride);
        #endregion
    }
}
