using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_buffer_range
    {
        #region Interop
        static GL_ARB_texture_buffer_range()
        {
            Console.WriteLine("Initalising GL_ARB_texture_buffer_range interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexBufferRange();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_BUFFER_OFFSET = 0x919D;
        public static UInt32 GL_TEXTURE_BUFFER_SIZE = 0x919E;
        public static UInt32 GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT = 0x919F;
        #endregion

        #region Commands
        internal delegate void glTexBufferRangeFunc(GLenum @target, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size);
        internal static glTexBufferRangeFunc glTexBufferRangePtr;
        internal static void loadTexBufferRange()
        {
            try
            {
                glTexBufferRangePtr = (glTexBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBufferRange"), typeof(glTexBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBufferRange'.");
            }
        }
        public static void glTexBufferRange(GLenum @target, GLenum @internalformat, GLuint @buffer, GLintptr @offset, GLsizeiptr @size) => glTexBufferRangePtr(@target, @internalformat, @buffer, @offset, @size);
        #endregion
    }
}
