using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_program_interface_query
    {
        #region Interop
        static GL_ARB_program_interface_query()
        {
            Console.WriteLine("Initalising GL_ARB_program_interface_query interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetProgramInterfaceiv();
            loadGetProgramResourceIndex();
            loadGetProgramResourceName();
            loadGetProgramResourceiv();
            loadGetProgramResourceLocation();
            loadGetProgramResourceLocationIndex();
        }
        #endregion

        #region Enums
        public static UInt32 GL_UNIFORM = 0x92E1;
        public static UInt32 GL_UNIFORM_BLOCK = 0x92E2;
        public static UInt32 GL_PROGRAM_INPUT = 0x92E3;
        public static UInt32 GL_PROGRAM_OUTPUT = 0x92E4;
        public static UInt32 GL_BUFFER_VARIABLE = 0x92E5;
        public static UInt32 GL_SHADER_STORAGE_BLOCK = 0x92E6;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER = 0x92C0;
        public static UInt32 GL_VERTEX_SUBROUTINE = 0x92E8;
        public static UInt32 GL_TESS_CONTROL_SUBROUTINE = 0x92E9;
        public static UInt32 GL_TESS_EVALUATION_SUBROUTINE = 0x92EA;
        public static UInt32 GL_GEOMETRY_SUBROUTINE = 0x92EB;
        public static UInt32 GL_FRAGMENT_SUBROUTINE = 0x92EC;
        public static UInt32 GL_COMPUTE_SUBROUTINE = 0x92ED;
        public static UInt32 GL_VERTEX_SUBROUTINE_UNIFORM = 0x92EE;
        public static UInt32 GL_TESS_CONTROL_SUBROUTINE_UNIFORM = 0x92EF;
        public static UInt32 GL_TESS_EVALUATION_SUBROUTINE_UNIFORM = 0x92F0;
        public static UInt32 GL_GEOMETRY_SUBROUTINE_UNIFORM = 0x92F1;
        public static UInt32 GL_FRAGMENT_SUBROUTINE_UNIFORM = 0x92F2;
        public static UInt32 GL_COMPUTE_SUBROUTINE_UNIFORM = 0x92F3;
        public static UInt32 GL_TRANSFORM_FEEDBACK_VARYING = 0x92F4;
        public static UInt32 GL_ACTIVE_RESOURCES = 0x92F5;
        public static UInt32 GL_MAX_NAME_LENGTH = 0x92F6;
        public static UInt32 GL_MAX_NUM_ACTIVE_VARIABLES = 0x92F7;
        public static UInt32 GL_MAX_NUM_COMPATIBLE_SUBROUTINES = 0x92F8;
        public static UInt32 GL_NAME_LENGTH = 0x92F9;
        public static UInt32 GL_TYPE = 0x92FA;
        public static UInt32 GL_ARRAY_SIZE = 0x92FB;
        public static UInt32 GL_OFFSET = 0x92FC;
        public static UInt32 GL_BLOCK_INDEX = 0x92FD;
        public static UInt32 GL_ARRAY_STRIDE = 0x92FE;
        public static UInt32 GL_MATRIX_STRIDE = 0x92FF;
        public static UInt32 GL_IS_ROW_MAJOR = 0x9300;
        public static UInt32 GL_ATOMIC_COUNTER_BUFFER_INDEX = 0x9301;
        public static UInt32 GL_BUFFER_BINDING = 0x9302;
        public static UInt32 GL_BUFFER_DATA_SIZE = 0x9303;
        public static UInt32 GL_NUM_ACTIVE_VARIABLES = 0x9304;
        public static UInt32 GL_ACTIVE_VARIABLES = 0x9305;
        public static UInt32 GL_REFERENCED_BY_VERTEX_SHADER = 0x9306;
        public static UInt32 GL_REFERENCED_BY_TESS_CONTROL_SHADER = 0x9307;
        public static UInt32 GL_REFERENCED_BY_TESS_EVALUATION_SHADER = 0x9308;
        public static UInt32 GL_REFERENCED_BY_GEOMETRY_SHADER = 0x9309;
        public static UInt32 GL_REFERENCED_BY_FRAGMENT_SHADER = 0x930A;
        public static UInt32 GL_REFERENCED_BY_COMPUTE_SHADER = 0x930B;
        public static UInt32 GL_TOP_LEVEL_ARRAY_SIZE = 0x930C;
        public static UInt32 GL_TOP_LEVEL_ARRAY_STRIDE = 0x930D;
        public static UInt32 GL_LOCATION = 0x930E;
        public static UInt32 GL_LOCATION_INDEX = 0x930F;
        public static UInt32 GL_IS_PER_PATCH = 0x92E7;
        public static UInt32 GL_NUM_COMPATIBLE_SUBROUTINES = 0x8E4A;
        public static UInt32 GL_COMPATIBLE_SUBROUTINES = 0x8E4B;
        #endregion

        #region Commands
        internal delegate void glGetProgramInterfaceivFunc(GLuint @program, GLenum @programInterface, GLenum @pname, GLint * @params);
        internal static glGetProgramInterfaceivFunc glGetProgramInterfaceivPtr;
        internal static void loadGetProgramInterfaceiv()
        {
            try
            {
                glGetProgramInterfaceivPtr = (glGetProgramInterfaceivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramInterfaceiv"), typeof(glGetProgramInterfaceivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramInterfaceiv'.");
            }
        }
        public static void glGetProgramInterfaceiv(GLuint @program, GLenum @programInterface, GLenum @pname, GLint * @params) => glGetProgramInterfaceivPtr(@program, @programInterface, @pname, @params);

        internal delegate GLuint glGetProgramResourceIndexFunc(GLuint @program, GLenum @programInterface, const GLchar * @name);
        internal static glGetProgramResourceIndexFunc glGetProgramResourceIndexPtr;
        internal static void loadGetProgramResourceIndex()
        {
            try
            {
                glGetProgramResourceIndexPtr = (glGetProgramResourceIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceIndex"), typeof(glGetProgramResourceIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceIndex'.");
            }
        }
        public static GLuint glGetProgramResourceIndex(GLuint @program, GLenum @programInterface, const GLchar * @name) => glGetProgramResourceIndexPtr(@program, @programInterface, @name);

        internal delegate void glGetProgramResourceNameFunc(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLchar * @name);
        internal static glGetProgramResourceNameFunc glGetProgramResourceNamePtr;
        internal static void loadGetProgramResourceName()
        {
            try
            {
                glGetProgramResourceNamePtr = (glGetProgramResourceNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceName"), typeof(glGetProgramResourceNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceName'.");
            }
        }
        public static void glGetProgramResourceName(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @bufSize, GLsizei * @length, GLchar * @name) => glGetProgramResourceNamePtr(@program, @programInterface, @index, @bufSize, @length, @name);

        internal delegate void glGetProgramResourceivFunc(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @propCount, const GLenum * @props, GLsizei @bufSize, GLsizei * @length, GLint * @params);
        internal static glGetProgramResourceivFunc glGetProgramResourceivPtr;
        internal static void loadGetProgramResourceiv()
        {
            try
            {
                glGetProgramResourceivPtr = (glGetProgramResourceivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceiv"), typeof(glGetProgramResourceivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceiv'.");
            }
        }
        public static void glGetProgramResourceiv(GLuint @program, GLenum @programInterface, GLuint @index, GLsizei @propCount, const GLenum * @props, GLsizei @bufSize, GLsizei * @length, GLint * @params) => glGetProgramResourceivPtr(@program, @programInterface, @index, @propCount, @props, @bufSize, @length, @params);

        internal delegate GLint glGetProgramResourceLocationFunc(GLuint @program, GLenum @programInterface, const GLchar * @name);
        internal static glGetProgramResourceLocationFunc glGetProgramResourceLocationPtr;
        internal static void loadGetProgramResourceLocation()
        {
            try
            {
                glGetProgramResourceLocationPtr = (glGetProgramResourceLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceLocation"), typeof(glGetProgramResourceLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceLocation'.");
            }
        }
        public static GLint glGetProgramResourceLocation(GLuint @program, GLenum @programInterface, const GLchar * @name) => glGetProgramResourceLocationPtr(@program, @programInterface, @name);

        internal delegate GLint glGetProgramResourceLocationIndexFunc(GLuint @program, GLenum @programInterface, const GLchar * @name);
        internal static glGetProgramResourceLocationIndexFunc glGetProgramResourceLocationIndexPtr;
        internal static void loadGetProgramResourceLocationIndex()
        {
            try
            {
                glGetProgramResourceLocationIndexPtr = (glGetProgramResourceLocationIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramResourceLocationIndex"), typeof(glGetProgramResourceLocationIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramResourceLocationIndex'.");
            }
        }
        public static GLint glGetProgramResourceLocationIndex(GLuint @program, GLenum @programInterface, const GLchar * @name) => glGetProgramResourceLocationIndexPtr(@program, @programInterface, @name);
        #endregion
    }
}
