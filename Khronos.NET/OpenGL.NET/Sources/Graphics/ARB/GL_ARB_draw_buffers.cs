using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_draw_buffers
    {
        #region Interop
        static GL_ARB_draw_buffers()
        {
            Console.WriteLine("Initalising GL_ARB_draw_buffers interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawBuffersARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_DRAW_BUFFERS_ARB = 0x8824;
        public static UInt32 GL_DRAW_BUFFER0_ARB = 0x8825;
        public static UInt32 GL_DRAW_BUFFER1_ARB = 0x8826;
        public static UInt32 GL_DRAW_BUFFER2_ARB = 0x8827;
        public static UInt32 GL_DRAW_BUFFER3_ARB = 0x8828;
        public static UInt32 GL_DRAW_BUFFER4_ARB = 0x8829;
        public static UInt32 GL_DRAW_BUFFER5_ARB = 0x882A;
        public static UInt32 GL_DRAW_BUFFER6_ARB = 0x882B;
        public static UInt32 GL_DRAW_BUFFER7_ARB = 0x882C;
        public static UInt32 GL_DRAW_BUFFER8_ARB = 0x882D;
        public static UInt32 GL_DRAW_BUFFER9_ARB = 0x882E;
        public static UInt32 GL_DRAW_BUFFER10_ARB = 0x882F;
        public static UInt32 GL_DRAW_BUFFER11_ARB = 0x8830;
        public static UInt32 GL_DRAW_BUFFER12_ARB = 0x8831;
        public static UInt32 GL_DRAW_BUFFER13_ARB = 0x8832;
        public static UInt32 GL_DRAW_BUFFER14_ARB = 0x8833;
        public static UInt32 GL_DRAW_BUFFER15_ARB = 0x8834;
        #endregion

        #region Commands
        internal delegate void glDrawBuffersARBFunc(GLsizei @n, const GLenum * @bufs);
        internal static glDrawBuffersARBFunc glDrawBuffersARBPtr;
        internal static void loadDrawBuffersARB()
        {
            try
            {
                glDrawBuffersARBPtr = (glDrawBuffersARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawBuffersARB"), typeof(glDrawBuffersARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawBuffersARB'.");
            }
        }
        public static void glDrawBuffersARB(GLsizei @n, const GLenum * @bufs) => glDrawBuffersARBPtr(@n, @bufs);
        #endregion
    }
}
