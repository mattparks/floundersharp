using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_cube_map_array
    {
        #region Interop
        static GL_ARB_texture_cube_map_array()
        {
            Console.WriteLine("Initalising GL_ARB_texture_cube_map_array interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_CUBE_MAP_ARRAY_ARB = 0x9009;
        public static UInt32 GL_TEXTURE_BINDING_CUBE_MAP_ARRAY_ARB = 0x900A;
        public static UInt32 GL_PROXY_TEXTURE_CUBE_MAP_ARRAY_ARB = 0x900B;
        public static UInt32 GL_SAMPLER_CUBE_MAP_ARRAY_ARB = 0x900C;
        public static UInt32 GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW_ARB = 0x900D;
        public static UInt32 GL_INT_SAMPLER_CUBE_MAP_ARRAY_ARB = 0x900E;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY_ARB = 0x900F;
        #endregion

        #region Commands
        #endregion
    }
}
