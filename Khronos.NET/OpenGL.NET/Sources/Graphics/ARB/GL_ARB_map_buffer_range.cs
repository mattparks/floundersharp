using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_map_buffer_range
    {
        #region Interop
        static GL_ARB_map_buffer_range()
        {
            Console.WriteLine("Initalising GL_ARB_map_buffer_range interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMapBufferRange();
            loadFlushMappedBufferRange();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAP_READ_BIT = 0x0001;
        public static UInt32 GL_MAP_WRITE_BIT = 0x0002;
        public static UInt32 GL_MAP_INVALIDATE_RANGE_BIT = 0x0004;
        public static UInt32 GL_MAP_INVALIDATE_BUFFER_BIT = 0x0008;
        public static UInt32 GL_MAP_FLUSH_EXPLICIT_BIT = 0x0010;
        public static UInt32 GL_MAP_UNSYNCHRONIZED_BIT = 0x0020;
        #endregion

        #region Commands
        internal delegate void * glMapBufferRangeFunc(GLenum @target, GLintptr @offset, GLsizeiptr @length, GLbitfield @access);
        internal static glMapBufferRangeFunc glMapBufferRangePtr;
        internal static void loadMapBufferRange()
        {
            try
            {
                glMapBufferRangePtr = (glMapBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMapBufferRange"), typeof(glMapBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMapBufferRange'.");
            }
        }
        public static void * glMapBufferRange(GLenum @target, GLintptr @offset, GLsizeiptr @length, GLbitfield @access) => glMapBufferRangePtr(@target, @offset, @length, @access);

        internal delegate void glFlushMappedBufferRangeFunc(GLenum @target, GLintptr @offset, GLsizeiptr @length);
        internal static glFlushMappedBufferRangeFunc glFlushMappedBufferRangePtr;
        internal static void loadFlushMappedBufferRange()
        {
            try
            {
                glFlushMappedBufferRangePtr = (glFlushMappedBufferRangeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlushMappedBufferRange"), typeof(glFlushMappedBufferRangeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlushMappedBufferRange'.");
            }
        }
        public static void glFlushMappedBufferRange(GLenum @target, GLintptr @offset, GLsizeiptr @length) => glFlushMappedBufferRangePtr(@target, @offset, @length);
        #endregion
    }
}
