using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_multitexture
    {
        #region Interop
        static GL_ARB_multitexture()
        {
            Console.WriteLine("Initalising GL_ARB_multitexture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadActiveTextureARB();
            loadClientActiveTextureARB();
            loadMultiTexCoord1dARB();
            loadMultiTexCoord1dvARB();
            loadMultiTexCoord1fARB();
            loadMultiTexCoord1fvARB();
            loadMultiTexCoord1iARB();
            loadMultiTexCoord1ivARB();
            loadMultiTexCoord1sARB();
            loadMultiTexCoord1svARB();
            loadMultiTexCoord2dARB();
            loadMultiTexCoord2dvARB();
            loadMultiTexCoord2fARB();
            loadMultiTexCoord2fvARB();
            loadMultiTexCoord2iARB();
            loadMultiTexCoord2ivARB();
            loadMultiTexCoord2sARB();
            loadMultiTexCoord2svARB();
            loadMultiTexCoord3dARB();
            loadMultiTexCoord3dvARB();
            loadMultiTexCoord3fARB();
            loadMultiTexCoord3fvARB();
            loadMultiTexCoord3iARB();
            loadMultiTexCoord3ivARB();
            loadMultiTexCoord3sARB();
            loadMultiTexCoord3svARB();
            loadMultiTexCoord4dARB();
            loadMultiTexCoord4dvARB();
            loadMultiTexCoord4fARB();
            loadMultiTexCoord4fvARB();
            loadMultiTexCoord4iARB();
            loadMultiTexCoord4ivARB();
            loadMultiTexCoord4sARB();
            loadMultiTexCoord4svARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE0_ARB = 0x84C0;
        public static UInt32 GL_TEXTURE1_ARB = 0x84C1;
        public static UInt32 GL_TEXTURE2_ARB = 0x84C2;
        public static UInt32 GL_TEXTURE3_ARB = 0x84C3;
        public static UInt32 GL_TEXTURE4_ARB = 0x84C4;
        public static UInt32 GL_TEXTURE5_ARB = 0x84C5;
        public static UInt32 GL_TEXTURE6_ARB = 0x84C6;
        public static UInt32 GL_TEXTURE7_ARB = 0x84C7;
        public static UInt32 GL_TEXTURE8_ARB = 0x84C8;
        public static UInt32 GL_TEXTURE9_ARB = 0x84C9;
        public static UInt32 GL_TEXTURE10_ARB = 0x84CA;
        public static UInt32 GL_TEXTURE11_ARB = 0x84CB;
        public static UInt32 GL_TEXTURE12_ARB = 0x84CC;
        public static UInt32 GL_TEXTURE13_ARB = 0x84CD;
        public static UInt32 GL_TEXTURE14_ARB = 0x84CE;
        public static UInt32 GL_TEXTURE15_ARB = 0x84CF;
        public static UInt32 GL_TEXTURE16_ARB = 0x84D0;
        public static UInt32 GL_TEXTURE17_ARB = 0x84D1;
        public static UInt32 GL_TEXTURE18_ARB = 0x84D2;
        public static UInt32 GL_TEXTURE19_ARB = 0x84D3;
        public static UInt32 GL_TEXTURE20_ARB = 0x84D4;
        public static UInt32 GL_TEXTURE21_ARB = 0x84D5;
        public static UInt32 GL_TEXTURE22_ARB = 0x84D6;
        public static UInt32 GL_TEXTURE23_ARB = 0x84D7;
        public static UInt32 GL_TEXTURE24_ARB = 0x84D8;
        public static UInt32 GL_TEXTURE25_ARB = 0x84D9;
        public static UInt32 GL_TEXTURE26_ARB = 0x84DA;
        public static UInt32 GL_TEXTURE27_ARB = 0x84DB;
        public static UInt32 GL_TEXTURE28_ARB = 0x84DC;
        public static UInt32 GL_TEXTURE29_ARB = 0x84DD;
        public static UInt32 GL_TEXTURE30_ARB = 0x84DE;
        public static UInt32 GL_TEXTURE31_ARB = 0x84DF;
        public static UInt32 GL_ACTIVE_TEXTURE_ARB = 0x84E0;
        public static UInt32 GL_CLIENT_ACTIVE_TEXTURE_ARB = 0x84E1;
        public static UInt32 GL_MAX_TEXTURE_UNITS_ARB = 0x84E2;
        #endregion

        #region Commands
        internal delegate void glActiveTextureARBFunc(GLenum @texture);
        internal static glActiveTextureARBFunc glActiveTextureARBPtr;
        internal static void loadActiveTextureARB()
        {
            try
            {
                glActiveTextureARBPtr = (glActiveTextureARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glActiveTextureARB"), typeof(glActiveTextureARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glActiveTextureARB'.");
            }
        }
        public static void glActiveTextureARB(GLenum @texture) => glActiveTextureARBPtr(@texture);

        internal delegate void glClientActiveTextureARBFunc(GLenum @texture);
        internal static glClientActiveTextureARBFunc glClientActiveTextureARBPtr;
        internal static void loadClientActiveTextureARB()
        {
            try
            {
                glClientActiveTextureARBPtr = (glClientActiveTextureARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClientActiveTextureARB"), typeof(glClientActiveTextureARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClientActiveTextureARB'.");
            }
        }
        public static void glClientActiveTextureARB(GLenum @texture) => glClientActiveTextureARBPtr(@texture);

        internal delegate void glMultiTexCoord1dARBFunc(GLenum @target, GLdouble @s);
        internal static glMultiTexCoord1dARBFunc glMultiTexCoord1dARBPtr;
        internal static void loadMultiTexCoord1dARB()
        {
            try
            {
                glMultiTexCoord1dARBPtr = (glMultiTexCoord1dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1dARB"), typeof(glMultiTexCoord1dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1dARB'.");
            }
        }
        public static void glMultiTexCoord1dARB(GLenum @target, GLdouble @s) => glMultiTexCoord1dARBPtr(@target, @s);

        internal delegate void glMultiTexCoord1dvARBFunc(GLenum @target, const GLdouble * @v);
        internal static glMultiTexCoord1dvARBFunc glMultiTexCoord1dvARBPtr;
        internal static void loadMultiTexCoord1dvARB()
        {
            try
            {
                glMultiTexCoord1dvARBPtr = (glMultiTexCoord1dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1dvARB"), typeof(glMultiTexCoord1dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1dvARB'.");
            }
        }
        public static void glMultiTexCoord1dvARB(GLenum @target, const GLdouble * @v) => glMultiTexCoord1dvARBPtr(@target, @v);

        internal delegate void glMultiTexCoord1fARBFunc(GLenum @target, GLfloat @s);
        internal static glMultiTexCoord1fARBFunc glMultiTexCoord1fARBPtr;
        internal static void loadMultiTexCoord1fARB()
        {
            try
            {
                glMultiTexCoord1fARBPtr = (glMultiTexCoord1fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1fARB"), typeof(glMultiTexCoord1fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1fARB'.");
            }
        }
        public static void glMultiTexCoord1fARB(GLenum @target, GLfloat @s) => glMultiTexCoord1fARBPtr(@target, @s);

        internal delegate void glMultiTexCoord1fvARBFunc(GLenum @target, const GLfloat * @v);
        internal static glMultiTexCoord1fvARBFunc glMultiTexCoord1fvARBPtr;
        internal static void loadMultiTexCoord1fvARB()
        {
            try
            {
                glMultiTexCoord1fvARBPtr = (glMultiTexCoord1fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1fvARB"), typeof(glMultiTexCoord1fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1fvARB'.");
            }
        }
        public static void glMultiTexCoord1fvARB(GLenum @target, const GLfloat * @v) => glMultiTexCoord1fvARBPtr(@target, @v);

        internal delegate void glMultiTexCoord1iARBFunc(GLenum @target, GLint @s);
        internal static glMultiTexCoord1iARBFunc glMultiTexCoord1iARBPtr;
        internal static void loadMultiTexCoord1iARB()
        {
            try
            {
                glMultiTexCoord1iARBPtr = (glMultiTexCoord1iARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1iARB"), typeof(glMultiTexCoord1iARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1iARB'.");
            }
        }
        public static void glMultiTexCoord1iARB(GLenum @target, GLint @s) => glMultiTexCoord1iARBPtr(@target, @s);

        internal delegate void glMultiTexCoord1ivARBFunc(GLenum @target, const GLint * @v);
        internal static glMultiTexCoord1ivARBFunc glMultiTexCoord1ivARBPtr;
        internal static void loadMultiTexCoord1ivARB()
        {
            try
            {
                glMultiTexCoord1ivARBPtr = (glMultiTexCoord1ivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1ivARB"), typeof(glMultiTexCoord1ivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1ivARB'.");
            }
        }
        public static void glMultiTexCoord1ivARB(GLenum @target, const GLint * @v) => glMultiTexCoord1ivARBPtr(@target, @v);

        internal delegate void glMultiTexCoord1sARBFunc(GLenum @target, GLshort @s);
        internal static glMultiTexCoord1sARBFunc glMultiTexCoord1sARBPtr;
        internal static void loadMultiTexCoord1sARB()
        {
            try
            {
                glMultiTexCoord1sARBPtr = (glMultiTexCoord1sARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1sARB"), typeof(glMultiTexCoord1sARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1sARB'.");
            }
        }
        public static void glMultiTexCoord1sARB(GLenum @target, GLshort @s) => glMultiTexCoord1sARBPtr(@target, @s);

        internal delegate void glMultiTexCoord1svARBFunc(GLenum @target, const GLshort * @v);
        internal static glMultiTexCoord1svARBFunc glMultiTexCoord1svARBPtr;
        internal static void loadMultiTexCoord1svARB()
        {
            try
            {
                glMultiTexCoord1svARBPtr = (glMultiTexCoord1svARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord1svARB"), typeof(glMultiTexCoord1svARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord1svARB'.");
            }
        }
        public static void glMultiTexCoord1svARB(GLenum @target, const GLshort * @v) => glMultiTexCoord1svARBPtr(@target, @v);

        internal delegate void glMultiTexCoord2dARBFunc(GLenum @target, GLdouble @s, GLdouble @t);
        internal static glMultiTexCoord2dARBFunc glMultiTexCoord2dARBPtr;
        internal static void loadMultiTexCoord2dARB()
        {
            try
            {
                glMultiTexCoord2dARBPtr = (glMultiTexCoord2dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2dARB"), typeof(glMultiTexCoord2dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2dARB'.");
            }
        }
        public static void glMultiTexCoord2dARB(GLenum @target, GLdouble @s, GLdouble @t) => glMultiTexCoord2dARBPtr(@target, @s, @t);

        internal delegate void glMultiTexCoord2dvARBFunc(GLenum @target, const GLdouble * @v);
        internal static glMultiTexCoord2dvARBFunc glMultiTexCoord2dvARBPtr;
        internal static void loadMultiTexCoord2dvARB()
        {
            try
            {
                glMultiTexCoord2dvARBPtr = (glMultiTexCoord2dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2dvARB"), typeof(glMultiTexCoord2dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2dvARB'.");
            }
        }
        public static void glMultiTexCoord2dvARB(GLenum @target, const GLdouble * @v) => glMultiTexCoord2dvARBPtr(@target, @v);

        internal delegate void glMultiTexCoord2fARBFunc(GLenum @target, GLfloat @s, GLfloat @t);
        internal static glMultiTexCoord2fARBFunc glMultiTexCoord2fARBPtr;
        internal static void loadMultiTexCoord2fARB()
        {
            try
            {
                glMultiTexCoord2fARBPtr = (glMultiTexCoord2fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2fARB"), typeof(glMultiTexCoord2fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2fARB'.");
            }
        }
        public static void glMultiTexCoord2fARB(GLenum @target, GLfloat @s, GLfloat @t) => glMultiTexCoord2fARBPtr(@target, @s, @t);

        internal delegate void glMultiTexCoord2fvARBFunc(GLenum @target, const GLfloat * @v);
        internal static glMultiTexCoord2fvARBFunc glMultiTexCoord2fvARBPtr;
        internal static void loadMultiTexCoord2fvARB()
        {
            try
            {
                glMultiTexCoord2fvARBPtr = (glMultiTexCoord2fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2fvARB"), typeof(glMultiTexCoord2fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2fvARB'.");
            }
        }
        public static void glMultiTexCoord2fvARB(GLenum @target, const GLfloat * @v) => glMultiTexCoord2fvARBPtr(@target, @v);

        internal delegate void glMultiTexCoord2iARBFunc(GLenum @target, GLint @s, GLint @t);
        internal static glMultiTexCoord2iARBFunc glMultiTexCoord2iARBPtr;
        internal static void loadMultiTexCoord2iARB()
        {
            try
            {
                glMultiTexCoord2iARBPtr = (glMultiTexCoord2iARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2iARB"), typeof(glMultiTexCoord2iARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2iARB'.");
            }
        }
        public static void glMultiTexCoord2iARB(GLenum @target, GLint @s, GLint @t) => glMultiTexCoord2iARBPtr(@target, @s, @t);

        internal delegate void glMultiTexCoord2ivARBFunc(GLenum @target, const GLint * @v);
        internal static glMultiTexCoord2ivARBFunc glMultiTexCoord2ivARBPtr;
        internal static void loadMultiTexCoord2ivARB()
        {
            try
            {
                glMultiTexCoord2ivARBPtr = (glMultiTexCoord2ivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2ivARB"), typeof(glMultiTexCoord2ivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2ivARB'.");
            }
        }
        public static void glMultiTexCoord2ivARB(GLenum @target, const GLint * @v) => glMultiTexCoord2ivARBPtr(@target, @v);

        internal delegate void glMultiTexCoord2sARBFunc(GLenum @target, GLshort @s, GLshort @t);
        internal static glMultiTexCoord2sARBFunc glMultiTexCoord2sARBPtr;
        internal static void loadMultiTexCoord2sARB()
        {
            try
            {
                glMultiTexCoord2sARBPtr = (glMultiTexCoord2sARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2sARB"), typeof(glMultiTexCoord2sARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2sARB'.");
            }
        }
        public static void glMultiTexCoord2sARB(GLenum @target, GLshort @s, GLshort @t) => glMultiTexCoord2sARBPtr(@target, @s, @t);

        internal delegate void glMultiTexCoord2svARBFunc(GLenum @target, const GLshort * @v);
        internal static glMultiTexCoord2svARBFunc glMultiTexCoord2svARBPtr;
        internal static void loadMultiTexCoord2svARB()
        {
            try
            {
                glMultiTexCoord2svARBPtr = (glMultiTexCoord2svARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord2svARB"), typeof(glMultiTexCoord2svARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord2svARB'.");
            }
        }
        public static void glMultiTexCoord2svARB(GLenum @target, const GLshort * @v) => glMultiTexCoord2svARBPtr(@target, @v);

        internal delegate void glMultiTexCoord3dARBFunc(GLenum @target, GLdouble @s, GLdouble @t, GLdouble @r);
        internal static glMultiTexCoord3dARBFunc glMultiTexCoord3dARBPtr;
        internal static void loadMultiTexCoord3dARB()
        {
            try
            {
                glMultiTexCoord3dARBPtr = (glMultiTexCoord3dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3dARB"), typeof(glMultiTexCoord3dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3dARB'.");
            }
        }
        public static void glMultiTexCoord3dARB(GLenum @target, GLdouble @s, GLdouble @t, GLdouble @r) => glMultiTexCoord3dARBPtr(@target, @s, @t, @r);

        internal delegate void glMultiTexCoord3dvARBFunc(GLenum @target, const GLdouble * @v);
        internal static glMultiTexCoord3dvARBFunc glMultiTexCoord3dvARBPtr;
        internal static void loadMultiTexCoord3dvARB()
        {
            try
            {
                glMultiTexCoord3dvARBPtr = (glMultiTexCoord3dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3dvARB"), typeof(glMultiTexCoord3dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3dvARB'.");
            }
        }
        public static void glMultiTexCoord3dvARB(GLenum @target, const GLdouble * @v) => glMultiTexCoord3dvARBPtr(@target, @v);

        internal delegate void glMultiTexCoord3fARBFunc(GLenum @target, GLfloat @s, GLfloat @t, GLfloat @r);
        internal static glMultiTexCoord3fARBFunc glMultiTexCoord3fARBPtr;
        internal static void loadMultiTexCoord3fARB()
        {
            try
            {
                glMultiTexCoord3fARBPtr = (glMultiTexCoord3fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3fARB"), typeof(glMultiTexCoord3fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3fARB'.");
            }
        }
        public static void glMultiTexCoord3fARB(GLenum @target, GLfloat @s, GLfloat @t, GLfloat @r) => glMultiTexCoord3fARBPtr(@target, @s, @t, @r);

        internal delegate void glMultiTexCoord3fvARBFunc(GLenum @target, const GLfloat * @v);
        internal static glMultiTexCoord3fvARBFunc glMultiTexCoord3fvARBPtr;
        internal static void loadMultiTexCoord3fvARB()
        {
            try
            {
                glMultiTexCoord3fvARBPtr = (glMultiTexCoord3fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3fvARB"), typeof(glMultiTexCoord3fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3fvARB'.");
            }
        }
        public static void glMultiTexCoord3fvARB(GLenum @target, const GLfloat * @v) => glMultiTexCoord3fvARBPtr(@target, @v);

        internal delegate void glMultiTexCoord3iARBFunc(GLenum @target, GLint @s, GLint @t, GLint @r);
        internal static glMultiTexCoord3iARBFunc glMultiTexCoord3iARBPtr;
        internal static void loadMultiTexCoord3iARB()
        {
            try
            {
                glMultiTexCoord3iARBPtr = (glMultiTexCoord3iARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3iARB"), typeof(glMultiTexCoord3iARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3iARB'.");
            }
        }
        public static void glMultiTexCoord3iARB(GLenum @target, GLint @s, GLint @t, GLint @r) => glMultiTexCoord3iARBPtr(@target, @s, @t, @r);

        internal delegate void glMultiTexCoord3ivARBFunc(GLenum @target, const GLint * @v);
        internal static glMultiTexCoord3ivARBFunc glMultiTexCoord3ivARBPtr;
        internal static void loadMultiTexCoord3ivARB()
        {
            try
            {
                glMultiTexCoord3ivARBPtr = (glMultiTexCoord3ivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3ivARB"), typeof(glMultiTexCoord3ivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3ivARB'.");
            }
        }
        public static void glMultiTexCoord3ivARB(GLenum @target, const GLint * @v) => glMultiTexCoord3ivARBPtr(@target, @v);

        internal delegate void glMultiTexCoord3sARBFunc(GLenum @target, GLshort @s, GLshort @t, GLshort @r);
        internal static glMultiTexCoord3sARBFunc glMultiTexCoord3sARBPtr;
        internal static void loadMultiTexCoord3sARB()
        {
            try
            {
                glMultiTexCoord3sARBPtr = (glMultiTexCoord3sARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3sARB"), typeof(glMultiTexCoord3sARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3sARB'.");
            }
        }
        public static void glMultiTexCoord3sARB(GLenum @target, GLshort @s, GLshort @t, GLshort @r) => glMultiTexCoord3sARBPtr(@target, @s, @t, @r);

        internal delegate void glMultiTexCoord3svARBFunc(GLenum @target, const GLshort * @v);
        internal static glMultiTexCoord3svARBFunc glMultiTexCoord3svARBPtr;
        internal static void loadMultiTexCoord3svARB()
        {
            try
            {
                glMultiTexCoord3svARBPtr = (glMultiTexCoord3svARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord3svARB"), typeof(glMultiTexCoord3svARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord3svARB'.");
            }
        }
        public static void glMultiTexCoord3svARB(GLenum @target, const GLshort * @v) => glMultiTexCoord3svARBPtr(@target, @v);

        internal delegate void glMultiTexCoord4dARBFunc(GLenum @target, GLdouble @s, GLdouble @t, GLdouble @r, GLdouble @q);
        internal static glMultiTexCoord4dARBFunc glMultiTexCoord4dARBPtr;
        internal static void loadMultiTexCoord4dARB()
        {
            try
            {
                glMultiTexCoord4dARBPtr = (glMultiTexCoord4dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4dARB"), typeof(glMultiTexCoord4dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4dARB'.");
            }
        }
        public static void glMultiTexCoord4dARB(GLenum @target, GLdouble @s, GLdouble @t, GLdouble @r, GLdouble @q) => glMultiTexCoord4dARBPtr(@target, @s, @t, @r, @q);

        internal delegate void glMultiTexCoord4dvARBFunc(GLenum @target, const GLdouble * @v);
        internal static glMultiTexCoord4dvARBFunc glMultiTexCoord4dvARBPtr;
        internal static void loadMultiTexCoord4dvARB()
        {
            try
            {
                glMultiTexCoord4dvARBPtr = (glMultiTexCoord4dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4dvARB"), typeof(glMultiTexCoord4dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4dvARB'.");
            }
        }
        public static void glMultiTexCoord4dvARB(GLenum @target, const GLdouble * @v) => glMultiTexCoord4dvARBPtr(@target, @v);

        internal delegate void glMultiTexCoord4fARBFunc(GLenum @target, GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @q);
        internal static glMultiTexCoord4fARBFunc glMultiTexCoord4fARBPtr;
        internal static void loadMultiTexCoord4fARB()
        {
            try
            {
                glMultiTexCoord4fARBPtr = (glMultiTexCoord4fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4fARB"), typeof(glMultiTexCoord4fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4fARB'.");
            }
        }
        public static void glMultiTexCoord4fARB(GLenum @target, GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @q) => glMultiTexCoord4fARBPtr(@target, @s, @t, @r, @q);

        internal delegate void glMultiTexCoord4fvARBFunc(GLenum @target, const GLfloat * @v);
        internal static glMultiTexCoord4fvARBFunc glMultiTexCoord4fvARBPtr;
        internal static void loadMultiTexCoord4fvARB()
        {
            try
            {
                glMultiTexCoord4fvARBPtr = (glMultiTexCoord4fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4fvARB"), typeof(glMultiTexCoord4fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4fvARB'.");
            }
        }
        public static void glMultiTexCoord4fvARB(GLenum @target, const GLfloat * @v) => glMultiTexCoord4fvARBPtr(@target, @v);

        internal delegate void glMultiTexCoord4iARBFunc(GLenum @target, GLint @s, GLint @t, GLint @r, GLint @q);
        internal static glMultiTexCoord4iARBFunc glMultiTexCoord4iARBPtr;
        internal static void loadMultiTexCoord4iARB()
        {
            try
            {
                glMultiTexCoord4iARBPtr = (glMultiTexCoord4iARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4iARB"), typeof(glMultiTexCoord4iARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4iARB'.");
            }
        }
        public static void glMultiTexCoord4iARB(GLenum @target, GLint @s, GLint @t, GLint @r, GLint @q) => glMultiTexCoord4iARBPtr(@target, @s, @t, @r, @q);

        internal delegate void glMultiTexCoord4ivARBFunc(GLenum @target, const GLint * @v);
        internal static glMultiTexCoord4ivARBFunc glMultiTexCoord4ivARBPtr;
        internal static void loadMultiTexCoord4ivARB()
        {
            try
            {
                glMultiTexCoord4ivARBPtr = (glMultiTexCoord4ivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4ivARB"), typeof(glMultiTexCoord4ivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4ivARB'.");
            }
        }
        public static void glMultiTexCoord4ivARB(GLenum @target, const GLint * @v) => glMultiTexCoord4ivARBPtr(@target, @v);

        internal delegate void glMultiTexCoord4sARBFunc(GLenum @target, GLshort @s, GLshort @t, GLshort @r, GLshort @q);
        internal static glMultiTexCoord4sARBFunc glMultiTexCoord4sARBPtr;
        internal static void loadMultiTexCoord4sARB()
        {
            try
            {
                glMultiTexCoord4sARBPtr = (glMultiTexCoord4sARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4sARB"), typeof(glMultiTexCoord4sARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4sARB'.");
            }
        }
        public static void glMultiTexCoord4sARB(GLenum @target, GLshort @s, GLshort @t, GLshort @r, GLshort @q) => glMultiTexCoord4sARBPtr(@target, @s, @t, @r, @q);

        internal delegate void glMultiTexCoord4svARBFunc(GLenum @target, const GLshort * @v);
        internal static glMultiTexCoord4svARBFunc glMultiTexCoord4svARBPtr;
        internal static void loadMultiTexCoord4svARB()
        {
            try
            {
                glMultiTexCoord4svARBPtr = (glMultiTexCoord4svARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4svARB"), typeof(glMultiTexCoord4svARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4svARB'.");
            }
        }
        public static void glMultiTexCoord4svARB(GLenum @target, const GLshort * @v) => glMultiTexCoord4svARBPtr(@target, @v);
        #endregion
    }
}
