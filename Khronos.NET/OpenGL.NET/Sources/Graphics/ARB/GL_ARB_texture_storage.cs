using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_storage
    {
        #region Interop
        static GL_ARB_texture_storage()
        {
            Console.WriteLine("Initalising GL_ARB_texture_storage interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexStorage1D();
            loadTexStorage2D();
            loadTexStorage3D();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_IMMUTABLE_FORMAT = 0x912F;
        #endregion

        #region Commands
        internal delegate void glTexStorage1DFunc(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width);
        internal static glTexStorage1DFunc glTexStorage1DPtr;
        internal static void loadTexStorage1D()
        {
            try
            {
                glTexStorage1DPtr = (glTexStorage1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage1D"), typeof(glTexStorage1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage1D'.");
            }
        }
        public static void glTexStorage1D(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width) => glTexStorage1DPtr(@target, @levels, @internalformat, @width);

        internal delegate void glTexStorage2DFunc(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height);
        internal static glTexStorage2DFunc glTexStorage2DPtr;
        internal static void loadTexStorage2D()
        {
            try
            {
                glTexStorage2DPtr = (glTexStorage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage2D"), typeof(glTexStorage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage2D'.");
            }
        }
        public static void glTexStorage2D(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height) => glTexStorage2DPtr(@target, @levels, @internalformat, @width, @height);

        internal delegate void glTexStorage3DFunc(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth);
        internal static glTexStorage3DFunc glTexStorage3DPtr;
        internal static void loadTexStorage3D()
        {
            try
            {
                glTexStorage3DPtr = (glTexStorage3DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexStorage3D"), typeof(glTexStorage3DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexStorage3D'.");
            }
        }
        public static void glTexStorage3D(GLenum @target, GLsizei @levels, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth) => glTexStorage3DPtr(@target, @levels, @internalformat, @width, @height, @depth);
        #endregion
    }
}
