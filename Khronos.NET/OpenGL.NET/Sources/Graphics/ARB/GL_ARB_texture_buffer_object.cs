using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_buffer_object
    {
        #region Interop
        static GL_ARB_texture_buffer_object()
        {
            Console.WriteLine("Initalising GL_ARB_texture_buffer_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexBufferARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_BUFFER_ARB = 0x8C2A;
        public static UInt32 GL_MAX_TEXTURE_BUFFER_SIZE_ARB = 0x8C2B;
        public static UInt32 GL_TEXTURE_BINDING_BUFFER_ARB = 0x8C2C;
        public static UInt32 GL_TEXTURE_BUFFER_DATA_STORE_BINDING_ARB = 0x8C2D;
        public static UInt32 GL_TEXTURE_BUFFER_FORMAT_ARB = 0x8C2E;
        #endregion

        #region Commands
        internal delegate void glTexBufferARBFunc(GLenum @target, GLenum @internalformat, GLuint @buffer);
        internal static glTexBufferARBFunc glTexBufferARBPtr;
        internal static void loadTexBufferARB()
        {
            try
            {
                glTexBufferARBPtr = (glTexBufferARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexBufferARB"), typeof(glTexBufferARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexBufferARB'.");
            }
        }
        public static void glTexBufferARB(GLenum @target, GLenum @internalformat, GLuint @buffer) => glTexBufferARBPtr(@target, @internalformat, @buffer);
        #endregion
    }
}
