using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_sparse_buffer
    {
        #region Interop
        static GL_ARB_sparse_buffer()
        {
            Console.WriteLine("Initalising GL_ARB_sparse_buffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBufferPageCommitmentARB();
            loadNamedBufferPageCommitmentEXT();
            loadNamedBufferPageCommitmentARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SPARSE_STORAGE_BIT_ARB = 0x0400;
        public static UInt32 GL_SPARSE_BUFFER_PAGE_SIZE_ARB = 0x82F8;
        #endregion

        #region Commands
        internal delegate void glBufferPageCommitmentARBFunc(GLenum @target, GLintptr @offset, GLsizeiptr @size, GLboolean @commit);
        internal static glBufferPageCommitmentARBFunc glBufferPageCommitmentARBPtr;
        internal static void loadBufferPageCommitmentARB()
        {
            try
            {
                glBufferPageCommitmentARBPtr = (glBufferPageCommitmentARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferPageCommitmentARB"), typeof(glBufferPageCommitmentARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferPageCommitmentARB'.");
            }
        }
        public static void glBufferPageCommitmentARB(GLenum @target, GLintptr @offset, GLsizeiptr @size, GLboolean @commit) => glBufferPageCommitmentARBPtr(@target, @offset, @size, @commit);

        internal delegate void glNamedBufferPageCommitmentEXTFunc(GLuint @buffer, GLintptr @offset, GLsizeiptr @size, GLboolean @commit);
        internal static glNamedBufferPageCommitmentEXTFunc glNamedBufferPageCommitmentEXTPtr;
        internal static void loadNamedBufferPageCommitmentEXT()
        {
            try
            {
                glNamedBufferPageCommitmentEXTPtr = (glNamedBufferPageCommitmentEXTFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedBufferPageCommitmentEXT"), typeof(glNamedBufferPageCommitmentEXTFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedBufferPageCommitmentEXT'.");
            }
        }
        public static void glNamedBufferPageCommitmentEXT(GLuint @buffer, GLintptr @offset, GLsizeiptr @size, GLboolean @commit) => glNamedBufferPageCommitmentEXTPtr(@buffer, @offset, @size, @commit);

        internal delegate void glNamedBufferPageCommitmentARBFunc(GLuint @buffer, GLintptr @offset, GLsizeiptr @size, GLboolean @commit);
        internal static glNamedBufferPageCommitmentARBFunc glNamedBufferPageCommitmentARBPtr;
        internal static void loadNamedBufferPageCommitmentARB()
        {
            try
            {
                glNamedBufferPageCommitmentARBPtr = (glNamedBufferPageCommitmentARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedBufferPageCommitmentARB"), typeof(glNamedBufferPageCommitmentARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedBufferPageCommitmentARB'.");
            }
        }
        public static void glNamedBufferPageCommitmentARB(GLuint @buffer, GLintptr @offset, GLsizeiptr @size, GLboolean @commit) => glNamedBufferPageCommitmentARBPtr(@buffer, @offset, @size, @commit);
        #endregion
    }
}
