using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_shader_subroutine
    {
        #region Interop
        static GL_ARB_shader_subroutine()
        {
            Console.WriteLine("Initalising GL_ARB_shader_subroutine interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetSubroutineUniformLocation();
            loadGetSubroutineIndex();
            loadGetActiveSubroutineUniformiv();
            loadGetActiveSubroutineUniformName();
            loadGetActiveSubroutineName();
            loadUniformSubroutinesuiv();
            loadGetUniformSubroutineuiv();
            loadGetProgramStageiv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_ACTIVE_SUBROUTINES = 0x8DE5;
        public static UInt32 GL_ACTIVE_SUBROUTINE_UNIFORMS = 0x8DE6;
        public static UInt32 GL_ACTIVE_SUBROUTINE_UNIFORM_LOCATIONS = 0x8E47;
        public static UInt32 GL_ACTIVE_SUBROUTINE_MAX_LENGTH = 0x8E48;
        public static UInt32 GL_ACTIVE_SUBROUTINE_UNIFORM_MAX_LENGTH = 0x8E49;
        public static UInt32 GL_MAX_SUBROUTINES = 0x8DE7;
        public static UInt32 GL_MAX_SUBROUTINE_UNIFORM_LOCATIONS = 0x8DE8;
        public static UInt32 GL_NUM_COMPATIBLE_SUBROUTINES = 0x8E4A;
        public static UInt32 GL_COMPATIBLE_SUBROUTINES = 0x8E4B;
        public static UInt32 GL_UNIFORM_SIZE = 0x8A38;
        public static UInt32 GL_UNIFORM_NAME_LENGTH = 0x8A39;
        #endregion

        #region Commands
        internal delegate GLint glGetSubroutineUniformLocationFunc(GLuint @program, GLenum @shadertype, const GLchar * @name);
        internal static glGetSubroutineUniformLocationFunc glGetSubroutineUniformLocationPtr;
        internal static void loadGetSubroutineUniformLocation()
        {
            try
            {
                glGetSubroutineUniformLocationPtr = (glGetSubroutineUniformLocationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSubroutineUniformLocation"), typeof(glGetSubroutineUniformLocationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSubroutineUniformLocation'.");
            }
        }
        public static GLint glGetSubroutineUniformLocation(GLuint @program, GLenum @shadertype, const GLchar * @name) => glGetSubroutineUniformLocationPtr(@program, @shadertype, @name);

        internal delegate GLuint glGetSubroutineIndexFunc(GLuint @program, GLenum @shadertype, const GLchar * @name);
        internal static glGetSubroutineIndexFunc glGetSubroutineIndexPtr;
        internal static void loadGetSubroutineIndex()
        {
            try
            {
                glGetSubroutineIndexPtr = (glGetSubroutineIndexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSubroutineIndex"), typeof(glGetSubroutineIndexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSubroutineIndex'.");
            }
        }
        public static GLuint glGetSubroutineIndex(GLuint @program, GLenum @shadertype, const GLchar * @name) => glGetSubroutineIndexPtr(@program, @shadertype, @name);

        internal delegate void glGetActiveSubroutineUniformivFunc(GLuint @program, GLenum @shadertype, GLuint @index, GLenum @pname, GLint * @values);
        internal static glGetActiveSubroutineUniformivFunc glGetActiveSubroutineUniformivPtr;
        internal static void loadGetActiveSubroutineUniformiv()
        {
            try
            {
                glGetActiveSubroutineUniformivPtr = (glGetActiveSubroutineUniformivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveSubroutineUniformiv"), typeof(glGetActiveSubroutineUniformivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveSubroutineUniformiv'.");
            }
        }
        public static void glGetActiveSubroutineUniformiv(GLuint @program, GLenum @shadertype, GLuint @index, GLenum @pname, GLint * @values) => glGetActiveSubroutineUniformivPtr(@program, @shadertype, @index, @pname, @values);

        internal delegate void glGetActiveSubroutineUniformNameFunc(GLuint @program, GLenum @shadertype, GLuint @index, GLsizei @bufsize, GLsizei * @length, GLchar * @name);
        internal static glGetActiveSubroutineUniformNameFunc glGetActiveSubroutineUniformNamePtr;
        internal static void loadGetActiveSubroutineUniformName()
        {
            try
            {
                glGetActiveSubroutineUniformNamePtr = (glGetActiveSubroutineUniformNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveSubroutineUniformName"), typeof(glGetActiveSubroutineUniformNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveSubroutineUniformName'.");
            }
        }
        public static void glGetActiveSubroutineUniformName(GLuint @program, GLenum @shadertype, GLuint @index, GLsizei @bufsize, GLsizei * @length, GLchar * @name) => glGetActiveSubroutineUniformNamePtr(@program, @shadertype, @index, @bufsize, @length, @name);

        internal delegate void glGetActiveSubroutineNameFunc(GLuint @program, GLenum @shadertype, GLuint @index, GLsizei @bufsize, GLsizei * @length, GLchar * @name);
        internal static glGetActiveSubroutineNameFunc glGetActiveSubroutineNamePtr;
        internal static void loadGetActiveSubroutineName()
        {
            try
            {
                glGetActiveSubroutineNamePtr = (glGetActiveSubroutineNameFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetActiveSubroutineName"), typeof(glGetActiveSubroutineNameFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetActiveSubroutineName'.");
            }
        }
        public static void glGetActiveSubroutineName(GLuint @program, GLenum @shadertype, GLuint @index, GLsizei @bufsize, GLsizei * @length, GLchar * @name) => glGetActiveSubroutineNamePtr(@program, @shadertype, @index, @bufsize, @length, @name);

        internal delegate void glUniformSubroutinesuivFunc(GLenum @shadertype, GLsizei @count, const GLuint * @indices);
        internal static glUniformSubroutinesuivFunc glUniformSubroutinesuivPtr;
        internal static void loadUniformSubroutinesuiv()
        {
            try
            {
                glUniformSubroutinesuivPtr = (glUniformSubroutinesuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glUniformSubroutinesuiv"), typeof(glUniformSubroutinesuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glUniformSubroutinesuiv'.");
            }
        }
        public static void glUniformSubroutinesuiv(GLenum @shadertype, GLsizei @count, const GLuint * @indices) => glUniformSubroutinesuivPtr(@shadertype, @count, @indices);

        internal delegate void glGetUniformSubroutineuivFunc(GLenum @shadertype, GLint @location, GLuint * @params);
        internal static glGetUniformSubroutineuivFunc glGetUniformSubroutineuivPtr;
        internal static void loadGetUniformSubroutineuiv()
        {
            try
            {
                glGetUniformSubroutineuivPtr = (glGetUniformSubroutineuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetUniformSubroutineuiv"), typeof(glGetUniformSubroutineuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetUniformSubroutineuiv'.");
            }
        }
        public static void glGetUniformSubroutineuiv(GLenum @shadertype, GLint @location, GLuint * @params) => glGetUniformSubroutineuivPtr(@shadertype, @location, @params);

        internal delegate void glGetProgramStageivFunc(GLuint @program, GLenum @shadertype, GLenum @pname, GLint * @values);
        internal static glGetProgramStageivFunc glGetProgramStageivPtr;
        internal static void loadGetProgramStageiv()
        {
            try
            {
                glGetProgramStageivPtr = (glGetProgramStageivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramStageiv"), typeof(glGetProgramStageivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramStageiv'.");
            }
        }
        public static void glGetProgramStageiv(GLuint @program, GLenum @shadertype, GLenum @pname, GLint * @values) => glGetProgramStageivPtr(@program, @shadertype, @pname, @values);
        #endregion
    }
}
