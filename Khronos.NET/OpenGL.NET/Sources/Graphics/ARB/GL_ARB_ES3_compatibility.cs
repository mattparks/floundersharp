using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_ES3_compatibility
    {
        #region Interop
        static GL_ARB_ES3_compatibility()
        {
            Console.WriteLine("Initalising GL_ARB_ES3_compatibility interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_RGB8_ETC2 = 0x9274;
        public static UInt32 GL_COMPRESSED_SRGB8_ETC2 = 0x9275;
        public static UInt32 GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2 = 0x9276;
        public static UInt32 GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2 = 0x9277;
        public static UInt32 GL_COMPRESSED_RGBA8_ETC2_EAC = 0x9278;
        public static UInt32 GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC = 0x9279;
        public static UInt32 GL_COMPRESSED_R11_EAC = 0x9270;
        public static UInt32 GL_COMPRESSED_SIGNED_R11_EAC = 0x9271;
        public static UInt32 GL_COMPRESSED_RG11_EAC = 0x9272;
        public static UInt32 GL_COMPRESSED_SIGNED_RG11_EAC = 0x9273;
        public static UInt32 GL_PRIMITIVE_RESTART_FIXED_INDEX = 0x8D69;
        public static UInt32 GL_ANY_SAMPLES_PASSED_CONSERVATIVE = 0x8D6A;
        public static UInt32 GL_MAX_ELEMENT_INDEX = 0x8D6B;
        #endregion

        #region Commands
        #endregion
    }
}
