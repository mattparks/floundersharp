using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_tessellation_shader
    {
        #region Interop
        static GL_ARB_tessellation_shader()
        {
            Console.WriteLine("Initalising GL_ARB_tessellation_shader interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPatchParameteri();
            loadPatchParameterfv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_PATCHES = 0x000E;
        public static UInt32 GL_PATCH_VERTICES = 0x8E72;
        public static UInt32 GL_PATCH_DEFAULT_INNER_LEVEL = 0x8E73;
        public static UInt32 GL_PATCH_DEFAULT_OUTER_LEVEL = 0x8E74;
        public static UInt32 GL_TESS_CONTROL_OUTPUT_VERTICES = 0x8E75;
        public static UInt32 GL_TESS_GEN_MODE = 0x8E76;
        public static UInt32 GL_TESS_GEN_SPACING = 0x8E77;
        public static UInt32 GL_TESS_GEN_VERTEX_ORDER = 0x8E78;
        public static UInt32 GL_TESS_GEN_POINT_MODE = 0x8E79;
        public static UInt32 GL_TRIANGLES = 0x0004;
        public static UInt32 GL_ISOLINES = 0x8E7A;
        public static UInt32 GL_QUADS = 0x0007;
        public static UInt32 GL_EQUAL = 0x0202;
        public static UInt32 GL_FRACTIONAL_ODD = 0x8E7B;
        public static UInt32 GL_FRACTIONAL_EVEN = 0x8E7C;
        public static UInt32 GL_CCW = 0x0901;
        public static UInt32 GL_CW = 0x0900;
        public static UInt32 GL_MAX_PATCH_VERTICES = 0x8E7D;
        public static UInt32 GL_MAX_TESS_GEN_LEVEL = 0x8E7E;
        public static UInt32 GL_MAX_TESS_CONTROL_UNIFORM_COMPONENTS = 0x8E7F;
        public static UInt32 GL_MAX_TESS_EVALUATION_UNIFORM_COMPONENTS = 0x8E80;
        public static UInt32 GL_MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS = 0x8E81;
        public static UInt32 GL_MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS = 0x8E82;
        public static UInt32 GL_MAX_TESS_CONTROL_OUTPUT_COMPONENTS = 0x8E83;
        public static UInt32 GL_MAX_TESS_PATCH_COMPONENTS = 0x8E84;
        public static UInt32 GL_MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS = 0x8E85;
        public static UInt32 GL_MAX_TESS_EVALUATION_OUTPUT_COMPONENTS = 0x8E86;
        public static UInt32 GL_MAX_TESS_CONTROL_UNIFORM_BLOCKS = 0x8E89;
        public static UInt32 GL_MAX_TESS_EVALUATION_UNIFORM_BLOCKS = 0x8E8A;
        public static UInt32 GL_MAX_TESS_CONTROL_INPUT_COMPONENTS = 0x886C;
        public static UInt32 GL_MAX_TESS_EVALUATION_INPUT_COMPONENTS = 0x886D;
        public static UInt32 GL_MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS = 0x8E1E;
        public static UInt32 GL_MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS = 0x8E1F;
        public static UInt32 GL_UNIFORM_BLOCK_REFERENCED_BY_TESS_CONTROL_SHADER = 0x84F0;
        public static UInt32 GL_UNIFORM_BLOCK_REFERENCED_BY_TESS_EVALUATION_SHADER = 0x84F1;
        public static UInt32 GL_TESS_EVALUATION_SHADER = 0x8E87;
        public static UInt32 GL_TESS_CONTROL_SHADER = 0x8E88;
        #endregion

        #region Commands
        internal delegate void glPatchParameteriFunc(GLenum @pname, GLint @value);
        internal static glPatchParameteriFunc glPatchParameteriPtr;
        internal static void loadPatchParameteri()
        {
            try
            {
                glPatchParameteriPtr = (glPatchParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPatchParameteri"), typeof(glPatchParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPatchParameteri'.");
            }
        }
        public static void glPatchParameteri(GLenum @pname, GLint @value) => glPatchParameteriPtr(@pname, @value);

        internal delegate void glPatchParameterfvFunc(GLenum @pname, const GLfloat * @values);
        internal static glPatchParameterfvFunc glPatchParameterfvPtr;
        internal static void loadPatchParameterfv()
        {
            try
            {
                glPatchParameterfvPtr = (glPatchParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPatchParameterfv"), typeof(glPatchParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPatchParameterfv'.");
            }
        }
        public static void glPatchParameterfv(GLenum @pname, const GLfloat * @values) => glPatchParameterfvPtr(@pname, @values);
        #endregion
    }
}
