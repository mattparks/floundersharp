using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_point_parameters
    {
        #region Interop
        static GL_ARB_point_parameters()
        {
            Console.WriteLine("Initalising GL_ARB_point_parameters interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadPointParameterfARB();
            loadPointParameterfvARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_POINT_SIZE_MIN_ARB = 0x8126;
        public static UInt32 GL_POINT_SIZE_MAX_ARB = 0x8127;
        public static UInt32 GL_POINT_FADE_THRESHOLD_SIZE_ARB = 0x8128;
        public static UInt32 GL_POINT_DISTANCE_ATTENUATION_ARB = 0x8129;
        #endregion

        #region Commands
        internal delegate void glPointParameterfARBFunc(GLenum @pname, GLfloat @param);
        internal static glPointParameterfARBFunc glPointParameterfARBPtr;
        internal static void loadPointParameterfARB()
        {
            try
            {
                glPointParameterfARBPtr = (glPointParameterfARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterfARB"), typeof(glPointParameterfARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterfARB'.");
            }
        }
        public static void glPointParameterfARB(GLenum @pname, GLfloat @param) => glPointParameterfARBPtr(@pname, @param);

        internal delegate void glPointParameterfvARBFunc(GLenum @pname, const GLfloat * @params);
        internal static glPointParameterfvARBFunc glPointParameterfvARBPtr;
        internal static void loadPointParameterfvARB()
        {
            try
            {
                glPointParameterfvARBPtr = (glPointParameterfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterfvARB"), typeof(glPointParameterfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterfvARB'.");
            }
        }
        public static void glPointParameterfvARB(GLenum @pname, const GLfloat * @params) => glPointParameterfvARBPtr(@pname, @params);
        #endregion
    }
}
