using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_enhanced_layouts
    {
        #region Interop
        static GL_ARB_enhanced_layouts()
        {
            Console.WriteLine("Initalising GL_ARB_enhanced_layouts interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_LOCATION_COMPONENT = 0x934A;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER = 0x8C8E;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_INDEX = 0x934B;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_STRIDE = 0x934C;
        #endregion

        #region Commands
        #endregion
    }
}
