using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_buffer_storage
    {
        #region Interop
        static GL_ARB_buffer_storage()
        {
            Console.WriteLine("Initalising GL_ARB_buffer_storage interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBufferStorage();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAP_READ_BIT = 0x0001;
        public static UInt32 GL_MAP_WRITE_BIT = 0x0002;
        public static UInt32 GL_MAP_PERSISTENT_BIT = 0x0040;
        public static UInt32 GL_MAP_COHERENT_BIT = 0x0080;
        public static UInt32 GL_DYNAMIC_STORAGE_BIT = 0x0100;
        public static UInt32 GL_CLIENT_STORAGE_BIT = 0x0200;
        public static UInt32 GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT = 0x00004000;
        public static UInt32 GL_BUFFER_IMMUTABLE_STORAGE = 0x821F;
        public static UInt32 GL_BUFFER_STORAGE_FLAGS = 0x8220;
        #endregion

        #region Commands
        internal delegate void glBufferStorageFunc(GLenum @target, GLsizeiptr @size, const void * @data, GLbitfield @flags);
        internal static glBufferStorageFunc glBufferStoragePtr;
        internal static void loadBufferStorage()
        {
            try
            {
                glBufferStoragePtr = (glBufferStorageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferStorage"), typeof(glBufferStorageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferStorage'.");
            }
        }
        public static void glBufferStorage(GLenum @target, GLsizeiptr @size, const void * @data, GLbitfield @flags) => glBufferStoragePtr(@target, @size, @data, @flags);
        #endregion
    }
}
