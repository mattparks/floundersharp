using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_fragment_layer_viewport
    {
        #region Interop
        static GL_ARB_fragment_layer_viewport()
        {
            Console.WriteLine("Initalising GL_ARB_fragment_layer_viewport interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        #endregion
    }
}
