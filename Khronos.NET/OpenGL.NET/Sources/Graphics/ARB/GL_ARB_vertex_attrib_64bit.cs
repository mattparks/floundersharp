using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_vertex_attrib_64bit
    {
        #region Interop
        static GL_ARB_vertex_attrib_64bit()
        {
            Console.WriteLine("Initalising GL_ARB_vertex_attrib_64bit interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexAttribL1d();
            loadVertexAttribL2d();
            loadVertexAttribL3d();
            loadVertexAttribL4d();
            loadVertexAttribL1dv();
            loadVertexAttribL2dv();
            loadVertexAttribL3dv();
            loadVertexAttribL4dv();
            loadVertexAttribLPointer();
            loadGetVertexAttribLdv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_RGB32I = 0x8D83;
        public static UInt32 GL_DOUBLE_VEC2 = 0x8FFC;
        public static UInt32 GL_DOUBLE_VEC3 = 0x8FFD;
        public static UInt32 GL_DOUBLE_VEC4 = 0x8FFE;
        public static UInt32 GL_DOUBLE_MAT2 = 0x8F46;
        public static UInt32 GL_DOUBLE_MAT3 = 0x8F47;
        public static UInt32 GL_DOUBLE_MAT4 = 0x8F48;
        public static UInt32 GL_DOUBLE_MAT2x3 = 0x8F49;
        public static UInt32 GL_DOUBLE_MAT2x4 = 0x8F4A;
        public static UInt32 GL_DOUBLE_MAT3x2 = 0x8F4B;
        public static UInt32 GL_DOUBLE_MAT3x4 = 0x8F4C;
        public static UInt32 GL_DOUBLE_MAT4x2 = 0x8F4D;
        public static UInt32 GL_DOUBLE_MAT4x3 = 0x8F4E;
        #endregion

        #region Commands
        internal delegate void glVertexAttribL1dFunc(GLuint @index, GLdouble @x);
        internal static glVertexAttribL1dFunc glVertexAttribL1dPtr;
        internal static void loadVertexAttribL1d()
        {
            try
            {
                glVertexAttribL1dPtr = (glVertexAttribL1dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL1d"), typeof(glVertexAttribL1dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL1d'.");
            }
        }
        public static void glVertexAttribL1d(GLuint @index, GLdouble @x) => glVertexAttribL1dPtr(@index, @x);

        internal delegate void glVertexAttribL2dFunc(GLuint @index, GLdouble @x, GLdouble @y);
        internal static glVertexAttribL2dFunc glVertexAttribL2dPtr;
        internal static void loadVertexAttribL2d()
        {
            try
            {
                glVertexAttribL2dPtr = (glVertexAttribL2dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL2d"), typeof(glVertexAttribL2dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL2d'.");
            }
        }
        public static void glVertexAttribL2d(GLuint @index, GLdouble @x, GLdouble @y) => glVertexAttribL2dPtr(@index, @x, @y);

        internal delegate void glVertexAttribL3dFunc(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glVertexAttribL3dFunc glVertexAttribL3dPtr;
        internal static void loadVertexAttribL3d()
        {
            try
            {
                glVertexAttribL3dPtr = (glVertexAttribL3dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL3d"), typeof(glVertexAttribL3dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL3d'.");
            }
        }
        public static void glVertexAttribL3d(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z) => glVertexAttribL3dPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttribL4dFunc(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glVertexAttribL4dFunc glVertexAttribL4dPtr;
        internal static void loadVertexAttribL4d()
        {
            try
            {
                glVertexAttribL4dPtr = (glVertexAttribL4dFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL4d"), typeof(glVertexAttribL4dFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL4d'.");
            }
        }
        public static void glVertexAttribL4d(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glVertexAttribL4dPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttribL1dvFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttribL1dvFunc glVertexAttribL1dvPtr;
        internal static void loadVertexAttribL1dv()
        {
            try
            {
                glVertexAttribL1dvPtr = (glVertexAttribL1dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL1dv"), typeof(glVertexAttribL1dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL1dv'.");
            }
        }
        public static void glVertexAttribL1dv(GLuint @index, const GLdouble * @v) => glVertexAttribL1dvPtr(@index, @v);

        internal delegate void glVertexAttribL2dvFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttribL2dvFunc glVertexAttribL2dvPtr;
        internal static void loadVertexAttribL2dv()
        {
            try
            {
                glVertexAttribL2dvPtr = (glVertexAttribL2dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL2dv"), typeof(glVertexAttribL2dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL2dv'.");
            }
        }
        public static void glVertexAttribL2dv(GLuint @index, const GLdouble * @v) => glVertexAttribL2dvPtr(@index, @v);

        internal delegate void glVertexAttribL3dvFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttribL3dvFunc glVertexAttribL3dvPtr;
        internal static void loadVertexAttribL3dv()
        {
            try
            {
                glVertexAttribL3dvPtr = (glVertexAttribL3dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL3dv"), typeof(glVertexAttribL3dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL3dv'.");
            }
        }
        public static void glVertexAttribL3dv(GLuint @index, const GLdouble * @v) => glVertexAttribL3dvPtr(@index, @v);

        internal delegate void glVertexAttribL4dvFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttribL4dvFunc glVertexAttribL4dvPtr;
        internal static void loadVertexAttribL4dv()
        {
            try
            {
                glVertexAttribL4dvPtr = (glVertexAttribL4dvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribL4dv"), typeof(glVertexAttribL4dvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribL4dv'.");
            }
        }
        public static void glVertexAttribL4dv(GLuint @index, const GLdouble * @v) => glVertexAttribL4dvPtr(@index, @v);

        internal delegate void glVertexAttribLPointerFunc(GLuint @index, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glVertexAttribLPointerFunc glVertexAttribLPointerPtr;
        internal static void loadVertexAttribLPointer()
        {
            try
            {
                glVertexAttribLPointerPtr = (glVertexAttribLPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribLPointer"), typeof(glVertexAttribLPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribLPointer'.");
            }
        }
        public static void glVertexAttribLPointer(GLuint @index, GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glVertexAttribLPointerPtr(@index, @size, @type, @stride, @pointer);

        internal delegate void glGetVertexAttribLdvFunc(GLuint @index, GLenum @pname, GLdouble * @params);
        internal static glGetVertexAttribLdvFunc glGetVertexAttribLdvPtr;
        internal static void loadGetVertexAttribLdv()
        {
            try
            {
                glGetVertexAttribLdvPtr = (glGetVertexAttribLdvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribLdv"), typeof(glGetVertexAttribLdvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribLdv'.");
            }
        }
        public static void glGetVertexAttribLdv(GLuint @index, GLenum @pname, GLdouble * @params) => glGetVertexAttribLdvPtr(@index, @pname, @params);
        #endregion
    }
}
