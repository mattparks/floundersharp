using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_mirror_clamp_to_edge
    {
        #region Interop
        static GL_ARB_texture_mirror_clamp_to_edge()
        {
            Console.WriteLine("Initalising GL_ARB_texture_mirror_clamp_to_edge interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_MIRROR_CLAMP_TO_EDGE = 0x8743;
        #endregion

        #region Commands
        #endregion
    }
}
