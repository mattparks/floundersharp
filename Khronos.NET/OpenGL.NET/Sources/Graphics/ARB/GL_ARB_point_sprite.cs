using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_point_sprite
    {
        #region Interop
        static GL_ARB_point_sprite()
        {
            Console.WriteLine("Initalising GL_ARB_point_sprite interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_POINT_SPRITE_ARB = 0x8861;
        public static UInt32 GL_COORD_REPLACE_ARB = 0x8862;
        #endregion

        #region Commands
        #endregion
    }
}
