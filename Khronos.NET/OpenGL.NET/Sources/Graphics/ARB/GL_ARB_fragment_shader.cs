using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_fragment_shader
    {
        #region Interop
        static GL_ARB_fragment_shader()
        {
            Console.WriteLine("Initalising GL_ARB_fragment_shader interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAGMENT_SHADER_ARB = 0x8B30;
        public static UInt32 GL_MAX_FRAGMENT_UNIFORM_COMPONENTS_ARB = 0x8B49;
        public static UInt32 GL_FRAGMENT_SHADER_DERIVATIVE_HINT_ARB = 0x8B8B;
        #endregion

        #region Commands
        #endregion
    }
}
