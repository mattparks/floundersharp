using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_draw_indirect
    {
        #region Interop
        static GL_ARB_draw_indirect()
        {
            Console.WriteLine("Initalising GL_ARB_draw_indirect interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawArraysIndirect();
            loadDrawElementsIndirect();
        }
        #endregion

        #region Enums
        public static UInt32 GL_DRAW_INDIRECT_BUFFER = 0x8F3F;
        public static UInt32 GL_DRAW_INDIRECT_BUFFER_BINDING = 0x8F43;
        #endregion

        #region Commands
        internal delegate void glDrawArraysIndirectFunc(GLenum @mode, const void * @indirect);
        internal static glDrawArraysIndirectFunc glDrawArraysIndirectPtr;
        internal static void loadDrawArraysIndirect()
        {
            try
            {
                glDrawArraysIndirectPtr = (glDrawArraysIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArraysIndirect"), typeof(glDrawArraysIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArraysIndirect'.");
            }
        }
        public static void glDrawArraysIndirect(GLenum @mode, const void * @indirect) => glDrawArraysIndirectPtr(@mode, @indirect);

        internal delegate void glDrawElementsIndirectFunc(GLenum @mode, GLenum @type, const void * @indirect);
        internal static glDrawElementsIndirectFunc glDrawElementsIndirectPtr;
        internal static void loadDrawElementsIndirect()
        {
            try
            {
                glDrawElementsIndirectPtr = (glDrawElementsIndirectFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElementsIndirect"), typeof(glDrawElementsIndirectFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElementsIndirect'.");
            }
        }
        public static void glDrawElementsIndirect(GLenum @mode, GLenum @type, const void * @indirect) => glDrawElementsIndirectPtr(@mode, @type, @indirect);
        #endregion
    }
}
