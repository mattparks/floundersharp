using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_depth_buffer_float
    {
        #region Interop
        static GL_ARB_depth_buffer_float()
        {
            Console.WriteLine("Initalising GL_ARB_depth_buffer_float interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_COMPONENT32F = 0x8CAC;
        public static UInt32 GL_DEPTH32F_STENCIL8 = 0x8CAD;
        public static UInt32 GL_FLOAT_32_UNSIGNED_INT_24_8_REV = 0x8DAD;
        #endregion

        #region Commands
        #endregion
    }
}
