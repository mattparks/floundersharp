using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_parallel_shader_compile
    {
        #region Interop
        static GL_ARB_parallel_shader_compile()
        {
            Console.WriteLine("Initalising GL_ARB_parallel_shader_compile interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadMaxShaderCompilerThreadsARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_SHADER_COMPILER_THREADS_ARB = 0x91B0;
        public static UInt32 GL_COMPLETION_STATUS_ARB = 0x91B1;
        #endregion

        #region Commands
        internal delegate void glMaxShaderCompilerThreadsARBFunc(GLuint @count);
        internal static glMaxShaderCompilerThreadsARBFunc glMaxShaderCompilerThreadsARBPtr;
        internal static void loadMaxShaderCompilerThreadsARB()
        {
            try
            {
                glMaxShaderCompilerThreadsARBPtr = (glMaxShaderCompilerThreadsARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMaxShaderCompilerThreadsARB"), typeof(glMaxShaderCompilerThreadsARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMaxShaderCompilerThreadsARB'.");
            }
        }
        public static void glMaxShaderCompilerThreadsARB(GLuint @count) => glMaxShaderCompilerThreadsARBPtr(@count);
        #endregion
    }
}
