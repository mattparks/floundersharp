using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_get_texture_sub_image
    {
        #region Interop
        static GL_ARB_get_texture_sub_image()
        {
            Console.WriteLine("Initalising GL_ARB_get_texture_sub_image interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGetTextureSubImage();
            loadGetCompressedTextureSubImage();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glGetTextureSubImageFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, GLsizei @bufSize, void * @pixels);
        internal static glGetTextureSubImageFunc glGetTextureSubImagePtr;
        internal static void loadGetTextureSubImage()
        {
            try
            {
                glGetTextureSubImagePtr = (glGetTextureSubImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTextureSubImage"), typeof(glGetTextureSubImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTextureSubImage'.");
            }
        }
        public static void glGetTextureSubImage(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLenum @type, GLsizei @bufSize, void * @pixels) => glGetTextureSubImagePtr(@texture, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @type, @bufSize, @pixels);

        internal delegate void glGetCompressedTextureSubImageFunc(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLsizei @bufSize, void * @pixels);
        internal static glGetCompressedTextureSubImageFunc glGetCompressedTextureSubImagePtr;
        internal static void loadGetCompressedTextureSubImage()
        {
            try
            {
                glGetCompressedTextureSubImagePtr = (glGetCompressedTextureSubImageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCompressedTextureSubImage"), typeof(glGetCompressedTextureSubImageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCompressedTextureSubImage'.");
            }
        }
        public static void glGetCompressedTextureSubImage(GLuint @texture, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLsizei @bufSize, void * @pixels) => glGetCompressedTextureSubImagePtr(@texture, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @bufSize, @pixels);
        #endregion
    }
}
