using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_conditional_render_inverted
    {
        #region Interop
        static GL_ARB_conditional_render_inverted()
        {
            Console.WriteLine("Initalising GL_ARB_conditional_render_inverted interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_QUERY_WAIT_INVERTED = 0x8E17;
        public static UInt32 GL_QUERY_NO_WAIT_INVERTED = 0x8E18;
        public static UInt32 GL_QUERY_BY_REGION_WAIT_INVERTED = 0x8E19;
        public static UInt32 GL_QUERY_BY_REGION_NO_WAIT_INVERTED = 0x8E1A;
        #endregion

        #region Commands
        #endregion
    }
}
