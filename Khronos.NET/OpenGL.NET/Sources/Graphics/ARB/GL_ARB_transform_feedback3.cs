using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_transform_feedback3
    {
        #region Interop
        static GL_ARB_transform_feedback3()
        {
            Console.WriteLine("Initalising GL_ARB_transform_feedback3 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDrawTransformFeedbackStream();
            loadBeginQueryIndexed();
            loadEndQueryIndexed();
            loadGetQueryIndexediv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_TRANSFORM_FEEDBACK_BUFFERS = 0x8E70;
        public static UInt32 GL_MAX_VERTEX_STREAMS = 0x8E71;
        #endregion

        #region Commands
        internal delegate void glDrawTransformFeedbackStreamFunc(GLenum @mode, GLuint @id, GLuint @stream);
        internal static glDrawTransformFeedbackStreamFunc glDrawTransformFeedbackStreamPtr;
        internal static void loadDrawTransformFeedbackStream()
        {
            try
            {
                glDrawTransformFeedbackStreamPtr = (glDrawTransformFeedbackStreamFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTransformFeedbackStream"), typeof(glDrawTransformFeedbackStreamFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTransformFeedbackStream'.");
            }
        }
        public static void glDrawTransformFeedbackStream(GLenum @mode, GLuint @id, GLuint @stream) => glDrawTransformFeedbackStreamPtr(@mode, @id, @stream);

        internal delegate void glBeginQueryIndexedFunc(GLenum @target, GLuint @index, GLuint @id);
        internal static glBeginQueryIndexedFunc glBeginQueryIndexedPtr;
        internal static void loadBeginQueryIndexed()
        {
            try
            {
                glBeginQueryIndexedPtr = (glBeginQueryIndexedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBeginQueryIndexed"), typeof(glBeginQueryIndexedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBeginQueryIndexed'.");
            }
        }
        public static void glBeginQueryIndexed(GLenum @target, GLuint @index, GLuint @id) => glBeginQueryIndexedPtr(@target, @index, @id);

        internal delegate void glEndQueryIndexedFunc(GLenum @target, GLuint @index);
        internal static glEndQueryIndexedFunc glEndQueryIndexedPtr;
        internal static void loadEndQueryIndexed()
        {
            try
            {
                glEndQueryIndexedPtr = (glEndQueryIndexedFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEndQueryIndexed"), typeof(glEndQueryIndexedFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEndQueryIndexed'.");
            }
        }
        public static void glEndQueryIndexed(GLenum @target, GLuint @index) => glEndQueryIndexedPtr(@target, @index);

        internal delegate void glGetQueryIndexedivFunc(GLenum @target, GLuint @index, GLenum @pname, GLint * @params);
        internal static glGetQueryIndexedivFunc glGetQueryIndexedivPtr;
        internal static void loadGetQueryIndexediv()
        {
            try
            {
                glGetQueryIndexedivPtr = (glGetQueryIndexedivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetQueryIndexediv"), typeof(glGetQueryIndexedivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetQueryIndexediv'.");
            }
        }
        public static void glGetQueryIndexediv(GLenum @target, GLuint @index, GLenum @pname, GLint * @params) => glGetQueryIndexedivPtr(@target, @index, @pname, @params);
        #endregion
    }
}
