using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_float
    {
        #region Interop
        static GL_ARB_texture_float()
        {
            Console.WriteLine("Initalising GL_ARB_texture_float interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_RED_TYPE_ARB = 0x8C10;
        public static UInt32 GL_TEXTURE_GREEN_TYPE_ARB = 0x8C11;
        public static UInt32 GL_TEXTURE_BLUE_TYPE_ARB = 0x8C12;
        public static UInt32 GL_TEXTURE_ALPHA_TYPE_ARB = 0x8C13;
        public static UInt32 GL_TEXTURE_LUMINANCE_TYPE_ARB = 0x8C14;
        public static UInt32 GL_TEXTURE_INTENSITY_TYPE_ARB = 0x8C15;
        public static UInt32 GL_TEXTURE_DEPTH_TYPE_ARB = 0x8C16;
        public static UInt32 GL_UNSIGNED_NORMALIZED_ARB = 0x8C17;
        public static UInt32 GL_RGBA32F_ARB = 0x8814;
        public static UInt32 GL_RGB32F_ARB = 0x8815;
        public static UInt32 GL_ALPHA32F_ARB = 0x8816;
        public static UInt32 GL_INTENSITY32F_ARB = 0x8817;
        public static UInt32 GL_LUMINANCE32F_ARB = 0x8818;
        public static UInt32 GL_LUMINANCE_ALPHA32F_ARB = 0x8819;
        public static UInt32 GL_RGBA16F_ARB = 0x881A;
        public static UInt32 GL_RGB16F_ARB = 0x881B;
        public static UInt32 GL_ALPHA16F_ARB = 0x881C;
        public static UInt32 GL_INTENSITY16F_ARB = 0x881D;
        public static UInt32 GL_LUMINANCE16F_ARB = 0x881E;
        public static UInt32 GL_LUMINANCE_ALPHA16F_ARB = 0x881F;
        #endregion

        #region Commands
        #endregion
    }
}
