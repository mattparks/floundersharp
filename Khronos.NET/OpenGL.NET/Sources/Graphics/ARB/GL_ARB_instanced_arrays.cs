using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_instanced_arrays
    {
        #region Interop
        static GL_ARB_instanced_arrays()
        {
            Console.WriteLine("Initalising GL_ARB_instanced_arrays interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexAttribDivisorARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_DIVISOR_ARB = 0x88FE;
        #endregion

        #region Commands
        internal delegate void glVertexAttribDivisorARBFunc(GLuint @index, GLuint @divisor);
        internal static glVertexAttribDivisorARBFunc glVertexAttribDivisorARBPtr;
        internal static void loadVertexAttribDivisorARB()
        {
            try
            {
                glVertexAttribDivisorARBPtr = (glVertexAttribDivisorARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribDivisorARB"), typeof(glVertexAttribDivisorARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribDivisorARB'.");
            }
        }
        public static void glVertexAttribDivisorARB(GLuint @index, GLuint @divisor) => glVertexAttribDivisorARBPtr(@index, @divisor);
        #endregion
    }
}
