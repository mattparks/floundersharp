using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_multisample
    {
        #region Interop
        static GL_ARB_texture_multisample()
        {
            Console.WriteLine("Initalising GL_ARB_texture_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadTexImage2DMultisample();
            loadTexImage3DMultisample();
            loadGetMultisamplefv();
            loadSampleMaski();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SAMPLE_POSITION = 0x8E50;
        public static UInt32 GL_SAMPLE_MASK = 0x8E51;
        public static UInt32 GL_SAMPLE_MASK_VALUE = 0x8E52;
        public static UInt32 GL_MAX_SAMPLE_MASK_WORDS = 0x8E59;
        public static UInt32 GL_TEXTURE_2D_MULTISAMPLE = 0x9100;
        public static UInt32 GL_PROXY_TEXTURE_2D_MULTISAMPLE = 0x9101;
        public static UInt32 GL_TEXTURE_2D_MULTISAMPLE_ARRAY = 0x9102;
        public static UInt32 GL_PROXY_TEXTURE_2D_MULTISAMPLE_ARRAY = 0x9103;
        public static UInt32 GL_TEXTURE_BINDING_2D_MULTISAMPLE = 0x9104;
        public static UInt32 GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY = 0x9105;
        public static UInt32 GL_TEXTURE_SAMPLES = 0x9106;
        public static UInt32 GL_TEXTURE_FIXED_SAMPLE_LOCATIONS = 0x9107;
        public static UInt32 GL_SAMPLER_2D_MULTISAMPLE = 0x9108;
        public static UInt32 GL_INT_SAMPLER_2D_MULTISAMPLE = 0x9109;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE = 0x910A;
        public static UInt32 GL_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910B;
        public static UInt32 GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910C;
        public static UInt32 GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910D;
        public static UInt32 GL_MAX_COLOR_TEXTURE_SAMPLES = 0x910E;
        public static UInt32 GL_MAX_DEPTH_TEXTURE_SAMPLES = 0x910F;
        public static UInt32 GL_MAX_INTEGER_SAMPLES = 0x9110;
        #endregion

        #region Commands
        internal delegate void glTexImage2DMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations);
        internal static glTexImage2DMultisampleFunc glTexImage2DMultisamplePtr;
        internal static void loadTexImage2DMultisample()
        {
            try
            {
                glTexImage2DMultisamplePtr = (glTexImage2DMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage2DMultisample"), typeof(glTexImage2DMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage2DMultisample'.");
            }
        }
        public static void glTexImage2DMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLboolean @fixedsamplelocations) => glTexImage2DMultisamplePtr(@target, @samples, @internalformat, @width, @height, @fixedsamplelocations);

        internal delegate void glTexImage3DMultisampleFunc(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations);
        internal static glTexImage3DMultisampleFunc glTexImage3DMultisamplePtr;
        internal static void loadTexImage3DMultisample()
        {
            try
            {
                glTexImage3DMultisamplePtr = (glTexImage3DMultisampleFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage3DMultisample"), typeof(glTexImage3DMultisampleFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage3DMultisample'.");
            }
        }
        public static void glTexImage3DMultisample(GLenum @target, GLsizei @samples, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLboolean @fixedsamplelocations) => glTexImage3DMultisamplePtr(@target, @samples, @internalformat, @width, @height, @depth, @fixedsamplelocations);

        internal delegate void glGetMultisamplefvFunc(GLenum @pname, GLuint @index, GLfloat * @val);
        internal static glGetMultisamplefvFunc glGetMultisamplefvPtr;
        internal static void loadGetMultisamplefv()
        {
            try
            {
                glGetMultisamplefvPtr = (glGetMultisamplefvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMultisamplefv"), typeof(glGetMultisamplefvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMultisamplefv'.");
            }
        }
        public static void glGetMultisamplefv(GLenum @pname, GLuint @index, GLfloat * @val) => glGetMultisamplefvPtr(@pname, @index, @val);

        internal delegate void glSampleMaskiFunc(GLuint @maskNumber, GLbitfield @mask);
        internal static glSampleMaskiFunc glSampleMaskiPtr;
        internal static void loadSampleMaski()
        {
            try
            {
                glSampleMaskiPtr = (glSampleMaskiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleMaski"), typeof(glSampleMaskiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleMaski'.");
            }
        }
        public static void glSampleMaski(GLuint @maskNumber, GLbitfield @mask) => glSampleMaskiPtr(@maskNumber, @mask);
        #endregion
    }
}
