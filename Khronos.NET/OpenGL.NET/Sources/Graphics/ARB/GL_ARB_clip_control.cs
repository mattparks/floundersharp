using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_clip_control
    {
        #region Interop
        static GL_ARB_clip_control()
        {
            Console.WriteLine("Initalising GL_ARB_clip_control interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadClipControl();
        }
        #endregion

        #region Enums
        public static UInt32 GL_LOWER_LEFT = 0x8CA1;
        public static UInt32 GL_UPPER_LEFT = 0x8CA2;
        public static UInt32 GL_NEGATIVE_ONE_TO_ONE = 0x935E;
        public static UInt32 GL_ZERO_TO_ONE = 0x935F;
        public static UInt32 GL_CLIP_ORIGIN = 0x935C;
        public static UInt32 GL_CLIP_DEPTH_MODE = 0x935D;
        #endregion

        #region Commands
        internal delegate void glClipControlFunc(GLenum @origin, GLenum @depth);
        internal static glClipControlFunc glClipControlPtr;
        internal static void loadClipControl()
        {
            try
            {
                glClipControlPtr = (glClipControlFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClipControl"), typeof(glClipControlFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClipControl'.");
            }
        }
        public static void glClipControl(GLenum @origin, GLenum @depth) => glClipControlPtr(@origin, @depth);
        #endregion
    }
}
