using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_filter_minmax
    {
        #region Interop
        static GL_ARB_texture_filter_minmax()
        {
            Console.WriteLine("Initalising GL_ARB_texture_filter_minmax interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_REDUCTION_MODE_ARB = 0x9366;
        public static UInt32 GL_WEIGHTED_AVERAGE_ARB = 0x9367;
        #endregion

        #region Commands
        #endregion
    }
}
