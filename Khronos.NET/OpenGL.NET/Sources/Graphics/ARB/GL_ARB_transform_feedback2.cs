using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_transform_feedback2
    {
        #region Interop
        static GL_ARB_transform_feedback2()
        {
            Console.WriteLine("Initalising GL_ARB_transform_feedback2 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBindTransformFeedback();
            loadDeleteTransformFeedbacks();
            loadGenTransformFeedbacks();
            loadIsTransformFeedback();
            loadPauseTransformFeedback();
            loadResumeTransformFeedback();
            loadDrawTransformFeedback();
        }
        #endregion

        #region Enums
        public static UInt32 GL_TRANSFORM_FEEDBACK = 0x8E22;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_PAUSED = 0x8E23;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BUFFER_ACTIVE = 0x8E24;
        public static UInt32 GL_TRANSFORM_FEEDBACK_BINDING = 0x8E25;
        #endregion

        #region Commands
        internal delegate void glBindTransformFeedbackFunc(GLenum @target, GLuint @id);
        internal static glBindTransformFeedbackFunc glBindTransformFeedbackPtr;
        internal static void loadBindTransformFeedback()
        {
            try
            {
                glBindTransformFeedbackPtr = (glBindTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTransformFeedback"), typeof(glBindTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTransformFeedback'.");
            }
        }
        public static void glBindTransformFeedback(GLenum @target, GLuint @id) => glBindTransformFeedbackPtr(@target, @id);

        internal delegate void glDeleteTransformFeedbacksFunc(GLsizei @n, const GLuint * @ids);
        internal static glDeleteTransformFeedbacksFunc glDeleteTransformFeedbacksPtr;
        internal static void loadDeleteTransformFeedbacks()
        {
            try
            {
                glDeleteTransformFeedbacksPtr = (glDeleteTransformFeedbacksFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteTransformFeedbacks"), typeof(glDeleteTransformFeedbacksFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteTransformFeedbacks'.");
            }
        }
        public static void glDeleteTransformFeedbacks(GLsizei @n, const GLuint * @ids) => glDeleteTransformFeedbacksPtr(@n, @ids);

        internal delegate void glGenTransformFeedbacksFunc(GLsizei @n, GLuint * @ids);
        internal static glGenTransformFeedbacksFunc glGenTransformFeedbacksPtr;
        internal static void loadGenTransformFeedbacks()
        {
            try
            {
                glGenTransformFeedbacksPtr = (glGenTransformFeedbacksFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenTransformFeedbacks"), typeof(glGenTransformFeedbacksFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenTransformFeedbacks'.");
            }
        }
        public static void glGenTransformFeedbacks(GLsizei @n, GLuint * @ids) => glGenTransformFeedbacksPtr(@n, @ids);

        internal delegate GLboolean glIsTransformFeedbackFunc(GLuint @id);
        internal static glIsTransformFeedbackFunc glIsTransformFeedbackPtr;
        internal static void loadIsTransformFeedback()
        {
            try
            {
                glIsTransformFeedbackPtr = (glIsTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsTransformFeedback"), typeof(glIsTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsTransformFeedback'.");
            }
        }
        public static GLboolean glIsTransformFeedback(GLuint @id) => glIsTransformFeedbackPtr(@id);

        internal delegate void glPauseTransformFeedbackFunc();
        internal static glPauseTransformFeedbackFunc glPauseTransformFeedbackPtr;
        internal static void loadPauseTransformFeedback()
        {
            try
            {
                glPauseTransformFeedbackPtr = (glPauseTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPauseTransformFeedback"), typeof(glPauseTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPauseTransformFeedback'.");
            }
        }
        public static void glPauseTransformFeedback() => glPauseTransformFeedbackPtr();

        internal delegate void glResumeTransformFeedbackFunc();
        internal static glResumeTransformFeedbackFunc glResumeTransformFeedbackPtr;
        internal static void loadResumeTransformFeedback()
        {
            try
            {
                glResumeTransformFeedbackPtr = (glResumeTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glResumeTransformFeedback"), typeof(glResumeTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glResumeTransformFeedback'.");
            }
        }
        public static void glResumeTransformFeedback() => glResumeTransformFeedbackPtr();

        internal delegate void glDrawTransformFeedbackFunc(GLenum @mode, GLuint @id);
        internal static glDrawTransformFeedbackFunc glDrawTransformFeedbackPtr;
        internal static void loadDrawTransformFeedback()
        {
            try
            {
                glDrawTransformFeedbackPtr = (glDrawTransformFeedbackFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawTransformFeedback"), typeof(glDrawTransformFeedbackFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawTransformFeedback'.");
            }
        }
        public static void glDrawTransformFeedback(GLenum @mode, GLuint @id) => glDrawTransformFeedbackPtr(@mode, @id);
        #endregion
    }
}
