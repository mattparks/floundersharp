using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_compressed_texture_pixel_storage
    {
        #region Interop
        static GL_ARB_compressed_texture_pixel_storage()
        {
            Console.WriteLine("Initalising GL_ARB_compressed_texture_pixel_storage interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_UNPACK_COMPRESSED_BLOCK_WIDTH = 0x9127;
        public static UInt32 GL_UNPACK_COMPRESSED_BLOCK_HEIGHT = 0x9128;
        public static UInt32 GL_UNPACK_COMPRESSED_BLOCK_DEPTH = 0x9129;
        public static UInt32 GL_UNPACK_COMPRESSED_BLOCK_SIZE = 0x912A;
        public static UInt32 GL_PACK_COMPRESSED_BLOCK_WIDTH = 0x912B;
        public static UInt32 GL_PACK_COMPRESSED_BLOCK_HEIGHT = 0x912C;
        public static UInt32 GL_PACK_COMPRESSED_BLOCK_DEPTH = 0x912D;
        public static UInt32 GL_PACK_COMPRESSED_BLOCK_SIZE = 0x912E;
        #endregion

        #region Commands
        #endregion
    }
}
