using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_multisample
    {
        #region Interop
        static GL_ARB_multisample()
        {
            Console.WriteLine("Initalising GL_ARB_multisample interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadSampleCoverageARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MULTISAMPLE_ARB = 0x809D;
        public static UInt32 GL_SAMPLE_ALPHA_TO_COVERAGE_ARB = 0x809E;
        public static UInt32 GL_SAMPLE_ALPHA_TO_ONE_ARB = 0x809F;
        public static UInt32 GL_SAMPLE_COVERAGE_ARB = 0x80A0;
        public static UInt32 GL_SAMPLE_BUFFERS_ARB = 0x80A8;
        public static UInt32 GL_SAMPLES_ARB = 0x80A9;
        public static UInt32 GL_SAMPLE_COVERAGE_VALUE_ARB = 0x80AA;
        public static UInt32 GL_SAMPLE_COVERAGE_INVERT_ARB = 0x80AB;
        public static UInt32 GL_MULTISAMPLE_BIT_ARB = 0x20000000;
        #endregion

        #region Commands
        internal delegate void glSampleCoverageARBFunc(GLfloat @value, GLboolean @invert);
        internal static glSampleCoverageARBFunc glSampleCoverageARBPtr;
        internal static void loadSampleCoverageARB()
        {
            try
            {
                glSampleCoverageARBPtr = (glSampleCoverageARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleCoverageARB"), typeof(glSampleCoverageARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleCoverageARB'.");
            }
        }
        public static void glSampleCoverageARB(GLfloat @value, GLboolean @invert) => glSampleCoverageARBPtr(@value, @invert);
        #endregion
    }
}
