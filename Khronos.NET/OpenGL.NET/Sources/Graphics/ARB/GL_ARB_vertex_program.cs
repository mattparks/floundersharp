using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_vertex_program
    {
        #region Interop
        static GL_ARB_vertex_program()
        {
            Console.WriteLine("Initalising GL_ARB_vertex_program interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadVertexAttrib1dARB();
            loadVertexAttrib1dvARB();
            loadVertexAttrib1fARB();
            loadVertexAttrib1fvARB();
            loadVertexAttrib1sARB();
            loadVertexAttrib1svARB();
            loadVertexAttrib2dARB();
            loadVertexAttrib2dvARB();
            loadVertexAttrib2fARB();
            loadVertexAttrib2fvARB();
            loadVertexAttrib2sARB();
            loadVertexAttrib2svARB();
            loadVertexAttrib3dARB();
            loadVertexAttrib3dvARB();
            loadVertexAttrib3fARB();
            loadVertexAttrib3fvARB();
            loadVertexAttrib3sARB();
            loadVertexAttrib3svARB();
            loadVertexAttrib4NbvARB();
            loadVertexAttrib4NivARB();
            loadVertexAttrib4NsvARB();
            loadVertexAttrib4NubARB();
            loadVertexAttrib4NubvARB();
            loadVertexAttrib4NuivARB();
            loadVertexAttrib4NusvARB();
            loadVertexAttrib4bvARB();
            loadVertexAttrib4dARB();
            loadVertexAttrib4dvARB();
            loadVertexAttrib4fARB();
            loadVertexAttrib4fvARB();
            loadVertexAttrib4ivARB();
            loadVertexAttrib4sARB();
            loadVertexAttrib4svARB();
            loadVertexAttrib4ubvARB();
            loadVertexAttrib4uivARB();
            loadVertexAttrib4usvARB();
            loadVertexAttribPointerARB();
            loadEnableVertexAttribArrayARB();
            loadDisableVertexAttribArrayARB();
            loadProgramStringARB();
            loadBindProgramARB();
            loadDeleteProgramsARB();
            loadGenProgramsARB();
            loadProgramEnvParameter4dARB();
            loadProgramEnvParameter4dvARB();
            loadProgramEnvParameter4fARB();
            loadProgramEnvParameter4fvARB();
            loadProgramLocalParameter4dARB();
            loadProgramLocalParameter4dvARB();
            loadProgramLocalParameter4fARB();
            loadProgramLocalParameter4fvARB();
            loadGetProgramEnvParameterdvARB();
            loadGetProgramEnvParameterfvARB();
            loadGetProgramLocalParameterdvARB();
            loadGetProgramLocalParameterfvARB();
            loadGetProgramivARB();
            loadGetProgramStringARB();
            loadGetVertexAttribdvARB();
            loadGetVertexAttribfvARB();
            loadGetVertexAttribivARB();
            loadGetVertexAttribPointervARB();
            loadIsProgramARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COLOR_SUM_ARB = 0x8458;
        public static UInt32 GL_VERTEX_PROGRAM_ARB = 0x8620;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_ENABLED_ARB = 0x8622;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_SIZE_ARB = 0x8623;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_STRIDE_ARB = 0x8624;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_TYPE_ARB = 0x8625;
        public static UInt32 GL_CURRENT_VERTEX_ATTRIB_ARB = 0x8626;
        public static UInt32 GL_PROGRAM_LENGTH_ARB = 0x8627;
        public static UInt32 GL_PROGRAM_STRING_ARB = 0x8628;
        public static UInt32 GL_MAX_PROGRAM_MATRIX_STACK_DEPTH_ARB = 0x862E;
        public static UInt32 GL_MAX_PROGRAM_MATRICES_ARB = 0x862F;
        public static UInt32 GL_CURRENT_MATRIX_STACK_DEPTH_ARB = 0x8640;
        public static UInt32 GL_CURRENT_MATRIX_ARB = 0x8641;
        public static UInt32 GL_VERTEX_PROGRAM_POINT_SIZE_ARB = 0x8642;
        public static UInt32 GL_VERTEX_PROGRAM_TWO_SIDE_ARB = 0x8643;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_POINTER_ARB = 0x8645;
        public static UInt32 GL_PROGRAM_ERROR_POSITION_ARB = 0x864B;
        public static UInt32 GL_PROGRAM_BINDING_ARB = 0x8677;
        public static UInt32 GL_MAX_VERTEX_ATTRIBS_ARB = 0x8869;
        public static UInt32 GL_VERTEX_ATTRIB_ARRAY_NORMALIZED_ARB = 0x886A;
        public static UInt32 GL_PROGRAM_ERROR_STRING_ARB = 0x8874;
        public static UInt32 GL_PROGRAM_FORMAT_ASCII_ARB = 0x8875;
        public static UInt32 GL_PROGRAM_FORMAT_ARB = 0x8876;
        public static UInt32 GL_PROGRAM_INSTRUCTIONS_ARB = 0x88A0;
        public static UInt32 GL_MAX_PROGRAM_INSTRUCTIONS_ARB = 0x88A1;
        public static UInt32 GL_PROGRAM_NATIVE_INSTRUCTIONS_ARB = 0x88A2;
        public static UInt32 GL_MAX_PROGRAM_NATIVE_INSTRUCTIONS_ARB = 0x88A3;
        public static UInt32 GL_PROGRAM_TEMPORARIES_ARB = 0x88A4;
        public static UInt32 GL_MAX_PROGRAM_TEMPORARIES_ARB = 0x88A5;
        public static UInt32 GL_PROGRAM_NATIVE_TEMPORARIES_ARB = 0x88A6;
        public static UInt32 GL_MAX_PROGRAM_NATIVE_TEMPORARIES_ARB = 0x88A7;
        public static UInt32 GL_PROGRAM_PARAMETERS_ARB = 0x88A8;
        public static UInt32 GL_MAX_PROGRAM_PARAMETERS_ARB = 0x88A9;
        public static UInt32 GL_PROGRAM_NATIVE_PARAMETERS_ARB = 0x88AA;
        public static UInt32 GL_MAX_PROGRAM_NATIVE_PARAMETERS_ARB = 0x88AB;
        public static UInt32 GL_PROGRAM_ATTRIBS_ARB = 0x88AC;
        public static UInt32 GL_MAX_PROGRAM_ATTRIBS_ARB = 0x88AD;
        public static UInt32 GL_PROGRAM_NATIVE_ATTRIBS_ARB = 0x88AE;
        public static UInt32 GL_MAX_PROGRAM_NATIVE_ATTRIBS_ARB = 0x88AF;
        public static UInt32 GL_PROGRAM_ADDRESS_REGISTERS_ARB = 0x88B0;
        public static UInt32 GL_MAX_PROGRAM_ADDRESS_REGISTERS_ARB = 0x88B1;
        public static UInt32 GL_PROGRAM_NATIVE_ADDRESS_REGISTERS_ARB = 0x88B2;
        public static UInt32 GL_MAX_PROGRAM_NATIVE_ADDRESS_REGISTERS_ARB = 0x88B3;
        public static UInt32 GL_MAX_PROGRAM_LOCAL_PARAMETERS_ARB = 0x88B4;
        public static UInt32 GL_MAX_PROGRAM_ENV_PARAMETERS_ARB = 0x88B5;
        public static UInt32 GL_PROGRAM_UNDER_NATIVE_LIMITS_ARB = 0x88B6;
        public static UInt32 GL_TRANSPOSE_CURRENT_MATRIX_ARB = 0x88B7;
        public static UInt32 GL_MATRIX0_ARB = 0x88C0;
        public static UInt32 GL_MATRIX1_ARB = 0x88C1;
        public static UInt32 GL_MATRIX2_ARB = 0x88C2;
        public static UInt32 GL_MATRIX3_ARB = 0x88C3;
        public static UInt32 GL_MATRIX4_ARB = 0x88C4;
        public static UInt32 GL_MATRIX5_ARB = 0x88C5;
        public static UInt32 GL_MATRIX6_ARB = 0x88C6;
        public static UInt32 GL_MATRIX7_ARB = 0x88C7;
        public static UInt32 GL_MATRIX8_ARB = 0x88C8;
        public static UInt32 GL_MATRIX9_ARB = 0x88C9;
        public static UInt32 GL_MATRIX10_ARB = 0x88CA;
        public static UInt32 GL_MATRIX11_ARB = 0x88CB;
        public static UInt32 GL_MATRIX12_ARB = 0x88CC;
        public static UInt32 GL_MATRIX13_ARB = 0x88CD;
        public static UInt32 GL_MATRIX14_ARB = 0x88CE;
        public static UInt32 GL_MATRIX15_ARB = 0x88CF;
        public static UInt32 GL_MATRIX16_ARB = 0x88D0;
        public static UInt32 GL_MATRIX17_ARB = 0x88D1;
        public static UInt32 GL_MATRIX18_ARB = 0x88D2;
        public static UInt32 GL_MATRIX19_ARB = 0x88D3;
        public static UInt32 GL_MATRIX20_ARB = 0x88D4;
        public static UInt32 GL_MATRIX21_ARB = 0x88D5;
        public static UInt32 GL_MATRIX22_ARB = 0x88D6;
        public static UInt32 GL_MATRIX23_ARB = 0x88D7;
        public static UInt32 GL_MATRIX24_ARB = 0x88D8;
        public static UInt32 GL_MATRIX25_ARB = 0x88D9;
        public static UInt32 GL_MATRIX26_ARB = 0x88DA;
        public static UInt32 GL_MATRIX27_ARB = 0x88DB;
        public static UInt32 GL_MATRIX28_ARB = 0x88DC;
        public static UInt32 GL_MATRIX29_ARB = 0x88DD;
        public static UInt32 GL_MATRIX30_ARB = 0x88DE;
        public static UInt32 GL_MATRIX31_ARB = 0x88DF;
        #endregion

        #region Commands
        internal delegate void glVertexAttrib1dARBFunc(GLuint @index, GLdouble @x);
        internal static glVertexAttrib1dARBFunc glVertexAttrib1dARBPtr;
        internal static void loadVertexAttrib1dARB()
        {
            try
            {
                glVertexAttrib1dARBPtr = (glVertexAttrib1dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1dARB"), typeof(glVertexAttrib1dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1dARB'.");
            }
        }
        public static void glVertexAttrib1dARB(GLuint @index, GLdouble @x) => glVertexAttrib1dARBPtr(@index, @x);

        internal delegate void glVertexAttrib1dvARBFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttrib1dvARBFunc glVertexAttrib1dvARBPtr;
        internal static void loadVertexAttrib1dvARB()
        {
            try
            {
                glVertexAttrib1dvARBPtr = (glVertexAttrib1dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1dvARB"), typeof(glVertexAttrib1dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1dvARB'.");
            }
        }
        public static void glVertexAttrib1dvARB(GLuint @index, const GLdouble * @v) => glVertexAttrib1dvARBPtr(@index, @v);

        internal delegate void glVertexAttrib1fARBFunc(GLuint @index, GLfloat @x);
        internal static glVertexAttrib1fARBFunc glVertexAttrib1fARBPtr;
        internal static void loadVertexAttrib1fARB()
        {
            try
            {
                glVertexAttrib1fARBPtr = (glVertexAttrib1fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1fARB"), typeof(glVertexAttrib1fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1fARB'.");
            }
        }
        public static void glVertexAttrib1fARB(GLuint @index, GLfloat @x) => glVertexAttrib1fARBPtr(@index, @x);

        internal delegate void glVertexAttrib1fvARBFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib1fvARBFunc glVertexAttrib1fvARBPtr;
        internal static void loadVertexAttrib1fvARB()
        {
            try
            {
                glVertexAttrib1fvARBPtr = (glVertexAttrib1fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1fvARB"), typeof(glVertexAttrib1fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1fvARB'.");
            }
        }
        public static void glVertexAttrib1fvARB(GLuint @index, const GLfloat * @v) => glVertexAttrib1fvARBPtr(@index, @v);

        internal delegate void glVertexAttrib1sARBFunc(GLuint @index, GLshort @x);
        internal static glVertexAttrib1sARBFunc glVertexAttrib1sARBPtr;
        internal static void loadVertexAttrib1sARB()
        {
            try
            {
                glVertexAttrib1sARBPtr = (glVertexAttrib1sARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1sARB"), typeof(glVertexAttrib1sARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1sARB'.");
            }
        }
        public static void glVertexAttrib1sARB(GLuint @index, GLshort @x) => glVertexAttrib1sARBPtr(@index, @x);

        internal delegate void glVertexAttrib1svARBFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib1svARBFunc glVertexAttrib1svARBPtr;
        internal static void loadVertexAttrib1svARB()
        {
            try
            {
                glVertexAttrib1svARBPtr = (glVertexAttrib1svARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib1svARB"), typeof(glVertexAttrib1svARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib1svARB'.");
            }
        }
        public static void glVertexAttrib1svARB(GLuint @index, const GLshort * @v) => glVertexAttrib1svARBPtr(@index, @v);

        internal delegate void glVertexAttrib2dARBFunc(GLuint @index, GLdouble @x, GLdouble @y);
        internal static glVertexAttrib2dARBFunc glVertexAttrib2dARBPtr;
        internal static void loadVertexAttrib2dARB()
        {
            try
            {
                glVertexAttrib2dARBPtr = (glVertexAttrib2dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2dARB"), typeof(glVertexAttrib2dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2dARB'.");
            }
        }
        public static void glVertexAttrib2dARB(GLuint @index, GLdouble @x, GLdouble @y) => glVertexAttrib2dARBPtr(@index, @x, @y);

        internal delegate void glVertexAttrib2dvARBFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttrib2dvARBFunc glVertexAttrib2dvARBPtr;
        internal static void loadVertexAttrib2dvARB()
        {
            try
            {
                glVertexAttrib2dvARBPtr = (glVertexAttrib2dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2dvARB"), typeof(glVertexAttrib2dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2dvARB'.");
            }
        }
        public static void glVertexAttrib2dvARB(GLuint @index, const GLdouble * @v) => glVertexAttrib2dvARBPtr(@index, @v);

        internal delegate void glVertexAttrib2fARBFunc(GLuint @index, GLfloat @x, GLfloat @y);
        internal static glVertexAttrib2fARBFunc glVertexAttrib2fARBPtr;
        internal static void loadVertexAttrib2fARB()
        {
            try
            {
                glVertexAttrib2fARBPtr = (glVertexAttrib2fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2fARB"), typeof(glVertexAttrib2fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2fARB'.");
            }
        }
        public static void glVertexAttrib2fARB(GLuint @index, GLfloat @x, GLfloat @y) => glVertexAttrib2fARBPtr(@index, @x, @y);

        internal delegate void glVertexAttrib2fvARBFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib2fvARBFunc glVertexAttrib2fvARBPtr;
        internal static void loadVertexAttrib2fvARB()
        {
            try
            {
                glVertexAttrib2fvARBPtr = (glVertexAttrib2fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2fvARB"), typeof(glVertexAttrib2fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2fvARB'.");
            }
        }
        public static void glVertexAttrib2fvARB(GLuint @index, const GLfloat * @v) => glVertexAttrib2fvARBPtr(@index, @v);

        internal delegate void glVertexAttrib2sARBFunc(GLuint @index, GLshort @x, GLshort @y);
        internal static glVertexAttrib2sARBFunc glVertexAttrib2sARBPtr;
        internal static void loadVertexAttrib2sARB()
        {
            try
            {
                glVertexAttrib2sARBPtr = (glVertexAttrib2sARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2sARB"), typeof(glVertexAttrib2sARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2sARB'.");
            }
        }
        public static void glVertexAttrib2sARB(GLuint @index, GLshort @x, GLshort @y) => glVertexAttrib2sARBPtr(@index, @x, @y);

        internal delegate void glVertexAttrib2svARBFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib2svARBFunc glVertexAttrib2svARBPtr;
        internal static void loadVertexAttrib2svARB()
        {
            try
            {
                glVertexAttrib2svARBPtr = (glVertexAttrib2svARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib2svARB"), typeof(glVertexAttrib2svARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib2svARB'.");
            }
        }
        public static void glVertexAttrib2svARB(GLuint @index, const GLshort * @v) => glVertexAttrib2svARBPtr(@index, @v);

        internal delegate void glVertexAttrib3dARBFunc(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glVertexAttrib3dARBFunc glVertexAttrib3dARBPtr;
        internal static void loadVertexAttrib3dARB()
        {
            try
            {
                glVertexAttrib3dARBPtr = (glVertexAttrib3dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3dARB"), typeof(glVertexAttrib3dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3dARB'.");
            }
        }
        public static void glVertexAttrib3dARB(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z) => glVertexAttrib3dARBPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttrib3dvARBFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttrib3dvARBFunc glVertexAttrib3dvARBPtr;
        internal static void loadVertexAttrib3dvARB()
        {
            try
            {
                glVertexAttrib3dvARBPtr = (glVertexAttrib3dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3dvARB"), typeof(glVertexAttrib3dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3dvARB'.");
            }
        }
        public static void glVertexAttrib3dvARB(GLuint @index, const GLdouble * @v) => glVertexAttrib3dvARBPtr(@index, @v);

        internal delegate void glVertexAttrib3fARBFunc(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glVertexAttrib3fARBFunc glVertexAttrib3fARBPtr;
        internal static void loadVertexAttrib3fARB()
        {
            try
            {
                glVertexAttrib3fARBPtr = (glVertexAttrib3fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3fARB"), typeof(glVertexAttrib3fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3fARB'.");
            }
        }
        public static void glVertexAttrib3fARB(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z) => glVertexAttrib3fARBPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttrib3fvARBFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib3fvARBFunc glVertexAttrib3fvARBPtr;
        internal static void loadVertexAttrib3fvARB()
        {
            try
            {
                glVertexAttrib3fvARBPtr = (glVertexAttrib3fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3fvARB"), typeof(glVertexAttrib3fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3fvARB'.");
            }
        }
        public static void glVertexAttrib3fvARB(GLuint @index, const GLfloat * @v) => glVertexAttrib3fvARBPtr(@index, @v);

        internal delegate void glVertexAttrib3sARBFunc(GLuint @index, GLshort @x, GLshort @y, GLshort @z);
        internal static glVertexAttrib3sARBFunc glVertexAttrib3sARBPtr;
        internal static void loadVertexAttrib3sARB()
        {
            try
            {
                glVertexAttrib3sARBPtr = (glVertexAttrib3sARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3sARB"), typeof(glVertexAttrib3sARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3sARB'.");
            }
        }
        public static void glVertexAttrib3sARB(GLuint @index, GLshort @x, GLshort @y, GLshort @z) => glVertexAttrib3sARBPtr(@index, @x, @y, @z);

        internal delegate void glVertexAttrib3svARBFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib3svARBFunc glVertexAttrib3svARBPtr;
        internal static void loadVertexAttrib3svARB()
        {
            try
            {
                glVertexAttrib3svARBPtr = (glVertexAttrib3svARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib3svARB"), typeof(glVertexAttrib3svARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib3svARB'.");
            }
        }
        public static void glVertexAttrib3svARB(GLuint @index, const GLshort * @v) => glVertexAttrib3svARBPtr(@index, @v);

        internal delegate void glVertexAttrib4NbvARBFunc(GLuint @index, const GLbyte * @v);
        internal static glVertexAttrib4NbvARBFunc glVertexAttrib4NbvARBPtr;
        internal static void loadVertexAttrib4NbvARB()
        {
            try
            {
                glVertexAttrib4NbvARBPtr = (glVertexAttrib4NbvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4NbvARB"), typeof(glVertexAttrib4NbvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4NbvARB'.");
            }
        }
        public static void glVertexAttrib4NbvARB(GLuint @index, const GLbyte * @v) => glVertexAttrib4NbvARBPtr(@index, @v);

        internal delegate void glVertexAttrib4NivARBFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttrib4NivARBFunc glVertexAttrib4NivARBPtr;
        internal static void loadVertexAttrib4NivARB()
        {
            try
            {
                glVertexAttrib4NivARBPtr = (glVertexAttrib4NivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4NivARB"), typeof(glVertexAttrib4NivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4NivARB'.");
            }
        }
        public static void glVertexAttrib4NivARB(GLuint @index, const GLint * @v) => glVertexAttrib4NivARBPtr(@index, @v);

        internal delegate void glVertexAttrib4NsvARBFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib4NsvARBFunc glVertexAttrib4NsvARBPtr;
        internal static void loadVertexAttrib4NsvARB()
        {
            try
            {
                glVertexAttrib4NsvARBPtr = (glVertexAttrib4NsvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4NsvARB"), typeof(glVertexAttrib4NsvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4NsvARB'.");
            }
        }
        public static void glVertexAttrib4NsvARB(GLuint @index, const GLshort * @v) => glVertexAttrib4NsvARBPtr(@index, @v);

        internal delegate void glVertexAttrib4NubARBFunc(GLuint @index, GLubyte @x, GLubyte @y, GLubyte @z, GLubyte @w);
        internal static glVertexAttrib4NubARBFunc glVertexAttrib4NubARBPtr;
        internal static void loadVertexAttrib4NubARB()
        {
            try
            {
                glVertexAttrib4NubARBPtr = (glVertexAttrib4NubARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4NubARB"), typeof(glVertexAttrib4NubARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4NubARB'.");
            }
        }
        public static void glVertexAttrib4NubARB(GLuint @index, GLubyte @x, GLubyte @y, GLubyte @z, GLubyte @w) => glVertexAttrib4NubARBPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4NubvARBFunc(GLuint @index, const GLubyte * @v);
        internal static glVertexAttrib4NubvARBFunc glVertexAttrib4NubvARBPtr;
        internal static void loadVertexAttrib4NubvARB()
        {
            try
            {
                glVertexAttrib4NubvARBPtr = (glVertexAttrib4NubvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4NubvARB"), typeof(glVertexAttrib4NubvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4NubvARB'.");
            }
        }
        public static void glVertexAttrib4NubvARB(GLuint @index, const GLubyte * @v) => glVertexAttrib4NubvARBPtr(@index, @v);

        internal delegate void glVertexAttrib4NuivARBFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttrib4NuivARBFunc glVertexAttrib4NuivARBPtr;
        internal static void loadVertexAttrib4NuivARB()
        {
            try
            {
                glVertexAttrib4NuivARBPtr = (glVertexAttrib4NuivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4NuivARB"), typeof(glVertexAttrib4NuivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4NuivARB'.");
            }
        }
        public static void glVertexAttrib4NuivARB(GLuint @index, const GLuint * @v) => glVertexAttrib4NuivARBPtr(@index, @v);

        internal delegate void glVertexAttrib4NusvARBFunc(GLuint @index, const GLushort * @v);
        internal static glVertexAttrib4NusvARBFunc glVertexAttrib4NusvARBPtr;
        internal static void loadVertexAttrib4NusvARB()
        {
            try
            {
                glVertexAttrib4NusvARBPtr = (glVertexAttrib4NusvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4NusvARB"), typeof(glVertexAttrib4NusvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4NusvARB'.");
            }
        }
        public static void glVertexAttrib4NusvARB(GLuint @index, const GLushort * @v) => glVertexAttrib4NusvARBPtr(@index, @v);

        internal delegate void glVertexAttrib4bvARBFunc(GLuint @index, const GLbyte * @v);
        internal static glVertexAttrib4bvARBFunc glVertexAttrib4bvARBPtr;
        internal static void loadVertexAttrib4bvARB()
        {
            try
            {
                glVertexAttrib4bvARBPtr = (glVertexAttrib4bvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4bvARB"), typeof(glVertexAttrib4bvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4bvARB'.");
            }
        }
        public static void glVertexAttrib4bvARB(GLuint @index, const GLbyte * @v) => glVertexAttrib4bvARBPtr(@index, @v);

        internal delegate void glVertexAttrib4dARBFunc(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glVertexAttrib4dARBFunc glVertexAttrib4dARBPtr;
        internal static void loadVertexAttrib4dARB()
        {
            try
            {
                glVertexAttrib4dARBPtr = (glVertexAttrib4dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4dARB"), typeof(glVertexAttrib4dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4dARB'.");
            }
        }
        public static void glVertexAttrib4dARB(GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glVertexAttrib4dARBPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4dvARBFunc(GLuint @index, const GLdouble * @v);
        internal static glVertexAttrib4dvARBFunc glVertexAttrib4dvARBPtr;
        internal static void loadVertexAttrib4dvARB()
        {
            try
            {
                glVertexAttrib4dvARBPtr = (glVertexAttrib4dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4dvARB"), typeof(glVertexAttrib4dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4dvARB'.");
            }
        }
        public static void glVertexAttrib4dvARB(GLuint @index, const GLdouble * @v) => glVertexAttrib4dvARBPtr(@index, @v);

        internal delegate void glVertexAttrib4fARBFunc(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glVertexAttrib4fARBFunc glVertexAttrib4fARBPtr;
        internal static void loadVertexAttrib4fARB()
        {
            try
            {
                glVertexAttrib4fARBPtr = (glVertexAttrib4fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4fARB"), typeof(glVertexAttrib4fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4fARB'.");
            }
        }
        public static void glVertexAttrib4fARB(GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glVertexAttrib4fARBPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4fvARBFunc(GLuint @index, const GLfloat * @v);
        internal static glVertexAttrib4fvARBFunc glVertexAttrib4fvARBPtr;
        internal static void loadVertexAttrib4fvARB()
        {
            try
            {
                glVertexAttrib4fvARBPtr = (glVertexAttrib4fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4fvARB"), typeof(glVertexAttrib4fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4fvARB'.");
            }
        }
        public static void glVertexAttrib4fvARB(GLuint @index, const GLfloat * @v) => glVertexAttrib4fvARBPtr(@index, @v);

        internal delegate void glVertexAttrib4ivARBFunc(GLuint @index, const GLint * @v);
        internal static glVertexAttrib4ivARBFunc glVertexAttrib4ivARBPtr;
        internal static void loadVertexAttrib4ivARB()
        {
            try
            {
                glVertexAttrib4ivARBPtr = (glVertexAttrib4ivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4ivARB"), typeof(glVertexAttrib4ivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4ivARB'.");
            }
        }
        public static void glVertexAttrib4ivARB(GLuint @index, const GLint * @v) => glVertexAttrib4ivARBPtr(@index, @v);

        internal delegate void glVertexAttrib4sARBFunc(GLuint @index, GLshort @x, GLshort @y, GLshort @z, GLshort @w);
        internal static glVertexAttrib4sARBFunc glVertexAttrib4sARBPtr;
        internal static void loadVertexAttrib4sARB()
        {
            try
            {
                glVertexAttrib4sARBPtr = (glVertexAttrib4sARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4sARB"), typeof(glVertexAttrib4sARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4sARB'.");
            }
        }
        public static void glVertexAttrib4sARB(GLuint @index, GLshort @x, GLshort @y, GLshort @z, GLshort @w) => glVertexAttrib4sARBPtr(@index, @x, @y, @z, @w);

        internal delegate void glVertexAttrib4svARBFunc(GLuint @index, const GLshort * @v);
        internal static glVertexAttrib4svARBFunc glVertexAttrib4svARBPtr;
        internal static void loadVertexAttrib4svARB()
        {
            try
            {
                glVertexAttrib4svARBPtr = (glVertexAttrib4svARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4svARB"), typeof(glVertexAttrib4svARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4svARB'.");
            }
        }
        public static void glVertexAttrib4svARB(GLuint @index, const GLshort * @v) => glVertexAttrib4svARBPtr(@index, @v);

        internal delegate void glVertexAttrib4ubvARBFunc(GLuint @index, const GLubyte * @v);
        internal static glVertexAttrib4ubvARBFunc glVertexAttrib4ubvARBPtr;
        internal static void loadVertexAttrib4ubvARB()
        {
            try
            {
                glVertexAttrib4ubvARBPtr = (glVertexAttrib4ubvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4ubvARB"), typeof(glVertexAttrib4ubvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4ubvARB'.");
            }
        }
        public static void glVertexAttrib4ubvARB(GLuint @index, const GLubyte * @v) => glVertexAttrib4ubvARBPtr(@index, @v);

        internal delegate void glVertexAttrib4uivARBFunc(GLuint @index, const GLuint * @v);
        internal static glVertexAttrib4uivARBFunc glVertexAttrib4uivARBPtr;
        internal static void loadVertexAttrib4uivARB()
        {
            try
            {
                glVertexAttrib4uivARBPtr = (glVertexAttrib4uivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4uivARB"), typeof(glVertexAttrib4uivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4uivARB'.");
            }
        }
        public static void glVertexAttrib4uivARB(GLuint @index, const GLuint * @v) => glVertexAttrib4uivARBPtr(@index, @v);

        internal delegate void glVertexAttrib4usvARBFunc(GLuint @index, const GLushort * @v);
        internal static glVertexAttrib4usvARBFunc glVertexAttrib4usvARBPtr;
        internal static void loadVertexAttrib4usvARB()
        {
            try
            {
                glVertexAttrib4usvARBPtr = (glVertexAttrib4usvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttrib4usvARB"), typeof(glVertexAttrib4usvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttrib4usvARB'.");
            }
        }
        public static void glVertexAttrib4usvARB(GLuint @index, const GLushort * @v) => glVertexAttrib4usvARBPtr(@index, @v);

        internal delegate void glVertexAttribPointerARBFunc(GLuint @index, GLint @size, GLenum @type, GLboolean @normalized, GLsizei @stride, const void * @pointer);
        internal static glVertexAttribPointerARBFunc glVertexAttribPointerARBPtr;
        internal static void loadVertexAttribPointerARB()
        {
            try
            {
                glVertexAttribPointerARBPtr = (glVertexAttribPointerARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexAttribPointerARB"), typeof(glVertexAttribPointerARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexAttribPointerARB'.");
            }
        }
        public static void glVertexAttribPointerARB(GLuint @index, GLint @size, GLenum @type, GLboolean @normalized, GLsizei @stride, const void * @pointer) => glVertexAttribPointerARBPtr(@index, @size, @type, @normalized, @stride, @pointer);

        internal delegate void glEnableVertexAttribArrayARBFunc(GLuint @index);
        internal static glEnableVertexAttribArrayARBFunc glEnableVertexAttribArrayARBPtr;
        internal static void loadEnableVertexAttribArrayARB()
        {
            try
            {
                glEnableVertexAttribArrayARBPtr = (glEnableVertexAttribArrayARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableVertexAttribArrayARB"), typeof(glEnableVertexAttribArrayARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableVertexAttribArrayARB'.");
            }
        }
        public static void glEnableVertexAttribArrayARB(GLuint @index) => glEnableVertexAttribArrayARBPtr(@index);

        internal delegate void glDisableVertexAttribArrayARBFunc(GLuint @index);
        internal static glDisableVertexAttribArrayARBFunc glDisableVertexAttribArrayARBPtr;
        internal static void loadDisableVertexAttribArrayARB()
        {
            try
            {
                glDisableVertexAttribArrayARBPtr = (glDisableVertexAttribArrayARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableVertexAttribArrayARB"), typeof(glDisableVertexAttribArrayARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableVertexAttribArrayARB'.");
            }
        }
        public static void glDisableVertexAttribArrayARB(GLuint @index) => glDisableVertexAttribArrayARBPtr(@index);

        internal delegate void glProgramStringARBFunc(GLenum @target, GLenum @format, GLsizei @len, const void * @string);
        internal static glProgramStringARBFunc glProgramStringARBPtr;
        internal static void loadProgramStringARB()
        {
            try
            {
                glProgramStringARBPtr = (glProgramStringARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramStringARB"), typeof(glProgramStringARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramStringARB'.");
            }
        }
        public static void glProgramStringARB(GLenum @target, GLenum @format, GLsizei @len, const void * @string) => glProgramStringARBPtr(@target, @format, @len, @string);

        internal delegate void glBindProgramARBFunc(GLenum @target, GLuint @program);
        internal static glBindProgramARBFunc glBindProgramARBPtr;
        internal static void loadBindProgramARB()
        {
            try
            {
                glBindProgramARBPtr = (glBindProgramARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindProgramARB"), typeof(glBindProgramARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindProgramARB'.");
            }
        }
        public static void glBindProgramARB(GLenum @target, GLuint @program) => glBindProgramARBPtr(@target, @program);

        internal delegate void glDeleteProgramsARBFunc(GLsizei @n, const GLuint * @programs);
        internal static glDeleteProgramsARBFunc glDeleteProgramsARBPtr;
        internal static void loadDeleteProgramsARB()
        {
            try
            {
                glDeleteProgramsARBPtr = (glDeleteProgramsARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteProgramsARB"), typeof(glDeleteProgramsARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteProgramsARB'.");
            }
        }
        public static void glDeleteProgramsARB(GLsizei @n, const GLuint * @programs) => glDeleteProgramsARBPtr(@n, @programs);

        internal delegate void glGenProgramsARBFunc(GLsizei @n, GLuint * @programs);
        internal static glGenProgramsARBFunc glGenProgramsARBPtr;
        internal static void loadGenProgramsARB()
        {
            try
            {
                glGenProgramsARBPtr = (glGenProgramsARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenProgramsARB"), typeof(glGenProgramsARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenProgramsARB'.");
            }
        }
        public static void glGenProgramsARB(GLsizei @n, GLuint * @programs) => glGenProgramsARBPtr(@n, @programs);

        internal delegate void glProgramEnvParameter4dARBFunc(GLenum @target, GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glProgramEnvParameter4dARBFunc glProgramEnvParameter4dARBPtr;
        internal static void loadProgramEnvParameter4dARB()
        {
            try
            {
                glProgramEnvParameter4dARBPtr = (glProgramEnvParameter4dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameter4dARB"), typeof(glProgramEnvParameter4dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameter4dARB'.");
            }
        }
        public static void glProgramEnvParameter4dARB(GLenum @target, GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glProgramEnvParameter4dARBPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramEnvParameter4dvARBFunc(GLenum @target, GLuint @index, const GLdouble * @params);
        internal static glProgramEnvParameter4dvARBFunc glProgramEnvParameter4dvARBPtr;
        internal static void loadProgramEnvParameter4dvARB()
        {
            try
            {
                glProgramEnvParameter4dvARBPtr = (glProgramEnvParameter4dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameter4dvARB"), typeof(glProgramEnvParameter4dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameter4dvARB'.");
            }
        }
        public static void glProgramEnvParameter4dvARB(GLenum @target, GLuint @index, const GLdouble * @params) => glProgramEnvParameter4dvARBPtr(@target, @index, @params);

        internal delegate void glProgramEnvParameter4fARBFunc(GLenum @target, GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glProgramEnvParameter4fARBFunc glProgramEnvParameter4fARBPtr;
        internal static void loadProgramEnvParameter4fARB()
        {
            try
            {
                glProgramEnvParameter4fARBPtr = (glProgramEnvParameter4fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameter4fARB"), typeof(glProgramEnvParameter4fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameter4fARB'.");
            }
        }
        public static void glProgramEnvParameter4fARB(GLenum @target, GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glProgramEnvParameter4fARBPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramEnvParameter4fvARBFunc(GLenum @target, GLuint @index, const GLfloat * @params);
        internal static glProgramEnvParameter4fvARBFunc glProgramEnvParameter4fvARBPtr;
        internal static void loadProgramEnvParameter4fvARB()
        {
            try
            {
                glProgramEnvParameter4fvARBPtr = (glProgramEnvParameter4fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramEnvParameter4fvARB"), typeof(glProgramEnvParameter4fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramEnvParameter4fvARB'.");
            }
        }
        public static void glProgramEnvParameter4fvARB(GLenum @target, GLuint @index, const GLfloat * @params) => glProgramEnvParameter4fvARBPtr(@target, @index, @params);

        internal delegate void glProgramLocalParameter4dARBFunc(GLenum @target, GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glProgramLocalParameter4dARBFunc glProgramLocalParameter4dARBPtr;
        internal static void loadProgramLocalParameter4dARB()
        {
            try
            {
                glProgramLocalParameter4dARBPtr = (glProgramLocalParameter4dARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameter4dARB"), typeof(glProgramLocalParameter4dARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameter4dARB'.");
            }
        }
        public static void glProgramLocalParameter4dARB(GLenum @target, GLuint @index, GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glProgramLocalParameter4dARBPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramLocalParameter4dvARBFunc(GLenum @target, GLuint @index, const GLdouble * @params);
        internal static glProgramLocalParameter4dvARBFunc glProgramLocalParameter4dvARBPtr;
        internal static void loadProgramLocalParameter4dvARB()
        {
            try
            {
                glProgramLocalParameter4dvARBPtr = (glProgramLocalParameter4dvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameter4dvARB"), typeof(glProgramLocalParameter4dvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameter4dvARB'.");
            }
        }
        public static void glProgramLocalParameter4dvARB(GLenum @target, GLuint @index, const GLdouble * @params) => glProgramLocalParameter4dvARBPtr(@target, @index, @params);

        internal delegate void glProgramLocalParameter4fARBFunc(GLenum @target, GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glProgramLocalParameter4fARBFunc glProgramLocalParameter4fARBPtr;
        internal static void loadProgramLocalParameter4fARB()
        {
            try
            {
                glProgramLocalParameter4fARBPtr = (glProgramLocalParameter4fARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameter4fARB"), typeof(glProgramLocalParameter4fARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameter4fARB'.");
            }
        }
        public static void glProgramLocalParameter4fARB(GLenum @target, GLuint @index, GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glProgramLocalParameter4fARBPtr(@target, @index, @x, @y, @z, @w);

        internal delegate void glProgramLocalParameter4fvARBFunc(GLenum @target, GLuint @index, const GLfloat * @params);
        internal static glProgramLocalParameter4fvARBFunc glProgramLocalParameter4fvARBPtr;
        internal static void loadProgramLocalParameter4fvARB()
        {
            try
            {
                glProgramLocalParameter4fvARBPtr = (glProgramLocalParameter4fvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glProgramLocalParameter4fvARB"), typeof(glProgramLocalParameter4fvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glProgramLocalParameter4fvARB'.");
            }
        }
        public static void glProgramLocalParameter4fvARB(GLenum @target, GLuint @index, const GLfloat * @params) => glProgramLocalParameter4fvARBPtr(@target, @index, @params);

        internal delegate void glGetProgramEnvParameterdvARBFunc(GLenum @target, GLuint @index, GLdouble * @params);
        internal static glGetProgramEnvParameterdvARBFunc glGetProgramEnvParameterdvARBPtr;
        internal static void loadGetProgramEnvParameterdvARB()
        {
            try
            {
                glGetProgramEnvParameterdvARBPtr = (glGetProgramEnvParameterdvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramEnvParameterdvARB"), typeof(glGetProgramEnvParameterdvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramEnvParameterdvARB'.");
            }
        }
        public static void glGetProgramEnvParameterdvARB(GLenum @target, GLuint @index, GLdouble * @params) => glGetProgramEnvParameterdvARBPtr(@target, @index, @params);

        internal delegate void glGetProgramEnvParameterfvARBFunc(GLenum @target, GLuint @index, GLfloat * @params);
        internal static glGetProgramEnvParameterfvARBFunc glGetProgramEnvParameterfvARBPtr;
        internal static void loadGetProgramEnvParameterfvARB()
        {
            try
            {
                glGetProgramEnvParameterfvARBPtr = (glGetProgramEnvParameterfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramEnvParameterfvARB"), typeof(glGetProgramEnvParameterfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramEnvParameterfvARB'.");
            }
        }
        public static void glGetProgramEnvParameterfvARB(GLenum @target, GLuint @index, GLfloat * @params) => glGetProgramEnvParameterfvARBPtr(@target, @index, @params);

        internal delegate void glGetProgramLocalParameterdvARBFunc(GLenum @target, GLuint @index, GLdouble * @params);
        internal static glGetProgramLocalParameterdvARBFunc glGetProgramLocalParameterdvARBPtr;
        internal static void loadGetProgramLocalParameterdvARB()
        {
            try
            {
                glGetProgramLocalParameterdvARBPtr = (glGetProgramLocalParameterdvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramLocalParameterdvARB"), typeof(glGetProgramLocalParameterdvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramLocalParameterdvARB'.");
            }
        }
        public static void glGetProgramLocalParameterdvARB(GLenum @target, GLuint @index, GLdouble * @params) => glGetProgramLocalParameterdvARBPtr(@target, @index, @params);

        internal delegate void glGetProgramLocalParameterfvARBFunc(GLenum @target, GLuint @index, GLfloat * @params);
        internal static glGetProgramLocalParameterfvARBFunc glGetProgramLocalParameterfvARBPtr;
        internal static void loadGetProgramLocalParameterfvARB()
        {
            try
            {
                glGetProgramLocalParameterfvARBPtr = (glGetProgramLocalParameterfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramLocalParameterfvARB"), typeof(glGetProgramLocalParameterfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramLocalParameterfvARB'.");
            }
        }
        public static void glGetProgramLocalParameterfvARB(GLenum @target, GLuint @index, GLfloat * @params) => glGetProgramLocalParameterfvARBPtr(@target, @index, @params);

        internal delegate void glGetProgramivARBFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetProgramivARBFunc glGetProgramivARBPtr;
        internal static void loadGetProgramivARB()
        {
            try
            {
                glGetProgramivARBPtr = (glGetProgramivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramivARB"), typeof(glGetProgramivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramivARB'.");
            }
        }
        public static void glGetProgramivARB(GLenum @target, GLenum @pname, GLint * @params) => glGetProgramivARBPtr(@target, @pname, @params);

        internal delegate void glGetProgramStringARBFunc(GLenum @target, GLenum @pname, void * @string);
        internal static glGetProgramStringARBFunc glGetProgramStringARBPtr;
        internal static void loadGetProgramStringARB()
        {
            try
            {
                glGetProgramStringARBPtr = (glGetProgramStringARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetProgramStringARB"), typeof(glGetProgramStringARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetProgramStringARB'.");
            }
        }
        public static void glGetProgramStringARB(GLenum @target, GLenum @pname, void * @string) => glGetProgramStringARBPtr(@target, @pname, @string);

        internal delegate void glGetVertexAttribdvARBFunc(GLuint @index, GLenum @pname, GLdouble * @params);
        internal static glGetVertexAttribdvARBFunc glGetVertexAttribdvARBPtr;
        internal static void loadGetVertexAttribdvARB()
        {
            try
            {
                glGetVertexAttribdvARBPtr = (glGetVertexAttribdvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribdvARB"), typeof(glGetVertexAttribdvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribdvARB'.");
            }
        }
        public static void glGetVertexAttribdvARB(GLuint @index, GLenum @pname, GLdouble * @params) => glGetVertexAttribdvARBPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribfvARBFunc(GLuint @index, GLenum @pname, GLfloat * @params);
        internal static glGetVertexAttribfvARBFunc glGetVertexAttribfvARBPtr;
        internal static void loadGetVertexAttribfvARB()
        {
            try
            {
                glGetVertexAttribfvARBPtr = (glGetVertexAttribfvARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribfvARB"), typeof(glGetVertexAttribfvARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribfvARB'.");
            }
        }
        public static void glGetVertexAttribfvARB(GLuint @index, GLenum @pname, GLfloat * @params) => glGetVertexAttribfvARBPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribivARBFunc(GLuint @index, GLenum @pname, GLint * @params);
        internal static glGetVertexAttribivARBFunc glGetVertexAttribivARBPtr;
        internal static void loadGetVertexAttribivARB()
        {
            try
            {
                glGetVertexAttribivARBPtr = (glGetVertexAttribivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribivARB"), typeof(glGetVertexAttribivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribivARB'.");
            }
        }
        public static void glGetVertexAttribivARB(GLuint @index, GLenum @pname, GLint * @params) => glGetVertexAttribivARBPtr(@index, @pname, @params);

        internal delegate void glGetVertexAttribPointervARBFunc(GLuint @index, GLenum @pname, void ** @pointer);
        internal static glGetVertexAttribPointervARBFunc glGetVertexAttribPointervARBPtr;
        internal static void loadGetVertexAttribPointervARB()
        {
            try
            {
                glGetVertexAttribPointervARBPtr = (glGetVertexAttribPointervARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetVertexAttribPointervARB"), typeof(glGetVertexAttribPointervARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetVertexAttribPointervARB'.");
            }
        }
        public static void glGetVertexAttribPointervARB(GLuint @index, GLenum @pname, void ** @pointer) => glGetVertexAttribPointervARBPtr(@index, @pname, @pointer);

        internal delegate GLboolean glIsProgramARBFunc(GLuint @program);
        internal static glIsProgramARBFunc glIsProgramARBPtr;
        internal static void loadIsProgramARB()
        {
            try
            {
                glIsProgramARBPtr = (glIsProgramARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsProgramARB"), typeof(glIsProgramARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsProgramARB'.");
            }
        }
        public static GLboolean glIsProgramARB(GLuint @program) => glIsProgramARBPtr(@program);
        #endregion
    }
}
