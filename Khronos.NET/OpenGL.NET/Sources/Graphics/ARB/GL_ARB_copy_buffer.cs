using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_copy_buffer
    {
        #region Interop
        static GL_ARB_copy_buffer()
        {
            Console.WriteLine("Initalising GL_ARB_copy_buffer interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCopyBufferSubData();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COPY_READ_BUFFER = 0x8F36;
        public static UInt32 GL_COPY_WRITE_BUFFER = 0x8F37;
        #endregion

        #region Commands
        internal delegate void glCopyBufferSubDataFunc(GLenum @readTarget, GLenum @writeTarget, GLintptr @readOffset, GLintptr @writeOffset, GLsizeiptr @size);
        internal static glCopyBufferSubDataFunc glCopyBufferSubDataPtr;
        internal static void loadCopyBufferSubData()
        {
            try
            {
                glCopyBufferSubDataPtr = (glCopyBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyBufferSubData"), typeof(glCopyBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyBufferSubData'.");
            }
        }
        public static void glCopyBufferSubData(GLenum @readTarget, GLenum @writeTarget, GLintptr @readOffset, GLintptr @writeOffset, GLsizeiptr @size) => glCopyBufferSubDataPtr(@readTarget, @writeTarget, @readOffset, @writeOffset, @size);
        #endregion
    }
}
