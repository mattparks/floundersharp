using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_framebuffer_no_attachments
    {
        #region Interop
        static GL_ARB_framebuffer_no_attachments()
        {
            Console.WriteLine("Initalising GL_ARB_framebuffer_no_attachments interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFramebufferParameteri();
            loadGetFramebufferParameteriv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_WIDTH = 0x9310;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_HEIGHT = 0x9311;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_LAYERS = 0x9312;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_SAMPLES = 0x9313;
        public static UInt32 GL_FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS = 0x9314;
        public static UInt32 GL_MAX_FRAMEBUFFER_WIDTH = 0x9315;
        public static UInt32 GL_MAX_FRAMEBUFFER_HEIGHT = 0x9316;
        public static UInt32 GL_MAX_FRAMEBUFFER_LAYERS = 0x9317;
        public static UInt32 GL_MAX_FRAMEBUFFER_SAMPLES = 0x9318;
        #endregion

        #region Commands
        internal delegate void glFramebufferParameteriFunc(GLenum @target, GLenum @pname, GLint @param);
        internal static glFramebufferParameteriFunc glFramebufferParameteriPtr;
        internal static void loadFramebufferParameteri()
        {
            try
            {
                glFramebufferParameteriPtr = (glFramebufferParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferParameteri"), typeof(glFramebufferParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferParameteri'.");
            }
        }
        public static void glFramebufferParameteri(GLenum @target, GLenum @pname, GLint @param) => glFramebufferParameteriPtr(@target, @pname, @param);

        internal delegate void glGetFramebufferParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetFramebufferParameterivFunc glGetFramebufferParameterivPtr;
        internal static void loadGetFramebufferParameteriv()
        {
            try
            {
                glGetFramebufferParameterivPtr = (glGetFramebufferParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFramebufferParameteriv"), typeof(glGetFramebufferParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFramebufferParameteriv'.");
            }
        }
        public static void glGetFramebufferParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetFramebufferParameterivPtr(@target, @pname, @params);
        #endregion
    }
}
