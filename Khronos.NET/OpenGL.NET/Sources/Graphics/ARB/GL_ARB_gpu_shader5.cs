using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_gpu_shader5
    {
        #region Interop
        static GL_ARB_gpu_shader5()
        {
            Console.WriteLine("Initalising GL_ARB_gpu_shader5 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_GEOMETRY_SHADER_INVOCATIONS = 0x887F;
        public static UInt32 GL_MAX_GEOMETRY_SHADER_INVOCATIONS = 0x8E5A;
        public static UInt32 GL_MIN_FRAGMENT_INTERPOLATION_OFFSET = 0x8E5B;
        public static UInt32 GL_MAX_FRAGMENT_INTERPOLATION_OFFSET = 0x8E5C;
        public static UInt32 GL_FRAGMENT_INTERPOLATION_OFFSET_BITS = 0x8E5D;
        public static UInt32 GL_MAX_VERTEX_STREAMS = 0x8E71;
        #endregion

        #region Commands
        #endregion
    }
}
