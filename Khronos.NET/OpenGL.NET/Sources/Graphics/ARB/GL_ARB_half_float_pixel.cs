using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_half_float_pixel
    {
        #region Interop
        static GL_ARB_half_float_pixel()
        {
            Console.WriteLine("Initalising GL_ARB_half_float_pixel interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_HALF_FLOAT_ARB = 0x140B;
        #endregion

        #region Commands
        #endregion
    }
}
