using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_depth_texture
    {
        #region Interop
        static GL_ARB_depth_texture()
        {
            Console.WriteLine("Initalising GL_ARB_depth_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_DEPTH_COMPONENT16_ARB = 0x81A5;
        public static UInt32 GL_DEPTH_COMPONENT24_ARB = 0x81A6;
        public static UInt32 GL_DEPTH_COMPONENT32_ARB = 0x81A7;
        public static UInt32 GL_TEXTURE_DEPTH_SIZE_ARB = 0x884A;
        public static UInt32 GL_DEPTH_TEXTURE_MODE_ARB = 0x884B;
        #endregion

        #region Commands
        #endregion
    }
}
