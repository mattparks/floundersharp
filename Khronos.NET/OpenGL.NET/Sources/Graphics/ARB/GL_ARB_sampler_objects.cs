using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_sampler_objects
    {
        #region Interop
        static GL_ARB_sampler_objects()
        {
            Console.WriteLine("Initalising GL_ARB_sampler_objects interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadGenSamplers();
            loadDeleteSamplers();
            loadIsSampler();
            loadBindSampler();
            loadSamplerParameteri();
            loadSamplerParameteriv();
            loadSamplerParameterf();
            loadSamplerParameterfv();
            loadSamplerParameterIiv();
            loadSamplerParameterIuiv();
            loadGetSamplerParameteriv();
            loadGetSamplerParameterIiv();
            loadGetSamplerParameterfv();
            loadGetSamplerParameterIuiv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SAMPLER_BINDING = 0x8919;
        #endregion

        #region Commands
        internal delegate void glGenSamplersFunc(GLsizei @count, GLuint * @samplers);
        internal static glGenSamplersFunc glGenSamplersPtr;
        internal static void loadGenSamplers()
        {
            try
            {
                glGenSamplersPtr = (glGenSamplersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenSamplers"), typeof(glGenSamplersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenSamplers'.");
            }
        }
        public static void glGenSamplers(GLsizei @count, GLuint * @samplers) => glGenSamplersPtr(@count, @samplers);

        internal delegate void glDeleteSamplersFunc(GLsizei @count, const GLuint * @samplers);
        internal static glDeleteSamplersFunc glDeleteSamplersPtr;
        internal static void loadDeleteSamplers()
        {
            try
            {
                glDeleteSamplersPtr = (glDeleteSamplersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteSamplers"), typeof(glDeleteSamplersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteSamplers'.");
            }
        }
        public static void glDeleteSamplers(GLsizei @count, const GLuint * @samplers) => glDeleteSamplersPtr(@count, @samplers);

        internal delegate GLboolean glIsSamplerFunc(GLuint @sampler);
        internal static glIsSamplerFunc glIsSamplerPtr;
        internal static void loadIsSampler()
        {
            try
            {
                glIsSamplerPtr = (glIsSamplerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsSampler"), typeof(glIsSamplerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsSampler'.");
            }
        }
        public static GLboolean glIsSampler(GLuint @sampler) => glIsSamplerPtr(@sampler);

        internal delegate void glBindSamplerFunc(GLuint @unit, GLuint @sampler);
        internal static glBindSamplerFunc glBindSamplerPtr;
        internal static void loadBindSampler()
        {
            try
            {
                glBindSamplerPtr = (glBindSamplerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindSampler"), typeof(glBindSamplerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindSampler'.");
            }
        }
        public static void glBindSampler(GLuint @unit, GLuint @sampler) => glBindSamplerPtr(@unit, @sampler);

        internal delegate void glSamplerParameteriFunc(GLuint @sampler, GLenum @pname, GLint @param);
        internal static glSamplerParameteriFunc glSamplerParameteriPtr;
        internal static void loadSamplerParameteri()
        {
            try
            {
                glSamplerParameteriPtr = (glSamplerParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameteri"), typeof(glSamplerParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameteri'.");
            }
        }
        public static void glSamplerParameteri(GLuint @sampler, GLenum @pname, GLint @param) => glSamplerParameteriPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterivFunc(GLuint @sampler, GLenum @pname, const GLint * @param);
        internal static glSamplerParameterivFunc glSamplerParameterivPtr;
        internal static void loadSamplerParameteriv()
        {
            try
            {
                glSamplerParameterivPtr = (glSamplerParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameteriv"), typeof(glSamplerParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameteriv'.");
            }
        }
        public static void glSamplerParameteriv(GLuint @sampler, GLenum @pname, const GLint * @param) => glSamplerParameterivPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterfFunc(GLuint @sampler, GLenum @pname, GLfloat @param);
        internal static glSamplerParameterfFunc glSamplerParameterfPtr;
        internal static void loadSamplerParameterf()
        {
            try
            {
                glSamplerParameterfPtr = (glSamplerParameterfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterf"), typeof(glSamplerParameterfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterf'.");
            }
        }
        public static void glSamplerParameterf(GLuint @sampler, GLenum @pname, GLfloat @param) => glSamplerParameterfPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterfvFunc(GLuint @sampler, GLenum @pname, const GLfloat * @param);
        internal static glSamplerParameterfvFunc glSamplerParameterfvPtr;
        internal static void loadSamplerParameterfv()
        {
            try
            {
                glSamplerParameterfvPtr = (glSamplerParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterfv"), typeof(glSamplerParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterfv'.");
            }
        }
        public static void glSamplerParameterfv(GLuint @sampler, GLenum @pname, const GLfloat * @param) => glSamplerParameterfvPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterIivFunc(GLuint @sampler, GLenum @pname, const GLint * @param);
        internal static glSamplerParameterIivFunc glSamplerParameterIivPtr;
        internal static void loadSamplerParameterIiv()
        {
            try
            {
                glSamplerParameterIivPtr = (glSamplerParameterIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterIiv"), typeof(glSamplerParameterIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterIiv'.");
            }
        }
        public static void glSamplerParameterIiv(GLuint @sampler, GLenum @pname, const GLint * @param) => glSamplerParameterIivPtr(@sampler, @pname, @param);

        internal delegate void glSamplerParameterIuivFunc(GLuint @sampler, GLenum @pname, const GLuint * @param);
        internal static glSamplerParameterIuivFunc glSamplerParameterIuivPtr;
        internal static void loadSamplerParameterIuiv()
        {
            try
            {
                glSamplerParameterIuivPtr = (glSamplerParameterIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSamplerParameterIuiv"), typeof(glSamplerParameterIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSamplerParameterIuiv'.");
            }
        }
        public static void glSamplerParameterIuiv(GLuint @sampler, GLenum @pname, const GLuint * @param) => glSamplerParameterIuivPtr(@sampler, @pname, @param);

        internal delegate void glGetSamplerParameterivFunc(GLuint @sampler, GLenum @pname, GLint * @params);
        internal static glGetSamplerParameterivFunc glGetSamplerParameterivPtr;
        internal static void loadGetSamplerParameteriv()
        {
            try
            {
                glGetSamplerParameterivPtr = (glGetSamplerParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameteriv"), typeof(glGetSamplerParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameteriv'.");
            }
        }
        public static void glGetSamplerParameteriv(GLuint @sampler, GLenum @pname, GLint * @params) => glGetSamplerParameterivPtr(@sampler, @pname, @params);

        internal delegate void glGetSamplerParameterIivFunc(GLuint @sampler, GLenum @pname, GLint * @params);
        internal static glGetSamplerParameterIivFunc glGetSamplerParameterIivPtr;
        internal static void loadGetSamplerParameterIiv()
        {
            try
            {
                glGetSamplerParameterIivPtr = (glGetSamplerParameterIivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterIiv"), typeof(glGetSamplerParameterIivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterIiv'.");
            }
        }
        public static void glGetSamplerParameterIiv(GLuint @sampler, GLenum @pname, GLint * @params) => glGetSamplerParameterIivPtr(@sampler, @pname, @params);

        internal delegate void glGetSamplerParameterfvFunc(GLuint @sampler, GLenum @pname, GLfloat * @params);
        internal static glGetSamplerParameterfvFunc glGetSamplerParameterfvPtr;
        internal static void loadGetSamplerParameterfv()
        {
            try
            {
                glGetSamplerParameterfvPtr = (glGetSamplerParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterfv"), typeof(glGetSamplerParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterfv'.");
            }
        }
        public static void glGetSamplerParameterfv(GLuint @sampler, GLenum @pname, GLfloat * @params) => glGetSamplerParameterfvPtr(@sampler, @pname, @params);

        internal delegate void glGetSamplerParameterIuivFunc(GLuint @sampler, GLenum @pname, GLuint * @params);
        internal static glGetSamplerParameterIuivFunc glGetSamplerParameterIuivPtr;
        internal static void loadGetSamplerParameterIuiv()
        {
            try
            {
                glGetSamplerParameterIuivPtr = (glGetSamplerParameterIuivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSamplerParameterIuiv"), typeof(glGetSamplerParameterIuivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSamplerParameterIuiv'.");
            }
        }
        public static void glGetSamplerParameterIuiv(GLuint @sampler, GLenum @pname, GLuint * @params) => glGetSamplerParameterIuivPtr(@sampler, @pname, @params);
        #endregion
    }
}
