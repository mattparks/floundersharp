using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_imaging
    {
        #region Interop
        static GL_ARB_imaging()
        {
            Console.WriteLine("Initalising GL_ARB_imaging interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadBlendColor();
            loadBlendEquation();
            loadColorTable();
            loadColorTableParameterfv();
            loadColorTableParameteriv();
            loadCopyColorTable();
            loadGetColorTable();
            loadGetColorTableParameterfv();
            loadGetColorTableParameteriv();
            loadColorSubTable();
            loadCopyColorSubTable();
            loadConvolutionFilter1D();
            loadConvolutionFilter2D();
            loadConvolutionParameterf();
            loadConvolutionParameterfv();
            loadConvolutionParameteri();
            loadConvolutionParameteriv();
            loadCopyConvolutionFilter1D();
            loadCopyConvolutionFilter2D();
            loadGetConvolutionFilter();
            loadGetConvolutionParameterfv();
            loadGetConvolutionParameteriv();
            loadGetSeparableFilter();
            loadSeparableFilter2D();
            loadGetHistogram();
            loadGetHistogramParameterfv();
            loadGetHistogramParameteriv();
            loadGetMinmax();
            loadGetMinmaxParameterfv();
            loadGetMinmaxParameteriv();
            loadHistogram();
            loadMinmax();
            loadResetHistogram();
            loadResetMinmax();
        }
        #endregion

        #region Enums
        public static UInt32 GL_CONSTANT_COLOR = 0x8001;
        public static UInt32 GL_ONE_MINUS_CONSTANT_COLOR = 0x8002;
        public static UInt32 GL_CONSTANT_ALPHA = 0x8003;
        public static UInt32 GL_ONE_MINUS_CONSTANT_ALPHA = 0x8004;
        public static UInt32 GL_BLEND_COLOR = 0x8005;
        public static UInt32 GL_FUNC_ADD = 0x8006;
        public static UInt32 GL_MIN = 0x8007;
        public static UInt32 GL_MAX = 0x8008;
        public static UInt32 GL_BLEND_EQUATION = 0x8009;
        public static UInt32 GL_FUNC_SUBTRACT = 0x800A;
        public static UInt32 GL_FUNC_REVERSE_SUBTRACT = 0x800B;
        public static UInt32 GL_CONVOLUTION_1D = 0x8010;
        public static UInt32 GL_CONVOLUTION_2D = 0x8011;
        public static UInt32 GL_SEPARABLE_2D = 0x8012;
        public static UInt32 GL_CONVOLUTION_BORDER_MODE = 0x8013;
        public static UInt32 GL_CONVOLUTION_FILTER_SCALE = 0x8014;
        public static UInt32 GL_CONVOLUTION_FILTER_BIAS = 0x8015;
        public static UInt32 GL_REDUCE = 0x8016;
        public static UInt32 GL_CONVOLUTION_FORMAT = 0x8017;
        public static UInt32 GL_CONVOLUTION_WIDTH = 0x8018;
        public static UInt32 GL_CONVOLUTION_HEIGHT = 0x8019;
        public static UInt32 GL_MAX_CONVOLUTION_WIDTH = 0x801A;
        public static UInt32 GL_MAX_CONVOLUTION_HEIGHT = 0x801B;
        public static UInt32 GL_POST_CONVOLUTION_RED_SCALE = 0x801C;
        public static UInt32 GL_POST_CONVOLUTION_GREEN_SCALE = 0x801D;
        public static UInt32 GL_POST_CONVOLUTION_BLUE_SCALE = 0x801E;
        public static UInt32 GL_POST_CONVOLUTION_ALPHA_SCALE = 0x801F;
        public static UInt32 GL_POST_CONVOLUTION_RED_BIAS = 0x8020;
        public static UInt32 GL_POST_CONVOLUTION_GREEN_BIAS = 0x8021;
        public static UInt32 GL_POST_CONVOLUTION_BLUE_BIAS = 0x8022;
        public static UInt32 GL_POST_CONVOLUTION_ALPHA_BIAS = 0x8023;
        public static UInt32 GL_HISTOGRAM = 0x8024;
        public static UInt32 GL_PROXY_HISTOGRAM = 0x8025;
        public static UInt32 GL_HISTOGRAM_WIDTH = 0x8026;
        public static UInt32 GL_HISTOGRAM_FORMAT = 0x8027;
        public static UInt32 GL_HISTOGRAM_RED_SIZE = 0x8028;
        public static UInt32 GL_HISTOGRAM_GREEN_SIZE = 0x8029;
        public static UInt32 GL_HISTOGRAM_BLUE_SIZE = 0x802A;
        public static UInt32 GL_HISTOGRAM_ALPHA_SIZE = 0x802B;
        public static UInt32 GL_HISTOGRAM_LUMINANCE_SIZE = 0x802C;
        public static UInt32 GL_HISTOGRAM_SINK = 0x802D;
        public static UInt32 GL_MINMAX = 0x802E;
        public static UInt32 GL_MINMAX_FORMAT = 0x802F;
        public static UInt32 GL_MINMAX_SINK = 0x8030;
        public static UInt32 GL_TABLE_TOO_LARGE = 0x8031;
        public static UInt32 GL_COLOR_MATRIX = 0x80B1;
        public static UInt32 GL_COLOR_MATRIX_STACK_DEPTH = 0x80B2;
        public static UInt32 GL_MAX_COLOR_MATRIX_STACK_DEPTH = 0x80B3;
        public static UInt32 GL_POST_COLOR_MATRIX_RED_SCALE = 0x80B4;
        public static UInt32 GL_POST_COLOR_MATRIX_GREEN_SCALE = 0x80B5;
        public static UInt32 GL_POST_COLOR_MATRIX_BLUE_SCALE = 0x80B6;
        public static UInt32 GL_POST_COLOR_MATRIX_ALPHA_SCALE = 0x80B7;
        public static UInt32 GL_POST_COLOR_MATRIX_RED_BIAS = 0x80B8;
        public static UInt32 GL_POST_COLOR_MATRIX_GREEN_BIAS = 0x80B9;
        public static UInt32 GL_POST_COLOR_MATRIX_BLUE_BIAS = 0x80BA;
        public static UInt32 GL_POST_COLOR_MATRIX_ALPHA_BIAS = 0x80BB;
        public static UInt32 GL_COLOR_TABLE = 0x80D0;
        public static UInt32 GL_POST_CONVOLUTION_COLOR_TABLE = 0x80D1;
        public static UInt32 GL_POST_COLOR_MATRIX_COLOR_TABLE = 0x80D2;
        public static UInt32 GL_PROXY_COLOR_TABLE = 0x80D3;
        public static UInt32 GL_PROXY_POST_CONVOLUTION_COLOR_TABLE = 0x80D4;
        public static UInt32 GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE = 0x80D5;
        public static UInt32 GL_COLOR_TABLE_SCALE = 0x80D6;
        public static UInt32 GL_COLOR_TABLE_BIAS = 0x80D7;
        public static UInt32 GL_COLOR_TABLE_FORMAT = 0x80D8;
        public static UInt32 GL_COLOR_TABLE_WIDTH = 0x80D9;
        public static UInt32 GL_COLOR_TABLE_RED_SIZE = 0x80DA;
        public static UInt32 GL_COLOR_TABLE_GREEN_SIZE = 0x80DB;
        public static UInt32 GL_COLOR_TABLE_BLUE_SIZE = 0x80DC;
        public static UInt32 GL_COLOR_TABLE_ALPHA_SIZE = 0x80DD;
        public static UInt32 GL_COLOR_TABLE_LUMINANCE_SIZE = 0x80DE;
        public static UInt32 GL_COLOR_TABLE_INTENSITY_SIZE = 0x80DF;
        public static UInt32 GL_CONSTANT_BORDER = 0x8151;
        public static UInt32 GL_REPLICATE_BORDER = 0x8153;
        public static UInt32 GL_CONVOLUTION_BORDER_COLOR = 0x8154;
        #endregion

        #region Commands
        internal delegate void glBlendColorFunc(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha);
        internal static glBlendColorFunc glBlendColorPtr;
        internal static void loadBlendColor()
        {
            try
            {
                glBlendColorPtr = (glBlendColorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendColor"), typeof(glBlendColorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendColor'.");
            }
        }
        public static void glBlendColor(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha) => glBlendColorPtr(@red, @green, @blue, @alpha);

        internal delegate void glBlendEquationFunc(GLenum @mode);
        internal static glBlendEquationFunc glBlendEquationPtr;
        internal static void loadBlendEquation()
        {
            try
            {
                glBlendEquationPtr = (glBlendEquationFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendEquation"), typeof(glBlendEquationFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendEquation'.");
            }
        }
        public static void glBlendEquation(GLenum @mode) => glBlendEquationPtr(@mode);

        internal delegate void glColorTableFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLenum @format, GLenum @type, const void * @table);
        internal static glColorTableFunc glColorTablePtr;
        internal static void loadColorTable()
        {
            try
            {
                glColorTablePtr = (glColorTableFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorTable"), typeof(glColorTableFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorTable'.");
            }
        }
        public static void glColorTable(GLenum @target, GLenum @internalformat, GLsizei @width, GLenum @format, GLenum @type, const void * @table) => glColorTablePtr(@target, @internalformat, @width, @format, @type, @table);

        internal delegate void glColorTableParameterfvFunc(GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glColorTableParameterfvFunc glColorTableParameterfvPtr;
        internal static void loadColorTableParameterfv()
        {
            try
            {
                glColorTableParameterfvPtr = (glColorTableParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorTableParameterfv"), typeof(glColorTableParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorTableParameterfv'.");
            }
        }
        public static void glColorTableParameterfv(GLenum @target, GLenum @pname, const GLfloat * @params) => glColorTableParameterfvPtr(@target, @pname, @params);

        internal delegate void glColorTableParameterivFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glColorTableParameterivFunc glColorTableParameterivPtr;
        internal static void loadColorTableParameteriv()
        {
            try
            {
                glColorTableParameterivPtr = (glColorTableParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorTableParameteriv"), typeof(glColorTableParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorTableParameteriv'.");
            }
        }
        public static void glColorTableParameteriv(GLenum @target, GLenum @pname, const GLint * @params) => glColorTableParameterivPtr(@target, @pname, @params);

        internal delegate void glCopyColorTableFunc(GLenum @target, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width);
        internal static glCopyColorTableFunc glCopyColorTablePtr;
        internal static void loadCopyColorTable()
        {
            try
            {
                glCopyColorTablePtr = (glCopyColorTableFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyColorTable"), typeof(glCopyColorTableFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyColorTable'.");
            }
        }
        public static void glCopyColorTable(GLenum @target, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width) => glCopyColorTablePtr(@target, @internalformat, @x, @y, @width);

        internal delegate void glGetColorTableFunc(GLenum @target, GLenum @format, GLenum @type, void * @table);
        internal static glGetColorTableFunc glGetColorTablePtr;
        internal static void loadGetColorTable()
        {
            try
            {
                glGetColorTablePtr = (glGetColorTableFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetColorTable"), typeof(glGetColorTableFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetColorTable'.");
            }
        }
        public static void glGetColorTable(GLenum @target, GLenum @format, GLenum @type, void * @table) => glGetColorTablePtr(@target, @format, @type, @table);

        internal delegate void glGetColorTableParameterfvFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetColorTableParameterfvFunc glGetColorTableParameterfvPtr;
        internal static void loadGetColorTableParameterfv()
        {
            try
            {
                glGetColorTableParameterfvPtr = (glGetColorTableParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetColorTableParameterfv"), typeof(glGetColorTableParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetColorTableParameterfv'.");
            }
        }
        public static void glGetColorTableParameterfv(GLenum @target, GLenum @pname, GLfloat * @params) => glGetColorTableParameterfvPtr(@target, @pname, @params);

        internal delegate void glGetColorTableParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetColorTableParameterivFunc glGetColorTableParameterivPtr;
        internal static void loadGetColorTableParameteriv()
        {
            try
            {
                glGetColorTableParameterivPtr = (glGetColorTableParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetColorTableParameteriv"), typeof(glGetColorTableParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetColorTableParameteriv'.");
            }
        }
        public static void glGetColorTableParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetColorTableParameterivPtr(@target, @pname, @params);

        internal delegate void glColorSubTableFunc(GLenum @target, GLsizei @start, GLsizei @count, GLenum @format, GLenum @type, const void * @data);
        internal static glColorSubTableFunc glColorSubTablePtr;
        internal static void loadColorSubTable()
        {
            try
            {
                glColorSubTablePtr = (glColorSubTableFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorSubTable"), typeof(glColorSubTableFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorSubTable'.");
            }
        }
        public static void glColorSubTable(GLenum @target, GLsizei @start, GLsizei @count, GLenum @format, GLenum @type, const void * @data) => glColorSubTablePtr(@target, @start, @count, @format, @type, @data);

        internal delegate void glCopyColorSubTableFunc(GLenum @target, GLsizei @start, GLint @x, GLint @y, GLsizei @width);
        internal static glCopyColorSubTableFunc glCopyColorSubTablePtr;
        internal static void loadCopyColorSubTable()
        {
            try
            {
                glCopyColorSubTablePtr = (glCopyColorSubTableFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyColorSubTable"), typeof(glCopyColorSubTableFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyColorSubTable'.");
            }
        }
        public static void glCopyColorSubTable(GLenum @target, GLsizei @start, GLint @x, GLint @y, GLsizei @width) => glCopyColorSubTablePtr(@target, @start, @x, @y, @width);

        internal delegate void glConvolutionFilter1DFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLenum @format, GLenum @type, const void * @image);
        internal static glConvolutionFilter1DFunc glConvolutionFilter1DPtr;
        internal static void loadConvolutionFilter1D()
        {
            try
            {
                glConvolutionFilter1DPtr = (glConvolutionFilter1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionFilter1D"), typeof(glConvolutionFilter1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionFilter1D'.");
            }
        }
        public static void glConvolutionFilter1D(GLenum @target, GLenum @internalformat, GLsizei @width, GLenum @format, GLenum @type, const void * @image) => glConvolutionFilter1DPtr(@target, @internalformat, @width, @format, @type, @image);

        internal delegate void glConvolutionFilter2DFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @image);
        internal static glConvolutionFilter2DFunc glConvolutionFilter2DPtr;
        internal static void loadConvolutionFilter2D()
        {
            try
            {
                glConvolutionFilter2DPtr = (glConvolutionFilter2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionFilter2D"), typeof(glConvolutionFilter2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionFilter2D'.");
            }
        }
        public static void glConvolutionFilter2D(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @image) => glConvolutionFilter2DPtr(@target, @internalformat, @width, @height, @format, @type, @image);

        internal delegate void glConvolutionParameterfFunc(GLenum @target, GLenum @pname, GLfloat @params);
        internal static glConvolutionParameterfFunc glConvolutionParameterfPtr;
        internal static void loadConvolutionParameterf()
        {
            try
            {
                glConvolutionParameterfPtr = (glConvolutionParameterfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionParameterf"), typeof(glConvolutionParameterfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionParameterf'.");
            }
        }
        public static void glConvolutionParameterf(GLenum @target, GLenum @pname, GLfloat @params) => glConvolutionParameterfPtr(@target, @pname, @params);

        internal delegate void glConvolutionParameterfvFunc(GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glConvolutionParameterfvFunc glConvolutionParameterfvPtr;
        internal static void loadConvolutionParameterfv()
        {
            try
            {
                glConvolutionParameterfvPtr = (glConvolutionParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionParameterfv"), typeof(glConvolutionParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionParameterfv'.");
            }
        }
        public static void glConvolutionParameterfv(GLenum @target, GLenum @pname, const GLfloat * @params) => glConvolutionParameterfvPtr(@target, @pname, @params);

        internal delegate void glConvolutionParameteriFunc(GLenum @target, GLenum @pname, GLint @params);
        internal static glConvolutionParameteriFunc glConvolutionParameteriPtr;
        internal static void loadConvolutionParameteri()
        {
            try
            {
                glConvolutionParameteriPtr = (glConvolutionParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionParameteri"), typeof(glConvolutionParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionParameteri'.");
            }
        }
        public static void glConvolutionParameteri(GLenum @target, GLenum @pname, GLint @params) => glConvolutionParameteriPtr(@target, @pname, @params);

        internal delegate void glConvolutionParameterivFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glConvolutionParameterivFunc glConvolutionParameterivPtr;
        internal static void loadConvolutionParameteriv()
        {
            try
            {
                glConvolutionParameterivPtr = (glConvolutionParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glConvolutionParameteriv"), typeof(glConvolutionParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glConvolutionParameteriv'.");
            }
        }
        public static void glConvolutionParameteriv(GLenum @target, GLenum @pname, const GLint * @params) => glConvolutionParameterivPtr(@target, @pname, @params);

        internal delegate void glCopyConvolutionFilter1DFunc(GLenum @target, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width);
        internal static glCopyConvolutionFilter1DFunc glCopyConvolutionFilter1DPtr;
        internal static void loadCopyConvolutionFilter1D()
        {
            try
            {
                glCopyConvolutionFilter1DPtr = (glCopyConvolutionFilter1DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyConvolutionFilter1D"), typeof(glCopyConvolutionFilter1DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyConvolutionFilter1D'.");
            }
        }
        public static void glCopyConvolutionFilter1D(GLenum @target, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width) => glCopyConvolutionFilter1DPtr(@target, @internalformat, @x, @y, @width);

        internal delegate void glCopyConvolutionFilter2DFunc(GLenum @target, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyConvolutionFilter2DFunc glCopyConvolutionFilter2DPtr;
        internal static void loadCopyConvolutionFilter2D()
        {
            try
            {
                glCopyConvolutionFilter2DPtr = (glCopyConvolutionFilter2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyConvolutionFilter2D"), typeof(glCopyConvolutionFilter2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyConvolutionFilter2D'.");
            }
        }
        public static void glCopyConvolutionFilter2D(GLenum @target, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyConvolutionFilter2DPtr(@target, @internalformat, @x, @y, @width, @height);

        internal delegate void glGetConvolutionFilterFunc(GLenum @target, GLenum @format, GLenum @type, void * @image);
        internal static glGetConvolutionFilterFunc glGetConvolutionFilterPtr;
        internal static void loadGetConvolutionFilter()
        {
            try
            {
                glGetConvolutionFilterPtr = (glGetConvolutionFilterFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetConvolutionFilter"), typeof(glGetConvolutionFilterFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetConvolutionFilter'.");
            }
        }
        public static void glGetConvolutionFilter(GLenum @target, GLenum @format, GLenum @type, void * @image) => glGetConvolutionFilterPtr(@target, @format, @type, @image);

        internal delegate void glGetConvolutionParameterfvFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetConvolutionParameterfvFunc glGetConvolutionParameterfvPtr;
        internal static void loadGetConvolutionParameterfv()
        {
            try
            {
                glGetConvolutionParameterfvPtr = (glGetConvolutionParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetConvolutionParameterfv"), typeof(glGetConvolutionParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetConvolutionParameterfv'.");
            }
        }
        public static void glGetConvolutionParameterfv(GLenum @target, GLenum @pname, GLfloat * @params) => glGetConvolutionParameterfvPtr(@target, @pname, @params);

        internal delegate void glGetConvolutionParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetConvolutionParameterivFunc glGetConvolutionParameterivPtr;
        internal static void loadGetConvolutionParameteriv()
        {
            try
            {
                glGetConvolutionParameterivPtr = (glGetConvolutionParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetConvolutionParameteriv"), typeof(glGetConvolutionParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetConvolutionParameteriv'.");
            }
        }
        public static void glGetConvolutionParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetConvolutionParameterivPtr(@target, @pname, @params);

        internal delegate void glGetSeparableFilterFunc(GLenum @target, GLenum @format, GLenum @type, void * @row, void * @column, void * @span);
        internal static glGetSeparableFilterFunc glGetSeparableFilterPtr;
        internal static void loadGetSeparableFilter()
        {
            try
            {
                glGetSeparableFilterPtr = (glGetSeparableFilterFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSeparableFilter"), typeof(glGetSeparableFilterFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSeparableFilter'.");
            }
        }
        public static void glGetSeparableFilter(GLenum @target, GLenum @format, GLenum @type, void * @row, void * @column, void * @span) => glGetSeparableFilterPtr(@target, @format, @type, @row, @column, @span);

        internal delegate void glSeparableFilter2DFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @row, const void * @column);
        internal static glSeparableFilter2DFunc glSeparableFilter2DPtr;
        internal static void loadSeparableFilter2D()
        {
            try
            {
                glSeparableFilter2DPtr = (glSeparableFilter2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSeparableFilter2D"), typeof(glSeparableFilter2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSeparableFilter2D'.");
            }
        }
        public static void glSeparableFilter2D(GLenum @target, GLenum @internalformat, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @row, const void * @column) => glSeparableFilter2DPtr(@target, @internalformat, @width, @height, @format, @type, @row, @column);

        internal delegate void glGetHistogramFunc(GLenum @target, GLboolean @reset, GLenum @format, GLenum @type, void * @values);
        internal static glGetHistogramFunc glGetHistogramPtr;
        internal static void loadGetHistogram()
        {
            try
            {
                glGetHistogramPtr = (glGetHistogramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetHistogram"), typeof(glGetHistogramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetHistogram'.");
            }
        }
        public static void glGetHistogram(GLenum @target, GLboolean @reset, GLenum @format, GLenum @type, void * @values) => glGetHistogramPtr(@target, @reset, @format, @type, @values);

        internal delegate void glGetHistogramParameterfvFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetHistogramParameterfvFunc glGetHistogramParameterfvPtr;
        internal static void loadGetHistogramParameterfv()
        {
            try
            {
                glGetHistogramParameterfvPtr = (glGetHistogramParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetHistogramParameterfv"), typeof(glGetHistogramParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetHistogramParameterfv'.");
            }
        }
        public static void glGetHistogramParameterfv(GLenum @target, GLenum @pname, GLfloat * @params) => glGetHistogramParameterfvPtr(@target, @pname, @params);

        internal delegate void glGetHistogramParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetHistogramParameterivFunc glGetHistogramParameterivPtr;
        internal static void loadGetHistogramParameteriv()
        {
            try
            {
                glGetHistogramParameterivPtr = (glGetHistogramParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetHistogramParameteriv"), typeof(glGetHistogramParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetHistogramParameteriv'.");
            }
        }
        public static void glGetHistogramParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetHistogramParameterivPtr(@target, @pname, @params);

        internal delegate void glGetMinmaxFunc(GLenum @target, GLboolean @reset, GLenum @format, GLenum @type, void * @values);
        internal static glGetMinmaxFunc glGetMinmaxPtr;
        internal static void loadGetMinmax()
        {
            try
            {
                glGetMinmaxPtr = (glGetMinmaxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMinmax"), typeof(glGetMinmaxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMinmax'.");
            }
        }
        public static void glGetMinmax(GLenum @target, GLboolean @reset, GLenum @format, GLenum @type, void * @values) => glGetMinmaxPtr(@target, @reset, @format, @type, @values);

        internal delegate void glGetMinmaxParameterfvFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetMinmaxParameterfvFunc glGetMinmaxParameterfvPtr;
        internal static void loadGetMinmaxParameterfv()
        {
            try
            {
                glGetMinmaxParameterfvPtr = (glGetMinmaxParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMinmaxParameterfv"), typeof(glGetMinmaxParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMinmaxParameterfv'.");
            }
        }
        public static void glGetMinmaxParameterfv(GLenum @target, GLenum @pname, GLfloat * @params) => glGetMinmaxParameterfvPtr(@target, @pname, @params);

        internal delegate void glGetMinmaxParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetMinmaxParameterivFunc glGetMinmaxParameterivPtr;
        internal static void loadGetMinmaxParameteriv()
        {
            try
            {
                glGetMinmaxParameterivPtr = (glGetMinmaxParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMinmaxParameteriv"), typeof(glGetMinmaxParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMinmaxParameteriv'.");
            }
        }
        public static void glGetMinmaxParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetMinmaxParameterivPtr(@target, @pname, @params);

        internal delegate void glHistogramFunc(GLenum @target, GLsizei @width, GLenum @internalformat, GLboolean @sink);
        internal static glHistogramFunc glHistogramPtr;
        internal static void loadHistogram()
        {
            try
            {
                glHistogramPtr = (glHistogramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glHistogram"), typeof(glHistogramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glHistogram'.");
            }
        }
        public static void glHistogram(GLenum @target, GLsizei @width, GLenum @internalformat, GLboolean @sink) => glHistogramPtr(@target, @width, @internalformat, @sink);

        internal delegate void glMinmaxFunc(GLenum @target, GLenum @internalformat, GLboolean @sink);
        internal static glMinmaxFunc glMinmaxPtr;
        internal static void loadMinmax()
        {
            try
            {
                glMinmaxPtr = (glMinmaxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMinmax"), typeof(glMinmaxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMinmax'.");
            }
        }
        public static void glMinmax(GLenum @target, GLenum @internalformat, GLboolean @sink) => glMinmaxPtr(@target, @internalformat, @sink);

        internal delegate void glResetHistogramFunc(GLenum @target);
        internal static glResetHistogramFunc glResetHistogramPtr;
        internal static void loadResetHistogram()
        {
            try
            {
                glResetHistogramPtr = (glResetHistogramFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glResetHistogram"), typeof(glResetHistogramFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glResetHistogram'.");
            }
        }
        public static void glResetHistogram(GLenum @target) => glResetHistogramPtr(@target);

        internal delegate void glResetMinmaxFunc(GLenum @target);
        internal static glResetMinmaxFunc glResetMinmaxPtr;
        internal static void loadResetMinmax()
        {
            try
            {
                glResetMinmaxPtr = (glResetMinmaxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glResetMinmax"), typeof(glResetMinmaxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glResetMinmax'.");
            }
        }
        public static void glResetMinmax(GLenum @target) => glResetMinmaxPtr(@target);
        #endregion
    }
}
