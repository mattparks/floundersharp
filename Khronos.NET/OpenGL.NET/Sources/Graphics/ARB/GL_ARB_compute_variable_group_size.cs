using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_compute_variable_group_size
    {
        #region Interop
        static GL_ARB_compute_variable_group_size()
        {
            Console.WriteLine("Initalising GL_ARB_compute_variable_group_size interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadDispatchComputeGroupSizeARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_COMPUTE_VARIABLE_GROUP_INVOCATIONS_ARB = 0x9344;
        public static UInt32 GL_MAX_COMPUTE_FIXED_GROUP_INVOCATIONS_ARB = 0x90EB;
        public static UInt32 GL_MAX_COMPUTE_VARIABLE_GROUP_SIZE_ARB = 0x9345;
        public static UInt32 GL_MAX_COMPUTE_FIXED_GROUP_SIZE_ARB = 0x91BF;
        #endregion

        #region Commands
        internal delegate void glDispatchComputeGroupSizeARBFunc(GLuint @num_groups_x, GLuint @num_groups_y, GLuint @num_groups_z, GLuint @group_size_x, GLuint @group_size_y, GLuint @group_size_z);
        internal static glDispatchComputeGroupSizeARBFunc glDispatchComputeGroupSizeARBPtr;
        internal static void loadDispatchComputeGroupSizeARB()
        {
            try
            {
                glDispatchComputeGroupSizeARBPtr = (glDispatchComputeGroupSizeARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDispatchComputeGroupSizeARB"), typeof(glDispatchComputeGroupSizeARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDispatchComputeGroupSizeARB'.");
            }
        }
        public static void glDispatchComputeGroupSizeARB(GLuint @num_groups_x, GLuint @num_groups_y, GLuint @num_groups_z, GLuint @group_size_x, GLuint @group_size_y, GLuint @group_size_z) => glDispatchComputeGroupSizeARBPtr(@num_groups_x, @num_groups_y, @num_groups_z, @group_size_x, @group_size_y, @group_size_z);
        #endregion
    }
}
