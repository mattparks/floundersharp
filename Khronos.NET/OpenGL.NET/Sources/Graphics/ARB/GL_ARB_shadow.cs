using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_shadow
    {
        #region Interop
        static GL_ARB_shadow()
        {
            Console.WriteLine("Initalising GL_ARB_shadow interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_COMPARE_MODE_ARB = 0x884C;
        public static UInt32 GL_TEXTURE_COMPARE_FUNC_ARB = 0x884D;
        public static UInt32 GL_COMPARE_R_TO_TEXTURE_ARB = 0x884E;
        #endregion

        #region Commands
        #endregion
    }
}
