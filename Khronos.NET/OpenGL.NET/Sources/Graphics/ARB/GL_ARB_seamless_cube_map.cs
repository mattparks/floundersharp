using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_seamless_cube_map
    {
        #region Interop
        static GL_ARB_seamless_cube_map()
        {
            Console.WriteLine("Initalising GL_ARB_seamless_cube_map interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_CUBE_MAP_SEAMLESS = 0x884F;
        #endregion

        #region Commands
        #endregion
    }
}
