using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_sync
    {
        #region Interop
        static GL_ARB_sync()
        {
            Console.WriteLine("Initalising GL_ARB_sync interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFenceSync();
            loadIsSync();
            loadDeleteSync();
            loadClientWaitSync();
            loadWaitSync();
            loadGetInteger64v();
            loadGetSynciv();
        }
        #endregion

        #region Enums
        public static UInt32 GL_MAX_SERVER_WAIT_TIMEOUT = 0x9111;
        public static UInt32 GL_OBJECT_TYPE = 0x9112;
        public static UInt32 GL_SYNC_CONDITION = 0x9113;
        public static UInt32 GL_SYNC_STATUS = 0x9114;
        public static UInt32 GL_SYNC_FLAGS = 0x9115;
        public static UInt32 GL_SYNC_FENCE = 0x9116;
        public static UInt32 GL_SYNC_GPU_COMMANDS_COMPLETE = 0x9117;
        public static UInt32 GL_UNSIGNALED = 0x9118;
        public static UInt32 GL_SIGNALED = 0x9119;
        public static UInt32 GL_ALREADY_SIGNALED = 0x911A;
        public static UInt32 GL_TIMEOUT_EXPIRED = 0x911B;
        public static UInt32 GL_CONDITION_SATISFIED = 0x911C;
        public static UInt32 GL_WAIT_FAILED = 0x911D;
        public static UInt32 GL_SYNC_FLUSH_COMMANDS_BIT = 0x00000001;
        public static UInt64 GL_TIMEOUT_IGNORED = 0xFFFFFFFFFFFFFFFF;
        #endregion

        #region Commands
        internal delegate GLsync glFenceSyncFunc(GLenum @condition, GLbitfield @flags);
        internal static glFenceSyncFunc glFenceSyncPtr;
        internal static void loadFenceSync()
        {
            try
            {
                glFenceSyncPtr = (glFenceSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFenceSync"), typeof(glFenceSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFenceSync'.");
            }
        }
        public static GLsync glFenceSync(GLenum @condition, GLbitfield @flags) => glFenceSyncPtr(@condition, @flags);

        internal delegate GLboolean glIsSyncFunc(GLsync @sync);
        internal static glIsSyncFunc glIsSyncPtr;
        internal static void loadIsSync()
        {
            try
            {
                glIsSyncPtr = (glIsSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsSync"), typeof(glIsSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsSync'.");
            }
        }
        public static GLboolean glIsSync(GLsync @sync) => glIsSyncPtr(@sync);

        internal delegate void glDeleteSyncFunc(GLsync @sync);
        internal static glDeleteSyncFunc glDeleteSyncPtr;
        internal static void loadDeleteSync()
        {
            try
            {
                glDeleteSyncPtr = (glDeleteSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteSync"), typeof(glDeleteSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteSync'.");
            }
        }
        public static void glDeleteSync(GLsync @sync) => glDeleteSyncPtr(@sync);

        internal delegate GLenum glClientWaitSyncFunc(GLsync @sync, GLbitfield @flags, GLuint64 @timeout);
        internal static glClientWaitSyncFunc glClientWaitSyncPtr;
        internal static void loadClientWaitSync()
        {
            try
            {
                glClientWaitSyncPtr = (glClientWaitSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClientWaitSync"), typeof(glClientWaitSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClientWaitSync'.");
            }
        }
        public static GLenum glClientWaitSync(GLsync @sync, GLbitfield @flags, GLuint64 @timeout) => glClientWaitSyncPtr(@sync, @flags, @timeout);

        internal delegate void glWaitSyncFunc(GLsync @sync, GLbitfield @flags, GLuint64 @timeout);
        internal static glWaitSyncFunc glWaitSyncPtr;
        internal static void loadWaitSync()
        {
            try
            {
                glWaitSyncPtr = (glWaitSyncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWaitSync"), typeof(glWaitSyncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWaitSync'.");
            }
        }
        public static void glWaitSync(GLsync @sync, GLbitfield @flags, GLuint64 @timeout) => glWaitSyncPtr(@sync, @flags, @timeout);

        internal delegate void glGetInteger64vFunc(GLenum @pname, GLint64 * @data);
        internal static glGetInteger64vFunc glGetInteger64vPtr;
        internal static void loadGetInteger64v()
        {
            try
            {
                glGetInteger64vPtr = (glGetInteger64vFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetInteger64v"), typeof(glGetInteger64vFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetInteger64v'.");
            }
        }
        public static void glGetInteger64v(GLenum @pname, GLint64 * @data) => glGetInteger64vPtr(@pname, @data);

        internal delegate void glGetSyncivFunc(GLsync @sync, GLenum @pname, GLsizei @bufSize, GLsizei * @length, GLint * @values);
        internal static glGetSyncivFunc glGetSyncivPtr;
        internal static void loadGetSynciv()
        {
            try
            {
                glGetSyncivPtr = (glGetSyncivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetSynciv"), typeof(glGetSyncivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetSynciv'.");
            }
        }
        public static void glGetSynciv(GLsync @sync, GLenum @pname, GLsizei @bufSize, GLsizei * @length, GLint * @values) => glGetSyncivPtr(@sync, @pname, @bufSize, @length, @values);
        #endregion
    }
}
