using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_shader_storage_buffer_object
    {
        #region Interop
        static GL_ARB_shader_storage_buffer_object()
        {
            Console.WriteLine("Initalising GL_ARB_shader_storage_buffer_object interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadShaderStorageBlockBinding();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SHADER_STORAGE_BUFFER = 0x90D2;
        public static UInt32 GL_SHADER_STORAGE_BUFFER_BINDING = 0x90D3;
        public static UInt32 GL_SHADER_STORAGE_BUFFER_START = 0x90D4;
        public static UInt32 GL_SHADER_STORAGE_BUFFER_SIZE = 0x90D5;
        public static UInt32 GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS = 0x90D6;
        public static UInt32 GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS = 0x90D7;
        public static UInt32 GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS = 0x90D8;
        public static UInt32 GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS = 0x90D9;
        public static UInt32 GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS = 0x90DA;
        public static UInt32 GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS = 0x90DB;
        public static UInt32 GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS = 0x90DC;
        public static UInt32 GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS = 0x90DD;
        public static UInt32 GL_MAX_SHADER_STORAGE_BLOCK_SIZE = 0x90DE;
        public static UInt32 GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT = 0x90DF;
        public static UInt32 GL_SHADER_STORAGE_BARRIER_BIT = 0x00002000;
        public static UInt32 GL_MAX_COMBINED_SHADER_OUTPUT_RESOURCES = 0x8F39;
        public static UInt32 GL_MAX_COMBINED_IMAGE_UNITS_AND_FRAGMENT_OUTPUTS = 0x8F39;
        #endregion

        #region Commands
        internal delegate void glShaderStorageBlockBindingFunc(GLuint @program, GLuint @storageBlockIndex, GLuint @storageBlockBinding);
        internal static glShaderStorageBlockBindingFunc glShaderStorageBlockBindingPtr;
        internal static void loadShaderStorageBlockBinding()
        {
            try
            {
                glShaderStorageBlockBindingPtr = (glShaderStorageBlockBindingFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShaderStorageBlockBinding"), typeof(glShaderStorageBlockBindingFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShaderStorageBlockBinding'.");
            }
        }
        public static void glShaderStorageBlockBinding(GLuint @program, GLuint @storageBlockIndex, GLuint @storageBlockBinding) => glShaderStorageBlockBindingPtr(@program, @storageBlockIndex, @storageBlockBinding);
        #endregion
    }
}
