using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_shading_language_include
    {
        #region Interop
        static GL_ARB_shading_language_include()
        {
            Console.WriteLine("Initalising GL_ARB_shading_language_include interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadNamedStringARB();
            loadDeleteNamedStringARB();
            loadCompileShaderIncludeARB();
            loadIsNamedStringARB();
            loadGetNamedStringARB();
            loadGetNamedStringivARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_SHADER_INCLUDE_ARB = 0x8DAE;
        public static UInt32 GL_NAMED_STRING_LENGTH_ARB = 0x8DE9;
        public static UInt32 GL_NAMED_STRING_TYPE_ARB = 0x8DEA;
        #endregion

        #region Commands
        internal delegate void glNamedStringARBFunc(GLenum @type, GLint @namelen, const GLchar * @name, GLint @stringlen, const GLchar * @string);
        internal static glNamedStringARBFunc glNamedStringARBPtr;
        internal static void loadNamedStringARB()
        {
            try
            {
                glNamedStringARBPtr = (glNamedStringARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNamedStringARB"), typeof(glNamedStringARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNamedStringARB'.");
            }
        }
        public static void glNamedStringARB(GLenum @type, GLint @namelen, const GLchar * @name, GLint @stringlen, const GLchar * @string) => glNamedStringARBPtr(@type, @namelen, @name, @stringlen, @string);

        internal delegate void glDeleteNamedStringARBFunc(GLint @namelen, const GLchar * @name);
        internal static glDeleteNamedStringARBFunc glDeleteNamedStringARBPtr;
        internal static void loadDeleteNamedStringARB()
        {
            try
            {
                glDeleteNamedStringARBPtr = (glDeleteNamedStringARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteNamedStringARB"), typeof(glDeleteNamedStringARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteNamedStringARB'.");
            }
        }
        public static void glDeleteNamedStringARB(GLint @namelen, const GLchar * @name) => glDeleteNamedStringARBPtr(@namelen, @name);

        internal delegate void glCompileShaderIncludeARBFunc(GLuint @shader, GLsizei @count, const GLchar *const* @path, const GLint * @length);
        internal static glCompileShaderIncludeARBFunc glCompileShaderIncludeARBPtr;
        internal static void loadCompileShaderIncludeARB()
        {
            try
            {
                glCompileShaderIncludeARBPtr = (glCompileShaderIncludeARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompileShaderIncludeARB"), typeof(glCompileShaderIncludeARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompileShaderIncludeARB'.");
            }
        }
        public static void glCompileShaderIncludeARB(GLuint @shader, GLsizei @count, const GLchar *const* @path, const GLint * @length) => glCompileShaderIncludeARBPtr(@shader, @count, @path, @length);

        internal delegate GLboolean glIsNamedStringARBFunc(GLint @namelen, const GLchar * @name);
        internal static glIsNamedStringARBFunc glIsNamedStringARBPtr;
        internal static void loadIsNamedStringARB()
        {
            try
            {
                glIsNamedStringARBPtr = (glIsNamedStringARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsNamedStringARB"), typeof(glIsNamedStringARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsNamedStringARB'.");
            }
        }
        public static GLboolean glIsNamedStringARB(GLint @namelen, const GLchar * @name) => glIsNamedStringARBPtr(@namelen, @name);

        internal delegate void glGetNamedStringARBFunc(GLint @namelen, const GLchar * @name, GLsizei @bufSize, GLint * @stringlen, GLchar * @string);
        internal static glGetNamedStringARBFunc glGetNamedStringARBPtr;
        internal static void loadGetNamedStringARB()
        {
            try
            {
                glGetNamedStringARBPtr = (glGetNamedStringARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedStringARB"), typeof(glGetNamedStringARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedStringARB'.");
            }
        }
        public static void glGetNamedStringARB(GLint @namelen, const GLchar * @name, GLsizei @bufSize, GLint * @stringlen, GLchar * @string) => glGetNamedStringARBPtr(@namelen, @name, @bufSize, @stringlen, @string);

        internal delegate void glGetNamedStringivARBFunc(GLint @namelen, const GLchar * @name, GLenum @pname, GLint * @params);
        internal static glGetNamedStringivARBFunc glGetNamedStringivARBPtr;
        internal static void loadGetNamedStringivARB()
        {
            try
            {
                glGetNamedStringivARBPtr = (glGetNamedStringivARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetNamedStringivARB"), typeof(glGetNamedStringivARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetNamedStringivARB'.");
            }
        }
        public static void glGetNamedStringivARB(GLint @namelen, const GLchar * @name, GLenum @pname, GLint * @params) => glGetNamedStringivARBPtr(@namelen, @name, @pname, @params);
        #endregion
    }
}
