using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_ARB_texture_compression
    {
        #region Interop
        static GL_ARB_texture_compression()
        {
            Console.WriteLine("Initalising GL_ARB_texture_compression interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadCompressedTexImage3DARB();
            loadCompressedTexImage2DARB();
            loadCompressedTexImage1DARB();
            loadCompressedTexSubImage3DARB();
            loadCompressedTexSubImage2DARB();
            loadCompressedTexSubImage1DARB();
            loadGetCompressedTexImageARB();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COMPRESSED_ALPHA_ARB = 0x84E9;
        public static UInt32 GL_COMPRESSED_LUMINANCE_ARB = 0x84EA;
        public static UInt32 GL_COMPRESSED_LUMINANCE_ALPHA_ARB = 0x84EB;
        public static UInt32 GL_COMPRESSED_INTENSITY_ARB = 0x84EC;
        public static UInt32 GL_COMPRESSED_RGB_ARB = 0x84ED;
        public static UInt32 GL_COMPRESSED_RGBA_ARB = 0x84EE;
        public static UInt32 GL_TEXTURE_COMPRESSION_HINT_ARB = 0x84EF;
        public static UInt32 GL_TEXTURE_COMPRESSED_IMAGE_SIZE_ARB = 0x86A0;
        public static UInt32 GL_TEXTURE_COMPRESSED_ARB = 0x86A1;
        public static UInt32 GL_NUM_COMPRESSED_TEXTURE_FORMATS_ARB = 0x86A2;
        public static UInt32 GL_COMPRESSED_TEXTURE_FORMATS_ARB = 0x86A3;
        #endregion

        #region Commands
        internal delegate void glCompressedTexImage3DARBFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexImage3DARBFunc glCompressedTexImage3DARBPtr;
        internal static void loadCompressedTexImage3DARB()
        {
            try
            {
                glCompressedTexImage3DARBPtr = (glCompressedTexImage3DARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexImage3DARB"), typeof(glCompressedTexImage3DARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexImage3DARB'.");
            }
        }
        public static void glCompressedTexImage3DARB(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLsizei @depth, GLint @border, GLsizei @imageSize, const void * @data) => glCompressedTexImage3DARBPtr(@target, @level, @internalformat, @width, @height, @depth, @border, @imageSize, @data);

        internal delegate void glCompressedTexImage2DARBFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexImage2DARBFunc glCompressedTexImage2DARBPtr;
        internal static void loadCompressedTexImage2DARB()
        {
            try
            {
                glCompressedTexImage2DARBPtr = (glCompressedTexImage2DARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexImage2DARB"), typeof(glCompressedTexImage2DARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexImage2DARB'.");
            }
        }
        public static void glCompressedTexImage2DARB(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLsizei @imageSize, const void * @data) => glCompressedTexImage2DARBPtr(@target, @level, @internalformat, @width, @height, @border, @imageSize, @data);

        internal delegate void glCompressedTexImage1DARBFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLint @border, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexImage1DARBFunc glCompressedTexImage1DARBPtr;
        internal static void loadCompressedTexImage1DARB()
        {
            try
            {
                glCompressedTexImage1DARBPtr = (glCompressedTexImage1DARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexImage1DARB"), typeof(glCompressedTexImage1DARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexImage1DARB'.");
            }
        }
        public static void glCompressedTexImage1DARB(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLint @border, GLsizei @imageSize, const void * @data) => glCompressedTexImage1DARBPtr(@target, @level, @internalformat, @width, @border, @imageSize, @data);

        internal delegate void glCompressedTexSubImage3DARBFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexSubImage3DARBFunc glCompressedTexSubImage3DARBPtr;
        internal static void loadCompressedTexSubImage3DARB()
        {
            try
            {
                glCompressedTexSubImage3DARBPtr = (glCompressedTexSubImage3DARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexSubImage3DARB"), typeof(glCompressedTexSubImage3DARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexSubImage3DARB'.");
            }
        }
        public static void glCompressedTexSubImage3DARB(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @zoffset, GLsizei @width, GLsizei @height, GLsizei @depth, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTexSubImage3DARBPtr(@target, @level, @xoffset, @yoffset, @zoffset, @width, @height, @depth, @format, @imageSize, @data);

        internal delegate void glCompressedTexSubImage2DARBFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexSubImage2DARBFunc glCompressedTexSubImage2DARBPtr;
        internal static void loadCompressedTexSubImage2DARB()
        {
            try
            {
                glCompressedTexSubImage2DARBPtr = (glCompressedTexSubImage2DARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexSubImage2DARB"), typeof(glCompressedTexSubImage2DARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexSubImage2DARB'.");
            }
        }
        public static void glCompressedTexSubImage2DARB(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTexSubImage2DARBPtr(@target, @level, @xoffset, @yoffset, @width, @height, @format, @imageSize, @data);

        internal delegate void glCompressedTexSubImage1DARBFunc(GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexSubImage1DARBFunc glCompressedTexSubImage1DARBPtr;
        internal static void loadCompressedTexSubImage1DARB()
        {
            try
            {
                glCompressedTexSubImage1DARBPtr = (glCompressedTexSubImage1DARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexSubImage1DARB"), typeof(glCompressedTexSubImage1DARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexSubImage1DARB'.");
            }
        }
        public static void glCompressedTexSubImage1DARB(GLenum @target, GLint @level, GLint @xoffset, GLsizei @width, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTexSubImage1DARBPtr(@target, @level, @xoffset, @width, @format, @imageSize, @data);

        internal delegate void glGetCompressedTexImageARBFunc(GLenum @target, GLint @level, void * @img);
        internal static glGetCompressedTexImageARBFunc glGetCompressedTexImageARBPtr;
        internal static void loadGetCompressedTexImageARB()
        {
            try
            {
                glGetCompressedTexImageARBPtr = (glGetCompressedTexImageARBFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetCompressedTexImageARB"), typeof(glGetCompressedTexImageARBFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetCompressedTexImageARB'.");
            }
        }
        public static void glGetCompressedTexImageARB(GLenum @target, GLint @level, void * @img) => glGetCompressedTexImageARBPtr(@target, @level, @img);
        #endregion
    }
}
