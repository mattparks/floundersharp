using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGI_color_matrix
    {
        #region Interop
        static GL_SGI_color_matrix()
        {
            Console.WriteLine("Initalising GL_SGI_color_matrix interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_COLOR_MATRIX_SGI = 0x80B1;
        public static UInt32 GL_COLOR_MATRIX_STACK_DEPTH_SGI = 0x80B2;
        public static UInt32 GL_MAX_COLOR_MATRIX_STACK_DEPTH_SGI = 0x80B3;
        public static UInt32 GL_POST_COLOR_MATRIX_RED_SCALE_SGI = 0x80B4;
        public static UInt32 GL_POST_COLOR_MATRIX_GREEN_SCALE_SGI = 0x80B5;
        public static UInt32 GL_POST_COLOR_MATRIX_BLUE_SCALE_SGI = 0x80B6;
        public static UInt32 GL_POST_COLOR_MATRIX_ALPHA_SCALE_SGI = 0x80B7;
        public static UInt32 GL_POST_COLOR_MATRIX_RED_BIAS_SGI = 0x80B8;
        public static UInt32 GL_POST_COLOR_MATRIX_GREEN_BIAS_SGI = 0x80B9;
        public static UInt32 GL_POST_COLOR_MATRIX_BLUE_BIAS_SGI = 0x80BA;
        public static UInt32 GL_POST_COLOR_MATRIX_ALPHA_BIAS_SGI = 0x80BB;
        #endregion

        #region Commands
        #endregion
    }
}
