using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGI_color_table
    {
        #region Interop
        static GL_SGI_color_table()
        {
            Console.WriteLine("Initalising GL_SGI_color_table interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadColorTableSGI();
            loadColorTableParameterfvSGI();
            loadColorTableParameterivSGI();
            loadCopyColorTableSGI();
            loadGetColorTableSGI();
            loadGetColorTableParameterfvSGI();
            loadGetColorTableParameterivSGI();
        }
        #endregion

        #region Enums
        public static UInt32 GL_COLOR_TABLE_SGI = 0x80D0;
        public static UInt32 GL_POST_CONVOLUTION_COLOR_TABLE_SGI = 0x80D1;
        public static UInt32 GL_POST_COLOR_MATRIX_COLOR_TABLE_SGI = 0x80D2;
        public static UInt32 GL_PROXY_COLOR_TABLE_SGI = 0x80D3;
        public static UInt32 GL_PROXY_POST_CONVOLUTION_COLOR_TABLE_SGI = 0x80D4;
        public static UInt32 GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE_SGI = 0x80D5;
        public static UInt32 GL_COLOR_TABLE_SCALE_SGI = 0x80D6;
        public static UInt32 GL_COLOR_TABLE_BIAS_SGI = 0x80D7;
        public static UInt32 GL_COLOR_TABLE_FORMAT_SGI = 0x80D8;
        public static UInt32 GL_COLOR_TABLE_WIDTH_SGI = 0x80D9;
        public static UInt32 GL_COLOR_TABLE_RED_SIZE_SGI = 0x80DA;
        public static UInt32 GL_COLOR_TABLE_GREEN_SIZE_SGI = 0x80DB;
        public static UInt32 GL_COLOR_TABLE_BLUE_SIZE_SGI = 0x80DC;
        public static UInt32 GL_COLOR_TABLE_ALPHA_SIZE_SGI = 0x80DD;
        public static UInt32 GL_COLOR_TABLE_LUMINANCE_SIZE_SGI = 0x80DE;
        public static UInt32 GL_COLOR_TABLE_INTENSITY_SIZE_SGI = 0x80DF;
        #endregion

        #region Commands
        internal delegate void glColorTableSGIFunc(GLenum @target, GLenum @internalformat, GLsizei @width, GLenum @format, GLenum @type, const void * @table);
        internal static glColorTableSGIFunc glColorTableSGIPtr;
        internal static void loadColorTableSGI()
        {
            try
            {
                glColorTableSGIPtr = (glColorTableSGIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorTableSGI"), typeof(glColorTableSGIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorTableSGI'.");
            }
        }
        public static void glColorTableSGI(GLenum @target, GLenum @internalformat, GLsizei @width, GLenum @format, GLenum @type, const void * @table) => glColorTableSGIPtr(@target, @internalformat, @width, @format, @type, @table);

        internal delegate void glColorTableParameterfvSGIFunc(GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glColorTableParameterfvSGIFunc glColorTableParameterfvSGIPtr;
        internal static void loadColorTableParameterfvSGI()
        {
            try
            {
                glColorTableParameterfvSGIPtr = (glColorTableParameterfvSGIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorTableParameterfvSGI"), typeof(glColorTableParameterfvSGIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorTableParameterfvSGI'.");
            }
        }
        public static void glColorTableParameterfvSGI(GLenum @target, GLenum @pname, const GLfloat * @params) => glColorTableParameterfvSGIPtr(@target, @pname, @params);

        internal delegate void glColorTableParameterivSGIFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glColorTableParameterivSGIFunc glColorTableParameterivSGIPtr;
        internal static void loadColorTableParameterivSGI()
        {
            try
            {
                glColorTableParameterivSGIPtr = (glColorTableParameterivSGIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorTableParameterivSGI"), typeof(glColorTableParameterivSGIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorTableParameterivSGI'.");
            }
        }
        public static void glColorTableParameterivSGI(GLenum @target, GLenum @pname, const GLint * @params) => glColorTableParameterivSGIPtr(@target, @pname, @params);

        internal delegate void glCopyColorTableSGIFunc(GLenum @target, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width);
        internal static glCopyColorTableSGIFunc glCopyColorTableSGIPtr;
        internal static void loadCopyColorTableSGI()
        {
            try
            {
                glCopyColorTableSGIPtr = (glCopyColorTableSGIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyColorTableSGI"), typeof(glCopyColorTableSGIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyColorTableSGI'.");
            }
        }
        public static void glCopyColorTableSGI(GLenum @target, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width) => glCopyColorTableSGIPtr(@target, @internalformat, @x, @y, @width);

        internal delegate void glGetColorTableSGIFunc(GLenum @target, GLenum @format, GLenum @type, void * @table);
        internal static glGetColorTableSGIFunc glGetColorTableSGIPtr;
        internal static void loadGetColorTableSGI()
        {
            try
            {
                glGetColorTableSGIPtr = (glGetColorTableSGIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetColorTableSGI"), typeof(glGetColorTableSGIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetColorTableSGI'.");
            }
        }
        public static void glGetColorTableSGI(GLenum @target, GLenum @format, GLenum @type, void * @table) => glGetColorTableSGIPtr(@target, @format, @type, @table);

        internal delegate void glGetColorTableParameterfvSGIFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetColorTableParameterfvSGIFunc glGetColorTableParameterfvSGIPtr;
        internal static void loadGetColorTableParameterfvSGI()
        {
            try
            {
                glGetColorTableParameterfvSGIPtr = (glGetColorTableParameterfvSGIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetColorTableParameterfvSGI"), typeof(glGetColorTableParameterfvSGIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetColorTableParameterfvSGI'.");
            }
        }
        public static void glGetColorTableParameterfvSGI(GLenum @target, GLenum @pname, GLfloat * @params) => glGetColorTableParameterfvSGIPtr(@target, @pname, @params);

        internal delegate void glGetColorTableParameterivSGIFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetColorTableParameterivSGIFunc glGetColorTableParameterivSGIPtr;
        internal static void loadGetColorTableParameterivSGI()
        {
            try
            {
                glGetColorTableParameterivSGIPtr = (glGetColorTableParameterivSGIFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetColorTableParameterivSGI"), typeof(glGetColorTableParameterivSGIFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetColorTableParameterivSGI'.");
            }
        }
        public static void glGetColorTableParameterivSGI(GLenum @target, GLenum @pname, GLint * @params) => glGetColorTableParameterivSGIPtr(@target, @pname, @params);
        #endregion
    }
}
