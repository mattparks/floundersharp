using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_SGI_texture_color_table
    {
        #region Interop
        static GL_SGI_texture_color_table()
        {
            Console.WriteLine("Initalising GL_SGI_texture_color_table interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_TEXTURE_COLOR_TABLE_SGI = 0x80BC;
        public static UInt32 GL_PROXY_TEXTURE_COLOR_TABLE_SGI = 0x80BD;
        #endregion

        #region Commands
        #endregion
    }
}
