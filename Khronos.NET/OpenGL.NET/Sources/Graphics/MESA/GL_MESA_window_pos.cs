using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_MESA_window_pos
    {
        #region Interop
        static GL_MESA_window_pos()
        {
            Console.WriteLine("Initalising GL_MESA_window_pos interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadWindowPos2dMESA();
            loadWindowPos2dvMESA();
            loadWindowPos2fMESA();
            loadWindowPos2fvMESA();
            loadWindowPos2iMESA();
            loadWindowPos2ivMESA();
            loadWindowPos2sMESA();
            loadWindowPos2svMESA();
            loadWindowPos3dMESA();
            loadWindowPos3dvMESA();
            loadWindowPos3fMESA();
            loadWindowPos3fvMESA();
            loadWindowPos3iMESA();
            loadWindowPos3ivMESA();
            loadWindowPos3sMESA();
            loadWindowPos3svMESA();
            loadWindowPos4dMESA();
            loadWindowPos4dvMESA();
            loadWindowPos4fMESA();
            loadWindowPos4fvMESA();
            loadWindowPos4iMESA();
            loadWindowPos4ivMESA();
            loadWindowPos4sMESA();
            loadWindowPos4svMESA();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glWindowPos2dMESAFunc(GLdouble @x, GLdouble @y);
        internal static glWindowPos2dMESAFunc glWindowPos2dMESAPtr;
        internal static void loadWindowPos2dMESA()
        {
            try
            {
                glWindowPos2dMESAPtr = (glWindowPos2dMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2dMESA"), typeof(glWindowPos2dMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2dMESA'.");
            }
        }
        public static void glWindowPos2dMESA(GLdouble @x, GLdouble @y) => glWindowPos2dMESAPtr(@x, @y);

        internal delegate void glWindowPos2dvMESAFunc(const GLdouble * @v);
        internal static glWindowPos2dvMESAFunc glWindowPos2dvMESAPtr;
        internal static void loadWindowPos2dvMESA()
        {
            try
            {
                glWindowPos2dvMESAPtr = (glWindowPos2dvMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2dvMESA"), typeof(glWindowPos2dvMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2dvMESA'.");
            }
        }
        public static void glWindowPos2dvMESA(const GLdouble * @v) => glWindowPos2dvMESAPtr(@v);

        internal delegate void glWindowPos2fMESAFunc(GLfloat @x, GLfloat @y);
        internal static glWindowPos2fMESAFunc glWindowPos2fMESAPtr;
        internal static void loadWindowPos2fMESA()
        {
            try
            {
                glWindowPos2fMESAPtr = (glWindowPos2fMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2fMESA"), typeof(glWindowPos2fMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2fMESA'.");
            }
        }
        public static void glWindowPos2fMESA(GLfloat @x, GLfloat @y) => glWindowPos2fMESAPtr(@x, @y);

        internal delegate void glWindowPos2fvMESAFunc(const GLfloat * @v);
        internal static glWindowPos2fvMESAFunc glWindowPos2fvMESAPtr;
        internal static void loadWindowPos2fvMESA()
        {
            try
            {
                glWindowPos2fvMESAPtr = (glWindowPos2fvMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2fvMESA"), typeof(glWindowPos2fvMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2fvMESA'.");
            }
        }
        public static void glWindowPos2fvMESA(const GLfloat * @v) => glWindowPos2fvMESAPtr(@v);

        internal delegate void glWindowPos2iMESAFunc(GLint @x, GLint @y);
        internal static glWindowPos2iMESAFunc glWindowPos2iMESAPtr;
        internal static void loadWindowPos2iMESA()
        {
            try
            {
                glWindowPos2iMESAPtr = (glWindowPos2iMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2iMESA"), typeof(glWindowPos2iMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2iMESA'.");
            }
        }
        public static void glWindowPos2iMESA(GLint @x, GLint @y) => glWindowPos2iMESAPtr(@x, @y);

        internal delegate void glWindowPos2ivMESAFunc(const GLint * @v);
        internal static glWindowPos2ivMESAFunc glWindowPos2ivMESAPtr;
        internal static void loadWindowPos2ivMESA()
        {
            try
            {
                glWindowPos2ivMESAPtr = (glWindowPos2ivMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2ivMESA"), typeof(glWindowPos2ivMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2ivMESA'.");
            }
        }
        public static void glWindowPos2ivMESA(const GLint * @v) => glWindowPos2ivMESAPtr(@v);

        internal delegate void glWindowPos2sMESAFunc(GLshort @x, GLshort @y);
        internal static glWindowPos2sMESAFunc glWindowPos2sMESAPtr;
        internal static void loadWindowPos2sMESA()
        {
            try
            {
                glWindowPos2sMESAPtr = (glWindowPos2sMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2sMESA"), typeof(glWindowPos2sMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2sMESA'.");
            }
        }
        public static void glWindowPos2sMESA(GLshort @x, GLshort @y) => glWindowPos2sMESAPtr(@x, @y);

        internal delegate void glWindowPos2svMESAFunc(const GLshort * @v);
        internal static glWindowPos2svMESAFunc glWindowPos2svMESAPtr;
        internal static void loadWindowPos2svMESA()
        {
            try
            {
                glWindowPos2svMESAPtr = (glWindowPos2svMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos2svMESA"), typeof(glWindowPos2svMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos2svMESA'.");
            }
        }
        public static void glWindowPos2svMESA(const GLshort * @v) => glWindowPos2svMESAPtr(@v);

        internal delegate void glWindowPos3dMESAFunc(GLdouble @x, GLdouble @y, GLdouble @z);
        internal static glWindowPos3dMESAFunc glWindowPos3dMESAPtr;
        internal static void loadWindowPos3dMESA()
        {
            try
            {
                glWindowPos3dMESAPtr = (glWindowPos3dMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3dMESA"), typeof(glWindowPos3dMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3dMESA'.");
            }
        }
        public static void glWindowPos3dMESA(GLdouble @x, GLdouble @y, GLdouble @z) => glWindowPos3dMESAPtr(@x, @y, @z);

        internal delegate void glWindowPos3dvMESAFunc(const GLdouble * @v);
        internal static glWindowPos3dvMESAFunc glWindowPos3dvMESAPtr;
        internal static void loadWindowPos3dvMESA()
        {
            try
            {
                glWindowPos3dvMESAPtr = (glWindowPos3dvMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3dvMESA"), typeof(glWindowPos3dvMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3dvMESA'.");
            }
        }
        public static void glWindowPos3dvMESA(const GLdouble * @v) => glWindowPos3dvMESAPtr(@v);

        internal delegate void glWindowPos3fMESAFunc(GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glWindowPos3fMESAFunc glWindowPos3fMESAPtr;
        internal static void loadWindowPos3fMESA()
        {
            try
            {
                glWindowPos3fMESAPtr = (glWindowPos3fMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3fMESA"), typeof(glWindowPos3fMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3fMESA'.");
            }
        }
        public static void glWindowPos3fMESA(GLfloat @x, GLfloat @y, GLfloat @z) => glWindowPos3fMESAPtr(@x, @y, @z);

        internal delegate void glWindowPos3fvMESAFunc(const GLfloat * @v);
        internal static glWindowPos3fvMESAFunc glWindowPos3fvMESAPtr;
        internal static void loadWindowPos3fvMESA()
        {
            try
            {
                glWindowPos3fvMESAPtr = (glWindowPos3fvMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3fvMESA"), typeof(glWindowPos3fvMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3fvMESA'.");
            }
        }
        public static void glWindowPos3fvMESA(const GLfloat * @v) => glWindowPos3fvMESAPtr(@v);

        internal delegate void glWindowPos3iMESAFunc(GLint @x, GLint @y, GLint @z);
        internal static glWindowPos3iMESAFunc glWindowPos3iMESAPtr;
        internal static void loadWindowPos3iMESA()
        {
            try
            {
                glWindowPos3iMESAPtr = (glWindowPos3iMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3iMESA"), typeof(glWindowPos3iMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3iMESA'.");
            }
        }
        public static void glWindowPos3iMESA(GLint @x, GLint @y, GLint @z) => glWindowPos3iMESAPtr(@x, @y, @z);

        internal delegate void glWindowPos3ivMESAFunc(const GLint * @v);
        internal static glWindowPos3ivMESAFunc glWindowPos3ivMESAPtr;
        internal static void loadWindowPos3ivMESA()
        {
            try
            {
                glWindowPos3ivMESAPtr = (glWindowPos3ivMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3ivMESA"), typeof(glWindowPos3ivMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3ivMESA'.");
            }
        }
        public static void glWindowPos3ivMESA(const GLint * @v) => glWindowPos3ivMESAPtr(@v);

        internal delegate void glWindowPos3sMESAFunc(GLshort @x, GLshort @y, GLshort @z);
        internal static glWindowPos3sMESAFunc glWindowPos3sMESAPtr;
        internal static void loadWindowPos3sMESA()
        {
            try
            {
                glWindowPos3sMESAPtr = (glWindowPos3sMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3sMESA"), typeof(glWindowPos3sMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3sMESA'.");
            }
        }
        public static void glWindowPos3sMESA(GLshort @x, GLshort @y, GLshort @z) => glWindowPos3sMESAPtr(@x, @y, @z);

        internal delegate void glWindowPos3svMESAFunc(const GLshort * @v);
        internal static glWindowPos3svMESAFunc glWindowPos3svMESAPtr;
        internal static void loadWindowPos3svMESA()
        {
            try
            {
                glWindowPos3svMESAPtr = (glWindowPos3svMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos3svMESA"), typeof(glWindowPos3svMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos3svMESA'.");
            }
        }
        public static void glWindowPos3svMESA(const GLshort * @v) => glWindowPos3svMESAPtr(@v);

        internal delegate void glWindowPos4dMESAFunc(GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w);
        internal static glWindowPos4dMESAFunc glWindowPos4dMESAPtr;
        internal static void loadWindowPos4dMESA()
        {
            try
            {
                glWindowPos4dMESAPtr = (glWindowPos4dMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos4dMESA"), typeof(glWindowPos4dMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos4dMESA'.");
            }
        }
        public static void glWindowPos4dMESA(GLdouble @x, GLdouble @y, GLdouble @z, GLdouble @w) => glWindowPos4dMESAPtr(@x, @y, @z, @w);

        internal delegate void glWindowPos4dvMESAFunc(const GLdouble * @v);
        internal static glWindowPos4dvMESAFunc glWindowPos4dvMESAPtr;
        internal static void loadWindowPos4dvMESA()
        {
            try
            {
                glWindowPos4dvMESAPtr = (glWindowPos4dvMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos4dvMESA"), typeof(glWindowPos4dvMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos4dvMESA'.");
            }
        }
        public static void glWindowPos4dvMESA(const GLdouble * @v) => glWindowPos4dvMESAPtr(@v);

        internal delegate void glWindowPos4fMESAFunc(GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w);
        internal static glWindowPos4fMESAFunc glWindowPos4fMESAPtr;
        internal static void loadWindowPos4fMESA()
        {
            try
            {
                glWindowPos4fMESAPtr = (glWindowPos4fMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos4fMESA"), typeof(glWindowPos4fMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos4fMESA'.");
            }
        }
        public static void glWindowPos4fMESA(GLfloat @x, GLfloat @y, GLfloat @z, GLfloat @w) => glWindowPos4fMESAPtr(@x, @y, @z, @w);

        internal delegate void glWindowPos4fvMESAFunc(const GLfloat * @v);
        internal static glWindowPos4fvMESAFunc glWindowPos4fvMESAPtr;
        internal static void loadWindowPos4fvMESA()
        {
            try
            {
                glWindowPos4fvMESAPtr = (glWindowPos4fvMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos4fvMESA"), typeof(glWindowPos4fvMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos4fvMESA'.");
            }
        }
        public static void glWindowPos4fvMESA(const GLfloat * @v) => glWindowPos4fvMESAPtr(@v);

        internal delegate void glWindowPos4iMESAFunc(GLint @x, GLint @y, GLint @z, GLint @w);
        internal static glWindowPos4iMESAFunc glWindowPos4iMESAPtr;
        internal static void loadWindowPos4iMESA()
        {
            try
            {
                glWindowPos4iMESAPtr = (glWindowPos4iMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos4iMESA"), typeof(glWindowPos4iMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos4iMESA'.");
            }
        }
        public static void glWindowPos4iMESA(GLint @x, GLint @y, GLint @z, GLint @w) => glWindowPos4iMESAPtr(@x, @y, @z, @w);

        internal delegate void glWindowPos4ivMESAFunc(const GLint * @v);
        internal static glWindowPos4ivMESAFunc glWindowPos4ivMESAPtr;
        internal static void loadWindowPos4ivMESA()
        {
            try
            {
                glWindowPos4ivMESAPtr = (glWindowPos4ivMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos4ivMESA"), typeof(glWindowPos4ivMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos4ivMESA'.");
            }
        }
        public static void glWindowPos4ivMESA(const GLint * @v) => glWindowPos4ivMESAPtr(@v);

        internal delegate void glWindowPos4sMESAFunc(GLshort @x, GLshort @y, GLshort @z, GLshort @w);
        internal static glWindowPos4sMESAFunc glWindowPos4sMESAPtr;
        internal static void loadWindowPos4sMESA()
        {
            try
            {
                glWindowPos4sMESAPtr = (glWindowPos4sMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos4sMESA"), typeof(glWindowPos4sMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos4sMESA'.");
            }
        }
        public static void glWindowPos4sMESA(GLshort @x, GLshort @y, GLshort @z, GLshort @w) => glWindowPos4sMESAPtr(@x, @y, @z, @w);

        internal delegate void glWindowPos4svMESAFunc(const GLshort * @v);
        internal static glWindowPos4svMESAFunc glWindowPos4svMESAPtr;
        internal static void loadWindowPos4svMESA()
        {
            try
            {
                glWindowPos4svMESAPtr = (glWindowPos4svMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glWindowPos4svMESA"), typeof(glWindowPos4svMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glWindowPos4svMESA'.");
            }
        }
        public static void glWindowPos4svMESA(const GLshort * @v) => glWindowPos4svMESAPtr(@v);
        #endregion
    }
}
