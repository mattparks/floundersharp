using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_MESA_resize_buffers
    {
        #region Interop
        static GL_MESA_resize_buffers()
        {
            Console.WriteLine("Initalising GL_MESA_resize_buffers interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadResizeBuffersMESA();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glResizeBuffersMESAFunc();
        internal static glResizeBuffersMESAFunc glResizeBuffersMESAPtr;
        internal static void loadResizeBuffersMESA()
        {
            try
            {
                glResizeBuffersMESAPtr = (glResizeBuffersMESAFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glResizeBuffersMESA"), typeof(glResizeBuffersMESAFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glResizeBuffersMESA'.");
            }
        }
        public static void glResizeBuffersMESA() => glResizeBuffersMESAPtr();
        #endregion
    }
}
