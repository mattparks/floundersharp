using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_MESA_ycbcr_texture
    {
        #region Interop
        static GL_MESA_ycbcr_texture()
        {
            Console.WriteLine("Initalising GL_MESA_ycbcr_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_UNSIGNED_SHORT_8_8_MESA = 0x85BA;
        public static UInt32 GL_UNSIGNED_SHORT_8_8_REV_MESA = 0x85BB;
        public static UInt32 GL_YCBCR_MESA = 0x8757;
        #endregion

        #region Commands
        #endregion
    }
}
