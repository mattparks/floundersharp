using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_MESA_pack_invert
    {
        #region Interop
        static GL_MESA_pack_invert()
        {
            Console.WriteLine("Initalising GL_MESA_pack_invert interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_PACK_INVERT_MESA = 0x8758;
        #endregion

        #region Commands
        #endregion
    }
}
