using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_S3_s3tc
    {
        #region Interop
        static GL_S3_s3tc()
        {
            Console.WriteLine("Initalising GL_S3_s3tc interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

        }
        #endregion

        #region Enums
        public static UInt32 GL_RGB_S3TC = 0x83A0;
        public static UInt32 GL_RGB4_S3TC = 0x83A1;
        public static UInt32 GL_RGBA_S3TC = 0x83A2;
        public static UInt32 GL_RGBA4_S3TC = 0x83A3;
        public static UInt32 GL_RGBA_DXT5_S3TC = 0x83A4;
        public static UInt32 GL_RGBA4_DXT5_S3TC = 0x83A5;
        #endregion

        #region Commands
        #endregion
    }
}
