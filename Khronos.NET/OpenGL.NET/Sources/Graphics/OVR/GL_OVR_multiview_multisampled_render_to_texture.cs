using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OVR_multiview_multisampled_render_to_texture
    {
        #region Interop
        static GL_OVR_multiview_multisampled_render_to_texture()
        {
            Console.WriteLine("Initalising GL_OVR_multiview_multisampled_render_to_texture interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFramebufferTextureMultisampleMultiviewOVR();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glFramebufferTextureMultisampleMultiviewOVRFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLsizei @samples, GLint @baseViewIndex, GLsizei @numViews);
        internal static glFramebufferTextureMultisampleMultiviewOVRFunc glFramebufferTextureMultisampleMultiviewOVRPtr;
        internal static void loadFramebufferTextureMultisampleMultiviewOVR()
        {
            try
            {
                glFramebufferTextureMultisampleMultiviewOVRPtr = (glFramebufferTextureMultisampleMultiviewOVRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureMultisampleMultiviewOVR"), typeof(glFramebufferTextureMultisampleMultiviewOVRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureMultisampleMultiviewOVR'.");
            }
        }
        public static void glFramebufferTextureMultisampleMultiviewOVR(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLsizei @samples, GLint @baseViewIndex, GLsizei @numViews) => glFramebufferTextureMultisampleMultiviewOVRPtr(@target, @attachment, @texture, @level, @samples, @baseViewIndex, @numViews);
        #endregion
    }
}
