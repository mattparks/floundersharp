using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_OVR_multiview
    {
        #region Interop
        static GL_OVR_multiview()
        {
            Console.WriteLine("Initalising GL_OVR_multiview interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFramebufferTextureMultiviewOVR();
        }
        #endregion

        #region Enums
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_NUM_VIEWS_OVR = 0x9630;
        public static UInt32 GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_BASE_VIEW_INDEX_OVR = 0x9632;
        public static UInt32 GL_MAX_VIEWS_OVR = 0x9631;
        #endregion

        #region Commands
        internal delegate void glFramebufferTextureMultiviewOVRFunc(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @baseViewIndex, GLsizei @numViews);
        internal static glFramebufferTextureMultiviewOVRFunc glFramebufferTextureMultiviewOVRPtr;
        internal static void loadFramebufferTextureMultiviewOVR()
        {
            try
            {
                glFramebufferTextureMultiviewOVRPtr = (glFramebufferTextureMultiviewOVRFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFramebufferTextureMultiviewOVR"), typeof(glFramebufferTextureMultiviewOVRFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFramebufferTextureMultiviewOVR'.");
            }
        }
        public static void glFramebufferTextureMultiviewOVR(GLenum @target, GLenum @attachment, GLuint @texture, GLint @level, GLint @baseViewIndex, GLsizei @numViews) => glFramebufferTextureMultiviewOVRPtr(@target, @attachment, @texture, @level, @baseViewIndex, @numViews);
        #endregion
    }
}
