using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GLES10
    {
        #region Interop
        static GLES10()
        {
            Console.WriteLine("Initalising GLES10 interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadAlphaFunc();
            loadClearColor();
            loadClearDepthf();
            loadClipPlanef();
            loadColor4f();
            loadDepthRangef();
            loadFogf();
            loadFogfv();
            loadFrustumf();
            loadGetClipPlanef();
            loadGetFloatv();
            loadGetLightfv();
            loadGetMaterialfv();
            loadGetTexEnvfv();
            loadGetTexParameterfv();
            loadLightModelf();
            loadLightModelfv();
            loadLightf();
            loadLightfv();
            loadLineWidth();
            loadLoadMatrixf();
            loadMaterialf();
            loadMaterialfv();
            loadMultMatrixf();
            loadMultiTexCoord4f();
            loadNormal3f();
            loadOrthof();
            loadPointParameterf();
            loadPointParameterfv();
            loadPointSize();
            loadPolygonOffset();
            loadRotatef();
            loadScalef();
            loadTexEnvf();
            loadTexEnvfv();
            loadTexParameterf();
            loadTexParameterfv();
            loadTranslatef();
            loadActiveTexture();
            loadAlphaFuncx();
            loadBindBuffer();
            loadBindTexture();
            loadBlendFunc();
            loadBufferData();
            loadBufferSubData();
            loadClear();
            loadClearColorx();
            loadClearDepthx();
            loadClearStencil();
            loadClientActiveTexture();
            loadClipPlanex();
            loadColor4ub();
            loadColor4x();
            loadColorMask();
            loadColorPointer();
            loadCompressedTexImage2D();
            loadCompressedTexSubImage2D();
            loadCopyTexImage2D();
            loadCopyTexSubImage2D();
            loadCullFace();
            loadDeleteBuffers();
            loadDeleteTextures();
            loadDepthFunc();
            loadDepthMask();
            loadDepthRangex();
            loadDisable();
            loadDisableClientState();
            loadDrawArrays();
            loadDrawElements();
            loadEnable();
            loadEnableClientState();
            loadFinish();
            loadFlush();
            loadFogx();
            loadFogxv();
            loadFrontFace();
            loadFrustumx();
            loadGetBooleanv();
            loadGetBufferParameteriv();
            loadGetClipPlanex();
            loadGenBuffers();
            loadGenTextures();
            loadGetError();
            loadGetFixedv();
            loadGetIntegerv();
            loadGetLightxv();
            loadGetMaterialxv();
            loadGetPointerv();
            loadGetString();
            loadGetTexEnviv();
            loadGetTexEnvxv();
            loadGetTexParameteriv();
            loadGetTexParameterxv();
            loadHint();
            loadIsBuffer();
            loadIsEnabled();
            loadIsTexture();
            loadLightModelx();
            loadLightModelxv();
            loadLightx();
            loadLightxv();
            loadLineWidthx();
            loadLoadIdentity();
            loadLoadMatrixx();
            loadLogicOp();
            loadMaterialx();
            loadMaterialxv();
            loadMatrixMode();
            loadMultMatrixx();
            loadMultiTexCoord4x();
            loadNormal3x();
            loadNormalPointer();
            loadOrthox();
            loadPixelStorei();
            loadPointParameterx();
            loadPointParameterxv();
            loadPointSizex();
            loadPolygonOffsetx();
            loadPopMatrix();
            loadPushMatrix();
            loadReadPixels();
            loadRotatex();
            loadSampleCoverage();
            loadSampleCoveragex();
            loadScalex();
            loadScissor();
            loadShadeModel();
            loadStencilFunc();
            loadStencilMask();
            loadStencilOp();
            loadTexCoordPointer();
            loadTexEnvi();
            loadTexEnvx();
            loadTexEnviv();
            loadTexEnvxv();
            loadTexImage2D();
            loadTexParameteri();
            loadTexParameterx();
            loadTexParameteriv();
            loadTexParameterxv();
            loadTexSubImage2D();
            loadTranslatex();
            loadVertexPointer();
            loadViewport();
        }
        #endregion

        #region Enums
        public static UInt32 GL_VERSION_ES_CL_1_0 = 1;
        public static UInt32 GL_VERSION_ES_CM_1_1 = 1;
        public static UInt32 GL_VERSION_ES_CL_1_1 = 1;
        public static UInt32 GL_DEPTH_BUFFER_BIT = 0x00000100;
        public static UInt32 GL_STENCIL_BUFFER_BIT = 0x00000400;
        public static UInt32 GL_COLOR_BUFFER_BIT = 0x00004000;
        public static UInt32 GL_FALSE = 0;
        public static UInt32 GL_TRUE = 1;
        public static UInt32 GL_POINTS = 0x0000;
        public static UInt32 GL_LINES = 0x0001;
        public static UInt32 GL_LINE_LOOP = 0x0002;
        public static UInt32 GL_LINE_STRIP = 0x0003;
        public static UInt32 GL_TRIANGLES = 0x0004;
        public static UInt32 GL_TRIANGLE_STRIP = 0x0005;
        public static UInt32 GL_TRIANGLE_FAN = 0x0006;
        public static UInt32 GL_NEVER = 0x0200;
        public static UInt32 GL_LESS = 0x0201;
        public static UInt32 GL_EQUAL = 0x0202;
        public static UInt32 GL_LEQUAL = 0x0203;
        public static UInt32 GL_GREATER = 0x0204;
        public static UInt32 GL_NOTEQUAL = 0x0205;
        public static UInt32 GL_GEQUAL = 0x0206;
        public static UInt32 GL_ALWAYS = 0x0207;
        public static UInt32 GL_ZERO = 0;
        public static UInt32 GL_ONE = 1;
        public static UInt32 GL_SRC_COLOR = 0x0300;
        public static UInt32 GL_ONE_MINUS_SRC_COLOR = 0x0301;
        public static UInt32 GL_SRC_ALPHA = 0x0302;
        public static UInt32 GL_ONE_MINUS_SRC_ALPHA = 0x0303;
        public static UInt32 GL_DST_ALPHA = 0x0304;
        public static UInt32 GL_ONE_MINUS_DST_ALPHA = 0x0305;
        public static UInt32 GL_DST_COLOR = 0x0306;
        public static UInt32 GL_ONE_MINUS_DST_COLOR = 0x0307;
        public static UInt32 GL_SRC_ALPHA_SATURATE = 0x0308;
        public static UInt32 GL_CLIP_PLANE0 = 0x3000;
        public static UInt32 GL_CLIP_PLANE1 = 0x3001;
        public static UInt32 GL_CLIP_PLANE2 = 0x3002;
        public static UInt32 GL_CLIP_PLANE3 = 0x3003;
        public static UInt32 GL_CLIP_PLANE4 = 0x3004;
        public static UInt32 GL_CLIP_PLANE5 = 0x3005;
        public static UInt32 GL_FRONT = 0x0404;
        public static UInt32 GL_BACK = 0x0405;
        public static UInt32 GL_FRONT_AND_BACK = 0x0408;
        public static UInt32 GL_FOG = 0x0B60;
        public static UInt32 GL_LIGHTING = 0x0B50;
        public static UInt32 GL_TEXTURE_2D = 0x0DE1;
        public static UInt32 GL_CULL_FACE = 0x0B44;
        public static UInt32 GL_ALPHA_TEST = 0x0BC0;
        public static UInt32 GL_BLEND = 0x0BE2;
        public static UInt32 GL_COLOR_LOGIC_OP = 0x0BF2;
        public static UInt32 GL_DITHER = 0x0BD0;
        public static UInt32 GL_STENCIL_TEST = 0x0B90;
        public static UInt32 GL_DEPTH_TEST = 0x0B71;
        public static UInt32 GL_POINT_SMOOTH = 0x0B10;
        public static UInt32 GL_LINE_SMOOTH = 0x0B20;
        public static UInt32 GL_SCISSOR_TEST = 0x0C11;
        public static UInt32 GL_COLOR_MATERIAL = 0x0B57;
        public static UInt32 GL_NORMALIZE = 0x0BA1;
        public static UInt32 GL_RESCALE_NORMAL = 0x803A;
        public static UInt32 GL_VERTEX_ARRAY = 0x8074;
        public static UInt32 GL_NORMAL_ARRAY = 0x8075;
        public static UInt32 GL_COLOR_ARRAY = 0x8076;
        public static UInt32 GL_TEXTURE_COORD_ARRAY = 0x8078;
        public static UInt32 GL_MULTISAMPLE = 0x809D;
        public static UInt32 GL_SAMPLE_ALPHA_TO_COVERAGE = 0x809E;
        public static UInt32 GL_SAMPLE_ALPHA_TO_ONE = 0x809F;
        public static UInt32 GL_SAMPLE_COVERAGE = 0x80A0;
        public static UInt32 GL_NO_ERROR = 0;
        public static UInt32 GL_INVALID_ENUM = 0x0500;
        public static UInt32 GL_INVALID_VALUE = 0x0501;
        public static UInt32 GL_INVALID_OPERATION = 0x0502;
        public static UInt32 GL_STACK_OVERFLOW = 0x0503;
        public static UInt32 GL_STACK_UNDERFLOW = 0x0504;
        public static UInt32 GL_OUT_OF_MEMORY = 0x0505;
        public static UInt32 GL_EXP = 0x0800;
        public static UInt32 GL_EXP2 = 0x0801;
        public static UInt32 GL_FOG_DENSITY = 0x0B62;
        public static UInt32 GL_FOG_START = 0x0B63;
        public static UInt32 GL_FOG_END = 0x0B64;
        public static UInt32 GL_FOG_MODE = 0x0B65;
        public static UInt32 GL_FOG_COLOR = 0x0B66;
        public static UInt32 GL_CW = 0x0900;
        public static UInt32 GL_CCW = 0x0901;
        public static UInt32 GL_CURRENT_COLOR = 0x0B00;
        public static UInt32 GL_CURRENT_NORMAL = 0x0B02;
        public static UInt32 GL_CURRENT_TEXTURE_COORDS = 0x0B03;
        public static UInt32 GL_POINT_SIZE = 0x0B11;
        public static UInt32 GL_POINT_SIZE_MIN = 0x8126;
        public static UInt32 GL_POINT_SIZE_MAX = 0x8127;
        public static UInt32 GL_POINT_FADE_THRESHOLD_SIZE = 0x8128;
        public static UInt32 GL_POINT_DISTANCE_ATTENUATION = 0x8129;
        public static UInt32 GL_SMOOTH_POINT_SIZE_RANGE = 0x0B12;
        public static UInt32 GL_LINE_WIDTH = 0x0B21;
        public static UInt32 GL_SMOOTH_LINE_WIDTH_RANGE = 0x0B22;
        public static UInt32 GL_ALIASED_POINT_SIZE_RANGE = 0x846D;
        public static UInt32 GL_ALIASED_LINE_WIDTH_RANGE = 0x846E;
        public static UInt32 GL_CULL_FACE_MODE = 0x0B45;
        public static UInt32 GL_FRONT_FACE = 0x0B46;
        public static UInt32 GL_SHADE_MODEL = 0x0B54;
        public static UInt32 GL_DEPTH_RANGE = 0x0B70;
        public static UInt32 GL_DEPTH_WRITEMASK = 0x0B72;
        public static UInt32 GL_DEPTH_CLEAR_VALUE = 0x0B73;
        public static UInt32 GL_DEPTH_FUNC = 0x0B74;
        public static UInt32 GL_STENCIL_CLEAR_VALUE = 0x0B91;
        public static UInt32 GL_STENCIL_FUNC = 0x0B92;
        public static UInt32 GL_STENCIL_VALUE_MASK = 0x0B93;
        public static UInt32 GL_STENCIL_FAIL = 0x0B94;
        public static UInt32 GL_STENCIL_PASS_DEPTH_FAIL = 0x0B95;
        public static UInt32 GL_STENCIL_PASS_DEPTH_PASS = 0x0B96;
        public static UInt32 GL_STENCIL_REF = 0x0B97;
        public static UInt32 GL_STENCIL_WRITEMASK = 0x0B98;
        public static UInt32 GL_MATRIX_MODE = 0x0BA0;
        public static UInt32 GL_VIEWPORT = 0x0BA2;
        public static UInt32 GL_MODELVIEW_STACK_DEPTH = 0x0BA3;
        public static UInt32 GL_PROJECTION_STACK_DEPTH = 0x0BA4;
        public static UInt32 GL_TEXTURE_STACK_DEPTH = 0x0BA5;
        public static UInt32 GL_MODELVIEW_MATRIX = 0x0BA6;
        public static UInt32 GL_PROJECTION_MATRIX = 0x0BA7;
        public static UInt32 GL_TEXTURE_MATRIX = 0x0BA8;
        public static UInt32 GL_ALPHA_TEST_FUNC = 0x0BC1;
        public static UInt32 GL_ALPHA_TEST_REF = 0x0BC2;
        public static UInt32 GL_BLEND_DST = 0x0BE0;
        public static UInt32 GL_BLEND_SRC = 0x0BE1;
        public static UInt32 GL_LOGIC_OP_MODE = 0x0BF0;
        public static UInt32 GL_SCISSOR_BOX = 0x0C10;
        public static UInt32 GL_COLOR_CLEAR_VALUE = 0x0C22;
        public static UInt32 GL_COLOR_WRITEMASK = 0x0C23;
        public static UInt32 GL_MAX_LIGHTS = 0x0D31;
        public static UInt32 GL_MAX_CLIP_PLANES = 0x0D32;
        public static UInt32 GL_MAX_TEXTURE_SIZE = 0x0D33;
        public static UInt32 GL_MAX_MODELVIEW_STACK_DEPTH = 0x0D36;
        public static UInt32 GL_MAX_PROJECTION_STACK_DEPTH = 0x0D38;
        public static UInt32 GL_MAX_TEXTURE_STACK_DEPTH = 0x0D39;
        public static UInt32 GL_MAX_VIEWPORT_DIMS = 0x0D3A;
        public static UInt32 GL_MAX_TEXTURE_UNITS = 0x84E2;
        public static UInt32 GL_SUBPIXEL_BITS = 0x0D50;
        public static UInt32 GL_RED_BITS = 0x0D52;
        public static UInt32 GL_GREEN_BITS = 0x0D53;
        public static UInt32 GL_BLUE_BITS = 0x0D54;
        public static UInt32 GL_ALPHA_BITS = 0x0D55;
        public static UInt32 GL_DEPTH_BITS = 0x0D56;
        public static UInt32 GL_STENCIL_BITS = 0x0D57;
        public static UInt32 GL_POLYGON_OFFSET_UNITS = 0x2A00;
        public static UInt32 GL_POLYGON_OFFSET_FILL = 0x8037;
        public static UInt32 GL_POLYGON_OFFSET_FACTOR = 0x8038;
        public static UInt32 GL_TEXTURE_BINDING_2D = 0x8069;
        public static UInt32 GL_VERTEX_ARRAY_SIZE = 0x807A;
        public static UInt32 GL_VERTEX_ARRAY_TYPE = 0x807B;
        public static UInt32 GL_VERTEX_ARRAY_STRIDE = 0x807C;
        public static UInt32 GL_NORMAL_ARRAY_TYPE = 0x807E;
        public static UInt32 GL_NORMAL_ARRAY_STRIDE = 0x807F;
        public static UInt32 GL_COLOR_ARRAY_SIZE = 0x8081;
        public static UInt32 GL_COLOR_ARRAY_TYPE = 0x8082;
        public static UInt32 GL_COLOR_ARRAY_STRIDE = 0x8083;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_SIZE = 0x8088;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_TYPE = 0x8089;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_STRIDE = 0x808A;
        public static UInt32 GL_VERTEX_ARRAY_POINTER = 0x808E;
        public static UInt32 GL_NORMAL_ARRAY_POINTER = 0x808F;
        public static UInt32 GL_COLOR_ARRAY_POINTER = 0x8090;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_POINTER = 0x8092;
        public static UInt32 GL_SAMPLE_BUFFERS = 0x80A8;
        public static UInt32 GL_SAMPLES = 0x80A9;
        public static UInt32 GL_SAMPLE_COVERAGE_VALUE = 0x80AA;
        public static UInt32 GL_SAMPLE_COVERAGE_INVERT = 0x80AB;
        public static UInt32 GL_NUM_COMPRESSED_TEXTURE_FORMATS = 0x86A2;
        public static UInt32 GL_COMPRESSED_TEXTURE_FORMATS = 0x86A3;
        public static UInt32 GL_DONT_CARE = 0x1100;
        public static UInt32 GL_FASTEST = 0x1101;
        public static UInt32 GL_NICEST = 0x1102;
        public static UInt32 GL_PERSPECTIVE_CORRECTION_HINT = 0x0C50;
        public static UInt32 GL_POINT_SMOOTH_HINT = 0x0C51;
        public static UInt32 GL_LINE_SMOOTH_HINT = 0x0C52;
        public static UInt32 GL_FOG_HINT = 0x0C54;
        public static UInt32 GL_GENERATE_MIPMAP_HINT = 0x8192;
        public static UInt32 GL_LIGHT_MODEL_AMBIENT = 0x0B53;
        public static UInt32 GL_LIGHT_MODEL_TWO_SIDE = 0x0B52;
        public static UInt32 GL_AMBIENT = 0x1200;
        public static UInt32 GL_DIFFUSE = 0x1201;
        public static UInt32 GL_SPECULAR = 0x1202;
        public static UInt32 GL_POSITION = 0x1203;
        public static UInt32 GL_SPOT_DIRECTION = 0x1204;
        public static UInt32 GL_SPOT_EXPONENT = 0x1205;
        public static UInt32 GL_SPOT_CUTOFF = 0x1206;
        public static UInt32 GL_CONSTANT_ATTENUATION = 0x1207;
        public static UInt32 GL_LINEAR_ATTENUATION = 0x1208;
        public static UInt32 GL_QUADRATIC_ATTENUATION = 0x1209;
        public static UInt32 GL_BYTE = 0x1400;
        public static UInt32 GL_UNSIGNED_BYTE = 0x1401;
        public static UInt32 GL_SHORT = 0x1402;
        public static UInt32 GL_UNSIGNED_SHORT = 0x1403;
        public static UInt32 GL_FLOAT = 0x1406;
        public static UInt32 GL_FIXED = 0x140C;
        public static UInt32 GL_CLEAR = 0x1500;
        public static UInt32 GL_AND = 0x1501;
        public static UInt32 GL_AND_REVERSE = 0x1502;
        public static UInt32 GL_COPY = 0x1503;
        public static UInt32 GL_AND_INVERTED = 0x1504;
        public static UInt32 GL_NOOP = 0x1505;
        public static UInt32 GL_XOR = 0x1506;
        public static UInt32 GL_OR = 0x1507;
        public static UInt32 GL_NOR = 0x1508;
        public static UInt32 GL_EQUIV = 0x1509;
        public static UInt32 GL_INVERT = 0x150A;
        public static UInt32 GL_OR_REVERSE = 0x150B;
        public static UInt32 GL_COPY_INVERTED = 0x150C;
        public static UInt32 GL_OR_INVERTED = 0x150D;
        public static UInt32 GL_NAND = 0x150E;
        public static UInt32 GL_SET = 0x150F;
        public static UInt32 GL_EMISSION = 0x1600;
        public static UInt32 GL_SHININESS = 0x1601;
        public static UInt32 GL_AMBIENT_AND_DIFFUSE = 0x1602;
        public static UInt32 GL_MODELVIEW = 0x1700;
        public static UInt32 GL_PROJECTION = 0x1701;
        public static UInt32 GL_TEXTURE = 0x1702;
        public static UInt32 GL_ALPHA = 0x1906;
        public static UInt32 GL_RGB = 0x1907;
        public static UInt32 GL_RGBA = 0x1908;
        public static UInt32 GL_LUMINANCE = 0x1909;
        public static UInt32 GL_LUMINANCE_ALPHA = 0x190A;
        public static UInt32 GL_UNPACK_ALIGNMENT = 0x0CF5;
        public static UInt32 GL_PACK_ALIGNMENT = 0x0D05;
        public static UInt32 GL_UNSIGNED_SHORT_4_4_4_4 = 0x8033;
        public static UInt32 GL_UNSIGNED_SHORT_5_5_5_1 = 0x8034;
        public static UInt32 GL_UNSIGNED_SHORT_5_6_5 = 0x8363;
        public static UInt32 GL_FLAT = 0x1D00;
        public static UInt32 GL_SMOOTH = 0x1D01;
        public static UInt32 GL_KEEP = 0x1E00;
        public static UInt32 GL_REPLACE = 0x1E01;
        public static UInt32 GL_INCR = 0x1E02;
        public static UInt32 GL_DECR = 0x1E03;
        public static UInt32 GL_VENDOR = 0x1F00;
        public static UInt32 GL_RENDERER = 0x1F01;
        public static UInt32 GL_VERSION = 0x1F02;
        public static UInt32 GL_EXTENSIONS = 0x1F03;
        public static UInt32 GL_MODULATE = 0x2100;
        public static UInt32 GL_DECAL = 0x2101;
        public static UInt32 GL_ADD = 0x0104;
        public static UInt32 GL_TEXTURE_ENV_MODE = 0x2200;
        public static UInt32 GL_TEXTURE_ENV_COLOR = 0x2201;
        public static UInt32 GL_TEXTURE_ENV = 0x2300;
        public static UInt32 GL_NEAREST = 0x2600;
        public static UInt32 GL_LINEAR = 0x2601;
        public static UInt32 GL_NEAREST_MIPMAP_NEAREST = 0x2700;
        public static UInt32 GL_LINEAR_MIPMAP_NEAREST = 0x2701;
        public static UInt32 GL_NEAREST_MIPMAP_LINEAR = 0x2702;
        public static UInt32 GL_LINEAR_MIPMAP_LINEAR = 0x2703;
        public static UInt32 GL_TEXTURE_MAG_FILTER = 0x2800;
        public static UInt32 GL_TEXTURE_MIN_FILTER = 0x2801;
        public static UInt32 GL_TEXTURE_WRAP_S = 0x2802;
        public static UInt32 GL_TEXTURE_WRAP_T = 0x2803;
        public static UInt32 GL_GENERATE_MIPMAP = 0x8191;
        public static UInt32 GL_TEXTURE0 = 0x84C0;
        public static UInt32 GL_TEXTURE1 = 0x84C1;
        public static UInt32 GL_TEXTURE2 = 0x84C2;
        public static UInt32 GL_TEXTURE3 = 0x84C3;
        public static UInt32 GL_TEXTURE4 = 0x84C4;
        public static UInt32 GL_TEXTURE5 = 0x84C5;
        public static UInt32 GL_TEXTURE6 = 0x84C6;
        public static UInt32 GL_TEXTURE7 = 0x84C7;
        public static UInt32 GL_TEXTURE8 = 0x84C8;
        public static UInt32 GL_TEXTURE9 = 0x84C9;
        public static UInt32 GL_TEXTURE10 = 0x84CA;
        public static UInt32 GL_TEXTURE11 = 0x84CB;
        public static UInt32 GL_TEXTURE12 = 0x84CC;
        public static UInt32 GL_TEXTURE13 = 0x84CD;
        public static UInt32 GL_TEXTURE14 = 0x84CE;
        public static UInt32 GL_TEXTURE15 = 0x84CF;
        public static UInt32 GL_TEXTURE16 = 0x84D0;
        public static UInt32 GL_TEXTURE17 = 0x84D1;
        public static UInt32 GL_TEXTURE18 = 0x84D2;
        public static UInt32 GL_TEXTURE19 = 0x84D3;
        public static UInt32 GL_TEXTURE20 = 0x84D4;
        public static UInt32 GL_TEXTURE21 = 0x84D5;
        public static UInt32 GL_TEXTURE22 = 0x84D6;
        public static UInt32 GL_TEXTURE23 = 0x84D7;
        public static UInt32 GL_TEXTURE24 = 0x84D8;
        public static UInt32 GL_TEXTURE25 = 0x84D9;
        public static UInt32 GL_TEXTURE26 = 0x84DA;
        public static UInt32 GL_TEXTURE27 = 0x84DB;
        public static UInt32 GL_TEXTURE28 = 0x84DC;
        public static UInt32 GL_TEXTURE29 = 0x84DD;
        public static UInt32 GL_TEXTURE30 = 0x84DE;
        public static UInt32 GL_TEXTURE31 = 0x84DF;
        public static UInt32 GL_ACTIVE_TEXTURE = 0x84E0;
        public static UInt32 GL_CLIENT_ACTIVE_TEXTURE = 0x84E1;
        public static UInt32 GL_REPEAT = 0x2901;
        public static UInt32 GL_CLAMP_TO_EDGE = 0x812F;
        public static UInt32 GL_LIGHT0 = 0x4000;
        public static UInt32 GL_LIGHT1 = 0x4001;
        public static UInt32 GL_LIGHT2 = 0x4002;
        public static UInt32 GL_LIGHT3 = 0x4003;
        public static UInt32 GL_LIGHT4 = 0x4004;
        public static UInt32 GL_LIGHT5 = 0x4005;
        public static UInt32 GL_LIGHT6 = 0x4006;
        public static UInt32 GL_LIGHT7 = 0x4007;
        public static UInt32 GL_ARRAY_BUFFER = 0x8892;
        public static UInt32 GL_ELEMENT_ARRAY_BUFFER = 0x8893;
        public static UInt32 GL_ARRAY_BUFFER_BINDING = 0x8894;
        public static UInt32 GL_ELEMENT_ARRAY_BUFFER_BINDING = 0x8895;
        public static UInt32 GL_VERTEX_ARRAY_BUFFER_BINDING = 0x8896;
        public static UInt32 GL_NORMAL_ARRAY_BUFFER_BINDING = 0x8897;
        public static UInt32 GL_COLOR_ARRAY_BUFFER_BINDING = 0x8898;
        public static UInt32 GL_TEXTURE_COORD_ARRAY_BUFFER_BINDING = 0x889A;
        public static UInt32 GL_STATIC_DRAW = 0x88E4;
        public static UInt32 GL_DYNAMIC_DRAW = 0x88E8;
        public static UInt32 GL_BUFFER_SIZE = 0x8764;
        public static UInt32 GL_BUFFER_USAGE = 0x8765;
        public static UInt32 GL_SUBTRACT = 0x84E7;
        public static UInt32 GL_COMBINE = 0x8570;
        public static UInt32 GL_COMBINE_RGB = 0x8571;
        public static UInt32 GL_COMBINE_ALPHA = 0x8572;
        public static UInt32 GL_RGB_SCALE = 0x8573;
        public static UInt32 GL_ADD_SIGNED = 0x8574;
        public static UInt32 GL_INTERPOLATE = 0x8575;
        public static UInt32 GL_CONSTANT = 0x8576;
        public static UInt32 GL_PRIMARY_COLOR = 0x8577;
        public static UInt32 GL_PREVIOUS = 0x8578;
        public static UInt32 GL_OPERAND0_RGB = 0x8590;
        public static UInt32 GL_OPERAND1_RGB = 0x8591;
        public static UInt32 GL_OPERAND2_RGB = 0x8592;
        public static UInt32 GL_OPERAND0_ALPHA = 0x8598;
        public static UInt32 GL_OPERAND1_ALPHA = 0x8599;
        public static UInt32 GL_OPERAND2_ALPHA = 0x859A;
        public static UInt32 GL_ALPHA_SCALE = 0x0D1C;
        public static UInt32 GL_SRC0_RGB = 0x8580;
        public static UInt32 GL_SRC1_RGB = 0x8581;
        public static UInt32 GL_SRC2_RGB = 0x8582;
        public static UInt32 GL_SRC0_ALPHA = 0x8588;
        public static UInt32 GL_SRC1_ALPHA = 0x8589;
        public static UInt32 GL_SRC2_ALPHA = 0x858A;
        public static UInt32 GL_DOT3_RGB = 0x86AE;
        public static UInt32 GL_DOT3_RGBA = 0x86AF;
        #endregion

        #region Commands
        internal delegate void glAlphaFuncFunc(GLenum @func, GLfloat @ref);
        internal static glAlphaFuncFunc glAlphaFuncPtr;
        internal static void loadAlphaFunc()
        {
            try
            {
                glAlphaFuncPtr = (glAlphaFuncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAlphaFunc"), typeof(glAlphaFuncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAlphaFunc'.");
            }
        }
        public static void glAlphaFunc(GLenum @func, GLfloat @ref) => glAlphaFuncPtr(@func, @ref);

        internal delegate void glClearColorFunc(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha);
        internal static glClearColorFunc glClearColorPtr;
        internal static void loadClearColor()
        {
            try
            {
                glClearColorPtr = (glClearColorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearColor"), typeof(glClearColorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearColor'.");
            }
        }
        public static void glClearColor(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha) => glClearColorPtr(@red, @green, @blue, @alpha);

        internal delegate void glClearDepthfFunc(GLfloat @d);
        internal static glClearDepthfFunc glClearDepthfPtr;
        internal static void loadClearDepthf()
        {
            try
            {
                glClearDepthfPtr = (glClearDepthfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearDepthf"), typeof(glClearDepthfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearDepthf'.");
            }
        }
        public static void glClearDepthf(GLfloat @d) => glClearDepthfPtr(@d);

        internal delegate void glClipPlanefFunc(GLenum @p, const GLfloat * @eqn);
        internal static glClipPlanefFunc glClipPlanefPtr;
        internal static void loadClipPlanef()
        {
            try
            {
                glClipPlanefPtr = (glClipPlanefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClipPlanef"), typeof(glClipPlanefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClipPlanef'.");
            }
        }
        public static void glClipPlanef(GLenum @p, const GLfloat * @eqn) => glClipPlanefPtr(@p, @eqn);

        internal delegate void glColor4fFunc(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha);
        internal static glColor4fFunc glColor4fPtr;
        internal static void loadColor4f()
        {
            try
            {
                glColor4fPtr = (glColor4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4f"), typeof(glColor4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4f'.");
            }
        }
        public static void glColor4f(GLfloat @red, GLfloat @green, GLfloat @blue, GLfloat @alpha) => glColor4fPtr(@red, @green, @blue, @alpha);

        internal delegate void glDepthRangefFunc(GLfloat @n, GLfloat @f);
        internal static glDepthRangefFunc glDepthRangefPtr;
        internal static void loadDepthRangef()
        {
            try
            {
                glDepthRangefPtr = (glDepthRangefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangef"), typeof(glDepthRangefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangef'.");
            }
        }
        public static void glDepthRangef(GLfloat @n, GLfloat @f) => glDepthRangefPtr(@n, @f);

        internal delegate void glFogfFunc(GLenum @pname, GLfloat @param);
        internal static glFogfFunc glFogfPtr;
        internal static void loadFogf()
        {
            try
            {
                glFogfPtr = (glFogfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogf"), typeof(glFogfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogf'.");
            }
        }
        public static void glFogf(GLenum @pname, GLfloat @param) => glFogfPtr(@pname, @param);

        internal delegate void glFogfvFunc(GLenum @pname, const GLfloat * @params);
        internal static glFogfvFunc glFogfvPtr;
        internal static void loadFogfv()
        {
            try
            {
                glFogfvPtr = (glFogfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogfv"), typeof(glFogfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogfv'.");
            }
        }
        public static void glFogfv(GLenum @pname, const GLfloat * @params) => glFogfvPtr(@pname, @params);

        internal delegate void glFrustumfFunc(GLfloat @l, GLfloat @r, GLfloat @b, GLfloat @t, GLfloat @n, GLfloat @f);
        internal static glFrustumfFunc glFrustumfPtr;
        internal static void loadFrustumf()
        {
            try
            {
                glFrustumfPtr = (glFrustumfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFrustumf"), typeof(glFrustumfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFrustumf'.");
            }
        }
        public static void glFrustumf(GLfloat @l, GLfloat @r, GLfloat @b, GLfloat @t, GLfloat @n, GLfloat @f) => glFrustumfPtr(@l, @r, @b, @t, @n, @f);

        internal delegate void glGetClipPlanefFunc(GLenum @plane, GLfloat * @equation);
        internal static glGetClipPlanefFunc glGetClipPlanefPtr;
        internal static void loadGetClipPlanef()
        {
            try
            {
                glGetClipPlanefPtr = (glGetClipPlanefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetClipPlanef"), typeof(glGetClipPlanefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetClipPlanef'.");
            }
        }
        public static void glGetClipPlanef(GLenum @plane, GLfloat * @equation) => glGetClipPlanefPtr(@plane, @equation);

        internal delegate void glGetFloatvFunc(GLenum @pname, GLfloat * @data);
        internal static glGetFloatvFunc glGetFloatvPtr;
        internal static void loadGetFloatv()
        {
            try
            {
                glGetFloatvPtr = (glGetFloatvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFloatv"), typeof(glGetFloatvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFloatv'.");
            }
        }
        public static void glGetFloatv(GLenum @pname, GLfloat * @data) => glGetFloatvPtr(@pname, @data);

        internal delegate void glGetLightfvFunc(GLenum @light, GLenum @pname, GLfloat * @params);
        internal static glGetLightfvFunc glGetLightfvPtr;
        internal static void loadGetLightfv()
        {
            try
            {
                glGetLightfvPtr = (glGetLightfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetLightfv"), typeof(glGetLightfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetLightfv'.");
            }
        }
        public static void glGetLightfv(GLenum @light, GLenum @pname, GLfloat * @params) => glGetLightfvPtr(@light, @pname, @params);

        internal delegate void glGetMaterialfvFunc(GLenum @face, GLenum @pname, GLfloat * @params);
        internal static glGetMaterialfvFunc glGetMaterialfvPtr;
        internal static void loadGetMaterialfv()
        {
            try
            {
                glGetMaterialfvPtr = (glGetMaterialfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMaterialfv"), typeof(glGetMaterialfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMaterialfv'.");
            }
        }
        public static void glGetMaterialfv(GLenum @face, GLenum @pname, GLfloat * @params) => glGetMaterialfvPtr(@face, @pname, @params);

        internal delegate void glGetTexEnvfvFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetTexEnvfvFunc glGetTexEnvfvPtr;
        internal static void loadGetTexEnvfv()
        {
            try
            {
                glGetTexEnvfvPtr = (glGetTexEnvfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexEnvfv"), typeof(glGetTexEnvfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexEnvfv'.");
            }
        }
        public static void glGetTexEnvfv(GLenum @target, GLenum @pname, GLfloat * @params) => glGetTexEnvfvPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterfvFunc(GLenum @target, GLenum @pname, GLfloat * @params);
        internal static glGetTexParameterfvFunc glGetTexParameterfvPtr;
        internal static void loadGetTexParameterfv()
        {
            try
            {
                glGetTexParameterfvPtr = (glGetTexParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterfv"), typeof(glGetTexParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterfv'.");
            }
        }
        public static void glGetTexParameterfv(GLenum @target, GLenum @pname, GLfloat * @params) => glGetTexParameterfvPtr(@target, @pname, @params);

        internal delegate void glLightModelfFunc(GLenum @pname, GLfloat @param);
        internal static glLightModelfFunc glLightModelfPtr;
        internal static void loadLightModelf()
        {
            try
            {
                glLightModelfPtr = (glLightModelfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightModelf"), typeof(glLightModelfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightModelf'.");
            }
        }
        public static void glLightModelf(GLenum @pname, GLfloat @param) => glLightModelfPtr(@pname, @param);

        internal delegate void glLightModelfvFunc(GLenum @pname, const GLfloat * @params);
        internal static glLightModelfvFunc glLightModelfvPtr;
        internal static void loadLightModelfv()
        {
            try
            {
                glLightModelfvPtr = (glLightModelfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightModelfv"), typeof(glLightModelfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightModelfv'.");
            }
        }
        public static void glLightModelfv(GLenum @pname, const GLfloat * @params) => glLightModelfvPtr(@pname, @params);

        internal delegate void glLightfFunc(GLenum @light, GLenum @pname, GLfloat @param);
        internal static glLightfFunc glLightfPtr;
        internal static void loadLightf()
        {
            try
            {
                glLightfPtr = (glLightfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightf"), typeof(glLightfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightf'.");
            }
        }
        public static void glLightf(GLenum @light, GLenum @pname, GLfloat @param) => glLightfPtr(@light, @pname, @param);

        internal delegate void glLightfvFunc(GLenum @light, GLenum @pname, const GLfloat * @params);
        internal static glLightfvFunc glLightfvPtr;
        internal static void loadLightfv()
        {
            try
            {
                glLightfvPtr = (glLightfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightfv"), typeof(glLightfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightfv'.");
            }
        }
        public static void glLightfv(GLenum @light, GLenum @pname, const GLfloat * @params) => glLightfvPtr(@light, @pname, @params);

        internal delegate void glLineWidthFunc(GLfloat @width);
        internal static glLineWidthFunc glLineWidthPtr;
        internal static void loadLineWidth()
        {
            try
            {
                glLineWidthPtr = (glLineWidthFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLineWidth"), typeof(glLineWidthFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLineWidth'.");
            }
        }
        public static void glLineWidth(GLfloat @width) => glLineWidthPtr(@width);

        internal delegate void glLoadMatrixfFunc(const GLfloat * @m);
        internal static glLoadMatrixfFunc glLoadMatrixfPtr;
        internal static void loadLoadMatrixf()
        {
            try
            {
                glLoadMatrixfPtr = (glLoadMatrixfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadMatrixf"), typeof(glLoadMatrixfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadMatrixf'.");
            }
        }
        public static void glLoadMatrixf(const GLfloat * @m) => glLoadMatrixfPtr(@m);

        internal delegate void glMaterialfFunc(GLenum @face, GLenum @pname, GLfloat @param);
        internal static glMaterialfFunc glMaterialfPtr;
        internal static void loadMaterialf()
        {
            try
            {
                glMaterialfPtr = (glMaterialfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMaterialf"), typeof(glMaterialfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMaterialf'.");
            }
        }
        public static void glMaterialf(GLenum @face, GLenum @pname, GLfloat @param) => glMaterialfPtr(@face, @pname, @param);

        internal delegate void glMaterialfvFunc(GLenum @face, GLenum @pname, const GLfloat * @params);
        internal static glMaterialfvFunc glMaterialfvPtr;
        internal static void loadMaterialfv()
        {
            try
            {
                glMaterialfvPtr = (glMaterialfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMaterialfv"), typeof(glMaterialfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMaterialfv'.");
            }
        }
        public static void glMaterialfv(GLenum @face, GLenum @pname, const GLfloat * @params) => glMaterialfvPtr(@face, @pname, @params);

        internal delegate void glMultMatrixfFunc(const GLfloat * @m);
        internal static glMultMatrixfFunc glMultMatrixfPtr;
        internal static void loadMultMatrixf()
        {
            try
            {
                glMultMatrixfPtr = (glMultMatrixfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultMatrixf"), typeof(glMultMatrixfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultMatrixf'.");
            }
        }
        public static void glMultMatrixf(const GLfloat * @m) => glMultMatrixfPtr(@m);

        internal delegate void glMultiTexCoord4fFunc(GLenum @target, GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @q);
        internal static glMultiTexCoord4fFunc glMultiTexCoord4fPtr;
        internal static void loadMultiTexCoord4f()
        {
            try
            {
                glMultiTexCoord4fPtr = (glMultiTexCoord4fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4f"), typeof(glMultiTexCoord4fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4f'.");
            }
        }
        public static void glMultiTexCoord4f(GLenum @target, GLfloat @s, GLfloat @t, GLfloat @r, GLfloat @q) => glMultiTexCoord4fPtr(@target, @s, @t, @r, @q);

        internal delegate void glNormal3fFunc(GLfloat @nx, GLfloat @ny, GLfloat @nz);
        internal static glNormal3fFunc glNormal3fPtr;
        internal static void loadNormal3f()
        {
            try
            {
                glNormal3fPtr = (glNormal3fFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3f"), typeof(glNormal3fFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3f'.");
            }
        }
        public static void glNormal3f(GLfloat @nx, GLfloat @ny, GLfloat @nz) => glNormal3fPtr(@nx, @ny, @nz);

        internal delegate void glOrthofFunc(GLfloat @l, GLfloat @r, GLfloat @b, GLfloat @t, GLfloat @n, GLfloat @f);
        internal static glOrthofFunc glOrthofPtr;
        internal static void loadOrthof()
        {
            try
            {
                glOrthofPtr = (glOrthofFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glOrthof"), typeof(glOrthofFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glOrthof'.");
            }
        }
        public static void glOrthof(GLfloat @l, GLfloat @r, GLfloat @b, GLfloat @t, GLfloat @n, GLfloat @f) => glOrthofPtr(@l, @r, @b, @t, @n, @f);

        internal delegate void glPointParameterfFunc(GLenum @pname, GLfloat @param);
        internal static glPointParameterfFunc glPointParameterfPtr;
        internal static void loadPointParameterf()
        {
            try
            {
                glPointParameterfPtr = (glPointParameterfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterf"), typeof(glPointParameterfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterf'.");
            }
        }
        public static void glPointParameterf(GLenum @pname, GLfloat @param) => glPointParameterfPtr(@pname, @param);

        internal delegate void glPointParameterfvFunc(GLenum @pname, const GLfloat * @params);
        internal static glPointParameterfvFunc glPointParameterfvPtr;
        internal static void loadPointParameterfv()
        {
            try
            {
                glPointParameterfvPtr = (glPointParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterfv"), typeof(glPointParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterfv'.");
            }
        }
        public static void glPointParameterfv(GLenum @pname, const GLfloat * @params) => glPointParameterfvPtr(@pname, @params);

        internal delegate void glPointSizeFunc(GLfloat @size);
        internal static glPointSizeFunc glPointSizePtr;
        internal static void loadPointSize()
        {
            try
            {
                glPointSizePtr = (glPointSizeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointSize"), typeof(glPointSizeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointSize'.");
            }
        }
        public static void glPointSize(GLfloat @size) => glPointSizePtr(@size);

        internal delegate void glPolygonOffsetFunc(GLfloat @factor, GLfloat @units);
        internal static glPolygonOffsetFunc glPolygonOffsetPtr;
        internal static void loadPolygonOffset()
        {
            try
            {
                glPolygonOffsetPtr = (glPolygonOffsetFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPolygonOffset"), typeof(glPolygonOffsetFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPolygonOffset'.");
            }
        }
        public static void glPolygonOffset(GLfloat @factor, GLfloat @units) => glPolygonOffsetPtr(@factor, @units);

        internal delegate void glRotatefFunc(GLfloat @angle, GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glRotatefFunc glRotatefPtr;
        internal static void loadRotatef()
        {
            try
            {
                glRotatefPtr = (glRotatefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRotatef"), typeof(glRotatefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRotatef'.");
            }
        }
        public static void glRotatef(GLfloat @angle, GLfloat @x, GLfloat @y, GLfloat @z) => glRotatefPtr(@angle, @x, @y, @z);

        internal delegate void glScalefFunc(GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glScalefFunc glScalefPtr;
        internal static void loadScalef()
        {
            try
            {
                glScalefPtr = (glScalefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScalef"), typeof(glScalefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScalef'.");
            }
        }
        public static void glScalef(GLfloat @x, GLfloat @y, GLfloat @z) => glScalefPtr(@x, @y, @z);

        internal delegate void glTexEnvfFunc(GLenum @target, GLenum @pname, GLfloat @param);
        internal static glTexEnvfFunc glTexEnvfPtr;
        internal static void loadTexEnvf()
        {
            try
            {
                glTexEnvfPtr = (glTexEnvfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexEnvf"), typeof(glTexEnvfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexEnvf'.");
            }
        }
        public static void glTexEnvf(GLenum @target, GLenum @pname, GLfloat @param) => glTexEnvfPtr(@target, @pname, @param);

        internal delegate void glTexEnvfvFunc(GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glTexEnvfvFunc glTexEnvfvPtr;
        internal static void loadTexEnvfv()
        {
            try
            {
                glTexEnvfvPtr = (glTexEnvfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexEnvfv"), typeof(glTexEnvfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexEnvfv'.");
            }
        }
        public static void glTexEnvfv(GLenum @target, GLenum @pname, const GLfloat * @params) => glTexEnvfvPtr(@target, @pname, @params);

        internal delegate void glTexParameterfFunc(GLenum @target, GLenum @pname, GLfloat @param);
        internal static glTexParameterfFunc glTexParameterfPtr;
        internal static void loadTexParameterf()
        {
            try
            {
                glTexParameterfPtr = (glTexParameterfFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterf"), typeof(glTexParameterfFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterf'.");
            }
        }
        public static void glTexParameterf(GLenum @target, GLenum @pname, GLfloat @param) => glTexParameterfPtr(@target, @pname, @param);

        internal delegate void glTexParameterfvFunc(GLenum @target, GLenum @pname, const GLfloat * @params);
        internal static glTexParameterfvFunc glTexParameterfvPtr;
        internal static void loadTexParameterfv()
        {
            try
            {
                glTexParameterfvPtr = (glTexParameterfvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterfv"), typeof(glTexParameterfvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterfv'.");
            }
        }
        public static void glTexParameterfv(GLenum @target, GLenum @pname, const GLfloat * @params) => glTexParameterfvPtr(@target, @pname, @params);

        internal delegate void glTranslatefFunc(GLfloat @x, GLfloat @y, GLfloat @z);
        internal static glTranslatefFunc glTranslatefPtr;
        internal static void loadTranslatef()
        {
            try
            {
                glTranslatefPtr = (glTranslatefFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTranslatef"), typeof(glTranslatefFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTranslatef'.");
            }
        }
        public static void glTranslatef(GLfloat @x, GLfloat @y, GLfloat @z) => glTranslatefPtr(@x, @y, @z);

        internal delegate void glActiveTextureFunc(GLenum @texture);
        internal static glActiveTextureFunc glActiveTexturePtr;
        internal static void loadActiveTexture()
        {
            try
            {
                glActiveTexturePtr = (glActiveTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glActiveTexture"), typeof(glActiveTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glActiveTexture'.");
            }
        }
        public static void glActiveTexture(GLenum @texture) => glActiveTexturePtr(@texture);

        internal delegate void glAlphaFuncxFunc(GLenum @func, GLfixed @ref);
        internal static glAlphaFuncxFunc glAlphaFuncxPtr;
        internal static void loadAlphaFuncx()
        {
            try
            {
                glAlphaFuncxPtr = (glAlphaFuncxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glAlphaFuncx"), typeof(glAlphaFuncxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glAlphaFuncx'.");
            }
        }
        public static void glAlphaFuncx(GLenum @func, GLfixed @ref) => glAlphaFuncxPtr(@func, @ref);

        internal delegate void glBindBufferFunc(GLenum @target, GLuint @buffer);
        internal static glBindBufferFunc glBindBufferPtr;
        internal static void loadBindBuffer()
        {
            try
            {
                glBindBufferPtr = (glBindBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindBuffer"), typeof(glBindBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindBuffer'.");
            }
        }
        public static void glBindBuffer(GLenum @target, GLuint @buffer) => glBindBufferPtr(@target, @buffer);

        internal delegate void glBindTextureFunc(GLenum @target, GLuint @texture);
        internal static glBindTextureFunc glBindTexturePtr;
        internal static void loadBindTexture()
        {
            try
            {
                glBindTexturePtr = (glBindTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBindTexture"), typeof(glBindTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBindTexture'.");
            }
        }
        public static void glBindTexture(GLenum @target, GLuint @texture) => glBindTexturePtr(@target, @texture);

        internal delegate void glBlendFuncFunc(GLenum @sfactor, GLenum @dfactor);
        internal static glBlendFuncFunc glBlendFuncPtr;
        internal static void loadBlendFunc()
        {
            try
            {
                glBlendFuncPtr = (glBlendFuncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBlendFunc"), typeof(glBlendFuncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBlendFunc'.");
            }
        }
        public static void glBlendFunc(GLenum @sfactor, GLenum @dfactor) => glBlendFuncPtr(@sfactor, @dfactor);

        internal delegate void glBufferDataFunc(GLenum @target, GLsizeiptr @size, const void * @data, GLenum @usage);
        internal static glBufferDataFunc glBufferDataPtr;
        internal static void loadBufferData()
        {
            try
            {
                glBufferDataPtr = (glBufferDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferData"), typeof(glBufferDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferData'.");
            }
        }
        public static void glBufferData(GLenum @target, GLsizeiptr @size, const void * @data, GLenum @usage) => glBufferDataPtr(@target, @size, @data, @usage);

        internal delegate void glBufferSubDataFunc(GLenum @target, GLintptr @offset, GLsizeiptr @size, const void * @data);
        internal static glBufferSubDataFunc glBufferSubDataPtr;
        internal static void loadBufferSubData()
        {
            try
            {
                glBufferSubDataPtr = (glBufferSubDataFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glBufferSubData"), typeof(glBufferSubDataFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glBufferSubData'.");
            }
        }
        public static void glBufferSubData(GLenum @target, GLintptr @offset, GLsizeiptr @size, const void * @data) => glBufferSubDataPtr(@target, @offset, @size, @data);

        internal delegate void glClearFunc(GLbitfield @mask);
        internal static glClearFunc glClearPtr;
        internal static void loadClear()
        {
            try
            {
                glClearPtr = (glClearFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClear"), typeof(glClearFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClear'.");
            }
        }
        public static void glClear(GLbitfield @mask) => glClearPtr(@mask);

        internal delegate void glClearColorxFunc(GLfixed @red, GLfixed @green, GLfixed @blue, GLfixed @alpha);
        internal static glClearColorxFunc glClearColorxPtr;
        internal static void loadClearColorx()
        {
            try
            {
                glClearColorxPtr = (glClearColorxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearColorx"), typeof(glClearColorxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearColorx'.");
            }
        }
        public static void glClearColorx(GLfixed @red, GLfixed @green, GLfixed @blue, GLfixed @alpha) => glClearColorxPtr(@red, @green, @blue, @alpha);

        internal delegate void glClearDepthxFunc(GLfixed @depth);
        internal static glClearDepthxFunc glClearDepthxPtr;
        internal static void loadClearDepthx()
        {
            try
            {
                glClearDepthxPtr = (glClearDepthxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearDepthx"), typeof(glClearDepthxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearDepthx'.");
            }
        }
        public static void glClearDepthx(GLfixed @depth) => glClearDepthxPtr(@depth);

        internal delegate void glClearStencilFunc(GLint @s);
        internal static glClearStencilFunc glClearStencilPtr;
        internal static void loadClearStencil()
        {
            try
            {
                glClearStencilPtr = (glClearStencilFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClearStencil"), typeof(glClearStencilFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClearStencil'.");
            }
        }
        public static void glClearStencil(GLint @s) => glClearStencilPtr(@s);

        internal delegate void glClientActiveTextureFunc(GLenum @texture);
        internal static glClientActiveTextureFunc glClientActiveTexturePtr;
        internal static void loadClientActiveTexture()
        {
            try
            {
                glClientActiveTexturePtr = (glClientActiveTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClientActiveTexture"), typeof(glClientActiveTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClientActiveTexture'.");
            }
        }
        public static void glClientActiveTexture(GLenum @texture) => glClientActiveTexturePtr(@texture);

        internal delegate void glClipPlanexFunc(GLenum @plane, const GLfixed * @equation);
        internal static glClipPlanexFunc glClipPlanexPtr;
        internal static void loadClipPlanex()
        {
            try
            {
                glClipPlanexPtr = (glClipPlanexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glClipPlanex"), typeof(glClipPlanexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glClipPlanex'.");
            }
        }
        public static void glClipPlanex(GLenum @plane, const GLfixed * @equation) => glClipPlanexPtr(@plane, @equation);

        internal delegate void glColor4ubFunc(GLubyte @red, GLubyte @green, GLubyte @blue, GLubyte @alpha);
        internal static glColor4ubFunc glColor4ubPtr;
        internal static void loadColor4ub()
        {
            try
            {
                glColor4ubPtr = (glColor4ubFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4ub"), typeof(glColor4ubFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4ub'.");
            }
        }
        public static void glColor4ub(GLubyte @red, GLubyte @green, GLubyte @blue, GLubyte @alpha) => glColor4ubPtr(@red, @green, @blue, @alpha);

        internal delegate void glColor4xFunc(GLfixed @red, GLfixed @green, GLfixed @blue, GLfixed @alpha);
        internal static glColor4xFunc glColor4xPtr;
        internal static void loadColor4x()
        {
            try
            {
                glColor4xPtr = (glColor4xFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColor4x"), typeof(glColor4xFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColor4x'.");
            }
        }
        public static void glColor4x(GLfixed @red, GLfixed @green, GLfixed @blue, GLfixed @alpha) => glColor4xPtr(@red, @green, @blue, @alpha);

        internal delegate void glColorMaskFunc(GLboolean @red, GLboolean @green, GLboolean @blue, GLboolean @alpha);
        internal static glColorMaskFunc glColorMaskPtr;
        internal static void loadColorMask()
        {
            try
            {
                glColorMaskPtr = (glColorMaskFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorMask"), typeof(glColorMaskFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorMask'.");
            }
        }
        public static void glColorMask(GLboolean @red, GLboolean @green, GLboolean @blue, GLboolean @alpha) => glColorMaskPtr(@red, @green, @blue, @alpha);

        internal delegate void glColorPointerFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glColorPointerFunc glColorPointerPtr;
        internal static void loadColorPointer()
        {
            try
            {
                glColorPointerPtr = (glColorPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glColorPointer"), typeof(glColorPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glColorPointer'.");
            }
        }
        public static void glColorPointer(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glColorPointerPtr(@size, @type, @stride, @pointer);

        internal delegate void glCompressedTexImage2DFunc(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexImage2DFunc glCompressedTexImage2DPtr;
        internal static void loadCompressedTexImage2D()
        {
            try
            {
                glCompressedTexImage2DPtr = (glCompressedTexImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexImage2D"), typeof(glCompressedTexImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexImage2D'.");
            }
        }
        public static void glCompressedTexImage2D(GLenum @target, GLint @level, GLenum @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLsizei @imageSize, const void * @data) => glCompressedTexImage2DPtr(@target, @level, @internalformat, @width, @height, @border, @imageSize, @data);

        internal delegate void glCompressedTexSubImage2DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @data);
        internal static glCompressedTexSubImage2DFunc glCompressedTexSubImage2DPtr;
        internal static void loadCompressedTexSubImage2D()
        {
            try
            {
                glCompressedTexSubImage2DPtr = (glCompressedTexSubImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCompressedTexSubImage2D"), typeof(glCompressedTexSubImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCompressedTexSubImage2D'.");
            }
        }
        public static void glCompressedTexSubImage2D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLsizei @imageSize, const void * @data) => glCompressedTexSubImage2DPtr(@target, @level, @xoffset, @yoffset, @width, @height, @format, @imageSize, @data);

        internal delegate void glCopyTexImage2DFunc(GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLint @border);
        internal static glCopyTexImage2DFunc glCopyTexImage2DPtr;
        internal static void loadCopyTexImage2D()
        {
            try
            {
                glCopyTexImage2DPtr = (glCopyTexImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexImage2D"), typeof(glCopyTexImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexImage2D'.");
            }
        }
        public static void glCopyTexImage2D(GLenum @target, GLint @level, GLenum @internalformat, GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLint @border) => glCopyTexImage2DPtr(@target, @level, @internalformat, @x, @y, @width, @height, @border);

        internal delegate void glCopyTexSubImage2DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glCopyTexSubImage2DFunc glCopyTexSubImage2DPtr;
        internal static void loadCopyTexSubImage2D()
        {
            try
            {
                glCopyTexSubImage2DPtr = (glCopyTexSubImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCopyTexSubImage2D"), typeof(glCopyTexSubImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCopyTexSubImage2D'.");
            }
        }
        public static void glCopyTexSubImage2D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glCopyTexSubImage2DPtr(@target, @level, @xoffset, @yoffset, @x, @y, @width, @height);

        internal delegate void glCullFaceFunc(GLenum @mode);
        internal static glCullFaceFunc glCullFacePtr;
        internal static void loadCullFace()
        {
            try
            {
                glCullFacePtr = (glCullFaceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glCullFace"), typeof(glCullFaceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glCullFace'.");
            }
        }
        public static void glCullFace(GLenum @mode) => glCullFacePtr(@mode);

        internal delegate void glDeleteBuffersFunc(GLsizei @n, const GLuint * @buffers);
        internal static glDeleteBuffersFunc glDeleteBuffersPtr;
        internal static void loadDeleteBuffers()
        {
            try
            {
                glDeleteBuffersPtr = (glDeleteBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteBuffers"), typeof(glDeleteBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteBuffers'.");
            }
        }
        public static void glDeleteBuffers(GLsizei @n, const GLuint * @buffers) => glDeleteBuffersPtr(@n, @buffers);

        internal delegate void glDeleteTexturesFunc(GLsizei @n, const GLuint * @textures);
        internal static glDeleteTexturesFunc glDeleteTexturesPtr;
        internal static void loadDeleteTextures()
        {
            try
            {
                glDeleteTexturesPtr = (glDeleteTexturesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDeleteTextures"), typeof(glDeleteTexturesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDeleteTextures'.");
            }
        }
        public static void glDeleteTextures(GLsizei @n, const GLuint * @textures) => glDeleteTexturesPtr(@n, @textures);

        internal delegate void glDepthFuncFunc(GLenum @func);
        internal static glDepthFuncFunc glDepthFuncPtr;
        internal static void loadDepthFunc()
        {
            try
            {
                glDepthFuncPtr = (glDepthFuncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthFunc"), typeof(glDepthFuncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthFunc'.");
            }
        }
        public static void glDepthFunc(GLenum @func) => glDepthFuncPtr(@func);

        internal delegate void glDepthMaskFunc(GLboolean @flag);
        internal static glDepthMaskFunc glDepthMaskPtr;
        internal static void loadDepthMask()
        {
            try
            {
                glDepthMaskPtr = (glDepthMaskFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthMask"), typeof(glDepthMaskFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthMask'.");
            }
        }
        public static void glDepthMask(GLboolean @flag) => glDepthMaskPtr(@flag);

        internal delegate void glDepthRangexFunc(GLfixed @n, GLfixed @f);
        internal static glDepthRangexFunc glDepthRangexPtr;
        internal static void loadDepthRangex()
        {
            try
            {
                glDepthRangexPtr = (glDepthRangexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDepthRangex"), typeof(glDepthRangexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDepthRangex'.");
            }
        }
        public static void glDepthRangex(GLfixed @n, GLfixed @f) => glDepthRangexPtr(@n, @f);

        internal delegate void glDisableFunc(GLenum @cap);
        internal static glDisableFunc glDisablePtr;
        internal static void loadDisable()
        {
            try
            {
                glDisablePtr = (glDisableFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisable"), typeof(glDisableFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisable'.");
            }
        }
        public static void glDisable(GLenum @cap) => glDisablePtr(@cap);

        internal delegate void glDisableClientStateFunc(GLenum @array);
        internal static glDisableClientStateFunc glDisableClientStatePtr;
        internal static void loadDisableClientState()
        {
            try
            {
                glDisableClientStatePtr = (glDisableClientStateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDisableClientState"), typeof(glDisableClientStateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDisableClientState'.");
            }
        }
        public static void glDisableClientState(GLenum @array) => glDisableClientStatePtr(@array);

        internal delegate void glDrawArraysFunc(GLenum @mode, GLint @first, GLsizei @count);
        internal static glDrawArraysFunc glDrawArraysPtr;
        internal static void loadDrawArrays()
        {
            try
            {
                glDrawArraysPtr = (glDrawArraysFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawArrays"), typeof(glDrawArraysFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawArrays'.");
            }
        }
        public static void glDrawArrays(GLenum @mode, GLint @first, GLsizei @count) => glDrawArraysPtr(@mode, @first, @count);

        internal delegate void glDrawElementsFunc(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices);
        internal static glDrawElementsFunc glDrawElementsPtr;
        internal static void loadDrawElements()
        {
            try
            {
                glDrawElementsPtr = (glDrawElementsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glDrawElements"), typeof(glDrawElementsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glDrawElements'.");
            }
        }
        public static void glDrawElements(GLenum @mode, GLsizei @count, GLenum @type, const void * @indices) => glDrawElementsPtr(@mode, @count, @type, @indices);

        internal delegate void glEnableFunc(GLenum @cap);
        internal static glEnableFunc glEnablePtr;
        internal static void loadEnable()
        {
            try
            {
                glEnablePtr = (glEnableFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnable"), typeof(glEnableFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnable'.");
            }
        }
        public static void glEnable(GLenum @cap) => glEnablePtr(@cap);

        internal delegate void glEnableClientStateFunc(GLenum @array);
        internal static glEnableClientStateFunc glEnableClientStatePtr;
        internal static void loadEnableClientState()
        {
            try
            {
                glEnableClientStatePtr = (glEnableClientStateFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glEnableClientState"), typeof(glEnableClientStateFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glEnableClientState'.");
            }
        }
        public static void glEnableClientState(GLenum @array) => glEnableClientStatePtr(@array);

        internal delegate void glFinishFunc();
        internal static glFinishFunc glFinishPtr;
        internal static void loadFinish()
        {
            try
            {
                glFinishPtr = (glFinishFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFinish"), typeof(glFinishFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFinish'.");
            }
        }
        public static void glFinish() => glFinishPtr();

        internal delegate void glFlushFunc();
        internal static glFlushFunc glFlushPtr;
        internal static void loadFlush()
        {
            try
            {
                glFlushPtr = (glFlushFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFlush"), typeof(glFlushFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFlush'.");
            }
        }
        public static void glFlush() => glFlushPtr();

        internal delegate void glFogxFunc(GLenum @pname, GLfixed @param);
        internal static glFogxFunc glFogxPtr;
        internal static void loadFogx()
        {
            try
            {
                glFogxPtr = (glFogxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogx"), typeof(glFogxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogx'.");
            }
        }
        public static void glFogx(GLenum @pname, GLfixed @param) => glFogxPtr(@pname, @param);

        internal delegate void glFogxvFunc(GLenum @pname, const GLfixed * @param);
        internal static glFogxvFunc glFogxvPtr;
        internal static void loadFogxv()
        {
            try
            {
                glFogxvPtr = (glFogxvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFogxv"), typeof(glFogxvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFogxv'.");
            }
        }
        public static void glFogxv(GLenum @pname, const GLfixed * @param) => glFogxvPtr(@pname, @param);

        internal delegate void glFrontFaceFunc(GLenum @mode);
        internal static glFrontFaceFunc glFrontFacePtr;
        internal static void loadFrontFace()
        {
            try
            {
                glFrontFacePtr = (glFrontFaceFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFrontFace"), typeof(glFrontFaceFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFrontFace'.");
            }
        }
        public static void glFrontFace(GLenum @mode) => glFrontFacePtr(@mode);

        internal delegate void glFrustumxFunc(GLfixed @l, GLfixed @r, GLfixed @b, GLfixed @t, GLfixed @n, GLfixed @f);
        internal static glFrustumxFunc glFrustumxPtr;
        internal static void loadFrustumx()
        {
            try
            {
                glFrustumxPtr = (glFrustumxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFrustumx"), typeof(glFrustumxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFrustumx'.");
            }
        }
        public static void glFrustumx(GLfixed @l, GLfixed @r, GLfixed @b, GLfixed @t, GLfixed @n, GLfixed @f) => glFrustumxPtr(@l, @r, @b, @t, @n, @f);

        internal delegate void glGetBooleanvFunc(GLenum @pname, GLboolean * @data);
        internal static glGetBooleanvFunc glGetBooleanvPtr;
        internal static void loadGetBooleanv()
        {
            try
            {
                glGetBooleanvPtr = (glGetBooleanvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBooleanv"), typeof(glGetBooleanvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBooleanv'.");
            }
        }
        public static void glGetBooleanv(GLenum @pname, GLboolean * @data) => glGetBooleanvPtr(@pname, @data);

        internal delegate void glGetBufferParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetBufferParameterivFunc glGetBufferParameterivPtr;
        internal static void loadGetBufferParameteriv()
        {
            try
            {
                glGetBufferParameterivPtr = (glGetBufferParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetBufferParameteriv"), typeof(glGetBufferParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetBufferParameteriv'.");
            }
        }
        public static void glGetBufferParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetBufferParameterivPtr(@target, @pname, @params);

        internal delegate void glGetClipPlanexFunc(GLenum @plane, GLfixed * @equation);
        internal static glGetClipPlanexFunc glGetClipPlanexPtr;
        internal static void loadGetClipPlanex()
        {
            try
            {
                glGetClipPlanexPtr = (glGetClipPlanexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetClipPlanex"), typeof(glGetClipPlanexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetClipPlanex'.");
            }
        }
        public static void glGetClipPlanex(GLenum @plane, GLfixed * @equation) => glGetClipPlanexPtr(@plane, @equation);

        internal delegate void glGenBuffersFunc(GLsizei @n, GLuint * @buffers);
        internal static glGenBuffersFunc glGenBuffersPtr;
        internal static void loadGenBuffers()
        {
            try
            {
                glGenBuffersPtr = (glGenBuffersFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenBuffers"), typeof(glGenBuffersFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenBuffers'.");
            }
        }
        public static void glGenBuffers(GLsizei @n, GLuint * @buffers) => glGenBuffersPtr(@n, @buffers);

        internal delegate void glGenTexturesFunc(GLsizei @n, GLuint * @textures);
        internal static glGenTexturesFunc glGenTexturesPtr;
        internal static void loadGenTextures()
        {
            try
            {
                glGenTexturesPtr = (glGenTexturesFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGenTextures"), typeof(glGenTexturesFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGenTextures'.");
            }
        }
        public static void glGenTextures(GLsizei @n, GLuint * @textures) => glGenTexturesPtr(@n, @textures);

        internal delegate GLenum glGetErrorFunc();
        internal static glGetErrorFunc glGetErrorPtr;
        internal static void loadGetError()
        {
            try
            {
                glGetErrorPtr = (glGetErrorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetError"), typeof(glGetErrorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetError'.");
            }
        }
        public static GLenum glGetError() => glGetErrorPtr();

        internal delegate void glGetFixedvFunc(GLenum @pname, GLfixed * @params);
        internal static glGetFixedvFunc glGetFixedvPtr;
        internal static void loadGetFixedv()
        {
            try
            {
                glGetFixedvPtr = (glGetFixedvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetFixedv"), typeof(glGetFixedvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetFixedv'.");
            }
        }
        public static void glGetFixedv(GLenum @pname, GLfixed * @params) => glGetFixedvPtr(@pname, @params);

        internal delegate void glGetIntegervFunc(GLenum @pname, GLint * @data);
        internal static glGetIntegervFunc glGetIntegervPtr;
        internal static void loadGetIntegerv()
        {
            try
            {
                glGetIntegervPtr = (glGetIntegervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetIntegerv"), typeof(glGetIntegervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetIntegerv'.");
            }
        }
        public static void glGetIntegerv(GLenum @pname, GLint * @data) => glGetIntegervPtr(@pname, @data);

        internal delegate void glGetLightxvFunc(GLenum @light, GLenum @pname, GLfixed * @params);
        internal static glGetLightxvFunc glGetLightxvPtr;
        internal static void loadGetLightxv()
        {
            try
            {
                glGetLightxvPtr = (glGetLightxvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetLightxv"), typeof(glGetLightxvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetLightxv'.");
            }
        }
        public static void glGetLightxv(GLenum @light, GLenum @pname, GLfixed * @params) => glGetLightxvPtr(@light, @pname, @params);

        internal delegate void glGetMaterialxvFunc(GLenum @face, GLenum @pname, GLfixed * @params);
        internal static glGetMaterialxvFunc glGetMaterialxvPtr;
        internal static void loadGetMaterialxv()
        {
            try
            {
                glGetMaterialxvPtr = (glGetMaterialxvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetMaterialxv"), typeof(glGetMaterialxvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetMaterialxv'.");
            }
        }
        public static void glGetMaterialxv(GLenum @face, GLenum @pname, GLfixed * @params) => glGetMaterialxvPtr(@face, @pname, @params);

        internal delegate void glGetPointervFunc(GLenum @pname, void ** @params);
        internal static glGetPointervFunc glGetPointervPtr;
        internal static void loadGetPointerv()
        {
            try
            {
                glGetPointervPtr = (glGetPointervFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetPointerv"), typeof(glGetPointervFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetPointerv'.");
            }
        }
        public static void glGetPointerv(GLenum @pname, void ** @params) => glGetPointervPtr(@pname, @params);

        internal delegate constGLubyte* glGetStringFunc(GLenum @name);
        internal static glGetStringFunc glGetStringPtr;
        internal static void loadGetString()
        {
            try
            {
                glGetStringPtr = (glGetStringFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetString"), typeof(glGetStringFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetString'.");
            }
        }
        public static constGLubyte* glGetString(GLenum @name) => glGetStringPtr(@name);

        internal delegate void glGetTexEnvivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetTexEnvivFunc glGetTexEnvivPtr;
        internal static void loadGetTexEnviv()
        {
            try
            {
                glGetTexEnvivPtr = (glGetTexEnvivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexEnviv"), typeof(glGetTexEnvivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexEnviv'.");
            }
        }
        public static void glGetTexEnviv(GLenum @target, GLenum @pname, GLint * @params) => glGetTexEnvivPtr(@target, @pname, @params);

        internal delegate void glGetTexEnvxvFunc(GLenum @target, GLenum @pname, GLfixed * @params);
        internal static glGetTexEnvxvFunc glGetTexEnvxvPtr;
        internal static void loadGetTexEnvxv()
        {
            try
            {
                glGetTexEnvxvPtr = (glGetTexEnvxvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexEnvxv"), typeof(glGetTexEnvxvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexEnvxv'.");
            }
        }
        public static void glGetTexEnvxv(GLenum @target, GLenum @pname, GLfixed * @params) => glGetTexEnvxvPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterivFunc(GLenum @target, GLenum @pname, GLint * @params);
        internal static glGetTexParameterivFunc glGetTexParameterivPtr;
        internal static void loadGetTexParameteriv()
        {
            try
            {
                glGetTexParameterivPtr = (glGetTexParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameteriv"), typeof(glGetTexParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameteriv'.");
            }
        }
        public static void glGetTexParameteriv(GLenum @target, GLenum @pname, GLint * @params) => glGetTexParameterivPtr(@target, @pname, @params);

        internal delegate void glGetTexParameterxvFunc(GLenum @target, GLenum @pname, GLfixed * @params);
        internal static glGetTexParameterxvFunc glGetTexParameterxvPtr;
        internal static void loadGetTexParameterxv()
        {
            try
            {
                glGetTexParameterxvPtr = (glGetTexParameterxvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glGetTexParameterxv"), typeof(glGetTexParameterxvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glGetTexParameterxv'.");
            }
        }
        public static void glGetTexParameterxv(GLenum @target, GLenum @pname, GLfixed * @params) => glGetTexParameterxvPtr(@target, @pname, @params);

        internal delegate void glHintFunc(GLenum @target, GLenum @mode);
        internal static glHintFunc glHintPtr;
        internal static void loadHint()
        {
            try
            {
                glHintPtr = (glHintFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glHint"), typeof(glHintFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glHint'.");
            }
        }
        public static void glHint(GLenum @target, GLenum @mode) => glHintPtr(@target, @mode);

        internal delegate GLboolean glIsBufferFunc(GLuint @buffer);
        internal static glIsBufferFunc glIsBufferPtr;
        internal static void loadIsBuffer()
        {
            try
            {
                glIsBufferPtr = (glIsBufferFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsBuffer"), typeof(glIsBufferFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsBuffer'.");
            }
        }
        public static GLboolean glIsBuffer(GLuint @buffer) => glIsBufferPtr(@buffer);

        internal delegate GLboolean glIsEnabledFunc(GLenum @cap);
        internal static glIsEnabledFunc glIsEnabledPtr;
        internal static void loadIsEnabled()
        {
            try
            {
                glIsEnabledPtr = (glIsEnabledFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsEnabled"), typeof(glIsEnabledFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsEnabled'.");
            }
        }
        public static GLboolean glIsEnabled(GLenum @cap) => glIsEnabledPtr(@cap);

        internal delegate GLboolean glIsTextureFunc(GLuint @texture);
        internal static glIsTextureFunc glIsTexturePtr;
        internal static void loadIsTexture()
        {
            try
            {
                glIsTexturePtr = (glIsTextureFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glIsTexture"), typeof(glIsTextureFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glIsTexture'.");
            }
        }
        public static GLboolean glIsTexture(GLuint @texture) => glIsTexturePtr(@texture);

        internal delegate void glLightModelxFunc(GLenum @pname, GLfixed @param);
        internal static glLightModelxFunc glLightModelxPtr;
        internal static void loadLightModelx()
        {
            try
            {
                glLightModelxPtr = (glLightModelxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightModelx"), typeof(glLightModelxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightModelx'.");
            }
        }
        public static void glLightModelx(GLenum @pname, GLfixed @param) => glLightModelxPtr(@pname, @param);

        internal delegate void glLightModelxvFunc(GLenum @pname, const GLfixed * @param);
        internal static glLightModelxvFunc glLightModelxvPtr;
        internal static void loadLightModelxv()
        {
            try
            {
                glLightModelxvPtr = (glLightModelxvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightModelxv"), typeof(glLightModelxvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightModelxv'.");
            }
        }
        public static void glLightModelxv(GLenum @pname, const GLfixed * @param) => glLightModelxvPtr(@pname, @param);

        internal delegate void glLightxFunc(GLenum @light, GLenum @pname, GLfixed @param);
        internal static glLightxFunc glLightxPtr;
        internal static void loadLightx()
        {
            try
            {
                glLightxPtr = (glLightxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightx"), typeof(glLightxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightx'.");
            }
        }
        public static void glLightx(GLenum @light, GLenum @pname, GLfixed @param) => glLightxPtr(@light, @pname, @param);

        internal delegate void glLightxvFunc(GLenum @light, GLenum @pname, const GLfixed * @params);
        internal static glLightxvFunc glLightxvPtr;
        internal static void loadLightxv()
        {
            try
            {
                glLightxvPtr = (glLightxvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLightxv"), typeof(glLightxvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLightxv'.");
            }
        }
        public static void glLightxv(GLenum @light, GLenum @pname, const GLfixed * @params) => glLightxvPtr(@light, @pname, @params);

        internal delegate void glLineWidthxFunc(GLfixed @width);
        internal static glLineWidthxFunc glLineWidthxPtr;
        internal static void loadLineWidthx()
        {
            try
            {
                glLineWidthxPtr = (glLineWidthxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLineWidthx"), typeof(glLineWidthxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLineWidthx'.");
            }
        }
        public static void glLineWidthx(GLfixed @width) => glLineWidthxPtr(@width);

        internal delegate void glLoadIdentityFunc();
        internal static glLoadIdentityFunc glLoadIdentityPtr;
        internal static void loadLoadIdentity()
        {
            try
            {
                glLoadIdentityPtr = (glLoadIdentityFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadIdentity"), typeof(glLoadIdentityFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadIdentity'.");
            }
        }
        public static void glLoadIdentity() => glLoadIdentityPtr();

        internal delegate void glLoadMatrixxFunc(const GLfixed * @m);
        internal static glLoadMatrixxFunc glLoadMatrixxPtr;
        internal static void loadLoadMatrixx()
        {
            try
            {
                glLoadMatrixxPtr = (glLoadMatrixxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLoadMatrixx"), typeof(glLoadMatrixxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLoadMatrixx'.");
            }
        }
        public static void glLoadMatrixx(const GLfixed * @m) => glLoadMatrixxPtr(@m);

        internal delegate void glLogicOpFunc(GLenum @opcode);
        internal static glLogicOpFunc glLogicOpPtr;
        internal static void loadLogicOp()
        {
            try
            {
                glLogicOpPtr = (glLogicOpFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glLogicOp"), typeof(glLogicOpFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glLogicOp'.");
            }
        }
        public static void glLogicOp(GLenum @opcode) => glLogicOpPtr(@opcode);

        internal delegate void glMaterialxFunc(GLenum @face, GLenum @pname, GLfixed @param);
        internal static glMaterialxFunc glMaterialxPtr;
        internal static void loadMaterialx()
        {
            try
            {
                glMaterialxPtr = (glMaterialxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMaterialx"), typeof(glMaterialxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMaterialx'.");
            }
        }
        public static void glMaterialx(GLenum @face, GLenum @pname, GLfixed @param) => glMaterialxPtr(@face, @pname, @param);

        internal delegate void glMaterialxvFunc(GLenum @face, GLenum @pname, const GLfixed * @param);
        internal static glMaterialxvFunc glMaterialxvPtr;
        internal static void loadMaterialxv()
        {
            try
            {
                glMaterialxvPtr = (glMaterialxvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMaterialxv"), typeof(glMaterialxvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMaterialxv'.");
            }
        }
        public static void glMaterialxv(GLenum @face, GLenum @pname, const GLfixed * @param) => glMaterialxvPtr(@face, @pname, @param);

        internal delegate void glMatrixModeFunc(GLenum @mode);
        internal static glMatrixModeFunc glMatrixModePtr;
        internal static void loadMatrixMode()
        {
            try
            {
                glMatrixModePtr = (glMatrixModeFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMatrixMode"), typeof(glMatrixModeFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMatrixMode'.");
            }
        }
        public static void glMatrixMode(GLenum @mode) => glMatrixModePtr(@mode);

        internal delegate void glMultMatrixxFunc(const GLfixed * @m);
        internal static glMultMatrixxFunc glMultMatrixxPtr;
        internal static void loadMultMatrixx()
        {
            try
            {
                glMultMatrixxPtr = (glMultMatrixxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultMatrixx"), typeof(glMultMatrixxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultMatrixx'.");
            }
        }
        public static void glMultMatrixx(const GLfixed * @m) => glMultMatrixxPtr(@m);

        internal delegate void glMultiTexCoord4xFunc(GLenum @texture, GLfixed @s, GLfixed @t, GLfixed @r, GLfixed @q);
        internal static glMultiTexCoord4xFunc glMultiTexCoord4xPtr;
        internal static void loadMultiTexCoord4x()
        {
            try
            {
                glMultiTexCoord4xPtr = (glMultiTexCoord4xFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glMultiTexCoord4x"), typeof(glMultiTexCoord4xFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glMultiTexCoord4x'.");
            }
        }
        public static void glMultiTexCoord4x(GLenum @texture, GLfixed @s, GLfixed @t, GLfixed @r, GLfixed @q) => glMultiTexCoord4xPtr(@texture, @s, @t, @r, @q);

        internal delegate void glNormal3xFunc(GLfixed @nx, GLfixed @ny, GLfixed @nz);
        internal static glNormal3xFunc glNormal3xPtr;
        internal static void loadNormal3x()
        {
            try
            {
                glNormal3xPtr = (glNormal3xFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormal3x"), typeof(glNormal3xFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormal3x'.");
            }
        }
        public static void glNormal3x(GLfixed @nx, GLfixed @ny, GLfixed @nz) => glNormal3xPtr(@nx, @ny, @nz);

        internal delegate void glNormalPointerFunc(GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glNormalPointerFunc glNormalPointerPtr;
        internal static void loadNormalPointer()
        {
            try
            {
                glNormalPointerPtr = (glNormalPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glNormalPointer"), typeof(glNormalPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glNormalPointer'.");
            }
        }
        public static void glNormalPointer(GLenum @type, GLsizei @stride, const void * @pointer) => glNormalPointerPtr(@type, @stride, @pointer);

        internal delegate void glOrthoxFunc(GLfixed @l, GLfixed @r, GLfixed @b, GLfixed @t, GLfixed @n, GLfixed @f);
        internal static glOrthoxFunc glOrthoxPtr;
        internal static void loadOrthox()
        {
            try
            {
                glOrthoxPtr = (glOrthoxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glOrthox"), typeof(glOrthoxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glOrthox'.");
            }
        }
        public static void glOrthox(GLfixed @l, GLfixed @r, GLfixed @b, GLfixed @t, GLfixed @n, GLfixed @f) => glOrthoxPtr(@l, @r, @b, @t, @n, @f);

        internal delegate void glPixelStoreiFunc(GLenum @pname, GLint @param);
        internal static glPixelStoreiFunc glPixelStoreiPtr;
        internal static void loadPixelStorei()
        {
            try
            {
                glPixelStoreiPtr = (glPixelStoreiFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPixelStorei"), typeof(glPixelStoreiFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPixelStorei'.");
            }
        }
        public static void glPixelStorei(GLenum @pname, GLint @param) => glPixelStoreiPtr(@pname, @param);

        internal delegate void glPointParameterxFunc(GLenum @pname, GLfixed @param);
        internal static glPointParameterxFunc glPointParameterxPtr;
        internal static void loadPointParameterx()
        {
            try
            {
                glPointParameterxPtr = (glPointParameterxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterx"), typeof(glPointParameterxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterx'.");
            }
        }
        public static void glPointParameterx(GLenum @pname, GLfixed @param) => glPointParameterxPtr(@pname, @param);

        internal delegate void glPointParameterxvFunc(GLenum @pname, const GLfixed * @params);
        internal static glPointParameterxvFunc glPointParameterxvPtr;
        internal static void loadPointParameterxv()
        {
            try
            {
                glPointParameterxvPtr = (glPointParameterxvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointParameterxv"), typeof(glPointParameterxvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointParameterxv'.");
            }
        }
        public static void glPointParameterxv(GLenum @pname, const GLfixed * @params) => glPointParameterxvPtr(@pname, @params);

        internal delegate void glPointSizexFunc(GLfixed @size);
        internal static glPointSizexFunc glPointSizexPtr;
        internal static void loadPointSizex()
        {
            try
            {
                glPointSizexPtr = (glPointSizexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPointSizex"), typeof(glPointSizexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPointSizex'.");
            }
        }
        public static void glPointSizex(GLfixed @size) => glPointSizexPtr(@size);

        internal delegate void glPolygonOffsetxFunc(GLfixed @factor, GLfixed @units);
        internal static glPolygonOffsetxFunc glPolygonOffsetxPtr;
        internal static void loadPolygonOffsetx()
        {
            try
            {
                glPolygonOffsetxPtr = (glPolygonOffsetxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPolygonOffsetx"), typeof(glPolygonOffsetxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPolygonOffsetx'.");
            }
        }
        public static void glPolygonOffsetx(GLfixed @factor, GLfixed @units) => glPolygonOffsetxPtr(@factor, @units);

        internal delegate void glPopMatrixFunc();
        internal static glPopMatrixFunc glPopMatrixPtr;
        internal static void loadPopMatrix()
        {
            try
            {
                glPopMatrixPtr = (glPopMatrixFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPopMatrix"), typeof(glPopMatrixFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPopMatrix'.");
            }
        }
        public static void glPopMatrix() => glPopMatrixPtr();

        internal delegate void glPushMatrixFunc();
        internal static glPushMatrixFunc glPushMatrixPtr;
        internal static void loadPushMatrix()
        {
            try
            {
                glPushMatrixPtr = (glPushMatrixFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glPushMatrix"), typeof(glPushMatrixFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glPushMatrix'.");
            }
        }
        public static void glPushMatrix() => glPushMatrixPtr();

        internal delegate void glReadPixelsFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, void * @pixels);
        internal static glReadPixelsFunc glReadPixelsPtr;
        internal static void loadReadPixels()
        {
            try
            {
                glReadPixelsPtr = (glReadPixelsFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glReadPixels"), typeof(glReadPixelsFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glReadPixels'.");
            }
        }
        public static void glReadPixels(GLint @x, GLint @y, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, void * @pixels) => glReadPixelsPtr(@x, @y, @width, @height, @format, @type, @pixels);

        internal delegate void glRotatexFunc(GLfixed @angle, GLfixed @x, GLfixed @y, GLfixed @z);
        internal static glRotatexFunc glRotatexPtr;
        internal static void loadRotatex()
        {
            try
            {
                glRotatexPtr = (glRotatexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glRotatex"), typeof(glRotatexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glRotatex'.");
            }
        }
        public static void glRotatex(GLfixed @angle, GLfixed @x, GLfixed @y, GLfixed @z) => glRotatexPtr(@angle, @x, @y, @z);

        internal delegate void glSampleCoverageFunc(GLfloat @value, GLboolean @invert);
        internal static glSampleCoverageFunc glSampleCoveragePtr;
        internal static void loadSampleCoverage()
        {
            try
            {
                glSampleCoveragePtr = (glSampleCoverageFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleCoverage"), typeof(glSampleCoverageFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleCoverage'.");
            }
        }
        public static void glSampleCoverage(GLfloat @value, GLboolean @invert) => glSampleCoveragePtr(@value, @invert);

        internal delegate void glSampleCoveragexFunc(GLclampx @value, GLboolean @invert);
        internal static glSampleCoveragexFunc glSampleCoveragexPtr;
        internal static void loadSampleCoveragex()
        {
            try
            {
                glSampleCoveragexPtr = (glSampleCoveragexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glSampleCoveragex"), typeof(glSampleCoveragexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glSampleCoveragex'.");
            }
        }
        public static void glSampleCoveragex(GLclampx @value, GLboolean @invert) => glSampleCoveragexPtr(@value, @invert);

        internal delegate void glScalexFunc(GLfixed @x, GLfixed @y, GLfixed @z);
        internal static glScalexFunc glScalexPtr;
        internal static void loadScalex()
        {
            try
            {
                glScalexPtr = (glScalexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScalex"), typeof(glScalexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScalex'.");
            }
        }
        public static void glScalex(GLfixed @x, GLfixed @y, GLfixed @z) => glScalexPtr(@x, @y, @z);

        internal delegate void glScissorFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glScissorFunc glScissorPtr;
        internal static void loadScissor()
        {
            try
            {
                glScissorPtr = (glScissorFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glScissor"), typeof(glScissorFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glScissor'.");
            }
        }
        public static void glScissor(GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glScissorPtr(@x, @y, @width, @height);

        internal delegate void glShadeModelFunc(GLenum @mode);
        internal static glShadeModelFunc glShadeModelPtr;
        internal static void loadShadeModel()
        {
            try
            {
                glShadeModelPtr = (glShadeModelFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glShadeModel"), typeof(glShadeModelFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glShadeModel'.");
            }
        }
        public static void glShadeModel(GLenum @mode) => glShadeModelPtr(@mode);

        internal delegate void glStencilFuncFunc(GLenum @func, GLint @ref, GLuint @mask);
        internal static glStencilFuncFunc glStencilFuncPtr;
        internal static void loadStencilFunc()
        {
            try
            {
                glStencilFuncPtr = (glStencilFuncFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilFunc"), typeof(glStencilFuncFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilFunc'.");
            }
        }
        public static void glStencilFunc(GLenum @func, GLint @ref, GLuint @mask) => glStencilFuncPtr(@func, @ref, @mask);

        internal delegate void glStencilMaskFunc(GLuint @mask);
        internal static glStencilMaskFunc glStencilMaskPtr;
        internal static void loadStencilMask()
        {
            try
            {
                glStencilMaskPtr = (glStencilMaskFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilMask"), typeof(glStencilMaskFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilMask'.");
            }
        }
        public static void glStencilMask(GLuint @mask) => glStencilMaskPtr(@mask);

        internal delegate void glStencilOpFunc(GLenum @fail, GLenum @zfail, GLenum @zpass);
        internal static glStencilOpFunc glStencilOpPtr;
        internal static void loadStencilOp()
        {
            try
            {
                glStencilOpPtr = (glStencilOpFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStencilOp"), typeof(glStencilOpFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStencilOp'.");
            }
        }
        public static void glStencilOp(GLenum @fail, GLenum @zfail, GLenum @zpass) => glStencilOpPtr(@fail, @zfail, @zpass);

        internal delegate void glTexCoordPointerFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glTexCoordPointerFunc glTexCoordPointerPtr;
        internal static void loadTexCoordPointer()
        {
            try
            {
                glTexCoordPointerPtr = (glTexCoordPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexCoordPointer"), typeof(glTexCoordPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexCoordPointer'.");
            }
        }
        public static void glTexCoordPointer(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glTexCoordPointerPtr(@size, @type, @stride, @pointer);

        internal delegate void glTexEnviFunc(GLenum @target, GLenum @pname, GLint @param);
        internal static glTexEnviFunc glTexEnviPtr;
        internal static void loadTexEnvi()
        {
            try
            {
                glTexEnviPtr = (glTexEnviFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexEnvi"), typeof(glTexEnviFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexEnvi'.");
            }
        }
        public static void glTexEnvi(GLenum @target, GLenum @pname, GLint @param) => glTexEnviPtr(@target, @pname, @param);

        internal delegate void glTexEnvxFunc(GLenum @target, GLenum @pname, GLfixed @param);
        internal static glTexEnvxFunc glTexEnvxPtr;
        internal static void loadTexEnvx()
        {
            try
            {
                glTexEnvxPtr = (glTexEnvxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexEnvx"), typeof(glTexEnvxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexEnvx'.");
            }
        }
        public static void glTexEnvx(GLenum @target, GLenum @pname, GLfixed @param) => glTexEnvxPtr(@target, @pname, @param);

        internal delegate void glTexEnvivFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glTexEnvivFunc glTexEnvivPtr;
        internal static void loadTexEnviv()
        {
            try
            {
                glTexEnvivPtr = (glTexEnvivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexEnviv"), typeof(glTexEnvivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexEnviv'.");
            }
        }
        public static void glTexEnviv(GLenum @target, GLenum @pname, const GLint * @params) => glTexEnvivPtr(@target, @pname, @params);

        internal delegate void glTexEnvxvFunc(GLenum @target, GLenum @pname, const GLfixed * @params);
        internal static glTexEnvxvFunc glTexEnvxvPtr;
        internal static void loadTexEnvxv()
        {
            try
            {
                glTexEnvxvPtr = (glTexEnvxvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexEnvxv"), typeof(glTexEnvxvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexEnvxv'.");
            }
        }
        public static void glTexEnvxv(GLenum @target, GLenum @pname, const GLfixed * @params) => glTexEnvxvPtr(@target, @pname, @params);

        internal delegate void glTexImage2DFunc(GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexImage2DFunc glTexImage2DPtr;
        internal static void loadTexImage2D()
        {
            try
            {
                glTexImage2DPtr = (glTexImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexImage2D"), typeof(glTexImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexImage2D'.");
            }
        }
        public static void glTexImage2D(GLenum @target, GLint @level, GLint @internalformat, GLsizei @width, GLsizei @height, GLint @border, GLenum @format, GLenum @type, const void * @pixels) => glTexImage2DPtr(@target, @level, @internalformat, @width, @height, @border, @format, @type, @pixels);

        internal delegate void glTexParameteriFunc(GLenum @target, GLenum @pname, GLint @param);
        internal static glTexParameteriFunc glTexParameteriPtr;
        internal static void loadTexParameteri()
        {
            try
            {
                glTexParameteriPtr = (glTexParameteriFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameteri"), typeof(glTexParameteriFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameteri'.");
            }
        }
        public static void glTexParameteri(GLenum @target, GLenum @pname, GLint @param) => glTexParameteriPtr(@target, @pname, @param);

        internal delegate void glTexParameterxFunc(GLenum @target, GLenum @pname, GLfixed @param);
        internal static glTexParameterxFunc glTexParameterxPtr;
        internal static void loadTexParameterx()
        {
            try
            {
                glTexParameterxPtr = (glTexParameterxFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterx"), typeof(glTexParameterxFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterx'.");
            }
        }
        public static void glTexParameterx(GLenum @target, GLenum @pname, GLfixed @param) => glTexParameterxPtr(@target, @pname, @param);

        internal delegate void glTexParameterivFunc(GLenum @target, GLenum @pname, const GLint * @params);
        internal static glTexParameterivFunc glTexParameterivPtr;
        internal static void loadTexParameteriv()
        {
            try
            {
                glTexParameterivPtr = (glTexParameterivFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameteriv"), typeof(glTexParameterivFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameteriv'.");
            }
        }
        public static void glTexParameteriv(GLenum @target, GLenum @pname, const GLint * @params) => glTexParameterivPtr(@target, @pname, @params);

        internal delegate void glTexParameterxvFunc(GLenum @target, GLenum @pname, const GLfixed * @params);
        internal static glTexParameterxvFunc glTexParameterxvPtr;
        internal static void loadTexParameterxv()
        {
            try
            {
                glTexParameterxvPtr = (glTexParameterxvFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexParameterxv"), typeof(glTexParameterxvFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexParameterxv'.");
            }
        }
        public static void glTexParameterxv(GLenum @target, GLenum @pname, const GLfixed * @params) => glTexParameterxvPtr(@target, @pname, @params);

        internal delegate void glTexSubImage2DFunc(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels);
        internal static glTexSubImage2DFunc glTexSubImage2DPtr;
        internal static void loadTexSubImage2D()
        {
            try
            {
                glTexSubImage2DPtr = (glTexSubImage2DFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTexSubImage2D"), typeof(glTexSubImage2DFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTexSubImage2D'.");
            }
        }
        public static void glTexSubImage2D(GLenum @target, GLint @level, GLint @xoffset, GLint @yoffset, GLsizei @width, GLsizei @height, GLenum @format, GLenum @type, const void * @pixels) => glTexSubImage2DPtr(@target, @level, @xoffset, @yoffset, @width, @height, @format, @type, @pixels);

        internal delegate void glTranslatexFunc(GLfixed @x, GLfixed @y, GLfixed @z);
        internal static glTranslatexFunc glTranslatexPtr;
        internal static void loadTranslatex()
        {
            try
            {
                glTranslatexPtr = (glTranslatexFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glTranslatex"), typeof(glTranslatexFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glTranslatex'.");
            }
        }
        public static void glTranslatex(GLfixed @x, GLfixed @y, GLfixed @z) => glTranslatexPtr(@x, @y, @z);

        internal delegate void glVertexPointerFunc(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer);
        internal static glVertexPointerFunc glVertexPointerPtr;
        internal static void loadVertexPointer()
        {
            try
            {
                glVertexPointerPtr = (glVertexPointerFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glVertexPointer"), typeof(glVertexPointerFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glVertexPointer'.");
            }
        }
        public static void glVertexPointer(GLint @size, GLenum @type, GLsizei @stride, const void * @pointer) => glVertexPointerPtr(@size, @type, @stride, @pointer);

        internal delegate void glViewportFunc(GLint @x, GLint @y, GLsizei @width, GLsizei @height);
        internal static glViewportFunc glViewportPtr;
        internal static void loadViewport()
        {
            try
            {
                glViewportPtr = (glViewportFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glViewport"), typeof(glViewportFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glViewport'.");
            }
        }
        public static void glViewport(GLint @x, GLint @y, GLsizei @width, GLsizei @height) => glViewportPtr(@x, @y, @width, @height);
        #endregion
    }
}
