using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_GREMEDY_frame_terminator
    {
        #region Interop
        static GL_GREMEDY_frame_terminator()
        {
            Console.WriteLine("Initalising GL_GREMEDY_frame_terminator interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadFrameTerminatorGREMEDY();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glFrameTerminatorGREMEDYFunc();
        internal static glFrameTerminatorGREMEDYFunc glFrameTerminatorGREMEDYPtr;
        internal static void loadFrameTerminatorGREMEDY()
        {
            try
            {
                glFrameTerminatorGREMEDYPtr = (glFrameTerminatorGREMEDYFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glFrameTerminatorGREMEDY"), typeof(glFrameTerminatorGREMEDYFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glFrameTerminatorGREMEDY'.");
            }
        }
        public static void glFrameTerminatorGREMEDY() => glFrameTerminatorGREMEDYPtr();
        #endregion
    }
}
