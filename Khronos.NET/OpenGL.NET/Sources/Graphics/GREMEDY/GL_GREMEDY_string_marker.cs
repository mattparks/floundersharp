using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL
{
    public static class GL_GREMEDY_string_marker
    {
        #region Interop
        static GL_GREMEDY_string_marker()
        {
            Console.WriteLine("Initalising GL_GREMEDY_string_marker interop methods!");

            if (OpenGLInit.GetProcAddress == null)
            {
                throw new ArgumentException("Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!", "OpenGLInit.GetProcAddress");
            }

            loadStringMarkerGREMEDY();
        }
        #endregion

        #region Enums
        #endregion

        #region Commands
        internal delegate void glStringMarkerGREMEDYFunc(GLsizei @len, const void * @string);
        internal static glStringMarkerGREMEDYFunc glStringMarkerGREMEDYPtr;
        internal static void loadStringMarkerGREMEDY()
        {
            try
            {
                glStringMarkerGREMEDYPtr = (glStringMarkerGREMEDYFunc)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress("glStringMarkerGREMEDY"), typeof(glStringMarkerGREMEDYFunc));
            }
            catch
            {
                Console.WriteLine("Failed to get function pointer for 'glStringMarkerGREMEDY'.");
            }
        }
        public static void glStringMarkerGREMEDY(GLsizei @len, const void * @string) => glStringMarkerGREMEDYPtr(@len, @string);
        #endregion
    }
}
