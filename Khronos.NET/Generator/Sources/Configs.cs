﻿using System;
using System.IO;
using System.Net;

namespace Generator
{
    public static class Configs
    {
        /// <summary>
        /// The main workspace that holds the project.
        /// </summary>
        public const string glWorkspaceName = "Khronos.NET";

        /// <summary>
        /// The project where files will be placed.
        /// </summary>
        public const string glTargetProjectName = "OpenGL.NET";

        /// <summary>
        /// The root directory.
        /// </summary>
        public static DirectoryInfo directoryRoot;

        /// <summary>
        /// The gl target path.
        /// </summary>
        public static string glTargetPath;

        /// <summary>
        /// The project to generate code in.
        /// </summary>
        public static string glTargetSourcesPath;

        /// <summary>
        /// The path to where to download / get OpenGL specs.
        /// </summary>
        public static string glTargetSpecs;

        /// <summary>
        /// The path to where to download / get OpenGL docs.
        /// </summary>
        public static string glTargetDocs;

        /// <summary>
        /// Initialise this instance.
        /// </summary>
        public static void Initialise()
        {
            directoryRoot = new DirectoryInfo(Directory.GetCurrentDirectory());

            // Walk up to the solution file so we can then go into the project and write to the C# files directly.
            while (directoryRoot != null && !File.Exists(Path.Combine(directoryRoot.FullName, glWorkspaceName + ".sln")))
            {
                directoryRoot = directoryRoot.Parent;
            }

            // If the solution wasn't found (maybe its not not running from the bin directory), just write out to the current directory.
            if (directoryRoot == null)
            {
                directoryRoot = new DirectoryInfo(Directory.GetCurrentDirectory());
            }

            // Sets the specified gl paths.
            glTargetPath = Path.Combine(directoryRoot.FullName, glTargetProjectName);
            glTargetSourcesPath = glTargetPath + "/" + "Sources";
            glTargetSpecs = glTargetPath + "/" + "GLSpecs";
            glTargetDocs = glTargetPath + "/" + "GLDocs";

            // Downloads the required files.
            DownloadGLSpecs();
            DownloadGLDocs();
        }

        private static void DownloadGLSpecs()
        {
            // The URLS for the khronos specs.
            string[] urls = new[]
                {
                    "https://cvs.khronos.org/svn/repos/ogl/trunk/doc/registry/public/api/egl.xml",
                    "https://cvs.khronos.org/svn/repos/ogl/trunk/doc/registry/public/api/gl.xml",
                    "https://cvs.khronos.org/svn/repos/ogl/trunk/doc/registry/public/api/glx.xml",
                    "https://cvs.khronos.org/svn/repos/ogl/trunk/doc/registry/public/api/wgl.xml",
                    "https://cvs.khronos.org/svn/repos/ogl/trunk/doc/registry/public/api/readme.pdf",
                };

            // Creates the specs path directory if it does not exist!
            if (!Directory.Exists(glTargetSpecs))
            {
                Directory.CreateDirectory(glTargetSpecs);
            }

            // Downloads the specs files.
            foreach (string url in urls)
            {
                string[] spilit = url.Split('/');
                string p = "";

                if (!File.Exists((p = Path.Combine(glTargetSpecs, spilit[spilit.Length - 1]))))
                {
                    Console.WriteLine("Downloading the required {0} document!", spilit[spilit.Length - 1]);
                    WebClient glClient = new WebClient();
                    glClient.DownloadFile(url, p);
                }
            }
        }

        public static void DownloadGLDocs()
        {
            // The URLS for the khronos docs.
            string[] urls = new string[]
                { 
                    "https://cvs.khronos.org/svn/repos/ogl/trunk/ecosystem/public/sdk/docs/man2/", 
                    "https://cvs.khronos.org/svn/repos/ogl/trunk/ecosystem/public/sdk/docs/man3/", 
                    "https://cvs.khronos.org/svn/repos/ogl/trunk/ecosystem/public/sdk/docs/man4/",
                };

            // Creates the docs path directory if it does not exist!
            if (!Directory.Exists(glTargetDocs))
            {
                Directory.CreateDirectory(glTargetDocs);
            }

            // Downlaods the docs files.
            foreach (string rootUrl in urls)
            {
                Console.WriteLine("Looking for xml doc's in: " + rootUrl);

                WebRequest rootRequest = WebRequest.Create(rootUrl);
                WebResponse rootResponse = rootRequest.GetResponse();

                using (StreamReader sr = new StreamReader(rootResponse.GetResponseStream()))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        line = line.Trim();

                        if (line.StartsWith("<file name=\""))
                        {
                            line = line.Substring(12);
                            line = line.Substring(0, line.IndexOf('"'));

                            if (line.EndsWith(".xml")) //  && line.StartsWith("gl")
                            {
                                string filePath = glTargetDocs + "/" + line;

                                if (!File.Exists(filePath))
                                {
                                    Console.WriteLine("Downloading: " + filePath);

                                    WebRequest request = WebRequest.Create(rootUrl + "/" + line);
                                    WebResponse response = request.GetResponse();

                                    using (StreamReader sr2 = new StreamReader(response.GetResponseStream()))
                                    {
                                        File.WriteAllText(filePath, sr2.ReadToEnd());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

