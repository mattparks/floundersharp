﻿using System;
using System.Xml.Linq;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Generator
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            // Sets up the console.
            Console.Title = "OpenGL Generator";
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;

            // Sets up the configs class.
            Configs.Initialise();

            // Generate files! TODO: OpenAL & OpenCL
            Generate("glfw", "");
            Generate("gl", "");
            //Generate("wgl", "");
            //Generate("glx", "");
            //Generate("egl", "");

            // Wait for the user to end the program.
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        private static void Generate(string xmlName, string dllPath)
        {
            #if DEBUG
            Stopwatch sw = new Stopwatch();
            sw.Start();
            #endif

            var rootPath = "";
            var source = XDocument.Load(Configs.glTargetSpecs + "/" + xmlName + ".xml");        
            var sourceTypes = XDocument.Load(Configs.glTargetSpecs + "/type.xml");
            Directory.CreateDirectory(rootPath = Configs.glTargetSourcesPath + "/Graphics/");

            var enumerationObject = new Enumeration(source);
            var commandObject = new Command(source);
            var featureObject = new Feature(source);
            var extensionObject = new Extension(source);        
            var types = new VariableTypes(sourceTypes);

            foreach (GLFeature feature in featureObject.features)
            {
                using (StreamWriter writer = new StreamWriter(rootPath + "/" + feature.name + ".cs"))
                {
                    CodeWriter code = new CodeWriter(writer);
                    string className = "";
                    GenerateHeader(code, className = Regex.Replace(feature.api, @"\d", "").ToUpper() + feature.number.ToString().Replace(".", ""));
                    WriteInterop(code, className, commandObject, feature.commands);
                    WriteEnums(code, types, enumerationObject, feature.enums);
                    WriteCommands(code, types, commandObject, feature.commands);
                    CloseFile(code);
                }
            }

            foreach (GLExtension extension in extensionObject.extensions)
            {
                if (!string.IsNullOrWhiteSpace(extension.name))
                {
                    string str2;
                    Directory.CreateDirectory(str2 = rootPath + Extension.GetVendor(extension.name) + "/");
                    using (StreamWriter writer3 = new StreamWriter(str2 + extension.name + ".cs"))
                    {
                        CodeWriter writer4 = new CodeWriter(writer3);
                        string str3 = "";
                        GenerateHeader(writer4, str3 = extension.name);
                        WriteInterop(writer4, str3, commandObject, extension.commands);
                        WriteEnums(writer4, types, enumerationObject, extension.enums);
                        WriteCommands(writer4, types, commandObject, extension.commands);
                        CloseFile(writer4);
                    }
                }
            }
            CreateInitFile(Configs.glTargetSourcesPath + "/Graphics/", "OpenGLInit");


            #if DEBUG
            sw.Stop();
            Console.WriteLine("Creating C# files for {0} took {1} milliseconds.", xmlName.ToUpper(), sw.ElapsedMilliseconds);
            #endif
        }

        private static void WriteInterop(CodeWriter code, string className, Command commands, List<string> requiredCommands)
        {
            code.WriteLine("#region Interop");
            code.WriteLine("static {0}()", className);
            code.WriteOpenBraceAndIndent();
            code.WriteLine("Console.WriteLine(\"Initalising {0} interop methods!\");", className);
            code.WriteLine();
            code.WriteLine("if (OpenGLInit.GetProcAddress == null)");
            code.WriteOpenBraceAndIndent();
            code.WriteLine("throw new ArgumentException(\"Value OpenGLInit.GetProcAddress cannot be null, call 'OpenGLInit.Init(Func<string, IntPtr> @procAddress)' before using OpenGl!\", \"OpenGLInit.GetProcAddress\");");
            code.WriteCloseBraceAndDedent();
            code.WriteLine();

            foreach (var required in requiredCommands)
            {
                var command = FindCommand(commands, required);

                if (command != null)
                {
                    code.WriteLine("load{0}();", command.name.Replace("gl", ""));
                }
            }

            code.WriteCloseBraceAndDedent();
            code.WriteLine("#endregion");
            code.WriteLine();
        }

        private static void WriteEnums(CodeWriter code, VariableTypes types, Enumeration enumeration, List<string> requiredEnums)
        {
            code.WriteLine("#region Enums");

            foreach (string str in requiredEnums.Distinct().ToList())
            {
                var enum2 = FindEnumeration(enumeration, str);

                if (enum2 != null)
                {
                    code.WriteLine("public static {0} {1} = {2};", types.GetEnumName(enum2.value), enum2.name, enum2.value);
                }
            }

            code.WriteLine("#endregion");
            code.WriteLine();
        }

        private static void WriteCommands(CodeWriter code, VariableTypes types, Command commandGroups, List<string> requiredCommands)
        {
            code.WriteLine("#region Commands");
            int count = requiredCommands.Count;

            foreach (var str in requiredCommands.Distinct<string>().ToList<string>())
            {
                var command = FindCommand(commandGroups, str);

                if (command != null)
                {
                    string paramCode = "";
                    string argsInCall = "";
                    bool firstParam = true;

                    foreach (var param in command.parameters)
                    {
                        if (!firstParam)
                        {
                            paramCode = paramCode + ", ";
                            argsInCall = argsInCall + ", ";
                        }

                        if (firstParam)
                        {
                            firstParam = false;
                        }

                        var cSName = types.GetCSName(param.type);
                        paramCode = paramCode + cSName + " " + param.name.Trim();
                        argsInCall = argsInCall + (cSName.Contains("ref") ? "ref " : "") + param.name.Trim();
                    }

                    code.WriteLine("internal delegate {0} {1}Func({2});", types.GetCSName(command.returnType), command.name, paramCode);
                    code.WriteLine("internal static {0}Func {0}Ptr;", command.name);
                    code.WriteLine("internal static void load{0}()", command.name.Replace("gl", ""));
                    code.WriteOpenBraceAndIndent();
                    code.WriteLine("try");
                    code.WriteOpenBraceAndIndent();
                    code.WriteLine(string.Format("{0}Ptr = ({0}Func)Marshal.GetDelegateForFunctionPointer(OpenGLInit.GetProcAddress(\"{0}\"), typeof({0}Func));", command.name));
                    code.WriteCloseBraceAndDedent();
                    code.WriteLine("catch");
                    code.WriteOpenBraceAndIndent();
                    code.WriteLine("Console.WriteLine(\"Failed to get function pointer for '{0}'.\");", command.name);
                    code.WriteCloseBraceAndDedent();
                    code.WriteCloseBraceAndDedent();
                    code.WriteLine("public static {0} {1}({2}) => {1}Ptr({3});", types.GetCSName(command.returnType), command.name, paramCode, argsInCall);

                    if (count > 1)
                    {
                        code.WriteLine();
                    }

                    count--;
                }
            }

            code.WriteLine("#endregion");
        }

        private static void CreateInitFile(string path, string initClassName)
        {
            using (StreamWriter writer = new StreamWriter(path + "/" + initClassName + ".cs"))
            {
                var code = new CodeWriter(writer);
                GenerateHeader(code, initClassName);
                code.WriteLine("internal static Func<string, IntPtr> GetProcAddress;");
                code.WriteLine();
                code.WriteLine("public static void Init(Func<string, IntPtr> @procAddress)");
                code.WriteOpenBraceAndIndent();
                code.WriteLine("GetProcAddress = @procAddress;");
                code.WriteCloseBraceAndDedent();
                CloseFile(code);
            }
        }

        private static void GenerateHeader(CodeWriter code, string className)
        {
            Console.WriteLine("Generating class: {0}", className);
            code.WriteLine("using System;");
            code.WriteLine("using System.Diagnostics;");
            code.WriteLine("using System.Reflection;");
            code.WriteLine("using System.Runtime.InteropServices;");
            code.WriteLine();
            code.WriteLine("namespace OpenGL");
            code.WriteOpenBraceAndIndent();
            code.WriteLine("public static class {0}", className);
            code.WriteOpenBraceAndIndent();
        }

        private static void CloseFile(CodeWriter code)
        {
            code.WriteCloseBraceAndDedent();
            code.WriteCloseBraceAndDedent();
        }

        private static GLCommand FindCommand(Command commands, string name)
        {
            foreach (var command in commands.commands)
            {
                if (command.name.Trim() == name.Trim())
                {
                    return command;
                }
            }

            return null;
        }

        private static GLEnum FindEnumeration(Enumeration enumeration, string name)
        {
            foreach (var enums in enumeration.enums)
            {
                foreach (var enum2 in enums.enums)
                {
                    if (enum2.name.Trim() == name.Trim())
                    {
                        return enum2;
                    }
                }
            }

            return null;
        }

        private static GLFeature FindFeature(Feature features, string name)
        {
            foreach (var feature in features.features)
            {
                if (feature.name.Trim() == name.Trim())
                {
                    return feature;
                }
            }

            return null;
        }
    }
}
