﻿using System;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Xml;
using System.Linq;

namespace Generator
{
    public class Command
    {
        public List<GLCommand> commands { get; private set; }

        public Command(XDocument source)
        {
            commands = new List<GLCommand>();

            foreach (var commandsElem in source.Root.Elements("commands"))
            {
                foreach (var commandElem in commandsElem.Elements("command"))
                {
                    commands.Add(new GLCommand(commandElem));
                }
            }
        }
    }

    public class GLCommand
    {    
        public string name { get; private set; }
        public string alias { get; private set; }
        public string vecEquiv { get; private set; }
        public string returnType { get; private set; }
        public List<GLParameter> parameters { get; private set; }
        public GLX glx { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Generator.GLCommand"/> class.
        ///
        /// <commands> defines a group of commands.
        ///   namespace - identifies a function namespace.
        ///
        /// <command> defines a single command.
        ///   <proto> is the C function prototype, including the return type.
        ///   <param> are function parameters, in order.
        ///     <ptype> is a <type> name, if present.
        ///     <name> is the function / parameter name.
        /// The textual contents of <proto> and <param> should be legal C for those parts of a function declaration.
        ///   <alias> - denotes function aliasing.
        ///     name - name of aliased function.
        ///   <vecequiv> - denotes scalar / vector function equivalence.
        ///     name - name of corresponding vector form, e.g. (glColor3f -> glColor3fv).
        ///   <glx> - information about GLX protocol.
        ///     type - "render", "single", or "vendor" for GLXRender, GLXSingle, GLXVendorPrivate{WithReply}.
        ///     opcode - numeric opcode of specified type for this function.
        ///     name - if present, protocol name (defaults to command name).
        ///     comment - unused.
        /// </summary>
        /// <param name="element">Element.</param>
        public GLCommand(XElement element)
        {        
            var nameAttribute = element.Element("proto").Element("name");
            var aliasAttribute = element.Element("alias");
            var vecequivAttribute = element.Element("vecequiv");
            var returnAttribute = element.Element("proto").Nodes()
                .Where(node => node.NodeType == XmlNodeType.Text || (node as XElement)?.Name != "name")
                .Select(node => node is XText ? ((XText)node).Value : (node as XElement)?.Value)
                .Select(t => t.Trim())
                .Aggregate("", (a, b) => a + b);
            var glxAttribute = element.Element("glx");

            if (nameAttribute != null)
            {
                name = nameAttribute.Value;
            }

            if (aliasAttribute != null)
            {
                alias = aliasAttribute.Value;
            }

            if (vecequivAttribute != null)
            {
                vecEquiv = vecequivAttribute.Value;
            }

            if (returnAttribute != null)
            {
                returnType = returnAttribute;
            }

            parameters = new List<GLParameter>();

            foreach (var paramElem in element.Elements("param"))
            {
                parameters.Add(new GLParameter(paramElem));
            }

            if (glxAttribute != null)
            {
                glx = new GLX(glxAttribute);
            }
        }
    }

    public class GLParameter
    {    
        public string group { get; private set; }
        public string len { get; private set; }
        public string name { get; private set; }
        public string type { get; private set; }

        public GLParameter(XElement element)
        {
            var groupAttribute = element.Attribute("group");
            var lenAttribute = element.Attribute("len");
            var nameAttribute = element.Element("name");
            var typeAttribute = element.Nodes()
                .Where(node => node.NodeType == XmlNodeType.Text || (node as XElement)?.Name != "name")
                .Select(node => node is XText ? ((XText)node).Value : (node as XElement)?.Value)
                .Aggregate("", (a, b) => a + b);

            if (groupAttribute != null)
            {
                group = groupAttribute.Value;
            }

            if (lenAttribute != null)
            {
                len = lenAttribute.Value;
            }

            if (nameAttribute != null)
            {
                name = "@" + nameAttribute.Value;
            }

            if (typeAttribute != null)
            {
                type = typeAttribute;
            }
        }
    }

    public class GLX
    {
        public string type { get; private set; }
        public string opcode { get; private set; }
        public string name { get; private set; }
        public string comment { get; private set; }

        public GLX(XElement element)
        {
            var typeAttribute = element.Attribute("type");
            var opcodeAttribute = element.Attribute("opcode");
            var nameAttribute = element.Attribute("name");
            var commentAttribute = element.Attribute("comment");

            if (typeAttribute != null)
            {
                type = typeAttribute.Value;
            }

            if (opcodeAttribute != null)
            {
                opcode = opcodeAttribute.Value;
            }

            if (nameAttribute != null)
            {
                name = nameAttribute.Value;
            }

            if (commentAttribute != null)
            {
                comment = commentAttribute.Value;
            }
        }

        public override string ToString()
        {
            return string.Format("[GLX: type={0}, opcode={1}, name={2}, comment={3}]", type, opcode, name, comment);
        }
    }
}

