﻿using System;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Generator
{
    public class VariableTypes
    {
        public List<GLType> types;

        public VariableTypes(XDocument source)
        {
            types = new List<GLType>();

            /*foreach (var current in source.Elements("typemap"))
            {
                foreach (var current2 in current.Elements("type"))
                {
                    var name = current2.Attribute("name");
                    var value = current2.Attribute("value");

                    if (name != null && value != null)
                    {
                        types.Add(new GLType(name.Value, value.Value));
                    }
                }
            }*/
        }

        public string GetCSName(string glName)
        {
            string result = glName;

        //    foreach (var current in this.types)
        //    {
        //        if (current.glName.Equals(glName))
        //        {
        //            result = current.csName;
        //        }
        //    }

            return result;
        }

        public string GetEnumName(string value)
        {
            var parseStyle = NumberStyles.Any;

            if (value.StartsWith("0x"))
            {
                value = value.Substring(2);
                parseStyle = NumberStyles.HexNumber;
            }

            UInt32 uintValue;

            if (UInt32.TryParse(value, parseStyle, CultureInfo.InvariantCulture, out uintValue))
            {
                return "UInt32";
            }

            UInt64 ulongValue;

            if (UInt64.TryParse(value, parseStyle, CultureInfo.InvariantCulture, out ulongValue))
            {
                return "UInt64";
            }

            Int32 intValue;

            if (Int32.TryParse(value, parseStyle, CultureInfo.InvariantCulture, out intValue))
            {
                return "Int32";
            }

            throw new InvalidOperationException(string.Format("Unhandled enum value type for value \"{0}\"!", value));
        }
    }

    public class GLType
    {
        public string glName;
        public string csName;

        public GLType(string glName, string csName)
        {
            this.glName = glName;
            this.csName = csName;
        }
    }
}

