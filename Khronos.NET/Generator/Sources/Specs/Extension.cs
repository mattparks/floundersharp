﻿using System;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Generator
{
    public class Extension
    {
        public List<GLExtension> extensions { get; private set; }

        public Extension(XDocument source)
        {
            extensions = new List<GLExtension>();

            foreach (var extensionsElem in source.Root.Elements("extensions"))
            {
                foreach (var extensionElem in extensionsElem.Elements("extension"))
                {
                    extensions.Add(new GLExtension(extensionElem));
                }
            }
        }

        public static string GetVendor(string extensionName)
        {
            var match = Regex.Match(extensionName, "^(GL|WGL|GLX|GLU|EGL)_(?<Vendor>[^_]+).*");

            if (!match.Success)
            {
                throw new NotSupportedException();
            }

            return match.Groups["Vendor"].Value;
        }
    }

    public class GLExtension
    {
        public string name { get; private set; }
        public string supported { get; private set; }
        public List<string> enums { get; private set; }
        public List<string> commands { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Generator.GLExtension"/> class.
        /// 
        /// <extension> defines the interface of an API <extension>. Like a "<feature>" tag, but with a slightly different api attribute.
        ///   api - regexp pattern matching one or more API tags, indicating
        ///     which APIs the extension is known to work with. The only
        ///     syntax supported is <name>{|<name>}* and each name must
        ///     exactly match an API being generated (implicit ^$ surrounding).
        ///     In addition, <require> / <remove> tags also support an api attribute:
        ///     api - only require/remove these features for the matching API. Not a regular expression.
        /// </summary>
        /// <param name="element">Element.</param>
        public GLExtension(XElement element)
        {
            var nameAttribute = element.Attribute("name");
            var supportedAttribute = element.Attribute("supported");

            if (nameAttribute != null)
            {
                name = nameAttribute.Value;
            }

            if (supportedAttribute != null)
            {
                supported = supportedAttribute.Value;
            }

            enums = new List<string>();

            foreach (var enumElem in element.Elements("require").Elements("enum"))
            {
                var enumNameAttribute = enumElem.Attribute("name");

                if (enumNameAttribute != null)
                {
                    enums.Add(enumNameAttribute.Value);
                }
            }

            commands = new List<string>();

            foreach (var commandElem in element.Elements("require").Elements("command"))
            {
                var commandNameAttribute = commandElem.Attribute("name");

                if (commandNameAttribute != null)
                {
                    commands.Add(commandNameAttribute.Value);
                }
            }
        }

        public override string ToString()
        {
            return string.Format("[GLExtension: name={0}, supported={1}, enums={2}, commands={3}]", name, supported, enums, commands);
        }
    }
}

