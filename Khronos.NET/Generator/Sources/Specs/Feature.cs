﻿using System;
using System.Xml.Linq;
using System.Collections.Generic;

namespace Generator
{
    public class Feature
    {
        public List<GLFeature> features { get; private set; }

        public Feature(XDocument source)
        {
            features = new List<GLFeature>();

            foreach (var featureElem in source.Root.Elements("feature"))
            {
                features.Add(new GLFeature(featureElem));
            }
        }
    }

    public class GLFeature
    {
        public string api { get; private set; }
        public string name { get; private set; }
        public Version number { get; private set; }
        public Dictionary<string, string> types { get; private set; }
        public List<string> enums { get; private set; }
        public List<string> commands { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Generator.GLFeature"/> class.
        /// 
        /// <feature> defines the interface of an API version (e.g. OpenGL 1.2) api - API tag (e.g. 'gl', 'gles2', etc. - used internally, not neccessarily an actual API name.
        ///   name - version name (C preprocessor name, e.g. GL_VERSION_4_2).
        ///   number - version number, e.g. 4.2.
        ///   protect - additional #ifdef symbol to place around the feature.
        ///   <require> / <remove> contains features to require or remove in this version.
        ///     profile - only require/remove when generated profile matches.
        ///     comment - unused.
        /// </summary>
        /// <param name="element">Element.</param>
        public GLFeature(XElement element)
        {
            var apiAttribute = element.Attribute("api");
            var nameAttribute = element.Attribute("name");
            var versionAttribute = element.Attribute("number");

            if (apiAttribute != null)
            {
                api = apiAttribute.Value;
            }

            if (nameAttribute != null)
            {
                name = nameAttribute.Value;
            }
           
            if (versionAttribute != null)
            {
                number = Version.Parse(versionAttribute.Value);
            }

            types = new Dictionary<string, string>();

            foreach (var typeElem in element.Elements("require").Elements("type"))
            {
                var typeNameElement = typeElem.Attribute("name");
                var typeCommentElement = typeElem.Attribute("comment");

                if (typeNameElement != null)
                {
                    string text = "";

                    if (typeCommentElement != null)
                    {
                        text = typeCommentElement.Value;

                        if (text[text.Length - 1] != '.')
                        {
                            text += ".";
                        }
                    }

                    types.Add(typeNameElement.Value, text);
                }
            }

            enums = new List<string>();

            foreach (var enumElem in element.Elements("require").Elements("enum"))
            {
                var enumNameAttribute = enumElem.Attribute("name");

                if (enumNameAttribute != null)
                {
                    enums.Add(enumNameAttribute.Value);
                }
            }

            commands = new List<string>();

            foreach (var commandElem in element.Elements("require").Elements("command"))
            {
                var commandNameAttribute = commandElem.Attribute("name");

                if (commandNameAttribute != null)
                {
                    commands.Add(commandNameAttribute.Value);
                }
            }
        }

        public override string ToString()
        {
            return string.Format("[GLFeature: api={0}, name={1}, number={2}, types={3}, enums={4}, commands={5}]", api, name, number, types, enums, commands);
        }
    }
}

