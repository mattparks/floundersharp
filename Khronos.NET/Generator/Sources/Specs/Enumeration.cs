﻿using System;
using System.Xml.Linq;
using System.Collections.Generic;

namespace Generator
{
    public class Enumeration
    {
        public List<GLGroup> groups { get; private set; }
        public List<GLEnums> enums { get; private set; }

        public Enumeration(XDocument source)
        {
            groups = new List<GLGroup>();
            enums = new List<GLEnums>();

            foreach (var groupsElem in source.Root.Elements("groups"))
            {
                foreach (var groupElem in source.Root.Elements("group"))
                {
                    groups.Add(new GLGroup(groupElem));
                }
            }

            foreach (var enumsElem in source.Root.Elements("enums"))
            {
                enums.Add(new GLEnums(enumsElem));
            }
        }
    }

    public class GLGroup
    {
        public string name { get; private set; }
        public List<string> enums { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Generator.GLGroup"/> class.
        /// 
        /// <groups> defines a group of enum groups.
        ///
        /// <group> defines a single enum group. Enums may be in multiple groups.
        ///   name - group name.
        ///   comment - unused.
        ///   <enum name=""> - members of the group.
        /// </summary>
        /// <param name="element">Element.</param>
        public GLGroup(XElement element)
        {
            var nameElement = element.Attribute("name");

            if (nameElement != null)
            {
                name = nameElement.Value;
            }

            enums = new List<string>();

            foreach (var enumElem in element.Elements("enum"))
            {
                var enumNameElement = enumElem.Attribute("name");

                if (enumNameElement != null)
                {
                    enums.Add(enumNameElement.Value);
                }
            }
        }
    }

    public class GLEnums
    {
        public string space { get; private set; }
        public string group { get; private set; }
        public string type { get; private set; }
        public string vendor { get; private set; }
        public string comment { get; private set; }
        public List<GLEnum> enums { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Generator.GLEnum"/> class.
        /// 
        /// <enums> defines a group of enumerants.
        ///   namespace - identifies a numeric namespace.
        ///   group - identifies a functional subset of the namespace - same as <group name=""> start, end - beginning and end of a numeric range in the namespace.
        ///   vendor - owner of the numeric range.
        ///   type - "bitmask", if present.
        ///   comment - unused.
        /// </summary>
        /// <param name="element">Element.</param>
        public GLEnums(XElement element)
        {
            var spaceElement = element.Attribute("namespace");
            var groupElement = element.Attribute("group");
            var typeElement = element.Attribute("type");
            var vendorElement = element.Attribute("vendor");
            var commentElement = element.Attribute("comment");

            if (spaceElement != null)
            {
                space = spaceElement.Value;
            }

            if (groupElement != null)
            {
                group = groupElement.Value;
            }

            if (typeElement != null)
            {
                type =  typeElement.Value;
            }

            if (vendorElement != null)
            {
                vendor = vendorElement.Value;
            }

            if (commentElement != null)
            {
                string commentValue = commentElement.Value;

                if (commentValue[commentValue.Length - 1] != '.')
                {
                    commentValue += ".";
                }

                comment = commentValue;
            }

            enums = new List<GLEnum>();

            foreach (var enumElem in element.Elements("enum"))
            {
                enums.Add(new GLEnum(enumElem));
            }
        }
    }

    public class GLEnum
    {
        public string value { get; private set; }
        public string api { get; private set; }
        public string name { get; private set; }
        public string type { get; private set; }
        public string alias { get; private set; }
        public string comment { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Generator.GLEnum"/> class.
        /// 
        /// <enum> defines a single enumerant.
        ///   value - integer (including hex) value of the enumerant.
        ///   api - matches a <feature> api attribute, if present.
        ///   type - "u" (unsigned), "ull" (uint64), or integer if not present.
        ///   name - enumerant name.
        ///   alias - another enumerant this is semantically identical to.
        ///   comment - unused.
        /// </summary>
        /// </summary>
        /// <param name="element">Element.</param>
        public GLEnum(XElement element)
        {
            var valueElement = element.Attribute("value");
            var apiElement = element.Attribute("api");
            var nameElement = element.Attribute("name");
            var typeElement = element.Attribute("type");
            var aliasElement = element.Attribute("alias");
            var commentElement = element.Attribute("comment");

            if (valueElement != null)
            {
                value = valueElement.Value;
            }

            if (apiElement != null)
            {
                api = apiElement.Value;
            }

            if (nameElement != null)
            {
                name = nameElement.Value;
            }

            if (typeElement != null)
            {
                type = typeElement.Value;
            }

            if (aliasElement != null)
            {
                alias = aliasElement.Value;
            }

            if (commentElement != null)
            {
                string commentValue = commentElement.Value;

                if (commentValue[commentValue.Length - 1] != '.')
                {
                    commentValue += ".";
                }

                comment = commentValue.Replace(":", "");
            }
        }
    }
}

