﻿using System;
using System.Runtime.InteropServices;

namespace OpenGL.GLFW
{
    [StructLayout(LayoutKind.Explicit, Size = sizeof(int) * 6)]
    public struct GLFWVidMode
    {
        [FieldOffset(sizeof(int) * 0)]
        public int Width;
        [FieldOffset(sizeof(int) * 1)]
        public int Height;
        [FieldOffset(sizeof(int) * 2)]
        public int RedBits;
        [FieldOffset(sizeof(int) * 3)]
        public int BlueBits;
        [FieldOffset(sizeof(int) * 4)]
        public int GreenBits;
        [FieldOffset(sizeof(int) * 5)]
        public int RefreshRate;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct GLFWGammaRampInternal
    {
        public uint* Red;
        public uint* Green;
        public uint* Blue;
        public uint Length;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct GLFWGammaRamp
    {
        [MarshalAs(UnmanagedType.LPArray)]
        public uint[] Red;
        [MarshalAs(UnmanagedType.LPArray)]
        public uint[] Green;
        [MarshalAs(UnmanagedType.LPArray)]
        public uint[] Blue;
        internal uint Length;
    }

    #pragma warning disable 0414

    [StructLayout(LayoutKind.Explicit)]
    public struct GLFWMonitorPtr
    {
        private GLFWMonitorPtr(IntPtr ptr)
        {
            inner_ptr = ptr;
        }

        [FieldOffsetAttribute(0)]
        private IntPtr inner_ptr;

        public readonly static GLFWMonitorPtr Null = new GLFWMonitorPtr(IntPtr.Zero);
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct GLFWWindowPtr
    {
        private GLFWWindowPtr(IntPtr ptr)
        {
            inner_ptr = ptr;
        }

        [FieldOffsetAttribute(0)]
        private IntPtr inner_ptr;

        public readonly static GLFWWindowPtr Null = new GLFWWindowPtr(IntPtr.Zero);
    }

    #pragma warning restore 0414
}

