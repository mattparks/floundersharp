﻿using System;
using System.Runtime.InteropServices;

namespace OpenGL.GLFW
{
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWErrorFun(GLFWError code, [MarshalAs(UnmanagedType.LPStr)] string desc);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWMonitorFun(GLFWMonitorPtr mtor, ConnectionState @enum);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWWindowCloseFun(GLFWWindowPtr wnd);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWWindowPosFun(GLFWWindowPtr wnd, int x, int y);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWWindowRefreshFun(GLFWWindowPtr wnd);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWWindowSizeFun(GLFWWindowPtr wnd, int width, int height);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWWindowFocusFun(GLFWWindowPtr wnd, bool focus);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWKeyFun(GLFWWindowPtr wnd,KeyCode key, int scanCode, KeyAction action, KeyModifiers mods);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWCharFun(GLFWWindowPtr wnd, char ch);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWMouseButtonFun(GLFWWindowPtr wnd, MouseButton btn, KeyAction action);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWWindowIconifyFun(GLFWWindowPtr wnd, bool iconify);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWCursorPosFun(GLFWWindowPtr wnd, double x, double y);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWCursorEnterFun(GLFWWindowPtr wnd, bool enter);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWScrollFun(GLFWWindowPtr wnd, double xoffset, double yoffset);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GLFWFramebufferSizeFun(GLFWWindowPtr wnd, int width, int height);
}

