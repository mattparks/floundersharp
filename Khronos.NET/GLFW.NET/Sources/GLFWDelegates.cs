﻿using System;
using System.Security;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace OpenGL.GLFW
{
    internal static unsafe class GLFWDelegates
    {
        static GLFWDelegates()
        {
            #if DEBUG
            Stopwatch sw = new Stopwatch();
            sw.Start();
            #endif

            Type glfwInterop = (IntPtr.Size == 8) ? typeof(GLFW64) : typeof(GLFW32);

            #if DEBUG
            Console.WriteLine("GLFW interop: {0}", glfwInterop.Name);
            #endif

            FieldInfo[] fields = typeof(GLFWDelegates).GetFields(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);

            foreach (FieldInfo fi in fields)
            {
                MethodInfo mi = glfwInterop.GetMethod(fi.Name, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
                Delegate function = Delegate.CreateDelegate(fi.FieldType, mi);
                fi.SetValue(null, function);
            }

            #if DEBUG
            sw.Stop();
            Console.WriteLine("Copying GLFW delegates took {0} milliseconds.", sw.ElapsedMilliseconds);
            #endif
        }

        #pragma warning disable 0649

        internal delegate int Init();
        internal static Init glfwInit;

        internal delegate void Terminate();
        internal static Terminate glfwTerminate;

        internal delegate void GetVersion(out int major, out int minor, out int rev);
        internal static GetVersion glfwGetVersion;

        internal delegate sbyte* GetVersionString();
        internal static GetVersionString glfwGetVersionString;

        internal delegate GLFWErrorFun SetErrorCallback(GLFWErrorFun cbfun);
        internal static SetErrorCallback glfwSetErrorCallback;

        internal delegate GLFWMonitorPtr* GetMonitors(out int count);
        internal static GetMonitors glfwGetMonitors;

        internal delegate GLFWMonitorPtr GetPrimaryMonitor();
        internal static GetPrimaryMonitor glfwGetPrimaryMonitor;

        internal delegate void GetMonitorPos(GLFWMonitorPtr monitor, out int xpos, out int ypos);
        internal static GetMonitorPos glfwGetMonitorPos;

        internal delegate void GetMonitorPhysicalSize(GLFWMonitorPtr monitor, out int width, out int height);
        internal static GetMonitorPhysicalSize glfwGetMonitorPhysicalSize;

        internal delegate sbyte* GetMonitorName(GLFWMonitorPtr monitor);
        internal static GetMonitorName glfwGetMonitorName;

        internal delegate GLFWVidMode* GetVideoModes(GLFWMonitorPtr monitor, out int count);
        internal static GetVideoModes glfwGetVideoModes;

        internal delegate GLFWVidMode* GetVideoMode(GLFWMonitorPtr monitor);
        internal static GetVideoMode glfwGetVideoMode;

        internal delegate void SetGamma(GLFWMonitorPtr monitor, float gamma);
        internal static SetGamma glfwSetGamma;

        internal delegate void GetGammaRamp(GLFWMonitorPtr monitor, out GLFWGammaRampInternal ramp);
        internal static GetGammaRamp glfwGetGammaRamp;

        internal delegate void SetGammaRamp(GLFWMonitorPtr monitor, ref GLFWGammaRamp ramp);
        internal static SetGammaRamp glfwSetGammaRamp;

        internal delegate void DefaultWindowHints();
        internal static DefaultWindowHints glfwDefaultWindowHints;

        internal delegate void WindowHint(OpenGL.GLFW.WindowHint target, int hint);
        internal static WindowHint glfwWindowHint;

        internal delegate GLFWWindowPtr CreateWindow(int width, int height, [MarshalAs(UnmanagedType.LPStr)] string title, GLFWMonitorPtr monitor, GLFWWindowPtr share);
        internal static CreateWindow glfwCreateWindow;

        internal delegate void DestroyWindow(GLFWWindowPtr window);
        internal static DestroyWindow glfwDestroyWindow;

        internal delegate int WindowShouldClose(GLFWWindowPtr window);
        internal static WindowShouldClose glfwWindowShouldClose;

        internal delegate void SetWindowShouldClose(GLFWWindowPtr window,int value);
        internal static SetWindowShouldClose glfwSetWindowShouldClose;

        internal delegate void SetWindowTitle(GLFWWindowPtr window, [MarshalAs(UnmanagedType.LPStr)] string title);
        internal static SetWindowTitle glfwSetWindowTitle;

        internal delegate void GetWindowPos(GLFWWindowPtr window, out int xpos, out int ypos);
        internal static GetWindowPos glfwGetWindowPos;

        internal delegate void SetWindowPos(GLFWWindowPtr window, int xpos, int ypos);
        internal static SetWindowPos glfwSetWindowPos;

        internal delegate void GetWindowSize(GLFWWindowPtr window, out int width, out int height);
        internal static GetWindowSize glfwGetWindowSize;

        internal delegate void SetWindowSize(GLFWWindowPtr window, int width, int height);
        internal static SetWindowSize glfwSetWindowSize;

        internal delegate void IconifyWindow(GLFWWindowPtr window);
        internal static IconifyWindow glfwIconifyWindow;

        internal delegate void RestoreWindow(GLFWWindowPtr window);
        internal static RestoreWindow glfwRestoreWindow;

        internal delegate void ShowWindow(GLFWWindowPtr window);
        internal static ShowWindow glfwShowWindow;

        internal delegate void HideWindow(GLFWWindowPtr window);
        internal static HideWindow glfwHideWindow;

        internal delegate GLFWMonitorPtr GetWindowMonitor(GLFWWindowPtr window);
        internal static GetWindowMonitor glfwGetWindowMonitor;

        internal delegate int GetWindowAttrib(GLFWWindowPtr window,int param);
        internal static GetWindowAttrib glfwGetWindowAttrib;

        internal delegate void SetWindowUserPointer(GLFWWindowPtr window,IntPtr pointer);
        internal static SetWindowUserPointer glfwSetWindowUserPointer;

        internal delegate IntPtr GetWindowUserPointer(GLFWWindowPtr window);
        internal static GetWindowUserPointer glfwGetWindowUserPointer;

        internal delegate GLFWWindowPosFun SetWindowPosCallback(GLFWWindowPtr window, GLFWWindowPosFun cbfun);
        internal static SetWindowPosCallback glfwSetWindowPosCallback;

        internal delegate GLFWWindowSizeFun SetWindowSizeCallback(GLFWWindowPtr window, GLFWWindowSizeFun cbfun);
        internal static SetWindowSizeCallback glfwSetWindowSizeCallback;

        internal delegate GLFWWindowCloseFun SetWindowCloseCallback(GLFWWindowPtr window, GLFWWindowCloseFun cbfun);
        internal static SetWindowCloseCallback glfwSetWindowCloseCallback;

        internal delegate GLFWWindowRefreshFun SetWindowRefreshCallback(GLFWWindowPtr window, GLFWWindowRefreshFun cbfun);
        internal static SetWindowRefreshCallback glfwSetWindowRefreshCallback;

        internal delegate GLFWWindowFocusFun SetWindowFocusCallback(GLFWWindowPtr window, GLFWWindowFocusFun cbfun);
        internal static SetWindowFocusCallback glfwSetWindowFocusCallback;

        internal delegate GLFWWindowIconifyFun SetWindowIconifyCallback(GLFWWindowPtr window, GLFWWindowIconifyFun cbfun);
        internal static SetWindowIconifyCallback glfwSetWindowIconifyCallback;

        internal delegate void PollEvents();
        internal static PollEvents glfwPollEvents;

        internal delegate void WaitEvents();
        internal static WaitEvents glfwWaitEvents;

        internal delegate int GetInputMode(GLFWWindowPtr window, InputMode mode);
        internal static GetInputMode glfwGetInputMode;

        internal delegate void SetInputMode(GLFWWindowPtr window, InputMode mode, CursorMode value);
        internal static SetInputMode glfwSetInputMode;

        internal delegate int GetKey(GLFWWindowPtr window, KeyCode key);
        internal static GetKey glfwGetKey;

        internal delegate int GetMouseButton(GLFWWindowPtr window, MouseButton button);
        internal static GetMouseButton glfwGetMouseButton;

        internal delegate void GetCursorPos(GLFWWindowPtr window, out double xpos, out double ypos);
        internal static GetCursorPos glfwGetCursorPos;

        internal delegate void SetCursorPos(GLFWWindowPtr window, double xpos, double ypos);
        internal static SetCursorPos glfwSetCursorPos;

        internal delegate GLFWKeyFun SetKeyCallback(GLFWWindowPtr window, GLFWKeyFun cbfun);
        internal static SetKeyCallback glfwSetKeyCallback;

        internal delegate GLFWCharFun SetCharCallback(GLFWWindowPtr window, GLFWCharFun cbfun);
        internal static SetCharCallback glfwSetCharCallback;

        internal delegate GLFWMouseButtonFun SetMouseButtonCallback(GLFWWindowPtr window, GLFWMouseButtonFun cbfun);
        internal static SetMouseButtonCallback glfwSetMouseButtonCallback;

        internal delegate GLFWCursorPosFun SetCursorPosCallback(GLFWWindowPtr window, GLFWCursorPosFun cbfun);
        internal static SetCursorPosCallback glfwSetCursorPosCallback;

        internal delegate GLFWCursorEnterFun SetCursorEnterCallback(GLFWWindowPtr window, GLFWCursorEnterFun cbfun);
        internal static SetCursorEnterCallback glfwSetCursorEnterCallback;

        internal delegate GLFWScrollFun SetScrollCallback(GLFWWindowPtr window, GLFWScrollFun cbfun);
        internal static SetScrollCallback glfwSetScrollCallback;

        internal delegate int JoystickPresent(Joystick joy);
        internal static JoystickPresent glfwJoystickPresent;

        internal delegate float* GetJoystickAxes(Joystick joy, out int numaxes);
        internal static GetJoystickAxes glfwGetJoystickAxes;

        internal delegate byte* GetJoystickButtons(Joystick joy, out int numbuttons);
        internal static GetJoystickButtons glfwGetJoystickButtons;

        internal delegate sbyte* GetJoystickName(Joystick joy);
        internal static GetJoystickName glfwGetJoystickName;

        internal delegate void SetClipboardString(GLFWWindowPtr window, [MarshalAs(UnmanagedType.LPStr)] string @string);
        internal static SetClipboardString glfwSetClipboardString;

        internal delegate sbyte* GetClipboardString(GLFWWindowPtr window);
        internal static GetClipboardString glfwGetClipboardString;

        internal delegate double GetTime();
        internal static GetTime glfwGetTime;

        internal delegate void SetTime(double time);
        internal static SetTime glfwSetTime;

        internal delegate void MakeContextCurrent(GLFWWindowPtr window);
        internal static MakeContextCurrent glfwMakeContextCurrent;

        internal delegate GLFWWindowPtr GetCurrentContext();
        internal static GetCurrentContext glfwGetCurrentContext;

        internal delegate void SwapBuffers(GLFWWindowPtr window);
        internal static SwapBuffers glfwSwapBuffers;

        internal delegate void SwapInterval(int interval);
        internal static SwapInterval glfwSwapInterval;

        internal delegate int ExtensionSupported([MarshalAs(UnmanagedType.LPStr)] string extension);
        internal static ExtensionSupported glfwExtensionSupported;

        internal delegate IntPtr GetProcAddress([MarshalAs(UnmanagedType.LPStr)] string procname);
        internal static GetProcAddress glfwGetProcAddress;

        internal delegate void GetFramebufferSize(GLFWWindowPtr window, out int width, out int height);
        internal static GetFramebufferSize glfwGetFramebufferSize;

        internal delegate GLFWFramebufferSizeFun SetFramebufferSizeCallback(GLFWWindowPtr window, GLFWFramebufferSizeFun cbfun);
        internal static SetFramebufferSizeCallback glfwSetFramebufferSizeCallback;
    }
}

