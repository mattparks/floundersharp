﻿using System;

namespace OpenGL.GLFW
{
    public static unsafe class GLFW3
    {
        #pragma warning disable 0414

        private static GLFWErrorFun errorCallback;
        private static GLFWFramebufferSizeFun framebufferSizeFun;
        private static GLFWWindowPosFun windowPosFun;
        private static GLFWWindowSizeFun windowSizeFun;
        private static GLFWWindowCloseFun windowCloseFun;
        private static GLFWWindowRefreshFun windowRefreshFun;
        private static GLFWWindowFocusFun windowFocusFun;
        private static GLFWWindowIconifyFun windowIconifyFun;
        private static GLFWKeyFun keyFun;
        private static GLFWCharFun charFun;
        private static GLFWMouseButtonFun mouseButtonFun;
        private static GLFWCursorPosFun cursorPosFun;
        private static GLFWCursorEnterFun cursorEnterFun;
        private static GLFWScrollFun scrollFun;

        public static bool Init() => GLFWDelegates.glfwInit() == 1;

        public static void Terminate() => GLFWDelegates.glfwTerminate();

        public static void GetVersion(out int major, out int minor, out int rev) => GLFWDelegates.glfwGetVersion(out major, out minor, out rev);

        public static string GetVersionString() => new string(GLFWDelegates.glfwGetVersionString());


        public static GLFWErrorFun SetErrorCallback(GLFWErrorFun cbfun) => GLFWDelegates.glfwSetErrorCallback(errorCallback = cbfun);

        public static unsafe GLFWMonitorPtr[] GetMonitors()
        {
            int count;
            GLFWMonitorPtr* array = GLFWDelegates.glfwGetMonitors(out count);
            GLFWMonitorPtr[] result = new GLFWMonitorPtr[count];

            for (int i = 0; i < count; ++i)
            {
                result[i] = array[i];
            }

            return result;
        }

        public static GLFWMonitorPtr GetPrimaryMonitor() => GLFWDelegates.glfwGetPrimaryMonitor();

        public static void GetMonitorPos(GLFWMonitorPtr monitor, out int xpos, out int ypos) => GLFWDelegates.glfwGetMonitorPos(monitor, out xpos, out ypos);

        public static void GetMonitorPhysicalSize(GLFWMonitorPtr monitor, out int width, out int height) => GLFWDelegates.glfwGetMonitorPhysicalSize(monitor, out width, out height);

        public static string GetMonitorName(GLFWMonitorPtr monitor) => new string(GLFWDelegates.glfwGetMonitorName(monitor));

        public static GLFWVidMode[] GetVideoModes(GLFWMonitorPtr monitor)
        {
            int count;
            GLFWVidMode* array = GLFWDelegates.glfwGetVideoModes(monitor, out count);
            GLFWVidMode[] result = new GLFWVidMode[count];

            for (int i = 0; i < count; ++i)
            {
                result[i] = array[i];
            }

            return result;
        }

        public static GLFWVidMode GetVideoMode(GLFWMonitorPtr monitor)
        {
            GLFWVidMode* vidMode = GLFWDelegates.glfwGetVideoMode(monitor);
            GLFWVidMode returnMode = new GLFWVidMode
            {
                RedBits = vidMode->RedBits,
                GreenBits = vidMode->GreenBits,
                BlueBits = vidMode->BlueBits,
                RefreshRate = vidMode->RefreshRate,
                Width = vidMode->Width,
                Height = vidMode->Height
            };

            return returnMode;
        }

        public static void SetGamma(GLFWMonitorPtr monitor, float gamma) => GLFWDelegates.glfwSetGamma(monitor, gamma);

        public static void GetGammaRamp(GLFWMonitorPtr monitor, out GLFWGammaRamp ramp)
        {
            GLFWGammaRampInternal rampI;
            GLFWDelegates.glfwGetGammaRamp(monitor, out rampI);
            uint length = rampI.Length;
            ramp = new GLFWGammaRamp();
            ramp.Red = new uint[length];
            ramp.Green = new uint[length];
            ramp.Blue = new uint[length];

            for (int i = 0; i < ramp.Red.Length; ++i)
            {
                ramp.Red[i] = rampI.Red[i];
            }

            for (int i = 0; i < ramp.Green.Length; ++i)
            {
                ramp.Green[i] = rampI.Green[i];
            }

            for (int i = 0; i < ramp.Blue.Length; ++i)
            {
                ramp.Blue[i] = rampI.Blue[i];
            }
        }

        public static void SetGammaRamp(GLFWMonitorPtr monitor, ref GLFWGammaRamp ramp)
        {
            ramp.Length = (uint)ramp.Red.Length;
            GLFWDelegates.glfwSetGammaRamp(monitor, ref ramp);
        }

        public static void DefaultWindowHints() => GLFWDelegates.glfwDefaultWindowHints();

        public static void WindowHint(WindowHint target, int hint) => GLFWDelegates.glfwWindowHint(target, hint);

        public static GLFWWindowPtr CreateWindow(int width, int height, string title, GLFWMonitorPtr monitor, GLFWWindowPtr share) => GLFWDelegates.glfwCreateWindow(width, height, title, monitor, share);

        public static void DestroyWindow(GLFWWindowPtr window) => GLFWDelegates.glfwDestroyWindow(window);

        public static void GetFramebufferSize(GLFWWindowPtr window, out int width, out int height) => GLFWDelegates.glfwGetFramebufferSize(window, out width, out height);

        public static bool WindowShouldClose(GLFWWindowPtr window) => GLFWDelegates.glfwWindowShouldClose(window) == 1;

        public static void SetWindowShouldClose(GLFWWindowPtr window, bool value) => GLFWDelegates.glfwSetWindowShouldClose(window, value ? 1 : 0);

        public static void SetWindowTitle(GLFWWindowPtr window, string title) => GLFWDelegates.glfwSetWindowTitle(window, title);

        public static void GetWindowPos(GLFWWindowPtr window, out int xpos, out int ypos) => GLFWDelegates.glfwGetWindowPos(window, out xpos, out ypos);

        public static void SetWindowPos(GLFWWindowPtr window, int xpos, int ypos) => GLFWDelegates.glfwSetWindowPos(window, xpos, ypos);

        public static void GetWindowSize(GLFWWindowPtr window, out int width, out int height) => GLFWDelegates.glfwGetWindowSize(window, out width, out height);

        public static void SetWindowSize(GLFWWindowPtr window, int width, int height) => GLFWDelegates.glfwSetWindowSize(window, width, height);

        public static void IconifyWindow(GLFWWindowPtr window) => GLFWDelegates.glfwIconifyWindow(window);

        public static void RestoreWindow(GLFWWindowPtr window) => GLFWDelegates.glfwRestoreWindow(window);

        public static void ShowWindow(GLFWWindowPtr window) => GLFWDelegates.glfwShowWindow(window);

        public static void HideWindow(GLFWWindowPtr window) => GLFWDelegates.glfwHideWindow(window);

        public static GLFWMonitorPtr GetWindowMonitor(GLFWWindowPtr window) => GLFWDelegates.glfwGetWindowMonitor(window);

        public static int GetWindowAttrib(GLFWWindowPtr window, WindowAttrib param) => GLFWDelegates.glfwGetWindowAttrib(window, (int)param);

        public static int GetWindowAttrib(GLFWWindowPtr window, WindowHint param) => GLFWDelegates.glfwGetWindowAttrib(window, (int)param);

        public static void SetWindowUserPointer(GLFWWindowPtr window, IntPtr pointer) => GLFWDelegates.glfwSetWindowUserPointer(window, pointer);

        public static IntPtr GetWindowUserPointer(GLFWWindowPtr window) => GLFWDelegates.glfwGetWindowUserPointer(window);

        public static GLFWFramebufferSizeFun SetFramebufferSizeCallback(GLFWWindowPtr window, GLFWFramebufferSizeFun cbfun) => GLFWDelegates.glfwSetFramebufferSizeCallback(window, framebufferSizeFun = cbfun);

        public static GLFWWindowPosFun SetWindowPosCallback(GLFWWindowPtr window, GLFWWindowPosFun cbfun) => GLFWDelegates.glfwSetWindowPosCallback(window, windowPosFun = cbfun);

        public static GLFWWindowSizeFun SetWindowSizeCallback(GLFWWindowPtr window, GLFWWindowSizeFun cbfun) => GLFWDelegates.glfwSetWindowSizeCallback(window, windowSizeFun = cbfun);

        public static GLFWWindowCloseFun SetWindowCloseCallback(GLFWWindowPtr window, GLFWWindowCloseFun cbfun) => GLFWDelegates.glfwSetWindowCloseCallback(window, windowCloseFun = cbfun);

        public static GLFWWindowRefreshFun SetWindowRefreshCallback(GLFWWindowPtr window, GLFWWindowRefreshFun cbfun) => GLFWDelegates.glfwSetWindowRefreshCallback(window, windowRefreshFun = cbfun);

        public static GLFWWindowFocusFun SetWindowFocusCallback(GLFWWindowPtr window, GLFWWindowFocusFun cbfun) => GLFWDelegates.glfwSetWindowFocusCallback(window, windowFocusFun = cbfun);

        public static GLFWWindowIconifyFun SetWindowIconifyCallback(GLFWWindowPtr window, GLFWWindowIconifyFun cbfun) => GLFWDelegates.glfwSetWindowIconifyCallback(window, windowIconifyFun = cbfun);

        public static void PollEvents() => GLFWDelegates.glfwPollEvents();

        public static void WaitEvents() => GLFWDelegates.glfwWaitEvents();

        public static int GetInputMode(GLFWWindowPtr window, InputMode mode) => GLFWDelegates.glfwGetInputMode(window, mode);

        public static void SetInputMode(GLFWWindowPtr window, InputMode mode, CursorMode value) => GLFWDelegates.glfwSetInputMode(window, mode, value);

        public static bool GetKey(GLFWWindowPtr window, KeyCode key) => GLFWDelegates.glfwGetKey(window, key) != 0;

        public static bool GetMouseButton(GLFWWindowPtr window, MouseButton button) => GLFWDelegates.glfwGetMouseButton(window, button) != 0;

        public static void GetCursorPos(GLFWWindowPtr window, out double xpos, out double ypos) => GLFWDelegates.glfwGetCursorPos(window, out xpos, out ypos);

        public static void SetCursorPos(GLFWWindowPtr window, double xpos, double ypos) => GLFWDelegates.glfwSetCursorPos(window, xpos, ypos);

        public static GLFWKeyFun SetKeyCallback(GLFWWindowPtr window, GLFWKeyFun cbfun) => GLFWDelegates.glfwSetKeyCallback(window, keyFun = cbfun);

        public static GLFWCharFun SetCharCallback(GLFWWindowPtr window, GLFWCharFun cbfun) => GLFWDelegates.glfwSetCharCallback(window, charFun = cbfun);

        public static GLFWMouseButtonFun SetMouseButtonCallback(GLFWWindowPtr window, GLFWMouseButtonFun cbfun) => GLFWDelegates.glfwSetMouseButtonCallback(window, mouseButtonFun = cbfun);

        public static GLFWCursorPosFun SetCursorPosCallback(GLFWWindowPtr window, GLFWCursorPosFun cbfun) => GLFWDelegates.glfwSetCursorPosCallback(window, cursorPosFun = cbfun);

        public static GLFWCursorEnterFun SetCursorEnterCallback(GLFWWindowPtr window, GLFWCursorEnterFun cbfun) => GLFWDelegates.glfwSetCursorEnterCallback(window, cursorEnterFun = cbfun);

        public static GLFWScrollFun SetScrollCallback(GLFWWindowPtr window, GLFWScrollFun cbfun) => GLFWDelegates.glfwSetScrollCallback(window, scrollFun = cbfun);

        public static bool JoystickPresent(Joystick joy) => GLFWDelegates.glfwJoystickPresent(joy) == 1;

        public static float[] GetJoystickAxes(Joystick joy)
        {
            int numaxes;
            float* axes = GLFWDelegates.glfwGetJoystickAxes(joy, out numaxes);
            float[] result = new float[numaxes];

            for (int i = 0; i < numaxes; ++i)
            {
                result[i] = axes[i];
            }

            return result;
        }

        public static byte[] GetJoystickButtons(Joystick joy)
        {
            int numbuttons;
            byte* buttons = GLFWDelegates.glfwGetJoystickButtons(joy, out numbuttons);
            byte[] result = new byte[numbuttons];

            for (int i = 0; i < numbuttons; ++i)
            {
                result[i] = buttons[i];
            }

            return result;
        }

        public static string GetJoystickName(Joystick joy) => new string(GLFWDelegates.glfwGetJoystickName(joy));

        public static void SetClipboardString(GLFWWindowPtr window, string @string) => GLFWDelegates.glfwSetClipboardString(window, @string);

        public static string GetClipboardString(GLFWWindowPtr window) => new string(GLFWDelegates.glfwGetClipboardString(window));

        public static double GetTime() => GLFWDelegates.glfwGetTime();

        public static void SetTime(double time) => GLFWDelegates.glfwSetTime(time);

        public static void MakeContextCurrent(GLFWWindowPtr window) => GLFWDelegates.glfwMakeContextCurrent(window);

        public static GLFWWindowPtr GetCurrentContext() => GLFWDelegates.glfwGetCurrentContext();

        public static void SwapBuffers(GLFWWindowPtr window) => GLFWDelegates.glfwSwapBuffers(window);

        public static void SwapInterval(int interval) => GLFWDelegates.glfwSwapInterval(interval);

        public static bool ExtensionSupported(string extension) => GLFWDelegates.glfwExtensionSupported(extension) == 1;

        public static IntPtr GetProcAddress(string procname) => GLFWDelegates.glfwGetProcAddress(procname);

        #pragma warning restore 0414
    }
}
