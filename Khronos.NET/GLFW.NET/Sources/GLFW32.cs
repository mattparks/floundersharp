﻿using System;
using System.Security;
using System.Runtime.InteropServices;

namespace OpenGL.GLFW
{
    internal static unsafe class GLFW32
    {
        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern int glfwInit();

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwTerminate();

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwGetVersion(out int major, out int minor, out int rev);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern sbyte* glfwGetVersionString();

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWErrorFun glfwSetErrorCallback(GLFWErrorFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWMonitorPtr* glfwGetMonitors(out int count);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWMonitorPtr glfwGetPrimaryMonitor();

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwGetMonitorPos(GLFWMonitorPtr monitor, out int xpos, out int ypos);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwGetMonitorPhysicalSize(GLFWMonitorPtr monitor, out int width, out int height);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern sbyte* glfwGetMonitorName(GLFWMonitorPtr monitor);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWVidMode* glfwGetVideoModes(GLFWMonitorPtr monitor, out int count);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWVidMode* glfwGetVideoMode(GLFWMonitorPtr monitor);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSetGamma(GLFWMonitorPtr monitor, float gamma);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwGetGammaRamp(GLFWMonitorPtr monitor, out GLFWGammaRampInternal ramp);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSetGammaRamp(GLFWMonitorPtr monitor, ref GLFWGammaRamp ramp);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwDefaultWindowHints();

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwWindowHint(WindowHint target, int hint);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWWindowPtr glfwCreateWindow(int width, int height, [MarshalAs(UnmanagedType.LPStr)] string title, GLFWMonitorPtr monitor, GLFWWindowPtr share);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwDestroyWindow(GLFWWindowPtr window);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern int glfwWindowShouldClose(GLFWWindowPtr window);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSetWindowShouldClose(GLFWWindowPtr window, int value);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSetWindowTitle(GLFWWindowPtr window, [MarshalAs(UnmanagedType.LPStr)] string title);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwGetWindowPos(GLFWWindowPtr window, out int xpos, out int ypos);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSetWindowPos(GLFWWindowPtr window, int xpos, int ypos);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwGetWindowSize(GLFWWindowPtr window, out int width, out int height);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSetWindowSize(GLFWWindowPtr window, int width, int height);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwIconifyWindow(GLFWWindowPtr window);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwRestoreWindow(GLFWWindowPtr window);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwShowWindow(GLFWWindowPtr window);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwHideWindow(GLFWWindowPtr window);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWMonitorPtr glfwGetWindowMonitor(GLFWWindowPtr window);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern int glfwGetWindowAttrib(GLFWWindowPtr window, int param);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSetWindowUserPointer(GLFWWindowPtr window, IntPtr pointer);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern IntPtr glfwGetWindowUserPointer(GLFWWindowPtr window);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWWindowPosFun glfwSetWindowPosCallback(GLFWWindowPtr window, GLFWWindowPosFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWWindowSizeFun glfwSetWindowSizeCallback(GLFWWindowPtr window, GLFWWindowSizeFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWWindowCloseFun glfwSetWindowCloseCallback(GLFWWindowPtr window, GLFWWindowCloseFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWWindowRefreshFun glfwSetWindowRefreshCallback(GLFWWindowPtr window, GLFWWindowRefreshFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWWindowFocusFun glfwSetWindowFocusCallback(GLFWWindowPtr window, GLFWWindowFocusFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWWindowIconifyFun glfwSetWindowIconifyCallback(GLFWWindowPtr window, GLFWWindowIconifyFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwPollEvents();

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwWaitEvents();

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern int glfwGetInputMode(GLFWWindowPtr window, InputMode mode);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSetInputMode(GLFWWindowPtr window, InputMode mode, CursorMode value);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern int glfwGetKey(GLFWWindowPtr window, KeyCode key);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern int glfwGetMouseButton(GLFWWindowPtr window, MouseButton button);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwGetCursorPos(GLFWWindowPtr window, out double xpos, out double ypos);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSetCursorPos(GLFWWindowPtr window, double xpos, double ypos);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWKeyFun glfwSetKeyCallback(GLFWWindowPtr window, GLFWKeyFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWCharFun glfwSetCharCallback(GLFWWindowPtr window, GLFWCharFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWMouseButtonFun glfwSetMouseButtonCallback(GLFWWindowPtr window, GLFWMouseButtonFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWCursorPosFun glfwSetCursorPosCallback(GLFWWindowPtr window, GLFWCursorPosFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWCursorEnterFun glfwSetCursorEnterCallback(GLFWWindowPtr window, GLFWCursorEnterFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWScrollFun glfwSetScrollCallback(GLFWWindowPtr window, GLFWScrollFun cbfun);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern int glfwJoystickPresent(Joystick joy);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern float* glfwGetJoystickAxes(Joystick joy, out int numaxes);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern byte* glfwGetJoystickButtons(Joystick joy, out int numbuttons);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern sbyte* glfwGetJoystickName(Joystick joy);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSetClipboardString(GLFWWindowPtr window, [MarshalAs(UnmanagedType.LPStr)] string @string);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern sbyte* glfwGetClipboardString(GLFWWindowPtr window);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern double glfwGetTime();

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSetTime(double time);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwMakeContextCurrent(GLFWWindowPtr window);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWWindowPtr glfwGetCurrentContext();

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSwapBuffers(GLFWWindowPtr window);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwSwapInterval(int interval);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern int glfwExtensionSupported([MarshalAs(UnmanagedType.LPStr)] string extension);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern IntPtr glfwGetProcAddress([MarshalAs(UnmanagedType.LPStr)] string procname);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern void glfwGetFramebufferSize(GLFWWindowPtr window, out int width, out int height);

        [DllImport("natives32/glfw.dll"), SuppressUnmanagedCodeSecurity]
        internal static extern GLFWFramebufferSizeFun glfwSetFramebufferSizeCallback(GLFWWindowPtr window, GLFWFramebufferSizeFun cbfun);
    }
}

