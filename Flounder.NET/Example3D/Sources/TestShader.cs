﻿using System;
using Flounder.Shaders;

namespace Flounder.Basics
{
    public class TestShader : ShaderProgram
    {
        public static string vertexShader = @"
#version 130

void main(void) 
{
}
        ";

        public static string fragmentShader = @"
#version 130

out vec4 out_colour;

void main(void) 
{
    out_colour = vec4(1, 0, 0, 1);
}
        ";
        
        public TestShader() : base(vertexShader, fragmentShader)
        {
        }
    }
}

