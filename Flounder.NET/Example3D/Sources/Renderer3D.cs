﻿using Flounder.Basics;
using Flounder.Devices;
using Flounder.Toolbox;
using Flounder.Toolbox.Matrices;
using Flounder.Toolbox.Vectors;
using OpenGL;
using System;
using Flounder.Basics.Options;
using System.Timers;

namespace Flounder.Basics
{
    /// <summary>
    /// Master renderer for 3d worlds.
    /// </summary>
    public class Renderer3D : IRendererMaster
    {
        private Colour clearColour;
        private Matrix4f projectionMatrix;

        // private TestShader shader;

        private float r = 255;
        private float g = 0;
        private float b = 0;

        ~Renderer3D()
        {
            CleanUp();
        }

        /// <summary>
        /// Initializes the various renderer types and various functionalities.
        /// </summary>
        public override void Init()
        {
            clearColour = new Colour(0.75f, 0.9f, 0);
            projectionMatrix = new Matrix4f();

           // shader = new TestShader();
        }

        /// <summary>
        /// Carries out the rendering of all components.
        /// </summary>
        public override void Render()
        {
            UpdateColour();

            /* Scene rendering. */
            RenderScene(new Vector4f(0, 1, 0, float.PositiveInfinity));

            /* Scene independents. */
            // guiRenderer.render(null, null);
            // fontRenderer.render(null, null);
        }

        private void RenderScene(Vector4f clipPlane)
        {
            OpenglUtils.PrepareNewRenderParse(clearColour);
            ICamera camera = EngineCore.module.camera;
            UpdatePerspectiveMatrix(camera.GetFOV(), DeviceDisplay.GetAspectRatio(), camera.GetNearPlane(), camera.GetFarPlane());
        }

        private void UpdateColour()
        {
            // Going from red to pink.
            if (r == 255 && g == 0 && b <= 254)
            {
                b += 2 * EngineCore.GetDeltaSeconds();
            }

            // Going from pink to blue.
            if (r >= 1 && g == 0 && b == 255)
            {
                r -= 2 * EngineCore.GetDeltaSeconds();
            }

            // Going from blue to cyan.
            if (r == 0 && g <= 254 && b == 255)
            {
                g += 2 * EngineCore.GetDeltaSeconds();
            }

            // Going from cyan to green.
            if (r == 0 && g == 255 && b >= 1)
            {
                b -= 2 * EngineCore.GetDeltaSeconds();
            }

            // Going from green to yellow.
            if (r <= 254 && g == 255 && b == 0)
            {
                r += 2 * EngineCore.GetDeltaSeconds();
            }

            // Going from yellow back to red.
            if (r == 255 && g >= 1 && b == 0)
            {
                g -= 2 * EngineCore.GetDeltaSeconds();
            }

            clearColour.Set(r, g, b, true);

            // Console.WriteLine(clearColour);
        }

        private void UpdatePerspectiveMatrix(float fov, float aspectRatio, float zNear, float zFar)
        {
            projectionMatrix.SetIdentity();
            float yScale = (float)(1f / Math.Tan(Maths.ToRadians(fov / 2f)) * aspectRatio);
            float xScale = yScale / aspectRatio;
            float depth = zFar - zNear;

            projectionMatrix.m00 = xScale;
            projectionMatrix.m11 = yScale;
            projectionMatrix.m22 = -((zFar + zNear) / depth);
            projectionMatrix.m23 = -1;
            projectionMatrix.m32 = -(2 * zNear * zFar / depth);
            projectionMatrix.m33 = 0;
        }

        public override void CleanUp()
        {
        }

        public override Matrix4f GetProjectionMatrix()
        {
            return projectionMatrix;
        }
    }
}
