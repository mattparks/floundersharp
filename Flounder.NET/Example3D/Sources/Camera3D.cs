﻿using Flounder.Basics;
using Flounder.Devices;
using Flounder.Space;
using Flounder.Toolbox;
using Flounder.Toolbox.Matrices;
using Flounder.Toolbox.Vectors;
using Flounder.Visual;
using OpenGL.GLFW;
using System;

namespace Example3D
{
    /// <summary>
    /// Represents the in-game camera and handles all of the camera movement and controls.
    /// </summary>
    public class Camera3D : ICamera
    {
        private static float ZOOM_AGILITY = 8f;
        private static float ROTATE_AGILITY = 6f;
        private static float PITCH_AGILITY = 8f;
        private static float FIELD_OF_VIEW = 70f;
        private static float NEAR_PLANE = 0.1f;
        private static float FAR_PLANE = 3200f;
        private static float CAMERA_AIM_OFFSET = 10.0f;
        private static float INFLUENCE_OF_MOUSEDY = -7200.0f;
        private static float INFLUENCE_OF_MOUSEDX = INFLUENCE_OF_MOUSEDY * 92.0f;
        private static float INFLUENCE_OF_MOUSE_WHEEL = 12.5f;
        private static float MAX_ANGLE_OF_ELEVATION = 1.5f;
        private static float PITCH_OFFSET = 3f;
        private static float MINIMUM_ZOOM = 8;
        private static float MAXIMUM_ZOOM = 300;
        private static float MAX_HORIZONTAL_CHANGE = 500;
        private static float MAX_VERTICAL_CHANGE = 5;
        private static float NORMAL_ZOOM = 32f;

        public static MouseButton toggleMouseMoveKey;
        private Matrix4f viewMatrix;
        private Vector3f position;

        private float horizontalDistanceFromFocus;
        private float verticalDistanceFromFocus;
        private float pitch;
        private float yaw;

        private SmoothFloat aimDistance;
        private MousePicker cameraAimer;

        private Vector3f targetPosition;
        private Vector3f targetRotation;
        private float actualDistanceFromPoint;
        private float targetZoom;
        private float targetElevation;
        private float angleOfElevation;
        private float targetRotationAngle;
        private float angleAroundPlayer;

        public Camera3D()
        {
            toggleMouseMoveKey = MouseButton.RightButton;
            viewMatrix = new Matrix4f();
            position = new Vector3f();
            targetPosition = new Vector3f(0, 0, 0);
            targetRotation = new Vector3f(0, 0, 0);
            actualDistanceFromPoint = NORMAL_ZOOM;
            targetZoom = actualDistanceFromPoint;
            targetElevation = 0.37f;
            angleOfElevation = targetElevation;
            targetRotationAngle = 0;
            angleAroundPlayer = targetRotationAngle;

            aimDistance = new SmoothFloat(1400.0f, 2.0f);
            cameraAimer = new MousePicker(this, true);

            CalculateDistances();
        }

        private static void CreateViewMatrix(Matrix4f viewMatrix, Vector3f position, float pitch, float yaw)
        {
            viewMatrix.SetIdentity();
            Vector3f cameraPos = new Vector3f(-position.x, -position.y, -position.z);
            Matrix4f.Rotate(viewMatrix, new Vector3f(1, 0, 0), Maths.ToRadians(pitch), viewMatrix);
            Matrix4f.Rotate(viewMatrix, new Vector3f(0, 1, 0), Maths.ToRadians(-yaw), viewMatrix);
            Matrix4f.Translate(viewMatrix, cameraPos, viewMatrix);
        }

        private void CalculateDistances()
        {
            horizontalDistanceFromFocus = (float)(actualDistanceFromPoint * Math.Cos(angleOfElevation));
            verticalDistanceFromFocus = (float)(actualDistanceFromPoint * Math.Sin(angleOfElevation));
        }

        
        public float GetNearPlane() // override
        {
            return NEAR_PLANE;
        }

        public float GetFarPlane() // override
        {
            return FAR_PLANE;
        }

        public float GetFOV() // override
        {
            return FIELD_OF_VIEW;
        }

        public void MoveCamera(Vector3f focusPosition, Vector3f focusRotation, bool gamePaused) // override
        {
            CalculateHorizontalAngle(gamePaused);
            CalculateVerticalAngle(gamePaused);
            CalculateZoom(gamePaused);

            targetPosition = focusPosition;
            targetRotation = focusRotation;

            UpdateActualZoom();
            UpdateHorizontalAngle();
            UpdatePitchAngle();
            CalculateDistances();
            CalculatePosition();
            CreateViewMatrix(viewMatrix, position, pitch, yaw);

            cameraAimer.Update();
            UpdateAimDistance();
        }

        private void CalculateHorizontalAngle(bool gamePaused)
        {
            float delta = EngineCore.GetDeltaSeconds();
            float angleChange = 0; // Maths.normalizeAngle(yaw) - Maths.normalizeAngle(entity.getRotation().getY())

            if (DeviceMouse.GetMouse((int)toggleMouseMoveKey) && !gamePaused)
            { // && !DeviceInput.isActiveInGUI()
                angleChange = (float)DeviceMouse.mouseDeltaX * INFLUENCE_OF_MOUSEDX;
            }

            if (angleChange > MAX_HORIZONTAL_CHANGE * delta)
            {
                angleChange = MAX_HORIZONTAL_CHANGE * delta;
            }
            else if (angleChange < -MAX_HORIZONTAL_CHANGE * delta)
            {
                angleChange = -MAX_HORIZONTAL_CHANGE * delta;
            }

            targetRotationAngle -= angleChange;

            if (targetRotationAngle >= Maths.DEGREES_IN_HALF_CIRCLE)
            {
                targetRotationAngle -= Maths.DEGREES_IN_CIRCLE;
            }
            else if (targetRotationAngle <= -Maths.DEGREES_IN_HALF_CIRCLE)
            {
                targetRotationAngle += Maths.DEGREES_IN_CIRCLE;
            }
        }

        private void CalculateVerticalAngle(bool gamePaused)
        {
            float delta = EngineCore.GetDeltaSeconds();
            float angleChange = 0;

            if (DeviceMouse.GetMouse((int)toggleMouseMoveKey) && !gamePaused)
            { // && !DeviceMouse.isActiveInGUI()
                angleChange = -(float)DeviceMouse.mouseDeltaY * INFLUENCE_OF_MOUSEDY;
            }

            if (angleChange > MAX_VERTICAL_CHANGE * delta)
            {
                angleChange = MAX_VERTICAL_CHANGE * delta;
            }
            else if (angleChange < -MAX_VERTICAL_CHANGE * delta)
            {
                angleChange = -MAX_VERTICAL_CHANGE * delta;
            }

            targetElevation -= angleChange;

            //if (targetElevation >= MAX_ANGLE_OF_ELEVATION) {
            //	targetElevation = MAX_ANGLE_OF_ELEVATION;
            //} else if (targetElevation <= 0) {
            //	targetElevation = 0;
            //}
        }

        private void CalculateZoom(bool gamePaused)
        {
            float zoomLevel = gamePaused ? 0 : (float)(DeviceMouse.mouseDeltaWheel / INFLUENCE_OF_MOUSE_WHEEL);
            // float zoomLevel = 0;

            if (zoomLevel != 0)
            {
                targetZoom -= zoomLevel;

                //if (targetZoom < MINIMUM_ZOOM) {
                //	targetZoom = MINIMUM_ZOOM;
                //} else if (targetZoom > MAXIMUM_ZOOM) {
                //	targetZoom = MAXIMUM_ZOOM;
                //}
            }
        }

        private void UpdateActualZoom()
        {
            float offset = targetZoom - actualDistanceFromPoint;
            float change = offset * EngineCore.GetDeltaSeconds() * ZOOM_AGILITY;
            actualDistanceFromPoint += change;
        }

        private void UpdateHorizontalAngle()
        {
            float offset = targetRotationAngle - angleAroundPlayer;

            if (Math.Abs(offset) > Maths.DEGREES_IN_HALF_CIRCLE)
            {
                if (offset < 0)
                {
                    offset = targetRotationAngle + Maths.DEGREES_IN_CIRCLE - angleAroundPlayer;
                }
                else
                {
                    offset = targetRotationAngle - Maths.DEGREES_IN_CIRCLE - angleAroundPlayer;
                }
            }

            float change = offset * EngineCore.GetDeltaSeconds() * ROTATE_AGILITY;
            angleAroundPlayer += change;

            if (angleAroundPlayer >= Maths.DEGREES_IN_HALF_CIRCLE)
            {
                angleAroundPlayer -= Maths.DEGREES_IN_CIRCLE;
            }
            else if (angleAroundPlayer <= -Maths.DEGREES_IN_HALF_CIRCLE)
            {
                angleAroundPlayer += Maths.DEGREES_IN_CIRCLE;
            }
        }

        private void UpdatePitchAngle()
        {
            float offset = targetElevation - angleOfElevation;
            float change = offset * EngineCore.GetDeltaSeconds() * PITCH_AGILITY;
            angleOfElevation += change;
        }

        private void CalculatePosition()
        {
            float theta = targetRotation.y + angleAroundPlayer;
            position.x = targetPosition.x - (float)(horizontalDistanceFromFocus * Math.Sin(Maths.ToRadians(theta)));
            position.z = targetPosition.z - (float)(horizontalDistanceFromFocus * Math.Cos(Maths.ToRadians(theta)));
            position.y = targetPosition.y + (verticalDistanceFromFocus + CAMERA_AIM_OFFSET);

            yaw = targetRotation.y + Maths.DEGREES_IN_HALF_CIRCLE + angleAroundPlayer;
            pitch = (float)Maths.ToDegrees(angleOfElevation) - PITCH_OFFSET;
        }

        private void UpdateAimDistance()
        {
            Vector3f aimPoint = cameraAimer.GetCurrentTerrainPoint();

            if (aimPoint != null)
            {
                aimDistance.Set(Vector3f.Subtract(aimPoint, position, null).Length());
            }
            else
            {
                aimDistance.Set(actualDistanceFromPoint);
            }

            aimDistance.Update();
        }

        public Matrix4f GetViewMatrix() // override
        {
            return viewMatrix;
        }

        public Frustum GetViewFrustum() // override
        {
            return Frustum.GetFrustum(EngineCore.module.rendererMaster.GetProjectionMatrix(), viewMatrix);
        }

        public Matrix4f GetReflectionViewMatrix(float planeHeight) // override
        {
            Matrix4f reflectionMatrix = new Matrix4f();
            Vector3f camPosition = new Vector3f(position);
            camPosition.y -= 2 * (position.y - planeHeight);
            float reflectedPitch = -pitch;
            CreateViewMatrix(reflectionMatrix, camPosition, reflectedPitch, yaw);
            return reflectionMatrix;
        }

        public void Reflect(float waterHeight) // override
        {
            position.y -= 2 * (position.y - waterHeight);
            pitch = -pitch;
            CreateViewMatrix(viewMatrix, position, pitch, yaw);
        }

        public Vector3f GetPosition() // override
        {
            return position;
        }

        public float GetPitch() // override
        {
            return pitch;
        }

        public float GetYaw()
        {
            return yaw;
        }

        public float GetAimDistance() // override
        {
            return aimDistance.Get();
        }
    }
}
