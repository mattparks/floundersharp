﻿using Flounder.Basics;
using Flounder.Inputs;
using Flounder.Toolbox.Vectors;
using OpenGL.GLFW;
using System;
using System.Timers;

namespace Example3D
{
    public class Game3D : IGame
    {
        private IAxis inputForward;
        private IAxis inputTurn;
        private IButton inputJump;
        private IButton inputBoost;
        private Timer loggerTimer;

        public Game3D() : base(new IModule(new Renderer3D(), new Camera3D()))
        {
        }

        public override void Init()
        {
            IButton leftKeyButtons = new KeyboardButton(new int[]{ (int)KeyCode.A, (int)KeyCode.Left });
            IButton rightKeyButtons = new KeyboardButton(new int[]{ (int)KeyCode.D, (int)KeyCode.Right });
            IButton upKeyButtons = new KeyboardButton(new int[]{ (int)KeyCode.W, (int)KeyCode.Up });
            IButton downKeyButtons = new KeyboardButton(new int[]{ (int)KeyCode.S, (int)KeyCode.Down });
            IButton jumpKeyButtons = new KeyboardButton((int)KeyCode.Space);
            IButton boostKeyButtons = new KeyboardButton((int)KeyCode.LeftShift);

            IAxis forwardJoyAxis = new JoystickAxis(0, 1);
            IAxis turnJoyAxis = new JoystickAxis(0, 3);
            IButton jumpJoyButton = new JoystickButton(0, 0);
            IButton boostJoyButton = new JoystickButton(0, 1);

            forwardJoyAxis = null;
            turnJoyAxis = null;
            jumpJoyButton = null;
            boostJoyButton = null;

            inputForward = new CompoundAxis(new IAxis[] { new ButtonAxis(upKeyButtons, downKeyButtons), forwardJoyAxis });
            inputTurn = new CompoundAxis(new IAxis[] { new ButtonAxis(leftKeyButtons, rightKeyButtons), turnJoyAxis });
            inputJump = new CompoundButton(new IButton[] { jumpKeyButtons, jumpJoyButton });
            inputBoost = new CompoundButton(new IButton[] { boostKeyButtons, boostJoyButton });

            loggerTimer = new Timer();
            loggerTimer.Elapsed += new ElapsedEventHandler(DisplayTimeEvent);
            loggerTimer.Interval = 500; // 1000 ms is one second.
            loggerTimer.Start();
        }

        public override void Update()
        {
            UpdateGame(new Vector3f(), new Vector3f(), false, 0);
        }

        public void DisplayTimeEvent(object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Jump: {0}, Boost: {1}, Forward: {2}, Turn: {3}", inputJump.IsDown(), inputBoost.IsDown(), inputForward.GetAmount(), inputTurn.GetAmount());
        }

        public override void CleanUp()
        {
            loggerTimer.Stop();
        }
    }
}

