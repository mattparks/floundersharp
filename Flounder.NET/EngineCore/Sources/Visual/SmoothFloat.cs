﻿using Flounder.Basics;

namespace Flounder.Visual
{
    public class SmoothFloat
    {
        private float agility;
        private float target;
        private float actual;

        public SmoothFloat(float initialValue, float agility)
        {
            this.target = initialValue;
            this.actual = initialValue;
            this.agility = agility;
        }

        ~SmoothFloat()
        {
        }

        public void Update()
        {
            var offset = target - actual;
            var change = offset * EngineCore.GetDeltaSeconds() * agility;
            actual += change;
        }

        public float Get()
        {
            return actual;
        }

        public void Set(float target)
        {
            this.target = target;
        }
    }
}
