﻿namespace Flounder.Visual
{
    public class LinearDriver : ValueDriver
    {
        private float startValue;
        private float difference;

        public LinearDriver(float startValue, float endValue, float length) : base(length)
        {
            this.startValue = startValue;
            this.difference = endValue - startValue;
        }

        ~LinearDriver()
        {
        }
        
        protected override float CalculateValue(float time)
        {
            return startValue + time * difference;
        }
    }
}
