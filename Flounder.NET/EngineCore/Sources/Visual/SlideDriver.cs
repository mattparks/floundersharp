﻿using Flounder.Toolbox;

namespace Flounder.Visual
{
    public class SlideDriver : ValueDriver
    {
        private float startValue;
        private float endValue;
        private float max = 0;
        private bool reachedTarget = false;

        public SlideDriver(float start, float end, float length) : base(length)
        {
            this.startValue = start;
            this.endValue = end;
        }

        ~SlideDriver()
        {
        }
        
        protected override float CalculateValue(float time)
        {
            if (!reachedTarget && time >= max)
            {
                max = time;
                return Maths.CosInterpolate(startValue, endValue, time);
            }
            else
            {
                reachedTarget = true;
                return startValue + (endValue - startValue);
            }
        }
    }
}
