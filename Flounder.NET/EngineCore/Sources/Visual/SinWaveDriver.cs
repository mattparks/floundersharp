﻿using System;

namespace Flounder.Visual
{
    public class SinWaveDriver : ValueDriver
    {
        private float min;
        private float amplitude;

        public SinWaveDriver(float min, float max, float length) : base(length)
        {
            this.min = min;
            this.amplitude = max - min;
        }

        ~SinWaveDriver()
        {
        }
        
        protected override float CalculateValue(float time)
        {
            var value = 0.5f + (float)Math.Sin(time * Math.PI * 2) * 0.5f;
            return min + value * amplitude;
        }
    }
}
