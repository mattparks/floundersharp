﻿namespace Flounder.Visual
{
    public class FadeDriver : ValueDriver
    {
        private float start { get; set; }
        private float end { get; set; }
        private float peak { get; set; }

        public FadeDriver(float peak, float start, float end, float duration) : base(duration)
        {
            this.peak = peak;
            this.start = start;
            this.end = end;
        }

        ~FadeDriver()
        {
        }

        protected override float CalculateValue(float time)
        {
            if (time < start)
            {
                return time / start * peak;
            }
            else if (time > end)
            {
                return (1 - (time - end) / (1 - end)) * peak;
            }
            else
            {
                return peak;
            }
        }
    }
}
