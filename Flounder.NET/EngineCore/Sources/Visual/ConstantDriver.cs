﻿namespace Flounder.Visual
{
    public class ConstantDriver : ValueDriver
    {
        private float value { get; set; }

        public ConstantDriver(float constant) : base(1)
        {
            value = constant;
        }

        ~ConstantDriver()
        {
        }
        
        protected override float CalculateValue(float time)
        {
            return value;
        }
    }
}
