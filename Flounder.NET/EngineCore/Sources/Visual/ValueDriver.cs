﻿namespace Flounder.Visual
{
    public abstract class ValueDriver
    {
        private float currentTime = 0;
        private float length;

        public ValueDriver(float length)
        {
            this.length = length;
        }

        ~ValueDriver()
        {
        }

        public float Update(float delta)
        {
            currentTime += delta;
            currentTime %= length;
            var time = currentTime / length;
            return CalculateValue(time);
        }

        protected abstract float CalculateValue(float time);
    }
}
