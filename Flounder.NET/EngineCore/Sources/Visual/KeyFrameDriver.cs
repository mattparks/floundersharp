﻿using System;

namespace Flounder.Visual
{
    public class KeyFrameDriver : ValueDriver
    {
        private KeyFrame[] keyFrames;

        public KeyFrameDriver(KeyFrame[] keyFrames, float length) : base(length)
        {
            this.keyFrames = keyFrames;
        }

        ~KeyFrameDriver()
        {
        }
        
        protected override float CalculateValue(float time)
        {
            var index = FindNextFrameIndex(time, 0, keyFrames.Length - 1);
            var previous = keyFrames[index - 1];
            var next = keyFrames[index];
            var factor = (time - previous.time) / (next.time - previous.time);
            var difference = next.value - previous.value;
            return previous.value + factor * difference;
        }

        private int FindNextFrameIndex(float time, int firstIndex, int lastIndex)
        {
            if (firstIndex == lastIndex)
            {
                return lastIndex + 1;
            }

            var length = 1.0f + lastIndex - firstIndex;
            var check = (int)Math.Floor(length / 2) + firstIndex - 1;
            var number1 = keyFrames[check].time;
            var number2 = keyFrames[check + 1].time;

            if (number1 > time)
            {
                return FindNextFrameIndex(time, firstIndex, check);
            }
            else
            {
                if (number2 > time)
                {
                    return check + 1;
                }
                else
                {
                    return FindNextFrameIndex(time, check + 1, lastIndex);
                }
            }
        }
    }
}
