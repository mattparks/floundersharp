﻿namespace Flounder.Visual
{
    public class KeyFrame
    {
        public float value { get; set; }
        public float time { get; set; }

        public KeyFrame(float time, float value)
        {
            this.time = time;
            this.value = value;
        }

        ~KeyFrame()
        {
        }
    }
}
