﻿using System;
using System.Collections.Generic;

namespace Flounder.Toolbox
{
    /// <summary>
    /// Various generic sorting algorithms. Values passed must be {@code comparable}!
    /// <p>Watch some of these algorithms at work here: @see <a href="https://www.youtube.com/watch?v=ZZuD6iUe3Pc">'Visualization and Comparison of Sorting Algorithms'</a></p>
    /// </summary>
    public static class Sorting
    {
        /// <summary>
        /// First divides a large array into two smaller sub-arrays: the low elements and the high elements. Then recursively sort the sub-arrays.
        /// </summary>
        /// <param name="list">The list to be sorted.</param>
        /// <typeparam name="T">The list type to be sorted.</typeparam>
        public static void QuickSort<T>(List<T> list) where T : IComparable
        {
            if (list.Count > 0) 
            {
                var pivot = list[0]; // This pivot can change to get faster results.

                var less = new List<T>();
                var pivotList = new List<T>();
                var more = new List<T>();

                foreach (var i in list) 
                {
                    if (i.CompareTo(pivot) < 0) 
                    {
                        less.Add(i);
                    } 
                    else if (i.CompareTo(pivot) > 0) 
                    {
                        more.Add(i);
                    } 
                    else
                    {
                        pivotList.Add(i);
                    }
                }

                QuickSort(less);
                QuickSort(more);
                less.AddRange(pivotList);
                less.AddRange(more);

                list.Clear();
                less.ForEach(i => list.Add(i));
            }
        }

        /// <summary>
        /// The first pass, 5-sorting, performs insertion sort on separate subarrays. The next pass, 3-sorting, performs insertion sort on the subarrays. The last pass, 1-sorting, is an ordinary insertion sort of the entire array.
        /// </summary>
        /// <param name="list">The list to be sorted.</param>
        /// <typeparam name="T">The list type to be sorted.</typeparam>
        public static void ShellSort<T>(List<T> list) where T : IComparable
        {
            var n = list.Count;
            var h = 1;

            while (h < (n >> 1))
            {
                h = (h << 1) + 1;
            }

            while (h >= 1)
            {
                for (var i = h; i < n; i++)
                {
                    var k = i - h;

                    for (var j = i; j >= h && list[j].CompareTo(list[k]) < 0; k -= h)
                    {
                        var temp = list[j];
                        list[j] = list[k];
                        list[k] = temp;
                        j = k;
                    }
                }

                h >>= 1;
            }
        }

        /// <summary>
        /// Divides the unsorted list into sublists, each containing 1 element, then repeatedlys merge sublists to produce new sorted sublists until there is only 1 sublist remaining.
        /// </summary>
        /// <param name="list">The list to be sorted.</param>
        /// <typeparam name="T">The list type to be sorted.</typeparam>
        public static void MergeSort<T>(List<T> list) where T : IComparable
        {
            // TODO: Merge Sort algorithms C#!
        }

        /// <summary>
        /// Prepares the list by first turning it into a max heap. Then repeatedly swaps the first value of the list with the last value, decreasing the range of values considered in the heap operation by one, and sifting the new first value into its position in the heap. It repeats until the range of values is one value in length.
        /// </summary>
        /// <param name="list">The list to be sorted.</param>
        /// <typeparam name="T">The list type to be sorted.</typeparam>
        public static void HeapSort<T>(List<T> list) where T : IComparable
        {
            var count = list.Count;
            var start = count / 2 - 1;
            var end = count - 1;

            while (start >= 0)
            {
                SiftDown(list, start, count - 1);
                start -= 1;
            }

            while (end > 0)
            {
                var temp = list[end];
                list[end] = list[0];
                list[0] = temp;
                end = end - 1;
                SiftDown(list, 0, end);
            }
        }

        /// <summary>
        /// Sifts down the list.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <param name="start">The list start.</param>
        /// <param name="end">The list end.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        private static void SiftDown<T>(List<T> list, int start, int end) where T : IComparable
        {
            var root = start;

            while (root * 2 + 1 <= end)
            {
                var child = root * 2 + 1;
                var swap = root;

                if (list[swap].CompareTo(list[child]) < 0)
                {
                    swap = child;
                }

                if (child + 1 <= end && list[swap].CompareTo(list[child + 1]) < 0)
                {
                    swap = child + 1;
                }

                if (swap != root)
                {
                    var temp = list[root];
                    list[root] = list[swap];
                    list[swap] = temp;
                    root = swap;
                }
                else 
                {
                    return;
                }
            }
        }

        /// <summary>
        /// The gap starts out as the length of the list being sorted divided by the shrink factor, and the list is sorted with that value as the gap. Then the gap is divided by the shrink factor again, the list is sorted with this new gap, and the process repeats until the gap is 1. At this point, comb sort continues using a gap of 1 until the list is fully sorted.
        /// </summary>
        /// <param name="list">The list to be sorted.</param>
        /// <typeparam name="T">The list type to be sorted.</typeparam>
        public static void CombSort<T>(List<T> list) where T : IComparable
        {
            var gap = list.Count;
            var swapped = true;

            while (gap > 1 || swapped)
            {
                if (gap > 1)
                {
                    gap = (int)(gap / 1.3);
                }

                swapped = false;

                for (var i = 0; i + gap < list.Count; i++)
                {
                    if (list[i].CompareTo(list[i + gap]) > 0)
                    {
                        var temp = list[i];
                        list[i] = list[i + gap];
                        list[i + gap] = temp;
                        swapped = true;
                    }
                }
            }
        }

        /// <summary>
        /// Iterates consuming one input element each repetition, and growing a sorted output list. Each iteration, insertion sort removes one element from the input data, finds the location it belongs within the sorted list, and inserts it there. It repeats until no input elements remain.
        /// </summary>
        /// <param name="list">The list to be sorted.</param>
        /// <typeparam name="T">The list type to be sorted.</typeparam>
        public static void InsertionSort<T>(List<T> list) where T : IComparable
        {
            for (var i = 1; i < list.Count; i++)
            {
                var toInsert = list[i];
                var j = i - 1;

                while (j >= 0 && (list[j].CompareTo(toInsert) > 0))
                {
                    list[j + 1] = list[j];
                    j--;
                }

                list[j + 1] = toInsert;
            }
        }

        /// <summary>
        /// Divides the input list into two parts: the sublist of items already sorted, which is built up from left to right at the front of the list, and the sublist of items remaining to be sorted that occupy the rest of the list.
        /// </summary>
        /// <param name="list">The list to be sorted.</param>
        /// <typeparam name="T">The list type to be sorted.</typeparam>
        public static void SelectionSort<T>(List<T> list) where T : IComparable
        {
            for (var x = 0; x < list.Count; x++)
            {
                var minimum = x;

                for (var y = x; y < list.Count; y++)
                {
                    if (list[minimum].CompareTo(list[y]) > 0)
                    {
                        minimum = y;
                    }
                }

                var temp = list[minimum];
                list[minimum] = list[x];
                list[x] = temp;
            }
        }

        /// <summary>
        /// Unlike bubble sort orders the array in both directions. Hence every iteration of the algorithm consists of two phases. In the first one the lightest bubble ascends to the end of the array, in the second phase the heaviest bubble descends to the beginning of the array.
        /// </summary>
        /// <param name="list">The list to be sorted.</param>
        /// <typeparam name="T">The list type to be sorted.</typeparam>
        public static void CocktailSort<T>(List<T> list) where T : IComparable
        {
            bool swapped;

            do
            {
                swapped = false;

                for (var i = 0; i <= list.Count - 2; i++)
                {
                    if (list[i].CompareTo(list[i + 1]) > 0)
                    {
                        var temp = list[i];
                        list[i] = list[i + 1];
                        list[i + 1] = temp;
                        swapped = true;
                    }
                }

                if (!swapped)
                {
                    break;
                }

                swapped = false;

                for (var i = list.Count - 2; i >= 0; i--)
                {
                    if (list[i].CompareTo(list[i + 1]) > 0)
                    {
                        var temp = list[i];
                        list[i] = list[i + 1];
                        list[i + 1] = temp;
                        swapped = true;
                    }
                }
            } while (swapped);
        }

        /// <summary>
        /// Finds the position of a target value within a sorted array.
        /// </summary>
        /// <param name="list">list The list to be searched.</param>
        /// <param name="value">The value to be found.</param>
        /// <typeparam name="T">The list type to be searched.</typeparam>
        /// <returns>The search for value.</returns>
        public static int BinarySearchForValue<T>(List<T> list, T value) where T : IComparable
        {
            var lowIndex = 0;
            var highIndex = list.Count - 1;

            while (lowIndex <= highIndex)
            {
                var middleIndex = (highIndex + lowIndex) / 2;

                if (list[middleIndex].CompareTo(value) < 0)
                {
                    lowIndex = middleIndex + 1;
                }
                else if (list[middleIndex].CompareTo(value) > 0)
                {
                    highIndex = middleIndex - 1;
                }
                else 
                {
                    return middleIndex;
                }
            }

            return -1;
        }

        /// <summary>
        /// Always finds the first place where two adjacent elements are in the wrong order, and swaps them. It takes advantage of the fact that performing a swap can introduce a new out-of-order adjacent pair only next to the two swapped elements. It does not assume that elements forward of the current position are sorted.
        /// </summary>
        /// <param name="list">The list to be sorted.</param>
        /// <typeparam name="T">The list type to be sorted.</typeparam>
        public static void GnomeSort<T>(List<T> list) where T : IComparable
        {
            var i = 1;
            var j = 2;

            while (i < list.Count)
            {
                if (list[i - 1].CompareTo(list[i]) <= 0)
                {
                    i = j;
                    j++;
                }
                else 
                {
                    var tmp = list[i - 1];
                    list[i - 1] = list[i];
                    list[i--] = tmp;
                    i = (i == 0) ? j++ : i;
                }
            }
        }

        /// <summary>
        /// Repeatedly steps through the list to be sorted, compares each pair of adjacent items and swaps them if they are in the wrong order. The pass through the list is repeated until no swaps are needed.
        /// </summary>
        /// <param name="list">The list to be sorted.</param>
        /// <typeparam name="T">The list type to be sorted.</typeparam>
        public static void BubbleSort<T>(List<T> list) where T : IComparable
        {
            for (var i = list.Count - 1; i > 1; i--)
            {
                for (var j = 0; j < i; j++)
                {
                    if (list[j].CompareTo(list[j + 1]) > 0)
                    {
                        var temp = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = temp;
                    }
                }
            }
        }
    }
}
