﻿using System;
using System.IO;

namespace Flounder.Toolbox.Resources
{
    /// <summary>
    /// A engine file that can be read from within the exported runnable.
    /// </summary>
    public class MyFile
    {
        /// <summary>
        /// The file seperator.
        /// </summary>
        public static char FILE_SEPARATOR = '/';

        /// <summary>
        /// The base res folder.
        /// </summary>
        public static MyFile RES_FOLDER = new MyFile("res");

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>The path.</value>
		public string path { get; private set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public string name { get; private set; }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Resources.MyFile"/> class.
        /// </summary>
        /// <param name="path">The path for this file to represent.</param>
        public MyFile(string path)
        {
            this.path = FILE_SEPARATOR + path;
            var dirs = path.Split(FILE_SEPARATOR);
            this.name = dirs[dirs.Length - 1];
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Resources.MyFile"/> class.
        /// </summary>
        /// <param name="paths">Paths for this file to represent.</param>
        public MyFile(string[] paths)
        {
            path = "";

            foreach (var part in paths)
            {
                path += FILE_SEPARATOR + part;
            }

            var dirs = path.Split(FILE_SEPARATOR);
            name = dirs[dirs.Length - 1];
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Resources.MyFile"/> class.
        /// </summary>
        /// <param name="file">The file for this file to represent.</param>
        /// <param name="subFile">The name of the represented sub file.</param>
        public MyFile(MyFile file, string subFile)
        {
            path = file.path + FILE_SEPARATOR + subFile;
            name = subFile;
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Resources.MyFile"/> class.
        /// </summary>
        /// <param name="file">The file for this file to represent.</param>
        /// <param name="subFiles">Names of the represented sub file.</param>
        public MyFile(MyFile file, string[] subFiles)
        {
            path = file.path;

            foreach (var part in subFiles)
            {
                path += FILE_SEPARATOR + part;
            }

            var dirs = path.Split(FILE_SEPARATOR);
            name = dirs[dirs.Length - 1];
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Toolbox.Resources.MyFile"/> is reclaimed by garbage collection.
        /// </summary>
        ~MyFile()
        {
        }

        /// <summary>
        /// Creates a reader for the file.
        /// </summary>
        /// <returns>A buffered reader for the file.</returns>
        public StreamReader GetReader()
        {
            try
            {
				return null; // TODO: new StreamReader(GetInputStream()); // TODO Reading Steam!
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldn't get reader for " + path);
                throw e;
            }
        }
        
        /// <summary>
        /// Gets the input steam to the file path.
        /// </summary>
        /// <returns>The input steam to the file path.</returns>
        public Stream GetInputStream()
        {
            return null; // Class.class.getResourceAsStream(path); // TODO Input Stream!
        }

        public override string ToString()
        {
            return path;
        }
    }
}
