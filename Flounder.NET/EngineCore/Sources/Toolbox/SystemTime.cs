﻿using System;

namespace Flounder.Toolbox
{
    /// <summary>
    /// Extension methods for accessing Microseconds and Nanoseconds of a DateTime object.
    /// </summary>
    public static class SystemTime
    {
        /// <summary>
        /// Gets the microsecond fraction of a DateTime.
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static long Microseconds()
        {
            return Environment.TickCount;
        }

        /// <summary>
        /// Gets the Nanosecond fraction of a DateTime.  Note that the DateTime
        /// object can only store nanoseconds at resolution of 100 nanoseconds.
        /// </summary>
        /// <param name="self">The DateTime object.</param>
        /// <returns>the number of Nanoseconds.</returns>
        public static long Nanoseconds()
        {
            return ConvertMillisecondsToNanoseconds(Environment.TickCount);
        }

        public static long ConvertMillisecondsToNanoseconds(long milliseconds)
        {
            // One million nanoseconds in one nanosecond.
            return milliseconds * 1000000;
        }

        public static long ConvertMicrosecondsToNanoseconds(long microseconds)
        {
            // One thousand microseconds in one nanosecond.
            return (long) (microseconds * 0.001);
        }

        public static long ConvertMillisecondsToMicroseconds(long milliseconds)
        {
            // One thousand milliseconds in one microsecond.
            return milliseconds * 1000;
        }

        public static long ConvertNanosecondsToMilliseconds(long nanoseconds)
        {
            // One million nanoseconds in one millisecond.
            return (long)(nanoseconds * 0.000001);
        }

        public static long ConvertMicrosecondsToMilliseconds(long microseconds)
        {
            // One thousand microseconds in one millisecond.
            return microseconds * 1000;
        }

        public static long ConvertNanosecondsToMicroseconds(long nanoseconds)
        {
            // One thousand nanoseconds in one microsecond.
            return nanoseconds * 1000;
        }
    }
}
