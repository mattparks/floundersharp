﻿using Flounder.Basics;
using Flounder.Devices;
using Flounder.Toolbox.Matrices;
using Flounder.Toolbox.Vectors;

namespace Flounder.Toolbox
{
    /// <summary>
    /// Represents a world terraun mouse picker.
    /// </summary>
    public class MousePicker
    {
        /// <summary>
        /// The total point recursions.
        /// </summary>
        private static int RECURSION_COUNT = 5;

        /// <summary>
        /// The rays range.
        /// </summary>
        private static float RAY_RANGE = 120.0f;

        /// <summary>
        /// The ray section length.
        /// </summary>
        private static float RAY_SECTION = 2.0f;

        /// <summary>
        /// The current ray.
        /// </summary>
        private Vector3f currentRay = new Vector3f();

        /// <summary>
        /// The camera.
        /// </summary>
        private ICamera camera;

        /// <summary>
        /// The current terrain point.
        /// </summary>
        private Vector3f currentTerrainPoint;

        /// <summary>
        /// Should it pick the center?
        /// </summary>
        private bool pickCenter;

        /// <summary>
        /// Is the ray up to date?
        /// </summary>
        private bool rayUpToDate = false;

        /// <summary>
        /// Is the terrain point up to date?
        /// </summary>
        private bool terrainPointUpToDate = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.MousePicker"/> class.
        /// </summary>
        /// <param name="camera">The camera.</param>
        /// <param name="pickCenter">If set to <c>true</c> it picks from the center.</param>
        public MousePicker(ICamera camera, bool pickCenter)
        {
            this.camera = camera;
            this.pickCenter = pickCenter;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Flounder.Toolbox.MousePicker"/> is reclaimed by garbage collection.
        /// </summary>
        ~MousePicker()
        {
            currentRay = null;
            camera = null;
            currentTerrainPoint = null;
        }

        /// <summary>
        /// Gets the current terrain point.
        /// </summary>
        /// <returns>The current terrain point.</returns>
        public Vector3f GetCurrentTerrainPoint()
        {
            if (!terrainPointUpToDate)
            {
                UpdateTerrainPoint();
            }

            return currentTerrainPoint;
        }

        /// <summary>
        /// Gets the current ray.
        /// </summary>
        /// <returns>The current ray.</returns>
        public Vector3f GetCurrentRay()
        {
            if (!rayUpToDate)
            {
                UpdateMouseRay();
            }

            return currentRay;
        }

        /// <summary>
        /// Updates this mouse picker.
        /// </summary>
        public void Update()
        {
            rayUpToDate = false;
            terrainPointUpToDate = false;
        }

        /// <summary>
        /// Updates the terrain point.
        /// </summary>
        private void UpdateTerrainPoint()
        {
            var ray = GetCurrentRay();

            if (IntersectionInRange(0.0f, 120.0f, ray))
            {
                var section = GetSectionID(ray);
                currentTerrainPoint = BinarySearch(0, section * 2.0f, (section + 1.0f) * 2.0f, ray);
            }
            else 
            {
                currentTerrainPoint = null;
            }

            terrainPointUpToDate = true;
        }

        /// <summary>
        /// Updates the mouse ray.
        /// </summary>
        private void UpdateMouseRay()
        {
            Vector2f normalizedCoords;

            if (pickCenter)
            {
                normalizedCoords = new Vector2f(0.0f, 0.0f);
            }
            else 
            {
                normalizedCoords = GetNormalisedDeviceCoordinates((float)DeviceMouse.mousePositionX, (float)DeviceMouse.mousePositionY);
            }

            currentRay = ToWorldCoords(ToEyeCoords(new Vector4f(normalizedCoords.x, normalizedCoords.y, -1.0f, 1.0f)));
            rayUpToDate = true;
        }

        /// <summary>
        /// Converts to the world coords.
        /// </summary>
        /// <param name="eyeCoords">Eye coords.</param>
        /// <returns>The world coords.</returns>
        private Vector3f ToWorldCoords(Vector4f eyeCoords)
        {
            var invertedView = Matrix4f.Invert(camera.GetViewMatrix(), null);
            var rayWorld = Matrix4f.Transform(invertedView, eyeCoords, null);
            var mouseRay = new Vector3f(rayWorld.x, rayWorld.y, rayWorld.z);
            mouseRay.Normalize();
            return mouseRay;
        }

        /// <summary>
        /// Converts to the eye coords.
        /// </summary>
        /// <param name="clipCoords">Clip coords.</param>
        /// <returns>The eye coords.</returns>
        private Vector4f ToEyeCoords(Vector4f clipCoords)
        {
            var invertedProjection = Matrix4f.Invert(EngineCore.module.rendererMaster.GetProjectionMatrix(), null);
            var eyeCoords = Matrix4f.Transform(invertedProjection, clipCoords, null);
            return new Vector4f(eyeCoords.x, eyeCoords.y, -1.0f, 0.0f);
        }

        /// <summary>
        /// Gets the normalised device coordinates.
        /// </summary>
        /// <param name="mouseX">Mouse x.</param>
        /// <param name="mouseY">Mouse y.</param>
        /// <returns>The normalised device coordinates.</returns>
        private Vector2f GetNormalisedDeviceCoordinates(float mouseX, float mouseY)
        {
            var x = 2.0f * mouseX / DeviceDisplay.GetWidth() - 1.0f;
            var y = 2.0f * mouseY / DeviceDisplay.GetHeight() - 1.0f;
            return new Vector2f(x, y);
        }

        /// <summary>
        /// Gets the section ID.
        /// </summary>
        /// <param name="ray">The ray.</param>
        /// <returns>The section ID.</returns>
        private int GetSectionID(Vector3f ray)
        {
            for (int i = 0; i < 60.0f; ++i)
            {
                if (IntersectionInRange(i * 2.0f, (i + 1) * 2.0f, ray))
                {
                    return i;
                }
            }

            return 60;
        }

        /// <summary>
        /// Gets the point on ray.
        /// </summary>
        /// <param name="ray">The ray.</param>
        /// <param name="distance">The distance.</param>
        /// <returns>The point on ray.</returns>
        private Vector3f GetPointOnRay(Vector3f ray, float distance)
        {
            var camPos = camera.GetPosition();
            var start = new Vector3f(camPos.x, camPos.y, camPos.z);
            var scaledRay = new Vector3f(ray.x * distance, ray.y * distance, ray.z * distance);
            return Vector3f.Add(start, scaledRay, null);
        }

        /// <summary>
        /// Binaries the search.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="start">The start.</param>
        /// <param name="finish">The finish.</param>
        /// <param name="ray">The ray.</param>
        /// <returns>The search.</returns>
        private Vector3f BinarySearch(int count, float start, float finish, Vector3f ray)
        {
            var half = start + (finish - start) / 2.0f;

            if (count >= RECURSION_COUNT)
            {
                var endPoint = GetPointOnRay(ray, half);
                //var terrain = World.getTerrainTree().get(0); // TODO: NOT JUST 0!!!

                //if (terrain != null) {
                return endPoint;
                //}

                // return null;
            }

            if (IntersectionInRange(start, half, ray))
            {
                return BinarySearch(count + 1, start, half, ray);
            }

            return BinarySearch(count + 1, half, finish, ray);
        }

        /// <summary>
        /// Is there a intersection the in range.
        /// </summary>
        /// <param name="start">Start.</param>
        /// <param name="finish">Finish.</param>
        /// <param name="ray">Ray.</param>
        /// <returns><c>true</c>, if in range was intersectioned, <c>false</c> otherwise.</returns>
        private bool IntersectionInRange(float start, float finish, Vector3f ray)
        {
            var startPoint = GetPointOnRay(ray, start);
            var endPoint = GetPointOnRay(ray, finish);
            return !IsUnderGround(startPoint) && IsUnderGround(endPoint);
        }

        /// <summary>
        /// Determines whether the specified testPoint is under ground.
        /// </summary>
        /// <param name="testPoint">Test point.</param>
        /// <returns><c>true</c> if this instance is under ground the specified testPoint; otherwise, <c>false</c>.</returns>
        private bool IsUnderGround(Vector3f testPoint)
        {
            var height = 0; // World.getTerrainHeight(testPoint.getX(), testPoint.getZ());
            return testPoint.y < height;
        }
    }
}