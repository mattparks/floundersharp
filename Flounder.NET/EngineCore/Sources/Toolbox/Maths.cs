﻿using System;
using Flounder.Toolbox.Matrices;
using Flounder.Toolbox.Vectors;
using System.Runtime.InteropServices;

namespace Flounder.Toolbox
{
    /// <summary>
    /// A class that holds many various math functions.
    /// </summary>
    public static class Maths
    {
        /// <summary>
        /// Degrees in a circle.
        /// </summary>
        public static float DEGREES_IN_CIRCLE = 360;

        /// <summary>
        /// Degrees in a half circle.
        /// </summary>
        public static float DEGREES_IN_HALF_CIRCLE = 180;

        /// <summary>
        /// The log of 1/2 constant.
        /// </summary>
        public static float LOG_HALF = (float) Math.Log(0.5);

        /// <summary>
        /// The length of a float value.
        /// </summary>
        public static int FLOAT_LENGTH = 4;

        /// <summary>
        /// The length of a int value.
        /// </summary>
        public static int INT_LENGTH = 4;

        /// <summary>
        /// The length of a sort value.
        /// </summary>
        public static int SHORT_LENGTH = 2;

        /// <summary>
        /// The length of a long value.
        /// </summary>
        public static int LONG_LENGTH = 8;

        /// <summary>
        /// A random number generator.
        /// </summary>
        public static Random RANDOM = new Random();

        /// <summary>
        /// Converts floats to uints.
        /// </summary>
        /// <param name="f">The float to convert.</param>
        /// <returns>The data in uints.</returns>
        public static unsafe uint FloatToUInt32Bits(float f)
        {
            return *((uint*)&f);
        }

        /// <summary>
        /// Gets the height on a point off of a 3d triangle.
        /// </summary>
        /// <param name="p1">Point 1 on the triangle.</param>
        /// <param name="p2">Point 2 on the triangle..</param>
        /// <param name="p3">Point 3 on the triangle.</param>
        /// <param name="pos">Position.</param>
        /// <returns>The height of the triangle at the position.</returns>
        public static float BaryCentric(Vector3f p1, Vector3f p2, Vector3f p3, Vector2f pos)
        {
            var det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
            var l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
            var l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
            var l3 = 1.0f - l1 - l2;
            return l1 * p1.y + l2 * p2.y + l3 * p3.y;
        }

        /// <summary>
        /// Rotates a vector in a direction by a amount.
        /// </summary>
        /// <param name="direction">The direction to rotate by.</param>
        /// <param name="rotX">Rotation in the x axis.</param>
        /// <param name="rotY">Rotation in the y axis.</param>
        /// <param name="rotZ">Rotation in the z axis.</param>
        /// <returns>Returns a rotated Vector3f.</returns>
        public static Vector3f RotateVector(Vector3f direction, float rotX, float rotY, float rotZ)
        {
            var matrix = CreateTransformationMatrix(new Vector3f(0, 0, 0), new Vector3f(rotX, rotY, rotZ), 1);
            var direction4 = new Vector4f(direction.x, direction.y, direction.z, 1.0f);
            Matrix4f.Transform(matrix, direction4, direction4);
            return new Vector3f(direction4.x, direction4.y, direction4.z);
        }

        /// <summary>
        /// Creates a new transformation matrix for a object in 2d space.
        /// </summary>
        /// <param name="translation">Translation amount the XY.</param>
        /// <param name="scale">How much to scale the matrix.</param>
        /// <returns>The transformation matrix.</returns>
        public static Matrix4f CreateTransformationMatrix(Vector2f translation, float scale)
        {
            var transformationMatrix = new Matrix4f();
            transformationMatrix.SetIdentity();
            Matrix4f.Translate(transformationMatrix, translation, transformationMatrix);
            Matrix4f.Scale(transformationMatrix, new Vector3f(scale, scale, scale), transformationMatrix);
            return transformationMatrix;
        }

        /// <summary>
        /// Creates a new transformation matrix for a object in 3d space.
        /// </summary>
        /// <param name="translation">Translation amount the XYZ.</param>
        /// <param name="rotation">Rotation amount the XYZ.</param>
        /// <param name="scale">How much to scale the matrix.</param>
        /// <returns>The transformation matrix.</returns>
        public static Matrix4f CreateTransformationMatrix(Vector3f translation, Vector3f rotation, float scale)
        {
            var transformationMatrix = new Matrix4f();
            transformationMatrix.SetIdentity();
            Matrix4f.Translate(transformationMatrix, translation, transformationMatrix);
            Matrix4f.Rotate(transformationMatrix, new Vector3f(1, 0, 0), (float)ToRadians(rotation.x), transformationMatrix); // Rotate the X component.
            Matrix4f.Rotate(transformationMatrix, new Vector3f(0, 1, 0), (float)ToRadians(rotation.y), transformationMatrix); // Rotate the Y component.
            Matrix4f.Rotate(transformationMatrix, new Vector3f(0, 0, 1), (float)ToRadians(rotation.z), transformationMatrix); // Rotate the Z component.
            Matrix4f.Scale(transformationMatrix, new Vector3f(scale, scale, scale), transformationMatrix);
            return transformationMatrix;
        }

        /// <summary>
        /// Gets the maximum vector size.
        /// </summary>
        /// <param name="left">The first vector to get values from.</param>
        /// <param name="right">The second vector to get values from.</param>
        /// <returns>The maximum vector.</returns>
        public static Vector3f Max(Vector3f left, Vector3f right)
        {
            return new Vector3f(Math.Max(left.x, right.x), Math.Max(left.y, right.y), Math.Max(left.z, right.z));
        }

        /// <summary>
        /// Gets the maximum value in a vector.
        /// </summary>
        /// <param name="vector">The value to get the maximum value from.</param>
        /// <returns>The maximum value.</returns>
        public static float Max(Vector3f vector)
        {
            return Math.Max(vector.x, Math.Max(vector.y, vector.z));
        }

        /// <summary>
        /// Gets the lowest vector size.
        /// </summary>
        /// <param name="left">The first vector to get values from.</param>
        /// <param name="right">The second vector to get values from.</param>
        /// <returns>The lowest vector.</returns>
        public static Vector3f Min(Vector3f left, Vector3f right)
        {
            return new Vector3f(Math.Min(left.x, right.x), Math.Min(left.y, right.y), Math.Min(left.z, right.z));
        }

        /// <summary>
        /// Gets the lowest value in a vector.
        /// </summary>
        /// <param name="vector">The value to get the maximum value from.</param>
        /// <returns>The lowest value.</returns>
        public static float Min(Vector3f vector)
        {
            return Math.Min(vector.x, Math.Min(vector.y, vector.z));
        }

        /// <summary>
        /// Gets the distance between two points squared.
        /// </summary>
        /// <param name="point1">The first point.</param>
        /// <param name="point2">The second point.</param>
        /// <returns>The squared distance between the two points.</returns>
        public static float GetDistanceSquared(Vector3f point1, Vector3f point2)
        {
            return (float)(Math.Pow(point1.x - point2.x, 2) + Math.Pow(point1.y - point2.y, 2) + Math.Pow(point1.z - point2.z, 2));
        }

        /// <summary>
        /// Gets the total between two points squared.
        /// </summary>
        /// <param name="point1">The first point.</param>
        /// <param name="point2">The second point.</param>
        /// <returns>The total distance between the two points.</returns>
        public static float GetDistance(Vector3f point1, Vector3f point2)
        {
            return (float)Math.Sqrt(GetDistanceSquared(point1, point2));
        }

        /// <summary>
        /// Gets the vector distance between 2 vectors.
        /// </summary>
        /// <param name="point1">The first point.</param>
        /// <param name="point2">The second point.</param>
        /// <returns>The vector distance between the points.</returns>
        public static Vector3f GetVectorDistance(Vector3f point1, Vector3f point2)
        {
            return new Vector3f((float)Math.Pow(point1.x - point2.x, 2), (float)Math.Pow(point1.y - point2.y, 2), (float)Math.Pow(point1.z - point2.z, 2));
        }

        /// <summary>
        /// Interpolates two values by a blendFactor using cos interpolation.
        /// </summary>
        /// <param name="left">The first value.</param>
        /// <param name="right">The second value.</param>
        /// <param name="blend">The blend value.</param>
        /// <returns>Returns a interpolated value.</returns>
        public static float CosInterpolate(float left, float right, float blend)
        {
            var f = (float)((1f - Math.Cos(blend * Math.PI)) * 0.5f);
            return left * (1 - f) + right * f;
        }

        /// <summary>
        /// Converts degrees to radians.
        /// </summary>
        /// <param name="degrees">The value in degrees</param>
        /// <returns>A value in radians.</returns>
        public static float ToRadians(float degrees)
        {
            return degrees * (float)(Math.PI / 180.0d);
        }

        /// <summary>
        /// Converts radians to degrees.
        /// </summary>
        /// <param name="radians">The value in radians</param>
        /// <returns>A value in degrees.</returns>
        public static float ToDegrees(float radians)
        {
            return radians * (float)(180.0d / Math.PI);
        }

        /// <summary>
        /// A flooring modulus operator. Works similarly to the % operator, except this rounds towards negative infinity rather than towards 0.
        /// For example:
        /// -7 % 3 = -2; floorMod(-7, 3) = 2 
        /// -6 % 3 = -0; floorMod(-6, 3) = 0 
        /// -5 % 3 = -2; floorMod(-5, 3) = 1 
        /// -4 % 3 = -1; floorMod(-4, 3) = 2 
        /// -3 % 3 = -0; floorMod(-3, 3) = 0 
        /// -2 % 3 = -2; floorMod(-2, 3) = 1 
        /// -1 % 3 = -1; floorMod(-1, 3) = 2 
        /// 0 % 3 = 0; floorMod(0, 3) = 0 
        /// 1 % 3 = 1; floorMod(1, 3) = 1 
        /// 2 % 3 = 2; floorMod(2, 3) = 2 
        /// 3 % 3 = 0; floorMod(3, 3) = 0 
        /// 4 % 3 = 1; floorMod(4, 3) = 1 
        /// </p>
        /// </summary>
        /// <param name="numerator">The numerator of the modulus operator.</param>
        /// <param name="denominator">The denominator of the modulus operator.</param>
        /// <returns>Numerator % denominator, rounded towards negative infinity.</returns>
        public static int FloorMod(int numerator, int denominator)
        {
            if (denominator < 0)
            {
                throw new ArgumentException("FloorMod does not support negative denominators!");
            }

            if (numerator > 0)
            {
                return numerator % denominator;
            }
            else 
            {
                var mod = (-numerator) % denominator;

                if (mod != 0)
                {
                    mod = denominator - mod;
                }

                return mod;
            }
        }

        /// <summary>
        /// Floors the mod.
        /// For example:
        /// -7 % 3 = -2; floorMod(-7, 3) = 2 
        /// -6 % 3 = -0; floorMod(-6, 3) = 0 
        /// -5 % 3 = -2; floorMod(-5, 3) = 1 
        /// -4 % 3 = -1; floorMod(-4, 3) = 2 
        /// -3 % 3 = -0; floorMod(-3, 3) = 0 
        /// -2 % 3 = -2; floorMod(-2, 3) = 1 
        /// -1 % 3 = -1; floorMod(-1, 3) = 2 
        /// 0 % 3 = 0; floorMod(0, 3) = 0 
        /// 1 % 3 = 1; floorMod(1, 3) = 1 
        /// 2 % 3 = 2; floorMod(2, 3) = 2 
        /// 3 % 3 = 0; floorMod(3, 3) = 0
        /// 4 % 3 = 1; floorMod(4, 3) = 1
        /// </p>
        /// </summary>
        /// <param name="numerator">The numerator of the modulus operator.</param>
        /// <param name="denominator">The denominator of the modulus operator.</param>
        /// <returns>Numerator % denominator, rounded towards negative infinity.</returns>
        public static double FloorMod(double numerator, double denominator)
        {
            if (denominator < 0)
            {
                throw new ArgumentException("FloorMod does not support negative denominators!");
            }

            if (numerator > 0)
            {
                return numerator % denominator;
            }
            else 
            {
                var mod = (-numerator) % denominator;

                if (mod != 0)
                {
                    mod = denominator - mod;
                }

                return mod;
            }
        }

        /// <summary>
        /// Normalizes a angle into the range of 0-360.
        /// </summary>
        /// <param name="angle">The source angle.</param>
        /// <returns>The normalized angle.</returns>
        public static float NormalizeAngle(float angle)
        {
            if (angle >= 360)
            {
                return angle - 360;
            }
            else if (angle < 0)
            {
                return angle + 360;
            }

            return angle;
        }

        /// <summary>
        /// Used to floor the value if less than the min.
        /// </summary>
        /// <param name="min">The minimum value.</param>
        /// <param name="value">The value.</param>
        /// <returns>A value with deadband applied.</returns>
        public static double Deadband(double min, double value)
        {
            return Math.Abs(value) >= Math.Abs(min) ? value : 0;
        }

        /// <summary>
        /// Ensures value is in the range of min to max. If value is greater than max, this will return max. If value is less than min, this will return min. Otherwise, value is returned unchanged.
        /// </summary>
        /// <param name="value">The value to clamp.</param>
        /// <param name="min">The smallest value of the result.</param>
        /// <param name="max">The largest value of the result.</param>
        /// <returns>A limited clamped between min and max.</returns>
        public static double Clamp(double value, double min, double max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }

        /// <summary>
        /// Limit the specified value by a limit.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="limit">The limit.</param>
        /// <returns>A limited value.</returns>
        public static float Limit(float value, float limit)
        {
            return value > limit ? limit : value;
        }
    }
}
