﻿using Flounder.Basics.Options;
using OpenGL;

namespace Flounder.Toolbox
{
    /// <summary>
    /// Runs basic OpenGL rendering functions.
    /// </summary>
    public static class OpenglUtils
    { 
        private static bool cullingBackFace = false;
        private static bool inWireframe = false;
        private static bool isAlphaBlending = false;
        private static bool additiveBlending = false;
        private static bool antialiasing = false;
        
        public static void PrepareNewRenderParse(Colour colour)
        {
            GL10.glClearColor(colour.r, colour.g, colour.b, 1);
            GL10.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
            DisableBlending();
            CullBackFaces(true);
            EnableDepthTesting();
        }

        public static void DisableBlending()
        {
            if (isAlphaBlending || additiveBlending)
            {
                GL10.glDisable(GL11.GL_BLEND);
                isAlphaBlending = false;
                additiveBlending = false;
            }
        }

        public static void CullBackFaces(bool cull)
        {
            if (cull && !cullingBackFace)
            {
                GL10.glEnable(GL11.GL_CULL_FACE);
                GL10.glCullFace(GL11.GL_BACK);
                cullingBackFace = true;
            }
            else if (!cull && cullingBackFace)
            {
                GL10.glDisable(GL11.GL_CULL_FACE);
                cullingBackFace = false;
            }
        }

        public static void EnableDepthTesting()
        {
            GL10.glEnable(GL11.GL_DEPTH_TEST);
        }

        public static bool IsInWireframe()
        {
            return inWireframe;
        }

        public static void GoWireframe(bool goWireframe)
        {
            if (goWireframe && !inWireframe)
            {
                GL10.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
                inWireframe = true;
            }
            else if (!goWireframe && inWireframe)
            {
                GL10.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
                inWireframe = false;
            }
        }

        public static void EnableAlphaBlending()
        {
            if (!isAlphaBlending)
            {
                GL10.glEnable(GL11.GL_BLEND);
                GL10.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
                isAlphaBlending = true;
                additiveBlending = false;
            }
        }

        public static void EnableAdditiveBlending()
        {
            if (!additiveBlending)
            {
                GL10.glEnable(GL11.GL_BLEND);
                GL10.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
                additiveBlending = true;
                isAlphaBlending = false;
            }
        }

        public static void Antialias(bool enable)
        {
            if (!OptionsGraphics.ANTI_ALIASING)
            {
                return;
            }

            if (enable && !antialiasing)
            {
                GL10.glEnable(GL13.GL_MULTISAMPLE);
                antialiasing = true;
            }
            else if (!enable && antialiasing)
            {
                GL10.glDisable(GL13.GL_MULTISAMPLE);
                antialiasing = false;
            }
        }

        public static void DisableDepthTesting()
        {
            GL10.glDisable(GL11.GL_DEPTH_TEST);
        }

        public static void BindVAO(uint vaoID, uint[] attributes)
        {
            GL30.glBindVertexArray(vaoID);

            foreach (var i in attributes)
            {
                GL20.glEnableVertexAttribArray(i);
            }
        }

        public static void UnbindVAO(uint[] attributes)
        {
            foreach (var i in attributes)
            {
                GL20.glDisableVertexAttribArray(i);
            }

            GL30.glBindVertexArray(0);
        }

        public static void BindTextureToBank(uint textureID, uint bankID)
        {
            GL13.glActiveTexture(GL13.GL_TEXTURE0 + bankID);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID);
        }

        public static void BindTextureToBank(uint textureID, uint bankID, int lodBias)
        {
            GL13.glActiveTexture(GL13.GL_TEXTURE0 + bankID);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID);
            GL10.glTexParameteri(GL11.GL_TEXTURE_2D, GL12.GL_TEXTURE_BASE_LEVEL, lodBias);
            GL13.glActiveTexture(0);
        }
    }
}
