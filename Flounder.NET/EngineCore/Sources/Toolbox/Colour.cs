﻿using System;
using Flounder.Toolbox.Vectors;

namespace Flounder.Toolbox
{
    /// <summary>
    /// Represents a 4 component colour.
    /// </summary>
    public class Colour
    {
        /// <summary>
        /// Gets or sets the red.
        /// </summary>
        /// <value>The red.</value>
        public float r { get; set; }

        /// <summary>
        /// Gets or sets the green.
        /// </summary>
        /// <value>The green.</value>
        public float g { get; set; }

        /// <summary>
        /// Gets or sets the blue.
        /// </summary>
        /// <value>The blue.</value>
        public float b { get; set; }

        /// <summary>
        /// Gets or sets alpha.
        /// </summary>
        /// <value>The alpha.</value>
        public float a { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Colour"/> class.
        /// </summary>
        public Colour()
        {
            Set(0, 0, 0, 1);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Colour"/> class.
        /// </summary>
        /// <param name="source">Source colour.</param>
        public Colour(Colour source)
        {
            Set(source);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Colour"/> class.
        /// </summary>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        public Colour(float r, float g, float b)
        {
            Set(r, g, b, 1);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Colour"/> class.
        /// </summary>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="a">The alpha component.</param>
        public Colour(float r, float g, float b, float a)
        {
            Set(r, g, b, a);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Colour"/> class.
        /// </summary>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="convert">If set to <c>true</c> to convert from 255 ranged colours.</param>
        public Colour(float r, float g, float b, bool convert)
        {
            Set(r, g, b, convert);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Toolbox.Colour"/> is reclaimed by garbage collection.
        /// </summary>
        ~Colour()
        {
        }

        public static Colour Add(Colour left, Colour right, Colour destination)
        {
            if (destination == null)
            {
                destination = new Colour();
            }

            destination.Set(left.r + right.r, left.g + right.g, left.b + right.b, left.a + right.a);
            return destination;
        }

        public static Colour Subtract(Colour left, Colour right, Colour destination)
        {
            if (destination == null)
            {
                destination = new Colour();
            }

            destination.Set(left.r - right.r, left.g - right.g, left.b - right.b, left.a - right.a);
            return destination;
        }

        public static Colour Multiply(Colour left, Colour right, Colour destination)
        {
            if (destination == null)
            {
                return new Colour(left.r * right.r, left.g * right.g, left.b * right.b);
            }
            else 
            {
                destination.r = left.r * right.r;
                destination.g = left.g * right.g;
                destination.b = left.b * right.b;
                return destination;
            }
        }

        public void Set(Colour source)
        {
            Set(source.r, source.g, source.b, source.a);
        }

        public void Set(float r, float g, float b, float a)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        public void Set(float r, float g, float b, bool convert)
        {
            if (convert)
            {
                this.r = r / 255f;
                this.g = g / 255f;
                this.b = b / 255f;
                a = 1;
            }
            else 
            {
                this.r = r;
                this.g = g;
                this.b = b;
                a = 1;
            }
        }

        public Colour Scale(float value)
        {
            r *= value;
            g *= value;
            b *= value;
            return this;
        }

        public float LengthSquared()
        {
            return r * r + g * g + b * b;
        }

        public float Length()
        {
            return (float)Math.Sqrt(LengthSquared());
        }

        public Colour GetUnit()
        {
            var colour = new Colour();

            if (r == 0 && g == 0 && b == 0)
            {
                return colour;
            }

            colour.Set(this);
            colour.Scale(1f / Length());
            return colour;
        }
        
        /// <summary>
        /// Gets a equivalent but in Vector3f form.
        /// </summary>
        /// <returns>A equivalent but in Vector3f form.</returns>
        public Vector3f ToVector()
        {
            return new Vector3f(r, g, b);
        }
        
        public override string ToString()
        {
            return "(" + r + ", " + g + ", " + b + ")";
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object o)
        {
            if (this == o)
            {
                return true;
            }

            if (o == null)
            {
                return false;
            }

            if (!GetType().Equals(o.GetType()))
            {
                return false;
            }

            var other = (Colour)o;

            return r == other.r && g == other.g && b == other.b && a == other.a;
        }
    }
}
