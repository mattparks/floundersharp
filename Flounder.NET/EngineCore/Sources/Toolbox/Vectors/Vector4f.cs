﻿using System;

namespace Flounder.Toolbox.Vectors
{
    /// <summary>
    /// Holds a 4-tuple vector.
    /// </summary>
    public class Vector4f
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
        public float w { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Vectors.Vector4f"/> class.
        /// </summary>
        public Vector4f()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Vectors.Vector4f"/> class.
        /// </summary>
        /// <param name="source">The vector source.</param>
        public Vector4f(Vector4f source)
        {
            Set(source);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Vectors.Vector4f"/> class.
        /// </summary>
        /// <param name="x">The x start.</param>
        /// <param name="y">The y start.</param>
        /// <param name="z">The z start.</param>
        /// <param name="w">The w start.</param>
        public Vector4f(float x, float y, float z, float w)
        {
            Set(x, y, z, w);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Toolbox.Vector.Vector4f"/> is reclaimed by garbage collection.
        /// </summary>
        ~Vector4f()
        {
        }

        /**
         * Adds two vectors together and places the result in the destination vector.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public static Vector4f Add(Vector4f left, Vector4f right, Vector4f destination)
        {
            if (destination == null)
            {
                destination = new Vector4f();
            }

            destination.Set(left.x + right.x, left.y + right.y, left.z + right.z, left.w + right.w);
            return destination;
        }

        /**
         * Subtracts two vectors from each other and places the result in the destination vector.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public static Vector4f Subtract(Vector4f left, Vector4f right, Vector4f destination)
        {
            if (destination == null)
            {
                destination = new Vector4f();
            }

            destination.Set(left.x - right.x, left.y - right.y, left.z - right.z, left.w - right.w);
            return destination;
        }

        /**
         * Multiplies two vectors together and places the result in the destination vector.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public static Vector4f Multiply(Vector4f left, Vector4f right, Vector4f destination)
        {
            if (destination == null)
            {
                destination = new Vector4f();
            }

            destination.Set(left.x * right.x, left.y * right.y, left.z * right.z, left.w * right.w);
            return destination;
        }

        /**
         * Divides two vectors together and places the result in the destination vector.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public static Vector4f Divide(Vector4f left, Vector4f right, Vector4f destination)
        {
            if (destination == null)
            {
                destination = new Vector4f();
            }

            destination.Set(left.x / right.x, left.y / right.y, left.z / right.z, left.w / right.w);
            return destination;
        }

        /**
         * Calculates the angle between two vectors.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         *
         * @return The angle between the two vectors, in radians.
         */
        public static float Angle(Vector4f left, Vector4f right)
        {
            float dls = Dot(left, right) / (left.Length() * right.Length());

            if (dls < -1f)
            {
                dls = -1f;
            }
            else if (dls > 1.0f)
            {
                dls = 1.0f;
            }

            return (float)Math.Acos(dls);
        }

        /**
         * The dot product of two vectors is calculated as v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.w * v2.w
         *
         * @param left The left source vector.
         * @param right The right source vector.
         *
         * @return Left dot right.
         */
        public static float Dot(Vector4f left, Vector4f right)
        {
            return left.x * right.x + left.y * right.y + left.z * right.z + left.w * right.w;
        }

        /**
         * Loads from another Vector4f.
         *
         * @param source The source vector.
         *
         * @return this.
         */
        public Vector4f Set(Vector4f source)
        {
            x = source.x;
            y = source.y;
            z = source.z;
            w = source.w;
            return this;
        }

        /**
         * Sets values in the vector.
         *
         * @param x The new X value.
         * @param y The new Y value.
         */
        public void Set(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        /**
         * Sets values in the vector.
         *
         * @param x The new X value.
         * @param y The new Y value.
         * @param z The new z value.
         */
        public void Set(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /**
         * Sets values in the vector.
         *
         * @param x The new X value.
         * @param y The new Y value.
         * @param z The new z value.
         * @param w The new w value.
         */
        public void Set(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        /**
         * Translates this vector.
         *
         * @param x The translation in x.
         * @param y the translation in y.
         * @param z the translation in z.
         * @param w the translation in w.
         *
         * @return this.
         */
        public Vector4f Translate(float x, float y, float z, float w)
        {
            this.x += x;
            this.y += y;
            this.z += z;
            this.w += w;
            return this;
        }

        /**
         * Negates this vector and places the result in a destination vector.
         *
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public Vector4f Negate(Vector4f destination)
        {
            if (destination == null)
            {
                destination = new Vector4f();
            }

            destination.x = -x;
            destination.y = -y;
            destination.z = -z;
            destination.w = -w;
            return destination;
        }

        /**
         * normalizes this vector and places the result in a destination vector.
         *
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public Vector4f Normalize(Vector4f destination)
        {
            if (destination == null)
            {
                destination = new Vector4f();
            }

            float l = Length();

            destination.Set(x / l, y / l, z / l, w / l);
            return destination;
        }

        /**
         * Negates this vector.
         *
         * @return this.
         */
        public Vector4f Negate()
        {
            x = -x;
            y = -y;
            z = -z;
            w = -w;
            return this;
        }

        /**
         * @return The length of the vector.
         */
        public float Length()
        {
            return (float)Math.Sqrt(LengthSquared());
        }

        /**
         * normalizes this vector.
         *
         * @return this.
         */
        public Vector4f Normalize()
        {
            float len = Length();

            if (len != 0.0f)
            {
                float l = 1.0f / len;
                return Scale(l);
            }
            else
            {
                throw new InvalidOperationException("Zero length vector");
            }
        }

        /**
         * Scales this vector.
         *
         * @param scale The scale factor.
         *
         * @return this.
         */
        public Vector4f Scale(float scale)
        {
            x *= scale;
            y *= scale;
            z *= scale;
            w *= scale;
            return this;
        }

        /**
         * @return The length squared of the vector.
         */
        public float LengthSquared()
        {
            return x * x + y * y + z * z + w * w;
        }

        public override bool Equals(object o)
        {
            if (this == o)
            {
                return true;
            }

            if (o == null)
            {
                return false;
            }

            if (!GetType().Equals(o.GetType()))
            {
                return false;
            }

            Vector4f other = (Vector4f)o;

            return x == other.x && y == other.y && z == other.z && w == other.w;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "Vector4f[" + x + ", " + y + ", " + z + ", " + w + "]";
        }
    }
}
