﻿using System;

namespace Flounder.Toolbox.Vectors
{
    /// <summary>
    /// Holds a 3-tuple vector.
    /// </summary>
    public class Vector3f
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Vectors.Vector3f"/> class.
        /// </summary>
        public Vector3f()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Vectors.Vector3f"/> class.
        /// </summary>
        /// <param name="source">The vector 3 source.</param>
        public Vector3f(Vector3f source)
        {
            Set(source);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Vectors.Vector3f"/> class.
        /// </summary>
        /// <param name="source">The vector source.</param>
        public Vector3f(Vector4f source)
        {
            Set(new Vector3f(source.x, source.y, source.z));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Vectors.Vector3f"/> class.
        /// </summary>
        /// <param name="x">The x start.</param>
        /// <param name="y">The y start.</param>
        /// <param name="z">The z start.</param>
        public Vector3f(float x, float y, float z)
        {
            Set(x, y, z);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Toolbox.Vector.Vector3f"/> is reclaimed by garbage collection.
        /// </summary>
        ~Vector3f()
        {
        }

        /**
         * Adds two vectors together and places the result in the destination vector.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public static Vector3f Add(Vector3f left, Vector3f right, Vector3f destination)
        {
            if (destination == null)
            {
                destination = new Vector3f();
            }

            destination.Set(left.x + right.x, left.y + right.y, left.z + right.z);
            return destination;
        }

        /**
         * Subtracts two vectors from each other and places the result in the destination vector.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public static Vector3f Subtract(Vector3f left, Vector3f right, Vector3f destination)
        {
            if (destination == null)
            {
                destination = new Vector3f();
            }

            destination.Set(left.x - right.x, left.y - right.y, left.z - right.z);
            return destination;
        }

        /**
         * Multiplies two vectors together and places the result in the destination vector.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public static Vector3f Multiply(Vector3f left, Vector3f right, Vector3f destination)
        {
            if (destination == null)
            {
                destination = new Vector3f();
            }

            destination.Set(left.x * right.x, left.y * right.y, left.z * right.z);
            return destination;
        }

        /**
         * Divides two vectors together and places the result in the destination vector.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public static Vector3f Divide(Vector3f left, Vector3f right, Vector3f destination)
        {
            if (destination == null)
            {
                destination = new Vector3f();
            }

            destination.Set(left.x / right.x, left.y / right.y, left.z / right.z);
            return destination;
        }

        /**
         * Calculates the angle between two vectors.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         *
         * @return The angle between the two vectors, in radians.
         */
        public static float Angle(Vector3f left, Vector3f right)
        {
            float dls = Dot(left, right) / (left.Length() * right.Length());

            if (dls < -1f)
            {
                dls = -1f;
            }
            else if (dls > 1.0f)
            {
                dls = 1.0f;
            }

            return (float)Math.Acos(dls);
        }

        /**
         * The dot product of two vectors is calculated as v1.x * v2.x + v1.y * v2.y + v1.z * v2.z
         *
         * @param left The left source vector.
         * @param right The right source vector.
         *
         * @return Left dot right.
         */
        public static float Dot(Vector3f left, Vector3f right)
        {
            return left.x * right.x + left.y * right.y + left.z * right.z;
        }

        /**
         * The cross product of two vectors.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination matrix.
         */
        public static Vector3f Cross(Vector3f left, Vector3f right, Vector3f destination)
        {
            if (destination == null)
            {
                destination = new Vector3f();
            }

            destination.Set(left.y * right.z - left.z * right.y, right.x * left.z - right.z * left.x, left.x * right.y - left.y * right.x);
            return destination;
        }

        /**
         * Loads from another Vector3f.
         *
         * @param source The source vector.
         *
         * @return this.
         */
        public Vector3f Set(Vector3f source)
        {
            x = source.x;
            y = source.y;
            z = source.z;
            return this;
        }


        /**
         * Sets values in the vector.
         *
         * @param x The new X value.
         * @param y The new Y value.
         */
        public void Set(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        /**
         * Sets values in the vector.
         *
         * @param x The new X value.
         * @param y The new Y value.
         * @param z The new z value.
         */
        public void Set(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /**
         * Translates this vector.
         *
         * @param x The translation in x.
         * @param y the translation in y.
         * @param z the translation in z.
         *
         * @return this.
         */
        public Vector3f Translate(float x, float y, float z)
        {
            this.x += x;
            this.y += y;
            this.z += z;
            return this;
        }

        /**
         * Negates this vector and places the result in a destination vector.
         *
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public Vector3f Negate(Vector3f destination)
        {
            if (destination == null)
            {
                destination = new Vector3f();
            }

            destination.x = -x;
            destination.y = -y;
            destination.z = -z;
            return destination;
        }

        /**
         * normalizes this vector and places the result in a destination vector.
         *
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public Vector3f Normalize(Vector3f destination)
        {
            float l = Length();

            if (destination == null)
            {
                destination = new Vector3f();
            }

            destination.Set(x / l, y / l, z / l);
            return destination;
        }

        /**
         * Negates this vector.
         *
         * @return this.
         */
        public Vector3f Negate()
        {
            x = -x;
            y = -y;
            z = -z;
            return this;
        }

        /// <summary>
        /// Gets the length of the vector.
        /// </summary>
        /// <returns>The length of the vector.</returns>
        public float Length()
        {
            return (float)Math.Sqrt(LengthSquared());
        }

        /// <summary>
        /// Normalizes this vector.
        /// </summary>
        /// <returns>this</returns>
        public Vector3f Normalize()
        {
            float len = Length();

            if (len != 0.0f)
            {
                float l = 1.0f / len;
                return Scale(l);
            }
            else
            {
                throw new InvalidOperationException("Zero length vector");
            }
        }

        /// <summary>
        /// Scales this vector.
        /// </summary>
        /// <param name="scale">The scale facto.</param>
        /// <returns>this</returns>
        public Vector3f Scale(float scale)
        {
            x *= scale;
            y *= scale;
            z *= scale;
            return this;
        }

        /// <summary>
        /// Gets the length squared of the vector.
        /// </summary>
        /// <returns>The length squared of the vector.</returns>
        public float LengthSquared()
        {
            return x * x + y * y + z * z;
        }

        public override bool Equals(object o)
        {
            if (this == o)
            {
                return true;
            }

            if (o == null)
            {
                return false;
            }

            if (!GetType().Equals(o.GetType()))
            {
                return false;
            }

            Vector3f other = (Vector3f)o;

            return x == other.x && y == other.y && z == other.z;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "Vector3f[" + x + ", " + y + ", " + z + "]";
        }
    }
}
