﻿using System;

namespace Flounder.Toolbox.Vectors
{
    /// <summary>
    /// Holds a 2-tuple vector.
    /// </summary>
    public class Vector2f
    {
        public float x { get; set; }
        public float y { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Vectors.Vector2f"/> class.
        /// </summary>
        public Vector2f()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Vectors.Vector2f"/> class.
        /// </summary>
        /// <param name="source">The vector source.</param>
        public Vector2f(Vector2f source)
        {
            Set(source);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Vectors.Vector2f"/> class.
        /// </summary>
        /// <param name="x">The x start.</param>
        /// <param name="y">The y start.</param>
        public Vector2f(float x, float y)
        {
            Set(x, y);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Toolbox.Vector.Vector2f"/> is reclaimed by garbage collection.
        /// </summary>
        ~Vector2f()
        {
        }

        /**
         * Adds two vectors together and places the result in the destination vector.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public static Vector2f Add(Vector2f left, Vector2f right, Vector2f destination)
        {
            if (destination == null)
            {
                destination = new Vector2f();
            }

            destination.Set(left.x + right.x, left.y + right.y);
            return destination;
        }

        /**
         * Subtracts two vectors from each other and places the result in the destination vector.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public static Vector2f Subtract(Vector2f left, Vector2f right, Vector2f destination)
        {
            if (destination == null)
            {
                destination = new Vector2f();
            }

            destination.Set(left.x - right.x, left.y - right.y);
            return destination;
        }

        /**
         * Multiplies two vectors together and places the result in the destination vector.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public static Vector2f Multiply(Vector2f left, Vector2f right, Vector2f destination)
        {
            if (destination == null)
            {
                destination = new Vector2f();
            }

            destination.Set(left.x * right.x, left.y * right.y);
            return destination;
        }

        /**
         * Divides two vectors together and places the result in the destination vector.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public static Vector2f Divide(Vector2f left, Vector2f right, Vector2f destination)
        {
            if (destination == null)
            {
                destination = new Vector2f();
            }

            destination.Set(left.x / right.x, left.y / right.y);
            return destination;
        }

        /**
         * Calculates the angle between two vectors.
         *
         * @param left The left source vector.
         * @param right The right source vector.
         *
         * @return The angle between the two vectors, in radians.
         */
        public static float Angle(Vector2f left, Vector2f right)
        {
            float dls = Dot(left, right) / (left.Length() * right.Length());

            if (dls < -1f)
            {
                dls = -1f;
            }
            else if (dls > 1.0f)
            {
                dls = 1.0f;
            }

            return (float)Math.Acos(dls);
        }

        /**
         * The dot product of two vectors is calculated as v1.x * v2.x + v1.y * v2.y
         *
         * @param left The left source vector.
         * @param right The right source vector.
         *
         * @return Left dot right.
         */
        public static float Dot(Vector2f left, Vector2f right)
        {
            return left.x * right.x + left.y * right.y;
        }

        /**
         * Loads from another Vector2f.
         *
         * @param source The source vector.
         *
         * @return this.
         */
        public Vector2f Set(Vector2f source)
        {
            x = source.x;
            y = source.y;
            return this;
        }

        /**
         * Sets values in the vector.
         *
         * @param x The new X value.
         * @param y The new Y value.
         */
        public void Set(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        /**
         * Translates this vector.
         *
         * @param x The translation in x.
         * @param y the translation in y.
         *
         * @return this.
         */
        public Vector2f Translate(float x, float y)
        {
            this.x += x;
            this.y += y;
            return this;
        }

        /**
         * Negates this vector and places the result in a destination vector.
         *
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public Vector2f Negate(Vector2f destination)
        {
            if (destination == null)
            {
                destination = new Vector2f();
            }

            destination.x = -x;
            destination.y = -y;
            return destination;
        }

        /**
         * Normalises this vector and places the result in a destination vector.
         *
         * @param destination The destination vector or null if a new vector is to be created.
         *
         * @return The destination vector.
         */
        public Vector2f Normalize(Vector2f destination)
        {
            if (destination == null)
            {
                destination = new Vector2f();
            }

            float l = Length();

            destination.Set(x / l, y / l);
            return destination;
        }

        /**
         * Negates this vector.
         *
         * @return this.
         */
        public Vector2f Negate()
        {
            x = -x;
            y = -y;
            return this;
        }

        /**
         * @return The length of the vector.
         */
        public float Length()
        {
            return (float)Math.Sqrt(LengthSquared());
        }

        /**
         * Normalises this vector.
         *
         * @return this.
         */
        public Vector2f Normalise()
        {
            float len = Length();

            if (len != 0.0f)
            {
                float l = 1.0f / len;
                return Scale(l);
            }
            else {
                throw new InvalidOperationException("Zero length vector");
            }
        }

        /**
         * Scales this vector.
         *
         * @param scale The scale factor.
         *
         * @return this.
         */
        public Vector2f Scale(float scale)
        {
            x *= scale;
            y *= scale;
            return this;
        }

        /**
         * @return The length squared of the vector.
         */
        public float LengthSquared()
        {
            return x * x + y * y;
        }

        public override bool Equals(object o)
        {
            if (this == o)
            {
                return true;
            }

            if (o == null)
            {
                return false;
            }

            if (!GetType().Equals(o.GetType()))
            {
                return false;
            }

            Vector2f other = (Vector2f)o;

            return x == other.x && y == other.y;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "Vector2f[" + x + ", " + y + "]";
        }
    }
}
