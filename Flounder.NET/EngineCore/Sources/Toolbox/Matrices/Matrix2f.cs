﻿using System;
using Flounder.Toolbox.Vectors;

namespace Flounder.Toolbox.Matrices
{
    /// <summary>
    /// Holds a 2x2 matrix.
    /// </summary>
    public class Matrix2f
    {
        public float m00 { get; set; }
        public float m01 { get; set; }
        public float m10 { get; set; }
        public float m11 { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Matrices.Matrix2f"/> class.
        /// </summary>
        public Matrix2f()
        {
            SetIdentity();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Matrices.Matrix2f"/> class.
        /// </summary>
        /// <param name="source">The matrix source.</param>
        public Matrix2f(Matrix2f source)
        {
            Load(source, this);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Toolbox.Matrix.Matrix2f"/> is reclaimed by garbage collection.
        /// </summary>
        ~Matrix2f()
        {
        }

        /**
         * Set the source matrix to be the identity matrix.
         *
         * @param source The matrix to set to the identity.
         *
         * @return The source matrix.
         */
        public static Matrix2f SetIdentity(Matrix2f source)
        {
            source.m00 = 1.0f;
            source.m01 = 0.0f;
            source.m10 = 0.0f;
            source.m11 = 1.0f;
            return source;
        }

        /**
         * Inverts the source matrix and puts the result in the destination matrix.
         *
         * @param source The source matrix to be inverted.
         * @param destination The destination matrix, or null if a new one is to be created.
         *
         * @return The inverted matrix, or null if source can't be reverted.
         */
        public static Matrix2f Invert(Matrix2f source, Matrix2f destination)
        {
            float determinant = source.Determinant();

            if (determinant != 0)
            {
                if (destination == null)
                {
                    destination = new Matrix2f();
                }

                float determinant_inv = 1f / determinant;
                float t00 = source.m11 * determinant_inv;
                float t01 = -source.m01 * determinant_inv;
                float t11 = source.m00 * determinant_inv;
                float t10 = -source.m10 * determinant_inv;

                destination.m00 = t00;
                destination.m01 = t01;
                destination.m10 = t10;
                destination.m11 = t11;
                return destination;
            }
            else {
                return null;
            }
        }

        /**
         * Transpose the source matrix and places the result in the destination matrix.
         *
         * @param source The source matrix.
         * @param destination The destination matrix or null if a new matrix is to be created.
         *
         * @return The transposed matrix.
         */
        public static Matrix2f Transpose(Matrix2f source, Matrix2f destination)
        {
            if (destination == null)
            {
                destination = new Matrix2f();
            }

            float m01 = source.m10;
            float m10 = source.m01;

            destination.m01 = m01;
            destination.m10 = m10;
            return destination;
        }

        /**
         * Sets the source matrix to 0.
         *
         * @param source The matrix to be set to 0.
         *
         * @return The matrix set to zero.
         */
        public static Matrix2f SetZero(Matrix2f source)
        {
            source.m00 = 0.0f;
            source.m01 = 0.0f;
            source.m10 = 0.0f;
            source.m11 = 0.0f;
            return source;
        }

        /**
         * Negates the source matrix and places the result in the destination matrix.
         *
         * @param source The source matrix.
         * @param destination The destination matrix or null if a new matrix is to be created.
         *
         * @return The negated matrix.
         */
        public static Matrix2f Negate(Matrix2f source, Matrix2f destination)
        {
            if (destination == null)
            {
                destination = new Matrix2f();
            }

            destination.m00 = -source.m00;
            destination.m01 = -source.m01;
            destination.m10 = -source.m10;
            destination.m11 = -source.m11;
            return destination;
        }

        /**
         * Copies the source matrix and places the result in the destination matrix.
         *
         * @param source The source matrix.
         * @param destination The destination matrix or null if a new matrix is to be created.
         *
         * @return The copied matrix.
         */
        public static Matrix2f Load(Matrix2f source, Matrix2f destination)
        {
            if (destination == null)
            {
                destination = new Matrix2f();
            }

            destination.m00 = source.m00;
            destination.m01 = source.m01;
            destination.m10 = source.m10;
            destination.m11 = source.m11;
            return destination;
        }

        /**
         * Adds two matrices together and places the result in the destination matrix.
         *
         * @param left The left source matrix.
         * @param right The right source matrix.
         * @param destination The destination matrix or null if a new matrix is to be created.
         *
         * @return The destination matrix.
         */
        public static Matrix2f Add(Matrix2f left, Matrix2f right, Matrix2f destination)
        {
            if (destination == null)
            {
                destination = new Matrix2f();
            }

            destination.m00 = left.m00 + right.m00;
            destination.m01 = left.m01 + right.m01;
            destination.m10 = left.m10 + right.m10;
            destination.m11 = left.m11 + right.m11;
            return destination;
        }

        /**
         * Subtracts two matrices together and places the result in the destination matrix.
         *
         * @param left The left source matrix.
         * @param right The right source matrix.
         * @param destination The destination matrix or null if a new matrix is to be created.
         *
         * @return The destination matrix.
         */
        public static Matrix2f Subtract(Matrix2f left, Matrix2f right, Matrix2f destination)
        {
            if (destination == null)
            {
                destination = new Matrix2f();
            }

            destination.m00 = left.m00 - right.m00;
            destination.m01 = left.m01 - right.m01;
            destination.m10 = left.m10 - right.m10;
            destination.m11 = left.m11 - right.m11;
            return destination;
        }

        /**
         * Multiplies two matrices together and places the result in the destination matrix.
         *
         * @param left The left source matrix.
         * @param right The right source matrix.
         * @param destination The destination matrix or null if a new matrix is to be created.
         *
         * @return The destination matrix.
         */
        public static Matrix2f Multiply(Matrix2f left, Matrix2f right, Matrix2f destination)
        {
            if (destination == null)
            {
                destination = new Matrix2f();
            }

            float m00 = left.m00 * right.m00 + left.m10 * right.m01;
            float m01 = left.m01 * right.m00 + left.m11 * right.m01;
            float m10 = left.m00 * right.m10 + left.m10 * right.m11;
            float m11 = left.m01 * right.m10 + left.m11 * right.m11;

            destination.m00 = m00;
            destination.m01 = m01;
            destination.m10 = m10;
            destination.m11 = m11;
            return destination;
        }

        /**
         * Transforms a matrix by a vector and places the result in the destination matrix.
         *
         * @param left The left source matrix.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new matrix is to be created.
         *
         * @return The destination vector.
         */
        public static Vector2f Transform(Matrix2f left, Vector2f right, Vector2f destination)
        {
            if (destination == null)
            {
                destination = new Vector2f();
            }

            float x = left.m00 * right.x + left.m10 * right.y;
            float y = left.m01 * right.x + left.m11 * right.y;

            destination.x = x;
            destination.y = y;
            return destination;
        }

        /// <summary>
        /// Turns a 2x2 matrix into an array.
        /// </summary>
        /// <param name="matrix">A 4 float array.</param>
        /// <returns>The matrix to turn into an array.</returns>
        public static float[] ToArray(Matrix2f matrix)
        {
            float[] result = new float[4];
            result[0] = matrix.m00;
            result[1] = matrix.m01;
            result[2] = matrix.m10;
            result[3] = matrix.m11;
            return result;
        }

        /// <summary>
        /// Turns a array into an 2x2 matrix.
        /// </summary>
        /// <param name="array">A 4 float array.</param>
        /// <returns>The array to turn into an matrix.</returns>
        public static Matrix2f ToMatrix(float[] array)
        {
            Matrix2f result = new Matrix2f();
            result.m00 = array[0];
            result.m01 = array[1];
            result.m10 = array[2];
            result.m11 = array[3];
            return result;
        }

        /**
         * Sets this matrix to be the identity matrix.
         *
         * @return this.
         */
        public Matrix2f SetIdentity()
        {
            return SetIdentity(this);
        }

        /**
         * Inverts this matrix.
         *
         * @return this.
         */
        public Matrix2f Invert()
        {
            return Invert(this, this);
        }
        
        /**
         * Negates this matrix.
         *
         * @return this.
         */
        public Matrix2f Negate()
        {
            return Negate(this, this);
        }
        
        /**
         * Transposes this matrix
         *
         * @return this.
         */
        public Matrix2f Transpose()
        {
            return Transpose(this, this);
        }

        /**
         * Sets this matrix to 0.
         *
         * @return this.
         */
        public Matrix2f SetZero()
        {
            return SetZero(this);
        }

        /**
         * @return The determinant of the matrix.
         */
        public float Determinant()
        {
            return m00 * m11 - m01 * m10;
        }

        public override bool Equals(object o)
        {
            if (this == o)
            {
                return true;
            }

            if (o == null)
            {
                return false;
            }

            if (!GetType().Equals(o.GetType()))
            {
                return false;
            }

            Matrix2f other = (Matrix2f)o;

            return this.m00 == other.m00 && this.m01 == other.m01 && this.m10 == other.m10 && this.m11 == other.m11;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "Matrix3f[" + m00 + " " + m10 + "\n" + m01 + " " + m11 + "]";
        }
    }
}
