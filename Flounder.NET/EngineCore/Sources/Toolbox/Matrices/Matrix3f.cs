﻿using System;
using Flounder.Toolbox.Vectors;

namespace Flounder.Toolbox.Matrices
{
    /// <summary>
    /// Holds a 3x3 matrix.
    /// </summary>
    public class Matrix3f
    {
        public float m00 { get; set; }
        public float m01 { get; set; }
        public float m02 { get; set; }
        public float m10 { get; set; }
        public float m11 { get; set; }
        public float m12 { get; set; }
        public float m20 { get; set; }
        public float m21 { get; set; }
        public float m22 { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Matrices.Matrix3f"/> class.
        /// </summary>
        public Matrix3f()
        {
            SetIdentity();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Toolbox.Matrices.Matrix3f"/> class.
        /// </summary>
        /// <param name="source">The matrix source.</param>
        public Matrix3f(Matrix3f source)
        {
            Load(source, this);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Toolbox.Matrix.Matrix3f"/> is reclaimed by garbage collection.
        /// </summary>
        ~Matrix3f()
        {
        }

        /**
         * Set the source matrix to be the identity matrix.
         *
         * @param source The matrix to set to the identity.
         *
         * @return The source matrix.
         */
        public static Matrix3f SetIdentity(Matrix3f source)
        {
            source.m00 = 1.0f;
            source.m01 = 0.0f;
            source.m02 = 0.0f;
            source.m10 = 0.0f;
            source.m11 = 1.0f;
            source.m12 = 0.0f;
            source.m20 = 0.0f;
            source.m21 = 0.0f;
            source.m22 = 1.0f;
            return source;
        }

        /**
         * Inverts the source matrix and puts the result into the destination matrix.
         *
         * @param source The source matrix to be inverted.
         * @param destination The destination matrix, or null if a new one is to be created.
         *
         * @return The inverted matrix if successful, null otherwise.
         */
        public static Matrix3f Invert(Matrix3f source, Matrix3f destination)
        {
            float determinant = source.Determinant();

            if (determinant != 0)
            {
                if (destination == null)
                {
                    destination = new Matrix3f();
                }

                /*
                 * Does it the ordinary way. inv(A) = 1/det(A) * adj(T), where adj(T) = transpose(Conjugate Matrix) m00 m01 m02 m10 m11 m12 m20 m21 m22
                 */
                float determinant_inv = 1f / determinant;

                // Get the conjugate matrix.
                float t00 = source.m11 * source.m22 - source.m12 * source.m21;
                float t01 = -source.m10 * source.m22 + source.m12 * source.m20;
                float t02 = source.m10 * source.m21 - source.m11 * source.m20;
                float t10 = -source.m01 * source.m22 + source.m02 * source.m21;
                float t11 = source.m00 * source.m22 - source.m02 * source.m20;
                float t12 = -source.m00 * source.m21 + source.m01 * source.m20;
                float t20 = source.m01 * source.m12 - source.m02 * source.m11;
                float t21 = -source.m00 * source.m12 + source.m02 * source.m10;
                float t22 = source.m00 * source.m11 - source.m01 * source.m10;

                destination.m00 = t00 * determinant_inv;
                destination.m11 = t11 * determinant_inv;
                destination.m22 = t22 * determinant_inv;
                destination.m01 = t10 * determinant_inv;
                destination.m10 = t01 * determinant_inv;
                destination.m20 = t02 * determinant_inv;
                destination.m02 = t20 * determinant_inv;
                destination.m12 = t21 * determinant_inv;
                destination.m21 = t12 * determinant_inv;
                return destination;
            }
            else {
                return null;
            }
        }

        /**
         * Transpose the source matrix and places the result in the destination matrix.
         *
         * @param source The source matrix.
         * @param destination The destination matrix or null if a new matrix is to be created.
         *
         * @return The transposed matrix.
         */
        public static Matrix3f Transpose(Matrix3f source, Matrix3f destination)
        {
            if (destination == null)
            {
                destination = new Matrix3f();
            }

            float m00 = source.m00;
            float m01 = source.m10;
            float m02 = source.m20;
            float m10 = source.m01;
            float m11 = source.m11;
            float m12 = source.m21;
            float m20 = source.m02;
            float m21 = source.m12;
            float m22 = source.m22;

            destination.m00 = m00;
            destination.m01 = m01;
            destination.m02 = m02;
            destination.m10 = m10;
            destination.m11 = m11;
            destination.m12 = m12;
            destination.m20 = m20;
            destination.m21 = m21;
            destination.m22 = m22;
            return destination;
        }

        /**
         * Sets the source matrix to 0.
         *
         * @param source The matrix to be set to 0.
         *
         * @return The matrix set to zero.
         */
        public static Matrix3f SetZero(Matrix3f source)
        {
            source.m00 = 0.0f;
            source.m01 = 0.0f;
            source.m02 = 0.0f;
            source.m10 = 0.0f;
            source.m11 = 0.0f;
            source.m12 = 0.0f;
            source.m20 = 0.0f;
            source.m21 = 0.0f;
            source.m22 = 0.0f;
            return source;
        }

        /**
         * Negates the source matrix and places the result in the destination matrix.
         *
         * @param source The source matrix.
         * @param destination The destination matrix or null if a new matrix is to be created.
         *
         * @return The negated matrix.
         */
        public static Matrix3f Negate(Matrix3f source, Matrix3f destination)
        {
            if (destination == null)
            {
                destination = new Matrix3f();
            }

            destination.m00 = -source.m00;
            destination.m01 = -source.m02;
            destination.m02 = -source.m01;
            destination.m10 = -source.m10;
            destination.m11 = -source.m12;
            destination.m12 = -source.m11;
            destination.m20 = -source.m20;
            destination.m21 = -source.m22;
            destination.m22 = -source.m21;
            return destination;
        }

        /**
         * Copies the source matrix and places the result in the destination matrix.
         *
         * @param source The source matrix.
         * @param destination The destination matrix or null if a new matrix is to be created.
         *
         * @return The copied matrix.
         */
        public static Matrix3f Load(Matrix3f source, Matrix3f destination)
        {
            if (destination == null)
            {
                destination = new Matrix3f();
            }

            destination.m00 = source.m00;
            destination.m10 = source.m10;
            destination.m20 = source.m20;
            destination.m01 = source.m01;
            destination.m11 = source.m11;
            destination.m21 = source.m21;
            destination.m02 = source.m02;
            destination.m12 = source.m12;
            destination.m22 = source.m22;
            return destination;
        }

        /**
         * Adds two matrices together and places the result in the destination matrix.
         *
         * @param left The left source matrix.
         * @param right The right source matrix.
         * @param destination The destination matrix or null if a new matrix is to be created.
         *
         * @return The destination matrix.
         */
        public static Matrix3f Add(Matrix3f left, Matrix3f right, Matrix3f destination)
        {
            if (destination == null)
            {
                destination = new Matrix3f();
            }

            destination.m00 = left.m00 + right.m00;
            destination.m01 = left.m01 + right.m01;
            destination.m02 = left.m02 + right.m02;
            destination.m10 = left.m10 + right.m10;
            destination.m11 = left.m11 + right.m11;
            destination.m12 = left.m12 + right.m12;
            destination.m20 = left.m20 + right.m20;
            destination.m21 = left.m21 + right.m21;
            destination.m22 = left.m22 + right.m22;
            return destination;
        }

        /**
         * Subtracts two matrices together and places the result in the destination matrix.
         *
         * @param left The left source matrix.
         * @param right The right source matrix.
         * @param destination The destination matrix or null if a new matrix is to be created.
         *
         * @return The destination matrix.
         */
        public static Matrix3f Subtract(Matrix3f left, Matrix3f right, Matrix3f destination)
        {
            if (destination == null)
            {
                destination = new Matrix3f();
            }

            destination.m00 = left.m00 - right.m00;
            destination.m01 = left.m01 - right.m01;
            destination.m02 = left.m02 - right.m02;
            destination.m10 = left.m10 - right.m10;
            destination.m11 = left.m11 - right.m11;
            destination.m12 = left.m12 - right.m12;
            destination.m20 = left.m20 - right.m20;
            destination.m21 = left.m21 - right.m21;
            destination.m22 = left.m22 - right.m22;
            return destination;
        }

        /**
         * Multiplies two matrices together and places the result in the destination matrix.
         *
         * @param left The left source matrix.
         * @param right The right source matrix.
         * @param destination The destination matrix or null if a new matrix is to be created.
         *
         * @return The destination matrix.
         */
        public static Matrix3f Multiply(Matrix3f left, Matrix3f right, Matrix3f destination)
        {
            if (destination == null)
            {
                destination = new Matrix3f();
            }

            float m00 = left.m00 * right.m00 + left.m10 * right.m01 + left.m20 * right.m02;
            float m01 = left.m01 * right.m00 + left.m11 * right.m01 + left.m21 * right.m02;
            float m02 = left.m02 * right.m00 + left.m12 * right.m01 + left.m22 * right.m02;
            float m10 = left.m00 * right.m10 + left.m10 * right.m11 + left.m20 * right.m12;
            float m11 = left.m01 * right.m10 + left.m11 * right.m11 + left.m21 * right.m12;
            float m12 = left.m02 * right.m10 + left.m12 * right.m11 + left.m22 * right.m12;
            float m20 = left.m00 * right.m20 + left.m10 * right.m21 + left.m20 * right.m22;
            float m21 = left.m01 * right.m20 + left.m11 * right.m21 + left.m21 * right.m22;
            float m22 = left.m02 * right.m20 + left.m12 * right.m21 + left.m22 * right.m22;

            destination.m00 = m00;
            destination.m01 = m01;
            destination.m02 = m02;
            destination.m10 = m10;
            destination.m11 = m11;
            destination.m12 = m12;
            destination.m20 = m20;
            destination.m21 = m21;
            destination.m22 = m22;
            return destination;
        }

        /**
         * Transforms a matrix by a vector and places the result in the destination matrix.
         *
         * @param left The left source matrix.
         * @param right The right source vector.
         * @param destination The destination vector or null if a new matrix is to be created.
         *
         * @return The destination vector.
         */
        public static Vector3f Transform(Matrix3f left, Vector3f right, Vector3f destination)
        {
            if (destination == null)
            {
                destination = new Vector3f();
            }

            float x = left.m00 * right.x + left.m10 * right.y + left.m20 * right.z;
            float y = left.m01 * right.x + left.m11 * right.y + left.m21 * right.z;
            float z = left.m02 * right.x + left.m12 * right.y + left.m22 * right.z;

            destination.x = x;
            destination.y = y;
            destination.z = z;
            return destination;
        }

        /// <summary>
        /// Turns a 3x3 matrix into an array.
        /// </summary>
        /// <param name="matrix">A 9 float array.</param>
        /// <returns>The matrix to turn into an array.</returns>
        public static float[] ToArray(Matrix3f matrix)
        {
            float[] result = new float[9];
            result[0] = matrix.m00;
            result[1] = matrix.m01;
            result[2] = matrix.m02;
            result[3] = matrix.m10;
            result[4] = matrix.m11;
            result[5] = matrix.m12;
            result[6] = matrix.m20;
            result[7] = matrix.m21;
            result[8] = matrix.m22;
            return result;
        }

        /// <summary>
        /// Turns a array into an 3x3 matrix.
        /// </summary>
        /// <param name="array">A 9 float array.</param>
        /// <returns>The array to turn into an matrix.</returns>
        public static Matrix3f ToMatrix(float[] array)
        {
            Matrix3f result = new Matrix3f();
            result.m00 = array[0];
            result.m01 = array[1];
            result.m02 = array[2];
            result.m10 = array[3];
            result.m11 = array[4];
            result.m12 = array[5];
            result.m20 = array[6];
            result.m21 = array[7];
            result.m22 = array[8];
            return result;
        }

        /**
         * Sets this matrix to be the identity matrix.
         *
         * @return this.
         */
        public Matrix3f SetIdentity()
        {
            return SetIdentity(this);
        }

        /**
         * Inverts this matrix.
         *
         * @return this.
         */
        public Matrix3f Invert()
        {
            return Invert(this, this);
        }
        
        /**
         * Negates this matrix.
         *
         * @return this.
         */
        public Matrix3f Negate()
        {
            return Negate(this, this);
        }
        
        /**
         * Transposes this matrix
         *
         * @return this.
         */
        public Matrix3f Transpose()
        {
            return Transpose(this, this);
        }

        /**
         * Sets this matrix to 0.
         *
         * @return this.
         */
        public Matrix3f SetZero()
        {
            return SetZero(this);
        }

        /**
         * @return The determinant of the matrix.
         */
        public float Determinant()
        {
            return m00 * (m11 * m22 - m12 * m21) + m01 * (m12 * m20 - m10 * m22) + m02 * (m10 * m21 - m11 * m20);
        }
        
        public override bool Equals(object o)
        {
            if (this == o)
            {
                return true;
            }

            if (o == null)
            {
                return false;
            }

            if (!GetType().Equals(o.GetType()))
            {
                return false;
            }

            Matrix3f other = (Matrix3f)o;

            return this.m00 == other.m00 && this.m01 == other.m01 && this.m02 == other.m02 && this.m10 == other.m10 && this.m11 == other.m11 && this.m12 == other.m12 && this.m20 == other.m20 && this.m21 == other.m21 && this.m22 == other.m22;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "Matrix3f[" + m00 + " " + m10 + " " + m20 + "\n" + m01 + " " + m11 + " " + m21 + "\n" + m02 + " " + m12 + " " + m22 + "]";
        }
    }
}
