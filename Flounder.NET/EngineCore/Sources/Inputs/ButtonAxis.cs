﻿namespace Flounder.Inputs
{
    /// <summary>
    /// Axis composed of two buttons.
    /// </summary>
    public class ButtonAxis : IAxis
    {
        /// <summary>
        /// The positive axis key.
        /// </summary>
        private IButton positive;

        /// <summary>
        /// The negative axis key.
        /// </summary>
        private IButton negative;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Inputs.ButtonAxis"/> class.
        /// </summary>
        /// <param name="negative">When this button is down, the axis is negative.</param>
        /// <param name="positive">When this button is down, the axis is positive.</param>
        public ButtonAxis(IButton negative, IButton positive)
        {
            this.negative = negative;
            this.positive = positive;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Inputs.ButtonAxis"/> is reclaimed by garbage collection.
        /// </summary>
        ~ButtonAxis() 
        {
        }

        public float GetAmount()
        {
            var result = 0.0f;

            if (positive.IsDown())
            {
                result += 1.0f;
            }

            if (negative.IsDown())
            {
                result -= 1.0f;
            }

            return result;
        }
    }
}
