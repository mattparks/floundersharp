﻿using Flounder.Toolbox;
using Flounder.Devices;

namespace Flounder.Inputs
{
    /// <summary>
    /// Axis from a joystick.
    /// </summary>
    public class JoystickAxis : IAxis
    {
        /// <summary>
        /// The tracked joystick.
        /// </summary>
        private int joystick;

        /// <summary>
        /// The array of tracked joystick axes.
        /// </summary>
        private int[] joystickAxes;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Inputs.JoystickAxis"/> class.
        /// </summary>
        /// <param name="joystick">The joystick. Should be in range of the GLFW joystick values.</param>
        /// <param name="joystickAxis">The axis on the joystick being checked.</param>
        public JoystickAxis(int joystick, int joystickAxis)
        {
            this.joystick = joystick;
            this.joystickAxes = new int[] { joystickAxis };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Inputs.JoystickAxis"/> class.
        /// </summary>
        /// <param name="joystick">The joystick. Should be in range of the GLFW joystick values.</param>
        /// <param name="joystickAxes">The axes on the joystick being checked.</param>
        JoystickAxis(int joystick, int[] joystickAxes)
        {
            this.joystick = joystick;
            this.joystickAxes = joystickAxes;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Inputs.JoystickAxis"/> is reclaimed by garbage collection.
        /// </summary>
        ~JoystickAxis()
        {
        }

        public float GetAmount()
        {
            if (joystickAxes == null || joystick == -1)
            {
                return 0.0f;
            }

            float result = 0.0f;

            if (DeviceJoystick.GetJoystick(joystick) != null)
            {
                foreach (var joystickAxe in joystickAxes)
                {
                    result += DeviceJoystick.GetJoystick(joystick).GetJoystickAxis(joystickAxe);
                }
            }

            return (float) Maths.Clamp(result, -1.0f, 1.0f);
        }
    }
}
