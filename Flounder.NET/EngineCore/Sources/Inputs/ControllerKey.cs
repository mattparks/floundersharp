﻿namespace Flounder.Inputs
{
    /// <summary>
    /// Handles controller keys.
    /// </summary>
    public class ControllerKey
    {
        /// <summary>
        /// The tracked key.
        /// </summary>
        private BaseButton key;

        /// <summary>
        /// Was the key down before?
        /// </summary>
        private bool wasDown;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Inputs.ControllerKey"/> class.
        /// </summary>
        /// <param name="key">The BaseButton key to be listened too.</param>
        public ControllerKey(BaseButton key)
        {
            this.key = key;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Inputs.ControllerKey"/> is reclaimed by garbage collection.
        /// </summary>
        ~ControllerKey()
        {
        }
        
        /// <summary>
        /// Gets if the key down and was not down before.
        /// </summary>
        /// <returns><c>true</c> if the key down and was not down before, the event is recognized as one click; otherwise, <c>false</c>.</returns>
        public bool WasPressed()
        {
            var stillDown = wasDown && IsDown();
            wasDown = IsDown();
            return wasDown == !stillDown;
        }
        
        /// <summary>
        /// Gets if this key is down.
        /// </summary>
        /// <returns><c>true</c> if the key is down; otherwise, <c>false</c>.</returns>
        public bool IsDown()
        {
            return key.IsDown();
        }
    }
}
