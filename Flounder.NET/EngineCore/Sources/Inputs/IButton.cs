﻿namespace Flounder.Inputs
{
    /// <summary>
    /// Interface for a binary input device.
    /// </summary>
    public interface IButton
    {
        /// <summary>
        /// Returns whether this button is currently pressed.
        /// </summary>
        /// <returns>True if the button is pressed, false otherwise.</returns>
        bool IsDown();
    }
}
