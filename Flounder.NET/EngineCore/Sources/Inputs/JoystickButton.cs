﻿using Flounder.Devices;

namespace Flounder.Inputs
{
    /// <summary>
    /// Handles buttons on a joystick.
    /// </summary>
    public class JoystickButton : BaseButton
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Inputs.JoystickButton"/> class.
        /// </summary>
        /// <param name="joystick">The joystick. Should be in range of the GLFW joystick values.</param>
        /// <param name="joystickButton">The button on the joystick being checked.</param>
        public JoystickButton(int joystick, int joystickButton) : base(new int[]{ joystickButton }, (int code)=>DeviceJoystick.GetJoystick(joystick).GetJoystickButton(code))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Inputs.JoystickButton"/> class.
        /// </summary>
        /// <param name="joystick">The joystick. Should be in range of the GLFW joystick values.</param>
        /// <param name="joystickButtons">The buttons on the joystick being checked.</param>
        public JoystickButton(int joystick, int[] joystickButtons) : base(joystickButtons, (int code)=>DeviceJoystick.GetJoystick(joystick).GetJoystickButton(code))
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Inputs.JoystickButton"/> is reclaimed by garbage collection.
        /// </summary>
        ~JoystickButton()
        {
        }
    }
}
