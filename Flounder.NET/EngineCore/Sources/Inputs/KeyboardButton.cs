﻿using Flounder.Devices;

namespace Flounder.Inputs
{
    /// <summary>
    /// Handles buttons from a keyboard.
    /// </summary>
    public class KeyboardButton : BaseButton
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Inputs.KeyboardButton"/> class.
        /// </summary>
        /// <param name="keyCode">The code for the key being checked. Should be in range of the GLFW keyboard values.</param>
        public KeyboardButton(int keyCode) : base(new int[] { keyCode }, (int code) => (DeviceKeyboard.GetKey(code)))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Inputs.KeyboardButton"/> class.
        /// </summary>
        /// <param name="keyCodes">The codes for the key being checked. Should be in range of the GLFW keyboard values.</param>
        public KeyboardButton(int[] keyCodes) : base(keyCodes, (int code) => (DeviceKeyboard.GetKey(code)))
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Inputs.KeyButton"/> is reclaimed by garbage collection.
        /// </summary>
        ~KeyboardButton()
        {
        }
    }
}
