﻿namespace Flounder.Inputs
{
    /// <summary>
    /// Handles multiple buttons at once.
    /// </summary>
    public class CompoundButton : IButton
    {
        /// <summary>
        /// The array of avalable buttons.
        /// </summary>
        private IButton[] buttons;

        /// <summary>
        /// Creates a new CompoundButton.
        /// </summary>
        /// <param name="buttons">The list of buttons being checked.</param>
        public CompoundButton(IButton[] buttons)
        {
            this.buttons = buttons;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Inputs.CompoundButton"/> is reclaimed by garbage collection.
        /// </summary>
        ~CompoundButton()
        {
        }
        
        public bool IsDown()
        {
            foreach (var button in buttons)
            {
                if (button != null)
                {
                    if (button.IsDown())
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
