﻿using Flounder.Toolbox;

namespace Flounder.Inputs
{
    /// <summary>
    /// Axis composed of multiple other axes.
    /// </summary>
    public class CompoundAxis : IAxis
    {
        /// <summary>
        /// The array of avalable axes.
        /// </summary>
        private IAxis[] axes;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Inputs.CompoundAxis"/> class.
        /// </summary>
        /// <param name="axes">The list of axes to combine.</param>
        public CompoundAxis(IAxis[] axes)
        {
            this.axes = axes;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Inputs.CompoundAxis"/> is reclaimed by garbage collection.
        /// </summary>
        ~CompoundAxis()
        {
        }

        public float GetAmount()
        {
            var result = 0.0f;

            foreach (var axe in axes)
            {
                if (axe != null)
                {
                    result += axe.GetAmount(); // TODO: Fix, causes wierd axis addition things!
                }
            }

            return (float) Maths.Clamp(result, -1.0f, 1.0f);
        }
    }
}
