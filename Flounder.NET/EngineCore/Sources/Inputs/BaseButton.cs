﻿namespace Flounder.Inputs
{
    /// <summary>
    /// Decides whether a certain code is down or not.
    /// </summary>
    /// <param name="code">The button code.</param>
    /// <returns><c>true</c> if the button specified by the code is down; otherwise, <c>false</c>.</returns>
    public delegate bool Command(int code); 

    /// <summary>
    /// Base class for typical buttons.
    /// </summary>
    public abstract class BaseButton : IButton
    {
        /// <summary>
        /// The array of avalable codes.
        /// </summary>
        private int[] codes;

        /// <summary>
        /// The command to get states of codes with.
        /// </summary>
        private Command command;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Inputs.BaseButton"/> class.
        /// </summary>
        /// <param name="codes">The list of codes this button is checking.</param>
        /// <param name="command">The method of deciding if a code is down or not in the input system.</param>
        public BaseButton(int[] codes, Command command)
        {
            this.codes = codes;
            this.command = command;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Inputs.BaseButton"/> is reclaimed by garbage collection.
        /// </summary>
        ~BaseButton() 
        {
        }

        public bool IsDown()
        {
            if (codes == null)
            {
                return false;
            }

            foreach (var code in codes)
            {
                if (command(code))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
