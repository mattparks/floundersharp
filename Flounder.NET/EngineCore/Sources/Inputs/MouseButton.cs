﻿using Flounder.Devices;

namespace Flounder.Inputs
{
    /// <summary>
    /// Handles buttons on a mouse.
    /// </summary>
    public class MouseButton : BaseButton
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Inputs.MouseButton"/> class.
        /// </summary>
        /// <param name="mouseButton">The code for the key being checked. Should be in range of the GLFW mouse button values.</param>
        public MouseButton(int mouseButton) : base(new int[] { mouseButton }, (int code) => (DeviceMouse.GetMouse(code)))
        { 
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Inputs.MouseButton"/> class.
        /// </summary>
        /// <param name="mouseButton">The codes for the key being checked. Should be in range of the GLFW mouse button values.</param>
        public MouseButton(int[] mouseButtons) : base(mouseButtons, (int code) => (DeviceMouse.GetMouse(code)))
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Inputs.MouseButton"/> is reclaimed by garbage collection.
        /// </summary>
        ~MouseButton()
        {
        }
    }
}
