﻿namespace Flounder.Inputs
{
    /// <summary>
    /// Interface for an axis based input device.
    /// </summary>
    public interface IAxis
    {
        /// <summary>
        /// Gets the current value along the axis. -1 is smallest input, 1 is largest input.
        /// </summary>
        /// <returns>The current value of the axis in the range (-1, 1).</returns>
        float GetAmount();
    }
}
