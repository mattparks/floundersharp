﻿using Flounder.Toolbox.Vectors;

namespace Flounder.Sounds
{
    /// <summary>
    /// A audio listener.
    /// </summary>
    public interface AudioListener
    {
        /// <summary>
        /// Gets the position.
        /// </summary>
        /// <returns>The position.</returns>
        Vector3f GetPosition();
    }
}

