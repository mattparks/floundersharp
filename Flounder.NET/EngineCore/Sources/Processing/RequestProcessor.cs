﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Flounder.Processing
{
    /// <summary>
    /// Class that is responsible for processing resource requests in a separate thread.
    /// </summary>
    public class RequestProcessor
    {
        /// <summary>
        /// The request processor.
        /// </summary>
        private static RequestProcessor PROCESSOR = new RequestProcessor();

        /// <summary>
        /// The to run on thead.
        /// </summary>
        private Thread thead;

        /// <summary>
        /// The request queue.
        /// </summary>
        private RequestQueue requestQueue;

        /// <summary>
        /// Is the processor running.
        /// </summary>
        private bool running;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Processing.RequestProcessor"/> class.
        /// </summary>
        public RequestProcessor()
        {
            this.running = true;
            this.thead = new Thread(new ThreadStart(Run));
            this.requestQueue = new RequestQueue();
            this.thead.Start();
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Processing.RequestProcessor"/> is reclaimed by garbage collection.
        /// </summary>
        ~RequestProcessor()
        {
            CleanUp();
        }

        /// <summary>
        /// Adds a new resource request to queue.
        /// </summary>
        /// <param name="request">The resource request to add.</param>
        public static void SendRequest(ResourceRequest request)
        {
            PROCESSOR.AddRequestToQueue(request);
        }

        /// <summary>
        /// Cleans up the request processor and destroys the thread.
        /// </summary>
        public static void CleanUp()
        {
            PROCESSOR.Kill();
        }

        /// <summary>
        /// Adds a request to queue.
        /// </summary>
        /// <param name="request">A request to add.</param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        private void AddRequestToQueue(ResourceRequest request)
        {
            requestQueue.AddRequest(request);
            Monitor.Pulse(this);
        }

        /// <summary>
        /// Kills this instance.
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        private void Kill()
        {
            running = false;
            Monitor.Pulse(this);
        }

        /// <summary>
        /// Runs this instance.
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        private void Run()
        {
            while (running || requestQueue.HasRequests())
            {
                if (requestQueue.HasRequests())
                {
                    requestQueue.AcceptNextRequest().DoResourceRequest();
                }
                else
                {
                    try
                    {
                        Monitor.Wait(this);
                    }
                    catch (ThreadInterruptedException e)
                    {
                        Console.WriteLine(e.StackTrace);
                    }
                }
            }
        }
    }
}
