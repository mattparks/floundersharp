﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flounder.Processing.RequestGL
{
    /// <summary>
    /// Class that is responsible for processing OpenGL requests.
    /// </summary>
    public class GlRequestProcessor
    {
        /// <summary>
        /// The max time in mills.
        /// </summary>
        public static float MAX_TIME_MILLIS = 10f;

        /// <summary>
        /// The max time.
        /// </summary>
        public static float MAX_TIME = 100f;

        /// <summary>
        /// The request queue.
        /// </summary>
        private static GlRequestQueue requestQueue = new GlRequestQueue();

        /// <summary>
        /// Sends a new request into queue.
        /// </summary>
        /// <param name="request">The request to add.</param>
        public static void SendRequest(GlRequest request)
        {
            requestQueue.AddRequest(request);
        }

        /// <summary>
        /// Deals with in the time slot available.
        /// </summary>
        public static void DealWithTopRequests()
        {
            var remainingTime = MAX_TIME;

            while (requestQueue.HasRequests())
            {
                if (requestQueue.GetNextRequestRequiredTime() <= remainingTime)
                {
                    var request = requestQueue.AcceptNextRequest();
                    remainingTime -= request.GetTimeRequired();
                    request.ExecuteGlRequest();
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Completes all requests left in queue.
        /// </summary>
        public static void CompleteAllRequests()
        {
            while (requestQueue.HasRequests())
            {
                requestQueue.AcceptNextRequest().ExecuteGlRequest();
            }
        }
    }
}
