﻿namespace Flounder.Processing.RequestGL
{
    /// <summary>
    /// Interface for executable OpenGL requests.
    /// </summary>
    public interface GlRequest
    {
        /// <summary>
        /// Executed when the request is being processed.
        /// </summary>
        void ExecuteGlRequest();
        
        /// <returns>Returns a estimate of the time required for the request to be processed.</returns>
        float GetTimeRequired();
    }
}
