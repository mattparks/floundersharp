﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Flounder.Processing.RequestGL
{
    /// <summary>
    /// Holds OpenGL requests that are in queue.
    /// </summary>
    public class GlRequestQueue
    {
        /// <summary>
        /// The request queue.
        /// </summary>
        private List<GlRequest> requestQueue = new List<GlRequest>();

        /// <summary>
        /// Adds a new GL request to queue.
        /// </summary>
        /// <param name="request">The GL request to add.</param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void AddRequest(GlRequest request)
        {
            requestQueue.Add(request);
        }

        /// <summary>
        /// Accepts the next request and then removes it from this list.
        /// </summary>
        /// <returns>The next request.</returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public GlRequest AcceptNextRequest()
        {
            var oldItem = requestQueue[0];
            requestQueue.RemoveAt(0);
            return oldItem;
        }

        /// <summary>
        /// Gets the next request required time in queue.
        /// </summary>
        /// <returns>The next request required time in queue.</returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public float GetNextRequestRequiredTime()
        {
            if (HasRequests())
            {
                return requestQueue[0].GetTimeRequired();
            }
            else 
            {
                Console.WriteLine("GL Request Queue is empty!");
                return 10000;
            }
        }
        
        /// <summary>
        /// Determines whether this instance has requests in queue.
        /// </summary>
        /// <returns><c>true</c> if this instance has requests in queue; otherwise, <c>false</c>.</returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool HasRequests()
        {
            return requestQueue.Count != 0;
        }
    }
}
