﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Flounder.Processing
{
    /// <summary>
    /// Holds resource requests that are in queue.
    /// </summary>
    public class RequestQueue
    {
        /// <summary>
        /// The request queue.
        /// </summary>
        private List<ResourceRequest> requestQueue = new List<ResourceRequest>();
        
        /// <summary>
        /// Adds a new resource request to queue.
        /// </summary>
        /// <param name="request">The resource request to add.</param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void AddRequest(ResourceRequest request)
        {
            requestQueue.Add(request);
        }
        
        /// <summary>
        /// Accepts the next request in queue.
        /// </summary>
        /// <returns>The next request.</returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public ResourceRequest AcceptNextRequest()
        {
            var oldItem = requestQueue[0];
            requestQueue.RemoveAt(0);
            return oldItem;
        }
        
        /// <summary>
        /// Determines whether this instance has requests in queue.
        /// </summary>
        /// <returns><c>true</c> if this instance has requests in queue; otherwise, <c>false</c>.</returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool HasRequests()
        {
            return requestQueue.Count != 0;
        }
    }
}
