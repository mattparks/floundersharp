﻿namespace Flounder.Processing
{
    /// <summary>
    /// Interface for executable resource requests.
    /// </summary>
    public interface ResourceRequest
    {
        /// <summary>
        /// Used to send a request to the request processor so it can be queued.
        /// </summary>
        void DoResourceRequest();
    }
}
