﻿using System;
using Flounder.Toolbox;
using Flounder.Toolbox.Matrices;
using Flounder.Physics;

namespace Flounder.Space
{
    /// <summary>
    /// Represents the view planes from a camera that can be used to determine if a object can be seen.
    /// </summary>
    public class Frustum
    {
        // Each frustum planes.
        private static int RIGHT = 0;
        private static int LEFT = 1;
        private static int BOTTOM = 2;
        private static int TOP = 3;
        private static int BACK = 4;
        private static int FRONT = 5;

        // The values stored in the planes.
        private static int A = 0;
        private static int B = 1;
        private static int C = 2;
        private static int D = 3;

        /// <summary>
        /// The frustum planes.
        /// </summary>
        private static float[,] planesFrustum = new float[6, 4];

        private static Frustum frustum = new Frustum();

        /// <summary>
        /// Creates a frustum from the view and projection matrix.
        /// </summary>
        /// <param name="projection">The projection matrix.</param>
        /// <param name="viewMatrix">The view matrix.</param>
        /// <returns>A frustum created from the projection and view matrices.</returns>
        public static Frustum GetFrustum(Matrix4f projection, Matrix4f viewMatrix) {
            frustum.CalculateFrustum(projection, viewMatrix);
            return frustum;
        }

        /// <summary>
        /// Updates a frustum from the view and projection matrix.
        /// </summary>
        /// <param name="projection">The projection matrix.</param>
        /// <param name="viewMatrix">The view matrix.</param>
        private void CalculateFrustum(Matrix4f projection, Matrix4f viewMatrix)
        {
            var proj = Matrix4f.ToArray(projection);
            var view = Matrix4f.ToArray(viewMatrix);
            var clip = new float[16];

            clip[0] = view[0] * proj[0] + view[1] * proj[4] + view[2] * proj[8] + view[3] * proj[12];
            clip[1] = view[0] * proj[1] + view[1] * proj[5] + view[2] * proj[9] + view[3] * proj[13];
            clip[2] = view[0] * proj[2] + view[1] * proj[6] + view[2] * proj[10] + view[3] * proj[14];
            clip[3] = view[0] * proj[3] + view[1] * proj[7] + view[2] * proj[11] + view[3] * proj[15];

            clip[4] = view[4] * proj[0] + view[5] * proj[4] + view[6] * proj[8] + view[7] * proj[12];
            clip[5] = view[4] * proj[1] + view[5] * proj[5] + view[6] * proj[9] + view[7] * proj[13];
            clip[6] = view[4] * proj[2] + view[5] * proj[6] + view[6] * proj[10] + view[7] * proj[14];
            clip[7] = view[4] * proj[3] + view[5] * proj[7] + view[6] * proj[11] + view[7] * proj[15];

            clip[8] = view[8] * proj[0] + view[9] * proj[4] + view[10] * proj[8] + view[11] * proj[12];
            clip[9] = view[8] * proj[1] + view[9] * proj[5] + view[10] * proj[9] + view[11] * proj[13];
            clip[10] = view[8] * proj[2] + view[9] * proj[6] + view[10] * proj[10] + view[11] * proj[14];
            clip[11] = view[8] * proj[3] + view[9] * proj[7] + view[10] * proj[11] + view[11] * proj[15];

            clip[12] = view[12] * proj[0] + view[13] * proj[4] + view[14] * proj[8] + view[15] * proj[12];
            clip[13] = view[12] * proj[1] + view[13] * proj[5] + view[14] * proj[9] + view[15] * proj[13];
            clip[14] = view[12] * proj[2] + view[13] * proj[6] + view[14] * proj[10] + view[15] * proj[14];
            clip[15] = view[12] * proj[3] + view[13] * proj[7] + view[14] * proj[11] + view[15] * proj[15];

            // This will extract the LEFT side of the frustum
            planesFrustum[LEFT, A] = clip[3] - clip[0];
            planesFrustum[LEFT, B] = clip[7] - clip[4];
            planesFrustum[LEFT, C] = clip[11] - clip[8];
            planesFrustum[LEFT, D] = clip[15] - clip[12];
            NormalizePlane(planesFrustum, LEFT);

            // This will extract the RIGHT side of the frustum
            planesFrustum[RIGHT, A] = clip[3] + clip[0];
            planesFrustum[RIGHT, B] = clip[7] + clip[4];
            planesFrustum[RIGHT, C] = clip[11] + clip[8];
            planesFrustum[RIGHT, D] = clip[15] + clip[12];
            NormalizePlane(planesFrustum, RIGHT);

            // This will extract the BOTTOM side of the frustum
            planesFrustum[BOTTOM, A] = clip[3] + clip[1];
            planesFrustum[BOTTOM, B] = clip[7] + clip[5];
            planesFrustum[BOTTOM, C] = clip[11] + clip[9];
            planesFrustum[BOTTOM, D] = clip[15] + clip[13];
            NormalizePlane(planesFrustum, BOTTOM);

            // This will extract the TOP side of the frustum
            planesFrustum[TOP, A] = clip[3] - clip[1];
            planesFrustum[TOP, B] = clip[7] - clip[5];
            planesFrustum[TOP, C] = clip[11] - clip[9];
            planesFrustum[TOP, D] = clip[15] - clip[13];
            NormalizePlane(planesFrustum, TOP);

            // This will extract the FRONT side of the frustum
            planesFrustum[FRONT, A] = clip[3] - clip[2];
            planesFrustum[FRONT, B] = clip[7] - clip[6];
            planesFrustum[FRONT, C] = clip[11] - clip[10];
            planesFrustum[FRONT, D] = clip[15] - clip[14];
            NormalizePlane(planesFrustum, FRONT);

            // This will extract the BACK side of the frustum
            planesFrustum[BACK, A] = clip[3] + clip[2];
            planesFrustum[BACK, B] = clip[7] + clip[6];
            planesFrustum[BACK, C] = clip[11] + clip[10];
            planesFrustum[BACK, D] = clip[15] + clip[14];
            NormalizePlane(planesFrustum, BACK);
        }

        /// <summary>
        /// Normalizes the plane.
        /// </summary>
        /// <param name="frustum">The frustum planes.</param>
        /// <param name="side">The side.</param>
        private void NormalizePlane(float[,] frustum, int side)
        {
            var magnitude = (float)Math.Sqrt(frustum[side, A] * frustum[side, A] + frustum[side, B] * frustum[side, B] + frustum[side, C] * frustum[side, C]);

            frustum[side, A] /= magnitude;
            frustum[side, B] /= magnitude;
            frustum[side, C] /= magnitude;
            frustum[side, D] /= magnitude;
        }

        /// <summary>
        /// Gets if the point contained in the frustum?
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="z">The z coordinate.</param>
        /// <returns><c>true</c>, if the point is contained, <c>false</c> otherwise.</returns>
        public bool PointInFrustum(float x, float y, float z)
        {
            for (var i = 0; i < 6; i++)
            {
                if (planesFrustum[i, 0] * x + planesFrustum[i, 1] * y + planesFrustum[i, 2] * z + planesFrustum[i, 3] <= 0.0F)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Gets if the sphere contained in the frustum?
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="z">The z coordinate.</param>
        /// <param name="radius">Radius.</param>
        /// <returns><c>true</c>, if the sphere is contained, <c>false</c> otherwise.</returns>
        public bool SphereInFrustum(float x, float y, float z, float radius)
        {
            for (var i = 0; i < 6; i++)
            {
                if (planesFrustum[i, 0] * x + planesFrustum[i, 1] * y + planesFrustum[i, 2] * z + planesFrustum[i, 3] <= -radius)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Gets if the cube contained fully in the frustum?.
        /// </summary>
        /// <param name="x1">The first x value.</param>
        /// <param name="y1">The first y value.</param>
        /// <param name="z1">The first z value.</param>
        /// <param name="x2">The second x value.</param>
        /// <param name="y2">The second y value.</param>
        /// <param name="z2">The second z value.</param>
        /// <returns><c>true</c>, if fully in frustum contained, <c>false</c> otherwise.</returns>
        public bool CubeFullyInFrustum(float x1, float y1, float z1, float x2, float y2, float z2)
        {
            for (var i = 0; i < 6; i++)
            {
                if (planesFrustum[i, 0] * x1 + planesFrustum[i, 1] * y1 + planesFrustum[i, 2] * z1 + planesFrustum[i, 3] <= 0.0F)
                {
                    return false;
                }

                if (planesFrustum[i, 0] * x2 + planesFrustum[i, 1] * y1 + planesFrustum[i, 2] * z1 + planesFrustum[i, 3] <= 0.0F)
                {
                    return false;
                }

                if (planesFrustum[i, 0] * x1 + planesFrustum[i, 1] * y2 + planesFrustum[i, 2] * z1 + planesFrustum[i, 3] <= 0.0F)
                {
                    return false;
                }

                if (planesFrustum[i, 0] * x2 + planesFrustum[i, 1] * y2 + planesFrustum[i, 2] * z1 + planesFrustum[i, 3] <= 0.0F)
                {
                    return false;
                }

                if (planesFrustum[i, 0] * x1 + planesFrustum[i, 1] * y1 + planesFrustum[i, 2] * z2 + planesFrustum[i, 3] <= 0.0F)
                {
                    return false;
                }

                if (planesFrustum[i, 0] * x2 + planesFrustum[i, 1] * y1 + planesFrustum[i, 2] * z2 + planesFrustum[i, 3] <= 0.0F)
                {
                    return false;
                }

                if (planesFrustum[i, 0] * x1 + planesFrustum[i, 1] * y2 + planesFrustum[i, 2] * z2 + planesFrustum[i, 3] <= 0.0F)
                {
                    return false;
                }

                if (planesFrustum[i, 0] * x2 + planesFrustum[i, 1] * y2 + planesFrustum[i, 2] * z2 + planesFrustum[i, 3] <= 0.0F)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Gets if the aabb contained partially in the frustum?
        /// </summary>
        /// <param name="aabb">The aabb.</param>
        /// <returns><c>true</c>, if partially contained, <c>false</c> otherwise.</returns>
        public bool AabbInFrustum(AABB aabb)
        {
            return aabb == null || CubeInFrustum(aabb.minExtents.x, aabb.minExtents.y, aabb.minExtents.z, aabb.maxExtents.x, aabb.maxExtents.y, aabb.maxExtents.z);
        }

        /// <summary>
        /// Gets if the cube contained partially in the frustum?
        /// </summary>
        /// <param name="x1">The first x value.</param>
        /// <param name="y1">The first y value.</param>
        /// <param name="z1">The first z value.</param>
        /// <param name="x2">The second x value.</param>
        /// <param name="y2">The second y value.</param>
        /// <param name="z2">The second z value.</param>
        /// <returns><c>true</c>, if partially contained, <c>false</c> otherwise.</returns>
        public bool CubeInFrustum(float x1, float y1, float z1, float x2, float y2, float z2)
        {
            for (var i = 0; i < 6; i++)
            {
                if (planesFrustum[i, 0] * x1 + planesFrustum[i, 1] * y1 + planesFrustum[i, 2] * z1 + planesFrustum[i, 3] <= 0.0F && planesFrustum[i, 0] * x2 + planesFrustum[i, 1] * y1 + planesFrustum[i, 2] * z1 + planesFrustum[i, 3] <= 0.0F && planesFrustum[i, 0] * x1 + planesFrustum[i, 1] * y2 + planesFrustum[i, 2] * z1 + planesFrustum[i, 3] <= 0.0F && planesFrustum[i, 0] * x2 + planesFrustum[i, 1] * y2 + planesFrustum[i, 2] * z1 + planesFrustum[i, 3] <= 0.0F && planesFrustum[i, 0] * x1 + planesFrustum[i, 1] * y1 + planesFrustum[i, 2] * z2 + planesFrustum[i, 3] <= 0.0F && planesFrustum[i, 0] * x2 + planesFrustum[i, 1] * y1 + planesFrustum[i, 2] * z2 + planesFrustum[i, 3] <= 0.0F && planesFrustum[i, 0] * x1 + planesFrustum[i, 1] * y2 + planesFrustum[i, 2] * z2 + planesFrustum[i, 3] <= 0.0F && planesFrustum[i, 0] * x2 + planesFrustum[i, 1] * y2 + planesFrustum[i, 2] * z2 + planesFrustum[i, 3] <= 0.0F)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
