﻿using Flounder.Physics;

namespace Flounder.Space
{
    /// <summary>
    /// Represents an object that has some notion of space, and can be stored in a {@link ISpatialStructure}.
    /// </summary>
    public interface ISpatialObject
    {
        /// <summary>
        /// Gets a AABB that fully encloses the object.
        /// </summary>
        /// <returns>Returns a AABB fully enclosing the object.</returns>
        AABB GetAABB();
    }
}
