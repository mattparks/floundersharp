﻿using Flounder.Physics;
using System.Collections.Generic;

namespace Flounder.Space
{
    /// <summary>
    /// A data structure that stores objects with a notion of space.
    /// </summary>
    public interface ISpatialStructure<T> where T : ISpatialObject
    {
        /// <summary>
        /// Adds a new object to the spatial structure.
        /// </summary>
        /// <param name="obj">The object to add.</param>
        void Add(T obj);

        /// <summary>
        /// Removes an object from the spatial structure.
        /// </summary>
        /// <param name="obj">The object to remove.</param>
        void Remove(T obj);

        /// <summary>
        /// Removes all objects from the spatial structure.
        /// </summary>
        void Clear();

        /// <summary>
        /// Returns a set of all objects in the spatial structure.
        /// </summary>
        /// <returns>The list specified by of all objects.</returns>
        List<T> GetAll();

        /// <summary>
        /// Returns a set of all objects in a specific range of the spatial structure.
        /// </summary>
        /// <param name="range">The frustum range of space being queried.</param>
        /// <returns>The list of all object in range.</returns>
        List<T> QueryInFrustum(Frustum range);

        /// <summary>
        /// Returns a set of all objects in a specific range of the spatial structure.
        /// </summary>
        /// <param name="range">The aabb range of space being queried.</param>
        /// <returns>The list of all object in range.</returns>
        List<T> QueryInAABB(AABB range);
    }
}
