﻿using Flounder.Toolbox;
using System;

namespace Flounder.Profiling
{
    /// <summary>
    /// Can be used to record various timings within the engine,
    /// </summary>
    public class ProfileTimer
    {
        /// <summary>
        /// The amount of invocations.
        /// </summary>
        private int invocations;

        /// <summary>
        /// The total time.
        /// </summary>
        private double totalTime;

        /// <summary>
        /// The start time.
        /// </summary>
        private double startTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Profiling.ProfileTimer"/> class.
        /// </summary>
        public ProfileTimer()
        {
            this.invocations = 0;
            this.totalTime = 0;
            this.startTime = 0;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Profiling.ProfileTimer"/> is reclaimed by garbage collection.
        /// </summary>
        ~ProfileTimer()
        {
        }

        /// <summary>
        /// Starts a new invocation.
        /// </summary>
        public void StartInvocation()
        {
            startTime = SystemTime.Nanoseconds();
        }

        /// <summary>
        /// Stops the current Invocation.
        /// </summary>
        public void StopInvocation()
        {
            if (startTime == 0)
            {
                Console.WriteLine("Stop Invocation called without matching start invocation!");
                // Assert.IsTrue(startTime != 0); // Stops from running faulty data. // TODO: Assert in mono!
            }

            invocations++;
            totalTime += SystemTime.Nanoseconds() - startTime;
            startTime = 0;
        }
        
        /// <summary>
        /// Gets the total time taken in ms, and resets the timer.
        /// </summary>
        /// <returns>The total time taken in ms.</returns>
        public double Reset()
        {
            var timeMs = ((totalTime / 1000000.0) / ((double)invocations));
            invocations = 0;
            totalTime = 0;
            startTime = 0;
            return timeMs;
        }
    }
}
