﻿using Flounder.Toolbox;
using Flounder.Toolbox.Vectors;
using System;

namespace Flounder.Physics
{
    /// <summary>
    /// Represents a 3D aabb that can suround objects.
    /// </summary>
    public class AABB
    {
        /// <summary>
        /// Gets or sets the minimum extents.
        /// </summary>
        /// <value>The minimum extents.</value>
        public Vector3f minExtents { get; set; }

        /// <summary>
        /// Gets or sets the max extents.
        /// </summary>
        /// <value>The max extents.</value>
        public Vector3f maxExtents { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Physics.AABB"/> class.
        /// </summary>
        public AABB()
        {
            minExtents = new Vector3f();
            maxExtents = new Vector3f();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Physics.AABB"/> class.
        /// </summary>
        /// <param name="minExtents">Minimum extents.</param>
        /// <param name="maxExtents">Maximum extents.</param>
        AABB(Vector3f minExtents, Vector3f maxExtents)
        {
            this.minExtents = minExtents;
            this.maxExtents = maxExtents;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Physics.AABB"/> is reclaimed by garbage collection.
        /// </summary>
        ~AABB()
        {
        }

        /// <summary>
        /// Creates a new AABB equivalent to this, scaled away from the center origin.
        /// </summary>
        /// <param name="scale">Amount to scale up the AABB.</param>
        /// <returns>A new AABB, scaled by the specified amounts.</returns>
        public AABB Scale(Vector3f scale)
        {
            return new AABB(Vector3f.Multiply(minExtents, scale, null), Vector3f.Multiply(maxExtents, scale, null));
        }

        /// <summary>
        /// Creates a new AABB equivalent to this, but scaled away from the origin by a certain amount.
        /// </summary>
        /// <param name="expand">Amount to scale up the AABB.</param>
        /// <returns>A new AABB, scaled by the specified amounts.</returns>
        public AABB Expand(Vector3f expand)
        {
            return new AABB(Vector3f.Subtract(minExtents, expand, null), Vector3f.Add(maxExtents, expand, null));
        }

        /// <summary>
        /// Creates an AABB that bounds both this AABB and another AABB.
        /// </summary>
        /// <param name="other">The other AABB being bounded.</param>
        /// <returns>An AABB that bounds both this AABB and {@code other}.</returns>
        public AABB Combine(AABB other)
        {
            var newMinX = Math.Min(minExtents.x, other.minExtents.x);
            var newMinY = Math.Min(minExtents.y, other.minExtents.y);
            var newMinZ = Math.Min(minExtents.z, other.minExtents.z);
            var newMaxX = Math.Max(maxExtents.x, other.maxExtents.x);
            var newMaxY = Math.Max(maxExtents.y, other.maxExtents.y);
            var newMaxZ = Math.Max(maxExtents.z, other.maxExtents.z);
            return new AABB(new Vector3f(newMinX, newMinY, newMinZ), new Vector3f(newMaxX, newMaxY, newMaxZ));
        }

        /// <summary>
        /// Creates a new AABB equivalent to this, but stretched by a certain amount.
        /// </summary>
        /// <param name="stretch">The amount to stretch.</param>
        /// <returns>A new AABB, stretched by the specified amounts.</returns>
        public AABB Stretch(Vector3f stretch)
        {
            float newMinX, newMaxX, newMinY, newMaxY, newMinZ, newMaxZ;

            if (stretch.x < 0)
            {
                newMinX = minExtents.x + stretch.x;
                newMaxX = maxExtents.x;
            }
            else
            {
                newMinX = minExtents.x;
                newMaxX = maxExtents.x + stretch.x;
            }

            if (stretch.y < 0)
            {
                newMinY = minExtents.y + stretch.y;
                newMaxY = maxExtents.y;
            }
            else
            {
                newMinY = minExtents.y;
                newMaxY = maxExtents.y + stretch.y;
            }

            if (stretch.z < 0)
            {
                newMinZ = minExtents.z + stretch.z;
                newMaxZ = maxExtents.z;
            }
            else
            {
                newMinZ = minExtents.z;
                newMaxZ = maxExtents.z + stretch.z;
            }

            return new AABB(new Vector3f(newMinX, newMinY, newMinZ), new Vector3f(newMaxX, newMaxY, newMaxZ));
        }

        /// <summary>
        /// Creates an AABB equivalent to this, but in a new position and rotation.
        /// </summary>
        /// <param name="position">The amount to move.</param>
        /// <param name="rotation">The amount to rotate.</param>
        /// <returns>An AABB equivalent to this, but in a new position.</returns>
        public AABB Recalculate(Vector3f position, Vector3f rotation)
        {
            // Scale, then Rotate, then lastly transform! IN THAT ORDER, use matricies.
            // Maths.rotateVector(minExtents, rotation.x, rotation.y, rotation.z)
            // Maths.rotateVector(maxExtents, rotation.x, rotation.y, rotation.z)
            return new AABB(Vector3f.Add(minExtents, position, null), Vector3f.Add(maxExtents, position, null));
        }

        /// <summary>
        /// Is it intersetcing the specified other AABB.
        /// </summary>
        /// <param name="other">The other AABB.</param>
        public IntersectData Intersects(AABB other)
        {
            if (other == null)
            {
                throw new InvalidOperationException("Null AABB collider.");
            }
            else if (Equals(other))
            {
                return new IntersectData(true, 0);
            }

            var maxDist = Maths.Max(Maths.Max(new Vector3f(minExtents.x - other.maxExtents.x, minExtents.y - other.maxExtents.y, minExtents.z - other.maxExtents.z), new Vector3f(other.minExtents.x - maxExtents.x, other.minExtents.y - maxExtents.y, other.minExtents.z - maxExtents.z)));

            return new IntersectData(maxDist < 0, maxDist);
        }

        /// <summary>
        /// Does the AABB contain the specified point.
        /// </summary>
        /// <param name="point">The specified point.</param>
        public bool Contains(Vector3f point)
        {
            if (point.x > maxExtents.x)
            {
                return false;
            }
            else if (point.x < minExtents.x)
            {
                return false;
            }
            else if (point.y > maxExtents.y)
            {
                return false;
            }
            else if (point.y < minExtents.y)
            {
                return false;
            }
            else if (point.z > maxExtents.z)
            {
                return false;
            }
            else if (point.z < minExtents.z)
            {
                return false;
            }

            return true;
        }

        public override string ToString()
        {
            return string.Format("[AABB: minExtents={0}, maxExtents={1}]", minExtents, maxExtents);
        }
    }
}
