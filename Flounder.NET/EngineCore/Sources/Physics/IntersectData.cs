﻿using Flounder.Toolbox;

namespace Flounder.Physics
{
    /// <summary>
    /// Represents a intersect.
    /// </summary>
    public class IntersectData
    {
        /// <summary>
        /// Gets or sets the intersection distance.
        /// </summary>
        /// <value>The distance.</value>
        public float distance { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Flounder.Physics.IntersectData"/> is a intersection.
        /// </summary>
        /// <value><c>true</c> if intersection; otherwise, <c>false</c>.</value>
        public bool intersection{ get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Physics.IntersectData"/> class.
        /// </summary>
        /// <param name="intersects">If set to <c>true</c> if it intersects.</param>
        /// <param name="distance">The intersection distance.</param>
        public IntersectData(bool intersects, float distance)
        {
            this.distance = distance;
            this.intersection = intersects;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Physics.IntersectData"/> is reclaimed by garbage collection.
        /// </summary>
        ~IntersectData()
        {
        }

        public override bool Equals(object o)
        {
            if (this == o)
            {
                return true;
            }

            if (o == null)
            {
                return false;
            }

            if (!GetType().Equals(o.GetType()))
            {
                return false;
            }

            var other = (IntersectData)o;

            if (Maths.FloatToUInt32Bits(distance) != Maths.FloatToUInt32Bits(other.distance))
            {
                return false;
            }
            else if (intersection != other.intersection)
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            var hash = 3;
            hash = (int) (89 * hash + Maths.FloatToUInt32Bits(distance));
            hash = 89 * hash + (intersection ? 1 : 0);
            return hash;
        }

        public override string ToString()
        {
            return string.Format("[IntersectData: distance={0}, intersection={1}]", distance, intersection);
        }
    }
}
