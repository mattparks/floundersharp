﻿using System;
using Flounder.Toolbox.Resources;
using Flounder.Sounds;

namespace Flounder.Devices
{
    /// <summary>
    /// Manages the creation, updating and destruction of sounds.
    /// </summary>
    public static class DeviceSound
    {
        /// <summary>
        /// The root sound folder.
        /// </summary>
        public static MyFile SOUND_FOLDER = new MyFile(MyFile.RES_FOLDER, "sounds");

        /// <summary>
        /// Creates a OpenAL sound device.
        /// </summary>
        public static void CreateDeviceSound() 
        {
        }

        /// <summary>
        /// Updates sounds. Should be called once before every frame.
        /// </summary>
        public static void Update()
        {
        }

        /// <summary>
        /// Closes the sound system, do not play sounds after calling this.
        /// </summary>
        public static void CleanUp()
        {
        }
    }
}

