﻿using Flounder.Toolbox;
using Flounder.Basics;
using OpenGL.GLFW;

namespace Flounder.Devices
{
    /// <summary>
    /// Manages the creation, updating and destruction of the mouse.
    /// </summary>
    public static class DeviceMouse
    {
        /// <summary>
        /// The array of avalable mouse buttons.
        /// </summary>
        private static bool[] buttons = new bool[(int) MouseButton.Last];

        /// <summary>
        /// The last mouse position x.
        /// </summary>
        private static double lastMousePositionX;

        /// <summary>
        /// The last mouse position y.
        /// </summary>
        private static double lastMousePositionY;

        /// <summary>
        /// Gets the mouse position x.
        /// </summary>
        /// <value>The mouse position x.</value>
        public static double mousePositionX { get; private set; }

        /// <summary>
        /// Gets the mouse position y.
        /// </summary>
        /// <value>The mouse position y.</value>
        public static double mousePositionY { get; private set; }

        /// <summary>
        /// Gets the mouse delta x.
        /// </summary>
        /// <value>The mouse delta x.</value>
        public static double mouseDeltaX { get; private set; }

        /// <summary>
        /// Gets the mouse delta y.
        /// </summary>
        /// <value>The mouse delta y.</value>
        public static double mouseDeltaY { get; private set; }

        /// <summary>
        /// Gets the mouse delta wheel.
        /// </summary>
        /// <value>The mouse delta wheel.</value>
        public static double mouseDeltaWheel { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Flounder.Devices.DeviceMouse"/> has the display selected.
        /// </summary>
        /// <value><c>true</c> if display is selected; otherwise, <c>false</c>.</value>
        public static bool displaySelected { get; private set; }

        /// <summary>
        /// Updates the mouse system. Should be called once every frame.
        /// </summary>
        public static void Update()
        {
            mouseDeltaX = (lastMousePositionX - mousePositionX) * EngineCore.GetDeltaSeconds();
            mouseDeltaY = (lastMousePositionY - mousePositionY) * EngineCore.GetDeltaSeconds();

            lastMousePositionX = mousePositionX;
            lastMousePositionY = mousePositionY;

            mouseDeltaWheel += (mouseDeltaWheel > 0) ? -25 * EngineCore.GetDeltaSeconds() : 25 * EngineCore.GetDeltaSeconds();
            mouseDeltaWheel = Maths.Deadband(0.875f, mouseDeltaWheel);
        }

        /// <summary>
        /// Gets whether or not a particular mouse button is currently pressed.
        /// </summary>
        /// <param name="button">The mouse button to test.</param>
        /// <returns><c>true</c>, if the mouse button is currently pressed, <c>false</c> otherwise.</returns>
        public static bool GetMouse(int button)
        {
            return buttons[button];
        }

        /// <summary>
        /// Closes the mouse system, do not check the mouse after calling this.
        /// </summary>
        public static void CleanUp()
        {
        }

        /// <summary>
        /// GLFW callbacks for the mouse device.
        /// </summary>
        public static class MouseCallbacks 
        {
            /// <summary>
            /// Mouses the scroll callback.
            /// </summary>
            /// <param name="window">The glfw window.</param>
            /// <param name="x">The x coordinate.</param>
            /// <param name="y">The y coordinate.</param>
            public static void MouseScrollCallback(GLFWWindowPtr window, double x, double y)
            {
                mouseDeltaWheel += y;
            }

            /// <summary>
            /// The mouses button callback.
            /// </summary>
            /// <param name="window">The glfw window.</param>
            /// <param name="button">The button.</param>
            /// <param name="action">If buttons action.</param>
            public static void MouseButtonCallback(GLFWWindowPtr window, MouseButton button, KeyAction action)
            {
                buttons[(int)button] = action.Equals(KeyAction.Press);
            }

            /// <summary>
            /// The mouses position callback.
            /// </summary>
            /// <param name="window">The glfw window.</param>
            /// <param name="x">The x coordinate.</param>
            /// <param name="y">The y coordinate.</param>
            public static void MousePositionCallback(GLFWWindowPtr window, double x, double y)
            {
                // var mouseXBefore = mousePositionX;
                // var mouseYBefore = mousePositionY;

                mousePositionX = x / DeviceDisplay.GetWidth();
                mousePositionY = y / DeviceDisplay.GetHeight();
            }

            /// <summary>
            /// The mouses enter callback.
            /// </summary>
            /// <param name="window">The glfw window.</param>
            /// <param name="value">If set to <c>true</c> value.</param>
            public static void MouseEnterCallback(GLFWWindowPtr window, bool value)
            {
                displaySelected = value;
            }
        }
    }
}
