﻿using OpenGL.GLFW;

namespace Flounder.Devices
{
    /// <summary>
    /// Manages the creation, updating and destruction of the keyboard keys.
    /// </summary>
    public static class DeviceKeyboard
    {
        /// <summary>
        /// The array of avalable keys.
        /// </summary>
        private static bool[] keys = new bool[(int) KeyCode.Last];

        /// <summary>
        /// Init this instance.
        /// </summary>
        public static void Init()
        {
        }

        /// <summary>
        /// Updates the keyboard system. Should be called once every frame.
        /// </summary>
        public static void Update()
        {
        }

        /// <summary>
        /// Gets whether or not a particular key is currently pressed.
        /// </summary>
        /// <param name="key">The key to test.</param>
        /// <returns><c>true</c>, if the key is currently pressed, <c>false</c> otherwise.</returns>
        public static bool GetKey(int key)
        {
            return keys[key];
        }

        /// <summary>
        /// Closes the keyboard system, do not check the keys after calling this.
        /// </summary>
        public static void CleanUp()
        {
        }

        /// <summary>
        /// GLFW callbacks for the keyboard device.
        /// </summary>
        public static class KeyboardCallbacks 
        {
            /// <summary>
            /// The keyboards key callback.
            /// </summary>
            /// <param name="window">The GLFW window.</param>
            /// <param name="key">The key.</param>
            /// <param name="scanCode">The keys scan code .</param>
            /// <param name="mods">The keys mods.</param>
            public static void KeyboardCallback(GLFWWindowPtr window, KeyCode key, int scanCode, KeyAction action, KeyModifiers mods)
            {
                keys[(int)key] = GLFW3.GetKey(window, key);
            }
        }
    }
}
