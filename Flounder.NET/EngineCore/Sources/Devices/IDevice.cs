﻿using System;

namespace Flounder.Devices
{
    public interface IDevice
    {
        void Init();

        void Update();

        void CleanUp();
    }
}

