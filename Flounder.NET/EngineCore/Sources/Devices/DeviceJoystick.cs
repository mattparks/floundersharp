﻿using OpenGL.GLFW;
using System;
using System.Collections.Generic;

namespace Flounder.Devices
{
    /// <summary>
    /// Manages the creation, updating and destruction of joysticks.
    /// </summary>
    public static class DeviceJoystick
    {
        /// <summary>
        /// The array of connected joysticks.
        /// </summary>
        private static JoystickObject[] joysticks = new JoystickObject[(int) Joystick.Last];

        /// <summary>
        /// Updates the joystick system. Should be called once every frame.
        /// </summary>
        public static void Update()
        {
            for (int i = 0; i < (int)joysticks.Length; i++)
            {
                var joy = (Joystick)i;

                if (joysticks[i] == null && GLFW3.JoystickPresent(joy))
                {
                    joysticks[i] = new JoystickObject(joy);
                } 
                else if (joysticks[i] != null && !GLFW3.JoystickPresent(joy))
                {
                    joysticks[i] = null;
                }
                else if (joysticks[i] != null)
                {
                    joysticks[i].Update();
                }
            }
        }

        /// <summary>
        /// Gets the joystick from a identifier.
        /// </summary>
        /// <param name="id">The GLFW joystick identifier.</param>
        /// <returns>A GLFW joystick object.</returns>
        public static JoystickObject GetJoystick(int id) {
            return joysticks[id];
        }

        /// <summary>
        /// Closes the joystick system, do not check the joystick after calling this.
        /// </summary>
        public static void CleanUp()
        {
        }
    }

    /// <summary>
    /// Represents a attached Joystick object.
    /// </summary>
    public class JoystickObject {
        /// <summary>
        /// The glfw joystick.
        /// </summary>
        private Joystick glfwJoystick;

        /// <summary>
        /// The array of avalable buttons.
        /// </summary>
        private bool[] buttons;

        /// <summary>
        /// The array of avalable axes.
        /// </summary>
        private float[] axes;

        /// <summary>
        /// Creates a representation of a attached joystick.
        /// </summary>
        /// <param name="glfwJoystick">The int representing the GLFW joystick.</param>
        public JoystickObject(Joystick glfwJoystick) {
            this.glfwJoystick = glfwJoystick;
            Update();
        }

        /// <summary>
        /// Updates this joystick instance.
        /// </summary>
        public void Update() {
            var states = GLFW3.GetJoystickButtons(glfwJoystick);
            buttons = new bool[states.Length];

            for (int i = 0; i < states.Length; i++)
            {
                buttons[i] = states[i] != 0;
            }

            axes = GLFW3.GetJoystickAxes(glfwJoystick);
        }

        /// <summary>
        /// Gets the value of a joystick's axis.
        /// </summary>
        /// <param name="axis">The axis of interest.</param>
        /// <returns>The reading from the joystick axis.</returns>
        public float GetJoystickAxis(int axis) {
            return !(axis >= 0 && axis <= GetNumJoystickAxes()) ? 0.0f : axes[axis];
        }

        /// <summary>
        /// Gets the whether a button on a joystick is pressed.
        /// </summary>
        /// <param name="button">The button of interest.</param>
        /// <returns>The state from the joystick interest.</returns>
        public bool GetJoystickButton(int button) {
            return !(button >= 0 || button <= GetNumJoystickButtons()) ? false : buttons[button];
        }

        /// <summary>
        /// Gets the number of axes a joystick offers.
        /// </summary>
        /// <returns>The number of axes the joystick offers.</returns>
        public int GetNumJoystickAxes() {
            return axes.Length;
        }

        /// <summary>
        /// Gets the number of buttons a joystick offers.
        /// </summary>
        /// <returns>The number of buttons the joystick offers.</returns>
        public int GetNumJoystickButtons() {
            return buttons.Length;
        }

        /// <summary>
        /// Gets a string describing the connected joystick.
        /// </summary>
        /// <returns>The string describing the connected joystick.</returns>
        public string GetJoystickName() {
            return GLFW3.GetJoystickName(glfwJoystick);
        }
    }
}

