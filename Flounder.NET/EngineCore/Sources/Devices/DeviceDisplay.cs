﻿using System;
using OpenGL.GLFW;
using OpenGL;
using Flounder.Basics.Options;

namespace Flounder.Devices
{
    /// <summary>
    /// Manages the creation, updating and destruction of the display, as well as timing and frame times.
    /// </summary>
    public static class DeviceDisplay
    {
        /// <summary>
        /// Has the window been close requested?
        /// </summary>
        private static bool closeRequested;

        /// <summary>
        /// The GLFW displays window.
        /// </summary>
        private static GLFWWindowPtr window;

        /// <summary>
        /// Creates a GLFW display device.
        /// </summary>
        public static void CreateDeviceDisplay()
        {
            closeRequested = false;

            // Sets up an error callback.
            GLFW3.SetErrorCallback(DeviceDisplay.DisplayCallbacks.ErrorCallback);

            // Initializes GLFW. Most GLFW functions will not work before doing this.
            if (!GLFW3.Init())
            {
                throw new InvalidOperationException("Unable to initialize GLFW!");
            }

            // Gets the resolution of the primary monitor.
            var vidMode = GLFW3.GetVideoMode(GLFW3.GetPrimaryMonitor());

            // Configures the window.
            GLFW3.DefaultWindowHints();
            GLFW3.WindowHint(WindowHint.ContextVersionMajor, 3);
            GLFW3.WindowHint(WindowHint.ContextVersionMinor, 2);
            GLFW3.WindowHint(WindowHint.Samples, 8);
            GLFW3.WindowHint(WindowHint.RefreshRate, 60);
            GLFW3.SwapInterval(OptionsGraphics.V_SYNC ? 1 : 0);
            
            // Creates a new window.
            window = GLFW3.CreateWindow(OptionsGraphics.DISPLAY_WIDTH, OptionsGraphics.DISPLAY_HEIGHT, OptionsGraphics.DISPLAY_TITLE, GLFWMonitorPtr.Null, GLFWWindowPtr.Null);

            // Checks if the window was created successfully.
            if (window.Equals(null))
            {
                GLFW3.Terminate();
                throw new InvalidOperationException("Failed to create the GLFW window.");
            }

            // Makes the OpenGL context current.
            GLFW3.MakeContextCurrent(window);

            // Sets up the engines callbacks.
            GLFW3.SetKeyCallback(window, DeviceKeyboard.KeyboardCallbacks.KeyboardCallback);
            GLFW3.SetScrollCallback(window, DeviceMouse.MouseCallbacks.MouseScrollCallback);
            GLFW3.SetMouseButtonCallback(window, DeviceMouse.MouseCallbacks.MouseButtonCallback);
            GLFW3.SetCursorPosCallback(window, DeviceMouse.MouseCallbacks.MousePositionCallback);
            GLFW3.SetCursorEnterCallback(window, DeviceMouse.MouseCallbacks.MouseEnterCallback);
            GLFW3.SetWindowSizeCallback(window, DeviceDisplay.DisplayCallbacks.ScreenSizeCallback);
            GLFW3.SetFramebufferSizeCallback(window, DeviceDisplay.DisplayCallbacks.FrameBufferSizeCallback);

            // Sets the GetProcAddress delegate for OpenGL.
            OpenGLInit.Init(GLFW3.GetProcAddress);

            // Clears OpenGL.
            GL10.glClear(GL11.GL_COLOR_BUFFER_BIT);
            GL10.glViewport(0, 0, OptionsGraphics.DISPLAY_WIDTH, OptionsGraphics.DISPLAY_HEIGHT);

            // Centers the window.
            GLFW3.SetWindowPos(window, (vidMode.Width - OptionsGraphics.DISPLAY_WIDTH) / 2, (vidMode.Height - OptionsGraphics.DISPLAY_HEIGHT) / 2);

            // Makes the window visible.
            GLFW3.ShowWindow(window);

            // Logs OpenGL version info.
            // TODO: Log into!
        }

        /// <summary>
        /// Updates the display system. Should be called once every frame.
        /// </summary>
        public static void Update()
        {
            // Polls for window events. The key callback will only be invoked during this call.
            GLFW3.PollEvents();

            // Updates the display image by swaping the colour buffers.
            GLFW3.SwapBuffers(window); 
        }

        /// <summary>
        /// Gets the current GLFW time.
        /// </summary>
        /// <returns>The current GLFW time * 1000.</returns>
        public static float GetCurrentTime()
        {
            return (float)(GLFW3.GetTime() * 1000);
        }

        /// <summary>
        /// Gets the width of the display in pixels
        /// </summary>
        /// <returns>The width of the display in pixels.</returns>
        public static int GetWidth()
        {
            return OptionsGraphics.DISPLAY_WIDTH;
        }

        /// <summary>
        /// Gets the height of the display in pixels
        /// </summary>
        /// <returns>The height of the display in pixels.</returns>
        public static int GetHeight()
        {
            return OptionsGraphics.DISPLAY_HEIGHT;
        }

        /// <summary>
        /// Gets the aspect ratio between the displays width and height.
        /// </summary>
        /// <returns>The aspect ratio between the displays width and height.</returns>
        public static float GetAspectRatio()
        {
            return GetWidth() * 1f / (GetHeight() * 1f);
        }

        /// <summary>
        /// Determines if the GLFW display is open.
        /// </summary>
        /// <returns><c>true</c> if the GLFW display is open or if close has not been requested; otherwise, <c>false</c>.</returns>
        public static bool IsOpen()
        {
            return !GLFW3.WindowShouldClose(window) && !closeRequested;
        }

        /// <summary>
        /// Indicates that the game has been requested to close. At the end of the current frame the main game loop will exit.
        /// </summary>
        public static void RequestClosure()
        {
            closeRequested = true;
        }

        /// <summary>
        /// Closes the GLFW display, do not renderObjects after calling this.
        /// </summary>
        public static void CleanUp()
        {
            GLFW3.DestroyWindow(window);
            GLFW3.Terminate();
        }

        /// <summary>
        /// GLFW callbacks for the display device.
        /// </summary>
        public static class DisplayCallbacks
        {
            /// <summary>
            /// The GLFW error callback.
            /// </summary>
            /// <param name="code">The error code.</param>
            /// <param name="desc">The error desc.</param>
            public static void ErrorCallback(GLFWError code, string desc)
            {
                Console.WriteLine("GLFW Error " + code + ": " + desc);
            }

            /// <summary>
            /// The screens size callback.
            /// </summary>
            /// <param name="window">The GLFW window.</param>
            /// <param name="width">The new width.</param>
            /// <param name="height">The new height.</param>
            public static void ScreenSizeCallback(GLFWWindowPtr window, int width, int height)
            {
                OptionsGraphics.DISPLAY_WIDTH = width;
                OptionsGraphics.DISPLAY_HEIGHT = height;
            }

            /// <summary>
            /// The frame buffer size callback.
            /// </summary>
            /// <param name="window">The GLFW window.</param>
            /// <param name="width">The new width.</param>
            /// <param name="height">The new height.</param>
            public static void FrameBufferSizeCallback(GLFWWindowPtr window, int width, int height)
            {
                GL10.glViewport(0, 0, width, height);
            }
        }
    }
}
