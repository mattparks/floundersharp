﻿using OpenGL;

namespace Flounder.Shaders
{
    /// <summary>
    /// Represents a texture sampler uniform type that can be loaded to the shader.
    /// </summary>
    public class UniformSampler : Uniform
    {
        /// <summary>
        /// The current value.
        /// </summary>
        private int currentValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Shaders.UniformSampler"/> class.
        /// </summary>
        /// <param name="name">The uniform name.</param>
        public UniformSampler(string name) : base(name)
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Flounder.Shaders.UniformSampler"/> is reclaimed by garbage collection.
        /// </summary>
        ~UniformSampler()
        {
        }

        /// <summary>
        /// Loads a int sampler to the uniform if the value already on the GPU is not the same as the new value.
        /// </summary>
        /// <param name="value">The new value.</param>
        public void LoadTexUnit(int value)
        {
            if (currentValue != value)
            {
                currentValue = value;
                GL20.glUniform1i(base.GetLocation(), value);
            }
        }
    }
}
