﻿using OpenGL;
using Flounder.Toolbox.Matrices;
using System.Runtime.InteropServices;
using Flounder.Toolbox;

namespace Flounder.Shaders
{
    /// <summary>
    /// Represents a Matrix4F uniform type that can be loaded to the shader.
    /// </summary>
    public class UniformMat4 : Uniform
    {
        /// <summary>
        /// The current value.
        /// </summary>
        private Matrix4f currentValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Shaders.UniformMat4"/> class.
        /// </summary>
        /// <param name="name">The uniform name.</param>
        public UniformMat4(string name) : base(name)
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Flounder.Shaders.UniformMat4"/> is reclaimed by garbage collection.
        /// </summary>
        ~UniformMat4()
        {
        }

        /// <summary>
        /// Loads a Matrix4F to the uniform if the value already on the GPU is not the same as the new value.
        /// </summary>
        /// <param name="value">The new value.</param>
        public unsafe void loadMat4(Matrix4f value) {
            if (value != null && (currentValue == null || !value.Equals(currentValue))) {
                Matrix4f.Load(value, currentValue);
                float[] array = Matrix4f.ToArray(value);
                GL20.glUniformMatrix4fv(base.GetLocation(), 1, false, ref array);
            }
        }
    }
}
