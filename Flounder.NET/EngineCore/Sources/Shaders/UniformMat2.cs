﻿using OpenGL;
using Flounder.Toolbox.Matrices;
using System.Runtime.InteropServices;
using Flounder.Toolbox;

namespace Flounder.Shaders
{
    /// <summary>
    /// Represents a Matrix2F uniform type that can be loaded to the shader.
    /// </summary>
    public class UniformMat2 : Uniform
    {
        /// <summary>
        /// The current value.
        /// </summary>
        private Matrix2f currentValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Shaders.UniformMat2"/> class.
        /// </summary>
        /// <param name="name">The uniform name.</param>
        public UniformMat2(string name) : base(name)
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Flounder.Shaders.UniformMat2"/> is reclaimed by garbage collection.
        /// </summary>
        ~UniformMat2()
        {
        }

        /// <summary>
        /// Loads a Matrix2F to the uniform if the value already on the GPU is not the same as the new value.
        /// </summary>
        /// <param name="value">The new value.</param>
        public unsafe void loadMat2(Matrix2f value) {
            if (value != null && (currentValue == null || !value.Equals(currentValue))) {
                Matrix2f.Load(value, currentValue);
                float[] array = Matrix2f.ToArray(value);
                GL20.glUniformMatrix2fv(base.GetLocation(), 1, false, ref array);
            }
        }
    }
}
