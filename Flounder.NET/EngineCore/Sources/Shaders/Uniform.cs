﻿using OpenGL;
using System;
using System.Runtime.InteropServices;

namespace Flounder.Shaders
{
    /// <summary>
    /// Represents a uniform variable uploaded from C# to OpenGL shaders.
    /// </summary>
    public abstract class Uniform
    {
        /// <summary>
        /// The uniform not found code.
        /// </summary>
        private static int NOT_FOUND = -1;

        /// <summary>
        /// The name.
        /// </summary>
        private string name;

        /// <summary>
        /// The location.
        /// </summary>
        private int location;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Shaders.Uniform"/> class.
        /// </summary>
        /// <param name="name">The uniform name.</param>
        public Uniform(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Shaders.Uniform"/> is reclaimed by garbage collection.
        /// </summary>
        ~Uniform()
        {
        }

        /// <summary>
        /// Stores the uniform location.
        /// </summary>
        /// <param name="programID">The program ID.</param>
        public unsafe void StoreUniformLocation(uint programID)
        {
            location = GL20.glGetUniformLocation(programID, ref name);

            if (location == NOT_FOUND)
            {
                Console.WriteLine("No uniform variable called " + name + " found!");
            }
        }

        /// <summary>
        /// Gets the location.
        /// </summary>
        /// <returns>The location.</returns>
        protected int GetLocation()
        {
            return location;
        }
    }
}
