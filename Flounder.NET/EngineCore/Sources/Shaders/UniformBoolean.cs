﻿using OpenGL;

namespace Flounder.Shaders
{
    /// <summary>
    /// Represents a boolean uniform type that can be loaded to the shader.
    /// </summary>
    public class UniformBoolean : Uniform
    {
        /// <summary>
        /// The current value.
        /// </summary>
        private bool currentValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Shaders.UniformBoolean"/> class.
        /// </summary>
        /// <param name="name">The uniform name.</param>
        public UniformBoolean(string name) : base(name)
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Flounder.Shaders.UniformBoolean"/> is reclaimed by garbage collection.
        /// </summary>
        ~UniformBoolean()
        {
        }
        
        /// <summary>
        /// Loads a boolean to the uniform if the value already on the GPU is not the same as the new value.
        /// </summary>
        /// <param name="value">The new value.</param>
        public void LoadBoolean(bool value)
        {
            if (currentValue != value)
            {
                currentValue = value;
                GL20.glUniform1f(base.GetLocation(), value ? 1f : 0f);
            }
        }
    }
}
