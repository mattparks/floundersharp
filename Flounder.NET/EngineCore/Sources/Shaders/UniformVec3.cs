﻿using Flounder.Toolbox.Vectors;
using OpenGL;

namespace Flounder.Shaders
{
    /// <summary>
    /// Represents a 3 value vector uniform type that can be loaded to the shader.
    /// </summary>
    public class UniformVec3 : Uniform
    {
        /// <summary>
        /// The current x.
        /// </summary>
        private float currentX;

        /// <summary>
        /// The current y.
        /// </summary>
        private float currentY;

        /// <summary>
        /// The current z.
        /// </summary>
        private float currentZ;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Shaders.UniformVec3"/> class.
        /// </summary>
        /// <param name="name">The uniform name.</param>
        public UniformVec3(string name) : base(name)
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Flounder.Shaders.UniformVec3"/> is reclaimed by garbage collection.
        /// </summary>
        ~UniformVec3()
        {
        }

        /// <summary>
        /// Loads a Vector3f to the uniform if the value already on the GPU is not the same as the new value.
        /// </summary>
        /// <param name="vector">The new vector.</param>
        public void LoadVec3(Vector3f vector)
        {
            LoadVec3(vector.x, vector.y, vector.z);
        }

        /// <summary>
        /// Loads a x, y and z value to the uniform if the value already on the GPU is not the same as the new value.
        /// </summary>
        /// <param name="x">The new x value.</param>
        /// <param name="y">The new y value.</param>
        /// <param name="z">The new z value.</param>
        public void LoadVec3(float x, float y, float z)
        {
            if (x != currentX || y != currentY || z != currentZ)
            {
                currentX = x;
                currentY = y;
                currentZ = z;
                GL20.glUniform3f(base.GetLocation(), x, y, z);
            }
        }
    }
}
