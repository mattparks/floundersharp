﻿using OpenGL;
using Flounder.Toolbox.Matrices;
using System.Runtime.InteropServices;
using Flounder.Toolbox;

namespace Flounder.Shaders
{
    /// <summary>
    /// Represents a Matrix3F uniform type that can be loaded to the shader.
    /// </summary>
    public class UniformMat3 : Uniform
    {
        /// <summary>
        /// The current value.
        /// </summary>
        private Matrix3f currentValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Shaders.UniformMat3"/> class.
        /// </summary>
        /// <param name="name">The uniform name.</param>
        public UniformMat3(string name) : base(name)
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Flounder.Shaders.UniformMat3"/> is reclaimed by garbage collection.
        /// </summary>
        ~UniformMat3()
        {
        }

        /// <summary>
        /// Loads a Matrix3F to the uniform if the value already on the GPU is not the same as the new value.
        /// </summary>
        /// <param name="value">The new value.</param>
        public unsafe void loadMat3(Matrix3f value) {
            if (value != null && (currentValue == null || !value.Equals(currentValue))) {
                Matrix3f.Load(value, currentValue);
                float[] array = Matrix3f.ToArray(value);
                GL20.glUniformMatrix3fv(base.GetLocation(), 1, false, ref array);
            }
        }
    }
}
