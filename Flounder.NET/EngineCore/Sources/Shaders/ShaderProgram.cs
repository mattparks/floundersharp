﻿using Flounder.Toolbox.Resources;
using OpenGL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Flounder.Shaders
{
    /// <summary>
    /// Represents a user-defined program designed to run on the graphics processor and manages loading, starting, stopping and cleaning up.
    /// </summary>
    public class ShaderProgram
    {
        private List<string> layoutLocations = new List<string>();
        private List<string> layoutBindings = new List<string>();

        /// <summary>
        /// The program ID.
        /// </summary>
        private uint programID;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Shaders.ShaderProgram"/> class.
        /// </summary>
        /// <param name="vertexBuilder">The vertex builder.</param>
        /// <param name="fragmentBuilder">The fragment builder.</param>
        public ShaderProgram(string vertexString, string fragmentString)
        {
            CreateShader(ReadShader(vertexString.Split('\n'), true, true), ReadShader(fragmentString.Split('\n'), true, true));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Shaders.ShaderProgram"/> class.
        /// </summary>
        /// <param name="vertexFile">The vertex shader file.</param>
        /// <param name="fragmentFile">The fragment shader file.</param>
        public ShaderProgram(MyFile vertexFile, MyFile fragmentFile)
        {
            CreateShader(ReadShader(vertexFile, true, true), ReadShader(fragmentFile, true, true));
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Shaders.ShaderProgram"/> is reclaimed by garbage collection.
        /// </summary>
        ~ShaderProgram()
        {
            CleanUp();
        }

        /// <summary>
        /// Creates the shader.
        /// </summary>
        /// <param name="vertexBuilder">The vertex builder.</param>
        /// <param name="fragmentBuilder">The fragment builder.</param>
        private void CreateShader(StringBuilder vertexBuilder, StringBuilder fragmentBuilder)
        {
            uint vertexShaderID = LoadShader(vertexBuilder, GL20.GL_VERTEX_SHADER);
            uint fragmentShaderID = LoadShader(fragmentBuilder, GL20.GL_FRAGMENT_SHADER);
            /*programID = GL20.glCreateProgram();
            GL20.glAttachShader(programID, vertexShaderID);
            GL20.glAttachShader(programID, fragmentShaderID);

            foreach (string l in layoutLocations)
            {
                string locationName = l.Substring(l.LastIndexOf(" ") + 1, l.Length - 1);
                uint locationValue = uint.Parse(l.Substring(FindCharPos(l, '=') + 1, FindCharPos(l, ')')).Replace("\\s+", ""));
                GL20.glBindAttribLocation(programID, locationValue, ref locationName);
            }

            GL20.glLinkProgram(programID);
            GL20.glDetachShader(programID, vertexShaderID);
            GL20.glDetachShader(programID, fragmentShaderID);
            GL20.glDeleteShader(vertexShaderID);
            GL20.glDeleteShader(fragmentShaderID);

            Start();

            foreach (string b in layoutBindings)
            {
                string bindingName = b.Substring(b.LastIndexOf(" ") + 1, b.Length - 1);
                int bindingValue = int.Parse(b.Substring(FindCharPos(b, '=') + 1, FindCharPos(b, ')')).Replace("\\s+", ""));
                UniformSampler sampler = new UniformSampler(bindingName);
                sampler.StoreUniformLocation(programID);
                sampler.LoadTexUnit(bindingValue);
            }

            Stop();*/
        }

        /// <summary>
        /// Loads the shader.
        /// </summary>
        /// <returns>The shader.</returns>
        /// <param name="builder">The shader builder string.</param>
        /// <param name="type">The shader type.</param>
        private uint LoadShader(StringBuilder builder, uint type)
        {
            uint shaderID = GL20.glCreateShader(type);
            var builderLength = new int[]{builder.Length};
            string[] source = builder.ToString().Split('\n');
            GL20.glShaderSource(shaderID, 1, ref source, ref builderLength);
            GL20.glCompileShader(shaderID);

            //if (GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            //    Console.WriteLine(GL20.glGetShaderInfoLog(shaderID, 500));
            //    Console.WriteLine("Could not compile shader " + file);
            //    Environment.Exit(-1);
           // }

            return shaderID;


            /*uint shaderID = GL20.glCreateShader(type);
            var builderLength = new int[]{builder.Length};
            string[] source = builder.ToString().Split('\n');
            GL20.glShaderSource(shaderID, 1, ref source, ref builderLength);
            GL20.glCompileShader(shaderID);

            var compiled = new int[]{0};
            GL20.glGetShaderiv(shaderID, GL20.GL_COMPILE_STATUS, ref compiled);

            if (compiled[0] == GL11.GL_FALSE)
            {
                int length = 0;
                var infoLog= new string[500];
                GL20.glGetShaderInfoLog(shaderID, 500, ref length, ref infoLog);
                Console.WriteLine(infoLog);
                Console.WriteLine("Could not compile shader Type " + type);
                Environment.Exit(-1);
            }

            return shaderID;*/
        }

        private StringBuilder ReadShader(string[] file, bool vertexShader, bool addToLayouts)
        {
            StringBuilder shaderSource = new StringBuilder();

            foreach (string l in file)
            {
                string line = l;

                if (line.Contains("varying"))
                {
                    if (vertexShader)
                    {
                        line = line.Replace("varying", "out");
                    }
                    else 
                    {
                        line = line.Replace("varying", "in");
                    }
                }

                if (line.Contains("#include"))
                {
                    String included = line.Replace("\\s+", "").Replace("\"", "");
                    included = included.Substring("#include".Length, included.Length);
                    StringBuilder includedString = new StringBuilder();

                    if (included.Contains("MATHS")) // TODO: Other includes!
                    {
                        string[] data = ShaderToolbox.MATHS.Split('\n');

                        foreach(string d in data)
                        {
                            includedString.Append(d);
                        }
                    }

                    if (includedString != null)
                    {
                        shaderSource.Append(includedString);
                    }
                } 
                else if (line.Replace("\\s+", "").StartsWith("layout") && addToLayouts)
                {
                    if (line.Contains("location"))
                    {
                        layoutLocations.Add(line);
                        shaderSource.Append(line.Substring(FindCharPos(line, ')') + 1, line.Length)).Append("//\n");
                    }
                    else if (line.Contains("binding"))
                    {
                        layoutBindings.Add(line);
                        shaderSource.Append(line.Substring(FindCharPos(line, ')') + 1, line.Length)).Append("//\n");
                    }
                }
                else
                {
                    shaderSource.Append(line).Append("//\n");
                }
            }

            return shaderSource;
        }

        /// <summary>
        /// Reads the shader.
        /// </summary>
        /// <returns>The shader.</returns>
        /// <param name="file">The shader file.</param>
        /// <param name="vertexShader">If set to <c>true</c> its a vertex shader.</param>
        /// <param name="addToLayouts">If set to <c>true</c> it adds to the layouts.</param>
        private StringBuilder ReadShader(MyFile file, bool vertexShader, bool addToLayouts)
        {
            StringBuilder shaderSource = new StringBuilder();

            try
            {
                StreamReader reader = file.GetReader();
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains("varying"))
                    {
                        if (vertexShader)
                        {
                            line = line.Replace("varying", "out");
                        }
                        else {
                            line = line.Replace("varying", "in");
                        }
                    }

                    if (line.Contains("#include"))
                    {
                        String included = line.Replace("\\s+", "").Replace("\"", "");
                        included = included.Substring("#include".Length, included.Length);
                        StringBuilder includedString = ReadShader(new MyFile(included), true, false);
                        shaderSource.Append(includedString);
                    }
                    else if (line.Replace("\\s+", "").StartsWith("layout") && addToLayouts)
                    {
                        if (line.Contains("location"))
                        {
                            layoutLocations.Add(line);
                            shaderSource.Append(line.Substring(FindCharPos(line, ')') + 1, line.Length)).Append("//\n");
                        }
                        else if (line.Contains("binding"))
                        {
                            layoutBindings.Add(line);
                            shaderSource.Append(line.Substring(FindCharPos(line, ')') + 1, line.Length)).Append("//\n");
                        }
                    }
                    else
                    {
                        shaderSource.Append(line).Append("//\n");
                    }
                }

                reader.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not read file " + file);
                Console.WriteLine(e.StackTrace);
                Environment.Exit(-1);
            }

            return shaderSource;
        }

        /// <summary>
        /// Finds the char position.
        /// </summary>
        /// <param name="line">The string line.</param>
        /// <param name="c">The char to find.</param>
        /// <returns>The char position.</returns>
        private int FindCharPos(string line, char c)
        {
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == c)
                {
                    return i;
                }
            }

            return 0;
        }

        /// <summary>
        /// Stores all uniform locations.
        /// </summary>
        /// <param name="uniforms">The uniforms to store.</param>
        public void StoreAllUniformLocations(Uniform[] uniforms)
        {
            foreach (Uniform uniform in uniforms)
            {
                uniform.StoreUniformLocation(programID);
            }

            GL20.glValidateProgram(programID);
        }

        /// <summary>
        /// Starts the shader program.
        /// </summary>
        public void Start()
        {
            GL20.glUseProgram(programID);
        }
        
        /// <summary>
        /// Stops the shader program.
        /// </summary>
        public void Stop()
        {
            GL20.glUseProgram(0);
        }
        
        /// <summary>
        /// Deletes the shader, do not start after calling this.
        /// </summary>
        public void CleanUp()
        {
            GL20.glUseProgram(0);
            GL20.glDeleteProgram(programID);
        }
    }
}
