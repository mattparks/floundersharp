﻿using Flounder.Toolbox.Vectors;
using OpenGL;

namespace Flounder.Shaders
{
    /// <summary>
    /// Represents a 2 value vector uniform type that can be loaded to the shader.
    /// </summary>
    public class UniformVec2 : Uniform
    {
        /// <summary>
        /// The current x.
        /// </summary>
        private float currentX;

        /// <summary>
        /// The current y.
        /// </summary>
        private float currentY;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Shaders.UniformVec2"/> class.
        /// </summary>
        /// <param name="name">The uniform name.</param>
        public UniformVec2(string name) : base(name)
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Flounder.Shaders.UniformVec2"/> is reclaimed by garbage collection.
        /// </summary>
        ~UniformVec2()
        {
        }

        /// <summary>
        /// Loads a Vector2f to the uniform if the value already on the GPU is not the same as the new value.
        /// </summary>
        /// <param name="vector">The new vector.</param>
        public void LoadVec2(Vector2f vector)
        {
            LoadVec2(vector.x, vector.y);
        }
        
        /// <summary>
        /// Loads a x and y value to the uniform if the value already on the GPU is not the same as the new value.
        /// </summary>
        /// <param name="x">The new x value.</param>
        /// <param name="y">The new y value.</param>
        public void LoadVec2(float x, float y)
        {
            if (x != currentX || y != currentY)
            {
                currentX = x;
                currentY = y;
                GL20.glUniform2f(base.GetLocation(), x, y);
            }
        }
    }
}
