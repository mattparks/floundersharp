﻿using Flounder.Toolbox.Vectors;
using OpenGL;

namespace Flounder.Shaders
{
    /// <summary>
    /// Represents a 4 value vector uniform type that can be loaded to the shader.
    /// </summary>
    public class UniformVec4 : Uniform
    {
        /// <summary>
        /// The current x.
        /// </summary>
        private float currentX;

        /// <summary>
        /// The current y.
        /// </summary>
        private float currentY;

        /// <summary>
        /// The current z.
        /// </summary>
        private float currentZ;

        /// <summary>
        /// The current w.
        /// </summary>
        private float currentW;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Shaders.UniformVec4"/> class.
        /// </summary>
        /// <param name="name">The uniform name.</param>
        public UniformVec4(string name) : base(name)
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Flounder.Shaders.UniformVec4"/> is reclaimed by garbage collection.
        /// </summary>
        ~UniformVec4()
        {
        }

        /// <summary>
        /// Loads a Vector4f to the uniform if the value already on the GPU is not the same as the new value.
        /// </summary>
        /// <param name="vector">The new vector.</param>
        public void LoadVec4(Vector4f vector)
        {
            LoadVec4(vector.x, vector.y, vector.z, vector.w);
        }

        /// <summary>
        /// Loads a x, y, z and w value to the uniform if the value already on the GPU is not the same as the new value.
        /// </summary>
        /// <param name="x">The new x value.</param>
        /// <param name="y">The new y value.</param>
        /// <param name="z">The new z value.</param>
        /// <param name="w">The new w value.</param>
        public void LoadVec4(float x, float y, float z, float w)
        {
            if (x != currentX || y != currentY || z != currentZ || w != currentW)
            {
                currentX = x;
                currentY = y;
                currentZ = z;
                currentW = w;
                GL20.glUniform4f(base.GetLocation(), x, y, z, w);
            }
        }
    }
}
