﻿using OpenGL;

namespace Flounder.Shaders
{
    /// <summary>
    /// Represents a float uniform type that can be loaded to the shader.
    /// </summary>
    public class UniformFloat : Uniform
    {
        /// <summary>
        /// The current value.
        /// </summary>
        private float currentValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Shaders.UniformFloat"/> class.
        /// </summary>
        /// <param name="name">The uniform name.</param>
        public UniformFloat(string name) : base(name)
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Flounder.Shaders.UniformFloat"/> is reclaimed by garbage collection.
        /// </summary>
        ~UniformFloat()
        {
        }

        /// <summary>
        /// Loads a float to the uniform if the value already on the GPU is not the same as the new value.
        /// </summary>
        /// <param name="value">The new value.</param>
        public void LoadFloat(float value)
        {
            if (currentValue != value)
            {
                currentValue = value;
                GL20.glUniform1f(base.GetLocation(), value);
            }
        }
    }
}
