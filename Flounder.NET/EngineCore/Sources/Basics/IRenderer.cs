﻿using Flounder.Profiling;
using Flounder.Toolbox.Vectors;

namespace Flounder.Basics
{
    /// <summary>
    /// Represents a sub renderer in the engine. 
    /// </summary>
    public abstract class IRenderer
    {
        /// <summary>
        /// The profile timer.
        /// </summary>
        private ProfileTimer profileTimer;

        /// <summary>
        /// Gets the last render time in ms.
        /// </summary>
        /// <value>The last render time in ms.</value>
        public double renderTimeMs { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Basics.IRenderer"/> class.
        /// </summary>
        public IRenderer()
        {
            profileTimer = new ProfileTimer();
            renderTimeMs = 0;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Basics.IRenderer"/> is reclaimed by garbage collection.
        /// </summary>
        ~IRenderer()
        {
            CleanUp();
        }

        /// <summary>
        /// Renders the internal renderer and manages profiling.
        /// </summary>
        /// <param name="clipPlane">The current clip plane.</param>
        /// <param name="camera">The camera to be used when rendering.</param>
        public void Render(Vector4f clipPlane, ICamera camera)
        {
            profileTimer.StartInvocation();
            RenderObjects(clipPlane, camera);
            profileTimer.StopInvocation();
            renderTimeMs = profileTimer.Reset();
        }

        /// <summary>
        /// Used as a internal render method for renderers.
        /// </summary>
        /// <param name="clipPlane">The current clip plane.</param>
        /// <param name="camera">The camera to be used when rendering.</param>
        public abstract void RenderObjects(Vector4f clipPlane, ICamera camera);

        /// <summary>
        /// Cleans up all of the renderers processes.
        /// </summary>
        public abstract void CleanUp();
    }
}
