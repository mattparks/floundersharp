﻿using Flounder.Toolbox.Matrices;

namespace Flounder.Basics
{
    /// <summary>
    /// The engines main renderer, it organizes render objects passes of subtract renderers.
    /// </summary>
    public abstract class IRendererMaster
    {
        /// <summary>
        /// Initializes the various renderer types and various functionalities.
        /// </summary>
        public abstract void Init();

        /// <summary>
        /// Carries out the rendering of all components.
        /// </summary>
        public abstract void Render();

        /// <summary>
        /// Cleans up all of the render objects processes. Should be called when the game closes.
        /// </summary>
        public abstract void CleanUp();

        /// <summary>
        /// Gets the projection matrix.
        /// </summary>
        /// <returns>The projection matrix.</returns>
        public abstract Matrix4f GetProjectionMatrix();
    }
}
