﻿using Flounder.Devices;
using Flounder.Processing;
using Flounder.Processing.RequestGL;
using System;

namespace Flounder.Basics
{
    /// <summary>
    /// Deals with much of the initializing, updating, and cleaning up of the engine.
    /// </summary>
    public static class EngineCore
    {
        /// <summary>
        /// Gets the engine module.
        /// </summary>
        /// <value>The current engine module.</value>
        public static IModule module { get; private set; }

        /// <summary>
        /// Carries out any necessary initializations of the engine.
        /// </summary>
        /// <param name="module">The engines main module component to run off of.</param>
        public static void Init(IModule module)
        {
            DeviceDisplay.CreateDeviceDisplay();
            DeviceSound.CreateDeviceSound();
            (EngineCore.module = module).Init();
        }

        /// <summary>
        /// Updates many engine systems before every frame.
        /// </summary>
        public static void PreRenderUpdate()
        {
            DeviceJoystick.Update();
            DeviceKeyboard.Update();
            DeviceMouse.Update();
            DeviceSound.Update();

            module.Update();
            // GuiManager.UpdateGuis();
        }

        /// <summary>
        /// Renders the engines master renderer.
        /// </summary>
        public static void Render()
        {
            module.rendererMaster.Render();
        }

        /// <summary>
        /// Updates the display and carries out OpenGL request calls.
        /// </summary>
        public static void PostRenderUpdate()
        {
            GlRequestProcessor.DealWithTopRequests();
            DeviceDisplay.Update();
        }

        /// <summary>
        /// Gets the display delta in seconds.
        /// </summary>
        /// <returns>The delta in seconds.</returns>
        public static float GetDeltaSeconds()
        {
            return module.game.delta;
        }

        /// <summary>
        /// Deals with closing down the engine and all necessary systems.
        /// </summary>
        public static void Close()
        {
            EngineCore.module.CleanUp();

            // Loader.CleanUp();
            RequestProcessor.CleanUp();
            GlRequestProcessor.CompleteAllRequests();
            // TextureManager.CleanUp();

            DeviceDisplay.CleanUp();
            DeviceJoystick.CleanUp();
            DeviceKeyboard.CleanUp();
            DeviceMouse.CleanUp();
            DeviceSound.CleanUp();

            #if DEBUG
            // Displays a message to close the engine.
            Console.WriteLine("Flounder Engine has been terminated! Press any key to exit.");
            Console.ReadKey();
            Environment.Exit(0);
            #endif
        }
    }
}
