﻿using Flounder.Toolbox.Vectors;
using Flounder.Devices;
using Flounder.Toolbox;
using OpenGL.GLFW;
using System.Threading;
using System;

namespace Flounder.Basics
{
    /// <summary>
    /// Represents a game loop for the engine to run from.
    /// </summary>
    public abstract class IGame
    {
        /// <summary>
        /// Gets the last frame time.
        /// </summary>
        /// <value>The last frame time.</value>
        public float lastFrameTime { get; private set; }

        /// <summary>
        /// Gets the frame delta.
        /// </summary>
        /// <value>The frame delta.</value>
        public float delta { get; private set; }

        /// <summary>
        /// Gets the game time.
        /// </summary>
        /// <value>The game time.</value>
        public float time { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Flounder.Basics.IGame"/> is running.
        /// </summary>
        /// <value><c>true</c> if is running; otherwise, <c>false</c>.</value>
        public bool isRunning { get; private set; }

        /// <summary>
        /// Gets the cameras focus position.
        /// </summary>
        /// <value>The cameras focus position.</value>
        public Vector3f focusPosition { get; private set; }

        /// <summary>
        /// Gets the cameras focus rotation.
        /// </summary>
        /// <value>The cameras focus rotation.</value>
        public Vector3f focusRotation { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this game is paused.
        /// </summary>
        /// <value><c>true</c> if game paused; otherwise, <c>false</c>.</value>
        public bool gamePaused { get; private set; }

        /// <summary>
        /// Gets the screen blur.
        /// </summary>
        /// <value>The screen blur.</value>
        public float screenBlur { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Basics.IGame"/> class.
        /// </summary>
        /// <param name="module">The engines module to be used when idealizing the engine.</param>
        public IGame(IModule module)
        {
            lastFrameTime = 0.0f;
            delta = 0.0f;
            time = 0.0f;
            isRunning = true;

            module.AddGame(this);
            EngineCore.Init(module);
            Run();
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Basics.IGame"/> is reclaimed by garbage collection.
        /// </summary>
        ~IGame()
        {
            // Stops from cleaning up if cleaned up!
            if (!isRunning)
            {
                CleanUp();
                EngineCore.Close();
                isRunning = false;
            }
        }

        /// <summary>
        /// Runs this game instance.
        /// </summary>
        private void Run()
        {
            var lastTime = SystemTime.Nanoseconds();
            var ns = 1000000000.0f / 60.0f;
            var delta2 = 0.0f;
            var frames = 0.0f;
            var updates = 0.0f;
            var timerStart = SystemTime.Microseconds();

            while (DeviceDisplay.IsOpen())
            {
                var startTime = SystemTime.Nanoseconds();
                delta2 += (startTime - lastTime) / ns;
                lastTime = startTime;
                var update = delta2 >= 1.0;

                if (update)
                {
                    EngineCore.PreRenderUpdate();
                    updates++;
                    delta2--;

                    // Updates static update times.
                    var currentFrameTime = (float)GLFW3.GetTime();
                    delta = currentFrameTime - lastFrameTime;
                    lastFrameTime = currentFrameTime;
                    time += delta;
                }

                EngineCore.Render();
                frames++;

                if (update)
                {
                    EngineCore.PostRenderUpdate();
                }

                if (SystemTime.Microseconds() - timerStart > 1000)
                {
                    Console.WriteLine("{0}ups, {1}fps.", updates, frames);
                    timerStart += 1000;
                    updates = 0;
                    frames = 0;
                }
            }

            CleanUp();
            EngineCore.Close();
            isRunning = false;
        }

        /// <summary>
        /// Updates the current engines game settings.
        /// </summary>
        /// <param name="focusPosition">The position of the object the camera focuses on.</param>
        /// <param name="focusRotation">The rotation of the object the camera focuses on.</param>
        /// <param name="gamePaused"><c>true</c> if game paused; otherwise, <c>false</c>. Used to stop inputs to camera in menus.</param>
        /// <param name="screenBlur">The factor (-1 to 1) by where the screen will be blurred from on the paused screen.</param>
        public void UpdateGame(Vector3f focusPosition, Vector3f focusRotation, bool gamePaused, float screenBlur)
        {
            this.focusPosition = focusPosition;
            this.focusRotation = focusRotation;
            this.gamePaused = gamePaused;
            this.screenBlur = screenBlur;
        }

        /// <summary>
        /// Used to initialise the game, can be used to generate the world.
        /// </summary>
        public abstract void Init();

        /// <summary>
        /// Used to update internal game objects and values. (call the {@link #updateGame(Vector3f, Vector3f, bool, float) updateGame} method at the end of the function.
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// Cleans up all of the game objects. Should be called when the game closes.
        /// </summary>
        public abstract void CleanUp();
    }
}