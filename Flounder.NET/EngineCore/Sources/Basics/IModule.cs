﻿using System;
using System.IO;

namespace Flounder.Basics
{
    /// <summary>
    /// Holds info that the Engine will run off of in the form of a engine module.
    /// </summary>
    public class IModule
    {
        /// <summary>
        /// Gets the modules renderer master.
        /// </summary>
        /// <value>The modules renderer master.</value>
        public IRendererMaster rendererMaster { get; private set; }

        /// <summary>
        /// Gets the modules camera.
        /// </summary>
        /// <value>The modules camera.</value>
        public ICamera camera { get; private set; }

        /// <summary>
        /// Gets the game object.
        /// </summary>
        /// <value>The game object.</value>
        public IGame game { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flounder.Basics.IModule"/> class.
        /// </summary>
        /// <param name="rendererMaster">The master renderer for the Engine to use when rendering.</param>
        /// <param name="camera">The camera to be used when running the master renderer.</param>
        public IModule(IRendererMaster rendererMaster, ICamera camera)
        {
            this.rendererMaster = rendererMaster;
            this.camera = camera;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the <see cref="Flounder.Basics.IModule"/> is reclaimed by garbage collection.
        /// </summary>
        ~IModule()
        {
            CleanUp();
        }

        /// <summary>
        /// Used to add the game object too the module.
        /// </summary>
        /// <param name="game">The game object to be added to the module.</param>
        public void AddGame(IGame game)
        {
            this.game = game;
        }

        /// <summary>
        /// Initialises this module instance.
        /// </summary>
        public void Init()
        {
            try
            {
                rendererMaster.Init();
                game.Init();
            }
            catch (IOException e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        /// <summary>
        /// Updates the game and the camera.
        /// </summary>
        public void Update()
        {
            game.Update();
            camera.MoveCamera(game.focusPosition, game.focusRotation, game.gamePaused);
        }

        /// <summary>
        /// Cleans up all of the modules processes. Should be called when the module closes.
        /// </summary>
        public void CleanUp()
        {
            rendererMaster.CleanUp();
            game.CleanUp();
        }
    }
}
