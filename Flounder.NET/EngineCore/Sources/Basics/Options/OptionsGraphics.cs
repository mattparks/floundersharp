﻿namespace Flounder.Basics.Options
{
    /// <summary>
    /// Holds basic graphical engine settings.
    /// </summary>
    public static class OptionsGraphics
    {
        /// <summary>
        /// The displays title.
        /// </summary>
        public static string DISPLAY_TITLE = "Flounder Demo";

        /// <summary>
        /// The displays width.
        /// </summary>
        public static int DISPLAY_WIDTH = 1280;

        /// <summary>
        /// The displays height.
        /// </summary>
        public static int DISPLAY_HEIGHT = 720;

        /// <summary>
        /// Is the display enabled to use vSync.
        /// </summary>
        public static bool V_SYNC = false;

        /// <summary>
        /// The displays FPS cap.
        /// </summary>
        public static int FPS_CAP = 144;

        /// <summary>
        /// Is the display fullscreen?
        /// </summary>
        public static bool FULLSCREEN = false;

        /// <summary>
        /// Are shadows enabled?
        /// </summary>
        public static bool SHADOWS = true;

        /// <summary>
        /// Is AA exabled for the final display image?
        /// </summary>
        public static bool ANTI_ALIASING = true;

        /// <summary>
        /// The MSAAA samples used in the final image.
        /// </summary>
        public static int MSAA_SAMPLES = 4;

        /// <summary>
        /// The display image to size ratio.
        /// </summary>
        public static float DISPLAY_PIXEL_RATIO = 1.0f;

        /// <summary>
        /// The current post effect selected.
        /// </summary>
        public static int POST_EFFECT = 0;

        /// <summary>
        /// How many post effects can the user select from?
        /// </summary>
        public static int POST_EFFECT_MAX = 10;
    }
}
