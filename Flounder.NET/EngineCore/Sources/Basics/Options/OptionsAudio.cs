﻿namespace Flounder.Basics.Options
{
    /// <summary>
    /// Holds basic audio engine settings.
    /// </summary>
    public static class OptionsAudio
    {
        /// <summary>
        /// The master sound volume.
        /// </summary>
        public static float SOUND_VOLUME = 1.0f;
    }
}
