﻿using Flounder.Toolbox.Vectors;
using Flounder.Toolbox.Matrices;
using Flounder.Space;
using Flounder.Sounds;

namespace Flounder.Basics
{
    /// <summary>
    /// This interface is used throughout the engine wherever the camera is involved, so that the engine doesn't rely at all on the camera's implementation.
    /// </summary>
    public interface ICamera : AudioListener 
    {   
        /// <summary>
        /// Gets the cameras near plane.
        /// </summary>
        /// <returns>The cameras near plane.</returns>
        float GetNearPlane();

        /// <summary>
        /// Gets the cameras far plane.
        /// </summary>
        /// <returns>The cameras far plane.</returns>
        float GetFarPlane();

        /// <summary>
        /// Gets the field of view angle for the view frustum.
        /// </summary>
        /// <returns>The field of view angle for the view frustum.</returns>
        float GetFOV();

        /// <summary>
        /// Checks inputs and carries out smooth camera movement. Should be called every frame.
        /// </summary>
        /// <param name="focusPosition">The position of the object the camera focuses on.</param>
        /// <param name="focusRotation">The rotation of the object the camera focuses on.</param>
        /// <param name="gamePaused"><c>True</c> if the game is currently paused. Used to stop inputs to camera in menus.</param>
        void MoveCamera(Vector3f focusPosition, Vector3f focusRotation, bool gamePaused);

        /// <summary>
        /// Gets the cameras view matrix created by the current camera position and rotation.
        /// </summary>
        /// <returns>The cameras view matrix.</returns>
        Matrix4f GetViewMatrix();

        /// <summary>
        /// Gets the cameras view frustum created by the current camera position and rotation.
        /// </summary>
        /// <returns>The cameras view frustum.</returns>
        Frustum GetViewFrustum();

        /// <summary>
        /// Calculates the view matrix for the reflection pass, given the height of the water plane.
        /// </summary>
        /// <param name="planeHeight">The plane height.</param>
        /// <returns>The reflection view matrix.</returns>
        Matrix4f GetReflectionViewMatrix(float planeHeight);

        /// <summary>
        /// Reflects the camera over the specified waterHeight.
        /// </summary>
        /// <param name="waterHeight">The water height.</param>
        void Reflect(float waterHeight);

        /// <summary>
        /// Gets the cameras position.
        /// </summary>
        /// <returns>The cameras position.</returns>
        new Vector3f GetPosition();

        /// <summary>
        /// Gets the cameras pitch (x rotation).
        /// </summary>
        /// <returns>The cameras pitch.</returns>
        float GetPitch();

        /// <summary>
        /// Gets the cameras yaw (y rotation).
        /// </summary>
        /// <returns>The cameras yaw.</returns>
        float GetYaw();

        /// <summary>
        /// Gets the cameras aim distance.
        /// </summary>
        /// <returns>The aim distance at terrain.</returns>
        float GetAimDistance();
    }
}
